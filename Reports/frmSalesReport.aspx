﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmSalesReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmSalesReport" %>

<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'SaleReport');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "SaleReport";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>



    <script type="text/javascript">
        function pageLoad() {

            var ctrltxtInventory;
            $('#divPurchaseDate').hide();
            $("[id*=divFilter]").hide();
            //$("[id*=divSalesAudit]").hide();

            //SetAutoComplete();

            var Bydatevalue = $("#<%=chkBydate.ClientID%>").is(':checked');
            if (!Bydatevalue) {
                SetDisable($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"), 'D')
            }
            var BydatevalueMore = $("#<%=chkBydateMore.ClientID%>").is(':checked');
            if (!BydatevalueMore) {
                SetDisable($("#<%= txtFromDateMore.ClientID %>"), $("#<%= txtToDateMore.ClientID %>"), 'D')
            }
            var BydatevalueCredit = $("#<%=chkBydateCredit.ClientID%>").is(':checked');
            if (!BydatevalueCredit) {
                SetDisable($("#<%= txtFromDateCredit.ClientID %>"), $("#<%= txtToDateCredit.ClientID %>"), 'D')
            }
            var BydatevalueReturn = $("#<%=chkByDateReturn.ClientID%>").is(':checked');
            if (!BydatevalueReturn) {
                SetDisable($("#<%= txtFromDateReturn.ClientID %>"), $("#<%= txtToDateReturn.ClientID %>"), 'D')
            }
            var BydatevaluePre = $("#<%=chkBydatePre.ClientID%>").is(':checked');
            if (!BydatevaluePre) {
                SetDisable($("#<%= txtFromDatePre.ClientID %>"), $("#<%= txtToDatePre.ClientID %>"), 'D')
            }
            var BydatevalueAudit = $("#<%=chkBydateSA.ClientID%>").is(':checked');
            if (!BydatevalueAudit) {
                SetDisable($("#<%= txtFromDateAudit.ClientID %>"), $("#<%= txtToDateAudit.ClientID %>"), 'D')
            }


            SetDefaultDate($("#<%= txtPurchaseFromDate.ClientID %>"), $("#<%= txtPurchaseToDate.ClientID %>"));
            SetDefaultDate($("#<%= txtSalFromDate.ClientID %>"), $("#<%= txtSalToDate.ClientID %>"));

            var vSales = $("[id*=DivSalesReport]").is(':visible');
            var vSalesMore = $("[id*=DivSalesMore]").is(':visible');
            var vPreSales = $("[id*=DivPreSales]").is(':visible');
            var vSalesreturn = $("[id*=DivSalesReturn]").is(':visible');
            var vCreditDue = $("[id*=DivCreditDue]").is(':visible');
            var vSalesAudit = $("[id*=divSalesAudit]").is(':visible');
            //alert('Sales = '+vSales);
            //alert('vSalesMore = ' + vSalesMore);
            //alert('vPreSales = ' + vPreSales);
            //alert('vSalesreturn = ' + vSalesreturn);
            //alert('vCreditDue = ' + vCreditDue);


            if (vSales) {
                var checkedvalue = $("#<%=rdoSalesReport.ClientID%>").find(":checked").val();
                //alert(checkedvalue);
                //aalayam
                //if ($.trim(checkedvalue) == 'InSummary' || $.trim(checkedvalue) == 'InDetail' || $.trim(checkedvalue) == 'Paymode' || $.trim(checkedvalue) == 'Session' || $.trim(checkedvalue) == 'Cashier' || $.trim(checkedvalue) == 'Hourly' || $.trim(checkedvalue) == 'Total'||$.trim(checkedvalue)=='InPaymode') {
                //    $("[id*=GrpfilterSales]").hide();
                //}
                //else {
                //    $("[id*=GrpfilterSales]").show();
                //    //$("[id*=GroupBy]").hide(); // dinesh
                //}


                //
                //if (checkedvalue == 'Selling') {
                //    Bydatevalue.checked();
                //}
                //if (checkedvalueMore == "BillRange") {
                //    $("#ContentPlaceHolder1_chkBydate").attr("disabled", "disabled");
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').show();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                //    $('#ContentPlaceHolder1_divTerminalSales').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                //    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                //    $("[id*=GroupByDailySD]").hide();
                //    $('#ContentPlaceHolder1_divBatchNo').hide();
                //    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                //}

                if (checkedvalue == "InPaymodeView1") {
                    $("#ContentPlaceHolder1_chkBydate").attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }

                if (checkedvalue == "InSummary" || checkedvalue == "InDetail")//aalayam 23042018
                {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $("[id*=GroupByDailySD]").hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }

                if (checkedvalue == "Item" || checkedvalue == "Shelf") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_divBatchNo').show();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    $("[id*=GroupByDailySD]").show();
                    if (checkedvalue == "Shelf") {
                        $('#ContentPlaceHolder1_divBatchNo').hide();
                    }
                }

                if (checkedvalue == "Department" || checkedvalue == "Category" || checkedvalue == "Brand"
                    || checkedvalue == "Vendor" || checkedvalue == "Class") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }
                if (checkedvalue == "Paymode" || checkedvalue == "Total" || checkedvalue == "Hourly"
                    || checkedvalue == "Terminal" || checkedvalue == "Cashier") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    if (checkedvalue == "Paymode")
                        $('#ContentPlaceHolder1_PaymodeDetail').show();
                }
                if (checkedvalue == "Selling") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }
                if (checkedvalue == "Batch" || checkedvalue == "NonBatch") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $('#ContentPlaceHolder1_divBatchNo').show();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    if (checkedvalue == "NonBatch") {
                        $('#ContentPlaceHolder1_divBatchNo').hide();
                    }
                    $("[id*=GroupByDailySD]").show();

                }
                if (checkedvalue == "BillRange") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").hide();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }
                if (checkedvalue == "WeightBalance") {
                    $("#ContentPlaceHolder1_chkBydate").attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").hide();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    $("[id*=GrpfilterSales]").hide();
                }
                if (checkedvalue == "TerminalPay") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();

                    if (checkedvalue == "WeightBalance") {
                        $("[id*=GrpfilterSales]").hide();
                    }
                }//vijayv

                if (checkedvalue == "Session" || checkedvalue == "WeightBalance")
                    $("[id*=GrpfilterSales]").hide();
                else
                    $("[id*=GrpfilterSales]").show();

                if (checkedvalue == "Category")//aalayam 04052018
                    $("[id*=GroupByGst]").show();
                else
                    $("[id*=GroupByGst]").hide();

            }
            if (vPreSales) {
                var chekedValuePreSales = $("#<%=rdoPreSales.ClientID%> input:radio:checked").val();
                if (chekedValuePreSales == 'Cancel' || chekedValuePreSales == 'SCancel' || chekedValuePreSales == 'MCancel')
                    $("[id*=GrpfilterPreSales]").hide();
                else
                    $("[id*=GrpfilterPreSales]").show();
                if (chekedValuePreSales.indexOf('M') != -1) {
                    $('#ContentPlaceHolder1_PreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_PreReportFilterUserControl_divCashier').hide();
                }
                else {
                    $('#ContentPlaceHolder1_PreReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_PreReportFilterUserControl_divCashier').show();
                }
            }

            if (vCreditDue) {
                var checkedvalueCredit = $("#<%=rdoCredidue.ClientID%>").find(":checked").val();
                if (checkedvalueCredit == 'Credit')//aalayam 02052018
                {
                    $('#ContentPlaceHolder1_divMember').show();
                    $('#ContentPlaceHolder1_divZone').hide();
                    $('#ContentPlaceHolder1_divInvoiceNo').show();
                    $('#ContentPlaceHolder1_divReceipt').hide();
                }
                if (checkedvalueCredit == 'Outstanding' || checkedvalueCredit == 'Limit' || checkedvalueCredit == 'Days' || checkedvalueCredit == 'Receipt' || checkedvalueCredit == 'Delete' || checkedvalueCredit == 'DueSales' || checkedvalueCredit == 'Pending' || checkedvalueCredit == 'Partialy' || checkedvalueCredit == 'Closed') {
                    $('#ContentPlaceHolder1_divMember').show();
                    $('#ContentPlaceHolder1_divZone').hide();
                    $('#ContentPlaceHolder1_divInvoiceNo').hide();
                    $('#ContentPlaceHolder1_divReceipt').hide();
                }
                if (checkedvalueCredit == 'DueReceipt') {
                    $('#ContentPlaceHolder1_divMember').hide();
                    $('#ContentPlaceHolder1_divZone').hide();
                    $('#ContentPlaceHolder1_divInvoiceNo').show();
                    $('#ContentPlaceHolder1_divReceipt').show();
                }
                if (checkedvalueCredit == 'Zone') {
                    $('#ContentPlaceHolder1_divMember').show();
                    $('#ContentPlaceHolder1_divZone').show();
                    $('#ContentPlaceHolder1_divInvoiceNo').hide();
                    $('#ContentPlaceHolder1_divReceipt').hide();
                }//
                if (checkedvalueCredit == 'Credit') {
                    $("[id*=GroupByCR]").show();
                    $("[id*=GroupByCRDue]").show();
                }
                else if (checkedvalueCredit == 'Outstanding') {
                    $("[id*=GroupByCR]").show();
                    $("[id*=GroupByCRDue]").hide();
                }
                else if (checkedvalueCredit == 'Receipt') {
                    $("[id*=GroupByCR]").show();
                    $("[id*=GroupByCRDue]").hide();
                }
                else {
                    $("[id*=GroupByCR]").hide();
                    $("[id*=GroupByCRDue]").hide();
                }
                if (checkedvalueCredit == 'Limit' || checkedvalueCredit == 'Days')//aalayam 02052018
                {
                    $("[id*=chkBydateCredit]").attr("disabled", "disabled");
                    $("[id*=txtFromDateCredit]").attr("disabled", "disabled");
                    $("[id*=txtToDateCredit]").attr("disabled", "disabled");
                }
                else {
                    $("[id*=chkBydateCredit]").removeAttr("disabled");
                    $("[id*=txtFromeDateCredit]").removeAttr("disabled");
                    $("[id*=txtToDateCredit]").removeAttr("disabled");//
                }
            }
            //<asp:ListItem Selected="True" Value="ALL">Sales Refund-ALL</asp:ListItem>
            //                   <asp:ListItem Value="CASH">Sales Refund-CASH</asp:ListItem>
            //                   <asp:ListItem Value="Exchange">Sales Refund-Exchange Voucher</asp:ListItem>
            //                   <asp:ListItem Value="UnUsed">Exchange Voucher - UnUsed</asp:ListItem>
            //                   <asp:ListItem Value="Used">Exchange Voucher - Used</asp:ListItem>
            //                   <asp:ListItem Value="Expired">Exchange Voucher - Expired</asp:ListItem>
            //                   <asp:ListItem Value="TradeUnUsed">Trade - UnUsed List</asp:ListItem>
            //                   <asp:ListItem Value="TradeUsed">Trade - Used List</asp:ListItem>
            //                   <asp:ListItem Value="TradeDeleted">Trade - Deleted List</asp:ListItem>
            //                   <asp:ListItem Value="Exchange">Exchange Items</asp:ListItem>
            if (vSalesreturn) {
                var checkedvalueReturn = $("#<%=rdoSalesReturn.ClientID%>").find(":checked").val();

                if (checkedvalueReturn == 'ALL') {
                    $("[id*=GroupByRetrun]").show();
                    $("[id*=GroupByRetrunType]").show();
                    $("[id*=GrpfilterSalesReturn]").show();
                }
                else if (checkedvalueReturn == 'CASH') {
                    $("[id*=GroupByRetrun]").show();
                    $("[id*=GroupByRetrunType]").show();
                    $("[id*=GrpfilterSalesReturn]").show();
                }
                else if (checkedvalueReturn == 'Exchange') {
                    $("[id*=GroupByRetrun]").show();
                    $("[id*=GroupByRetrunType]").show();
                    $("[id*=GrpfilterSalesReturn]").show();

                }
                else if (checkedvalueReturn == 'ExchangeItem') {
                    $("[id*=GrpfilterSalesReturn]").show();
                }
                else if (checkedvalueReturn == 'TradeUnUsed') {
                    $("[id*=GroupByRetrun]").show();
                    $("[id*=GroupByRetrunType]").hide();
                    $("[id*=GrpfilterSalesReturn]").hide();
                }
                else if (checkedvalueReturn == 'TradeUsed') {
                    $("[id*=GroupByRetrun]").show();
                    $("[id*=GroupByRetrunType]").hide();
                    $("[id*=GrpfilterSalesReturn]").hide();
                }
                else {
                    $("[id*=GroupByRetrun]").hide();
                    $("[id*=GroupByRetrunType]").hide();
                    $("[id*=GrpfilterSalesReturn]").hide();
                }

                if (checkedvalueReturn == 'ALL' || checkedvalueReturn == 'CASH' || checkedvalueReturn == 'Exchange') {
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divMemberSR').show();
                }
                if (checkedvalueReturn == 'ExchangeItem') {
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divMemberSR').hide();
                }
            }
            if (vSalesMore) {
                var checkedvalueMore = $("#<%=rdoSalesMore.ClientID%>").find(":checked").val();

                if (checkedvalueMore == 'Hold' || checkedvalueMore == 'Promotion') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_divTerminal').show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
                if (checkedvalueMore == 'Voucher' || checkedvalueMore == 'Denomination' || checkedvalueMore == 'CreditCard' || checkedvalueMore == 'Pending' || checkedvalueMore == 'Closed' || checkedvalueMore == 'Process' || checkedvalueMore == 'Cheque') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide(); //din
                    $('#ContentPlaceHolder1_divTerminal').show();
                    $('#ContentPlaceHolder1_divCustomerMore').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
                if (checkedvalueMore == 'Package' || checkedvalueMore == 'Transfer' || checkedvalueMore == 'Parent') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    if (checkedvalueMore == 'Transfer' || checkedvalueMore == 'Parent') { // dinesh 14082018
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                        $('#ContentPlaceHolder1_divTerminal').hide();
                        $('#ContentPlaceHolder1_divCustomerMore').hide();
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').show();
                    }
                    else {
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                        $('#ContentPlaceHolder1_divTerminal').show();
                        $('#ContentPlaceHolder1_divCustomerMore').show();
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').show();
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    }
                }
                if (checkedvalueMore == 'Cashier') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
                if (checkedvalueMore == 'Commission') { //sankar 
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_divSalesmanMore').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
                if (checkedvalueMore == 'Salesman') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
                if (checkedvalueMore == 'Floor') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
                if (checkedvalueMore == 'IMEI') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $("[id*=chkBydateMore]").attr("disabled", "disabled");
                    $("[id*=chkBydateMore]").attr("checked", false);
                    $("[id*=txtFromDateMore]").attr("disabled", "disabled");
                    $("[id*=txtToDateMore]").attr("disabled", "disabled");
                }
                else {

                    $("[id*=chkBydateMore]").removeAttr("disabled");
                    $("[id*=chkBydateMore]").attr("checked", true);
                    $("[id*=txtFromDateMore]").removeAttr("disabled");
                    $("[id*=txtToDateMore]").removeAttr("disabled");//
                }
                if (checkedvalueMore == 'Voucher') {
                    $("[id*=GroupBySD]").show();
                    $("[id*=GroupByDM]").hide();
                }
                else if (checkedvalueMore == 'Cashier' || checkedvalueMore == 'Salesman') {
                    $("[id*=GroupBySD]").hide();
                    $("[id*=GroupByDM]").show();
                }
                else if (checkedvalueMore == 'Floor' || checkedvalueMore == 'Helper' || checkedvalueMore == 'Commission') {
                    $("[id*=GroupBySD]").show();
                    $("[id*=GroupByDM]").show();
                }
                else {
                    $("[id*=GroupByDM]").hide();
                    $("[id*=GroupBySD]").hide();
                }
                if (checkedvalueMore == 'Helper') {
                    $("[id*=GrpfilterSalesMore]").hide();
                }
                else
                    $("[id*=GrpfilterSalesMore]").show();
                if (checkedvalueMore == 'Parent') {
                    $('#divPurchaseDate').show();
                    $('#divDate').hide();
                    $("[id*=chkBydateMore]").attr("disabled", "disabled");
                    $("[id*=chkBydateMore]").attr("checked", false);
                }
                else if (checkedvalueMore == 'BillRange') {
                    $('#divDate').show();
                    $("[id*=chkBydateMore]").attr("disabled", "disabled");
                    $("[id*=chkBydateMore]").attr("checked", true);
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
                else {
                    $('#divPurchaseDate').hide();
                    $('#divDate').show();
                    $("[id*=chkBydateMore]").removeAttr("disabled");
                    $("[id*=chkBydateMore]").attr("checked", true);
                }
            }

            if (vSalesAudit) {
                var checkedvalueAudit = $("#<%=rdoSalesAudit.ClientID%>").find(":checked").val();
                if (checkedvalueAudit == 'Cancel') {
                    //alert(checkedvalueAudit);
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Brand') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Department') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Category') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Break') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Pricechange') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Promotion') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Hold') {
                    $("[id*=GroupByAudit]").show();
                }
                else {
                    $("[id*=GroupByAudit]").hide();
                }
            }

            //$('.ziehharmonika').ziehharmonika({
            //    collapsible: false,
            //    arrow: true,
            //    scroll: true,
            //    prefix: '★',//'♫'//,
            //    collapseIcons: {
            //        opened: ' ',
            //        closed: ' '
            //    }
            //});

            //$('.ziehharmonika h3:eq(0)').ziehharmonika('open', true);

            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDatePre.ClientID %>"), $("#<%= txtToDatePre.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateMore.ClientID %>"), $("#<%= txtToDateMore.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateCredit.ClientID %>"), $("#<%= txtToDateCredit.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateReturn.ClientID %>"), $("#<%= txtToDateReturn.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateAudit.ClientID %>"), $("#<%= txtToDateAudit.ClientID %>"));

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($('#<%=hdnReportType.ClientID%>').val() == "C") { // dinesh
                $("[id$=SalesMoreTopHead]").text('Sales - MORE REPORTS');
                $("[id$=SalesReportTopHead]").text('Sales REPORTS');
                $("[id$=SalesReturnTopHead]").text('Sales RETURN REPORT');
            }
            else if ($('#<%=hdnReportType.ClientID%>').val() == "B") {
                $("[id$=SalesMoreTopHead]").text('Sales - more reports');
                $("[id$=SalesReportTopHead]").text('Sales reports');
                $("[id$=SalesReturnTopHead]").text('Sales return report');
            }
            else {
                $("[id$=SalesMoreTopHead]").text('Sales - More Reports');
                $("[id$=SalesReportTopHead]").text('Sales Reports');
                $("[id$=SalesReturnTopHead]").text('Sales Return Report');
            }

            $(document).ready(function () {
                $(document).on('keydown', disableFunctionKeys);
            });
            function disableFunctionKeys(e) {
                try {
                    if (e.ctrlKey && e.keyCode == 65) {
                        $('#<%=hdnReportType.ClientID%>').val('');
                        $("[id$=SalesMoreTopHead]").text('Sales - More Reports');
                        $("[id$=SalesReportTopHead]").text('Sales Reports');
                        $("[id$=SalesReturnTopHead]").text('Sales Return Report');
                        e.preventDefault();
                        return;
                    }
                    else if (e.ctrlKey && e.keyCode == 66) {
                        $('#<%=hdnReportType.ClientID%>').val('B');
                        $("[id$=SalesMoreTopHead]").text('Sales - more reports');
                        $("[id$=SalesReportTopHead]").text('Sales reports');
                        $("[id$=SalesReturnTopHead]").text('Sales return report');
                        e.preventDefault();
                        return;
                    }
                    else if (e.ctrlKey && e.keyCode == 67) {
                        $('#<%=hdnReportType.ClientID%>').val('C');
                        $("[id$=SalesMoreTopHead]").text('Sales - MORE REPORTS');
                        $("[id$=SalesReportTopHead]").text('Sales REPORTS');
                        $("[id$=SalesReturnTopHead]").text('Sales RETURN REPORT');
                        e.preventDefault();
                        return;
                    }
                    else if (e.keyCode == 112) {
                        e.preventDefault();
                    }
                }
                catch (err) {
                    ShowPopupMessageBox(err.message)
                }
            } //
        }

        function SetDisable(FromDate, Todate, sflag) {
            if (sflag == "D") {
                FromDate.attr("disabled", "disabled");
                Todate.attr("disabled", "disabled");
            }
            else {
                FromDate.removeAttr("disabled");
                Todate.removeAttr("disabled");
            }
        }

    <%--function SetAutoComplete() {

       ctrltxtInventory = $("#<%= ReportFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.ReportFilterUserControl.FilterControls.InventoryTextBox).ClientID %>");
    ctrltxtInventory.autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '<%=ResolveUrl("~/Reports/frmLocationWiseSales.aspx/GetInventoryDetails")%>',
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            valCode: item.split('|')[1]
                        }
                    }))
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        },
        select: function (e, i) {

            $("[id$=txtInventory]").val(i.item.valCode);
            $("#HidenInventoryName").val(i.item.label);

            //                    $("#HidenIsBatch").val($.trim(i.item.valBatch)); 

            //                    if ($("#HidenIsBatch").val() == 'True') {
            //                        fncGetBatchDetail_Master(i.item.valCode);
            //                    }
            //                    else {
            //                        AddRow($.trim(i.item.valCode), $.trim(i.item.valDescription), $.trim(i.item.valCost), 1, $.trim(i.item.valTax), '');
            //                    }

            return false;
        },
        minLength: 2
    });

}--%>

        function clearForm() {

            try {
                $('input[type="text"]').val('');
                $("[id$=txtInventory]").val('');
                $("select").val(0);
                $("select").trigger("liszt:updated");
            }
            catch (err) {

                alert(err.Message);
                return false;
            }

            return false;
        }
        $(document).ready(function () {

            $("input:radio").change(function () {
                var checkedvalue = $("#<%= rdoSalesReport.ClientID %> input:radio:checked").val();
                //alert(checkedvalue);
                //if ($.trim(checkedvalue) == 'InSummary' || $.trim(checkedvalue) == 'InDetail' || $.trim(checkedvalue) == 'Paymode' || $.trim(checkedvalue) == 'Session' || $.trim(checkedvalue) == 'Cashier' || $.trim(checkedvalue) == 'Hourly' || $.trim(checkedvalue) == 'Total' || $.trim(checkedvalue) == 'InPaymode') {//aalayam
                //    $("[id*=GrpfilterSales]").hide();
                //}
                //else {
                //    $("[id*=GrpfilterSales]").show();
                //}


                if ($("#<%=rdoDet.ClientID%>").is(':checked'))//aalayam 23042018
                {
                    $("[id*=rdoDate]").attr("disabled", "disabled");
                    $("[id*=rdoMonth]").attr("disabled", "disabled");
                    $("[id*=rdoNone]").attr("disabled", "disabled");
                }
                else {
                    $("[id*=rdoDate]").removeAttr("disabled");
                    $("[id*=rdoMonth]").removeAttr("disabled");
                    $("[id*=rdoNone]").removeAttr("disabled");//
                }
                if (checkedvalue == "Category")//aalayam 04052018

                    $("[id*=GroupByGst]").show();
                else
                    $("[id*=GroupByGst]").hide();
                if (checkedvalue == "InPaymodeView1")  //Vijay 20190211
                {
                    $("#ContentPlaceHolder1_chkBydate").attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }


                if (checkedvalue == "InSummary" || checkedvalue == "InDetail") { //aalayam 23042018
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $("[id*=GroupByDailySD]").hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }

                if (checkedvalue == "Item" || checkedvalue == "Shelf") {
                    debugger
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_divBatchNo').show();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    if (checkedvalue == "Shelf") {
                        $('#ContentPlaceHolder1_divBatchNo').hide();
                    }
                    $("[id*=GroupByDailySD]").show();

                }

                if (checkedvalue == "Department" || checkedvalue == "Category"
                    || checkedvalue == "Brand" || checkedvalue == "Vendor" || checkedvalue == "Class") {

                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();


                }
                if (checkedvalue == "Paymode" || checkedvalue == "InPaymode"
                    || checkedvalue == "Total" || checkedvalue == "Hourly" || checkedvalue == "Terminal" || checkedvalue == "Cashier") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    if (checkedvalue == "Paymode")
                        $('#ContentPlaceHolder1_PaymodeDetail').show();


                }
                if (checkedvalue == "Selling") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();


                }
                if (checkedvalue == "Batch" || checkedvalue == "NonBatch") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $('#ContentPlaceHolder1_divBatchNo').show();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    if (checkedvalue == "NonBatch") {
                        $('#ContentPlaceHolder1_divBatchNo').hide();
                    }
                    $("[id*=GroupByDailySD]").show();


                }//
                if (checkedvalue == "WeightBalance") {
                    $("#ContentPlaceHolder1_chkBydate").attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").hide();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    $("[id*=GrpfilterSales]").hide();
                }
                if (checkedvalue == "TerminalPay") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    if (checkedvalue == "WeightBalance")
                        $("[id*=GrpfilterSales]").hide();
                }//vijayv

                if (checkedvalue == "Session" || checkedvalue == "WeightBalance")
                    $("[id*=GrpfilterSales]").hide();
                else
                    $("[id*=GrpfilterSales]").show();


            });


            $("input:radio").change(function () { //aalayam 25042018
                var checkedvalueMore = $("#<%=rdoSalesMore.ClientID%>").find(":checked").val();

        if (checkedvalueMore == 'Helper')
            $("[id*=GrpfilterSalesMore]").hide();
        else
            $("[id*=GrpfilterSalesMore]").show();
    });//

            $("input:radio").change(function () { //aalayam 02052018

                var checkedvalueReturn = $("#<%=rdoSalesReturn.ClientID%>").find(":checked").val();
        if (checkedvalueReturn == 'ALL') {
            $("[id*=GroupByRetrun]").show();
            $("[id*=GroupByRetrunType]").show();
            $("[id*=GrpfilterSalesReturn]").show();
        }
        else if (checkedvalueReturn == 'CASH') {
            $("[id*=GroupByRetrun]").show();
            $("[id*=GroupByRetrunType]").show();
            $("[id*=GrpfilterSalesReturn]").show();
        }
        else if (checkedvalueReturn == 'Exchange') {
            $("[id*=GroupByRetrun]").show();
            $("[id*=GroupByRetrunType]").show();
            $("[id*=GrpfilterSalesReturn]").show();
        }
        else if (checkedvalueReturn == 'ExchangeItem') {
            $("[id*=GrpfilterSalesReturn]").show();
        }
        else if (checkedvalueReturn == 'TradeUnUsed') {
            $("[id*=GroupByRetrun]").show();
            $("[id*=GroupByRetrunType]").hide();
            $("[id*=GrpfilterSalesReturn]").hide();
        }
        else if (checkedvalueReturn == 'TradeUsed') {
            $("[id*=GroupByRetrun]").show();
            $("[id*=GroupByRetrunType]").hide();
            $("[id*=GrpfilterSalesReturn]").hide();
        }
        else {
            $("[id*=GroupByRetrun]").hide();
            $("[id*=GroupByRetrunType]").hide();
            $("[id*=GrpfilterSalesReturn]").hide();
        }
        if (checkedvalueReturn == 'ALL' || checkedvalueReturn == 'CASH' || checkedvalueReturn == 'Exchange') {
            $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divLocation').show();
            $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divCashier').show();
            $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divTerminal').show();
            $('#ContentPlaceHolder1_divMemberSR').show();
        }
        if (checkedvalueReturn == 'ExchangeItem') {
            $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divCashier').show();
            $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divTerminal').show();
            $('#ContentPlaceHolder1_divMemberSR').hide();
        }
    });

            $("input:radio").change(function () {//aalayam 02052018
                var checkedvaluecredit = $("#<%=rdoCredidue.ClientID%>").find(":checked").val();
        if (checkedvaluecredit == 'Credit') {
            $('#ContentPlaceHolder1_divMember').show();
            $('#ContentPlaceHolder1_divZone').hide();
            $('#ContentPlaceHolder1_divInvoiceNo').show();
            $('#ContentPlaceHolder1_divReceipt').hide();
        }
        if (checkedvaluecredit == 'Outstanding' || checkedvaluecredit == 'Limit' || checkedvaluecredit == 'Days' || checkedvaluecredit == 'Receipt' || checkedvaluecredit == 'Delete' || checkedvaluecredit == 'DueSales' || checkedvaluecredit == 'Pending' || checkedvaluecredit == 'Partialy' || checkedvaluecredit == 'Closed') {
            $('#ContentPlaceHolder1_divMember').show();
            $('#ContentPlaceHolder1_divZone').hide();
            $('#ContentPlaceHolder1_divInvoiceNo').hide();
            $('#ContentPlaceHolder1_divReceipt').hide();
        }
        if (checkedvaluecredit == 'DueReceipt') {
            $('#ContentPlaceHolder1_divMember').hide();
            $('#ContentPlaceHolder1_divZone').hide();
            $('#ContentPlaceHolder1_divInvoiceNo').show();
            $('#ContentPlaceHolder1_divReceipt').show();
        }
        if (checkedvaluecredit == 'Zone') {
            $('#ContentPlaceHolder1_divMember').show();
            $('#ContentPlaceHolder1_divZone').show();
            $('#ContentPlaceHolder1_divInvoiceNo').hide();
            $('#ContentPlaceHolder1_divReceipt').hide();
        }

        $('#ContentPlaceHolder1_txtMember').val('');
        //$('#ContentPlaceHolder1_ddlMember').trigger("liszt:updated");

        $('#ContentPlaceHolder1_txtZone').val('');
        //$('#ContentPlaceHolder1_ddlZone').trigger("liszt:updated");

        $('#ContentPlaceHolder1_txtInvoiceNo').val('');
        //$('#ContentPlaceHolder1_ddlInvoiceNo').trigger("liszt:updated");

        $('#ContentPlaceHolder1_txtReceiptNo').val('');
        //$('#ContentPlaceHolder1_ddlReceiptNo').trigger("liszt:updated");
        $('#ContentPlaceHolder1_txtBatchNo').val('');

        if (checkedvaluecredit == 'Limit' || checkedvaluecredit == 'Days') {
            $("[id*=chkBydateCredit]").attr("disabled", "disabled");
            $("[id*=txtFromDateCredit]").attr("disabled", "disabled");
            $("[id*=txtToDateCredit]").attr("disabled", "disabled");
        }
        else {
            $("[id*=chkBydateCredit]").removeAttr("disabled");
            $("[id*=txtFromeDateCredit]").removeAttr("disabled");
            $("[id*=txtToDateCredit]").removeAttr("disabled");//
        }
    });
        });

        $(function () {
            $('#<%= rdoSalesReport.ClientID %> input').click(function () {
                var checkedvalue = $("#<%= rdoSalesReport.ClientID %> input:radio:checked").val();
                //alert(checkedvalue);
                //if ($.trim(checkedvalue) == 'InSummary' || $.trim(checkedvalue) == 'InDetail' || $.trim(checkedvalue) == 'Paymode' || $.trim(checkedvalue) == 'Session' || $.trim(checkedvalue) == 'Cashier' || $.trim(checkedvalue) == 'Hourly' || $.trim(checkedvalue) == 'Total' || $.trim(checkedvalue) == 'InPaymode') {//aalayam
                //    $("[id*=GrpfilterSales]").hide();
                //}
                //else {
                //    $("[id*=GrpfilterSales]").show();
                //}
                if (checkedvalue == "BillRange") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divTerminalSales').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").hide();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }

                if (checkedvalue == "InPaymodeView1")  //Vijay 20190211
                {
                    $("#ContentPlaceHolder1_chkBydate").attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }
                if ($("#<%=rdoDet.ClientID%>").is(':checked'))//aalayam 23042018
                {
                    $("[id*=rdoDate]").attr("disabled", "disabled");
                    $("[id*=rdoMonth]").attr("disabled", "disabled");
                    $("[id*=rdoNone]").attr("disabled", "disabled");
                }
                else {
                    $("[id*=rdoDate]").removeAttr("disabled");
                    $("[id*=rdoMonth]").removeAttr("disabled");
                    $("[id*=rdoNone]").removeAttr("disabled");//
                }
                if (checkedvalue == "InSummary" || checkedvalue == "InDetail") //aalayam 2342018
                {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $("[id*=GroupByDailySD]").hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }

                if (checkedvalue == "Item" || checkedvalue == "Shelf") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    $('#ContentPlaceHolder1_divBatchNo').show();
                    if (checkedvalue == "Shelf") {
                        $('#ContentPlaceHolder1_divBatchNo').hide();
                    }
                    $("[id*=GroupByDailySD]").show();

                }
                if (checkedvalue == "Department" || checkedvalue == "Category"
                    || checkedvalue == "Brand" || checkedvalue == "Vendor" || checkedvalue == "Class") {

                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();

                }
                if (checkedvalue == "Paymode" || checkedvalue == "InPaymode" || checkedvalue == "InPaymodeView1"
                    || checkedvalue == "Total" || checkedvalue == "Hourly" || checkedvalue == "Terminal" || checkedvalue == "Cashier") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();

                }
                if (checkedvalue == "Selling") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();

                }
                if (checkedvalue == "Batch" || checkedvalue == "NonBatch") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    //
                }
                if (checkedvalue == "WeightBalance") {
                    $("#ContentPlaceHolder1_chkBydate").attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").hide();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    $("[id*=GrpfilterSales]").hide();
                }
                if (checkedvalue == "TerminalPay") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").show();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                    if (checkedvalue == "WeightBalance")
                        $("[id*=GrpfilterSales]").hide();
                }
                if (checkedvalue == "CashCounter") {
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminalSales').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divSalesTakeId').hide();
                    $('#ContentPlaceHolder1_ReportFilterUserControl_divOrgin').hide();
                    $("[id*=GroupByDailySD]").hide();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_PaymodeDetail').hide();
                }//vijayv

                if (checkedvalue == "Session" || checkedvalue == "WeightBalance")
                    $("[id*=GrpfilterSales]").hide();
                else
                    $("[id*=GrpfilterSales]").show();

                // dinesh
                $('#ContentPlaceHolder1_ReportFilterUserControl_txtInventory').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_txtInventory').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtDepartment').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlDept').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtCategory').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlCategory').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtBrand').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlBrand').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtVendor').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlVendor').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtCashier').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlCashier').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtTerminal').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlTerminal').trigger("liszt:updated");


                $('#ContentPlaceHolder1_txtTerminalSales').val(''); // dinesh
                //$('#ContentPlaceHolder1_ddTerminalSales').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtSubCategory').val('');//aalayam 23042018
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlSubCategory').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtClass').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlClass').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtSubClass').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlSubClass').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtFloor').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlFloor').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtManufacture').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlManufacture').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_ddlSalesTakeId').val('');
                $('#ContentPlaceHolder1_ReportFilterUserControl_ddlSalesTakeId').trigger("liszt:updated");


                $('#ContentPlaceHolder1_ReportFilterUserControl_txtOrgin').val('');
                //$('#ContentPlaceHolder1_ReportFilterUserControl_ddlOrgin').trigger("liszt:updated");
                $('#ContentPlaceHolder1_txtBatchNo').val('');

                $('#ContentPlaceHolder1_chkGst').removeAttr("checked");

                if (checkedvalue == "Category")//aalayam 04052018

                    $("[id*=GroupByGst]").show();
                else
                    $("[id*=GroupByGst]").hide();
                //
            });

            $('#<%= rdoCredidue.ClientID %> input').click(function () {
                var checkvalue = $("#<%= rdoCredidue.ClientID %> input:radio:checked").val();

        if (checkvalue == 'Credit') {
            $("[id*=GroupByCR]").show();
            $("[id*=GroupByCRDue]").show();
        }
        else if (checkvalue == 'Outstanding') {
            $("[id*=GroupByCR]").show();
            $("[id*=GroupByCRDue]").hide();
        }
        else if (checkvalue == 'Receipt') {
            $("[id*=GroupByCR]").show();
            $("[id*=GroupByCRDue]").hide();
        }
        else {
            $("[id*=GroupByCR]").hide();
            $("[id*=GroupByCRDue]").hide();
        }

        if (checkvalue == 'Credit')//aalayam 02052018
        {
            $('#ContentPlaceHolder1_divMember').show();
            $('#ContentPlaceHolder1_divZone').hide();
            $('#ContentPlaceHolder1_divInvoiceNo').show();
            $('#ContentPlaceHolder1_divReceipt').hide();
        }
        if (checkvalue == 'Outstanding' || checkedvalueCredit == 'Limit' || checkedvalueCredit == 'Days' || checkedvalueCredit == 'Receipt' || checkedvalueCredit == 'Delete' || checkedvalueCredit == 'DueSales' || checkedvalueCredit == 'Pending' || checkedvalueCredit == 'Partialy' || checkedvalueCredit == 'Closed') {
            $('#ContentPlaceHolder1_divMember').show();
            $('#ContentPlaceHolder1_divZone').hide();
            $('#ContentPlaceHolder1_divInvoiceNo').hide();
            $('#ContentPlaceHolder1_divReceipt').hide();
        }
        if (checkvalue == 'DueReceipt') {
            $('#ContentPlaceHolder1_divMember').hide();
            $('#ContentPlaceHolder1_divZone').hide();
            $('#ContentPlaceHolder1_divInvoiceNo').show();
            $('#ContentPlaceHolder1_divReceipt').show();
        }
        if (checkvalue == 'Zone') {
            $('#ContentPlaceHolder1_divMember').show();
            $('#ContentPlaceHolder1_divZone').show();
            $('#ContentPlaceHolder1_divInvoiceNo').hide();
            $('#ContentPlaceHolder1_divReceipt').hide();
        }

        $('#ContentPlaceHolder1_txtMember').val('');
        //$('#ContentPlaceHolder1_ddlMember').trigger("liszt:updated");
        $('#ContentPlaceHolder1_txtBatchNo').val('');
        $('#ContentPlaceHolder1_txtZone').val('');
        //$('#ContentPlaceHolder1_ddlZone').trigger("liszt:updated");

        $('#ContentPlaceHolder1_txtInvoiceNo').val('');
        //$('#ContentPlaceHolder1_ddlInvoiceNo').trigger("liszt:updated");

        $('#ContentPlaceHolder1_txtReceiptNo').val('');
        //$('#ContentPlaceHolder1_ddlReceiptNo').trigger("liszt:updated");

        if (checkvalue == 'Limit' || checkvalue == 'Days')//aalayam 02052018
        {
            $("[id*=chkBydateCredit]").attr("disabled", "disabled");
            $("[id*=txtFromDateCredit]").attr("disabled", "disabled");
            $("[id*=txtToDateCredit]").attr("disabled", "disabled");
        }
        else {
            $("[id*=chkBydateCredit]").removeAttr("disabled");
            $("[id*=txtFromeDateCredit]").removeAttr("disabled");
            $("[id*=txtToDateCredit]").removeAttr("disabled");//
        }

    });

            $('#<%= rdoSalesReturn.ClientID %> input').click(function () {
                var checkedvalueReturn = $("#<%= rdoSalesReturn.ClientID %> input:radio:checked").val();

                if (checkedvalueReturn == 'ALL') {
                    $("[id*=GroupByRetrun]").show();
                    $("[id*=GroupByRetrunType]").show();
                    $("[id*=GrpfilterSalesReturn]").show();
                }
                else if (checkedvalueReturn == 'CASH') {
                    $("[id*=GroupByRetrun]").show();
                    $("[id*=GroupByRetrunType]").show();
                    $("[id*=GrpfilterSalesReturn]").show();
                }
                else if (checkedvalueReturn == 'Exchange') {
                    $("[id*=GroupByRetrun]").show();
                    $("[id*=GroupByRetrunType]").show();
                    $("[id*=GrpfilterSalesReturn]").show();
                }
                else if (checkedvalueReturn == 'ExchangeItem') {
                    $("[id*=GrpfilterSalesReturn]").show();
                }
                else if (checkedvalueReturn == 'TradeUnUsed') {
                    $("[id*=GroupByRetrun]").show();
                    $("[id*=GroupByRetrunType]").hide();
                    $("[id*=GrpfilterSalesReturn]").hide();
                }
                else if (checkedvalueReturn == 'TradeUsed') {
                    $("[id*=GroupByRetrun]").show();
                    $("[id*=GroupByRetrunType]").hide();
                    $("[id*=GrpfilterSalesReturn]").hide();
                }
                else {
                    $("[id*=GroupByRetrun]").hide();
                    $("[id*=GroupByRetrunType]").hide();
                    $("[id*=GrpfilterSalesReturn]").hide();
                }
                if (checkedvalueReturn == 'ALL' || checkedvalueReturn == 'CASH' || checkedvalueReturn == 'Exchange') {
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divMemberSR').show();
                }
                if (checkedvalueReturn == 'ExchangeItem') {
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divMemberSR').hide();
                }
                $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_txtLocation').val('');
                //$('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_ddlLocation').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_txtCashier').val('');
                //$('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_ddlCashier').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_txtTerminal').val('');
                //$('#ContentPlaceHolder1_SalesReturnReportFilterUserControl_ddlTerminal').trigger("liszt:updated");

                $('#ContentPlaceHolder1_txtMemberSR').val('');
                //$('#ContentPlaceHolder1_ddlMemberSR').trigger("liszt:updated");
                $('#ContentPlaceHolder1_txtBatchNo').val('');



            });


    $('#<%= rdoSalesMore.ClientID %> input').click(function () {
        var checkvalue = $("#<%= rdoSalesMore.ClientID %> input:radio:checked").val();//aalayam 25042018
        
            if (checkvalue == 'Helper')
                    $("[id*=GrpfilterSalesMore]").hide();
                else
                    $("[id*=GrpfilterSalesMore]").show();
                if (checkvalue == 'Voucher' || checkvalue == 'Denomination' || checkvalue == 'CreditCard'
                    || checkvalue == 'Pending' || checkvalue == 'Closed' || checkvalue == 'Process' || checkvalue == 'Cheque') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminal').show();
                    $('#ContentPlaceHolder1_divCustomerMore').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
        if (checkvalue == 'Package' || checkvalue == 'Transfer' || checkvalue == 'Parent') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
            if (checkvalue == 'Transfer' || checkvalue == 'Parent') { // dinesh 14082018
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                        $('#ContentPlaceHolder1_divTerminal').hide();
                        $('#ContentPlaceHolder1_divCustomerMore').hide();
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').show();
                    }
                    else {
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                        $('#ContentPlaceHolder1_divTerminal').show();
                        $('#ContentPlaceHolder1_divCustomerMore').show();
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').show();
                        $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();

                    }
                }
                if (checkvalue == 'Cashier') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
                if (checkvalue == 'Salesman') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
                if (checkvalue == 'Commission') {  //SANKAR
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                }
                if (checkvalue == 'Floor') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
        }
        if (checkvalue == 'BillRange') {
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').show();
            $('#ContentPlaceHolder1_divTerminal').hide();
            $('#ContentPlaceHolder1_divCustomerMore').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
            $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
            $('#ContentPlaceHolder1_divSalesmanMore').hide();
            $('#ContentPlaceHolder1_divBatchNo').hide();
            $('#divPurchaseDate').hide();
        }
                if (checkvalue == 'IMEI') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $("[id*=chkBydateMore]").attr("disabled", "disabled");
                    $("[id*=chkBydateMore]").attr("checked", false);
                    $("[id*=txtFromDateMore]").attr("disabled", "disabled");
                    $("[id*=txtToDateMore]").attr("disabled", "disabled");
                }
                else {

                    $("[id*=chkBydateMore]").removeAttr("disabled");
                    $("[id*=chkBydateMore]").attr("checked", true);
                    $("[id*=txtFromDateMore]").removeAttr("disabled");
                    $("[id*=txtToDateMore]").removeAttr("disabled");//
                }
                if (checkvalue == 'Hold' || checkvalue == 'Promotion') {
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_divSalesmanMore').hide();
                    $('#ContentPlaceHolder1_divCustomerMore').hide();
                    $('#ContentPlaceHolder1_divTerminal').show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
        }
        if (checkvalue == 'Parent' ) {
            $('#divPurchaseDate').show();
            $('#divDate').hide();
            $("[id*=chkBydateMore]").attr("disabled", "disabled");
            $("[id*=chkBydateMore]").attr("checked", false);
        }
        else if (checkvalue == 'BillRange') {
            $("[id*=chkBydateMore]").attr("disabled", "disabled");
            $("[id*=chkBydateMore]").attr("checked", true);
            $('#divDate').show();
        }
        else {
            $('#divPurchaseDate').hide();
            $('#divDate').show();
            $("[id*=chkBydateMore]").removeAttr("disabled");
            $("[id*=chkBydateMore]").attr("checked", true);
        }
                $('#ContentPlaceHolder1_txtBatchNo').val('');
                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtLocation').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlLocation').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtDepartment').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlDept').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtCategory').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlCategory').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtBrand').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlBrand').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtVendor').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlVendor').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtCashier').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlCashier').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtTerminal').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlTerminal').trigger("liszt:updated");


                $('#ContentPlaceHolder1_txtTerminal').val(''); // dinesh
                //$('#ContentPlaceHolder1_ddTerminal').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtClass').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlClass').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtSubClass').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlSubClass').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtFloor').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlFloor').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_txtSubCategory').val('');
                //$('#ContentPlaceHolder1_SalesMoreReportFilterUserControl_ddlSubCategory').trigger("liszt:updated");

        $('#ContentPlaceHolder1_txtCustomerMore').val('');

                $('#ContentPlaceHolder1_txtSalesmanMore').val('');

                //
                if (checkvalue == 'Voucher' ) {
                    $("[id*=GroupBySD]").show();
                    $("[id*=GroupByDM]").hide();
                }
                else if (checkvalue == 'Cashier' || checkvalue == 'Salesman') {
                    $("[id*=GroupBySD]").hide();
                    $("[id*=GroupByDM]").show();
                }
                else if (checkvalue == 'Floor' || checkvalue == 'Helper' || checkvalue == 'Commission') {
                    $("[id*=GroupBySD]").show();
                    $("[id*=GroupByDM]").show();
                }
                else {
                    $("[id*=GroupByDM]").hide();
                    $("[id*=GroupBySD]").hide();
                }
            });
    $('#<%= rdoPreSales.ClientID %> input').click(function () { //aalayam 25042018
        var chekedValuePreSales = $("#<%=rdoPreSales.ClientID%> input:radio:checked").val();
                if (chekedValuePreSales == 'Cancel' || chekedValuePreSales == 'SCancel' || chekedValuePreSales == 'MCancel')
                    $("[id*=GrpfilterPreSales]").hide();
                else
                    $("[id*=GrpfilterPreSales]").show();
        if (chekedValuePreSales.indexOf('M') != -1) {
            $('#ContentPlaceHolder1_PreReportFilterUserControl_divTerminal').hide();
            $('#ContentPlaceHolder1_PreReportFilterUserControl_divCashier').hide();
        }
        else {
            $('#ContentPlaceHolder1_PreReportFilterUserControl_divTerminal').show();
            $('#ContentPlaceHolder1_PreReportFilterUserControl_divCashier').show();
        }
                $('#ContentPlaceHolder1_PreReportFilterUserControl_txtLocation').val('');
                //$('#ContentPlaceHolder1_PreReportFilterUserControl_ddlLocation').trigger("liszt:updated");
                //$('#ContentPlaceHolder1_PreReportFilterUserControl_ddlLocation').val('');

                $('#ContentPlaceHolder1_PreReportFilterUserControl_txtCashier').val('');
                //$('#ContentPlaceHolder1_PreReportFilterUserControl_ddlCashier').trigger("liszt:updated");
                //$('#ContentPlaceHolder1_PreReportFilterUserControl_ddlCashier').val('');

                $('#ContentPlaceHolder1_PreReportFilterUserControl_txtTerminal').val('');
                //$('#ContentPlaceHolder1_PreReportFilterUserControl_ddlTerminal').trigger("liszt:updated");
                //$('#ContentPlaceHolder1_PreReportFilterUserControl_ddlTerminal').val('');

                $('#ContentPlaceHolder1_txtCustomer').val('');
        $('#<%=hidCustCode.ClientID %>').val('');
            });//
    $('#<%= rdoSalesAudit.ClientID %> input').click(function () {
        var checkedvalueAudit = $("#<%= rdoSalesAudit.ClientID %> input:radio:checked").val();
                if (checkedvalueAudit == 'Cancel') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Brand') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Department') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Category') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Break') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Pricechange') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Promotion') {
                    $("[id*=GroupByAudit]").show();
                }
                else if (checkedvalueAudit == 'Hold') {
                    $("[id*=GroupByAudit]").show();
                }
                else {

                    $("[id*=GroupByAudit]").hide();
                }
            });

});


        $(function () {
            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDatePre.ClientID %>"), $("#<%= txtToDatePre.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateMore.ClientID %>"), $("#<%= txtToDateMore.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateCredit.ClientID %>"), $("#<%= txtToDateCredit.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateReturn.ClientID %>"), $("#<%= txtToDateReturn.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateAudit.ClientID %>"), $("#<%= txtToDateAudit.ClientID %>"));
            SetDefaultDate($("#<%= txtPurchaseFromDate.ClientID %>"), $("#<%= txtPurchaseToDate.ClientID %>"));
            SetDefaultDate($("#<%= txtSalFromDate.ClientID %>"), $("#<%= txtSalToDate.ClientID %>"));
        });


        function SetDefaultDate(FromDate, Todate) {

            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        }


        $(function () {

            try {


                $('#<%=chkBydate.ClientID%>').change(function () {
                    if (this.checked) {
                        SetDisable($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"), 'E')
                    }
                    else {
                        SetDisable($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"), 'D')
                    }
                });

                $('#<%=chkBydateMore.ClientID%>').change(function () {
                    if (this.checked) {
                        SetDisable($("#<%= txtFromDateMore.ClientID %>"), $("#<%= txtToDateMore.ClientID %>"), 'E')
                    }
                    else {
                        SetDisable($("#<%= txtFromDateMore.ClientID %>"), $("#<%= txtToDateMore.ClientID %>"), 'D')
                    }
                });

                $('#<%=chkBydateCredit.ClientID%>').change(function () {
                    if (this.checked) {
                        SetDisable($("#<%= txtFromDateCredit.ClientID %>"), $("#<%= txtToDateCredit.ClientID %>"), 'E')
                    }
                    else {
                        SetDisable($("#<%= txtFromDateCredit.ClientID %>"), $("#<%= txtToDateCredit.ClientID %>"), 'D')
                    }
                });

                $('#<%=chkByDateReturn.ClientID%>').change(function () {
                    if (this.checked) {
                        SetDisable($("#<%= txtFromDateReturn.ClientID %>"), $("#<%= txtToDateReturn.ClientID %>"), 'E')
                    }
                    else {
                        SetDisable($("#<%= txtFromDateReturn.ClientID %>"), $("#<%= txtToDateReturn.ClientID %>"), 'D')
                    }
                });

                $('#<%=chkBydatePre.ClientID%>').change(function () {
                    if (this.checked) {
                        SetDisable($("#<%= txtFromDatePre.ClientID %>"), $("#<%= txtToDatePre.ClientID %>"), 'E')
                    }
                    else {
                        SetDisable($("#<%= txtFromDatePre.ClientID %>"), $("#<%= txtToDatePre.ClientID %>"), 'D')
                    }
                });
                $('#<%=chkBydateSA.ClientID%>').change(function () {
                    if (this.checked) {
                        SetDisable($("#<%= txtFromDateAudit.ClientID %>"), $("#<%= txtToDateAudit.ClientID %>"), 'E')
                    }
                    else {
                        SetDisable($("#<%= txtFromDateAudit.ClientID %>"), $("#<%= txtToDateAudit.ClientID %>"), 'D')
                    }
                });

            }
            catch (err) {
                alert(err.Message);
            }
        });



        function ValidateForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }

                if (Show != '') {
                    alert(Show);
                    return false;
                }

                else {
                    DateCheck();
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncSetValue() {
            try {
                if (SearchTableName == "Salesman") {
                    $('#<%=txtSalesmanMore.ClientID %>').val($.trim(Description));
                }
                if (SearchTableName == "CUSTOMER") {
                    if (ControlID == "txtCustomer") {
                        $('#ContentPlaceHolder1_txtCustomer').val($.trim(Description));
                        $('#<%=hidCustCode.ClientID %>').val($.trim(Code)); 
                    }
                    if (ControlID == "txtCustomerMore") {
                        $('#ContentPlaceHolder1_txtCustomerMore').val($.trim(Description));
                    }
                    //$('#ContentPlaceHolder1_txtCustomer').val($.trim(Description));
                    //$('#ContentPlaceHolder1_txtCustomerMore').val($.trim(Description));
                }
                if (SearchTableName == "Member") {
                    $('#<%=txtMember.ClientID %>').val($.trim(Description));
                    $('#<%=HiddenCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtMemberSR.ClientID %>').val($.trim(Description));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>

    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }


        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales Reports</a><i class="fa fa-angle-right"></i></li>
                <li id="ReportTitle" runat="server" class="active-page">Sales Reports </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="ziehharmonika EnableScroll">
            <div style="overflow: visible; display: block;" runat="server" id="DivSalesReport">
                <h3 id="SalesReportTopHead">Sales Reports</h3>
                <div class="row">
                    <div class="col-md-3" style="margin-left: 20px">
                        <div>
                            <asp:RadioButtonList ID="rdoSalesReport" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Department">Department wise Sales</asp:ListItem>
                                <asp:ListItem Value="Category">Category wise Sales</asp:ListItem>
                                <asp:ListItem Value="Brand">Brand wise Sales</asp:ListItem>
                                <asp:ListItem Value="Class">Class wise Sales</asp:ListItem>
                                <asp:ListItem Value="Shelf">Shelf wise Sales</asp:ListItem>
                                <asp:ListItem Value="Vendor">Vendor wise Sales</asp:ListItem>
                                <asp:ListItem Value="Item">Item Wise Sales</asp:ListItem>
                                <asp:ListItem Value="InSummary">Invoice Wise Sales Summary</asp:ListItem>
                                <asp:ListItem Value="InDetail">Invoice Wise Sales Detail</asp:ListItem>
                                <asp:ListItem Value="Paymode">Paymode wise Sales</asp:ListItem>
                                <asp:ListItem Value="InPaymode">Invoice wise Paymode wise Sales</asp:ListItem>
                                <asp:ListItem Value="Total">Total Sales</asp:ListItem>
                                <asp:ListItem Value="Hourly">Hourly Sales</asp:ListItem>
                                <asp:ListItem Value="Session">Session Report On</asp:ListItem>
                                <asp:ListItem Value="Terminal">Terminal Wise Sales Detail</asp:ListItem>
                                <asp:ListItem Value="Cashier">Cashier and Paymode wise Sales</asp:ListItem>
                                <asp:ListItem Value="Selling">Selling Price Wise NonBatch Sales</asp:ListItem>
                                <asp:ListItem Value="Batch">Batch Wise Sales</asp:ListItem>
                                <asp:ListItem Value="NonBatch">Non Batch Wise Sales</asp:ListItem>
                                <asp:ListItem Value="TerminalPay">Terminal and Paymode wise Sales</asp:ListItem>
                                <asp:ListItem Value="CashCounter">Cash Counter and Paymode Sales</asp:ListItem>
                                <asp:ListItem Value="InPaymodeView1">Invoice wise Paymode wise Sales View1</asp:ListItem>
                                <asp:ListItem Value="WeightBalance">Weight Scale Balance Qty</asp:ListItem>
                                
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="GroupBy" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="rdoDate" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="DATE"
                                    Checked="true" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="rdoMonth" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="MONTH" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="rdoNone" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="NONE" />
                            </div>
                        </div>
                        <div id="GroupByDailySD" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoSum" Class="radioboxlist" runat="server" GroupName="RegularMenurdo" Text="SUMMARY"
                                    Checked="true" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoDet" Class="radioboxlist" runat="server" GroupName="RegularMenurdo" Text="DETAIL" />
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-5">
                                <%--<asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                            </div>

                            <asp:UpdatePanel ID="updpnlRefresh" runat="server" UpdateMode="Conditional">
                                <%--aalayam--%>
                                <ContentTemplate>
                                    <div class="col-md-2">
                                        <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadReport_Click">Load Report </asp:LinkButton>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>


                        </div>
                        <div id="GroupByGst" class="col-md-6 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:CheckBox ID="chkGst" Class="radioboxlistgreen" runat="server" Text="GST" />
                            </div>
                        </div>
                    </div>

                    <div id="GrpfilterSales" class="col-md-4 form_bootstyle_border">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="ReportFilterUserControl"
                                EnableVendorDropDown="true"
                                EnableCashierDropDown="true"
                                EnableDepartmentDropDown="true"
                                EnableCategoryDropDown="true"
                                EnableTerminalDropDown="false"
                                EnableClassDropDown="true"
                                EnableInventoryTextBox="true"
                                EnableFloorDropDown="true"
                                EnableBrandDropDown="true"
                                EnableOrginDropDown="true"
                                EnableManufactureDropDown="true"
                                EnableSubCategoryDropDown="true"
                                EnableSubClassDropDown="true"
                                EnablelocationDropDown="true" />
                            <div class="col-md-12 form_bootstyle" runat="server" id="divBatchNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblBatchNo" runat="server" Text="Batch No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtBatchNo" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'BatchNo',  'txtBatchNo', '');" MaxLength="15" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divTerminalSales">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblTerminalSales" runat="server" Text="Terminal"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddTerminalSales" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtTerminalSales" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Terminal', 'txtTerminalSales', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="PaymodeDetail">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label13" runat="server" Text="Paymode Type"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtPaymodeDetail" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'PaymodeDetail', 'txtPaymodeDetail', '');"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

          <div style="overflow: visible; display: block;" runat="server" id="DivSalesMore">
                <h3 id="SalesMoreTopHead">Sales - More Reports</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div>
                            <asp:RadioButtonList ID="rdoSalesMore" runat="server" Class="radioboxlist">
                          <%--      <asp:ListItem Selected="True" Value="Voucher">Voucher Sales</asp:ListItem>
                                <asp:ListItem Value="Denomination">Voucher Sales Denomination</asp:ListItem>--%>
                                <asp:ListItem Value="CreditCard">CreditCard Sales</asp:ListItem>
                                <asp:ListItem Value="Pending">Home Delivery Sales - Pending</asp:ListItem>
                                <asp:ListItem Value="Closed">Home Delivery Sales - Closed</asp:ListItem>
                                <asp:ListItem Value="Process">Home Delivery Process Detail</asp:ListItem>
                                <asp:ListItem Value="Package">Package Inventory Sales</asp:ListItem>
                                <asp:ListItem Value="Cashier">Cashier wise Sales</asp:ListItem>
                                <asp:ListItem Value="Salesman">Salesman wise Sales</asp:ListItem>
                                <asp:ListItem Value="Commission">SalesMan- Commission</asp:ListItem>
                                <asp:ListItem Value="Floor">Floor Wise Sales</asp:ListItem>
                                <asp:ListItem Value="Cheque">Cheque Sales</asp:ListItem>
                               <%-- <asp:ListItem Value="IMEI">IMEI Sales</asp:ListItem>--%>
                                <%--<asp:ListItem Value="Helper">Helper Wise Sales</asp:ListItem>--%>
                                <asp:ListItem Value="Hold">Sales Hold Bill Count</asp:ListItem>
                                <asp:ListItem Value="Promotion">Sales Promotion Applied ItemList</asp:ListItem>
                                <asp:ListItem Value="Transfer">TransferIn and Sold Qty</asp:ListItem>
                                <asp:ListItem Value="Parent">Parent Child Sales</asp:ListItem>
                                <asp:ListItem Value="BillRange">Bill Range</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div id="GrpfilterSalesMore" class="col-md-4 form_bootstyle_border">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="SalesMoreReportFilterUserControl"
                                EnableInventoryTextBox="true"
                                EnableTerminalDropDown="true"
                                EnableCashierDropDown="true"
                                EnableDepartmentDropDown="true"
                                EnableCategoryDropDown="true"
                                EnableSubCategoryDropDown="true"
                                EnableBrandDropDown="true"
                                EnableVendorDropDown="true"
                                EnableClassDropDown="true"
                                EnableSubClassDropDown="true"
                                EnablelocationDropDown="true"
                                EnableFloorDropDown="true" />
                            <div class="col-md-12 form_bootstyle" runat="server" id="divTerminal">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblTerminal" runat="server" Text="Terminal"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddTerminal" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtTerminal" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Terminal', 'txtTerminal', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divCustomerMore">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblCustomer" runat="server" Text="Customer"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlCustomerMore" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtCustomerMore" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'CUSTOMER', 'txtCustomerMore', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divSalesmanMore">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblSalesMan" runat="server" Text="Salesman"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlSalesmanMore" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtSalesmanMore" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Salesman', 'txtSalesmanMore', '');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border" style="margin-left: 45px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua" id="divDate">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label7" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDateMore" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label8" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDateMore" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="small-box bg-aqua" id="divPurchaseDate">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label14" runat="server" Text="Pur.FromDate"></asp:Label>
                                        <asp:TextBox ID="txtPurchaseFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label20" runat="server" Text="Pur.ToDate"></asp:Label>
                                        <asp:TextBox ID="txtPurchaseToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label21" runat="server" Text="Sal.FromDate"></asp:Label>
                                        <asp:TextBox ID="txtSalFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label23" runat="server" Text="Sal.ToDate"></asp:Label>
                                        <asp:TextBox ID="txtSalToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="GroupBySD" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoSumm" Class="radioboxlist" runat="server" GroupName="RegularMenu1" Text="SUMMARY"
                                    Checked="true" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoDetail" Class="radioboxlist" runat="server" GroupName="RegularMenu1" Text="DETAIL" />
                            </div>
                        </div>
                        <div id="GroupByDM" runat="server" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none">
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="rdoDateM" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="DATE"
                                    Checked="true" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="rdoMonthM" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="MONTH" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="rdoNoneM" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="NONE" />
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkBydateMore" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-5">
                                <%--  <asp:LinkButton ID="LinkButton4" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="lnkSalesMore" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkSalesMore_Click">Load Report </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="overflow: visible; display: block;" runat="server" id="DivPreSales">
                <h3>Pre Sales Report</h3>
                <div class="row">
                    <div class="col-md-3" style="margin-left: 20px">
                        <div>
                            <asp:RadioButtonList ID="rdoPreSales" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Pending">Sales Estimation- Pending</asp:ListItem>
                                <asp:ListItem Value="Closed">Sales Estimation- Closed</asp:ListItem>
                                <asp:ListItem Value="Cancel">Sales Estimation- Cancel</asp:ListItem>
                                <asp:ListItem Value="SPending">Sales Order - Pending</asp:ListItem>
                                <asp:ListItem Value="SClosed">Sales Order - Closed</asp:ListItem>
                                <asp:ListItem Value="SCancel">Sales Order - Cancel</asp:ListItem>
                                <asp:ListItem Value="MPending">Mobile Order - Pending</asp:ListItem>
                                <asp:ListItem Value="MClosed">Mobile Order - Closed</asp:ListItem>
                                <asp:ListItem Value="MCancel">Mobile Order - Cancel</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div id="GrpfilterPreSales" class="col-md-4 form_bootstyle_border" style="margin-left: 20px">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="PreReportFilterUserControl"
                                EnablelocationDropDown="false"
                                EnableCashierDropDown="false"
                                EnableTerminalDropDown="false" />
                            <div class="col-md-12 form_bootstyle" runat="server" id="div1">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label15" runat="server" Text="Location"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlCustomer" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-12 form_bootstyle" runat="server" id="div2">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label24" runat="server" Text="Cashier"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlCustomer" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtCashier" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Cashier', 'txtCashier', '');"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-12 form_bootstyle" runat="server" id="div3">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label25" runat="server" Text="Terminal"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlCustomer" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtTerminal1" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Terminal', 'txtTerminal1', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divCustomer">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label22" runat="server" Text="Customer"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlCustomer" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtCustomer" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'CUSTOMER', 'txtCustomer', '');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border" style="margin-left: 40px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label1" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDatePre" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label3" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDatePre" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="GroupByPS" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoPreSumm" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="SUMMARY"
                                    Checked="true" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoPreDet" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="DETAIL" />
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkBydatePre" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-5">
                                <%--  <asp:LinkButton ID="lnkPreClear" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="lnkPreSales" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkPreSales_Click">Load Report </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="overflow: visible; display: block;" runat="server" id="DivCreditDue">
                <h3>Credit & Due Sales Report</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div>
                            <asp:RadioButtonList ID="rdoCredidue" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Credit">Credit Sales Report</asp:ListItem>
                                <asp:ListItem Value="Outstanding">Customer Outstandings</asp:ListItem>
                                <asp:ListItem Value="Limit">Credit Limit Exceed</asp:ListItem>
                                <asp:ListItem Value="Days">Credit Days Exceed</asp:ListItem>
                                <asp:ListItem Value="Receipt">Customer- Receipt</asp:ListItem>
                                 <%--<asp:ListItem Value="Delete">Customer Receipt Delete</asp:ListItem>--%>
                                <asp:ListItem Value="DueSales">Customer wise Due Sales</asp:ListItem>
                                <asp:ListItem Value="Pending">Due Sales Pending</asp:ListItem>
                                <asp:ListItem Value="Partialy">Due Sales Partialy Pending</asp:ListItem>
                                <asp:ListItem Value="Closed">Due Sales Closed</asp:ListItem>
                                <asp:ListItem Value="DueReceipt">Due Receipt</asp:ListItem>
                                <asp:ListItem Value="Zone">Credit Sales Zone</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div id="GrpfilterCreditDueSales" class="col-md-4 form_bootstyle_border">
                        <%--aalayam 26042018 --%>
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">

                            <div class="col-md-12 form_bootstyle" runat="server" id="divMember">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label17" runat="server" Text="Member"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlMember" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtMember" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Member', 'txtMember', '');"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-12 form_bootstyle" runat="server" id="divZone">
                                <div class="col-md-4 form_bootstyle">
                                    <%--<asp:Label ID="Label15" runat="server" Text="Zone"></asp:Label>--%>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlZone" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <%--<asp:TextBox ID="txtZone" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Zone', 'txtZone', '');"></asp:TextBox>--%>
                                </div>
                            </div>

                            <div class="col-md-12 form_bootstyle" runat="server" id="divInvoiceNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label16" runat="server" Text="Invoice No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlInvoiceNo" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'InvoiceNo', 'txtInvoiceNo', '');"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-12 form_bootstyle" runat="server" id="divReceipt">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label18" runat="server" Text="Receipt No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlReceiptNo" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtReceiptNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'ReceiptNo', 'txtReceiptNo', '');"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border" style="margin-left: 85px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label2" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDateCredit" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label6" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDateCredit" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="GroupByCR" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoCRdueSumm" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="SUMMARY"
                                    Checked="true" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoCRduedet" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="DETAIL" />
                            </div>
                        </div>

                        <div id="GroupByCRDue" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none;">
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkPaid" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="PAID"
                                    Checked="true" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkUnPaid" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="UN PAID" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkAll" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="ALL" />
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkBydateCredit" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-5">
                                <%--<asp:LinkButton ID="LinkButton2" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="lnkCreditDue" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkCreditDue_Click">Load Report </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="overflow: visible; display: block;" runat="server" id="DivSalesReturn">
                <h3 id="SalesReturnTopHead">Sales Return Report</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div>
                            <asp:RadioButtonList ID="rdoSalesReturn" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="ALL">Sales Refund-ALL</asp:ListItem>
                                <asp:ListItem Value="CASH">Sales Refund-CASH</asp:ListItem>
                                <asp:ListItem Value="Exchange">Sales Refund-Exchange Voucher</asp:ListItem>
                                <asp:ListItem Value="UnUsed">Exchange Voucher - UnUsed</asp:ListItem>
                                <asp:ListItem Value="Used">Exchange Voucher - Used</asp:ListItem>
                                <asp:ListItem Value="Expired">Exchange Voucher - Expired</asp:ListItem>
                                <asp:ListItem Value="TradeUnUsed">Trade - UnUsed List</asp:ListItem>
                                <asp:ListItem Value="TradeUsed">Trade - Used List</asp:ListItem>
                                <%--<asp:ListItem Value="TradeDeleted">Trade - Deleted List</asp:ListItem>--%>
                                <asp:ListItem Value="ExchangeItem">Exchange Items</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div id="GrpfilterSalesReturn" class="col-md-4 form_bootstyle_border">
                        <%--aalayam 02052018 --%>
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="SalesReturnReportFilterUserControl"
                                EnableTerminalDropDown="true"
                                EnableCashierDropDown="true"
                                EnablelocationDropDown="true" />
                            <div class="col-md-12 form_bootstyle" runat="server" id="divMemberSR">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label19" runat="server" Text="Member"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlMemberSR" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtMemberSR" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Member', 'txtMemberSR', '');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border" style="margin-left: 45px;">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label9" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDateReturn" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label10" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDateReturn" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="GroupByRetrun" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="RdoSalRetSumm" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="SUMMARY"
                                    Checked="true" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="RdoSalRetDet" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="DETAIL" />
                            </div>
                        </div>

                        <div id="GroupByRetrunType" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none;">
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="RdoCashSales" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="CASH Sales"
                                    Checked="true" />
                            </div>
                            <div class="col-md-5 form_bootstyle">
                                <asp:RadioButton ID="RdoCreditSales" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="CREDIT Sales" />
                            </div>
                            <div class="col-md-3 form_bootstyle">
                                <asp:RadioButton ID="RdoAll" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="ALL" />
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkByDateReturn" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-5">
                                <%--   <asp:LinkButton ID="lnkSalesReturnClear" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="lnkSalesReturn" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkSalesReturn_Click">Load Report </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="overflow: visible; display: block;" runat="server" id="divSalesAudit">
                <h3>Sales - Audit Reports</h3>
                <div class="row">
                    <div class="col-md-7">
                        <div>
                            <asp:RadioButtonList ID="rdoSalesAudit" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Cancel">Bill Cancel Log</asp:ListItem>
                                <asp:ListItem Value="Discount">Bill Discount Log</asp:ListItem>
                                <asp:ListItem Value="Price">Price Change Log</asp:ListItem>
                                <asp:ListItem Value="Clear">Bill Clear Log</asp:ListItem>
                                <asp:ListItem Value="Paymode">Paymode Change Log</asp:ListItem>
                                <asp:ListItem Value="ItemCancel">ItemCancel Log(Delete item from Grid)</asp:ListItem>
                                <%--<asp:ListItem Value="Backup">DB Backup</asp:ListItem>--%>
                                <asp:ListItem Value="Module">Module Wise Reports</asp:ListItem>
                                <asp:ListItem Value="Brand">Brand wise Sales discount</asp:ListItem>
                                <asp:ListItem Value="Department">Department wise Sales discount</asp:ListItem>
                                <asp:ListItem Value="Category">Category wise Sales discount</asp:ListItem>
                                <asp:ListItem Value="Break">Break Price Sales discount</asp:ListItem>
                                <asp:ListItem Value="Pricechange">Pricechange Sales discount</asp:ListItem>
                                <asp:ListItem Value="Promotion">Promotion wise Discount</asp:ListItem>
                                <%--<asp:ListItem Value="Clearing">Clearing Sales Discount</asp:ListItem>--%>
                                <asp:ListItem Value="Hold">Hold bills</asp:ListItem>
                                <asp:ListItem Value="POS">POS Activity Log</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-5 form_bootstyle_border">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label11" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDateAudit" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label12" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDateAudit" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="GroupByAudit" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoSAdtSumm" Class="radioboxlist" runat="server" GroupName="RegularMenu1" Text="SUMMARY"
                                    Checked="true" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoSAdtDet" Class="radioboxlist" runat="server" GroupName="RegularMenu1" Text="DETAIL" />
                            </div>
                        </div>
                        <%--  <div id="Div2" runat="server" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none">
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="RadioButton3" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="DATE"
                                    Checked="true" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="RadioButton4" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="MONTH" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="RadioButton5" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="NONE" />
                            </div>
                        </div>--%>

                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkBydateSA" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-5">
                                <%--  <asp:LinkButton ID="LinkButton4" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="lnkLoadSalesAudit" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadSalesAudit_Click">Load Report </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:HiddenField ID="HidenInventoryName" runat="server" Value="" />
            <asp:HiddenField ID="hdnReportType" runat="server" Value="" />
            <asp:HiddenField ID="hdnValue" runat="server" Value="" />
            <asp:HiddenField ID="HiddenCode" runat="server" Value="" />
            <asp:HiddenField ID="hidCustCode" runat="server" Value="" />
        </div>
    </div>

</asp:Content>
