﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTerminalWiseReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmTerminalWiseReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'TerminalWiseReport');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "TerminalWiseReport";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }

        };


        function pageLoad() {
            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            $('#<%=chkDate.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                }
            });

            $('#<%= rdoTerminalGST.ClientID %> input').click(function () {
                var checkedstockvalue = $("#<%=rdoTerminalGST.ClientID%>").find(":checked").val();
                if (checkedstockvalue == "Sales") {
                    $("[id*=DateFilter]").show();
                }
                else {
                    $("[id*=DateFilter]").hide();
                }
            });
            $('.ziehharmonika').ziehharmonika({
                highlander: true,
                collapsible: true,
                prefix: '★'
            });

            $('#<%= rdoTerminalGST.ClientID %> input').click(function () {
                $('#ContentPlaceHolder1_txtLocation').val('');
                //$('#ContentPlaceHolder1_ddLocation').trigger("liszt:updated");
                $('#ContentPlaceHolder1_txtTerminal').val('');
                //$('#ContentPlaceHolder1_ddTerminal').trigger("liszt:updated");
                $('#ContentPlaceHolder1_txtDepartment').val('');
                //$('#ContentPlaceHolder1_ddDepartment').trigger("liszt:updated");
            });
        }

        function SetDefaultDate(FromDate, Todate) {

            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        }
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
            </li>
            <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
            <li class="active-page">TerminalWise GSTReport</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <div class="ziehharmonika">
        <div style="overflow: visible; display: block;" runat="server" id="DivTerminalGST">
            <h3>TerminalWise GST Report</h3>
            <div class="row">
                <div class="col-md-3" style="margin-left: 30px">
                    <div>
                        <asp:RadioButtonList ID="rdoTerminalGST" runat="server" Class="radioboxlist">
                            <asp:ListItem Selected="True" Value="Sales"> Sales GST</asp:ListItem>
                            <asp:ListItem Value="SalesInvoice"> Sales GST - InvoiceWise</asp:ListItem>
                            <asp:ListItem Value="SalesItem"> Sales GST - ItemWise</asp:ListItem>
                            <asp:ListItem Value="SalesReturn"> Sales Return GST</asp:ListItem>
                            <asp:ListItem Value="SalesReturnInvoice"> Sales Return GST - InvoiceWise</asp:ListItem>
                            <asp:ListItem Value="SalesReturnItem"> Sales Return GST - ItemWise</asp:ListItem>
                            <asp:ListItem Value="CreditSales"> Credit Sales</asp:ListItem>
                            <asp:ListItem Value="SalesLoaction"> Sales GST LocationWise</asp:ListItem>
                            <asp:ListItem Value="SalesHSN"> Sales GST HSN CodeWise</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                    <div class="report-header-filter">
                        Filtrations
                    </div>
                    <div class="small-box bg-aqua">
                        <div class="col-md-12 form_bootstyle" runat="server" id="divLocation">
                            <div class="col-md-4 form_bootstyle">
                                <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                            </div>
                            <div class="col-md-8 form_bootstyle">
                                <%--<asp:DropDownList ID="ddLocation" runat="server" Style="width: 100%">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtTerminal');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 form_bootstyle" runat="server" id="divTerminal">
                            <div class="col-md-4 form_bootstyle">
                                <asp:Label ID="lblTerminal" runat="server" Text="Terminal"></asp:Label>
                            </div>
                            <div class="col-md-8 form_bootstyle">
                                <%--<asp:DropDownList ID="ddTerminal" runat="server" Style="width: 100%">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtTerminal" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Terminal', 'txtTerminal', 'txtDepartment');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 form_bootstyle" runat="server" id="divDepartment">
                            <div class="col-md-4 form_bootstyle">
                                <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                            </div>
                            <div class="col-md-8 form_bootstyle">
                                <%--<asp:DropDownList ID="ddDepartment" runat="server" Style="width: 100%">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', '');"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 form_bootstyle_border">
                    <div class="barcode-header">
                        Date Filter
                    </div>
                    <div class="small-box bg-aqua">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-md-6 form_bootstyle">
                                    <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                </div>
                                <div class="col-md-6 form_bootstyle">
                                    <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 15px">
                                <div class="col-md-5 form_bootstyle">
                                    <asp:CheckBox ID="chkDate" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                </div>
                                <div class="col-md-2">
                                </div>
                                <asp:UpdatePanel ID="updpnlRefresh2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="col-md-5">
                                            <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadReport_Click">Load Report </asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="col-md-12" id="DateFilter" style="margin-left: 10px; margin-top: 15px">
                                <div class="col-md-4">
                                    <asp:RadioButton ID="rdoDate" Checked="true" runat="server" Text="Date" GroupName="Date" Class="radioboxlist" />
                                </div>
                                <div class="col-md-4">
                                    <asp:RadioButton ID="rdoMonth" runat="server" Text="Month" GroupName="Date" Class="radioboxlist" />
                                </div>
                                <div class="col-md-4">
                                    <asp:RadioButton ID="rdoNone" runat="server" Text="None" GroupName="Date" Class="radioboxlist" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
