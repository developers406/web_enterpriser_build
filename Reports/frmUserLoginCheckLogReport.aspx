﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmUserLoginCheckLogReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmUserLoginCheckLogReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    
    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 150px;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'UserLoginCheckLogReport');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "UserLoginCheckLogReport";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">
        function pageLoad() {

            $(function () {
                SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
                $("select").show().removeClass('chzn-done');
                $("select").next().remove();
                
                //On document ready
                $(function () {
                    $("input[type='radio']").on('click', function (e) {
                        getCheckedRadio($(this).attr("name"), $(this).val(), this.checked);
                    });
                });

                //External function to handle all radio selections
                function getCheckedRadio(group, item, value) {
                   
                    if (item == "UserLoginCheckLog") {
                        $("#<%= txtFromDate.ClientID %>").prop('disabled', false);
                        $("#<%= txtToDate.ClientID %>").prop('disabled', false);
                        $("#<%= txtLocation.ClientID %>").prop('disabled', false);
                        $("#<%= txtSystemName.ClientID %>").prop('disabled', false);
                        $("#<%= txtUserName.ClientID %>").prop('disabled', false);
                    }
                    else if (item == "UserLoginStatus")
                    {
                        $("#<%= txtFromDate.ClientID %>").prop("disabled", true);
                        $("#<%= txtToDate.ClientID %>").prop("disabled", true);
                        $("#<%= txtLocation.ClientID %>").prop("disabled", true);
                        $("#<%= txtSystemName.ClientID %>").prop("disabled", true);
                        $("#<%= txtUserName.ClientID %>").prop("disabled", true);
                    }
                }
            });
            function SetDefaultDate(FromDate, Todate) {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                if (FromDate.val() === '') {
                    FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
                if (Todate.val() === '') {
                    Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
            }
        }


        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
        <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div class="container" >
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Report</a><i class="fa fa-angle-right"></i></li>
                <li  class="active-page"><a>User Login Status</a><i class="fa fa-angle-right"></i></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
             </ul>
        </div>

        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>


               
                    <div style="overflow: visible; display: block;padding-top:10px">
                        <div class="row">

                            <div class="col-md-2">
                                <div>
                                    <asp:RadioButtonList ID="rdoMasterReport" runat="server" Class="radioboxlist">
                                        <asp:ListItem Selected="True" Value="UserLoginCheckLog">User Login Check Log</asp:ListItem>
                                        <asp:ListItem Value="UserLoginStatus">User Login Status</asp:ListItem>

                                    </asp:RadioButtonList>
                                </div>
                            </div>

                            <div class="col-md-4 form_bootstyle_border">
                                <div class="container-group-small">
                                    <div class="container-control">
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label2" runat="server" Text="System Name"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <%--<asp:DropDownList ID="ddlSystemName" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>--%>
                                                <asp:TextBox ID="txtSystemName" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'ComputerName', 'txtSystemName', 'txtUserName');"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label1" runat="server" Text="User Name"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <%--<asp:DropDownList ID="ddlusername" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>--%>
                                                <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'User', 'txtUserName', 'txtLocation');"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label5" runat="server" Text="Location"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>--%>
                                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label3" runat="server" Text="From"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label4" runat="server" Text="To"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="button-contol" style="margin-left: 175px">
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkLoadReport_Click"
                                                OnClientClick="return ValidateForm()"><i class="icon-play"></i>Load</asp:LinkButton>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="HidenInventoryName" runat="server" Value="" />
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>


    </div>
    <script type="text/javascript">
        
    </script>
</asp:Content>
