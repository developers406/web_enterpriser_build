﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmCustomerReceiptEntry.aspx.cs" Inherits="EnterpriserWebFinal.Sales.frmCustomerReceiptEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .process td:nth-child(1), .process th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .process td:nth-child(2), .process th:nth-child(2) {
            min-width: 110px;
            max-width: 110px;
            text-align: left;
        }

        .process td:nth-child(3), .process th:nth-child(3) {
            min-width: 260px;
            max-width: 260px;
            text-align: left;
        }

        .process td:nth-child(4), .process th:nth-child(4) {
            min-width: 95px;
            max-width: 95px;
            text-align: left;
        }

        .process td:nth-child(5), .process th:nth-child(5) {
            min-width: 60px;
            max-width: 60px;
            text-align: right;
        }

        .process td:nth-child(6), .process th:nth-child(6) {
            min-width: 60px;
            max-width: 60px;
            text-align: right;
        }

        .process td:nth-child(7), .process th:nth-child(7) {
            min-width: 70px;
            max-width: 70px;
            text-align: left;
        }

        .process td:nth-child(8), .process th:nth-child(8) {
            min-width: 70px;
            max-width: 70px;
            text-align: right;
        }

        process {
            padding: 2px;
        }


        #tblProcessValue {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #tblProcessValue {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-weight: 700;
        }

            #tblProcessValue td {
                border: 1px solid #ddd;
                padding: 2px;
            }

                #tblProcessValue td:nth-child(even) {
                    background-color: #f2f2f2;
                }

        #tblPendnigValue td:hover {
            background-color: #ddd;
            cursor: pointer;
        }

        #tblProcessValue th {
            padding-top: 2px;
            padding-bottom: 2px;
            background-color: #E91E63;
            font-weight: bolder;
            color: white;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'CustomerReceiptEntry');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "CustomerReceiptEntry";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


    <script type="text/javascript">
        function pageLoad() {
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc   
        }

        function fncInvoiceClick(Receiptno) {
            fncInvoiceLoad($.trim(Receiptno));
        }

        function fncInvoiceLoad(Receiptno) {
            try {
                var obj = {};
                obj.Mode = "7";
                obj.Receiptno = Receiptno;
                $.ajax({
                    type: "POST",
                    url: "frmCustomerReceiptEntry.aspx/fncGetInvoiceDetail",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        AssignTable(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }

                });
            }
            catch (err) {
                err.message();
            }
        }

        function AssignTable(msg) {
            var obj = jQuery.parseJSON(msg.d);
            var tblProcessBody = $("#tblProcessValue tbody");
            tblProcessBody.children().remove();
            if (obj.Table.length > 0) {
                for (var i = 0; i < obj.Table.length; i++) {
                    var row = "<tr id='PendingRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                         (parseFloat(i) + 1) + "</td><td id='tdInventorycode_" + i + "' >" +
                         $.trim(obj.Table[i]["Inventorycode"]) + "</td><td id='tdDescription_" + i + "' >" +
                         $.trim(obj.Table[i]["Description"]) + "</td><td id='tdInvoiceDate_" + i + "' >" +
                         obj.Table[i]["InvoiceDate"] + "</td><td id='tdQty_" + i + "' >" +
                         $.trim(obj.Table[i]["Qty"]) + "</td><td id='tdPrice_" + i + "' >" +
                         $.trim(obj.Table[i]["Price"]) + "</td><td id='tdBatchNo_" + i + "' >" +
                         $.trim(obj.Table[i]["BatchNo"]) + "</td><td id='tdUnitcost_" + i + "' >" +
                         $.trim(obj.Table[i]["Unitcost"]) + "</td></tr>";
                    tblProcessBody.append(row);
                }
            }

            $('#divInvoiceView').dialog({
                autoOpen: false,
                modal: true,
                width: 830,
                title: "Invoice Detail"
            });
            $('#divInvoiceView').dialog('open');
        }

        function fncSaveValidation() {
            try { 
                $('#<%= lnkSave.ClientID %>').css("display", "none");
            if ($('#<%= txtReferenceNo.ClientID %>').val() == "") {
                popUpObjectForSetFocusandOpen = $('#<%= txtReferenceNo.ClientID %>')
                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_RefNo%>');
                $('#<%= lnkSave.ClientID %>').css("display", "block");
                return false;
            }
            else if ($('#<%= ddlPaymode.ClientID %>').val() == '<%=Resources.LabelCaption.ddl_Empty%>' || $('#<%= ddlPaymode.ClientID %>').val() == "") {
                popUpObjectForSetFocusandOpen = $('#<%= ddlPaymode.ClientID %>');
                ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.Alert_PayType%>');
                $('#<%= lnkSave.ClientID %>').css("display", "block");
                return false;
            }
             <%-- else if ($('#<%= ddlBank.ClientID %>').val() == '<%=Resources.LabelCaption.ddl_Empty%>' || $('#<%= ddlBank.ClientID %>').val() == "") {
                popUpObjectForSetFocusandOpen = $('#<%= ddlBank.ClientID %>');
                ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.Alert_BankCode%>');
                $('#<%= lnkSave.ClientID %>').css("display", "block");
                return false;
            }--%>
            else if ($('#<%= ddlBank.ClientID %>').val() == '<%=Resources.LabelCaption.ddl_Empty%>' || $('#<%= ddlBank.ClientID %>').val() == ""
            && $('#<%= ddlPaymode.ClientID %>').val() != "CASH"  && $('#<%= ddlPaymode.ClientID %>').val() != "CD/DN.Adj") {
                popUpObjectForSetFocusandOpen = $('#<%= ddlBank.ClientID %>');
                ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.Alert_BankCode%>');
                $('#<%= lnkSave.ClientID %>').css("display", "block");
                return false;
            }
            else if ($('#<%= ddlPaymode.ClientID %>').val() == "CD/DN.Adj") {
                if (parseFloat($('#<%= txtAmountPaid.ClientID %>').val()) != 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_ZeroPayment%>');
                    $('#<%= lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
            }
            else if (parseInt($('#<%= txtAmountPaid.ClientID %>').val()) <= 0) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_InvalidAmt%>');
                $('#<%= lnkSave.ClientID %>').css("display", "block");
                return false;
            }
            else {
                return true;
            }

}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
     <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li> 
                <li><a href="../Sales/frmCustomerReceipts.aspx">Customer Receipts View</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Customer Receipts</li>  <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="receipt-entry-header">
                        <div class="receipt-entry-header-left">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label7" runat="server" Text="Receipt No"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtReceiptNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server" Text="Receipt Date"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtReceiptDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="receipt-entry-header-right">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label10" runat="server" Text="Customer"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtCustomerCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left" style="width: 100%">
                                        <asp:TextBox ID="txtCustomerName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-single">
                                <div class="label-left">
                                    <asp:Label ID="Label2" runat="server" Text="Remarks"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="receipt-entry-middle">
                        <div class="receipt-entry-middle-left">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text="Paymode"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlPaymode" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label5" runat="server" Text="Reference No"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtReferenceNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label6" runat="server" Text="Bank"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlBank" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label8" runat="server" Text="Cashier ID"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtCasierID" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="receipt-entry-middle-middle">
                        </div>
                        <div class="receipt-entry-middle-right">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label9" runat="server" Text="Total Due Amount"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtTotalDueAmt" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label11" runat="server" Text="Amount Paid"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtAmountPaid" runat="server" CssClass="form-control-res" OnTextChanged="txtAmountPaid_TextChanged"
                                        AutoPostBack="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label12" runat="server" Text="Un Applied Amount"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtUnAppliedAmt" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="receipt-entry-grid">
                        <div class="GridDetails" style="overflow: auto; height: 333px;">
                            <asp:GridView ID="gvBalanceDetails" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                CssClass="pshro_GridDgn" OnRowDataBound="grdItemDetails_RowDataBound">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No"></asp:BoundField>
                                    <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice Date"></asp:BoundField>
                                    <asp:BoundField DataField="TotalSalesValue" HeaderText="Total Amount"></asp:BoundField>
                                    <asp:BoundField DataField="PaidAmount" HeaderText="Paid Amount"></asp:BoundField>
                                    <asp:BoundField DataField="VoucherCommission" HeaderText="Voucher Commission"></asp:BoundField>
                                    <asp:BoundField NullDisplayText="0.00" HeaderText="Applied Amt"></asp:BoundField>
                                    <asp:BoundField DataField="BalanceAmount" HeaderText="Balance"></asp:BoundField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="Label18" runat="server" Text="Pay"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPay" runat="server" OnCheckedChanged="chkPay_CheckedChange"
                                                AutoPostBack="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField NullDisplayText="" HeaderText="Location"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="control-container">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click" class="button-red" Text="Print"></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return fncSaveValidation();" Text="Save" OnClick="lnkSave_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" Text="Clear"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="display_none">
        <div id="divInvoiceView" class="col-md-12">
            <div class="process Vendor_margin_border">
                <div class="col-md-12" style="display: none">
                    <table id="tblProcessValuehead" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                <th scope="col">Vendor Code
                                </th>
                                <th scope="col">Vendor Name
                                </th>
                                <th scope="col">PO Date
                                </th>
                                <th scope="col">Po Value
                                </th>
                                <th scope="col">Total
                                </th>
                                <th scope="col">PO.No
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-md-12" style="overflow-y: scroll; overflow-x: hidden; height: 200px">
                    <table id="tblProcessValue" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                <th scope="col">Inventory Code
                                </th>
                                <th scope="col">Description
                                </th>
                                <th scope="col">Invoice Date
                                </th>
                                <th scope="col">Qty
                                </th>
                                <th scope="col">Price
                                </th>
                                <th scope="col">BatchNo 
                                </th>
                                <th scope="col">Unitcost
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
