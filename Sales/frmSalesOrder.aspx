﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmSalesOrder.aspx.cs" Inherits="EnterpriserWebFinal.Sales.frmSalesOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type ="text/css">
            .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

       
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'SalesOrder');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "SalesOrder";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">

        function pageLoad() {
           // $("[id*=lnkSave]").hide();
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            SetAutoComplete();
        }

        $(function () {
            try {

                $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" });
                if ($("#<%= txtDeliveryDate.ClientID %>").val() === '') {
                    
                    $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
                } 
            }
            catch (err) {
                alert(err.Message);
            }
        });

        $("tbody.dataGridBatchTable_master_tablebody > tr").focus(function () {
            $(this).closest('tr').addClass('highlight_row');
        });

        function ShowAdditional() {
            $("#dialog - Additional").dialog("open");
        }

        function fncAddDeliveryDate() {
            //alert( $("#HiddenDeliveryDate").val());
            $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", $("#HiddenDeliveryDate").val());
        }

        

        function fncHidePrintBtn(evt) {
            //alert($(evt.target).val());
            if ($(evt.target).val() == '') {
                $('#<%=btnReprint.ClientID %>').hide();
            }
        }

        function SetAutoComplete() {

            $("[id$=txtDeliveryDate]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Managements/frmCreditInvoice.aspx/GetInvoiceDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {

                                    label: item.split('|')[0],
                                    valCode: item.split('|')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {

                    //$('#<%=txtAddress1.ClientID %>').val($.trim(i.item.valAddress1));                
                    $('#<%=btnReprint.ClientID %>').show();
                    return false;
                },
                minLength: 3
            });



            $('#<%=txtCustName.ClientID %>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Sales/frmSalesOrder.aspx/GetMemberDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valCode: item.split('|')[1],
                                    valAddress1: item.split('|')[2],
                                    valMobileNo: item.split('|')[3]
                                    //                                    valCode: item.split('|')[1],
                                    //                                    valAddress1: item.split('|')[2],
                                    //                                    valAddress2: item.split('|')[3],
                                    //                                    valAddress3: item.split('|')[4],
                                    //                                    valMobileNo: item.split('|')[5]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtCustName.ClientID %>').val($.trim(i.item.label));
                    event.preventDefault();
                },
                select: function (e, i) { 
                    $('#<%=txtCustName.ClientID %>').val($.trim(i.item.label));
                    $('#<%=txtAddress1.ClientID %>').val($.trim(i.item.valAddress1));
                    $('#<%=txtMobileNo.ClientID %>').val($.trim(i.item.valMobileNo));
                    $("#HiddenMemberCode").val($.trim(i.item.valCode));

                    $('#<%=txtInventory.ClientID %>').focus().select();

                    return false;
                },
                minLength: 1
            });

            $("[id$=txtInventory]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Managements/frmCreditInvoice.aspx/GetInventoryDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valCode: item.split('|')[1],
                                    valDescription: item.split('|')[2],
                                    valCost: item.split('|')[3],
                                    valTax: item.split('|')[4],
                                    valCess: item.split('|')[5],
                                    valBatch: item.split('|')[6]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {

                    $("#HidenIsBatch").val($.trim(i.item.valBatch));
                    $("#HiddenItemCode").val($.trim(i.item.valCode));
                    $("#HiddenItemName").val($.trim(i.item.valDescription));
                    $("#HiddenGSTperc").val($.trim(i.item.valTax));

                    if ($("#HidenIsBatch").val() == 'True') {
                        fncGetBatchDetail_Master(i.item.valCode);
                    }
                    else {
                        AddRow($.trim(i.item.valCode), $.trim(i.item.valDescription), $.trim(i.item.valCost), 1, $.trim(i.item.valTax), '');
                    }

                    return false;
                },
                minLength: 3
            });

        }

        function checkKey(e) {
            var event = window.event ? window.event : e;
            if (event.keyCode == 40) { //down
                var idx = $("tr:focus").attr("tabindex");
                idx++;
                if (idx > 6) {
                    idx = 0;
                }
                $("tr[tabindex=" + idx + "]").focus();
            }
            if (event.keyCode == 38) { //up
                var idx = $("tr:focus").attr("tabindex");
                idx--;
                if (idx < 0) {
                    idx = 6;
                }
                $("tr[tabindex=" + idx + "]").focus();
            }
        }

        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost) {
            try {

                AddRow($("#HiddenItemCode").val(), $("#HiddenItemName").val(), sprice, 1, $("#HiddenGSTperc").val(), batchno);

            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncLoadSOtogrid(SONo, CustomerCode, NetTotal, Advance) {
            try {

                $("#HiddenSONo").val(SONo);

                __doPostBack('ctl00$ContentPlaceHolder1$lnkView', '');
            }

            catch (err) {
                alert(err.message);
            }
        }


        function AddEmptyRow() {

            try {

                var row = $("[id*=grdItemDetail] tr:last").clone();
                $("td:nth-child(1)", row).html('');
                $("td:nth-child(2)", row).html('');
                $("td:nth-child(3)", row).html('');
                $("td:nth-child(4) > input", row).val('');
                $("td:nth-child(5) > input", row).val('');
                $("td:nth-child(6)", row).html('');
                $("td:nth-child(7)", row).html('');
                $("td:nth-child(8)", row).html('');
                $("td:nth-child(9)", row).html('');
                $("td:nth-child(10)", row).html('');
                $("td:nth-child(11)", row).html('');
                $("td:nth-child(12)", row).html('');
                $("td:nth-child(13)", row).html('');
                $("[id*=grdItemDetail] tbody").append(row);

                return false;

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        function AddRow(itemcode, itemname, price, Qty, Tax, BatchNo) {

            try {
                itemname = itemname.replace('&', ' ');
                //alert(itemname);
                var divide = 100 + parseFloat(Tax);
                var taxamt = (parseFloat(price) / divide) * parseFloat(Tax)
                var SellPrice = parseFloat(price) - taxamt.toFixed(2);
                var Totalamount = parseFloat(SellPrice) + parseFloat(taxamt);
                var itemcheck = false;
                $("#<%=grdItemDetail.ClientID%> tr").each(function () {
                    if (!this.rowIndex) return;
                    var gitem = $(this).find("td.Itemcode").html();
                    var gBatchNo = $(this).find("td.BatchNo").html();
                    //alert(gitem);

                    if ($.trim(gitem) == $.trim(itemcode) && $.trim(gBatchNo) == $.trim(BatchNo)) {
                        alert("Item already Exists !");
                        itemcheck = true;
                        $('#<%=txtInventory.ClientID %>').focus().select();
                    }
                });

                if (itemcode != "" && itemcheck == false) {
                    var row = $("[id*=grdItemDetail] tr:last").clone();
                    // $("[id*=grdChildItem] tr:last").remove(); 
                    $("td:nth-child(1)", row).html(itemcode);
                    $("td:nth-child(2)", row).html(BatchNo);
                    $("td:nth-child(3)", row).html(itemname);
                    $("td:nth-child(4) > input", row).val(Qty);
                    $("td:nth-child(4) > input", row).attr('id', 'Qty_' + itemcode);
                    $("td:nth-child(5) > input", row).val(SellPrice.toFixed(2));
                    $("td:nth-child(5) > input", row).attr('id', 'Price_' + itemcode);
                    $("td:nth-child(6)", row).html(SellPrice.toFixed(2));
                    $("td:nth-child(7)", row).html('0.00');
                    $("td:nth-child(8)", row).html('0.00');
                    $("td:nth-child(9)", row).html(SellPrice.toFixed(2));
                    $("td:nth-child(10)", row).html(Tax);
                    $("td:nth-child(11)", row).html(taxamt.toFixed(2));
                    $("td:nth-child(12)", row).html(Totalamount.toFixed(2));
                    $("td:nth-child(13)", row).html(SellPrice.toFixed(2));
                    $("[id*=grdItemDetail] tbody").append(row);

                    //$("td:nth-child(3) > input", row).on('change', isNumberKey());
                    $('#<%=txtInventory.ClientID %>').val('');
                    //$("td:nth-child(4) > input", row).focus().select();
                    $('#<%=txtInventory.ClientID %>').focus().select();
                }

                $("#<%=grdItemDetail.ClientID%> tr").each(function () {
                    if (!$.trim($(this).text())) $(this).remove();
                });
                fncDisplaytotal();
                // alert('PODA');
                XmlGridValue();

                $("[id*=lnkSave]").show();

                return false;

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        function XmlGridValue() {
            try {

                $("#HiddenNewitem").val('Y');
                var xml = '<NewDataSet>';
                $("#<%=grdItemDetail.ClientID %> tr").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        //alert(cells.eq(0).text().trim());
                        if (cells.eq(0).text().trim() != '') {
                            xml += "<Table>";
                            xml += "<Inventorycode>" + cells.eq(0).text().trim() + "</Inventorycode>";
                            xml += "<BatchNo>" + cells.eq(1).text().trim() + "</BatchNo>";
                            xml += "<Description>" + cells.eq(2).text().trim() + "</Description>";
                            xml += "<Quantity>" + cells.eq(3).find('input').val().trim() + "</Quantity>";
                            xml += "<Price>" + cells.eq(4).find('input').val().trim() + "</Price>";
                            xml += "<Amount>" + cells.eq(5).text().trim() + "</Amount>";
                            xml += "<Discount>" + cells.eq(6).text().trim() + "</Discount>";
                            xml += "<BillDisc>" + cells.eq(7).text().trim() + "</BillDisc>";
                            xml += "<Taxable>" + cells.eq(8).text().trim() + "</Taxable>";
                            xml += "<GST>" + cells.eq(9).text().trim() + "</GST>";
                            xml += "<GSTAmount>" + cells.eq(10).text().trim() + "</GSTAmount>";
                            xml += "<NetAmount>" + cells.eq(11).text().trim() + "</NetAmount>";

                            //                        for (var i = 1; i < cells.length; ++i) {
                            //                            xml += '<Data year="' + $("#grdChildItem").find("th").eq(i).text() + '" status="' + $("option:selected", cells.eq(i)).text() + '"/>';
                            //                        }

                            xml += "</Table>";
                        }
                    }
                });

                xml = xml + '</NewDataSet>'
                //alert(xml);
                $("#HiddenXmldata").val(escape(xml));
                //$("#HiddenXmldata").val(xml);
                //alert($("#HiddenXmldata").val());

                return false;

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 240,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncSaveValidation() {
            //            var r = confirm("Do you want to Save?");
            //            if (r === false) {
            //                return false;
            //            }
            //            ShowAdditional();
            //alert($("#HiddenMemberCode").val());

            if ($("#HiddenMemberCode").val() != '0') {
                //fncShowConfirmSaveMessage();
                XmlGridValue();
            }
            else {
                alert('Please Select Customer...!');
                $("[id*=txtCustName]").focus();
            }

            ////            alert($("#HiddenPONo").val());
            ////            alert($("#HiddenSerialNo").val());
            ////alert($("#HiddenXmldata").val());

            // return false;
        }

        function fncSetAdditionalField() {

            //            alert($("[id*=txtPoNumber]").val());
            //            alert($("[id*=txtRemarks]").val());

            $("#HiddenPONo").val($("[id*=txtPoNumber]").val());
            $("#HiddenSerialNo").val($("[id*=txtRemarks]").val());

            return false;
        }
        function AddremoveEmptyrow() {
            //            if ($("[id*=grdChildItem] tr:last-child").find('td').length = 1) {
            //                //Addemptyrow()
            //                $("[id*=grdChildItem] tr:last").after('<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>');
            //            }
            //            else {
            //                $("[id*=grdChildItem] tr").each(function () {
            //                    if (!$.trim($(this).text())) $(this).remove();
            //                });
            //            }
            $("[id*=grdItemDetail] tr").each(function () {
                if (!$.trim($(this).text())) $(this).remove();
            });

            return false;
        }

        function fncClearForm() {

            //$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val("");

            $("[id*=txtDeliveryDate]").val("");
            $("[id*=txtCustName]").val("");
            $("[id*=txtInventory]").val("");
            $("[id*=txtDiscount]").val("");
            $("[id*=txtDiscPercent]").val("");
            $("[id*=txtAddress1]").val("");
            $("[id*=txtMobileNo]").val("");

            $("[id*=lblAmount]").text("0.00");
            $("[id*=lblDiscount]").text("0.00");
            $("[id*=lblItemDiscount]").text("0.00");
            $("[id*=lblGSTAmount]").text("0.00");
            $("[id*=lblRoundOff]").text("0.00");
            $("[id*=lblNetAmount]").text("0.00");
            $("[id*=lblAdvance]").text("0.00");
            $("[id*=lblBalanceAmt]").text("0.00");
            //            $(':checkbox, :radio').prop('checked', false);
            //            $("select").val(0);
            //            $("select").trigger("liszt:updated");
        }

        function fncCleardialog() {

            $("[id*=txtPoNumber]").val('');
            $("[id*=txtRemarks]").val('');
            $("[id*=txtFreight]").val('');

            return false;
        }

        function fncLoadSaleOrder() {

            if ($("#HiddenMemberCode").val() != '0')
                fncGetSODetail($("#HiddenMemberCode").val());
            else
                ShowPopupMessageBox('Please Select Customer..!');

            return false;
        }

        function fncDiscountCalc(evt) {

            var totalQuantity = 0;
            var totaldiscount = 0;
            var discountpercent = 0;
            var totalAmount = 0;
            var totalGST = 0;
            var NetAmount = 0;
            var totalTaxable = 0;

            var objAmount = null;
            var objTaxable = null;
            var objDiscount = null;
            var objBillDiscount = null;
            var objGSTPerc = null;
            var objGSTamount = null;
            var objNetAmount = null;
            var objTotalQty = null;

            Netamount = $("[id*=lblAmount]").text();

            if (Netamount == '0.00')
                return;

            var ValidTD = $(evt.target).attr('id').indexOf("_txtDiscount");
            var discountvalue = $("[id*=txtDiscount]").val();
            var discountPerc = $("[id*=txtDiscPercent]").val();

            //            alert(Netamount);
            //            alert(discountvalue);
            //            alert(ValidTD);

            if (ValidTD != '-1') {
                totaldiscount = parseFloat(Netamount) - parseFloat(discountvalue);
                discountpercent = (discountvalue * 100) / parseFloat(Netamount);
                $("[id*=txtDiscPercent]").val(discountpercent.toFixed(2).toString());
            }
            else {

                totaldiscount = (parseFloat(Netamount) * discountPerc) / 100;
                $("[id*=txtDiscount]").val(totaldiscount.toFixed(2).toString());
            }

            var DiscountAmt = 0;
            var DiscountPerc = $("[id*=txtDiscPercent]").val();
            var dItemDisc = 0;
            var dtaxable = 0;
            var dGSTPerc = 0;

            $("[id*=grdItemDetail] tr").each(function () {
                objAmount = $(this).find(".LAmount");
                objGSTamount = $(this).find(".LGSTAmount");
                objGST = $(this).find(".LGST");
                objNetAmount = $(this).find(".LNetAmount");
                objTaxable = $(this).find(".LTaxable");
                objDiscount = $(this).find(".LDiscount");
                objBillDiscount = $(this).find(".LBillDisc");

                dItemDisc = parseFloat(objDiscount.text());
                dtaxable = parseFloat(objAmount.text());
                DiscountAmt = parseFloat(objAmount.text()) * DiscountPerc / 100;
                dGSTPerc = parseFloat(objGST.text());

                if (!isNaN(parseFloat(objAmount.text()))) {

                    //Discount
                    objBillDiscount.text(DiscountAmt.toFixed(2).toString());

                    //Taxable
                    totaldiscount = DiscountAmt + dItemDisc;
                    totalTaxable = dtaxable - totaldiscount;
                    objTaxable.text(totalTaxable.toFixed(2).toString());

                    //GST 
                    totalGST = totalTaxable * dGSTPerc / 100;
                    objGSTamount.text(totalGST.toFixed(2).toString());

                    //Net Amount 
                    NetAmount = totalTaxable + totalGST;
                    objNetAmount.text(NetAmount.toFixed(2).toString());
                }
            });


            fncDisplaytotal();
            XmlGridValue();

            return false;
        }

        function fncPriceCalc(evt) {

            var amountTD = null;
            var TaxableTD = null;
            var totalamountTD = null;
            var DiscountTD = null;
            var BillDiscountTD = null;
            var Pricetxt = null;
            var Qtytxt = null;
            var TaxpercTD = null;
            var DiscountTD = null;
            var ActPrice = null;
            var ItemcodeTD = null;

            var ValidTD = $(evt.target).attr('id').indexOf("Price_");
            var TaxamountTD = null; // $(evt.target).closest("td").next().next().next().text();
            //alert(TaxamountTD);

            if (ValidTD == '-1') {
                amountTD = $(evt.target).closest("td").next().next();
                Pricetxt = $(evt.target).closest("td").next().find("input");
                Qtytxt = $(evt.target);
                DiscountTD = $(evt.target).closest("td").next().next().next();
                BillDiscountTD = $(evt.target).closest("td").next().next().next().next();
                TaxableTD = $(evt.target).closest("td").next().next().next().next().next();
                TaxpercTD = $(evt.target).closest("td").next().next().next().next().next().next();
                TaxamountTD = $(evt.target).closest("td").next().next().next().next().next().next().next();
                totalamountTD = $(evt.target).closest("td").next().next().next().next().next().next().next().next();
                ActPrice = $(evt.target).closest("tr").children('td:last');

                //Pricetxt.focus().select();
            }
            else {

                amountTD = $(evt.target).closest("td").next();
                Qtytxt = $(evt.target).closest("td").prev().find("input");
                Pricetxt = $(evt.target);
                DiscountTD = $(evt.target).closest("td").next().next();
                BillDiscountTD = $(evt.target).closest("td").next().next().next()
                TaxableTD = $(evt.target).closest("td").next().next().next().next();
                TaxpercTD = $(evt.target).closest("td").next().next().next().next().next();
                TaxamountTD = $(evt.target).closest("td").next().next().next().next().next().next();
                totalamountTD = $(evt.target).closest("td").next().next().next().next().next().next().next();
                ActPrice = $(evt.target).closest("tr").children('td:last');

                //$('#<%=txtInventory.ClientID %>').focus().select();
            }
            var Actualprice = parseFloat(ActPrice.text());
            var SellPrice = parseFloat(Pricetxt.val());
            var dBillDisc = parseFloat(BillDiscountTD.text());

            //            alert(Actualprice);
            //            alert(SellPrice);
            if (!isNaN(Actualprice)) {

                if (SellPrice == 0) {
                    Pricetxt.val(Actualprice.toFixed(2));
                    Qtytxt.focus();
                    Pricetxt.focus().select();
                    return;
                }
                if (Qtytxt.val() == 0) {
                    Qtytxt.val('1');
                    Pricetxt.focus();
                    Qtytxt.focus().select();
                    return;
                }

                //                if (Actualprice < SellPrice) {
                //                    Pricetxt.val(Actualprice.toFixed(2));
                //                    alert('Price is Greater than MRP..!');
                //                    Qtytxt.focus();
                //                    Pricetxt.focus().select();
                //                    return;
                //                }

                //Change for Price change discount
                //var dAmount = Qtytxt.val() * Actualprice;

                var discount = 0;
                var dAmount = Qtytxt.val() * SellPrice;
                var dTaxable = 0;
                if (Actualprice > SellPrice) {
                    discount = Actualprice - SellPrice;
                }

                if (discount > 0) {
                    discount = discount * Qtytxt.val();
                    DiscountTD.text(discount.toFixed(2));
                    //alert(discount);
                }
                else {
                    DiscountTD.text('0.00');
                }


                discount = dBillDisc + discount;
                dTaxable = dAmount - discount;

                //                if (dTaxable < 0) {
                //                    dTaxable = (dTaxable * -1)
                //                }
                //                alert(dTaxable);
                //                alert(discount);

                var Taxamount = (dTaxable * TaxpercTD.text()) / 100;
                var TotalAmount = dTaxable + Taxamount;

                //                alert(dBaseamount);
                //                alert(Taxamount); 
                //                alert(TaxpercTD.text());
                //                alert(dAmount);
                //                alert(Taxamount);
                //                alert(TotalAmount);

                amountTD.text(dAmount.toFixed(2));
                TaxamountTD.text(Taxamount.toFixed(2));
                totalamountTD.text(TotalAmount.toFixed(2));
                TaxableTD.text(dTaxable.toFixed(2));

                fncDisplaytotal();

                XmlGridValue();

                //                Qtytxt.each(function () {
                //                    totalQuantity = totalQuantity + parseInt($(this).val());
                //                });

                //                amountTD.each(function () {
                //                    //alert($(this).text());
                //                    totalAmount = totalAmount + parseFloat($(this).text());
                //                });

                //                TaxamountTD.each(function () {
                //                    //alert($(this).text());
                //                    totalGST = totalGST + parseFloat($(this).text());
                //                });

                //                totalamountTD.each(function () {
                //                    //alert($(this).text());
                //                    NetAmount = NetAmount + parseFloat($(this).text());
                //                });



            }

            return true;
        }


        function fncDisplaytotal() {

            var totalQuantity = 0;
            var totaldiscount = 0;
            var itemdiscount = 0;
            var totalAmount = 0;
            var totalGST = 0;
            var NetAmount = 0;
            var BalanceAmount = 0;
            var totalTaxable = 0;
            var AdvanceAmount = 0;

            var objAmount = null;
            var objTaxable = null;
            var objDiscount = null;
            var objItemDiscount = null;
            var objGSTperc = null;
            var objGSTamount = null;
            var objNetAmount = null;
            var objTotalQty = null;

            $("[id*=grdItemDetail] tr").each(function () {
                objAmount = $(this).find(".LAmount");
                objGSTamount = $(this).find(".LGSTAmount");
                objGSTperc = $(this).find(".LGST");
                objNetAmount = $(this).find(".LNetAmount");
                objTotalQty = $(this).find(".gridInvoice-textbox");
                objDiscount = $(this).find(".LBillDisc");
                objItemDiscount = $(this).find(".LDiscount");

                if (objAmount.text() != '') {
                    totalAmount = totalAmount + parseFloat(objAmount.text());
                }
                if (objDiscount.text() != '') {
                    totaldiscount = totaldiscount + parseFloat(objDiscount.text());
                }
                if (objGSTamount.text() != '') {
                    totalGST = totalGST + parseFloat(objGSTamount.text());
                }
                if (objNetAmount.text() != '') {
                    NetAmount = NetAmount + parseFloat(objNetAmount.text());
                }

                if (objItemDiscount.text() != '') {
                    itemdiscount = itemdiscount + parseFloat(objItemDiscount.text());
                }

                //                if (objTotalQty.text() != '') {
                //                    totalQuantity = totalQuantity + parseFloat(objTotalQty.text());
                //                }
                //alert(objAmount.text()); 

            });

            //            var dRountOff = 0;
            var dRoundVal = 0;
            var dRoundAmount = 0;

            //            //NetAmount = parseFloat(NetAmount).toFixed(2);
            //
            //alert(NetAmount);
            if (isNaN(NetAmount) == true) {
                alert(NetAmount);
                return false;
            }

            dRoundAmount = NetAmount.toFixed(2);
            var fields = dRoundAmount.toString().split('.');
            dRoundVal = parseFloat(fields[1]);

            if (parseFloat(dRoundVal) >= 50) {

                dRountOff = (100 - dRoundVal) / 100;
            }
            else {

                dRountOff = (dRoundVal * -1) / 100;
            }
             
            //alert($("#HiddenAdvanceAmt").val());
            NetAmount = NetAmount + dRountOff;
            if ($("[id*=txtAdvance]").val() == 0)
            {
                AdvanceAmount = NetAmount * $("#HiddenAdvanceAmt").val() / 100;
                $("[id*=txtAdvance]").val(AdvanceAmount.toFixed(2).toString());
            } 
      
            //alert(AdvanceAmount);
            BalanceAmount = NetAmount - $("[id*=txtAdvance]").val();
            // alert(NetAmount);
            $("[id*=lblAmount]").text(totalAmount.toFixed(2).toString());
            $("[id*=lblDiscount]").text(totaldiscount.toFixed(2).toString());
            $("[id*=lblItemDiscount]").text(itemdiscount.toFixed(2).toString());

            $("[id*=lblGSTAmount]").text(totalGST.toFixed(2).toString());
            $("[id*=lblRoundOff]").text(dRountOff.toFixed(2).toString());
            $("[id*=lblNetAmount]").text(NetAmount.toFixed(2).toString());
            $("[id*=lblBalanceAmt]").text(BalanceAmount.toFixed(2).toString());
            $("[id*=lblAdvance]").text($("[id*=txtAdvance]").val());

            $("#HiddenSubtotal").val(totalAmount.toFixed(2));
            $("#HiddenDiscount").val(totaldiscount.toFixed(2));
            $("#HiddenItemDisc").val(itemdiscount.toFixed(2));
            $("#HiddenRoundoff").val(dRountOff.toFixed(2));
            $("#HiddenGST").val(totalGST.toFixed(2));
            $("#HiddenNetamount").val(NetAmount.toFixed(2));
            $("#HiddenBalanceAmt").val(BalanceAmount.toFixed(2));
            //alert(NetAmount); 
        }


        function fncDiscountAmountCalc(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            if (charCode == 13) {
                // $('#<%=txtDiscPercent.ClientID %>').focus().select();
            }

            return true;
        }

        function fncDiscountPercCalc(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            if (charCode == 13) {
                //$('#<%=txtDiscPercent.ClientID %>').focus().select();
            }

            return true;
        }


        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            if (charCode == 46 && $(evt.target).val().indexOf('.') != -1) {
                evt.preventDefault();
            } // prevent if already dot

            if (charCode == 13) {
                var ValidTD = $(evt.target).attr('id').indexOf("Price_");
                //alert($("#HidenIsBatch").val());

                if ($("#HidenIsBatch").val() == 'False') {
                    if (ValidTD == '-1') {
                        Pricetxt = $(evt.target).closest("td").next().find("input");
                        Pricetxt.focus().select();
                    }
                    else {
                        $('#<%=txtInventory.ClientID %>').focus().select();
                    }
                }

                $("#HidenIsBatch").val('False');

                //$(evt.target).closest("td").next().next().text('prabu');
                //alert($(evt.target).attr('id').indexOf("Price_"));
                //alert($(evt.target).closest("td").next().text());
            }
            return true;
        }

        function fncInventorySearch(event) {

            <%--            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                //                $('#<%=txtAddress1.ClientID %>').focus().select();
                //                fncGetBatchDetail_Master($(event.target).val());
            }
            return true;--%>

            try {

                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    //alert($("#HiddenItemCode").val());
                    //if ($("#HiddenItemCode").val() == '')
                    //{ 
                    //    return false;
                    //}
                    //event.preventDefault();
                    <%--fncGetBatchDetail_Master($("#<%=txtInventory.ClientID %>"));--%>
                    if ($("#<%=txtInventory.ClientID %>").val() != '') {
                        fncBarcodeDetail($("#<%=txtInventory.ClientID %>").val());
                    }
                    //event.preventDefault();
                    //event.stopPropagation();
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }



        function isMembertext(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode == 13) {

                $('#<%=txtInventory.ClientID %>').focus().select();

            }
            return true;
        }

        $(function () {
            $("[id*=grdItemDetail]").on('click', 'td:first-child', function (e) {
                var r = confirm("Do you want to Delete?");
                if (r === false) {
                    return false;
                }
                var totalRows = $("#<%=grdItemDetail.ClientID %> tr").length;
                //alert(totalRows);
                if (totalRows == 2) {
                    AddEmptyRow('', '', 0.00);
                    $(this).closest("tr").remove();
                    $("[id*=lnkSave]").hide();
                }
                else {
                    $(this).closest("tr").remove();
                }
                fncDisplaytotal();
                XmlGridValue();
                $('#<%=txtInventory.ClientID %>').focus().select();
                return false;
            });

        });


        function fncLoadSalesOrder(sSONo, sType) {
            //debugger;
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/Inventory/frmInventoryParentChildSettings.aspx/GetInventory")%>',
                data: "{'InvCode':'" + sSONo + "','Type':'" + sType + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }


        function fncBarcodeDetail(sItemCode) {
            //debugger;
            var isBatch, Code, Description, GST, Cost;
            $.ajax({
                url: '<%=ResolveUrl("~/Managements/frmCreditInvoice.aspx/GetBarcodeDetail")%>',
                data: "{ 'ItemCode': '" + sItemCode + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    //alert(data.d);

                    $.map(data.d, function (item) {

                        isBatch = item.split('|')[6];
                        Description = item.split('|')[0];
                        Code = item.split('|')[1];
                        GST = item.split('|')[4];
                        Cost = item.split('|')[3];
                    })

                    //alert(isBatch);
                    if (Code != 'NO') {
                        $("#HidenIsBatch").val($.trim(isBatch));
                        $("#HiddenItemCode").val($.trim(Code));
                        $("#HiddenItemName").val($.trim(Description));
                        $("#HiddenGSTperc").val($.trim(GST));

                        if ($("#HidenIsBatch").val() == 'True') {
                            fncGetBatchDetail_Master(Code);
                        }
                        else {
                            AddRow($.trim(Code), $.trim(Description), $.trim(Cost), 1, $.trim(GST), '');
                        }

                    }
                    //else
                    //{
                    //    alert('Invalid Barcode');
                    //} 
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        }



        //C.CInventorycode, I.Description, I.MRP, I.Uom, I.weight, C.PInventorycode 
        function OnSuccess(response) {
            try {

                if (response.d == '<NewDataSet />')
                    response.d = '<NewDataSet> <Table> <Inventorycode>  </Inventorycode> <Description>  </Description> <Mrp>  </Mrp> <Uom>  </Uom> <Weight> </Weight> <Parentcode>  </Parentcode> </Table>  </NewDataSet>';

                var xmlDoc = $.parseXML(response.d);
                var xml = $(xmlDoc);
                var customers = xml.find("Table");

                var row = $("[id*=grdItemDetail] tr:last-child").clone(true);
                $("[id*=grdItemDetail] tr").not($("[id*=grdItemDetail] tr:first-child")).remove();
                $.each(customers, function () {
                    var customer = $(this);
                    $("td", row).eq(0).html($(this).find("Inventorycode").text());
                    $("td", row).eq(1).html($(this).find("Description").text());
                    $("td", row).eq(2).html($(this).find("Mrp").text());
                    $("td", row).eq(3).html($(this).find("Uom").text());
                    $("td", row).eq(4).html($(this).find("Weight").text());
                    $("td", row).eq(5).html($(this).find("Parentcode").text());
                    $("[id*=grdItemDetail]").append(row);
                    row = $("[id*=grdItemDetail] tr:last-child").clone(true);
                });
            }
            catch (err) {
                alert(err.Message);
                console.log(err);
            }
        }


    </script>
    <%------------------------------------------- Clear Form -------------------------------------------%>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);

            $("select").val(0);
            $("select").trigger("liszt:updated");
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">

        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li class="active-page">Sales Order</li>  <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        <div class="container-group-full EnableScroll">
            <div class="container-pers-top-header-so">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-1" style="padding: 5px;">
                            <asp:Label ID="Label10" CssClass="label-invoice" runat="server" Text="Customer :"></asp:Label>
                        </div>

                        <div class="col-md-2" style="padding: 3px;">
                            <asp:TextBox ID="txtCustName" runat="server" CssClass="form-control-res" onkeypress="return isMembertext(event)"></asp:TextBox>
                        </div>

                        <div class="col-md-1" style="padding: 5px;">
                            <asp:Label ID="Label11" CssClass="label-invoice" runat="server" Text="Item Search :"></asp:Label>
                        </div>

                        <div class="col-md-3" style="padding: 3px;">
                            <asp:TextBox ID="txtInventory" runat="server" CssClass="form-control-res" onkeydown="return fncInventorySearch(event)"></asp:TextBox>
                        </div>

                        <div class="col-md-1" style="margin-left: 60px; padding: 5px;">
                            <asp:Label ID="Label12" CssClass="label-invoice" runat="server" Text="Delivery Date :"></asp:Label>
                        </div>

                        <div class="col-md-1" style="padding: 3px;">
                            <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="form-control-res" Style="text-align: center" onfocusout="return fncHidePrintBtn(event)"></asp:TextBox>
                        </div>

                        <div class="col-md-1" style="margin-left: 30px; padding: 5px;">
                            <asp:Label ID="Label9" CssClass="label-invoice" runat="server" Text="Advance :"></asp:Label>
                        </div>

                        <div class="col-md-1" style="padding: 3px;">
                            <asp:TextBox ID="txtAdvance" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center" oninput="fncDisplaytotal()" onkeypress="return isNumberKey(event)"></asp:TextBox>
                        </div>

                    </div>
                </div>
            </div>

            <div class="container-invoice-bottom">
                <div class="container-invoice-detail">
                    <div class="container-invoice-left">
                        <div class="container-invoice-header">
                            Customer Address
                        </div>
                        <div class="control-group-single">
                            <div class="label-left">
                                <asp:Label ID="Label18" runat="server" Font-Size="Smaller" Text="Address"></asp:Label>
                                <span
                                    class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control-res" Height="150px"
                                    TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <%--  <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label19" runat="server" Text='<%$ Resources:LabelCaption,lbl_Address2 %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control-res" Height="50px"
                                            TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label20" runat="server" Text='<%$ Resources:LabelCaption,lbl_Address3 %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>--%>
                        <div class="control-group-single">
                            <div class="label-left">
                                <asp:Label ID="Label28" runat="server" Font-Size="Smaller" Text="Mobile"></asp:Label>
                                <span
                                    class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control-res" Style="text-align: center" onkeypress="return isNumberKey(event)" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <%--<div class="control-group-single">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="GSTIN No"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtGSTIN" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>--%>
                        <div class="control-group-single">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Font-Size="Smaller" Text="SoNO" Visible="false"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlSoNo" runat="server" CssClass="form-control-res" AutoPostBack="true"
                                    Visible="false" OnSelectedIndexChanged="ddlSoNo_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-single">
                            <div class="control-group-right1">
                                <asp:LinkButton ID="btnSOSelect" runat="server" class="button-red" Style="display: none" Text="Sales Order"
                                    OnClientClick=" return fncLoadSaleOrder()"></asp:LinkButton>
                            </div>
                            <div class="label-left1">
                                <asp:LinkButton ID="btnReprint" runat="server" class="button-blue"
                                    Style="margin-left: 10px; display: none" Text="Re-Print" OnClick="btnReprint_Click"></asp:LinkButton>
                                <asp:Label ID="lblSONo" Style="margin-left: 10px; color: Teal; font-size: 15px" runat="server" Text=""></asp:Label>
                            </div>
                        </div>

                        <div class="col-md-12" style="padding: 3px;  border: 1px solid #d8d9d8; background-color: #f3f3f3; background-image: url('../images/ui-bg_diagonals-thick_18_b81900_40x40.png');">
                            <div class="col-lg-7" style="padding: 4px; display: none;">
                                <asp:Label ID="Label14" CssClass="label-invoice" runat="server" Text="Discount By % :"></asp:Label>
                            </div>
                            <div class="col-lg-5" style="padding: 4px; display: none;">
                                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control-res" Style="text-align: center" oninput="fncDiscountCalc(event)" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                            <div class="col-lg-7" style="padding: 4px;">
                                <asp:Label ID="Label17" CssClass="label-invoice" runat="server" Text="Discount :"></asp:Label>
                            </div>
                            <div class="col-lg-5" style="padding: 4px;">
                                <asp:TextBox ID="txtDiscount23" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center" oninput="fncDiscountCalc(event)" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="container-pers-middle-invoice">
                        <asp:UpdatePanel ID="UpdatePanelGrid" runat="Server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="control-group-split-invoice">
                                    <asp:GridView ID="grdItemDetail" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                        CssClass="pshro_GridDgn">
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                        <PagerStyle CssClass="pshro_text" />
                                        <Columns>
                                            <asp:BoundField DataField="InventoryCode" ItemStyle-CssClass="Itemcode" HeaderText="Item Code"></asp:BoundField>
                                            <asp:BoundField DataField="BatchNo" ItemStyle-CssClass="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                            <asp:BoundField DataField="Description" HeaderText="Item Name"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Quantity" ItemStyle-Width="100" ItemStyle-Height="100%">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity") %>' onkeypress="return isNumberKey(event)"
                                                        onfocusout="return fncPriceCalc(event)" CssClass="gridInvoice-textbox"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Price" ItemStyle-Width="100" ItemStyle-Height="100%">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtSellingPrice" runat="server" Text='<%# Eval("Price") %>' onkeypress="return isNumberKey(event)"
                                                        onfocusout="return fncPriceCalc(event)" CssClass="gridInvoice-textbox"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Amount" HeaderText="Amount" ItemStyle-CssClass="LAmount"></asp:BoundField>
                                            <asp:BoundField DataField="Discount" HeaderText="Discount" ItemStyle-CssClass="LDiscount"></asp:BoundField>
                                            <asp:BoundField DataField="BillDisc" HeaderText="BillDisc" ItemStyle-CssClass="LBillDisc"></asp:BoundField>
                                            <asp:BoundField DataField="Taxable" HeaderText="Taxable" ItemStyle-CssClass="LTaxable"></asp:BoundField>
                                            <asp:BoundField DataField="GST" HeaderText="GST%" ItemStyle-CssClass="LGST"></asp:BoundField>
                                            <asp:BoundField DataField="GSTAmount" HeaderText="GSTAmount" ItemStyle-CssClass="LGSTAmount"></asp:BoundField>
                                            <asp:BoundField DataField="NetAmount" HeaderText="NetAmount" ItemStyle-CssClass="LNetAmount"></asp:BoundField>
                                            <asp:BoundField DataField="BasicPrice" HeaderText="BasicPrice" ItemStyle-CssClass="hiddencol"
                                                HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="container-pers-detail">
                    <div class="col-lg-10">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-2" style="padding: 3px; display: none; border: 1px solid #d8d9d8; background-color: #f3f3f3; background-image: url('../images/ui-bg_diagonals-thick_18_b81900_40x40.png');">
                            <div class="col-lg-7" style="padding: 4px; display: none;">
                                <asp:Label ID="Label13" CssClass="label-invoice" runat="server" Text="Discount By % :"></asp:Label>
                            </div>
                            <div class="col-lg-5" style="padding: 4px; display: none;">
                                <asp:TextBox ID="txtDiscPercent" runat="server" CssClass="form-control-res" Style="text-align: center" oninput="fncDiscountCalc(event)" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                            <div class="col-lg-7" style="padding: 4px;">
                                <asp:Label ID="Label1" CssClass="label-invoice" runat="server" Text="Discount :"></asp:Label>
                            </div>
                            <div class="col-lg-5" style="padding: 4px;">
                                <asp:TextBox ID="txtDiscount1" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center" oninput="fncDiscountCalc(event)" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2" style="padding: 4px; border: 1px solid #d8d9d8; background-color: #f3f3f3; background-image: url('../images/ui-bg_diagonals-thick_18_b81900_40x40.png');">
                        <div class="control-group-single-res">
                            <div class="col-lg-6">
                                <asp:Label ID="Label15" runat="server" Style="text-align: left;" CssClass="label-invoice" Text="Sub Total :"></asp:Label>
                            </div>
                            <div class="col-lg-6">
                                <asp:Label ID="lblAmount" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center;"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="col-lg-6">
                                <asp:Label ID="Label5" runat="server" Style="text-align: left;" CssClass="label-invoice" Text="Discount :"></asp:Label>
                            </div>
                            <div class="col-lg-6">
                                <%--<asp:TextBox ID="lblDiscount" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>--%>
                                <asp:Label ID="lblDiscount" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center" oninput="fncDiscountCalc(event)" onkeypress="return isNumberKey(event)"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group-single-res" style="display: none;">
                            <div class="col-lg-6">
                                <asp:Label ID="Label8" runat="server" Style="text-align: left;" CssClass="label-invoice" Text="Item Disc:"></asp:Label>
                            </div>
                            <div class="col-lg-6">
                                <asp:Label ID="lblItemDiscount" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center;"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group-single-res" style="display: none;">
                            <div class="col-lg-6">
                                <asp:Label ID="Label16" runat="server" Style="text-align: left;" CssClass="label-invoice" Text="Total GST :"></asp:Label>
                            </div>
                            <div class="col-lg-6">
                                <asp:Label ID="lblGSTAmount" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center;"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group-single-res" style="display: none;">
                            <div class="col-lg-6">
                                <asp:Label ID="Label6" runat="server" Style="text-align: left;" CssClass="label-invoice" Text="Round Off:"></asp:Label>
                            </div>
                            <div class="col-lg-6">
                                <asp:Label ID="lblRoundOff" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center;"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="col-lg-6">
                                <asp:Label ID="Label3" runat="server" CssClass="label-invoice" Style="text-align: left;" Text="Net Amount :"></asp:Label>
                            </div>
                            <div class="col-lg-6">
                                <asp:Label ID="lblNetAmount" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center;"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="col-lg-6">
                                <asp:Label ID="Label19" runat="server" CssClass="label-invoice" Style="text-align: left;" Text="Advance :"></asp:Label>
                            </div>
                            <div class="col-lg-6">
                                <asp:Label ID="lblAdvance" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center;"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="col-lg-6">
                                <asp:Label ID="Label4" runat="server" CssClass="label-invoice" Style="text-align: left;" Text="Balance :"></asp:Label>
                            </div>
                            <div class="col-lg-6">
                                <asp:Label ID="lblBalanceAmt" runat="server" Text="0.00" CssClass="form-control-res" Style="text-align: center;"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:UpdatePanel ID="UpdatePanelSave" runat="Server">
            <ContentTemplate>
                <div class="control-container">
                    <%-- <div class="control-button">
                        <asp:LinkButton ID="lnkMemberAdjust" runat="server" class="button-blue" Text="Edit"></asp:LinkButton>
                    </div>--%>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkView" runat="server" class="button-blue" Visible="false" Text='<%$ Resources:LabelCaption,btn_View %>'
                            OnClick="lnkView_Click"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnSave %>'
                            OnClientClick="return fncSaveValidation()" OnClick="lnkSave_Click"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return fncClearForm()"
                            Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanelSave">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <asp:HiddenField ID="HiddenNewitem" runat="server" ClientIDMode="Static" Value="NO" />
        <asp:HiddenField ID="HiddenSubtotal" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenDiscount" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenItemDisc" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenRoundoff" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenGST" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenMemberCode" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenNetamount" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenXmldata" runat="server" ClientIDMode="Static" Value="NO" />
        <asp:HiddenField ID="HidenIsBatch" runat="server" ClientIDMode="Static" Value="False" />
        <asp:HiddenField ID="HiddenItemCode" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="HiddenItemName" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="HiddenGSTperc" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenSONo" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenPONo" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="HiddenSerialNo" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="HiddenBalanceAmt" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenAdvanceAmt" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="HiddenDeliveryDate" runat="server" ClientIDMode="Static" Value="0" />

    </div>

</asp:Content>
