﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmInvoiceTracker.aspx.cs" Inherits="EnterpriserWebFinal.Sales.frmInvoiceTracker" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">

    </style>
    <script type="text/javascript">
        function pageLoad() {
            
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "-1");
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {

                    $("#<%=txtInventory.ClientID %>").val(Code);



                }
                if (SearchTableName == "Member") {
                    $("#<%=txtCustomer.ClientID %>").val(Description);
                }
                

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncClear() {
            $("#<%=txtCustomer.ClientID %>").val("");
            $("#<%=txtInventory.ClientID %>").val("");
            $("#<%=txtInvoice.ClientID %>").val("");
            
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
            </li>
          <li class="active-page">InvoiceTracker</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
     <div class="container-group-small">
        <div class="container-control">                
            <div class="control-group-single-res">
          
            
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblInvoice" runat="server" Text="InvoiceNo"></asp:Label>
                </div>
                <div class="label-right">
                        <asp:TextBox ID="txtInvoice" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                </div>
                <div class="label-right">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                </div>
                <div class="label-right">
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblItem" runat="server" Text="ItemCode"></asp:Label>
                </div>
                <div class="label-right">
                        <asp:TextBox ID="txtInventory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory', 'txtInventory', '');"></asp:TextBox>
                             
                                                    </div>

            </div>
              <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblCustomer" runat="server" Text="CustomerName"></asp:Label>
                </div>
                <div class="label-right">
                        <asp:TextBox ID="txtCustomer" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Member', 'txtCustomer', '');"></asp:TextBox>
                             
                                                    </div>

            </div>
        

        </div>
         <div class="button-contol">
             <div class="control-button">
                 <asp:LinkButton ID="lnkLoad" runat="server" class="button-green" OnClick="lnkLoad_Click"><i class="icon-play"></i>Load</asp:LinkButton>
                </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="fncClear();return false;"><i class="icon-play"></i>Clear</asp:LinkButton>
                        </div>
                       
                    </div>
         </div>
      <div class="container-group-top">
                    <div class="col-md-12" style="overflow: auto; height: 550px;">
                        <div id="divgvPickedItemDetailss" >
                            <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" CssClass="pshro_GridDgn" ShowHeaderWhenEmpty="true"   AllowPaging="false" OnRowUpdating="gv1_RowUpdating">
                               
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label1" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <RowStyle Height="50px" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <AlternatingRowStyle Height="50px" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="SINo" runat="server" Text="SINo"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="SINo" runat="server" Text='<%# Eval("SINo") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                  
                                                                       
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <ItemTemplate>
                                            <asp:Button ID="Button3" runat="server" CssClass="btn btn-primary waves-effect" Text="View" CommandName="Update" />

                                        </ItemTemplate>
                            </asp:TemplateField>

                                     <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="InvoiceNo" runat="server" Text="InvoiceNo"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="InvoiceNo" runat="server" Text='<%# Eval("InvoiceNo") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="Invoicedate" runat="server" Text="Invoicedate"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Invoicedate" runat="server" Text='<%# Eval("Invoicedate") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="SoldQty" runat="server" Text="SoldQty"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="SoldQty" runat="server" Text='<%# Eval("SoldQty") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                     <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="BillAmount" runat="server" Text="BillAmount"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="BillAmount" runat="server" Text='<%# Eval("BillAmount") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                       <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="CustomerName" runat="server" Text="CustomerName"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="CustomerName" runat="server" Text='<%# Eval("CustomerName") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  

                                      <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="Discount" runat="server" Text="Discount"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Discount" runat="server" Text='<%# Eval("Discount") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="Tax" runat="server" Text="Tax"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Tax" runat="server" Text='<%# Eval("Tax") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                   
                                  
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                                </div>
</asp:Content>
