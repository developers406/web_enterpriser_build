﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmCustomerReceipts.aspx.cs" Inherits="EnterpriserWebFinal.Sales.frmCustomerReceipts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        

    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

       

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 65px;
            max-width: 65px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 95px;
            max-width: 95px;
            text-align: left;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 115px;
            max-width: 115px;
            text-align: left;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 375px;
            max-width: 375px;
            text-align: left;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
            text-align: right;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 105px;
            max-width: 105px;
            text-align: left;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 130px;
            max-width: 130px;
            text-align: left;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 150px;
            max-width: 150px;
            text-align: left;
        }

        .grdLoad td, th {
            padding: 2px;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'CustomerReceipt');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "CustomerReceipt";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            var sSplit = $("#<%= txtFromDate.ClientID %>").val().split('/');
            $("#<%= txtFromDate.ClientID %>").val('01' + '/' + sSplit[1] + '/' + sSplit[2]);
        });
    </script>
    <script type="text/javascript">
        function pageLoad() {
            $("select").chosen({ width: '100%' });
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            var sSplit = $("#<%= txtFromDate.ClientID %>").val().split('/');
            $("#<%= txtFromDate.ClientID %>").val('01' + '/' + sSplit[1] + '/' + sSplit[2]);
            if ($("#<%= ChkPending.ClientID %>").is(':checked')) {
                <%--$("#<%= txtFromDate.ClientID %>").prop("disabled", true); 
                $("#<%= txtToDate.ClientID %>").prop("disabled", true); --%> 
                $("#<%= ddlPaymode.ClientID %>").prop('Enabled', true).trigger("liszt:updated");
            }
            $("#<%=ddlPaymode.ClientID %>").change(function () {
                   $("#<%=btnchngloc.ClientID %>").click();
               });
        }

    </script>
    <script type="text/javascript">
        function fncOpenFrame(sCustomerCode, sReceiptNo) {
            var page = '<%=ResolveUrl("~/Sales/frmCustomerReceiptEntry.aspx") %>';
            var page = page + "?CustomerCode=" + sCustomerCode + "&ReceiptNo=" + sReceiptNo
            window.location.href = page;
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "CUSTOMER") {
                    $('#<%=hdfCustomerCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtCustomerName.ClientID %>').val($.trim(Description));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncClear() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
        }
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>

                <li><a href="">Sales</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Customer Receipts </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="customer-receipts-header">
                        <div class="customer-receipts-header-left">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label10" runat="server" Text="Customer Name"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtCustomerName" runat="server" CssClass="form-control-res"   onkeydown="return fncShowSearchDialogCommon(event, 'CUSTOMER', 'txtCustomerName', '');  "> </asp:TextBox>
                                        <asp:HiddenField ID="hdfCustomerCode" runat="server" />
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label11" runat="server" Text="Location"></asp:Label>
                                    </div>
                                    <div class="chzn-single chzn-default chzn-single-with-drop"  >
                                        <asp:DropDownList ID="ddlPaymode" runat="server" CssClass="form-control-res" style="width:194px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                 
                            </div>
                             

                        </div>
                        <div class="customer-receipts-header-right">
                            <div class="control-group-split">
                                <div class="control-group-left1">
                                    <div class="label-left">
                                        <asp:Label ID="Label2" runat="server" Text="Paymode" Style="visibility: hidden;"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:CheckBox ID="ChkPending" runat="server" Checked="true" OnCheckedChanged="ChkPending_CheckedChanged"
                                            AutoPostBack="true" />
                                        <asp:Label ID="Label1" runat="server" Text="Pending"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group-middle">
                                    <div class="label-left">
                                        <asp:Label ID="Label91" runat="server" Text="From Date"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right1">
                                    <div class="label-left">
                                        <asp:Label ID="Label92" runat="server" Text="To Date"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="customer-receipts-detail">
                        <div class="row">
                            <%--<div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                            <div class="grdLoad"  style="overflow: auto; height: 425px;">
                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col">Receipt No
                                            </th>
                                            <th scope="col">Receipt Date
                                            </th>
                                            <th scope="col">Customer Code
                                            </th>
                                            <th scope="col">Customer Name
                                            </th>
                                            <th scope="col">Amount
                                            </th>
                                            <th scope="col">Paymode
                                            </th>
                                            <th scope="col">Verified By
                                            </th>
                                            <th scope="col">Created By
                                            </th>
                                            <th scope="col">Created On
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="GridDetails">
                                    <asp:GridView ID="gvCustomerReceipts" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                        ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" OnSelectedIndexChanged="gvCustomerReceipts_SelectedIndexChanged"
                                        DataKeyNames="CustomerCode, ReceiptNo">
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                        <PagerStyle CssClass="pshro_text" />
                                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="true" ButtonType="Button" SelectText="Select" />
                                            <asp:BoundField DataField="ReceiptNo" HeaderText="Receipt No"></asp:BoundField>
                                            <asp:BoundField DataField="ReceiptDate" HeaderText="Receipt Date"></asp:BoundField>
                                            <asp:BoundField DataField="CustomerCode" HeaderText="Customer Code"></asp:BoundField>
                                            <asp:BoundField DataField="MemberName" HeaderText="Customer Name"></asp:BoundField>
                                            <asp:BoundField DataField="TotalDueAmount" HeaderText="Amount"></asp:BoundField>
                                            <asp:BoundField DataField="Paymode" HeaderText="Paymode"></asp:BoundField>
                                            <asp:BoundField DataField="Cashier" HeaderText="Verified By"></asp:BoundField>
                                            <asp:BoundField DataField="CreateUser" HeaderText="Created By"></asp:BoundField>
                                            <asp:BoundField DataField="Createdate" HeaderText="Created On"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-container">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkRefresh" runat="server" class="button-red" Text="Refresh"
                                OnClick="lnkRefresh_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button" style="display: none">
                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-red" Text="Print"></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="fncClear();return false;" class="button-red" Text="Clear"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="hiddencol">
            <asp:Button ID="btnchngloc" runat="server" onclick="ChkPending_CheckedChanged"/>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
