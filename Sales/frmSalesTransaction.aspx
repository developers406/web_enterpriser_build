﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmSalesTransaction.aspx.cs" Inherits="EnterpriserWebFinal.Sales.frmSalesTransaction" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

          .nav-tabs > li > a
        {
            color: White;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'SalesTransaction');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "SalesTransaction";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function pageLoad() {
              if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                  $('#<%=lnk_Preview.ClientID %>').css("display", "block"); 
                  $('#<%=lnkPrint.ClientID %>').css("display", "block");
             }
             else {
                  $('#<%=lnk_Preview.ClientID %>').css("display", "none"); 
                   $('#<%=lnkPrint.ClientID %>').css("display", "none"); 
             }
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc  
            //============================> For Maintain Current Tab while postback

            $("#<%=txtLocation.ClientID %>, #<%=txtLocation.ClientID %>_chzn, #<%=txtLocation.ClientID %>_chzn > div, #<%=txtLocation.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=txtPaymode.ClientID %>, #<%=txtPaymode.ClientID %>_chzn, #<%=txtPaymode.ClientID %>_chzn > div, #<%=txtPaymode.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=txtCashier.ClientID %>, #<%=txtCashier.ClientID %>_chzn, #<%=txtCashier.ClientID %>_chzn > div, #<%=txtCashier.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=txtTerminal.ClientID %>, #<%=txtTerminal.ClientID %>_chzn, #<%=txtTerminal.ClientID %>_chzn > div, #<%=txtTerminal.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=txtSettleLocation.ClientID %>, #<%=txtSettleLocation.ClientID %>_chzn, #<%=txtSettleLocation.ClientID %>_chzn > div, #<%=txtSettleLocation.ClientID %>_chzn > div > div > input").css("width", '100%');

        }

        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "SalesHistory";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs ul li a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
            <%--$("#<%=gridSalesDetails.ClientID%> tr").click(function () {
                DisplayDetails($(this).closest("tr"));
            });--%>
        });

       <%-- function DisplayDetails(row) {
            $("#<%= hidInvoiceNum.ClientID%>").val($("td", row).eq(0).html());
            //var url = "../Reports/View.aspx?ReportFilter=" + $("td", row).eq(0).html();
            //window.location.href = url;
            __doPostBack('ctl00$ContentPlaceHolder1$lnkReport', '');
        }--%>
    </script>
    <script type="text/javascript">
        $(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtSettlementDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtCFromdate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtCTodate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtSettlementDate.ClientID %>").val() === '') {
                $("#<%= txtSettlementDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtCFromdate.ClientID %>").val() === '') {
                $("#<%= txtCFromdate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtCTodate.ClientID %>").val() === '') {
                $("#<%= txtCTodate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

        });

    </script>
    <%------------------------------------------- Clear Form -------------------------------------------%>
    <script type="text/javascript">
       
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemName.ClientID %>').val($.trim(Description));
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncClear() {             
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("[id*=gridSalesDetails] tr").remove();
            $('#<%=rdoCashSales.ClientID %>').prop('checked', true);
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Sales</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Sales Transaction </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full">
            <div class="panel panel-default">
                <div id="Tabs" role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs custnav-invoice custnav-cm-trans" role="tablist">
                        <li><a href="#SalesHistory" aria-controls="SalesHistory" role="tab" data-toggle="tab">Sales History</a></li>
                        <li><a href="#SettlementDetails" aria-controls="SettlementDetails" role="tab" data-toggle="tab">Settlement Details</a></li>
                        <li><a href="#ConsolidatedSettlement" aria-controls="ConsolidatedSettlement" role="tab"
                            data-toggle="tab">Consolidated Settlement</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane" role="tabpanel" id="SalesHistory">
                            <div class="divSalesDetails" style="overflow: auto; height: 520px">
                                <div class="container-group-price">
                                    <div class="container-left-price" id="pnlFilter" runat="server">
                                        <div class="barcode-header">
                                            Search By
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label6" runat="server" Text="Location"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>--%>
                                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtInvoiceNo');"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label3" runat="server" Text="Bill No"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label4" runat="server" Text="Paymode"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <%--<asp:DropDownList ID="ddlPaymode" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>--%>
                                                <asp:TextBox ID="txtPaymode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Paymode', 'txtPaymode', '');"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label5" runat="server" Text="Cashier"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <%--<asp:DropDownList ID="ddlCashier" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>--%>
                                                <asp:TextBox ID="txtCashier" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Cashier', 'txtCashier', '');"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Labelsc" runat="server" Text="Terminal"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <%--<asp:DropDownList ID="ddlTerminal" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>--%>
                                                <asp:TextBox ID="txtTerminal" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Terminal', 'txtTerminal', '');"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label15" runat="server" Text="Customer Code"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtCustomerCode" runat="server" CssClass="form-control-res"  onkeydown="return fncShowSearchDialogCommon(event, 'Customer', 'txtCustomerCode', '');"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res" style="display:none">
                                            <div class="label-left">
                                                <asp:Label ID="Label16" runat="server" Text="Item Name"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtItemName" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemName', '');" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label1" runat="server" Text="Minimum Amt"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtMinimumAmt" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label7" runat="server" Text="Maximum Amt"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtMaximumAmt" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="barcode-header">
                                                Filterations
                                            </div>
                                        </div>
                                        <div class="control-group-single-res" style="margin-top: 10px">
                                            <div class="label-left">
                                                <asp:Label ID="Lblfrod" runat="server" Text="FromDate"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="lblto" runat="server" Text="ToDate"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res" style="margin-top: 10px">
                                            <div class="label-left">
                                                <asp:RadioButton ID="rdoCashSales" ForeColor="Green" Text="CASH SALES" runat="server"
                                                    GroupName="RegularMenu" Checked="True" />
                                            </div>
                                            <div class="label-right">
                                                <asp:RadioButton ID="rdoCrediSales" ForeColor="Green" Text="CREDIT SALES" runat="server"
                                                    GroupName="RegularMenu" />
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:RadioButton ID="rdoCancelBill" ForeColor="red" Text="CANCEL BILL" runat="server"
                                                    GroupName="RegularMenu" />
                                            </div>
                                            <div class="label-right">
                                                <asp:RadioButton ID="rdoExchangeBill" ForeColor="red" Text="EXCHANGE BILL" runat="server"
                                                    GroupName="RegularMenu" />
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:RadioButton ID="rdoDiscBill" ForeColor="Green" Text="DISCOUNT BILL" runat="server"
                                                    GroupName="RegularMenu" />
                                            </div>
                                            <div class="label-right">
                                                <asp:RadioButton ID="rdoSalesReturn" ForeColor="Green" Text="SALES RETURN BILL" runat="server"
                                                    GroupName="RegularMenu" />
                                            </div>
                                        </div>
                                        <div class="control-container">
                                            <div class="control-button">
                                                <asp:LinkButton ID="lnkLoadData" runat="server" class="button-red" OnClick="btnLoad_click"
                                                    Text='<%$ Resources:LabelCaption,btn_fetch %>'><i class="icon-play" ></i></asp:LinkButton>
                                            </div>
                                            <div class="control-button">
                                                <asp:LinkButton ID="LinkButton1" runat="server" class="button-red" OnClientClick ="fncClear();return false;" Text='<%$ Resources:LabelCaption,btnClear %>'><i class="icon-play"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                                        style="overflow: auto; height: 520px">
                                        <div class="gridDetails" style="overflow: auto; height: 520px">
                                            <asp:GridView ID="gridSalesDetails" runat="server" Width="100%" AutoGenerateColumns="true"
                                                CssClass="pshro_GridDgn">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Print">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnPrint" runat="server" ImageUrl="~/images/print1.png"
                                                                ToolTip="Click here to Print" OnClick="lnkReport_Click" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings FirstPageText="First" LastPageText="Last" PageButtonCount="5" Position="Bottom" />
                                                <PagerStyle Height="30px" VerticalAlign="Bottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" role="tabpanel" id="SettlementDetails">
                            <div class="container-pers-top-header-invoice">
                                <div class="container-pers-header-left">
                                    <div class="control-group-split">
                                        <div class="control-group-left">
                                            <div class="label-left">
                                                <asp:Label ID="Label29" runat="server" Text="Settlement Date" ForeColor="White" />
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtSettlementDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-right">
                                            <div class="label-left">
                                                <asp:Label ID="Label8" runat="server" Text="Location" ForeColor="White" />
                                            </div>
                                            <div class="label-right">
                                                <%--<asp:DropDownList ID="ddlSettleLocation" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>--%>
                                                <asp:TextBox ID="txtSettleLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtSettleLocation', '');"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-pers-header-right">
                                    <div class="control-group-split">
                                        <div class="control-group-left">
                                            <div class="label-right" style="width: 21% !important">
                                                <asp:LinkButton ID="lnk_FetchSettlement" runat="server" Text="Fetch" class="button-blue"
                                                    OnClick="btnFetchSettlement_click" />
                                            </div>
                                            <div class="label-left">
                                                <asp:LinkButton ID="lnkPrint" runat="server" Text="Print" class="button-blue" OnClick="lnkPrint_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container-pers-top">
                                <div class="container-other-bottom-left">
                                    <div class="gridDetails" style="overflow: auto; height: 470px">
                                        <asp:GridView ID="grdSettlementHeader" runat="server" AutoGenerateColumns="false"
                                            ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                            OnRowDataBound="grdSettlementHeader_RowDataBound" OnDataBound="grdSettlementHeader_OnDataBound"
                                            CssClass="pshro_GridDgn" OnRowCommand="grdSettlementHeader_RowCommand">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:BoundField DataField="Terminalcode" HeaderText="Terminal"></asp:BoundField>
                                                <asp:BoundField DataField="ComputerAmount" HeaderText="Computer Amount"></asp:BoundField>
                                                <asp:BoundField DataField="ManualAmount" HeaderText="Manual Amount"></asp:BoundField>
                                                <asp:BoundField DataField="SortOrExces" HeaderText="ShortOrExces"></asp:BoundField>
                                                <asp:BoundField DataField="SettlementCount" HeaderText="Settlement Count"></asp:BoundField>
                                                <asp:ButtonField ButtonType="Button" HeaderText="View" Text="Detail" CommandName="Detail" />
                                                <%-- <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />--%>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="container-other-bottom-right">
                                    <div class="gridDetails" style="overflow: auto; height: 470px">
                                        <asp:GridView ID="grdSettlementDetail" runat="server" AutoGenerateColumns="false"
                                            OnDataBound="OnDataBound" ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe"
                                            AlternatingRowStyle-BackColor="#c4ddff" CssClass="pshro_GridDgn">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:BoundField DataField="Cashier" HeaderText="Cashier"></asp:BoundField>
                                                <asp:BoundField DataField="Paymode" HeaderText="Computer Amount"></asp:BoundField>
                                                <asp:BoundField DataField="ComputerAmount" HeaderText="Computer Amount"></asp:BoundField>
                                                <asp:BoundField DataField="ManualAmount" HeaderText="Manual Amount"></asp:BoundField>
                                                <asp:BoundField DataField="SortOrExces" HeaderText="ShortOrExces"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" role="tabpanel" id="ConsolidatedSettlement">
                            <div class="container-pers-top">
                                <div class="container-pers-top-header">
                                    <div class="container-pers-header-left">
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label9" runat="server" Text="From Date" ForeColor="White" />
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtCFromdate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label10" runat="server" Text="To Date" ForeColor="White" />
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtCTodate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-pers-header-right">
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-right">
                                                    <asp:LinkButton ID="btnConsolidated" runat="server" Text="Fetch" class="button-blue"
                                                        OnClick="btnFetchConsolidated_click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gridDetails" style="overflow: auto; height: 395px">
                                    <asp:GridView ID="grdConsolidated" runat="server" AutoGenerateColumns="true" ShowHeaderWhenEmpty="true"
                                        OnDataBound="grdConsolidated_OnDataBound" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                        CssClass="pshro_GridDgn">
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                        <PagerStyle CssClass="pshro_text" />
                                        <Columns>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="control-container">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnk_Preview" runat="server" class="button-red" Text="Preview" OnClick="lnk_Preview_Click"></asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkMail" runat="server" class="button-red" Text="Mail" style="display:none"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="TabName" runat="server" />
            </div>
        </div>
        <%--   <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>--%>

        <div class="display_none">
            <asp:LinkButton ID="lnkReport" runat="server" ></asp:LinkButton>
            <asp:HiddenField ID="hidInvoiceNum" runat="server" />
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
        </div>
    </div>
</asp:Content>
