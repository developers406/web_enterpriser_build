﻿var row;
var object = "";
var canvas_id = "";
var div_id = "";
var imagebase64list = "";
var chunk = [];
var i = 1;

function ObjectList(List, listname, ddl, Name, TemplateList, HideMRP, HidePrice, HidePkdDate, HideExpiredate, HideCompany, BarcodeStatus) {
    alert
    var imglist = [];
    if (List.length > 0) {
        jQuery('#header_div').html('');
        $.each(List[0], function (key, valus) {
            
            var jsondata = listofarray(listname, JSON.parse(TemplateList), valus.Barcode, valus.Inventorycode, valus.Description, valus.SellingPrice, valus.MRP, valus.Discount, valus.VendorCode, valus.VendorName, valus.PritingCount, valus.PKDDate, valus.EXPDate, valus.SNO, valus.CompanyName, Name, key, HideMRP, HidePrice, HidePkdDate, HideExpiredate, HideCompany, BarcodeStatus);
            
            var ele = '#' + jsondata;
            html2canvas($(ele), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL("image/png", 1.0);
                    imglist.push({ base64: img, Tcount: valus.PritingCount, Divid: ele });
                }
            });
        });
    }
    return imglist;
}
function printerimagelist(imglist, CrossWidth, CrossHeight, PaperRoleWidth, FileName, CrossCount, PrinterStatus, PrintName,FormName) {
    
    var _objlist = [];
    var data = [];
    var paperrole = PaperRoleWidth * 2;
    var elem = document.getElementById('firstone');
    elem.style.width = paperrole + "cm";
    var elem1 = document.getElementById('firstone1');
    elem1.style.width = paperrole + "cm";
    $.each(imglist, function (key, valus) {
        $(valus.Divid).remove();
        _objlist.push({ TemplateImg: valus.base64, CrossWidth: CrossWidth, CrossHeight: CrossHeight, PaperRoleWidth: PaperRoleWidth, FileName: FileName, CrossCount: CrossCount, TotalCount: valus.Tcount });
    });
    $.each(_objlist, function (key, datalist) {
        for (var i = 0; i < datalist.TotalCount; i++) {
            data.push({
                TemplateImg: datalist.TemplateImg,
                CrossWidth: datalist.CrossWidth,
                CrossHeight: datalist.CrossHeight,
                PaperRoleWidth: datalist.PaperRoleWidth,
                FileName: datalist.FileName
            });
        }
    });
    images = "";
    while (data.length > 0) {
        var div_ids = "";
        var div = document.createElement("DIV");
        div.id = "secandone" + i++;
        div.className = 'container_divlist2';
        document.getElementById("firstone1").appendChild(div);
        div_ids = div.id;
        var CrossCounts = parseInt(CrossCount)
        chunk = data.splice(0, CrossCounts);
        chunk.forEach(function (entry, index) {
            var oImg = document.createElement("img");
            var height = entry.CrossHeight * 2;
            var width = entry.CrossWidth * 2;
            oImg.setAttribute('src', entry.TemplateImg);
            oImg.className = 'fake_img';
            oImg.id = 'Id_Imag'
            oImg.style.height = height + "cm";
            oImg.style.width = width + "cm";
            oImg.style["margin-top"] = "-15px";
            document.getElementById(div_ids).appendChild(oImg);
        });
        printer(div_ids, PrinterStatus, PrintName, FormName);
    }
}
var status = 'Qrcode'
function listofarray(listname, object_Templates, Barcode, Inventorycode, Description, SellingPrice, MRP, Discount, VendorCode, VendorName, PritingCount, PKDDate, EXPDate, SNo, Company, listout, idcount, HideMRP, HidePrice, HidePkdDate, HideExpiredate, HideCompany, Barcode_Status) {
    
    try {
        canvas_id = "";
        div_id = "";
        jsondata = object_Templates;
        $.each(listout, function (key, values) {
            $.each(listname, function (key, names) {
                if (names == values) {
                    var locallist = '#' + names;
                    if (key == 0) {
                        findTextAndReplace(jsondata, locallist, PritingCount.replace(/\s/g, ''));
                    }
                    else if (key == 1) {
                        //var qrcode = ImageAndReplaceQRCode(Barcode.replace(/\s/g, ''));                                     
                        findTextAndReplace(jsondata, locallist, Barcode.replace(/\s/g, ''));
                        if (Barcode_Status == 'Barcode') {
                            findImageAndReplace(jsondata, imagebase64list, ImageAndReplace(Barcode.replace(/\s/g, '')));
                        }
                        else {
                            findImageQRAndReplace(jsondata, imagebase64list, QR_ImageAndReplaceQRCode(Barcode.replace(/\s/g, '')));
                        }
                    }
                    else if (key == 2) {
                        findTextAndReplace(jsondata, locallist, Inventorycode.replace(/\s/g, ''));
                    }
                    else if (key == 3) {
                        findTextAndReplace(jsondata, locallist, Description.replace(/\s/g, ''));
                    }
                    else if (key == 4) {
                        debugger
                        if (HidePrice == "N") {
                            findTextAndReplace(jsondata, locallist, SellingPrice.replace(/\s/g, ''));
                        }
                        else {
                            findTextAndReplace(jsondata, "Rs", "".replace(/\s/g, ''));
                            findTextAndReplace(jsondata, "Rs.", "".replace(/\s/g, ''));
                            findTextAndReplace(jsondata, "₹", "".replace(/\s/g, ''));
                            findTextAndReplace(jsondata, locallist, "".replace(/\s/g, ''));
                        }
                    }
                    else if (key == 5) {
                        debugger
                        if (HideMRP == "N") {
                            findTextAndReplace(jsondata, locallist, MRP.replace(/\s/g, ''));
                        }
                        else {                            
                            findTextAndReplace(jsondata, "MRP Rs", "".replace(/\s/g, ''));
                            findTextAndReplace(jsondata, "MRP", "".replace(/\s/g, ''));
                            findTextAndReplace(jsondata, "₹", "".replace(/\s/g, ''));
                            findTextAndReplace(jsondata, locallist, "".replace(/\s/g, ''));
                        }
                    }
                    else if (key == 6) {
                        
                        if (HidePkdDate == "N") {
                            findTextAndReplace(jsondata, locallist, PKDDate.replace(/\s/g, ''));
                        }
                        else {
                            findTextAndReplace(jsondata, "P.Date", "".replace(/\s/g, ''));
                            findTextAndReplace(jsondata, "Pkd Date", "".replace(/\s/g, ''));
                            findTextAndReplace(jsondata, locallist, "".replace(/\s/g, ''));
                        }
                    }
                    else if (key == 7) {
                        
                        if (HideExpiredate == "N") {
                            findTextAndReplace(jsondata, locallist, EXPDate.replace(/\s/g, ''));
                        }
                        else {
                            findTextAndReplace(jsondata, "Exp Date", "".replace(/\s/g, ''));
                            findTextAndReplace(jsondata, "E.Date", "".replace(/\s/g, ''));
                            findTextAndReplace(jsondata, locallist, "".replace(/\s/g, ''));
                        }
                    }
                    else if (key == 8) {
                        findTextAndReplace(jsondata, locallist, Discount.replace(/\s/g, ''));
                    }
                    else if (key == 9) {
                        if (HideCompany == "N") {
                            findTextAndReplace(jsondata, locallist, Company);
                        }
                        else {
                            findTextAndReplace(jsondata, locallist, "".replace(/\s/g, ''));
                        }
                    }
                    else if (key == 10) {
                        findTextAndReplace(jsondata, locallist, VendorCode.replace(/\s/g, ''));
                    }
                    else if (key == 11) {
                        findTextAndReplace(jsondata, locallist, VendorName);
                    }
                    else if (key == 12) {
                        findTextAndReplace(jsondata, locallist, SNo.replace(/\s/g, ''));
                    }
                }
            });
        });
        var Div = document.createElement('div');
        Div.id = 'container-first' + idcount;
        Div.className = 'canvas-containerlist';
        document.getElementsByTagName('body')[0].appendChild(Div);
        div_id = 'container-first' + idcount;
        var canvasnew = document.createElement('canvas');
        canvasnew.id = "c" + idcount;
        canvasnew.width = 750;
        canvasnew.height = 500;
        document.getElementById(div_id).appendChild(canvasnew);
        canvas_id = "c" + idcount;
        var canvas = new fabric.Canvas(canvas_id);
        var jsonCanvas = JSON.stringify(jsondata);
        canvas.loadFromJSON(jsonCanvas, canvas.renderAll.bind(canvas));
        document.getElementById("header_div").appendChild(Div);
        return div_id;
    }
    catch (err) {
        fncToastError(err.message);
    }
}
function printer(div_ids, PrinterStatus, PrintName,FormName) {
    var urls = "";  
    if (PrinterStatus == "PS_Local") {
        urls = "Barcode.aspx/GetPrintBarcodeDetails";
    }
    else if (PrinterStatus == "PS_Server") {
        if (FormName == "GRN") {
            urls = "GoodsAcknowledgmentNote.aspx/SaveBarcodePrint";
        } else {
            urls = "Barcode.aspx/SaveBarcodePrint";
        }
    }
    var elements = "#" + div_ids;
    html2canvas($(elements), {
        onrendered: function (canvas) {
            var img = canvas.toDataURL("image/png", 1.0);
            var base64result = img.split(',')[1];
            var jobName = "label";
            var zpl = PrintName;
            $.ajax({
                type: "POST",
                url: urls,
                data: "{ 'printerName': '" + zpl + "' , 'base64Image' : '" + base64result + "', 'jobName' : '" + jobName + "'}",
                async: false,
                proccessData: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                cache: false,
                success: function (data) {
                    if (data.d == true) {
                        var id = "#" + div_ids;
                        $("#" + div_ids).remove();
                        if(FormName == "GRN")
                        {

                        }
                        else if( FormName == "BC")
                        {
                        }
                        else if (FormName == "DB")
                        {
                            //var d = "HQ1000000081";
                            //var ff = "/Inventory/frmDistribution.aspx?DistributionNo=" + d + "";
                            //alert(ff);
                            //window.location.href = ff;
                        }
                    }
                },
                error: function (jqXHR, exception) {
                    getErrorMessage(jqXHR, exception);
                }
            });
        }
    });
}
function findImageAndReplace(object, value, replacevalue) {
    for (var x in object) {
        if (typeof object[x] == typeof {}) {
            findImageAndReplace(object[x], value, replacevalue);
        }
        if (object[x] == value) {
            var obj_value = object["src"];
            if (obj_value != undefined) {
                const type = obj_value.split(';')[0].split('/')[1];
                if (type == "png") {
                    object["src"] = replacevalue;
                }
            }
        }
    }
}
function findImageQRAndReplace(object, value, replacevalue) {
    for (var x in object) {
        if (typeof object[x] == typeof {}) {
            findImageQRAndReplace(object[x], value, replacevalue);
        }
        if (object[x] == value) {
            object["src"] = replacevalue;
        }
    }
}
function findTextAndReplace(object, value, replacevalue) {
    for (var x in object) {
        if (typeof object[x] == typeof {}) {
            findTextAndReplace(object[x], value, replacevalue);
        }
        if (object[x] == value) {
            object["text"] = replacevalue;
        }
    }
}
function ImageAndReplace(value) {
    var iDiv = document.createElement('div');
    iDiv.id = 'block';
    iDiv.className = 'block';
    iDiv.style = "display:none";
    document.getElementsByTagName('body')[0].appendChild(iDiv);
    var base64 = []
    var img = document.createElement("img");
    img.id = "rlp_imgbarcode"
    img.style = "display:none";
    document.getElementById("block").appendChild(img);
    JsBarcode("#rlp_imgbarcode", value, { format: "CODE128", lineColor: "#000000", width: 2, height: 50, fontSize: 75, displayValue: false, fontWeight: 'bold', background: "#FFFFFF" });
    base64 = $('#rlp_imgbarcode')[0].src;
    $('#rlp_imgbarcode').remove();
    return base64;
}

function QR_ImageAndReplaceQRCode(values) {
    var base64 = [];
    var codecanvas = document.getElementById('qr-code'),
 qr = new QRious({
     element: codecanvas,
     size: 150,
     value: values
 });
    codecanvas.style.display = "none";
    dataUrl = codecanvas.toDataURL(),
         imageFoo = document.createElement('img');
    imageFoo.src = dataUrl;
    base64 = imageFoo.src;
    return base64;
}