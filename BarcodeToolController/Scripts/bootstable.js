/*
Bootstable
 @description  Javascript library to make HMTL tables editable, using Bootstrap
 @version 1.1
 @autor Tito Hinostroza
*/
  "use strict";
  //Global variables
  var params = null;  		//Parameters
  var colsEdi =null;
  var newColHtml = '<div class="btn-group pull-right">'+
'<button id="bEdit" type="button" class="btn btn-sm btn-default"  onclick="rowEdit(this);">' +
'<i class="glyphicon glyphicon-pencil"></i>'+
'</button>'+
'<button id="bElim" type="button" class="btn btn-sm btn-default"  onclick="rowElim(this);">' +
'<i class="glyphicon glyphicon-trash" aria-hidden="true"></i>'+
'</button>'+
'<button id="bAcep" type="button" class="btn btn-sm btn-default"  style="display:none;" onclick="rowAcep(this);">' + 
'<i class="glyphicon glyphicon-ok"></i>'+
'</button>'+
'<button id="bCanc" type="button" class="btn btn-sm btn-default" style="display:none;"  onclick="rowCancel(this);">' + 
'<i class="glyphicon glyphicon-remove" aria-hidden="true"></i>'+
'</button>'+
    '</div>';

     var saveColHtml = '<div class="btn-group pull-right">'+
'<button id="bEdit" type="button" class="btn btn-sm btn-default" style="display:none;" onclick="rowEdit(this);">' +
'<i class="glyphicon glyphicon-pencil"></i>'+
'</button>'+
'<button id="bElim" type="button" class="btn btn-sm btn-default" style="display:none;" onclick="rowElim(this);">' +
'<i class="glyphicon glyphicon-trash" aria-hidden="true"></i>'+
'</button>'+
'<button id="bAcep" type="button" class="btn btn-sm btn-default"   onclick="rowAcep(this);">' + 
'<i class="glyphicon glyphicon-ok"></i>'+
'</button>'+
'<button id="bCanc" type="button" class="btn btn-sm btn-default"  onclick="rowCancel(this);">' + 
'<i class="glyphicon glyphicon-remove" aria-hidden="true"></i>'+
'</button>'+
    '</div>';
  var colEdicHtml = '<td name="buttons">'+newColHtml+'</td>'; 
var colSaveHtml = '<td name="buttons">'+saveColHtml+'</td>';
    
  $.fn.SetEditable = function (options) {
    var defaults = {
        columnsEd: null,         //Index to editable columns. If null all td editables. Ex.: "1,2,3,4,5"
        $addButton: null,        //Jquery object of "Add" button
        onEdit: function() {},   //Called after edition
		onBeforeDelete: function() {}, //Called before deletion
        onDelete: function() {}, //Called after deletion
        onAdd: function() {}     //Called when added a new row
    };
    params = $.extend(defaults, options);
      this.find('thead tr').append('<th name="buttons"></th>');  //Empty header
    this.find('tbody tr').append(colEdicHtml);
	var $tabedi = this;   //Read reference to the current table, to resolve "this" here.
    //Process "addButton" parameter
    if (params.$addButton != null) {
        //Parameter was provided
        params.$addButton.click(function() {
            rowAddNew($tabedi.attr("id"));
        });
    }
    //Process "columnsEd" parameter
    if (params.columnsEd != null) {
        //Extract felds
        colsEdi = params.columnsEd.split(',');
    }
  };
function IterarCamposEdit($cols, tarea) {
    //Itera for the editable fields of a row
    var n = 0;
    $cols.each(function() {
        n++;
        if ($(this).attr('name') == 'buttons') return;  //Excludes button column
        if (!EsEditable(n - 1)) return;   //something s campo editable
        tarea($(this));
    });
    
    function EsEditable(idx) {
        //Indicates if the last column is set to be editable
        if (colsEdi == null) {  //it was not defined
            return true;  //all are editable
        } else {  //There is filter fields
//alert('verificando: ' + idx);
            for (var i = 0; i < colsEdi.length; i++) {
              if (idx == colsEdi[i]) return true;
            }
            return false;  //It was not found
        }
    }
}
function FijModoNormal(but) {
    $(but).parent().find('#bAcep').hide();
    $(but).parent().find('#bCanc').hide();
    $(but).parent().find('#bEdit').show();
    $(but).parent().find('#bElim').show();
    var $row = $(but).parents('tr');  //access the row
    $row.attr('id', '');  //remove mark
}
function FijModoEdit(but) {

    $(but).parent().find('#bAcep').show();
    $(but).parent().find('#bCanc').show();
    $(but).parent().find('#bEdit').hide();
    $(but).parent().find('#bElim').hide();
    var $row = $(but).parents('tr');  //access the row
    $row.attr('id', 'editing');  //Indicates that it is in edition
}
function ModoEdicion($row) {
    if ($row.attr('id')=='editing') {
        return true;
    } else {
        return false;
    }
}
function rowAcep(but) {
//Accept changes to the edition
    
    
    var $row = $(but).parents('tr');  //access the row
    var $cols = $row.find('td');  //read fields
    if (!ModoEdicion($row)) return;  //It is already in edition
    //It is in edition. The edition must be finalized
    IterarCamposEdit($cols, function ($td) {  //iterate through the columns
        var cont = $td.find('input').val(); //read input content
        $td.html(cont);  //fix content and eliminate controls
    });
    FijModoNormal(but);
    params.onEdit($row);
}
function rowCancel(but) {
//Reject changes to the edition
    var $row = $(but).parents('tr');  //access the row
    var $cols = $row.find('td');  //read fields
    if (!ModoEdicion($row)) return;  //It is already in edition
    //It is in edition. The edition must be finalized
    IterarCamposEdit($cols, function ($td) {  //iterate through the columns
        var cont = $td.find('div').html(); //read div content
        $td.html(cont);  //fix content and eliminate controls
    });
    FijModoNormal(but);
}
function rowEdit(but) {  
    var $td = $("tr[id='editing'] td");
    rowAcep($td)
    var $row = $(but).parents('tr');  
    var $cols = $row.find('td');  
    if (ModoEdicion($row)) return;  //It is already in edition
    //Put in edit mode
    IterarCamposEdit($cols, function ($td) {  //iterate through the columns
        var cont = $td.html(); //read content
        var div = '<div style="display: none;">' + cont + '</div>';  //save content
        var input = '<input class="form-control input-sm"  value="' + cont + '">';
        $td.html(div + input);  //fixed content
    });
    FijModoEdit(but);
}
function rowElim(but) {  //Delete the current row
    var $row = $(but).parents('tr');  //access the row
    params.onBeforeDelete($row);
    $row.remove();
    params.onDelete();
}
function rowAddNew(tabId) {  //Add row to the indicated table.
var $tab_en_edic = $("#" + tabId);  //Table to edit
    var $filas = $tab_en_edic.find('tbody tr');
    if ($filas.length==0) {
        //There are no rows of data. You have to create them complete
        var $row = $tab_en_edic.find('thead tr');  //header
        var $cols = $row.find('th');  //read fields
        //build html
        var htmlDat = '';
        $cols.each(function() {
            if ($(this).attr('name')=='buttons') {
                //It is a column of button
                htmlDat = htmlDat + colEdicHtml;  //add buttons
            } else {
                htmlDat = htmlDat + '<td></td>';
            }
        });
        $tab_en_edic.find('tbody').append('<tr>'+htmlDat+'</tr>');
    } else {
        //There are other rows, we can clone the last row, to copy the buttons
        var $ultFila = $tab_en_edic.find('tr:last');
        $ultFila.clone().appendTo($ultFila.parent()); 
        $tab_en_edic.find('tr:last').attr('id','editing'); 
        $ultFila = $tab_en_edic.find('tr:last');
        var $cols = $ultFila.find('td');  //read fields
        
        $cols.each(function() {
            if ($(this).attr('name')=='buttons') {
                //It is a column of buttons
            } else {
                var div = '<div style="display: none;"></div>';  //save content
                var input = '<input class="form-control input-sm"  value="">';

                $(this).html(div + input);  //clean content
            }
        });
         $ultFila.find('td:last').html(saveColHtml);

    }
	params.onAdd();
}

function TableToCSV(tabId, separator) {  //Convert table to CSV
    var datFil = '';
    var tmp = '';
    var $tab_en_edic = $("#" + tabId);  //Table source
    $tab_en_edic.find('tbody tr').each(function () {
        //Finish the edition if it exists
        if (ModoEdicion($(this))) {
            $(this).find('#bAcep').click();  //accept edition
        }
        var $cols = $(this).find('td');  //read fields
        datFil = '';
        $cols.each(function () {
            if ($(this).attr('name') == 'buttons') {
                //It is a column of buttons
            } else {
                datFil = datFil + $(this).html() + separator;
            }
        });
        if (datFil != '') {
            datFil = datFil.substr(0, datFil.length - separator.length);
        }
        tmp = tmp + datFil + '\n';
    });
    return tmp;
}
