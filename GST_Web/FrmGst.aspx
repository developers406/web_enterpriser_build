﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmGst.aspx.cs" Inherits="EnterpriserWebFinal.FrmGst" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="../js/GST/jquery-3.2.1.slim.min.js"></script>
    <script src="../js/GST/popper.min.js"></script>
    <script>
        $(document).ready(function () {

            $("#<%=Button7.ClientID %>").click(function () {
                window.location.href = "GstrMain.aspx";
                console.log("1");
                return false;
            });
            $(document).ready(function () {

                $("#divB2B").click(function () {
                    $("#b1").click();
                });
                $("#divB2C").click(function () {
                    $("#Button2").click();
                });
                $("#divB2CSmall").click(function () {
                    $("#Button3").click();
                });
                $("#divCNDN").click(function () {
                    $("#Button4").click();
                });
                $("#divCNDNUN").click(function () {
                    $("#Button5").click();
                });
                $("#divHSN").click(function () {
                    $("#Button6").click();
                });

            });

        });
    </script>
    <style>
        body {
            font-family: proxima-nova-light,sans-serif;
        }


        .btn-primary1:hover,
        .btn-primary1:focus {
            background-color: #1dd2af;
        }

        .btn-primary1 {
            color: #fff;
            background-color: #148F72;
            border-color: #0f705d !important;
            border-radius: 6px;
        }

        section {
            padding: 60px 0;
        }

            section .section-title {
                text-align: center;
                color: #368EE0;
                margin-bottom: 50px;
                text-transform: uppercase;
            }

        #team .card {
            border-color: black !important;
            background: #ffffff;
        }

        .card-body {
            padding: 0.25rem !important;
            background-color: white;
            /*background-image: url("Accounts_Icon1.png");
background-repeat: no-repeat;
background-position: center;*/
        }


        .frontside {
            position: relative;
            -webkit-transform: rotateY(0deg);
            -ms-transform: rotateY(0deg);
            z-index: 2;
            margin-bottom: 10px;
        }

        .backside {
            position: absolute;
            top: 0;
            left: 0;
            background: white;
            -webkit-transform: rotateY(-180deg);
            -moz-transform: rotateY(-180deg);
            -o-transform: rotateY(-180deg);
            -ms-transform: rotateY(-180deg);
            transform: rotateY(-180deg);
            -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        }

            .frontside .card,
            .backside .card {
                height: 220px;
            }

        card-text {
            text-align: justify !important;
        }

        .card-title {
            margin-bottom: -1.3rem;
            background-color: darkblue !important;
            border-radius: 2px;
            color: white !important;
            text-align: center;
            font-weight: bold;
            font-size: 15px;
        }

        hr {
            color: #368EE0 !important;
            background-color: #368EE0 !important;
        }

        .gst {
            text-align: left !important;
            padding-left: 0px;
            font-weight: bold;
            color: black;
        }

        .label2 {
            padding-left: 0px;
            font-weight: bold;
            color: black;
            text-align: right !important;
        }

        .text-center {
            text-align: left !important;
        }

        .label1 {
            font-weight: bold;
            color: wheat;
            text-align: left !important;
            padding-left: 0px;
            /*text-decoration-line:underline;*/
            background-color: black;
        }

        .value {
            font-weight: bold;
            float: right;
            padding-right: 0px;
            color: black;
        }

        .underline {
            /*text-decoration-line:underline;*/
            background-color: black;
            color: wheat !important;
            font-weight: bold;
        }

        .innerhr {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .innerhr1 {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        body {
            background-image: url("../images/GSTImage.PNG") !important;
        }

        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 10px;
        }

        .radioboxlist label {
            color: white;
            background-color: rebeccapurple;
            box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            padding-left: 6px;
            padding-right: 5px;
            padding-top: 2px;
            padding-bottom: 2px;
            border: 1px solid #AAAAAA;
            white-space: nowrap;
            clear: left;
            margin-right: 5px;
            margin-left: 5px;
            font-size: small;
        }

            .radioboxlist label:hover {
                background-color: dodgerblue;
                color: white;
                box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            }
    </style>
    <style>
        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
            box-shadow: 0 1px 1px rgba(0,0,0,.05);
        }

        .panel-default {
            border-color: #ddd;
        }

            .panel-default > .panel-heading {
                color: #333;
                background-color: #f5f5f5;
                border-color: #ddd;
            }

        .panel-heading {
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }

        .panel-body {
            padding: 15px;
        }
    </style>
</head>
<body style="overflow:hidden" >
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark" style="background: #368ee0 !important;display:none;" >
            <a class="navbar-brand" href="#">
                <img src="http://uniprotech.co.in/images/retail.jpg" alt="Dispute Bills" />
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar" >
                <ul class="navbar-nav" style="color: white!important">
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Masters</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Inventory</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Merchandising</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Purchase</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Sales</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Stock</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Management</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Transfers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">GST</a>
                    </li>
                </ul>
            </div>
        </nav>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="padding: 2px !important">
                <li class="breadcrumb-item">Menu</li>
                <li class="breadcrumb-item">GSTR1</li>
                <li class="breadcrumb-item active" aria-current="page">Summary</li>
                <div class="form-inline" style="padding-bottom: 0px;display:none;">

                    <%--                        <div class="form-check mb-2 mr-sm-2 mb-sm-0 radioboxlist">
                            <label class=" " style="font-weight: bold;">
                                <input class="form-check-input " name="1" type="radio">
                                Already Filed
                            </label>
                        </div>
                        <div class="form-check mb-2 mr-sm-2 mb-sm-0 radioboxlist">
                            <label class="form-check-label" style="font-weight: bold;">
                                <input class="form-check-input" name="1" type="radio">
                                To Be Filed
                            </label>
                        </div>--%>
                    <div class="form-check mb-2 mr-sm-2 mb-sm-0 radioboxlist" style="padding-right: 20px; margin-left: 157px">
                        <label class="form-check-label" style="padding-right: 10px; font-weight: bold;">
                            Financial Period
                        </label>
                       
                        <asp:TextBox ID="GstDate" ReadOnly="true" runat="server" type="month" Style="padding: 0px" CssClass="form-control"> </asp:TextBox>
                        <asp:Button ID="GenOTP" runat="server" OnClick="GenOTP_Click" Text="Generate OTP" />

                        <div class="radioboxlist">
                            <asp:Label ID="OTPExpiry" Style="margin-left: 187px;  background-color: cornsilk" Font-Bold="true" runat="server" Text="OTP Expiry"></asp:Label>
                        </div>
                        <asp:Panel ID="pnlTextBoxes" runat="server" Style="margin-top: 0px">
                        </asp:Panel>

                    </div>

                </div>
            </ol>
        </nav>
        <div class=" dynamicTile" style="margin-top: -15px !important; margin-bottom: -15px !important;">
          
            <div>

                <div id="team" class="pb-5" style="padding-top: 5px">
                    <div class="container">

                        <div class="row">
                            <!-- Team member -->
                            <div class="col-xs-12 col-sm-6 col-md-4" id="divB2B" style="cursor: pointer;">
                                <div class="image-flip">

                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p class="card-title">
                                                    B2B Sales (# :
                                                    <asp:Label ID="b2bCount" runat="server" Text=""></asp:Label>)
                                                </p>
                                                <hr />
                                                <div class="card-text">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label1" runat="server" class="label1" Text="Total Invoice Amount:"></asp:Label>
                                                        <div class="value underline">
                                                            &#x20B9;
                                                        <asp:Label ID="Label2" CssClass="" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr1" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label3" runat="server" class="label2" Text="Taxable Amount:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label4" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label5" runat="server" CssClass="gst" Text="Central GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label6" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label7" runat="server" CssClass="gst" Text="State GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label8" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label9" runat="server" CssClass="gst" Text="Integrated GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label10" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label11" runat="server" CssClass="gst" Text="CESS:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label12" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label73" runat="server" CssClass="gst" Text="Total:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label74" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div style="float: none; margin-bottom: 4px;">

                                                        <%-- <center>                                                            
                                                        <asp:Button ID="b1" Text="B2B Dtls" CssClass="btn-primary1" runat="server" OnClick="b2bData_clk" />
                                                            </center>--%>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4" id="divB2C" style="cursor: pointer;">
                                <div class="image-flip">

                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p class="card-title">
                                                    B2C Large Sales (# :
                                                    <asp:Label ID="b2cLargeCount" runat="server" Text=""></asp:Label>)
                                                </p>
                                                <hr />
                                                <div class="card-text">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label13" runat="server" class="label1" Text="Total Invoice Amount:"></asp:Label>
                                                        <div class="value underline">
                                                            &#x20B9;
                                                        <asp:Label ID="Label14" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr1" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label15" runat="server" class="label2" Text="Taxable Amount:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label16" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label17" runat="server" CssClass="gst" Text="Central GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label18" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label19" runat="server" CssClass="gst" Text="State GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label20" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label21" runat="server" CssClass="gst" Text="Integrated GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label22" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label23" runat="server" CssClass="gst" Text="CESS:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label24" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label75" runat="server" CssClass="gst" Text="Total:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label76" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div style="float: none; margin-bottom: 4px;">
                                                        <%-- <center>
                                                        <asp:Button ID="Button2" Text="B2CL Dtls" CssClass="btn-primary1" runat="server"  OnClick="b2clData_clk" />
                                                            </center>--%>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4" id="divB2CSmall" style="cursor: pointer;">
                                <div class="image-flip">

                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p class="card-title">
                                                    B2C Small Sales (# :
                                                    <asp:Label ID="b2cSmallCount" runat="server" Text=""></asp:Label>)
                                                </p>
                                                <hr />
                                                <div class="card-text">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label25" runat="server" class="label1" Text="Total Invoice Amount:"></asp:Label>
                                                        <div class="value underline">
                                                            &#x20B9;
                                                        <asp:Label ID="Label26" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr1" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label27" runat="server" class="label2" Text="Taxable Amount:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label28" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label29" runat="server" CssClass="gst" Text="Central GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label30" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label31" runat="server" CssClass="gst" Text="State GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label32" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label33" runat="server" CssClass="gst" Text="Integrated GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label34" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label35" runat="server" CssClass="gst" Text="CESS:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label36" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label77" runat="server" CssClass="gst" Text="Total:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label78" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div style="float: none; margin-bottom: 4px;">
                                                        <%--<center>
                                                        <asp:Button ID="Button3" Text="B2CS Dtls" CssClass="btn-primary1" runat="server" OnClick="b2csData_clk" />
                                                            </center>--%>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4" id="divCNDN" style="cursor: pointer;">
                                <div class="image-flip">

                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p class="card-title">
                                                    CN/DN Regd(CDNR) (# :
                                                    <asp:Label ID="cdnrCount" runat="server" Text=""></asp:Label>)
                                                </p>
                                                <hr />
                                                <div class="card-text">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label37" runat="server" class="label1" Text="Total CDN Amount:"></asp:Label>
                                                        <div class="value underline">
                                                            &#x20B9;
                                                        <asp:Label ID="Label38" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr1" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label39" runat="server" class="label2" Text="Taxable Amount:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label40" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label41" Font-Bold="true" runat="server" CssClass="gst" Text="Central GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label42" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label43" Font-Bold="true" runat="server" CssClass="gst" Text="State GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label44" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label45" Font-Bold="true" runat="server" CssClass="gst" Text="Integrated GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label46" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label47" Font-Bold="true" runat="server" CssClass="gst" Text="CESS:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label48" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label79" runat="server" CssClass="gst" Text="Total:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label80" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div style="float: none; margin-bottom: 4px;">
                                                        <%--<center>
                                                        <asp:Button ID="Button4" Text="CDNR Dtls" CssClass="btn-primary1" runat="server" OnClick="cdnrData_clk" />
                                                            </center>--%>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4" id="divCNDNUN" style="cursor: pointer;">
                                <div class="image-flip">

                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p class="card-title">
                                                    CN/DN UnRegd(CDNUR)
                                                    (# :
                                                    <asp:Label ID="cdnurCount" runat="server" Text=""></asp:Label>)
                                                </p>
                                                <hr />
                                                <div class="card-text">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label49" runat="server" class="label1" Text="Total CDN Amount:"></asp:Label>
                                                        <div class="value underline">
                                                            &#x20B9;
                                                        <asp:Label ID="Label50" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr1" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label51" runat="server" class="label2" Text="Taxable Amount:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label52" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label53" runat="server" Font-Bold="true" CssClass="gst" Text="Central GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label54" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label55" runat="server" Font-Bold="true" CssClass="gst" Text="State GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label56" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label57" runat="server" Font-Bold="true" CssClass="gst" Text="Integrated GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label58" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label59" runat="server" Font-Bold="true" CssClass="gst" Text="CESS:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label60" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label81" runat="server" CssClass="gst" Text="Total:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label82" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div style="float: none; margin-bottom: 4px;">
                                                        <%--<center>
                                                        <asp:Button ID="Button5" Text="CDNUR Dtls" CssClass="btn-primary1" runat="server" OnClick="cdnurData_clk"/>
                                                            </center>--%>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4" id="divHSN" style="cursor: pointer;">
                                <div class="image-flip">

                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p class="card-title">
                                                    HSN Summary (# :
                                                    <asp:Label ID="hsnCount" runat="server" Text=""></asp:Label>)
                                                </p>
                                                <hr />
                                                <div class="card-text">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label61" runat="server" class="label1" Text="Total Invoice Amount:"></asp:Label>
                                                        <div class="value underline">
                                                            &#x20B9;
                                                        <asp:Label ID="Label62" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr1" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label63" runat="server" class="label2" Text="Taxable Amount:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label64" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label65" runat="server" Font-Bold="true" CssClass="gst" Text="Central GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label66" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label67" runat="server" Font-Bold="true" CssClass="gst" Text="State GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label68" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label69" runat="server" Font-Bold="true" CssClass="gst" Text="Integrated GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label70" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label71" runat="server" Font-Bold="true" CssClass="gst" Text="CESS:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label72" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label83" runat="server" CssClass="gst" Text="Total:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="Label84" runat="server" Text="2500"></asp:Label>
                                                        </div>
                                                        <div style="float: none; margin-bottom: 4px;">
                                                            <%--<center>
                                                        <asp:Button ID="Button6" Text="HSN Dtls" CssClass="btn-primary1" runat="server" OnClick="hsnData_clk" />
                                                            </center>--%>
                                                        </div>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-8">
                             <asp:CheckBox ID="ChkBoxSubmit" style="color:red;" runat="server" Text="" />
                                <asp:Label runat="server" style="color:red;">Reviewed the details and the information is correct and would like to submit the details.</asp:Label><br/>
                            <asp:label runat="server" style="color:red;margin:20px">I am aware that no changes can be made after submit.</asp:label>
                            </div>
                            <div class="col-md-4" style="float:right;margin:-35px">
                                        <asp:Button ID="b1" style="display:none" Text="B2B Dtls" CssClass="btn-primary1" runat="server" OnClick="b2bData_clk" />
                            <asp:Button ID="Button2" style="display:none" Text="B2CL Dtls" CssClass="btn-primary1" runat="server" OnClick="b2clData_clk" />
                            <asp:Button ID="Button3" style="display:none" Text="B2CS Dtls" CssClass="btn-primary1" runat="server" OnClick="b2csData_clk" />
                            <asp:Button ID="Button4" style="display:none" Text="CDNR Dtls" CssClass="btn-primary1" runat="server" OnClick="cdnrData_clk" />
                            <asp:Button ID="Button5" style="display:none" Text="CDNUR Dtls" CssClass="btn-primary1" runat="server" OnClick="cdnurData_clk" />
                            <asp:Button ID="Button6" style="display:none" Text="HSN Dtls" CssClass="btn-primary1" runat="server" OnClick="hsnData_clk" />
                              <asp:Button ID="Button7" style="background-color:darkblue;color:white" CssClass="btn-primary1" runat="server" Text="Back"/>
                             <asp:Button ID="Button1" style="background-color:cornflowerblue;color:white" CssClass="btn-primary1" runat="server" Text="Submit" OnClick="Button1_Click" />
                             <asp:Button ID="btnfilegst" style="background-color:darkblue;color:white" CssClass="btn-primary1" runat="server" Text="File With DSC"  />
                        
                            </div>
                        </div>
                    </div>
                    </div>
                    

                </div>
            </div>
        </div>

    </form>
</body>

</html>
