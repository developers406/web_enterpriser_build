﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GstrMain.aspx.cs" Inherits="EnterpriserWebFinal.GstrMain" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="../js/GST/jquery-3.2.1.slim.min.js"></script>
    <script src="../js/GST/popper.min.js"></script>
    <script>
        $(document).ready(function () {

            $("#GSTR1").click(function () {
                window.location.href = "FrmGst.aspx"
            });
            $("#GSTR2").click(function () {
                window.location.href = "GSTR2A.aspx";
            });
            $("#GSTR3").click(function () {
                window.location.href = "https://services.gst.gov.in/services/login"

                console.log("3");
            }); 
        });

        function fncLoadPaymentSync() {
            try {
                window.location.href = "frmGstrMothwiseSalesSync.aspx"
            }
            catch (err) {
                alert(err.message);
            }
        }
    </script>

    <style>
        body {
            font-family: proxima-nova-light,sans-serif;
        }


        .btn-primary1:hover,
        .btn-primary1:focus {
            background-color: #1dd2af;
        }

        .btn-primary1 {
            color: #fff;
            background-color: #148F72;
            border-color: #0f705d !important;
            border-radius: 6px;
        }

        section {
            padding: 60px 0;
        }

            section .section-title {
                text-align: center;
                color: #368EE0;
                margin-bottom: 50px;
                text-transform: uppercase;
            }

        #team .card {
            border-color: black !important;
            background: #ffffff;
        }

        .card-body {
            padding: 2.25rem !important;
            background-color: white;
            border-radius: 25px;
            border-width: 25px;
            /*background-image: url("Accounts_Icon1.png");
background-repeat: no-repeat;
background-position: center;*/
        }


        .frontside {
            position: relative;
            -webkit-transform: rotateY(0deg);
            -ms-transform: rotateY(0deg);
            z-index: 2;
            margin-bottom: 10px;
            cursor: pointer;
        }

        .backside {
            position: absolute;
            top: 0;
            left: 0;
            background: white;
            -webkit-transform: rotateY(-180deg);
            -moz-transform: rotateY(-180deg);
            -o-transform: rotateY(-180deg);
            -ms-transform: rotateY(-180deg);
            transform: rotateY(-180deg);
            -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        }

            .frontside .card,
            .backside .card {
                height: 220px;
            }

        card-text {
            text-align: justify !important;
        }

        .card-title {
            background-color: darkblue !important;
            border-radius: 2px;
            color: white !important;
            text-align: center;
            font-weight: bold;
        }

        hr {
            color: #368EE0 !important;
            background-color: #368EE0 !important;
        }

        .gst {
            text-align: left !important;
            padding-left: 0px;
            font-weight: bold;
            color: black;
        }

        .label2 {
            padding-left: 0px;
            font-weight: bold;
            color: black;
            text-align: right !important;
        }

        .text-center {
            text-align: left !important;
        }

        .label1 {
            font-weight: bold;
            color: wheat;
            text-align: left !important;
            padding-left: 0px;
            /*text-decoration-line:underline;*/
            background-color: black;
        }

        .value {
            font-weight: bold;
            float: right;
            padding-right: 0px;
            color: black;
        }

        .underline {
            /*text-decoration-line:underline;*/
            background-color: black;
            color: wheat !important;
            font-weight: bold;
        }

        .innerhr {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .innerhr1 {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        body {
            background-image: url("../images/GSTImage.PNG") !important;
        }

        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 10px;
        }

        .radioboxlist label {
            color: white;
            background-color: rebeccapurple;
            box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            padding-left: 6px;
            padding-right: 5px;
            padding-top: 2px;
            padding-bottom: 2px;
            border: 1px solid #AAAAAA;
            white-space: nowrap;
            clear: left;
            margin-right: 5px;
            margin-left: 5px;
            font-size: small;
        }

            .radioboxlist label:hover {
                background-color: dodgerblue;
                color: white;
                box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            }
    </style>
    <style>
        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
            box-shadow: 0 1px 1px rgba(0,0,0,.05);
        }

        .panel-default {
            border-color: #ddd;
        }

            .panel-default > .panel-heading {
                color: #333;
                background-color: #f5f5f5;
                border-color: #ddd;
            }

        .panel-heading {
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }

        .panel-body {
            padding: 15px;
        }
    </style>
</head>
<body style="overflow: hidden">
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark" style="background: #368ee0 !important;display:none;">
            <a class="navbar-brand" href="#">
                <img src="http://uniprotech.co.in/images/retail.jpg" alt="Dispute Bills" />
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav" style="color: white!important">
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Masters</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Inventory</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Merchandising</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Purchase</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Sales</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Stock</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Management</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Transfers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">GST</a>
                    </li>
                </ul>
            </div>
        </nav>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="padding: 2px !important">
                <li class="breadcrumb-item">Menu</li>
                <li class="breadcrumb-item">GSTR1</li>
                <li class="breadcrumb-item active" aria-current="page">Summary</li>

            </ol>
        </nav>
        <div class=" dynamicTile" style="margin-top: -15px !important; margin-bottom: -15px !important;">

            <div>

                <div id="team" class="pb-5" style="padding-top: 20px">
                    <div class="container">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-inline" style="padding-bottom: 0px;">

                                    <%--                        <div class="form-check mb-2 mr-sm-2 mb-sm-0 radioboxlist">
                            <label class=" " style="font-weight: bold;">
                                <input class="form-check-input " name="1" type="radio">
                                Already Filed
                            </label>
                        </div>
                        <div class="form-check mb-2 mr-sm-2 mb-sm-0 radioboxlist">
                            <label class="form-check-label" style="font-weight: bold;">
                                <input class="form-check-input" name="1" type="radio">
                                To Be Filed
                            </label>
                        </div>--%>
                                    <div class="form-check mb-2 mr-sm-2 mb-sm-0 radioboxlist">
                                        <label class="form-check-label" style="padding-right: 10px; font-weight: bold;">
                                            Financial Period
                                        </label>

                                        <asp:TextBox ID="GstDate" ReadOnly="true" runat="server" Style="padding: 0px" CssClass="form-control"> </asp:TextBox>
                                        <asp:Button ID="GenOTP" runat="server" Style="margin-left: 10px;" Text="Generate OTP" OnClick="GenOTP_Click" />
                                        <asp:TextBox ID="otpText" runat="server" Style="margin-left: 10px;" CssClass="form-control"></asp:TextBox>
                                        <asp:Button ID="VerifyOTP" runat="server" Style="margin-left: 10px;" Text="Verify" OnClick="VerifyOTP_Click" />

                                        <asp:Label ID="OTPExpiry" Style="margin-left: 10px; background-color: cornsilk" Font-Bold="true" runat="server" Text="OTP Valid Till"></asp:Label>

                                        <asp:TextBox ID="txtOTPExpiry" ReadOnly="true" runat="server" Style="margin-left: 10px;" CssClass="form-control"> </asp:TextBox>

                                        <asp:Panel ID="pnlTextBoxes" runat="server" Style="margin-top: 0px">
                                        </asp:Panel>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- Team member -->
                            <div class="col-md-4" id="GSTR1">

                                <div class="image-flip">

                                    <div class="frontside">
                                        <div class="card">
                                            <h2 class="card-title">GSTR-1</h2>
                                            <div class="card-body text-center" style="cursor: pointer;">

                                                <div class="card-text">

                                                    <center><b>                                                           

                                                       <p>Details Of Outward Supplies(Sales)</p> </b>
                                                  
                                                      </center>

                                                </div>

                                            </div>

                                        </div>


                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4" id="GSTR2">
                                <a>
                                    <div class="image-flip">

                                        <div class="frontside">
                                            <div class="card">
                                                <h2 class="card-title">GSTR-2 & 2A</h2>
                                                <div class=" text-center" style="cursor: pointer;">

                                                    <div class="card-text">

                                                        <center><b>
                                                         <p>Auto Drafted Details Of Inward Supplies(Purchases)<br />
                                                             (View & Add Missing Purchase Entries)</p></b>
                                                    </center>




                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4" id="GSTR3">
                                <a>
                                    <div class="image-flip">

                                        <div class="frontside">
                                            <div class="card">
                                                <h2 class="card-title">GSTR-3</h2>
                                                <div class="card-body text-center" style="cursor: pointer;">

                                                    <div class="card-text">
                                                        <center><b>
                                                        <p>GST Liability (Redirected To GST Portal)</p></b>

                                                    </center>

                                                    </div>

                                                </div>


                                            </div>


                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4" id="GSTRSync">
                                <div class="col-md-12">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <asp:Button ID="btnPayment" runat="server" Text="Payments Sync" OnClientClick="fncLoadPaymentSync();return false;"/>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>

</html>
