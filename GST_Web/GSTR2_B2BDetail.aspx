﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GSTR2_B2BDetail.aspx.cs" Inherits="EnterpriserWebFinal.GSTR2_B2BDetail" %>

<!DOCTYPE html>

<link href="../css/GST/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/GST/pages.css" rel="stylesheet" type="text/css" />
    <link href="../css/GST/common-styles.css" rel="stylesheet" type="text/css" />
    <link href="../css/GST/style.css" rel="stylesheet" type="text/css" />
    <link href="../css/GST/pagination.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
       <%-- $(document).ready(function () {

            $("#<%=Button1.ClientID %>").click(function () {
                window.location.href = "GSTR2A.aspx";
                return false;
            });
           

        });--%>

    </script>
    <style type="text/css">
        .footermaster {
            text-align: center;
            width: 100%;
            margin-top: 10px;
            height: 40px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="navigation" runat="server">


            <div class="container-fluid">
                <%--<a href="../Masters/frmMain.aspx" id="brand">--%>
                <asp:HyperLink ID="brand" runat="server" NavigateUrl="~/Masters/frmMain.aspx">
                <img src="../images/CompanyLogo.png" style="display:block;max-width:100%;width:240px;height:50px;margin-top: 6px;" />
                </asp:HyperLink>
                <%--</a>--%>

                <div class="user">
                    <div class="dropdown">
                        <a href="#" class='dropdown-toggle' data-toggle="dropdown">
                            <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>&nbsp;&nbsp;
                        <img src="../images/avatar.png" style="margin-bottom: -5px" />
                        </a>
                        <ul class="dropdown-menu pull-right">

                            <li><a href="#" onclick="RefreshCacheClicked();return false;">Refresh Master Data</a>
                            </li>
                            <li>
                                <asp:LinkButton ID="lnkbtnLogOut" runat="server">Logout</asp:LinkButton>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="main-container" style="overflow: hidden">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb" style="padding: 2px !important">
                        <li class="breadcrumb-item">Menu</li>
                        <li class="breadcrumb-item">GSTR1</li>
                        <li class="breadcrumb-item active" aria-current="page">Summary</li>
                    </ol>
                </nav>
                <div class="gridDetails">
                    <asp:GridView ID="GridView1" runat="server" CssClass="pshro_GridDgn">
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                            NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                        <PagerStyle CssClass="pshro_text" />
                        
                    </asp:GridView>
                </div>

                <div class="button-contol" style="margin-left: 175px">
                    <div class="control-button">
                        <asp:Button ID="Button1" class="button-red" OnClick="back" runat="server" Text="Back" />
                    </div>
                    <div class="control-button">
                        <asp:Button ID="Button2" class="button-red" runat="server" Text="Save" />
                    </div>
                    <div class="control-button">
                        <asp:Button ID="Button3" class="button-red" runat="server" Text="Refresh" />
                    </div>
                </div>
    </form>
</body>
</html>
