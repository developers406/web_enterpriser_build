﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GSTR2A.aspx.cs" Inherits="EnterpriserWebFinal.GSTR2A" Async="true"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="../js/GST/jquery-3.2.1.slim.min.js"></script>
    <script src="../js/GST/popper.min.js"></script>
     <script>
        $(document).ready(function () {

            $("#<%=Button2.ClientID %>").click(function () {
                window.location.href = "GstrMain.aspx";
                console.log("1");
                return false;
            });
           

        });

         function Toast() {
             try{
                 var obj = {};
                 obj = jQuery.parseJSON($("#<%=hidJson.ClientID %>").val());
                 console.log(obj);
             }
             catch (err) {
                 alert(err.message);
             }
         }

    </script>
    <script>
        $(document).ready(function () {

            $("#divB2B").click(function () {
                $("#btGSTR2ABInvoices").click();
            });
            $("#divCreditDebitNotes").click(function () {
                $("#btGSTR2ACreditDebitNotes").click();
            });
           

        });
    </script>
    <style>
        body {
            font-family: proxima-nova-light,sans-serif;
        }


        .btn-primary1:hover,
        .btn-primary1:focus {
            background-color: #1dd2af;
        }

        .btn-primary1 {
            color: #fff;
            background-color: #148F72;
            border-color: #0f705d !important;
            border-radius: 6px;
        }

        section {
            padding: 60px 0;
        }

            section .section-title {
                text-align: center;
                color: #368EE0;
                margin-bottom: 50px;
                text-transform: uppercase;
            }

        #team .card {
            border-color: black !important;
            background: #ffffff;
        }

        .card-body {
            padding: 0.25rem !important;
            background-color: white;
            /*background-image: url("Accounts_Icon1.png");
background-repeat: no-repeat;
background-position: center;*/
        }


        .frontside {
            position: relative;
            -webkit-transform: rotateY(0deg);
            -ms-transform: rotateY(0deg);
            z-index: 2;
            margin-bottom: 10px;
        }

        .backside {
            position: absolute;
            top: 0;
            left: 0;
            background: white;
            -webkit-transform: rotateY(-180deg);
            -moz-transform: rotateY(-180deg);
            -o-transform: rotateY(-180deg);
            -ms-transform: rotateY(-180deg);
            transform: rotateY(-180deg);
            -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        }

            .frontside .card,
            .backside .card {
                height: 260px;
            }

        card-text {
            text-align: justify !important;
        }

        .card-title {
            margin-bottom: -1.3rem;
            background-color: darkblue !important;
            border-radius: 2px;
            color: white !important;
            text-align: center;
            font-weight: bold;
            font-size: 15px;
        }

        hr {
            color: #368EE0 !important;
            background-color: #368EE0 !important;
        }

        .gst {
            text-align: left !important;
            padding-left: 0px;
            font-weight: bold;
            color: black;
        }

        .label2 {
            padding-left: 0px;
            font-weight: bold;
            color: black;
            text-align: right !important;
        }

        .text-center {
            text-align: left !important;
        }

        .label1 {
            font-weight: bold;
            color: wheat;
            text-align: left !important;
            padding-left: 0px;
            /*text-decoration-line:underline;*/
            background-color: black;
        }

        .value {
            font-weight: bold;
            float: right;
            padding-right: 0px;
            color: black;
        }

        .underline {
            /*text-decoration-line:underline;*/
            background-color: black;
            color: wheat !important;
            font-weight: bold;
        }

        .innerhr {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .innerhr1 {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        body {
            background-image: url("GSTImage.PNG") !important;
        }

        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 10px;
        }

        .radioboxlist label {
            color: white;
            background-color: rebeccapurple;
            box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            padding-left: 6px;
            padding-right: 5px;
            padding-top: 2px;
            padding-bottom: 2px;
            border: 1px solid #AAAAAA;
            white-space: nowrap;
            clear: left;
            margin-right: 5px;
            margin-left: 5px;
            font-size: small;
        }

            .radioboxlist label:hover {
                background-color: dodgerblue;
                color: white;
                box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            }
    </style>
    <style>
        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
            box-shadow: 0 1px 1px rgba(0,0,0,.05);
        }

        .panel-default {
            border-color: #ddd;
        }

            .panel-default > .panel-heading {
                color: #333;
                background-color: #f5f5f5;
                border-color: #ddd;
            }

        .panel-heading {
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }

        .panel-body {
            padding: 15px;
        }
    </style>
</head>
<body style="overflow:hidden" >
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark" style="background: #368ee0 !important;display:none;">
            <a class="navbar-brand" href="#">
                <img src="http://uniprotech.co.in/images/retail.jpg" alt="Dispute Bills" />
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav" style="color: white!important">
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Masters</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Inventory</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Merchandising</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Purchase</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Sales</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Stock</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Management</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">Transfers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: white;" href="#">GST</a>
                    </li>
                </ul>
            </div>
        </nav>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="padding: 2px !important">
                <li class="breadcrumb-item">Menu</li>
                <li class="breadcrumb-item">GSTR2</li>
                <li class="breadcrumb-item active" aria-current="page">Summary</li>
                
            </ol>
        </nav>
      
        <div class=" dynamicTile" >
          
            <div>

                <div id="team" class="pb-5" style="padding-top: 5px">
                    <div class="container">
                          <div class="panel panel-default">
                        <div class="panel-body">
                             <label class="form-check-label" style="padding-right: 10px; font-weight: bold;">
                            GSTIN
                        </label>
                       
                        <asp:TextBox ID="TxtGSTIN" ReadOnly="true" runat="server" Style="padding: 0px"> </asp:TextBox>
                             <label class="form-check-label" style="padding-right: 10px; font-weight: bold;">
                            Company Name
                        </label>
                       
                        <asp:TextBox ID="TxtBusinessName" ReadOnly="true" runat="server" Style="padding: 0px;width:390px"> </asp:TextBox>
                             <label class="form-check-label" style="padding-right: 10px; font-weight: bold;">
                            Period
                        </label>
                       
                        <asp:TextBox ID="TxtFinPeriod" ReadOnly="true" runat="server" Style="padding: 0px"> </asp:TextBox>
                            
                        </div>
                    </div>
                      
                        <div class="row" style="margin-right:-585px">
                             
                            <!-- Team member -->
                            <div class="col-xs-12 col-sm-6 col-md-4" id="divB2B" style="cursor: pointer;">
                                <div class="image-flip">

                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p class="card-title">
                                                    B2B Invoices
                                                </p>
                                                <hr />
                                                <div class="card-text">
                                                      <p style="text-align:center">Inward Supplies (Purchases)</p>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label1" runat="server"  CssClass="gst" Text="Total Invoice Count-Local / GSTN:"></asp:Label>
                                                        <div class="value underline">

                                                        <asp:Label ID="lblGSTR2AB2BCount" CssClass="" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                  
                                                    <hr class="innerhr1" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label3" runat="server" CssClass="gst" Text="Taxable Amount(GSTN):"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2AB2BTxbl" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label5" runat="server" CssClass="gst" Text="Central GST(GSTN):"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2AB2BCGST" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label7" runat="server" CssClass="gst" Text="State GST(GSTN):"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2AB2BSGST" runat="server" Text=" "></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label9" runat="server" CssClass="gst" Text="Integrated GST(GSTN):"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2AB2BIGST" runat="server" Text=" "></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label11" runat="server" CssClass="gst" Text="CESS(GSTN):"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2AB2BCess" runat="server" Text=" "></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label73" runat="server" CssClass="gst" Text="Invoice Amt Local / GSTN):"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2AB2BInvVal" runat="server" Text=" "></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div style="float: none; margin-bottom: 4px;">

                                                        <%-- <center>                                                            
                                                        <asp:Button ID="b1" Text="B2B Dtls" CssClass="btn-primary1" runat="server" OnClick="b2bData_clk" />
                                                            </center>--%>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-xs-12 col-sm-6 col-md-4" id="divCreditDebitNotes" style="cursor: pointer;">
                                <div class="image-flip">

                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p class="card-title">
                                                    Credit/Debit Notes
                                                </p>
                                                <hr />
                                                <div class="card-text">
                                                      <p style="text-align:center">Credit/Debit Notes From Suppliers</p>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label13" runat="server" CssClass="gst" Text="Total C/D Notes Count:"></asp:Label>
                                                        <div class="value underline">
                                                            
                                                        <asp:Label ID="lblGSTR2ACDNCount" CssClass="" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </div>
                                                  
                                                    <hr class="innerhr1" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label15" runat="server" CssClass="gst" Text="Taxable Amount:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2ACDNTxbl" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label17" runat="server" CssClass="gst" Text="Central GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2ACDNCGST" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label19" runat="server" CssClass="gst" Text="State GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2ACDNSGST" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label21" runat="server" CssClass="gst" Text="Integrated GST:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2ACDNIGST" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label23" runat="server" CssClass="gst" Text="CESS:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2ACDNCess" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <hr class="innerhr" />
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label25" runat="server" CssClass="gst" Text="Total:"></asp:Label>
                                                        <div class="value">
                                                            &#x20B9;
                                                        <asp:Label ID="lblGSTR2ACDNVal" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div style="float: none; margin-bottom: 4px;">

                                                        <%-- <center>                                                            
                                                        <asp:Button ID="b1" Text="B2B Dtls" CssClass="btn-primary1" runat="server" OnClick="b2bData_clk" />
                                                            </center>--%>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                                 
                        </div>
                           <asp:Button ID="btGSTR2ABInvoices" Text="GSTR2ABInvoices" style="display:none;" runat="server" onclick ="GSTR2A_GetB2B_Click" />
                            <asp:Button ID="btGSTR2ACreditDebitNotes" Text="GSTR2ACreditDebitNotes" style="display:none;" runat="server"  onclick = "GSTR2A_CDN_Click" />
                        <div class="panel panel-default" style="margin-top:20px">
                        <div class="panel-body">

                           <asp:Button ID="RefreshB2B" style="margin-left:10px;background-color:darkblue;color:white" CssClass="btn-primary1" runat="server" Text="Refresh-B2B" OnClick="RefreshB2B_Click"/>
                           
                           <asp:Button ID="Button2" style="margin-left:250px;background-color:darkblue;color:white" CssClass="btn-primary1" runat="server" Text="Back"/>

                             <asp:Button ID="Button1" style="margin-left:10px;background-color:darkblue;color:white" CssClass="btn-primary1" runat="server" Text="GSTR-2 Add Missing Entries" OnClick="GSTR2_MisB2B_Clk"/>

                            <asp:Button ID="RefreshCDN" style="margin-left:300px;background-color:darkblue;color:white" CssClass="btn-primary1" runat="server" Text="Refresh-CDN"/>
                            
                        </div>
                    </div>
                    </div>
                    

                </div>
            </div>
            <asp:HiddenField ID="hidJson" runat ="server" />
        </div> 

    </form>
</body>

</html>
