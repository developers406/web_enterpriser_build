﻿<%@ Page Language="C#" AutoEventWireup="true"  Async="true" CodeBehind="frmGstrMothwiseSalesSync.aspx.cs" Inherits="EnterpriserWebFinal.frmGstrMothwiseSalesSync" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type ="text/css">
        body {
            font-family: proxima-nova-light,sans-serif;
        }


        .btn-primary1:hover,
        .btn-primary1:focus {
            background-color: #1dd2af;
        }

        .btn-primary1 {
            color: #fff;
            background-color: #148F72;
            border-color: #0f705d !important;
            border-radius: 6px;
        }

        section {
            padding: 60px 0;
        }

            section .section-title {
                text-align: center;
                color: #368EE0;
                margin-bottom: 50px;
                text-transform: uppercase;
            }

        #team .card {
            border-color: black !important;
            background: #ffffff;
        }

        .card-body {
            padding: 2.25rem !important;
            background-color: white;
            border-radius: 25px;
            border-width: 25px;
            /*background-image: url("Accounts_Icon1.png");
background-repeat: no-repeat;
background-position: center;*/
        }


        .frontside {
            position: relative;
            -webkit-transform: rotateY(0deg);
            -ms-transform: rotateY(0deg);
            z-index: 2;
            margin-bottom: 10px;
            cursor: pointer;
        }

        .backside {
            position: absolute;
            top: 0;
            left: 0;
            background: white;
            -webkit-transform: rotateY(-180deg);
            -moz-transform: rotateY(-180deg);
            -o-transform: rotateY(-180deg);
            -ms-transform: rotateY(-180deg);
            transform: rotateY(-180deg);
            -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        }

            .frontside .card,
            .backside .card {
                height: 220px;
            }

        card-text {
            text-align: justify !important;
        }

        .card-title {
            background-color: darkblue !important;
            border-radius: 2px;
            color: white !important;
            text-align: center;
            font-weight: bold;
        }

        hr {
            color: #368EE0 !important;
            background-color: #368EE0 !important;
        }

        .gst {
            text-align: left !important;
            padding-left: 0px;
            font-weight: bold;
            color: black;
        }

        .label2 {
            padding-left: 0px;
            font-weight: bold;
            color: black;
            text-align: right !important;
        }

        .text-center {
            text-align: left !important;
        }

        .label1 {
            font-weight: bold;
            color: wheat;
            text-align: left !important;
            padding-left: 0px;
            /*text-decoration-line:underline;*/
            background-color: black;
        }

        .value {
            font-weight: bold;
            float: right;
            padding-right: 0px;
            color: black;
        }

        .underline {
            /*text-decoration-line:underline;*/
            background-color: black;
            color: wheat !important;
            font-weight: bold;
        }

        .innerhr {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .innerhr1 {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        body {
            background-image: url("GSTImage.PNG") !important;
        }

        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 10px;
        }

        .radioboxlist label {
            color: white;
            background-color: rebeccapurple;
            box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            padding-left: 6px;
            padding-right: 5px;
            padding-top: 2px;
            padding-bottom: 2px;
            border: 1px solid #AAAAAA;
            white-space: nowrap;
            clear: left;
            margin-right: 5px;
            margin-left: 5px;
            font-size: small;
        }

            .radioboxlist label:hover {
                background-color: dodgerblue;
                color: white;
                box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            }
    </style>
    <style>
        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
            box-shadow: 0 1px 1px rgba(0,0,0,.05);
        }

        .panel-default {
            border-color: #ddd;
        }

            .panel-default > .panel-heading {
                color: #333;
                background-color: #f5f5f5;
                border-color: #ddd;
            }

        .panel-heading {
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }

        .panel-body {
            padding: 15px;
        }
    </style>
</head>
     
<body>

    <form id="form1" runat="server">
        <div class=" dynamicTile" style="margin-top: -15px !important; margin-bottom: -15px !important;">

            <div id="team" class="pb-5" style="padding-top: 20px">
                <div class="container">
                    <div class="panel panel-default">
                        <div class="panel-body" style ="display:none;">
                            <div class="form-inline" style="padding-bottom: 0px;">
                                <div class="form-check mb-2 mr-sm-2 mb-sm-0 radioboxlist">
                                    <label class="form-check-label" style="padding-right: 10px; font-weight: bold;">
                                        Financial Period
                                    </label>
                                    <asp:TextBox ID="GstDate" ReadOnly="true" runat="server" Style="padding: 0px" CssClass="form-control"></asp:TextBox> 
                                    <asp:TextBox ID="otpText" runat="server" Style="margin-left: 10px;" CssClass="form-control"></asp:TextBox>
                                    <asp:Button ID="VerifyOTP" runat="server" Style="margin-left: 10px;" Text="Verify" OnClick="VerifyOTP_Click" />
                                    <asp:Label ID="OTPExpiry" Style="margin-left: 10px; background-color: cornsilk" Font-Bold="true" runat="server" Text="OTP Valid Till"></asp:Label>
                                    <asp:TextBox ID="txtOTPExpiry" ReadOnly="true" runat="server" Style="margin-left: 10px;" CssClass="form-control"> </asp:TextBox>
                                    <asp:Panel ID="pnlTextBoxes" runat="server" Style="margin-top: 0px">
                                    </asp:Panel>
                                </div>

                            </div>
                        </div>
                         <label class="form-check-label" style="padding-right: 10px; font-weight: bold;">
                                        Financial Period
                                    </label>
                                    <asp:TextBox ID="txtFinePeriod"   runat="server" placeholder="MMYYYY" Style="padding: 0px" CssClass="form-control"></asp:TextBox> 
                         <asp:Button ID="btnFinePeriod" runat="server" Style="margin-left: 10px;" CssClass ="btn-primary1"   Text="Fetch" OnClick="GSTR2A_GetB2B_Click" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
