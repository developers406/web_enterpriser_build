﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GSTR1B2CS.aspx.cs" Inherits="EnterpriserWebFinal.GSTR1B2CS" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="../css/GST/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/GST/pages.css" rel="stylesheet" type="text/css" />
    <link href="../css/GST/common-styles.css" rel="stylesheet" type="text/css" />
    <link href="../css/GST/style.css" rel="stylesheet" type="text/css" />
    <link href="../css/GST/pagination.css" rel="stylesheet" type="text/css" />
    <title></title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="../js/GST/myjs.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="../js/GST/handlebars-v4.0.11.js"></script>

    <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <%--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">--%>

    <script id="gstr1-temp" type="text/x-handlebars-template">
        <div class="accordion" id="cstinAccordion">
            <%--  <input type="hidden" name="hdnpos" id="hdnpos" value="" />
        <input type="hidden" name="hdnsply_ty" id="hdnsply_ty" value="" />
        <input type="hidden" name="hdntyp" id="hdntyp" value="" />--%>
            <%--{{#each b2cs}}--%>





            <table class="itmTable table table-sm table-light table-striped">

                <thead>
                    <tr>
                        <th>Select</th>
                        <th>POS</th>
                        <th>Tax Value</th>
                        <th>Type</th>
                        <th>CAmt</th>
                        <th>SAmt Tax</th>
                        <th>IAmt</th>
                        <th>CSAmt</th>
                        <th>RT</th>
                        <th>Flag</th>
                        <th>Sply_Ty</th>
                        <th>ETIN</th>
                    </tr>
                </thead>
                <tbody>
                    {{#each b2cs}}
						<tr>
                            <td>
                                <input type="checkbox" name="chk" value="chk" runat="server" onclick="setb2csDeletion(this, '{{pos}}', '{{sply_ty}}', '{{typ}}');" /></td>
                            <%-- <td><button value="Delete Invoice" onclick="setb2csDeletion('{{pos}}','{{sply_ty}}','{{typ}}');" runat="server" onserverclick="Deleteb2cs_Click" >Delete Invoice </button></td>--%>
                            <td>{{pos}}</td>
                            <td>{{txval}}</td>
                            <td>{{typ}}</td>
                            <td>{{camt}}</td>
                            <td>{{samt}}</td>
                            <td>{{iamt}}</td>
                            <td>{{csamt}}</td>
                            <td>{{rt}}</td>
                            <td>{{flag}}</td>
                            <td>{{sply_ty}}</td>
                            <td>{{etin}}</td>
                        </tr>
                {{/each}}
					</tbody>
                        
            </table>



            <%--{{/each}}--%>
            <%-- <br/>
                <button value="Delete Invoice"   runat="server" onserverclick="Deleteb2cs_Click" >Delete Invoice </button>
                <br/><br/>--%>
            <br />
            <input type="hidden" name="hdnb2csForDeletion" id="hdnb2csForDeletion" />
            <button value="Delete Invoices" onclick="deleteInvoiceb2cs();" runat="server" onserverclick="DeleteInvoice_Click">Delete Invoice </button>
            <br />
            <br />
            </div>
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="navigation" runat="server">


            <div class="container-fluid">
                <%--<a href="../Masters/frmMain.aspx" id="brand">--%>
                <asp:HyperLink ID="brand" runat="server" NavigateUrl="~/Masters/frmMain.aspx">
                <img src="../images/CompanyLogo.png" style="display:block;max-width:100%;width:240px;height:50px;margin-top: 6px;" />
                </asp:HyperLink>
                <%--</a>--%>

                <div class="user">
                    <div class="dropdown">
                        <a href="#" class='dropdown-toggle' data-toggle="dropdown">
                            <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>&nbsp;&nbsp;
                        <img src="../images/avatar.png" style="margin-bottom: -5px" />
                        </a>
                        <ul class="dropdown-menu pull-right">

                            <li><a href="#" onclick="RefreshCacheClicked();return false;">Refresh Master Data</a>
                            </li>
                            <li>
                                <asp:LinkButton ID="lnkbtnLogOut" runat="server">Logout</asp:LinkButton>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
         <div class="container-fluid">
            <div class="main-container" style="overflow: hidden">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb" style="padding: 2px !important">
                        <li class="breadcrumb-item">Menu</li>
                        <li class="breadcrumb-item">GSTR1</li>
                        <li class="breadcrumb-item active" aria-current="page">Summary</li>
                    </ol>
                </nav>
            <div id="myJsonDiv" style="display: none" runat="server">
                <%= Session["GSTR1B2CS"].ToString() %>
            </div>

            <div id="dataDiv">
            </div>
            <script type="text/javascript">
                loadData();
            </script>
            <div class="button-contol" style="margin-left: 175px">
                <div class="control-button">
                    <asp:Button ID="Button2" runat="server" Text="Back" class="button-red" OnClick="Button2_Click" />
                </div>


            </div>
                </div>
          
            

        </div>
          <div>
                
                <div id="footerview" runat="server" class="panel-footer footermaster">
                    <div class="col-md-6">
                        <p>This Retail POS Product is best viewed in Google chrome @ 1366 x 768 resolution</p>
                    </div>
                    <div class="col-md-6">
                        <p>&copy 2017 Uniprotech. All rights reserved | Design and Maintained by  <a href="http://uniprotech.co.in/" target="_blank">Uniprotech Solution</a></p>
                    </div>


                </div>
            </div>
    </form>
</body>

</html>
