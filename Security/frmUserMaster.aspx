﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmUserMaster.aspx.cs" Inherits="EnterpriserWebFinal.Security.frmUserMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .UserMasterHideRow {
            display: none;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'UserMaster');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "UserMaster";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


    <script type="text/javascript">
        function Toast() {
            ShowPopupMessageBox("You have no permission to Edit this User");
            return false;
        }
        function DisableEnter(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
            return true;
        }
        function SearchUser(element) {

            var userCode = $('#txtUserCode').val().trim().toLowerCase();
            var userGroup = $('#txtUserGroup').val().trim().toLowerCase();
            var userName = $('#txtUserName').val().trim().toLowerCase();

            //console.log($(element).val());
            //console.log($(element).attr('id'));

            //alert($("[id=trUser],[id=trAlternateUser]").not('[class~=UserMasterHideRow]').length);

            $('[id=trUser],[id=trAlternateUser]').removeClass('UserMasterHideRow');

            $('[id=trUser],[id=trAlternateUser]').each(function () {

                //html() will show & as &amp;
                //console.log($(this).find('td:nth-child(3) > span').html());
                //console.log($(this).find('td:nth-child(3) > span').text());

                if (userCode != '' && $(this).find('td:nth-child(3) > span').text().toLowerCase().indexOf(userCode) < 0) {
                    $(this).addClass('UserMasterHideRow');
                }
                if (userGroup != '' && $(this).find('td:nth-child(4) > span').text().toLowerCase().indexOf(userGroup) < 0) {
                    $(this).addClass('UserMasterHideRow');
                }
                if (userName != '' && $(this).find('td:nth-child(5) > span').text().toLowerCase().indexOf(userName) < 0) {
                    $(this).addClass('UserMasterHideRow');
                }

            });

        }
        function ConfirmDelete(element) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this User");
                return false;
            }
            var userName = $(element).parent().parent().find('td:nth-child(5) > span').text();

            return confirm("Do you want to delete the user - " + userName);
        }

        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkNew.ClientID %>').css("display", "block");
                 }
                 else {
                     $('#<%=lnkNew.ClientID %>').css("display", "none");
                 }
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
            </li>
            <li><a style="text-decoration: none;">Security</a><i class="fa fa-angle-right"></i></li>
            <li class="active-page">User Master</li>  <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>

    <asp:Label ID="lblPathInfo" runat="server"></asp:Label>
    <div class="main-container">
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server" class ="EnableScroll">
            <ContentTemplate>
                <div class="grid-search" style="height: 30px">
                    <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" Width="100px" PostBackUrl="~/Security/frmUserMasterEdit.aspx">Add User</asp:LinkButton>
                </div>
                <div class="gridDetails" style="margin-bottom: 20px;">
                    <asp:Repeater runat="server" ID="rptUsers"
                        OnItemCommand="rptUsers_ItemCommand">
                        <HeaderTemplate>
                            <table class="pshro_GridDgn" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr class="pshro_GridDgnHeaderCellCenter">
                                        <th style='<%# editPermission ? "display:table-cell": "display:none" %>'>
                                            <span>Edit</span>
                                        </th>
                                        <th style='<%# deletePermission ? "display:table-cell": "display:none" %>'>
                                            <span>Delete</span>
                                        </th>
                                        <th>
                                            <span>User Code</span>
                                        </th>
                                        <th>
                                            <span>User Group</span>
                                        </th>
                                        <th>
                                            <span>User Name</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="pshro_GridDgnStyle_Alternative">
                                        <th style='<%# editPermission ? "display:table-cell": "display:none" %>'>
                                            <th style='<%# deletePermission ? "display:table-cell": "display:none" %>'>
                                                <td>
                                                    <%--<asp:TextBox ID="txtUserCode" runat="server" oninput="SearchUser(this)"/>--%>
                                                    <input id="txtUserCode" type="text" oninput="SearchUser(this)" onkeypress="return DisableEnter(event);" />
                                                </td>
                                                <td>
                                                    <%--<asp:TextBox ID="txtUserGroup" runat="server" />--%>
                                                    <input id="txtUserGroup" type="text" oninput="SearchUser(this)" onkeypress="return DisableEnter(event);" />
                                                </td>
                                                <td>
                                                    <%--<asp:TextBox ID="txtUserName" runat="server" />--%>
                                                    <input id="txtUserName" type="text" oninput="SearchUser(this)" onkeypress="return DisableEnter(event);" />
                                                </td>
                                    </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="trUser" class="pshro_GridDgnStyle">
                                <th style='<%# editPermission ? "display:table-cell": "display:none" %>'>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                        ToolTip="Edit" CommandName="Edit" CommandArgument='<%# Eval("UserCode") %>' />
                                    </td>
                                <th style='<%# deletePermission ? "display:table-cell": "display:none" %>'>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/delete.png"
                                        ToolTip="Delete" CommandName="Delete" CommandArgument='<%# Eval("UserCode") %>' OnClientClick="return ConfirmDelete(this);" />
                                    </td>
                                <td>
                                    <asp:Label ID="lblUserCode" runat="server" Text='<%# Eval("UserCode") %>' />
                                </td>
                                    <td>
                                        <asp:Label ID="lblUserGroup" runat="server" Text='<%# Eval("UserGroup") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("UserName") %>' />
                                    </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr id="trAlternateUser" class="pshro_GridDgnStyle_Alternative">
                                <th style='<%# editPermission ? "display:table-cell": "display:none" %>'>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                        ToolTip="Edit" CommandName="Edit" CommandArgument='<%# Eval("UserCode") %>' />
                                    </td>
                                <th style='<%# deletePermission ? "display:table-cell": "display:none" %>'>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/delete.png"
                                        ToolTip="Delete" CommandName="Delete" CommandArgument='<%# Eval("UserCode") %>' OnClientClick="return ConfirmDelete(this);" />
                                    </td>
                                <td>
                                    <asp:Label ID="lblUserCode" runat="server" Text='<%# Eval("UserCode") %>' />
                                </td>
                                    <td>
                                        <asp:Label ID="lblUserGroup" runat="server" Text='<%# Eval("UserGroup") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("UserName") %>' />
                                    </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            <tfoot>
                            </tfoot>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
