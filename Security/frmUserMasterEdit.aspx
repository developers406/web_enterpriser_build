﻿<%@ Page Title="User Edit" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmUserMasterEdit.aspx.cs" Inherits="EnterpriserWebFinal.Security.frmUserMasterEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

      .UserMasterEditHideRow
        {
            display: none;
        }
        .container-left
        {
            float: left;
            width: 32%;
        }
        .container-middle, .container-right
        {
            float: left;
            width: 32%;
            margin-left: 2%;
        }
        .container-group
        {
            width: 100%;
        }
        
        .button-red
        {
            text-shadow:none;
        }
        
        .groupTableTr
        {
            text-align:left;
            padding-left:8px;
        }
        .groupDiv
        {
            height:185px;
            overflow:auto;
            width:101%;
        }
        
        <%--To resize the textbox in chosen dropdown--%>
        li.search-choice
        {
            max-width:95%;
        }
        li.search-choice span
        {
            display:inline-block;
            max-width:100%;
            white-space: nowrap;
            overflow:hidden !important;
            text-overflow: ellipsis;
        }
        li.search-field input
        {
            min-height:60px;
        }
        
        <%--Hide unneeded columns from tblProgramAccess table--%>
        #tblProgramAccess > thead > tr th:nth-child(2),#tblProgramAccess > thead > tr th:nth-child(3)
        {
            display : none;
        }
        #tblProgramAccess > tbody > tr td:nth-child(2),#tblProgramAccess > tbody > tr td:nth-child(3)
        {
            display : none;
        }
        
        <%--Hide unneeded columns from tblActivityAccess table--%>
        #tblActivityAccess > thead > tr th:nth-child(2),#tblActivityAccess > thead > tr th:nth-child(3),#tblActivityAccess > thead > tr th:nth-child(4)
        {
            display : none;
        }
        #tblActivityAccess > tbody > tr td:nth-child(2),#tblActivityAccess > tbody > tr td:nth-child(3),#tblActivityAccess > tbody > tr td:nth-child(4)
        {
            display : none;
        }
        
        #tblProgramAccess > thead > tr th:nth-child(4),#tblProgramAccess > tbody > tr td:nth-child(4)
        {
            text-align:left;
            padding-left:30px;
        }
        #tblActivityAccess > thead > tr th:nth-child(5),#tblActivityAccess > tbody > tr td:nth-child(5)
        {
            text-align:left;
            padding-left:30px;
        }
         .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
                   width: 1%;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
          display : none;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
           display : none;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
           width: 55%;
            text-align:left;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
          width: 4%;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            width: 4%; 
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            width: 4%;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 55px;
            max-width: 55px; 
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
           width: 4%;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 55px;
            max-width: 55px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            width: 4%;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
             width: 4%;
        }
         .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
           width: 4%;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
              width: 4%;
        }

        .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
              width: 4%;
        }

         .grdLoad1 td:nth-child(1), .grdLoad1 th:nth-child(1) {
               width: 4%;
        }

        .grdLoad1 td:nth-child(2), .grdLoad1 th:nth-child(2) {
          display : none;
        }

        .grdLoad1 td:nth-child(3), .grdLoad1 th:nth-child(3) {
           display : none;
        }

        .grdLoad1 td:nth-child(4), .grdLoad1 th:nth-child(4) {
            display : none;
        }

        .grdLoad1 td:nth-child(5), .grdLoad1 th:nth-child(5) {
          min-width: 345px;
            max-width: 345px;
        }

        .grdLoad1 td:nth-child(6), .grdLoad1 th:nth-child(6) {
            min-width: 105px;
            max-width: 105px; 
        }

        .grdLoad1 td:nth-child(7), .grdLoad1 th:nth-child(7) {
            min-width: 325px;
            max-width: 325px;  
        }

        .grdLoad1 td:nth-child(8), .grdLoad1 th:nth-child(8) {
            min-width: 455px;
            max-width: 455px; 
        } 
    </style>
    <script type="text/javascript">

        $(function () {
            //            $("#tabs").removeAttr("style");
            //            $("#tabs").tabs({ disabled: [1] });
            $("#tabs").tabs();

            //Hide Search filter
            $('[href="#tabs-3"]').closest('li').hide();

            if ($('#<%=hdnNewUser.ClientID %>').val() == 'Y') {
                $("#tabs").tabs("option", "disabled", [1]);
            }
            else {
                //Remove mandatory red star * for password fields for existing user edit
                $("#<%=txtPassword.ClientID %> , #<%=txtConfirmPassword.ClientID %> , #<%=txtAuthenticationPassword.ClientID %>").parent().prev().find(".mandatory").css("display", "none");
            }

            //$(".controlgroup").controlgroup()


            $("#<%=ddlUserGroup.ClientID %>,#<%=ddlUserGroup.ClientID %>_chzn,#<%=ddlUserGroup.ClientID %>_chzn > div,#<%=ddlUserGroup.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlDesignation.ClientID %>,#<%=ddlDesignation.ClientID %>_chzn,#<%=ddlDesignation.ClientID %>_chzn > div,#<%=ddlDesignation.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=lstLocation.ClientID %>,#<%=lstLocation.ClientID %>_chzn,#<%=lstLocation.ClientID %>_chzn > div,#<%=lstLocation.ClientID %>_chzn > div > div > input").css("width", '100%');


            //$("#ContentPlaceHolder1_lstLocation").chosen();
            //$("#ContentPlaceHolder1_lstLocation").trigger("liszt:updated");

            //            $('select').on('change', function (evt, params) {
            //                //console.log($(this));
            //                //console.log($(this).find("option:selected").text());
            //                //console.log($(this).find("option:selected").length);
            //            });

            if ($("#<%=txtUserCode.ClientID %>").is("[readonly]")) {
                $("#<%=txtUserName.ClientID %>").focus();
            }
            else {
                $("#<%=txtUserCode.ClientID %>").focus();
            }


            InitializeTableGrouping('tblProgramAccess');
            InitializeTableGrouping('tblActivityAccess');

            InitializeProgramActivityAccess();

            //When clicked on td, checkboc must be checked
            //NOT WORKING
            //            $("#tblProgramAccess td").has("input[type=checkbox]").on("click", function () {
            //                //alert($(this).find("input[type=checkbox]").attr("id"));
            //                if ($(this).find("input[type=checkbox]").prop("checked")) {
            //                    $(this).find("input[type=checkbox]").prop("checked", false);
            //                }
            //                else {
            //                    $(this).find("input[type=checkbox]").prop("checked", true);
            //                }
            //                //$(this).find("input[type=checkbox]").trigger("click");
            //            });

            //            $("#tblProgramAccess input[type=checkbox]").parents("td").on("click", function () {
            //                alert($(this).html());
            //                //$(this).find("input[type=checkbox]").click();
            //            });

            //ToggleTableGroupingByGroupId('tblProgramAccess', 'POS');

            //alert($("#tblProgramAccess input[id*=chkAccess]").length);
            $("#tblProgramAccess input[id*=chkAccess]").on("change", checkboxProgramAccessChanged);
            $("#tblProgramAccess input[type=checkbox]").not("input[id*=chkAccess]").on("click", checkboxProgramOthersChanged);

            $("#tblActivityAccess input[id*=chkSelect]").on("change", function () {
                var tr = $(this).closest("tr");

                if (!$(this).prop("checked")) {

                    tr.find("input[id*=chkAuthenticationRequired]").prop("checked", false);

                    tr.find("input[id*=txtAuthenticationLevel]").val('');
                }
            });

            $("#tblActivityAccess input[id*=chkAuthenticationRequired]").on("click", function () {
                var tr = $(this).closest("tr");

                if (!tr.find("input[id*=chkSelect]").prop("checked")) {
                    return false;
                }

                if (tr.find("input[id*=chkAuthenticationRequired]").prop("checked")) {
                    tr.find("input[id*=txtAuthenticationLevel]").focus();
                }
                else {
                    tr.find("input[id*=txtAuthenticationLevel]").val('');
                }


            });


            //Disabled all input fields for Demo user
            //$('#tblProgramAccess input,#tblActivityAccess input').attr("disabled", true);            

            $(".main-container").show();
        });

        function DisableAccessMapping() {
            $('#tblProgramAccess input,#tblActivityAccess input').attr("disabled", true);            
        }
        function AuthenticationLevelTextChanged(element) {
            var tr = $(element).closest("tr");

            //console.log(tr.find("input[id*=chkSelect]"));

            if (!tr.find("input[id*=chkSelect]").prop("checked") || !tr.find("input[id*=chkAuthenticationRequired]").prop("checked")) {                
                return false;
            }
            else {
                return true;
            }   
        }
        function InitializeProgramActivityAccess() {
            $("#tblProgramAccess span[id*=lblProgramCode]").each(function () {
                if (!$(this).closest("tr").find("input[id*=chkAccess]").prop("checked")) {
                    HideAndUnCheckActivityGroup($(this).text());
                }
            });
        }
        function checkboxProgramOthersChanged() {
            var tr = $(this).closest("tr");

            if (!tr.find("input[id*=chkAccess]").prop("checked") && !$(this).attr("id").endsWith("_ChkAll")) {
                return false;
            }            
        }
        function checkboxProgramAccessChanged() {
            var tr = $(this).closest("tr");
            var programCode = tr.find("span[id*=lblProgramCode]").text();

            if ($(this).prop('checked')) {
                tr.find("input[type=checkbox]:enabled").not("input[id*=chkAccess]").prop("checked", true);
                
                //ShowTableGroupingByGroupId('tblActivityAccess', programCode);
                ShowActivityGroup(programCode) 
            }
            else {
                tr.find("input[type=checkbox]").not("input[id*=chkAccess]").prop("checked", false);
                
                HideAndUnCheckActivityGroup(programCode);
            }
        }

        function ShowActivityGroup(groupId) {
            ShowTableGroupingByGroupId('tblActivityAccess', groupId);
        }
        function HideAndUnCheckActivityGroup(groupId) {
            
            var tableGroupTr = $("#tblActivityAccess > tbody tr[data-tablegroupid=" + groupId + "]");
            
            if (tableGroupTr.length > 0) {
                
                HideTableGroupingByGroupId('tblActivityAccess', groupId);

                tableGroupTr.each(function () {
                    $(this).find("input[type=checkbox]").prop("checked", false);
                    $(this).find("input[type=text]").val('');

                    //$(this).find("input[id*=_chkSelect_]").prop("checked", false);
                });
            }
            
        }
        function InitializeTableGrouping(tableId) {
            //alert($("#" + tableId + " th").length);
            
            var columnLength = $("#" + tableId + " th").length;

            var dataAttrGroupId = 'data-tablegroupid';
            var dataAttrGroupName = 'data-tablegroupname';

            $("#" + tableId + " > tbody").children().each(function () {

                //var groupSpanId = 'lblRoleName'
                //var currentText = currentRow.find("span[id*=" + groupSpanId + "]").text();
                //var prevText = prevRow.find("span[id*=" + groupSpanId + "]").text();

                var currentRow = $(this);
                var prevRow = $(this).prev();

                var currentGroupId = currentRow.attr(dataAttrGroupId);
                var currentGroupName = currentRow.attr(dataAttrGroupName);
                var prevGroupId = prevRow.attr(dataAttrGroupId);

                if (currentGroupId != prevGroupId) {

                    var groupTrId = "trGroup_" + tableId + "_" + currentGroupId.replace(/\s/g, '');

                    var groupChkId = groupTrId + '_ChkAll';
                    //columnLength = columnLength - 1 //Adding second column for select All checkbox
                    currentRow.before("<tr id='" + groupTrId + "'><td colspan='" + columnLength + "' style='text-align:left;padding-left:8px;'><span style='cursor:pointer;' class='ui-icon ui-icon-circle-minus' ></span>" + currentGroupName + " ( All <input id='" + groupChkId + "' type='checkbox' style='vertical-align: top;'> )</td></tr>");

                    if ($('#<%=hdnDisable.ClientID %>').val() == 'Y') {
                        DisableAccessMapping();
                    }

                    $("#" + groupTrId + " span").on("click", null, { tableId: tableId, groupId: currentGroupId }, ToggleGroupDisplay);

                    $("#" + groupChkId).on("change", null, { groupId: currentGroupId }, function (event) {

                        //alert(event.data.groupId);

                        var trList = $(event.target).closest("tbody").find("tr[data-tablegroupid='" + event.data.groupId + "']")

                        //alert(trList.length);
                        //$("#tblProgramAccess input[id*=chkAccess]").on("change", checkboxProgramAccessChanged);
                        trList.each(function () {
                            var chkSelect = $(this).find("input[id*=chkSelect]");
                            var chkAccess = $(this).find("input[id*=chkAccess]");

                            if (chkSelect.length > 0) {
                                chkSelect.prop("checked", $(event.target).prop("checked"));
                            }
                            else {
                                chkAccess.prop("checked", $(event.target).prop("checked"));
                                checkboxProgramAccessChanged.call(chkAccess);
                            }
                        });

                        //trList.find("input[type=checkbox]:enabled").prop("checked", $(event.target).prop("checked"));
                    });

                    //console.log(prevText + '>>' + currentText);
                }
            });
        }
        function HideTableGroupingByGroupId(tableId, groupId) {
            CollapseTableGroupingByGroupId(tableId, groupId);

            var groupTrId = "trGroup_" + tableId + "_" + groupId.replace(/\s/g, '');
            $("#" + groupTrId).hide();
            
        }
        function ShowTableGroupingByGroupId(tableId, groupId) {
            ExpandTableGroupingByGroupId(tableId, groupId);

            var groupTrId = "trGroup_" + tableId + "_" + groupId.replace(/\s/g, '');
            $("#" + groupTrId).show();
        }
        function CollapseTableGroupingByGroupId(tableId, groupId) {
            
            var groupTrId = "trGroup_" + tableId + "_" + groupId.replace(/\s/g, '');
            var groupHeadElement = $("#" + groupTrId + " span");
            var minusClass = 'ui-icon-circle-minus';
            var plusClass = 'ui-icon-circle-plus';
            var fadeOutTime = 100;
            var fadeInTime = 300;

            var tr = $("#" + tableId + " > tbody tr[data-tablegroupid='" + groupId + "']");
           
            //tr.css("display", "none");
            tr.fadeOut(fadeOutTime);
            groupHeadElement.removeClass(minusClass)
            groupHeadElement.addClass(plusClass)
           
          
        }
        function ExpandTableGroupingByGroupId(tableId, groupId) {

            var groupTrId = "trGroup_" + tableId + "_" + groupId.replace(/\s/g, '');
            var groupHeadElement = $("#" + groupTrId + " span");
            var minusClass = 'ui-icon-circle-minus';
            var plusClass = 'ui-icon-circle-plus';
            var fadeOutTime = 100;
            var fadeInTime = 300;            

            var tr = $("#" + tableId + " > tbody tr[data-tablegroupid='" + groupId + "']");

            //tr.css("display", "table-row");
            tr.fadeIn(fadeInTime);
            groupHeadElement.removeClass(plusClass)
            groupHeadElement.addClass(minusClass)

        }

        function ToggleTableGroupingByGroupId(tableId, groupId) {
            //alert(tableId); alert(groupId);
            var groupTrId = "trGroup_" + tableId + "_" + groupId.replace(/\s/g, '');
            var groupHeadElement = $("#" + groupTrId + " span");
            var minusClass = 'ui-icon-circle-minus';
            var plusClass = 'ui-icon-circle-plus';
            var fadeOutTime = 100;
            var fadeInTime = 300;

            //console.log(event.data.tableId);

            var tr = $("#" + tableId + " > tbody tr[data-tablegroupid='" + groupId + "']");

            if (groupHeadElement.hasClass(minusClass)) {
                //tr.css("display", "none");
                tr.fadeOut(fadeOutTime);
                groupHeadElement.removeClass(minusClass)
                groupHeadElement.addClass(plusClass)
            }
            else {
                //tr.css("display", "table-row");
                tr.fadeIn(fadeInTime);
                groupHeadElement.removeClass(plusClass)
                groupHeadElement.addClass(minusClass)
            }
        }
        function ToggleGroupDisplay(event) {

            ToggleTableGroupingByGroupId(event.data.tableId, event.data.groupId);


            //            var minusClass = 'ui-icon-circle-minus';
            //            var plusClass = 'ui-icon-circle-plus';
            //            var fadeOutTime = 100;
            //            var fadeInTime = 500;

            //            console.log(event.data.tableId);

            //            var tr = $("#" + event.data.tableId + " > tbody tr[data-tablegroupid='" + event.data.groupId + "']");

            //            //alert($(this).hasClass('ui-icon-circle-minus'));

            //            if ($(this).hasClass(minusClass)) {
            //                //tr.css("display", "none");
            //                tr.fadeOut(fadeOutTime);
            //                $(this).removeClass(minusClass)
            //                $(this).addClass(plusClass)
            //            }
            //            else {
            //                //tr.css("display", "table-row");
            //                tr.fadeIn(fadeInTime);
            //                $(this).removeClass(plusClass)
            //                $(this).addClass(minusClass)
            //            }
        }
        function DisableEnter(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
            return true;
        }

        function ConfirmDelete(element) {

            var userName = $(element).parent().parent().find('td:nth-child(5) > span').text();

            return confirm("Do you want to delete the user - " + userName);
        }
        function ValidateTextInput(event,element) {
            //console.log(event);
            //if($(element).val().length > $(element).attr("maxlength")
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

                if (keyCode != 13) {
                    if ($(element).attr("id") == '<%=txtExpiryDays.ClientID %>') {

                        if (!$.isNumeric(event.key)) {
                            return false;
                        }
                    }
                    if ($(element).is("input[id*=txtAuthenticationLevel]") || $(element).is("input[id*=txtUserLevel]")) {
                        if ($.isNumeric(event.key)) {
                            if (parseInt(event.key) > 5) {
                                return false;
                            }
                        }
                        else {
                            return false;
                        }
                    }
                }
                if (keyCode == 13) {
                    if ($(element).attr("id") == '<%=txtUserCode.ClientID %>') {
                        $('#<%=txtUserName.ClientID %>').focus();
                    }
                    else if ($(element).attr("id") == '<%=txtUserName.ClientID %>') {
                        $('#<%=txtLoginName.ClientID %>').focus();
                    }
                    else if ($(element).attr("id") == '<%=txtLoginName.ClientID %>') {
                        $('#<%=txtPassword.ClientID %>').focus();
                    }
                    else if ($(element).attr("id") == '<%=txtPassword.ClientID %>') {
                        $('#<%=txtConfirmPassword.ClientID %>').focus();
                    }
                    else if ($(element).attr("id") == '<%=txtConfirmPassword.ClientID %>') {
                        $('#<%=txtExpiryDays.ClientID %>').focus();
                    }
                    else if ($(element).attr("id") == '<%=txtExpiryDays.ClientID %>') {
                        $('#<%=txtUserLevel.ClientID %>').focus();
                    }
                    else if ($(element).attr("id") == '<%=txtUserLevel.ClientID %>') {
                        $('#<%=txtAuthenticationPassword.ClientID %>').focus();
                    }
                    else if ($(element).attr("id") == '<%=txtAuthenticationPassword.ClientID %>') {
                        $('#<%=ddlUserGroup.ClientID %>').trigger('liszt:activate');
                    }                    
                    else if ($(element).attr("id") == '<%=txtPhoneNo.ClientID %>') {
                        $('#<%=txtEmail.ClientID %>').focus();
                    }
                    else if ($(element).attr("id") == '<%=txtEmail.ClientID %>') {                        
                        $('#<%=lstLocation.ClientID %>').trigger('liszt:activate');
                    }
                    return false; 
                }
                else {
                    //console.log($(element).val().length);
                    //console.log($(element).attr("maxlength"));
                    if ($(element).val().length+1 > $(element).attr("maxlength")) {
                        return false;
                    }
                }
            }
            catch (err) {
                alert(err.Message);
            }
            return true;
        }
        function ValidateUserSave() {

            if ($('#<%=txtUserCode.ClientID %>').val() == '') {
                alert('Please enter User Code');
                $('#<%=txtUserCode.ClientID %>').focus();
                return false;
            }
            if ($('#<%=txtUserName.ClientID %>').val() == '') {
                alert('Please enter User Name');
                $('#<%=txtUserName.ClientID %>').focus();
                return false;
            }
            if ($('#<%=txtLoginName.ClientID %>').val() == '') {
                alert('Please enter Login name');
                $('#<%=txtLoginName.ClientID %>').focus();
                return false;
            }

            var password = $('#<%=txtPassword.ClientID %>').val();
            var confirmPassword = $('#<%=txtConfirmPassword.ClientID %>').val();

            if (password != '' && password != confirmPassword) {
                alert('Password and confirm password must match');
                $('#<%=txtPassword.ClientID %>').focus();
                return false;
            }

            if (password == '' && $('#<%=hdnNewUser.ClientID %>').val() == 'Y') {                
                alert('Please enter password');
                $('#<%=txtPassword.ClientID %>').focus();
                return false;
            }

            if ($('#<%=txtExpiryDays.ClientID %>').val() == '') {
                alert('Please enter Expiry Days');
                $('#<%=txtExpiryDays.ClientID %>').focus();
                return false;
            }
            if ($('#<%=txtUserLevel.ClientID %>').val() == '') {
                alert('Please enter User Level');
                $('#<%=txtUserLevel.ClientID %>').focus();
                return false;
            }
            if ($('#<%=txtAuthenticationPassword.ClientID %>').val() == '' && $('#<%=hdnNewUser.ClientID %>').val() == 'Y') {
                alert('Please enter Authentication Password');
                $('#<%=txtAuthenticationPassword.ClientID %>').focus();
                return false;
            }
            if ($('#<%=ddlUserGroup.ClientID %>').val() == '') {
                alert('Please select User Group');
                $('#<%=ddlUserGroup.ClientID %>').trigger('liszt:activate');
                //$('#<%=ddlUserGroup.ClientID %>').focus();
                return false;
            }
            if ($('#<%=ddlDesignation.ClientID %>').val() == '') {
                alert('Please select Designation');
                $('#<%=ddlDesignation.ClientID %>').focus();
                return false;
            }

            if ($('#<%=txtEmail.ClientID %>').val() != '') {
                if (!isValidEmailAddress($('#<%=txtEmail.ClientID %>').val())) {
                    alert('Please enter valid email Id');
                    $('#<%=txtEmail.ClientID %>').focus();
                    return false;
                }
            }            

            return true;
        }
        function UserGroupChanged() {
            //console.log($('#<%=ddlUserGroup.ClientID %>').val());
            //console.log($('#<%=hdnUserGroup.ClientID %>').val());
            if ($('#<%=hdnNewUser.ClientID %>').val() == 'N') {

                if ($('#<%=ddlUserGroup.ClientID %>').val() != $('#<%=hdnUserGroup.ClientID %>').val()) {
                    if (confirm('Changing User group will reset permissions?')) {
                        $("#tabs").tabs("option", "disabled", [1]);
                    }
                    else {                        
                        $('#<%=ddlUserGroup.ClientID %>').val($('#<%=hdnUserGroup.ClientID %>').val());                        
                        $('#<%=ddlUserGroup.ClientID %>').trigger("liszt:updated");
                    }
                } else {
                    $("#tabs").tabs("option", "disabled", []);
                }
            }            
        }

      <%--  ///Save Popup
        function fncShowSaveDailog() {
            
            $(function () {
                $("#dialog-UserMessageBox").html('<%=Resources.LabelCaption.lbl_usersaved%>');
                $("#dialog-UserMessageBox").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        };--%>

        /////Save Popup
        function fncInitializeSaveDialog() {
            try {                               
                $("#dialog-UserMessageBox").dialog({
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function Toast() {
            try{
                ShowPopupMessageBox("Already two user Profiles are created");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'UserMasterEdit');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "UserMasterEdit";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
            </li>
            <li><a href="../Security/frmUserMaster.aspx">User Master</a> <i class="fa fa-angle-right">
            </i></li>
            <li class="active-page">Edit User</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <asp:HiddenField ID="hdnNewUser" runat="server" />
    <asp:HiddenField ID="hdnDisable" runat="server" />
    <div class="main-container" style="display:none">
        <div id="tabs" class ="EnableScroll">
            <ul>
                <li><a href="#tabs-1">User Details</a></li>
                <li><a href="#tabs-2">Form Access and Activity Mapping</a></li>
                <li><a href="#tabs-3">Search Filter Settings</a></li>
            </ul>
            <div id="tabs-1">
                <fieldset>
                    <legend>User Details -
                        <asp:Label runat="server" ID="lblStatus" />
                         <asp:Label runat="server" ID="lblUserCount" Font-Bold ="true"  Text ="You have Reached Licensed level Users" style ="margin-left: 265px;background: darkturquoise;font-family: none;" Visible ="false" />
                    </legend>
                    <div class="controlgroup">
                        <div class="container-group">
                            <div class="container-control">
                                <div class="container-left">
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label1" runat="server" Text="User Code"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtUserCode" runat="server" ReadOnly="true" CssClass="form-control-res" onkeypress="return ValidateTextInput(event,this)" MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label2" runat="server" Text="User Name"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control-res" onkeypress="return ValidateTextInput(event,this)" MaxLength="75"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label4" runat="server" Text="Login Name"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtLoginName" runat="server" CssClass="form-control-res" onkeypress="return ValidateTextInput(event,this)" MaxLength="75"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text="Password"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control-res" onkeypress="return ValidateTextInput(event,this)" MaxLength="75"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label3" runat="server" Text="Confirm Password"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" CssClass="form-control-res" onkeypress="return ValidateTextInput(event,this)" MaxLength="75"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-middle">
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label7" runat="server" Text="Expiry Days"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtExpiryDays" runat="server" CssClass="form-control-res" onkeypress="return ValidateTextInput(event,this)" MaxLength="5"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label8" runat="server" Text="User Level(1-5)"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtUserLevel" runat="server" CssClass="form-control-res" 
                                                TextMode="Number" min="1" max="5"  onkeypress="return ValidateTextInput(event,this)" MaxLength="1"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label5" runat="server" Text="Authentication Password"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtAuthenticationPassword" runat="server" TextMode="Password" CssClass="form-control-res" onkeypress="return ValidateTextInput(event,this)" MaxLength="75"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label6" runat="server" Text="User Group"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:HiddenField ID="hdnUserGroup" runat="server" />
                                            <asp:DropDownList runat="server" ID="ddlUserGroup" CssClass="form-control-res" data-placeholder="Select User Group" onchange="UserGroupChanged();">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label9" runat="server" Text="Designation"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:DropDownList runat="server" ID="ddlDesignation" CssClass="form-control-res"
                                                data-placeholder="Select Designation">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-right">
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label11" runat="server" Text="Phone No"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPhoneNo" runat="server" CssClass="form-control-res" onkeypress="return ValidateTextInput(event,this)" MaxLength="30"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label12" runat="server" Text="Email"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control-res" TextMode="Email" onkeypress="return ValidateTextInput(event,this)" MaxLength="30"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label13" runat="server" Text="Location"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <%--<asp:DropDownList runat="server" ID="ddlLocation" CssClass="form-control-res" multiple>
                                            </asp:DropDownList>--%>
                                            <asp:ListBox ID="lstLocation" Rows="4" SelectionMode="multiple" runat="server" data-placeholder=" " />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-contol">
                        <div class="control-button" id ="divSave" runat ="server">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateUserSave()">Save</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkBack" runat="server" class="button-red" OnClick="lnkBack_Click">Back</asp:LinkButton>
                        </div>
                        <div class="control-button" runat="server" visible="false">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearFormIn()">Clear</asp:LinkButton>
                        </div>
                        <div class="control-button" runat="server" visible="false">
                            <asp:LinkButton ID="lnkDelete" runat="server" class="button-red" OnClientClick="return DeleteClicked()">Delete</asp:LinkButton>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div id="tabs-2">
                <%--<fieldset>
                    <legend>Form Wise Access</legend>--%>
                <p>
                    <strong>Form Wise Access</strong></p>
                <div class="container-group">
                    <div class="container-control"> 
                         <div class="grdLoad">
                            <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                <thead>
                                    <tr>
                                        <th scope="col">
                                        </th>
                                        <th scope="col">Role
                                        </th>
                                        <th scope="col">Program Code
                                        </th>
                                        <th scope="col">Program Name
                                        </th>
                                        <th scope="col">Access
                                        </th>
                                        <th scope="col">New
                                        </th>
                                        <th scope="col">Edit
                                        </th>
                                        <th scope="col">Delete
                                        </th>
                                        <th scope="col">View
                                        </th>
                                        <th scope="col">Print
                                        </th>
                                        <th scope="col">Preview
                                        </th>
                                        
                                        <th scope="col">Cancel
                                        </th>
                                        <th scope="col">Authorise
                                        </th>
                                        <th scope="col">Amend
                                        </th>
                                        <th scope="col">Copy
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="groupDiv"> 
                            <div class="gridDetails">
                                <asp:Repeater runat="server" ID="rptProgramAccess" OnItemDataBound="rptProgramAccess_OnItemDataBound">
                                    <HeaderTemplate>
                                        <table id="tblProgramAccess" class="pshro_GridDgn" cellspacing="0" rules="all" border="1">
                                            <thead>
                                                <tr class="pshro_GridDgnHeaderCellCenter fixed_header display_none">
                                                    <th>
                                                        
                                                    </th>
                                                    <th>
                                                        Role
                                                    </th>
                                                    <th>
                                                        <span>Program Code</span>
                                                    </th>
                                                    <th>
                                                        <span>Program Name</span>
                                                    </th>
                                                    <th>
                                                        <span>Access</span>
                                                    </th>
                                                    <th>
                                                        <span>New</span>
                                                    </th>
                                                    <th>
                                                        <span>Edit</span>
                                                    </th>
                                                    <th>
                                                        <span>Delete</span>
                                                    </th>
                                                    <th>
                                                        <span>View</span>
                                                    </th>
                                                    <th>
                                                        <span>Print</span>
                                                    </th>
                                                    <th>
                                                        <span>Preview</span>
                                                    </th>
                                                    <th>
                                                        <span>Cancel</span>
                                                    </th>
                                                    <th>
                                                        <span>Authorise</span>
                                                    </th>
                                                    <th>
                                                        <span>Amend</span>
                                                    </th>
                                                    <th>
                                                        <span>Copy</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="trProgram" class="pshro_GridDgnStyle" data-tablegroupid='<%# Eval("RH_CODE") %>'
                                            data-tablegroupname='<%# Eval("RH_NAME") %>'>
                                            <td>
                                            </td>
                                            <td>
                                                <%--class="UserMasterEditHideRow"--%>
                                                <asp:Label ID="lblRoleName" runat="server" Text='<%# Eval("RH_NAME") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblProgramCode" runat="server" Text='<%# Eval("PM_PROGRAM_ID") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblProgram" runat="server" Text='<%# Eval("PM_PROGRAM_NAME") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkAccess" Checked='<%# Eval("Access") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkNew" Checked='<%# Eval("New") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"N") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkEdit" Checked='<%# Eval("Edit") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"E") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkDelete" Checked='<%# Eval("Delete") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"D") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkView" Checked='<%# Eval("View") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"V") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkPrint" Checked='<%# Eval("Print") %>' Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"P") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkPreview" Checked='<%# Eval("PPView") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"I") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkCancel" Checked='<%# Eval("Cancel") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"C") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkAuthorise" Checked='<%# Eval("Authorise") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"A") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkAmend" Checked='<%# Eval("Amend") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"M") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkCopy" Checked='<%# Eval("Copy") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"O") %>'/>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                         </div>
                    </div>
                </div>
                <%-- </fieldset>--%>
                <%-- <fieldset>
                    <legend>User Activity Mapping</legend>--%>
                <div style="padding: 5px 0 5px 0;">
                    <strong>Activity Mapping</strong></div>
                <div class="container-group">
                    <div class="container-control">
                         <div class="grdLoad1">
                        <table id="tblItemh" cellspacing="0" rules="all" border="1" class="grdLoad1 fixed_header">
                                <thead>
                                    <tr class="pshro_GridDgnHeaderCellCenter">
                                        <th scope="col">
                                        </th> 
                                        <th scope="col">Program Code
                                        </th>
                                        <th scope="col">Program 
                                        </th>
                                        <th scope="col">Activity Code
                                        </th>
                                        <th scope="col">Activity
                                        </th>
                                        <th scope="col">Select
                                        </th>
                                        <th scope="col">Authentication Required
                                        </th>
                                        <th scope="col">Authentication Level
                                        </th> 
                                    </tr>
                                </thead>
                            </table>
                             
                        <div class="groupDiv">
                            <div class="gridDetails">
                                <asp:Repeater runat="server" ID="rptActivityAccess">
                                    <HeaderTemplate>
                                        <table id="tblActivityAccess" class="pshro_GridDgn" cellspacing="0" rules="all" border="1">
                                            <thead >
                                                <tr class="pshro_GridDgnHeaderCellCenter display_none">
                                                    <th>
                                                        
                                                    </th>
                                                    <th>
                                                        Program Code
                                                    </th>
                                                    <th>
                                                        Program
                                                    </th>
                                                    <th>
                                                        Activity Code
                                                    </th>
                                                    <th>
                                                        <span>Activity</span>
                                                    </th>
                                                    <th>
                                                        <span>Select</span>
                                                    </th>
                                                    <th>
                                                        <span>Authentication Required</span>
                                                    </th>
                                                    <th>
                                                        <span>Authentication Level</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="trActivity" class="pshro_GridDgnStyle" data-tablegroupid='<%# Eval("PM_PROGRAM_ID") %>'
                                            data-tablegroupname='<%# Eval("PM_PROGRAM_NAME") %>'>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblProgramCode" runat="server" Text='<%# Eval("PM_PROGRAM_ID") %>' />
                                            </td>
                                            <td>
                                                <%--class="UserMasterEditHideRow"--%>
                                                <asp:Label ID="lblProgramName" runat="server" Text='<%# Eval("PM_PROGRAM_NAME") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblActivityCode" runat="server" Text='<%# Eval("PAM_ACTIVITY_CODE") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblActivity" runat="server" Text='<%# Eval("PAM_ACTIVITY_DESC") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkSelect" Checked='<%# Eval("Access") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkAuthenticationRequired" Checked='<%# Eval("UAS_AUTHENTICATION_FLAG") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAuthenticationLevel" runat="server" CssClass="form-control-res"  onkeydown="return AuthenticationLevelTextChanged(this)" onkeypress="return ValidateTextInput(event,this);"
                                                    Text='<%# Eval("UAS_AUTHENTICATION_LEVEL") %>' MaxLength="1"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                          </div>
                    </div>
                </div>
                <%--</fieldset>--%>
            </div>
            <div id="tabs-3">
                <%--<fieldset>
                    <legend>Form Wise Access</legend>--%>
                <p><strong>Search Filter Settings</strong></p>
                <div class="container-group">
                    <div class="container-control">
                        <div class="groupDiv">
                            <div class="gridDetails">
                                <asp:Repeater runat="server" ID="rptSearchFilterSettings" OnItemDataBound="rptSearchFilterSettings_OnItemDataBound">
                                    <HeaderTemplate>
                                        <table id="tblSearchFilterSettings" class="pshro_GridDgn" cellspacing="0" rules="all" border="1">
                                            <thead>
                                                <tr class="pshro_GridDgnHeaderCellCenter">
                                                    <th>
                                                        SL
                                                    </th>
                                                    <th>
                                                        Role
                                                    </th>
                                                    <th>
                                                        <span>Program Code</span>
                                                    </th>
                                                    <th>
                                                        <span>Program Name</span>
                                                    </th>
                                                    <th>
                                                        <span>Access</span>
                                                    </th>
                                                    <th>
                                                        <span>Vendor</span>
                                                    </th>
                                                    <th>
                                                        <span>Department</span>
                                                    </th>
                                                    <th>
                                                        <span>Category</span>
                                                    </th>
                                                    <th>
                                                        <span>SubCategory</span>
                                                    </th>
                                                    <th>
                                                        <span>Brand</span>
                                                    </th>
                                                    <th>
                                                        <span>Class</span>
                                                    </th>
                                                    <th>
                                                        <span>SubClass</span>
                                                    </th>
                                                    <th>
                                                        <span>Merchandise</span>
                                                    </th>
                                                    <th>
                                                        <span>Manufacture</span>
                                                    </th>
                                                    <th>
                                                        <span>Floor</span>
                                                    </th>
                                                    <th>
                                                        <span>Section</span>
                                                    </th>
                                                    <th>
                                                        <span>Bin</span>
                                                    </th>
                                                    <th>
                                                        <span>Shelf</span>
                                                    </th>
                                                    <th>
                                                        <span>Warehouse</span>
                                                    </th>
                                                    <th>
                                                        <span>Barcode</span>
                                                    </th>
                                                    <th>
                                                        <span>BatchNo</span>
                                                    </th>
                                                    <th>
                                                        <span>ItemCode</span>
                                                    </th>
                                                    <th>
                                                        <span>ItemName</span>
                                                    </th>
                                                    <th>
                                                        <span>ItemType</span>
                                                    </th>
                                                    <th>
                                                        <span>GRNNo</span>
                                                    </th>
                                                    <th>
                                                        <span>DisplayCode</span>
                                                    </th>
                                                    <th>
                                                        <span>Price</span>
                                                    </th>
                                                    <th>
                                                        <span>Date Range</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="trSearchFilterSettings" class="pshro_GridDgnStyle" data-tablegroupid='<%# Eval("RH_CODE") %>'
                                            data-tablegroupname='<%# Eval("RH_NAME") %>'>
                                            <td>
                                            </td>
                                            <td>
                                                <%--class="UserMasterEditHideRow"--%>
                                                <asp:Label ID="lblRoleName" runat="server" Text='<%# Eval("RH_NAME") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblProgramCode" runat="server" Text='<%# Eval("PM_PROGRAM_ID") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblProgram" runat="server" Text='<%# Eval("PM_PROGRAM_NAME") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkAccess" Checked='<%# Eval("Access") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkNew" Checked='<%# Eval("New") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"N") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkEdit" Checked='<%# Eval("Edit") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"E") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkDelete" Checked='<%# Eval("Delete") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"D") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkView" Checked='<%# Eval("View") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"V") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkPrint" Checked='<%# Eval("Print") %>' Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"P") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkPreview" Checked='<%# Eval("PPView") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"I") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkCancel" Checked='<%# Eval("Cancel") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"C") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkAuthorise" Checked='<%# Eval("Authorise") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"A") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkAmend" Checked='<%# Eval("Amend") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"M") %>'/>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkCopy" Checked='<%# Eval("Copy") %>'  Enabled='<%# !IsControlDisabled(Eval("PM_PROGRAM_ACCESS_CONTROL").ToString(),"O") %>'/>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hiddencol">
    <div id="dialog-UserMessageBox">
        <div>
            <asp:Label runat="server" ID="lblSave" Text='<%$ Resources:LabelCaption,lbl_usersaved %>' ></asp:Label>             
        </div>
         <div class="dialog_center">              
                        <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                           OnClick="lnkbtnOk_Click"> </asp:LinkButton>                    
            </div>
        </div>
        </div>
</asp:Content>
