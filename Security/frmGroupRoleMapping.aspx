﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmGroupRoleMapping.aspx.cs" Inherits="EnterpriserWebFinal.Security.frmGroupRoleMapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

       
    .UserMasterHideRow
    {
        display:none;
    }
</style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'GroupRoleMapping');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "GroupRoleMapping";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

<script type="text/javascript">
     function pageLoad() {
            try{
                 if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkNew.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkNew.ClientID %>').css("display", "none");
             }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    function DisableEnter(event) {
        try {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode == 13) {
                return false;
            }
        }
        catch (err) {
            alert(err.Message);
        }
        return true;
    }
    function GRM_Delete() {
         if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
             ShowPopupMessageBox("You have no permission to Delete this Group");
                return false;
            } 
        if (confirm("Are You Sure do You want to delete?")) {
            return;
        } else {
            return false;
        }

    }
    function SearchUser(element) {

        var userCode = $('#txtGroupCode').val().trim().toLowerCase();
        var userName = $('#txtGroupName').val().trim().toLowerCase();
//        alert(userCode);
        

//        console.log($(element).val());
//        console.log($(element).attr('id'));

        //alert($("[id=trUser],[id=trAlternateUser]").not('[class~=UserMasterHideRow]').length);

        $('[id=trUser]').removeClass('UserMasterHideRow');

        $('[id=trUser]').each(function () {
            
            //html() will show & as &amp;
            console.log($(this).find('td:nth-child(3) > span').html());
            console.log($(this).find('td:nth-child(3) > span').text());

            if (userCode != '' && $(this).find('td:nth-child(3) > span').text().toLowerCase().indexOf(userCode) < 0) {
               
                $(this).addClass('UserMasterHideRow');
                
            }
            if (userName != '' && $(this).find('td:nth-child(4) > span').text().toLowerCase().indexOf(userName) < 0) {
               
                $(this).addClass('UserMasterHideRow');
            }
           

        });

    }
    function Toast() {
        ShowPopupMessageBox("You have no permission to Edit this Group");
        return false;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
            </li>
             <li><a style="text-decoration:none;">Security</a><i class="fa fa-angle-right"></i></li>
            <li class="active-page">Group Role Mapping</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <div class="main-container">
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
            <br />
                <div class = "col-md-2">
                </div>
                <div class = "col-md-8">
                <div class="grid-search" style="height: 30px">
                        <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" Width="100px" PostBackUrl="~/Security/frmGroupRoleMappingEdit.aspx">New</asp:LinkButton>
                    </div>
                    <br />
                <div class="gridDetails">                    
                    <asp:Repeater runat="server" ID="rptUsers" 
                        onitemcommand="rptGroupRoleMapping_ItemCommand" >
                        <HeaderTemplate>
                            <table class="pshro_GridDgn" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr class="pshro_GridDgnHeaderCellCenter">
                                        <th>
                                          <span>Delete</span>
                                        </th>
                                        <th>
                                            <span>Edit</span>
                                        </th>
                                        <th>
                                            <span>Group Code</span>
                                        </th>
                                        <th>
                                            <span>Group Name</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="pshro_GridDgnStyle_Alternative">
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <%--<asp:TextBox ID="txtUserGroup" runat="server" />--%>
                                            <input id="txtGroupCode" type="text" oninput="SearchUser(this)" onkeypress="return DisableEnter(event);"/>
                                        </td>
                                        <td>
                                            <%--<asp:TextBox ID="txtUserName" runat="server" />--%>
                                            <input id="txtGroupName" type="text" oninput="SearchUser(this)" onkeypress="return DisableEnter(event);"/>
                                        </td>
                                    </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="trUser" class="pshro_GridDgnStyle">
                                <td>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/no.png" OnClientClick=" return GRM_Delete()"
                                        ToolTip="Delete" CommandName="Delete" CommandArgument='<%# Eval("GroupCode") %>'  />
                                </td>
                                <td>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                        ToolTip="Edit" CommandName="Edit" CommandArgument='<%# Eval("GroupCode") %>'  />
                                </td>
                                <td>
                                    <asp:Label ID="lblGroupCode" runat="server" Text='<%# Eval("GroupCode") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblGroupName" runat="server" Text='<%# Eval("GroupName") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        
                        <FooterTemplate>
                            </tbody>
                            <tfoot>
                            </tfoot>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                </div>
                <div class = "col-md-2">
                </div>   
            </ContentTemplate>
        </asp:UpdatePanel>
    </div> 
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
