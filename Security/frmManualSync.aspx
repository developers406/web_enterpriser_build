﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmManualSync.aspx.cs" Inherits="EnterpriserWebFinal.Security.frmManualSync" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .Sync td:nth-child(1), .Sync th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .Sync td:nth-child(2), .Sync th:nth-child(2) {
            min-width: 385px;
            max-width: 385px;
        }

        .Sync td:nth-child(3), .Sync th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            try {                
                fncchoosenReSet();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncchoosenReSet()
        {
            try
            {
                $("select").chosen({ width: '100%' }); // width in px, %, em, etc
                //$("#ContentPlaceHolder1_ddlDownload_chzn").css("width", "100%");
                //$("#ContentPlaceHolder1_ddlUpload_chzn").css("width", "100%");
            }
            catch(err)
            {
                fncToastError(err.message);
            }
        }

        function fncSaveInformation(msg,mode)
        {
            try
            {
                //fncchoosenReSet();
                fncToastInformation(msg);
                
                if (mode == "upload") {
                    $('#liUpload').addClass('active');
                    $('#Upload').addClass('active');

                    $('#liDownload').removeClass('active');
                    $('#Download').removeClass('active');
                }
            }
            catch(err)
            {
                fncToastError(err.message);
            }
        }


    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'ManualSync');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "ManualSync";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
            </li>
            <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
            <li class="active-page">Manual Sync</li>  <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>                
        </ul>
    </div>
    <div class="col-md-12">
        <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Always">
            <ContentTemplate>
                <div class="panel panel-default">
                    <div id="Tabs" role="tabpanel">
                        <ul class="nav nav-tabs custnav custnav-im" role="tablist">
                            <li id="liDownload" class="active attribute_tabwidth" style="width: 50% !important">
                                <a href="#Download" aria-controls="Download" role="tab" data-toggle="tab">Download</a>
                            </li>
                            <li id="liUpload" class="attribute_tabwidth" style="width: 50% !important">
                                <a href="#Upload" aria-controls="Upload" role="tab" data-toggle="tab">Upload</a>
                            </li>
                        </ul>
                        <div class="tab-content" style="padding-top: 5px; overflow: auto; height: 450px;">
                            <div class="tab-pane active" role="tabpanel" id="Download">
                                <div class="col-md-12">
                                    <div class="manual_sync">
                                        <asp:Label ID="lblDMethod" runat="server"  Text="Method"></asp:Label>
                                    </div>
                                    <div class="manual_sync">
                                        <asp:DropDownList ID="ddlDownload" CssClass="full_width" style="width:300px;" runat="server"></asp:DropDownList>
                                    </div>
                                    <div class="float_left">
                                        <asp:LinkButton ID="lnkDownload" runat="server" CssClass="button-blue" OnClick="lnkDownload_Click" Text="Download"></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-12 over_flowhorizontal" style="margin-top: 10px;">
                                    <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header Sync">
                                        <tr>
                                            <th>S.No</th>
                                            <th>Sync Method</th>
                                            <th>Last Sync Date</th>
                                        </tr>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 300px; width: 655px; background-color: aliceblue;">
                                        <asp:GridView ID="gvDownload" runat="server" ShowHeader="false"
                                            AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                            CssClass="Sync">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="Records Not Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                                <asp:BoundField DataField="Description" HeaderText="Sync Method"></asp:BoundField>
                                                <asp:BoundField DataField="SyncDate" HeaderText="Last Sync Date"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" role="tabpanel" id="Upload">
                                <div class="col-md-12">
                                    <div class="manual_sync">
                                        <asp:Label ID="lblUMethod" runat="server" Text="Method"></asp:Label>
                                    </div>
                                    <div class="manual_sync">
                                        <asp:DropDownList ID="ddlUpload" CssClass="full_width" style="width:200px;" runat="server"></asp:DropDownList>
                                    </div>
                                    <div class="float_left">
                                        <asp:LinkButton ID="lnkUpload" runat="server" CssClass="button-blue" OnClick="lnkUpload_Click" Text="Upload"></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-12 over_flowhorizontal" style="margin-top: 10px;">
                                    <table rules="all" border="1" id="Table1" runat="server" class="fixed_header Sync">
                                        <tr>
                                            <th>S.No</th>
                                            <th>Sync Method</th>
                                            <th>Last Sync Date</th>
                                        </tr>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 300px; width: 655px; background-color: aliceblue;">

                                        <asp:GridView ID="gvUpload" runat="server" ShowHeader="false"
                                            AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                            CssClass="Sync">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="Records Not Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                                <asp:BoundField DataField="Description" HeaderText="Sync Method"></asp:BoundField>
                                                <asp:BoundField DataField="SyncDate" HeaderText="Last Sync Date"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                        <%--    </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
