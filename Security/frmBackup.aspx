﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmBackup.aspx.cs" Inherits="EnterpriserWebFinal.Security.frmBackup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .buttonbackup {
            border: none;
            padding: 20px 30px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 20px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 10px;
        }
        .buttonbackup:hover {
    opacity: 0.7;
}
        .backup {
            background-color: #00ffff;
            color: black;
            margin-right: 135px;
        }

        .reports {
            background-color: #33ff33;
            color: black;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            $(function () {
                SetDefaultDate($("#<%= txtfromdate.ClientID %>"), $("#<%= txttodate.ClientID %>"));
            });
        }
        function fncbackupsuccess() {
            try {
                fncToastInformation("File Backup Successfully");
            }
            catch (er) {
                fncToastError(er.message);
            }
        }
        function fncbackupfail() {
            try {
                fncToastInformation("File Backup Failed");
            }
            catch (er) {
                fncToastError(er.message);
            }
        }
        function emptyUser() {
            try {
                fncToastInformation("Please Select User");
            }
            catch (er) {
                fncToastError(er.message);
            }
        }
        function SetDefaultDate(FromDate, Todate) {
            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        }

    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'Backup');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "Backup";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Security</a><i class="fa fa-angle-right"></i></li>                
                <li class="active-page">Back Up</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full">
            <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                <ContentTemplate>
                    
                         <div class="container-group-small" style="margin:0 auto;">
                            <div class="container-control">
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="lblfromdate" runat="server" style="" Text="FromDate" ></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="lbltodate" runat="server" style="text-align:left" Text="ToDate" ></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txttodate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="lblusername" runat="server" style="text-align:left" Text="UserName" ></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="backupuserlist" runat="server" class="chzn-select" CssClass="chzn-select"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="button-contol">
                                <div class="control-button">

                                    <asp:Button ID="btbackup" runat="server" Text="Backup" OnClick="Backup_Click" class="buttonbackup backup" />
                                </div>
                                <div class="control-button">
                                    <asp:Button ID="btreport" runat="server" Text="Report" OnClick="Report_Click" class="buttonbackup reports" />
                                </div>
                            </div>
                        </div>
                    

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
