﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmGroupRoleMappingEdit.aspx.cs" Inherits="EnterpriserWebFinal.Security.frmGroupRoleMappingEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
    

    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

       


        .UserMasterEditHideRow
        {
            display: none;
        }
        .container-left
        {
            float: left;
            width: 32%;
        }
        .container-middle, .container-right
        {
            float: left;
            width: 32%;
            margin-left: 2%;
        }
        .container-group
        {
            width: 100%;
        }
        
        .button-red
        {
            text-shadow:none;
        }
        
        .groupTableTr
        {
            text-align:left;
            padding-left:8px;
        }
        .groupDiv
        {
            height:480px;
            overflow:auto;
        }
        
       
        
     
        li.search-choice
        {
            max-width:95%;
        }
        li.search-choice span
        {
            display:inline-block;
            max-width:100%;
            white-space: nowrap;
            overflow:hidden !important;
            text-overflow: ellipsis;
        }
        li.search-field input
        {
            min-height:60px;
        }
        .control-group-single-res .label-left1
{
	float: left;
	width: 20%;
}
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'GroupRoleMappingEdit');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "GroupRoleMappingEdit";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">
        function GRM_Delete() {
            if (confirm("Are You Sure do You want to delete?")) {
                return;
            } else {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
   
        function ValidateUserSave() {

            if ($('#<%=txtgroupcode.ClientID %>').val() == '') {
                alert('Please enter valid  Group Code');
                $('#<%=txtgroupcode.ClientID %>').focus();
                return false;
            }

           
                if ($('#<%=txtgroupname.ClientID %>').val() == '') {
                    alert('Please enter valid Group Name');
                    $('#<%=txtgroupname.ClientID %>').focus();
                    return false;
                }
            

            return true;
        }
        

        
        $(function () {
            //    


            //$("#ContentPlaceHolder1_lstLocation").chosen();
            //$("#ContentPlaceHolder1_lstLocation").trigger("liszt:updated");

            //            $('select').on('change', function (evt, params) {
            //                //console.log($(this));
            //                //console.log($(this).find("option:selected").text());
            //                //console.log($(this).find("option:selected").length);
            //            });



           InitializeTableGrouping('tblProgramAccess');



            //            InitializeTableGrouping('tblActivityAccess');

            //            InitializeProgramActivityAccess();

            //When clicked on td, checkboc must be checked
            //NOT WORKING
            //            $("#tblProgramAccess td").has("input[type=checkbox]").on("click", function () {
            //                //alert($(this).find("input[type=checkbox]").attr("id"));
            //                if ($(this).find("input[type=checkbox]").prop("checked")) {
            //                    $(this).find("input[type=checkbox]").prop("checked", false);
            //                }
            //                else {
            //                    $(this).find("input[type=checkbox]").prop("checked", true);
            //                }
            //                //$(this).find("input[type=checkbox]").trigger("click");
            //            });

            //            $("#tblProgramAccess input[type=checkbox]").parents("td").on("click", function () {
            //                alert($(this).html());
            //                //$(this).find("input[type=checkbox]").click();
            //            });

            //ToggleTableGroupingByGroupId('tblProgramAccess', 'POS');

            //alert($("#tblProgramAccess input[id*=chkAccess]").length);
            
            
            
              $("#tblProgramAccess input[id*=chkAccess]").on("change", checkboxProgramAccessChanged);

            $("#tblProgramAccess input[type=checkbox]").not("input[id*=chkAccess]").on("click", checkboxProgramOthersChanged);

            $("#tblRoles input[id*=chkAccess_left]").on("change", checkboxRoleAccessChanged);

          //  $("#tblRoles input[id*=chkAccess_left]").each(checkboxRoleAccessChanged);
            
            
            
            //            $("#tblActivityAccess input[id*=chkSelect]").on("change", function () {
            //                var tr = $(this).closest("tr");

            //                if (!$(this).prop("checked")) {

            //                    tr.find("input[id*=chkAuthenticationRequired]").prop("checked", false);

            //                    tr.find("input[id*=txtAuthenticationLevel]").val('');
            //                }
            //            });

            //            $("#tblActivityAccess input[id*=chkAuthenticationRequired]").on("click", function () {
            //                var tr = $(this).closest("tr");

            //                if (!tr.find("input[id*=chkSelect]").prop("checked")) {
            //                    return false;
            //                }

            //                if (tr.find("input[id*=chkAuthenticationRequired]").prop("checked")) {
            //                    tr.find("input[id*=txtAuthenticationLevel]").focus();
            //                }
            //                else {
            //                    tr.find("input[id*=txtAuthenticationLevel]").val('');
            //                }


            //            });

        });
       

//        function AuthenticationLevelTextChanged(element) {
//            var tr = $(element).closest("tr");

//            //console.log(tr.find("input[id*=chkSelect]"));

//            if (!tr.find("input[id*=chkSelect]").prop("checked") || !tr.find("input[id*=chkAuthenticationRequired]").prop("checked")) {
//                return false;
//            }
//            else {
//                return true;
//            }
//        }
//        function InitializeProgramActivityAccess() {
//            $("#tblProgramAccess span[id*=lblProgramCode]").each(function () {
//                if (!$(this).closest("tr").find("input[id*=chkAccess]").prop("checked")) {
//                    HideAndUnCheckActivityGroup($(this).text());
//                }
//            });
//        }
        function checkboxProgramOthersChanged() {
            var tr = $(this).closest("tr");

            if (!tr.find("input[id*=chkAccess]").prop("checked")) {
                return false;
            }
        }
        function checkboxRoleAccessChanged() {

            var tr = $(this).closest("tr");
            var roleid = tr.attr('data-roleid');

            if ($(this).prop("checked"))
             {
                var tableGroupTr = $("#tblProgramAccess > tbody tr[data-tablegroupid=" + roleid + "]");
                tableGroupTr.each(function () {

                    //if (!$('#tblRoles').find("tr[data-roleid=" + currentGroupId + "]").find("input[id*=chkAccess_left]").attr("checked")) {
                   // alert('');
                    $(this).find("input[type=checkbox]:enabled").prop("checked", true);

                    //}


                });
                ShowTableGroupingByGroupId('tblProgramAccess', roleid);
//               
                
            }
            else
             {

                HideTableGroupingByGroupId('tblProgramAccess', roleid);
                var tableGroupTr = $("#tblProgramAccess > tbody tr[data-tablegroupid=" + roleid + "]");
                tableGroupTr.each(function () {
                    $(this).find("input[type=checkbox]:enabled").prop("checked", false);
                });
            }
        }
        function checkboxProgramAccessChanged() {
            var tr = $(this).closest("tr");
            
            var programCode = tr.find("span[id*=lblProgramCode]").text();

            if ($(this).prop('checked')) {
                tr.find("input[type=checkbox]:enabled").not("input[id*=chkAccess]").prop("checked", true);

                //ShowTableGroupingByGroupId('tblActivityAccess', programCode);
                //ShowActivityGroup(programCode)
            }
            else {
                tr.find("input[type=checkbox]").not("input[id*=chkAccess]").prop("checked", false);

//                HideAndUnCheckActivityGroup(programCode);
            }
        }

        function ShowActivityGroup(groupId) {
            ShowTableGroupingByGroupId('tblActivityAccess', groupId);
        }
        function HideAndUnCheckActivityGroup(groupId) {

            var tableGroupTr = $("#tblActivityAccess > tbody tr[data-tablegroupid=" + groupId + "]");

            if (tableGroupTr.length > 0) {

                HideTableGroupingByGroupId('tblActivityAccess', groupId);

                tableGroupTr.each(function () {
                    $(this).find("input[type=checkbox]").prop("checked", false);
                    $(this).find("input[type=text]").val('');

                    //$(this).find("input[id*=_chkSelect_]").prop("checked", false);
                });
            }

        }
        function InitializeTableGrouping(tableId) {
            //alert($("#" + tableId + " th").length);

            var columnLength = $("#" + tableId + " th").length;
           
            var dataAttrGroupId = 'data-tablegroupid';
            var dataAttrGroupName = 'data-tablegroupname';

            $("#" + tableId + " > tbody").children().each(function () {
                //alert(tableId);

                //var groupSpanId = 'lblRoleName'
                //var currentText = currentRow.find("span[id*=" + groupSpanId + "]").text();
                //var prevText = prevRow.find("span[id*=" + groupSpanId + "]").text();

                var currentRow = $(this);
                var prevRow = $(this).prev();

                var currentGroupId = currentRow.attr(dataAttrGroupId);

                var currentGroupName = currentRow.attr(dataAttrGroupName);
                var prevGroupId = prevRow.attr(dataAttrGroupId);
                //                alert(currentGroupId);
                //                alert(prevGroupId);
                if (currentGroupId != prevGroupId) {

                    var groupTrId = "trGroup_" + tableId + "_" + currentGroupId.replace(/\s/g, '');


                    currentRow.before("<tr id='" + groupTrId + "'><td colspan='" + columnLength + "' style='text-align:left;padding-left:8px;'><span style='cursor:pointer;' class='ui-icon ui-icon-circle-minus' ></span>" + currentGroupName + "</td></tr>");
                    $("#" + groupTrId + " span").on("click", null, { tableId: tableId, groupId: currentGroupId }, ToggleGroupDisplay);


                    //                    alert($('#tblRoles').find("tr[data-roleid=" + currentGroupId + "]").length);
                    //                    alert(currentGroupId);
                    if (!$('#tblRoles').find("tr[data-roleid=" + currentGroupId + "]").find("input[id*=chkAccess_left]").prop("checked")) {
                        HideTableGroupingByGroupId(tableId, currentGroupId);
                        
                    }

                    //$("#" + groupTrId + " span").click();

                    //console.log(prevText + '>>' + currentText);
                }
            });
        }
        function HideTableGroupingByGroupId(tableId, groupId) {
            CollapseTableGroupingByGroupId(tableId, groupId);

            var groupTrId = "trGroup_" + tableId + "_" + groupId.replace(/\s/g, '');
            $("#" + groupTrId).hide();

        }
        function ShowTableGroupingByGroupId(tableId, groupId) {
            ExpandTableGroupingByGroupId(tableId, groupId);

            var groupTrId = "trGroup_" + tableId + "_" + groupId.replace(/\s/g, '');
            $("#" + groupTrId).show();
        }
        function CollapseTableGroupingByGroupId(tableId, groupId) {

            var groupTrId = "trGroup_" + tableId + "_" + groupId.replace(/\s/g, '');
            var groupHeadElement = $("#" + groupTrId + " span");
            var minusClass = 'ui-icon-circle-minus';
            var plusClass = 'ui-icon-circle-plus';
            var fadeOutTime = 100;
            var fadeInTime = 300;

            var tr = $("#" + tableId + " > tbody tr[data-tablegroupid='" + groupId + "']");

            //tr.css("display", "none");
            tr.fadeOut(fadeOutTime);
            groupHeadElement.removeClass(minusClass)
            groupHeadElement.addClass(plusClass)


        }
        function ExpandTableGroupingByGroupId(tableId, groupId) {

            var groupTrId = "trGroup_" + tableId + "_" + groupId.replace(/\s/g, '');
            var groupHeadElement = $("#" + groupTrId + " span");
            var minusClass = 'ui-icon-circle-minus';
            var plusClass = 'ui-icon-circle-plus';
            var fadeOutTime = 100;
            var fadeInTime = 300;

            var tr = $("#" + tableId + " > tbody tr[data-tablegroupid='" + groupId + "']");

            //tr.css("display", "table-row");
            tr.fadeIn(fadeInTime);
            groupHeadElement.removeClass(plusClass)
            groupHeadElement.addClass(minusClass)

        }

        function ToggleTableGroupingByGroupId(tableId, groupId) {
            //alert(tableId); alert(groupId);
            var groupTrId = "trGroup_" + tableId + "_" + groupId.replace(/\s/g, '');
            var groupHeadElement = $("#" + groupTrId + " span");
            var minusClass = 'ui-icon-circle-minus';
            var plusClass = 'ui-icon-circle-plus';
            var fadeOutTime = 100;
            var fadeInTime = 300;

            //console.log(event.data.tableId);

            var tr = $("#" + tableId + " > tbody tr[data-tablegroupid='" + groupId + "']");

            if (groupHeadElement.hasClass(minusClass)) {
                //tr.css("display", "none");
                tr.fadeOut(fadeOutTime);
                groupHeadElement.removeClass(minusClass)
                groupHeadElement.addClass(plusClass)
            }
            else {
                //tr.css("display", "table-row");
                tr.fadeIn(fadeInTime);
                groupHeadElement.removeClass(plusClass)
                groupHeadElement.addClass(minusClass)
            }
        }
        function ToggleGroupDisplay(event) {

            ToggleTableGroupingByGroupId(event.data.tableId, event.data.groupId);


            //            var minusClass = 'ui-icon-circle-minus';
            //            var plusClass = 'ui-icon-circle-plus';
            //            var fadeOutTime = 100;
            //            var fadeInTime = 500;

            //            console.log(event.data.tableId);

            //            var tr = $("#" + event.data.tableId + " > tbody tr[data-tablegroupid='" + event.data.groupId + "']");

            //            //alert($(this).hasClass('ui-icon-circle-minus'));

            //            if ($(this).hasClass(minusClass)) {
            //                //tr.css("display", "none");
            //                tr.fadeOut(fadeOutTime);
            //                $(this).removeClass(minusClass)
            //                $(this).addClass(plusClass)
            //            }
            //            else {
            //                //tr.css("display", "table-row");
            //                tr.fadeIn(fadeInTime);
            //                $(this).removeClass(plusClass)
            //                $(this).addClass(minusClass)
            //            }
        }
        function DisableEnter(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
            return true;
        }

       
        
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <br />
    <div class="breadcrumbs">
        <ul>         <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul></div>
 <asp:HiddenField ID="hdnNewUser" runat="server" />
 <div class="col-md-12">
  <div class="container-control">
  <div class="gridDetails">
     <div class="controlgroup">
                        <div class="container-group">
                            <div class="container-control">
                                <div class="container-left">
                                    <div class="control-group-single-res">
                                        <div class="label-left1">
                                            <asp:Label ID="Label2" runat="server" Text="Group Code:"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtgroupcode" runat="server" ReadOnly="true" CssClass="form-control-res" MaxLength="30"></asp:TextBox>
                                        </div>
                                    </div>
                                 </div>
                                <div class="container-middle">
                                    <div class="control-group-single-res">
                                        <div class="label-left1">
                                            <asp:Label ID="Label7" runat="server" Text="Group Name:"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtgroupname" runat="server" CssClass="form-control-res"  MaxLength="30"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>


     </div>  
  </div>
</div>
 <div class="col-md-3">

  <div class="container-group">
                    <div class="container-control">
                        <div class="groupDiv">
                            <div class="gridDetails">
                             <p><strong>Role</strong></p>
 <asp:Repeater runat="server" ID="rptRole" >
                                    <HeaderTemplate>
                                        <table id="tblRoles" class="pshro_GridDgn" cellspacing="0" rules="all" border="1">
                                            <thead>
                                                <tr class="pshro_GridDgnHeaderCellCenter">
                                                    <th>
                                                        Role Name
                                                    </th>
                                                    <th>
                                                        Access
                                                    </th>
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="trRole" class="pshro_GridDgnStyle" data-roleid='<%# Eval("Rolecode") %>'>
                                            
                                            <td align="left" style="padding-left:25px;">
                                                <%--class="UserMasterEditHideRow"--%>
                                                <asp:Label ID="lblRoleName" runat="server"  Text='<%# Eval("Rolename") %>' />
                                            </td>
                                             <td>
                                                <asp:CheckBox runat="server" ID="chkAccess_left" Checked='<%# Eval("Access") %>' />
                                            </td>
                                            
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                </div>
                        </div>
                    </div>
                </div>
 </div>
 <div class="col-md-9">
               
                <div class="container-group">
                    <div class="container-control">
                        <div class="">
                        <p><strong>Program Wise Access</strong></p>
                        <div class="button-contol">
                             <div class="control-button">
                             <asp:LinkButton ID="lnkSave" runat="server" OnClick="lnkSave_Click" OnClientClick="return ValidateUserSave()" class="button-red">Save</asp:LinkButton>
                             </div>
                            
                             <div class="control-button">
                             <asp:LinkButton ID="lnkDelete" runat="server" Onclick="lnkDelete_Click" OnClientClick="return GRM_Delete()" class="button-red" >Delete</asp:LinkButton>
                             </div>

                             <div class="control-button">
                             <asp:LinkButton ID="lnkview" runat="server" class="button-red" 
                                     onclick="lnkView_Click">View</asp:LinkButton>
                             </div>
                             </div>
                            <div class="gridDetails groupDiv">
                             
                             

                                <asp:Repeater runat="server" ID="rptProgramAccess" >
                                    <HeaderTemplate>
                                        <table id="tblProgramAccess" class="pshro_GridDgn" cellspacing="0" rules="all" border="1">
                                            <thead>
                                                <tr class="pshro_GridDgnHeaderCellCenter">
                                                    <th style = "display:none">
                                                        Role Code
                                                    </th>
                                                    <th style = "display:none">
                                                        Program ID
                                                    </th>
                                                    <th>
                                                        Program Name
                                                    </th>
                                                    <th>
                                                        Access
                                                    </th>
                                                    <th>
                                                        <span>New</span>
                                                    </th>
                                                    <th>
                                                        <span>Edit</span>
                                                    </th>
                                                    <th>
                                                        <span>Delete</span>
                                                    </th>
                                                    <th>
                                                        <span>View</span>
                                                    </th>
                                                    <th>
                                                        <span>Print</span>
                                                    </th>
                                                    <th>
                                                        <span>Preview</span>
                                                    </th>
                                                    <th>
                                                        <span>Cancel</span>
                                                    </th>
                                                    <th>
                                                        <span>Authorise</span>
                                                    </th>
                                                    <th>
                                                        <span>Amend</span>
                                                    </th>
                                                    <th>
                                                        <span>Copy</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="trProgram" class="pshro_GridDgnStyle" data-tablegroupid='<%# Eval("RoleCode") %>'
                                            data-tablegroupname='<%# Eval("RoleName") %>'>
                                            <td style = "display:none">
                                                <asp:Label ID="lblRoleCode" runat="server" Text='<%# Eval("RoleCode") %>' />
                                            </td>
                                            <td style = "display:none">
                                                <asp:Label ID="lblProgramID" runat="server" Text='<%# Eval("PM_Program_ID") %>' />
                                            </td>
                                            <td align="left" style="padding-left:30px;">
                                                <asp:Label ID="lblProgramName" runat="server" Text='<%# Eval("PProgramName") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkAccess" Checked='<%# Eval("Access") %>'  />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkNew" Checked='<%# Eval("New") %>' Enabled='<%# !IsControlDisabled(Eval("ProgramAccesscontrol").ToString(),"N") %>'  />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkEdit" Checked='<%# Eval("Edit") %>' Enabled='<%# !IsControlDisabled(Eval("ProgramAccesscontrol").ToString(),"E") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkDelete" Checked='<%# Eval("Delete") %>' Enabled='<%# !IsControlDisabled(Eval("ProgramAccesscontrol").ToString(),"D") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkView" Checked='<%# Eval("View") %>' Enabled='<%# !IsControlDisabled(Eval("ProgramAccesscontrol").ToString(),"V") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkPrint" Checked='<%# Eval("Print") %>' Enabled='<%# !IsControlDisabled(Eval("ProgramAccesscontrol").ToString(),"P") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkPreview" Checked='<%# Eval("Preview") %>' Enabled='<%# !IsControlDisabled(Eval("ProgramAccesscontrol").ToString(),"I") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkCancel" Checked='<%# Eval("Cancel") %>' Enabled='<%# !IsControlDisabled(Eval("ProgramAccesscontrol").ToString(),"C") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkAuthorise" Checked='<%# Eval("Authorise") %>' Enabled='<%# !IsControlDisabled(Eval("ProgramAccesscontrol").ToString(),"A") %>'  />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkAmend"  Checked='<%# Eval("Amend") %>' Enabled='<%# !IsControlDisabled(Eval("ProgramAccesscontrol").ToString(),"M") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkCopy" Checked='<%# Eval("Copy") %>' Enabled='<%# !IsControlDisabled(Eval("ProgramAccesscontrol").ToString(),"O") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                
</asp:Content>
