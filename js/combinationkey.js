﻿$.alt = function (key, callback, args) {
    var isAlt = false;
    $(document).keydown(function (e) {
        if (!args) args = [];

        if (e.altKey) {
            isAlt = true;
        }
        else {
            isAlt = false;
        }
        if (e.keyCode == key.charCodeAt(0) && isAlt) {
            callback.apply(this, args);
            return false;
        }
    }).keyup(function (e) {
        if (e.altKey) isAlt = false;
    });
};
$.ctrl = function (key, callback, args) {

    var isCtrl = false;
    $(document).keydown(function (e) {
        if (!args) args = [];

        if (e.ctrlKey) {
            isCtrl = true;
        }
        else {
            isCtrl = false;
        }
        if (e.keyCode == key.charCodeAt(0) && isCtrl) {
            callback.apply(this, args);
            return false;
        }
    }).keyup(function (e) {
        if (e.ctrlKey) isCtrl = false;
    });
};
$.shift = function (key, callback, args) {
    var isShift = false;
    $(document).keydown(function (e) {
        if (!args) args = [];

        if (e.shiftKey) {
            isShift = true;
        }
        else {
            isShift = false;
        }
        if (e.keyCode == key.charCodeAt(0) && isShift) {
            callback.apply(this, args);
            return false;
        }
    }).keyup(function (e) {
        if (e.shiftKey) isShift = false;
    });
};