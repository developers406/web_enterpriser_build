﻿var cdnurData = new Array();
var b2cs = new Array();
var b2cl = new Array();
var b2b = new Array();
var cdnr = new Array();

function parseJson() {
    var x = $("#myJsonDiv").text();
    var jsonObj = $.parseJSON(x);
    //alert(jsonObj);
    return jsonObj;
}

function loadData() {
    var source = document.getElementById("gstr1-temp").innerHTML;
    var template = Handlebars.compile(source);

    var htmlResp = template(parseJson());

    $("#dataDiv").html(htmlResp);
    $(".itmTable").dataTable("{columnDefs:[{orderable: false, targets: '_all' }]}");

}

function setInvoiceValuesForDeletion(inum, idt) {

    $("#hdnInvoiceNo").val(inum);
    $("#hdnInvoiceDate").val(idt);
    //$("#hdnCTIN").val(ctin);
}
function setctinForDeletion(ctin) {

    ////$("#hdnInvoiceNo").val(inum);
    ////$("#hdnInvoiceDate").val(idt);
    $("#hdnCTIN").val(ctin);
}

//function setb2csDeletion(pos,sply_ty,typ) {

//    $("#hdnpos").val(pos);
//    $("#hdnsply_ty").val(sply_ty);
//    $("#hdntyp").val(typ);
//}

function setb2csDeletion(obj,pos, sply_ty, typ) {

    var data = typ + "," + pos + "," + sply_ty ;

    var ckBoxChecked = $(obj).is(':checked');
    //Check if checkbox is checked
    if (ckBoxChecked) {
        //if yes, check if element is already present inside array
        if (!b2cs.includes(data)) {
            b2cs.push(data);
        }

        //if present, dont add, if not present add to array

    }
    else {
        //if yes, check if element is already present inside array
        if (b2cs.includes(data)) {
            b2cs.pop(data);
        }
    }
}

function setposForDeletion(pos) {

    $("#hdnpos").val(pos);
}
//function setcdnrForDeletion(inum, idt,ntnum,ntdt) {

//    $("#hdnInvoiceNo").val(inum);
//    $("#hdnInvoiceDate").val(idt);
//    $("#hdnnt_num").val(ntnum);
//    $("#hdnnt_dt").val(ntdt);
//}
function setcdnrForDeletion(obj,inum, idt,ntnum,ntdt) {
    var data = document.getElementById('hdnCTIN').value + "," + ntnum + "," + ntdt + "," + inum + "," + idt;

    var ckBoxChecked = $(obj).is(':checked');
    //Check if checkbox is checked
    if (ckBoxChecked) {
        //if yes, check if element is already present inside array
        if (!cdnr.includes(data)) {
            cdnr.push(data);
        }

        //if present, dont add, if not present add to array

    }
    else {
        //if yes, check if element is already present inside array
        if (cdnr.includes(data)) {
            cdnr.pop(data);
        }
    }

}

function setcdnurForDeletion(obj, typ, ntnum, ntdt,inum, idt) {

    var data = typ + "," + ntnum + "," + ntdt + "," + inum + "," + idt;

    var ckBoxChecked = $(obj).is(':checked');
    //Check if checkbox is checked
    if (ckBoxChecked) {
        //if yes, check if element is already present inside array
        if (!cdnurData.includes(data)) {
             cdnurData.push(data);
        }

        //if present, dont add, if not present add to array

    }
    else {
        //if yes, check if element is already present inside array
        if (cdnurData.includes(data)) {
            cdnurData.pop(data);
        }
    }

}

function deleteInvoiceCdNur() {

    var returnVal = false;

  

    var bulkDataForDeletion = '';

    bulkDataForDeletion = cdnurData.join(';');

    if (bulkDataForDeletion != '') {
        alert(bulkDataForDeletion);
        $("#hdnCdnurForDeletion").val(bulkDataForDeletion);
        returnVal = true;
    }
    return returnVal;
}

function deleteInvoiceb2cs() {

    var returnVal = false;

  

    var bulkDataForDeletion = '';

    bulkDataForDeletion = b2cs.join(';');

    if (bulkDataForDeletion != '') {
        alert(bulkDataForDeletion);
        $("#hdnb2csForDeletion").val(bulkDataForDeletion);
        returnVal = true;
    }
    return returnVal;
}

function setb2clForDeletion(obj, inum, idt) {

    var data = document.getElementById('hdnpos').value + "," + inum + "," + idt;

    var ckBoxChecked = $(obj).is(':checked');
    //Check if checkbox is checked
    if (ckBoxChecked) {
        //if yes, check if element is already present inside array
        if (!b2cl.includes(data)) {
            b2cl.push(data);
        }

        //if present, dont add, if not present add to array

    }
    else {
        //if yes, check if element is already present inside array
        if (b2cl.includes(data)) {
            b2cl.pop(data);
        }
    }

}

function deleteInvoiceb2cl(){

    var returnVal = false;

  

    var bulkDataForDeletion = '';

    bulkDataForDeletion = b2cl.join(';');

    if (bulkDataForDeletion != '') {
        alert(bulkDataForDeletion);
        $("#hdnb2clForDeletion").val(bulkDataForDeletion);
        returnVal = true;
    }
    return returnVal;
}

function setb2bForDeletion(obj, inum, idt) {

    var data = document.getElementById('hdnCTIN').value + "," + inum + "," + idt;

    var ckBoxChecked = $(obj).is(':checked');
    //Check if checkbox is checked
    if (ckBoxChecked) {
        //if yes, check if element is already present inside array
        if (!b2b.includes(data)) {
            b2b.push(data);
        }

        //if present, dont add, if not present add to array

    }
    else {
        //if yes, check if element is already present inside array
        if (b2b.includes(data)) {
            b2b.pop(data);
        }
    }

}

function deleteInvoiceb2b() {

    var returnVal = false;



    var bulkDataForDeletion = '';

    bulkDataForDeletion = b2b.join(';');

    if (bulkDataForDeletion != '') {
        alert(bulkDataForDeletion);
        $("#hdnb2bForDeletion").val(bulkDataForDeletion);
        returnVal = true;
    }
    return returnVal;
}

function deleteInvoicecdnr() {

    var returnVal = false;



    var bulkDataForDeletion = '';

    bulkDataForDeletion = cdnr.join(';');

    if (bulkDataForDeletion != '') {
        alert(bulkDataForDeletion);
        $("#hdncdnrForDeletion").val(bulkDataForDeletion);
        returnVal = true;
    }
    return returnVal;
}

var button = document.getElementsByClassName("btn btn-link");
var chkbox = document.getElementsByName("chk");

function onChangeListener() {
    button.style.backgroundColor = "gray";
    for (var i = 0; i < chkbox.length; i++) {
        if (chkbox[i].checked) {
            button.style.backgroundColor = "green";
        }
    }
}

for (var i = 0; i < chkbox.length; i++) {
    var checkbox = chkbox;
    checkbox[i].addEventListener("change", onChangeListener);
}