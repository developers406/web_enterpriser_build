﻿
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    //var emailPat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    return pattern.test(emailAddress);
}

// ==========================> For Enter to Focus 
function controlEnter(obj, event, ischosen) {
    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if (keyCode == 13) {
        if (ischosen) {
            setTimeout(function () {
                $('#' + obj).trigger("liszt:open");
            }, 50);
        }
        else {
            $('#' + obj).focus().select();
        }

        return false;
    }
    else {
        return true;
    }
}

// ==========================> Set Focus to Object when Drop Down List Change
function fncDropDownChange(obj, ischosen) {
    try {

        if (ischosen) {
            setTimeout(function () {
                $('#' + obj).trigger("liszt:open");
            }, 50);
        }
        else {
            setTimeout(function () {
                $('#' + obj).focus();
            }, 50);

        }

    }
    catch (err) {
        alert(err.message);
    }

}

//alphabet key enter false without allow Decimal
function isNumberKey(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode >= 96 && charCode <= 105)) {
        event.preventDefault();
        return false;
    }
    return true;
}

function fncCheckJavaScript() {
    $("#hidjsCheck").val("Y");
}

////Assign Focus to Given Object
//function fncSetfocus(obj) {
//    obj.focus();
//}

//alphabet key enter false with allow Decimal
function isNumberKeywithDecimal(event) {
    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
}

//================================== Clear All Form Fields ==================================//
function clearForm() {
    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
    $(':checkbox, :radio').prop('checked', false);

    $("select").val(0);
    $("select").trigger("liszt:updated");
}
/////alphabet key enter false with allow Minize Symbol
function isNumberKeywithMin(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 45 && charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

