﻿$(document).ready(function () {
    $("input").first().focus();

    $('input').bind("keydown", function (e) {
        var n = $("input").length;
        if (e.which == 13) { //Enter key
            e.preventDefault(); //to skip default behavior of the enter key
            var nextIndex = $('input').index(this) + 1;
            if (nextIndex < n)
                $('input')[nextIndex].focus();
            else {
                $('input')[nextIndex - 1].blur();
               // $('#btnSubmit').click();
            }
        }
    });

    //$('#btnSubmit').click(function () {
       // alert('Form Submitted');
  //  });
});