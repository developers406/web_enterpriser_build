﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuUserControl.ascx.cs"
    Inherits="EnterpriserWebFinal.UserControls.MenuUserControl" %>

<ul id="menu" class='main-nav' runat="server" >
    <asp:PlaceHolder runat="server" ID="menuPlaceHolder"></asp:PlaceHolder>
</ul>
