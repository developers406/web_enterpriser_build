﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaginationUserControl.ascx.cs" Inherits="EnterpriserWebFinal.UserControls.PaginationUserControl" %>

<nav aria-label="Page navigation">
    <ul class="pagination">
        <li class="page-item" runat="server" id="liFirst">
            <asp:LinkButton CssClass="page-link" ID="lnkFirst" Text='<%$ Resources:LabelCaption,lbl_First %>' runat="server" CommandName="First" OnCommand="lnkPageNavigation_Command" aria-label="First">
            </asp:LinkButton>
        </li>
        <li class="page-item" runat="server" id="liPrevious">
            <asp:LinkButton CssClass="page-link" ID="lnkPrevious" runat="server" CommandName="Previous" OnCommand="lnkPageNavigation_Command" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>            
            </asp:LinkButton>
        </li>
        <asp:Repeater ID="rptrPagination" runat="server" OnItemCommand="rptrPagination_ItemCommand">
            <ItemTemplate>
                <li runat="server" id="liPageItem" class='<%# Convert.ToBoolean(Eval("Enabled")) ? "page-item" : "page-item active" %>'>
                    <asp:LinkButton ID="lnkrptpagination" Text='<%# Eval("Text") %>' CommandName="PageNumber" CommandArgument='<%# Eval("Value") %>' runat="server" 
                        OnClientClick='<%# !Convert.ToBoolean(Eval("Enabled")) ? "return false;" : "" %>'> ></asp:LinkButton>
                </li>
            </ItemTemplate>
        </asp:Repeater>
        <li class="page-item" runat="server" id="liNext">
            <asp:LinkButton CssClass="page-link" ID="lnkNext" runat="server" CommandName="Next" OnCommand="lnkPageNavigation_Command" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>            
            </asp:LinkButton>
        </li>
        <li class="page-item" runat="server" id="liLast">
            <asp:LinkButton CssClass="page-link" ID="lnkLast" Text='<%$ Resources:LabelCaption,lbl_Last %>' runat="server" CommandName="Last" OnCommand="lnkPageNavigation_Command" aria-label="Last">
            </asp:LinkButton>
        </li>
        <li class="page-item">
            <asp:Label ID="lblPageStatus" runat="server"></asp:Label>            
        </li>
    </ul>
</nav>
<br />
<%--<nav aria-label="Page navigation">
    <ul class="pagination">
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1">First</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#" tabindex="-1">Last</a>
        </li>
        <li class="page-item">
            <span>Page 1 of 10</span>
        </li>
    </ul>
</nav>--%>
