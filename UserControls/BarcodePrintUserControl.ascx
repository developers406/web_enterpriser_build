﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BarcodePrintUserControl.ascx.cs"
    Inherits="EnterpriserWebFinal.UserControls.BarcodePrintUserControl" %>

<div class="control-group-single-res">
    <div class="common_barcode_lbl">
        <asp:Label ID="lblPKDDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_PKDDate %>'></asp:Label>
    </div>
    <div class="common_barcode_txt">
        <asp:TextBox ID="txtPkdData" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
    <div class="common_barcode_lbl">
        <asp:Label ID="lblExpireDate" runat="server" Text='<%$ Resources:LabelCaption,lblExpiredDate %>'></asp:Label>
    </div>
    <div class="common_barcode_txt ">
        <asp:TextBox ID="txtExpiredate" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res">
    <div class="common_barcode_lbl">
        <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblDataformat %>'></asp:Label>
    </div>
    <div class="common_barcode_txt">
        <asp:DropDownList ID="ddlDateformat" runat="server" Style="width: 110px">
        </asp:DropDownList>
    </div>
    <div class="common_barcode_lbl">
        <asp:Label ID="lblTemplate" runat="server" Text='<%$ Resources:LabelCaption,lbl_Template %>'></asp:Label>
    </div>
    <div class="common_barcode_txt">
        <asp:DropDownList ID="ddlTemplate" runat="server" Style="width: 110px" onchange="fncTemplateChange()">
        </asp:DropDownList>
    </div>
</div>
<div class="control-group-single-res">
    <div class="common_barcode_cb">
        <asp:CheckBox ID="cbhidemrp" runat="server" />
        <%=Resources.LabelCaption.lblhidemrp%>
    </div>
    <div class="common_barcode_cb">
        <asp:CheckBox ID="cbHideprice" runat="server" />
        <%=Resources.LabelCaption.lblHideprice%>
    </div>
    <div class="common_barcode_cb">
        <asp:CheckBox ID="cbhidePkddate" runat="server" />
        Hide Date
        <%--<%=Resources.LabelCaption.lblHidePkddate%>--%>
    </div>
    <div class="common_barcode_cb display_none">
        <asp:CheckBox ID="cbexpiredate" runat="server" />
        <%=Resources.LabelCaption.lblHideExpDate%>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divSize">
    <div class="common_barcode_cb">
        <asp:CheckBox ID="cbhidecompany" runat="server" />
        <%=Resources.LabelCaption.lblHideCompany%>
    </div>
    <div class="common_barcode_size">
        <asp:Label ID="lblSize" runat="server" Text='<%$ Resources:LabelCaption,lbl_Size %>'></asp:Label>
    </div>
    <div class="common_barcode_cb">
        <asp:TextBox ID="txtSize" runat="server" CssClass="form-control" Style="width: 85px"
            onMouseDown="return false"></asp:TextBox>
    </div>
     <div class="common_barcode_lbl">
        <asp:Label ID="Label1" runat="server" Text="Printer Name"></asp:Label>
    </div>
    <div class="common_barcode_txt">
        <asp:DropDownList ID="ddlPrinterName" runat="server" Style="width: 110px"  >
        </asp:DropDownList> 
    </div>
    <div class="common_barcode_btn" style="margin-top:10px;">
        <asp:LinkButton ID="lnkbtnPrint" runat="server" CssClass="button-red" Text='<%$ Resources:LabelCaption,btn_Print %>'
            OnClick="lnkbtnPrint_Click"></asp:LinkButton>
    </div>
</div>
<script type="text/javascript">
    $("#ContentPlaceHolder1_barcodePrintUserControl_ddlDateformat").show().removeClass('chzn-done');
    $("#ContentPlaceHolder1_barcodePrintUserControl_ddlDateformat").next().remove();
    $("#ContentPlaceHolder1_barcodePrintUserControl_ddlTemplate").show().removeClass('chzn-done');
    $("#ContentPlaceHolder1_barcodePrintUserControl_ddlTemplate").next().remove();
    $("#ContentPlaceHolder1_barcodePrintUserControl_ddlPrinterName").show().removeClass('chzn-done');
    $("#ContentPlaceHolder1_barcodePrintUserControl_ddlPrinterName").next().remove();
</script>

