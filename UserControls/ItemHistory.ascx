﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemHistory.ascx.cs"
    Inherits="EnterpriserWebFinal.UserControls.ItemHistory" %>
<div class="container-inv-history" style="float: left; display: table">
    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
        <ContentTemplate>
            <div class="control-group-split">
                <div class="control-group-left" style="width: 100%">
                    <div class="label-left" style="width: 20%">
                        <asp:LinkButton ID="btnBack" runat="server" class="button-blue" OnClick="lnkBack_Click">Back</asp:LinkButton>
                    </div>
                    <div class="label-left cust-pages"  style="width: 25%">
                        <div class="control-button">
                            <asp:Label ID="LinkButton1" runat="server" Font-Bold="True">InventoryCode : </asp:Label>
                        </div>
                    </div>
                    <div class="label-right cust-pages" style="width: 40%">
                        <div class="control-button">
                            <asp:Label ID="txtVItemCode" runat="server" Font-Bold="True"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gridDetails grid-overflow" id="ItemHistorydiv" runat="server" style="height: 100px">
                <asp:GridView ID="gvItemHistory" runat="server" Width="100%" AutoGenerateColumns="False"
                    ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                    CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector"
                    OnSelectedIndexChanged="gvPO_SelectedIndexChanged" DataKeyNames="MNName" AutoGenerateSelectButton="True">
                    <EmptyDataTemplate>
                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                    <RowStyle CssClass="pshro_GridDgnStyle" />
                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                    <PagerStyle CssClass="pshro_text" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <HeaderTemplate>
                                <asp:Label ID="Label18" Visible="false" runat="server" Text="Select"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSingle" Visible="false" runat="server" Width="40px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="MNName" HeaderText="Month"></asp:BoundField>
                        <asp:BoundField DataField="WK1" HeaderText="1st Week" DataFormatString="{0:n}"></asp:BoundField>
                        <asp:BoundField DataField="WK2" HeaderText="2nd Week" DataFormatString="{0:n}"></asp:BoundField>
                        <asp:BoundField DataField="WK3" HeaderText="3rd Week" DataFormatString="{0:n}"></asp:BoundField>
                        <asp:BoundField DataField="WK4" HeaderText="4th Week" DataFormatString="{0:n}"></asp:BoundField>
                        <asp:BoundField DataField="WK5" HeaderText="5th Week" DataFormatString="{0:n}" />
                        <asp:BoundField DataField="WK6" HeaderText="6th Week" DataFormatString="{0:n}" />
                        <asp:BoundField DataField="Total" HeaderText="Total" DataFormatString="{0:n}" />
                    </Columns>
                </asp:GridView>
            </div>
            <div class="gridDetails grid-overflow" id="ItemHistoryLoc" runat="server" style="height: 100px">
                <asp:GridView ID="gvItemHistoryLoc" runat="server" Width="100%" AutoGenerateColumns="False"
                    ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                    CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                    <EmptyDataTemplate>
                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                    <RowStyle CssClass="pshro_GridDgnStyle" />
                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                    <PagerStyle CssClass="pshro_text" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <HeaderTemplate>
                                <asp:Label ID="Label18" Visible="false" runat="server" Text="Select"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSingle" Visible="false" runat="server" Width="40px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Location" HeaderText="Location"></asp:BoundField>
                        <asp:BoundField DataField="MNName" HeaderText="Month"></asp:BoundField>
                        <asp:BoundField DataField="WK1" HeaderText="1st Week" DataFormatString="{0:n}"></asp:BoundField>
                        <asp:BoundField DataField="WK2" HeaderText="2nd Week" DataFormatString="{0:n}"></asp:BoundField>
                        <asp:BoundField DataField="WK3" HeaderText="3rd Week" DataFormatString="{0:n}"></asp:BoundField>
                        <asp:BoundField DataField="WK4" HeaderText="4th Week" DataFormatString="{0:n}"></asp:BoundField>
                        <asp:BoundField DataField="WK5" HeaderText="5th Week" DataFormatString="{0:n}" />
                        <asp:BoundField DataField="WK6" HeaderText="6th Week" DataFormatString="{0:n}" />
                        <asp:BoundField DataField="Total" HeaderText="Total" DataFormatString="{0:n}" />
                    </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</div>
