﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageBarcodePrintUserControl.ascx.cs"
    Inherits="EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl" %>

<div class="control-group-single-res">
    <div class="common_barcode_lbl">
        <asp:Label ID="lblPKDDates" runat="server" Text='<%$ Resources:LabelCaption,lbl_PKDDate %>'></asp:Label>
    </div>
    <div class="common_barcode_txt">
        <asp:TextBox ID="txtPkdDatas" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
    <div class="common_barcode_lbl">
        <asp:Label ID="lblExpireDates" runat="server" Text='<%$ Resources:LabelCaption,lblExpiredDate %>'></asp:Label>
    </div>
    <div class="common_barcode_txt ">
        <asp:TextBox ID="txtExpiredates" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res">
    <div class="common_barcode_lbl">
        <asp:Label ID="Label4s" runat="server" Text='<%$ Resources:LabelCaption,lblDataformat %>'></asp:Label>
    </div>
    <div class="common_barcode_txt">
        <asp:DropDownList ID="ddlDateformats" runat="server" Style="width: 110px">
        </asp:DropDownList>
    </div>
    <div class="common_barcode_lbl">
        <asp:Label ID="lblTemplates" runat="server" Text='<%$ Resources:LabelCaption,lbl_Template %>'></asp:Label>
    </div>
    <div class="common_barcode_txt">
        <asp:DropDownList ID="ddlTemplates" runat="server" Style="width: 110px" onchange="fncTemplateChange()">
        </asp:DropDownList>
    </div>
</div>
<div class="control-group-single-res">
    <div class="common_barcode_cb">
        <asp:CheckBox ID="cbhidemrps" runat="server" />
        <%=Resources.LabelCaption.lblhidemrp%>
    </div>
    <div class="common_barcode_cb">
        <asp:CheckBox ID="cbHideprices" runat="server" />
        <%=Resources.LabelCaption.lblHideprice%>
    </div>
    <div class="common_barcode_cb">
        <asp:CheckBox ID="cbhidePkddates" runat="server" />
       <%-- Hide Date--%>
        <%=Resources.LabelCaption.lblHidePkddate%>
    </div>
    <div class="common_barcode_cb">
        <asp:CheckBox ID="cbexpiredates" runat="server" />
        <%=Resources.LabelCaption.lblHideExpDate%>
    </div>  
</div>
<div class="control-group-single-res" runat="server" id="divSizes">
    <div class="common_barcode_cb">
        <asp:CheckBox ID="cbhidecompanys" runat="server" />
        <%=Resources.LabelCaption.lblHideCompany%>
    </div>
    <div class="common_barcode_size">
        <asp:Label ID="lblSizes" runat="server" Text='<%$ Resources:LabelCaption,lbl_Size %>'></asp:Label>
    </div>
    <div class="common_barcode_cb">
        <asp:TextBox ID="txtSizes" runat="server" CssClass="form-control" Style="width: 85px"
            onMouseDown="return false"></asp:TextBox>
    </div>
     <div class="common_barcode_lbl">
        <asp:Label ID="Label1s" runat="server" Text="Printer Name"></asp:Label>
    </div>
    <div class="common_barcode_txt">
        <asp:DropDownList ID="ddlPrinterNames" runat="server" Style="width: 110px"  >
        </asp:DropDownList> 
    </div>
    <div class="common_barcode_btn" style="margin-top:10px;">
        <asp:LinkButton ID="lnkbtnPrints" runat="server" CssClass="button-red" Text='<%$ Resources:LabelCaption,btn_Print %>'
            OnClick="lnkbtnPrint_Click"></asp:LinkButton>
    </div>
</div>
<asp:HiddenField ID="hidImagebarcode" runat="server" />
<script type="text/javascript">
    $("#ContentPlaceHolder1_imagebarcodePrintUserControl_ddlDateformats").show().removeClass('chzn-done');
    $("#ContentPlaceHolder1_imagebarcodePrintUserControl_ddlDateformats").next().remove();
    $("#ContentPlaceHolder1_imagebarcodePrintUserControl_ddlTemplates").show().removeClass('chzn-done');
    $("#ContentPlaceHolder1_imagebarcodePrintUserControl_ddlTemplates").next().remove();
    $("#ContentPlaceHolder1_imagebarcodePrintUserControl_ddlPrinterNames").show().removeClass('chzn-done');
    $("#ContentPlaceHolder1_imagebarcodePrintUserControl_ddlPrinterNames").next().remove();
</script>

