﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchFilterUserControlForDialogBox.ascx.cs"
    Inherits="EnterpriserWebFinal.UserControls.SearchFilterUserControlForDialogBox" %>
<script type="text/javascript">

    $(document).ready(function () {
        $(document).on('keydown', disableFunctionKeys);
    });
    function disableFunctionKeys(e) {
        var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
        if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

            if (e.keyCode == 112) {
                e.preventDefault();
            }
        }
    };

    function fncSetValue() {
        try {
            if (SearchTableName == "Vendor") {
            $('#<%=txtVendor.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Department") {
            $('#<%=txtDepartment.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "category") {
            $('#<%=txtCategory.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Brand") {
            $('#<%=txtBrand.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "SubCategory") {
            $('#<%=txtSubCategory.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Class") {
            $('#<%=txtClass.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "SubClass") {
            $('#<%=txtSubClass.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Manafacture") {
            $('#<%=txtManufacture.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Merchandise") {
            $('#<%=txtMerchandise.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Floor") {
            $('#<%=txtFloor.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Section") {
            $('#<%=txtSection.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Bin") {
            $('#<%=txtBin.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Shelf") {
            $('#<%=txtShelf.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "WareHouse") {
           $('#<%=txtWarehouse.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Inventory") {
                $('#<%=txtItemCode.ClientID %>').val($.trim(Code));
                $('#<%=txtItemName.ClientID %>').val($.trim(Description));
            }
            if (SearchTableName == "ItemType") {
           $('#<%=txtItemType.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Location") {
           $('#<%=txtLocation.ClientID %>').val($.trim(Code));
            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
</script>

<div class="control-group-price-header">
    <asp:Label ID="lblSectionHeading" runat="server"></asp:Label>
</div>
<div class="control-group-single-res" runat="server" id="divLocation">
    <div class="label-left">
        <asp:Label ID="Label25" runat="server" Text='<%$ Resources:LabelCaption,lbl_location %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'searchFilterUserControlForDialogBox_txtLocation', 'searchFilterUserControlForDialogBox_txtVendor');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divVendorDropDown">
    <div class="label-left">
        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
    </div>
    <div class="label-right">
          <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'searchFilterUserControlForDialogBox_txtVendor', 'searchFilterUserControlForDialogBox_txtDepartment');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divDepartmentDropDown">
    <div class="label-left">
        <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'searchFilterUserControlForDialogBox_txtDepartment', 'searchFilterUserControlForDialogBox_txtCategory');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divCategoryDropDown">
    <div class="label-left">
        <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'searchFilterUserControlForDialogBox_txtCategory', 'searchFilterUserControlForDialogBox_txtBrand');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divBrandDropDown">
    <div class="label-left">
        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'searchFilterUserControlForDialogBox_txtBrand', 'searchFilterUserControlForDialogBox_txtSubCategory');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divSubCategoryDropDown">
    <div class="label-left">
        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblSubCategory %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'searchFilterUserControlForDialogBox_txtSubCategory', 'searchFilterUserControlForDialogBox_txtMerchandise');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divClassDropDown">
    <div class="label-left">
        <asp:Label ID="Label21" runat="server" Text='<%$ Resources:LabelCaption,lbl_Class %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlClass" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'searchFilterUserControlForDialogBox_txtClass', 'searchFilterUserControlForDialogBox_txtSubClass');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divSubClassDropDown">
    <div class="label-left">
        <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lblSubClass %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlSubClass" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtSubClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubClass', 'searchFilterUserControlForDialogBox_txtSubClass', 'searchFilterUserControlForDialogBox_txtMerchandise');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divMerchandiseDropDown">
    <div class="label-left">
        <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_Merchandise %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlMerchandise" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtMerchandise" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'searchFilterUserControlForDialogBox_txtMerchandise', 'searchFilterUserControlForDialogBox_txtManufacture');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divManufactureDropDown">
    <div class="label-left">
        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Manufacture %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlManufacture" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'searchFilterUserControlForDialogBox_txtManufacture', 'searchFilterUserControlForDialogBox_txtFloor');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divFloorDropDown">
    <div class="label-left">
        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblFloor %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'searchFilterUserControlForDialogBox_txtFloor', 'searchFilterUserControlForDialogBox_txtSection');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divSectionDropDown">
    <div class="label-left">
        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Section %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'searchFilterUserControlForDialogBox_txtSection', 'searchFilterUserControlForDialogBox_txtBin');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divBinDropDown">
    <div class="label-left">
        <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bin %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlBin" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'searchFilterUserControlForDialogBox_txtBin', 'searchFilterUserControlForDialogBox_txtShelf');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divShelfDropDown">
    <div class="label-left">
        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Shelf %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'searchFilterUserControlForDialogBox_txtShelf', 'searchFilterUserControlForDialogBox_txtWarehouse');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divWarehouseDropDown">
    <div class="label-left">
        <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lbl_WareHouse %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtWarehouse" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse', 'searchFilterUserControlForDialogBox_txtWarehouse', 'searchFilterUserControlForDialogBox_txtItemCode');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divBarcodeTextBox">
    <div class="label-left">
        <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Barcode %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtBarcode" runat="server" CssClass="form-control-res ucWidthForDialog"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divBatchNoTextBox">
    <div class="label-left">
        <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lblBatchNo %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res ucWidthForDialog"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divItemCodeTextBox">
    <div class="label-left">
        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtItemCode" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'searchFilterUserControlForDialogBox_txtItemCode', 'searchFilterUserControlForDialogBox_txtItemName');" runat="server" CssClass="form-control-res ucWidthForDialog"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divItemNameTextBox">
    <div class="label-left">
        <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemName %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res ucWidthForDialog"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divItemTypeDropDown">
    <div class="label-left">
        <asp:Label ID="Label24" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemType %>'></asp:Label>
    </div>
    <div class="label-right">
        <%--<asp:DropDownList ID="ddlItemType" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtItemType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'ItemType', 'searchFilterUserControlForDialogBox_txtItemType', '');"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divGRNNoTextBox">
    <div class="label-left">
        <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNNo %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtGRNNo" runat="server" CssClass="form-control-res ucWidthForDialog"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divDisplayCodeDropDown">
    <div class="label-left">
        <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lbl_DisplayCode %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:DropDownList ID="ddlDisplayCode" runat="server" CssClass="form-control-res ucWidthForDialog">
        </asp:DropDownList>
    </div>
</div>
<%-- <div class="control-group-single-res" runat="server" id="divDescriptionTextBox">
        <div class="label-left">
            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>--%>
<div class="control-group-single-res" runat="server" id="divPriceTextBox">
    <div class="label-left">
        <asp:Label ID="Label18" runat="server" Text='<%$ Resources:LabelCaption,lbl_price %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control-res ucWidthForDialog"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divFromDateTextBox">
    <div class="label-left">
        <asp:Label ID="Label19" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res ucWidthForDialog"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divToDateTextBox">
    <div class="label-left">
        <asp:Label ID="Label20" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res ucWidthForDialog"></asp:TextBox>
    </div>
</div>
<div class="control-container" runat="server" id="divControlContainer">
    <div class="control-button" runat="server" id="divFilterButton">
        <asp:LinkButton ID="lnkFilter" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btn_Fetch %>'
            OnClick="lnkFilter_Click"></asp:LinkButton>
    </div>
    <div class="control-button" runat="server" id="divClearButton">
        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btnClear %>'
            OnClick="lnkClear_Click"></asp:LinkButton>
    </div>
</div>

<%--<script type="text/javascript">
    $("select").show().removeClass('chzn-done');
    $("select").next().remove();
</script>--%>
