﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchFilterUserControl.ascx.cs"
    Inherits="EnterpriserWebFinal.UserControls.SearchFilterUserControl" %>

<style>
    .ui-accordion .ui-accordion-content {
        padding: 0px 10px !important;
        overflow:hidden;
    }

    .ui-accordion .ui-accordion-header {
        font-weight: bold !important;
        overflow:hidden;
    }
</style>
<script type="text/javascript">

    $(function () {
        var icons = {
            header: "ui-icon-circle-arrow-e",
            activeHeader: "ui-icon-circle-arrow-s"
        };
        $("#accordion").accordion({
            collapsible: true,
            icons: icons
        });
        $("#toggle").button().on("click", function () {
            if ($("#accordion").accordion("option", "icons")) {
                $("#accordion").accordion("option", "icons", null);
            } else {
                $("#accordion").accordion("option", "icons", icons);
            }
        });
    });
    $(document).ready(function () {
        $(document).on('keydown', disableFunctionKeys);
    });
    function disableFunctionKeys(e) {
        var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
        if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

            if (e.keyCode == 112) {
                e.preventDefault();
            }
        }
    };

    function fncShowSearchDialogInventory(event, Inventory, txtItemCode, txtItemType) {

        try {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode == 112) {
                fncShowSearchDialogCommon(event, Inventory, 'txtItemCode', 'txtItemType');
                event.preventDefault();
            }
            else if (keyCode == 13 && $("#<%=txtItemCode.ClientID %>").val() == '') {
                $("#<%=txtItemType.ClientID %>").focus();
                event.preventDefault();
            }
            else if (keyCode == 13) {
                event.preventDefault();
                fncGetInventoryCode_Master($("#<%=txtItemCode.ClientID %>"));
                event.preventDefault();
                event.stopPropagation();
                return false;
            }

            else {
                $("[id$=txtItemCode]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                                data: "{ 'prefix': '" + request.term + "'}",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('|')[0],
                                            valitemcode: item.split('|')[1],
                                            valName: item.split('|')[2]
                                        }
                                    }))
                                },
                                error: function (response) {
                                    ShowPopupMessageBox(response.message);
                                },
                                failure: function (response) {
                                    ShowPopupMessageBox(response.message);
                                }
                            });
                        },
                        select: function (e, i) {
                            //alert(i.item.val);
                            $('#<%=txtItemCode.ClientID %>').val($.trim(i.item.valitemcode));
                        $('#<%=txtItemName.ClientID %>').val($.trim(i.item.valName));

                        fncGetInventoryCode_Master($("#<%=txtItemCode.ClientID %>"));

                        return false;
                    },
                        minLength: 1
                    });
            }
}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }

}
//Get BatchStatus
function fncInventoryCodeEnterkey_Master() {
    try {
        fncGetBatchStatus_Master($('#<%=txtItemCode.ClientID%>').val(), $('#<%=txtBatchNo.ClientID%>'));
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }  
}
//Get Inventory Detail
function fncGetBatchStatus() {
    try {
        fncGetInventoryDetail_Master($('#<%=txtItemCode.ClientID%>').val());
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
    }
    function fncAssignInventoryDetailToControls_Master(inventorydata) {
        <%--$('#<%=txtItemCode.ClientID %>').val($.trim(Code));--%>
        $('#<%=txtItemName.ClientID %>').val($.trim(inventorydata[0]));
    }
        function fncSetValue() {
            try {
                if (SearchTableName == "InventoryType") {
                    $('#ContentPlaceHolder1_divVendorType').hide();
                    $('#ContentPlaceHolder1_divOutputvat').hide();
                    $('#ContentPlaceHolder1_txtInventoryType').val($.trim(Description));
                    $('#ContentPlaceHolder1_lblSelect').text($.trim(Description));
                    if ($('#ContentPlaceHolder1_txtInventoryType').val() == 'GST') {
                        $('#ContentPlaceHolder1_txtSelectedInventoryType').val('')
                        $('#ContentPlaceHolder1_divOutputvat').show();
                        $('#ContentPlaceHolder1_divVendorType').hide();
                        $('#ContentPlaceHolder1_txtOutputVat').select();
                    }
                    if ($('#ContentPlaceHolder1_txtInventoryType').val() == 'Vendor') {
                        $('#ContentPlaceHolder1_txtSelectedInventoryType').val('')
                        $('#ContentPlaceHolder1_divVendorType').show();
                        $('#ContentPlaceHolder1_divOutputvat').hide();
                    }
                }
                if (SearchTableName == "OutputVat") {
                    $('#ContentPlaceHolder1_txtOutputVat').val($.trim(Description));
                    $('#ContentPlaceHolder1_txtSelectedInventoryType').val($.trim(Description));
                }
                if (SearchTableName == "AutoPo") {
                    $('#ContentPlaceHolder1_txtAutoPo').val($.trim(Description));

                }
                if (SearchTableName == "Reorder") {
                    $('#ContentPlaceHolder1_txtReorder').val($.trim(Description));
                    if ($('#ContentPlaceHolder1_txtReorder').val() == 'Max.Qty')
                        $('#ContentPlaceHolder1_lblMinimum').text('Maximum Qty');
                    if ($('#ContentPlaceHolder1_txtReorder').val() == 'Order Cycle Days')
                        $('#ContentPlaceHolder1_lblMinimum').text('Order Cycle');
                }
                if (SearchTableName == "GST") {

                    $('#ContentPlaceHolder1_txtOutputVat').val($.trim(Description));
                }
                if (ControlID == "txtSelectedInventoryType") {
                    $('#ContentPlaceHolder1_txtSelectedInventoryType').val(Code)
                }
                else {
                    if (SearchTableName == "Vendor") {
                        $('#<%=txtVendor.ClientID %>').val($.trim(Code));
                    }
                    if (SearchTableName == "Department") {
                        $('#<%=txtDepartment.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "category") {                   
                    $('#<%=txtCategory.ClientID %>').val($.trim(Code));
                     // fncGetDepartmentForSelecetedCategory(Description);
                }
                    if (SearchTableName == "Brand") {
                     Brand = $('#<%=txtCategory.ClientID %>').val();
                    $('#<%=txtBrand.ClientID %>').val($.trim(Code));
                }
                    if (SearchTableName == "SubCategory") {
                         Brand = $('#<%=txtBrand.ClientID %>').val();
                    $('#<%=txtSubCategory.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "Class") {
                    $('#<%=txtClass.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "SubClass") {
                    $('#<%=txtSubClass.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "Manafacture") {
                    $('#<%=txtManufacture.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "Merchandise") {
                    $('#<%=txtMerchandise.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "Floor") {
                    $('#<%=txtFloor.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "Section") {
                    $('#<%=txtSection.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "Bin") {
                    $('#<%=txtBin.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "Shelf") {
                    $('#<%=txtShelf.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "WareHouse") {
                    $('#<%=txtWarehouse.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtItemName.ClientID %>').val($.trim(Description));
                }
                if (SearchTableName == "ItemType") {
                    $('#<%=txtItemType.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "Location") {
                    $('#<%=txtLocation.ClientID %>').val($.trim(Code));
                }
                if (SearchTableName == "DisplayCode") {
                    $('#<%=txtDisplayCode.ClientID %>').val($.trim(Code));
                }
            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

 function fncGetDepartmentForSelecetedCategory(CategoryCode) { //sankar

            try {
                var obj = {};
                obj.CategoryCode = CategoryCode.trim().replace("'", "''");
                //$data = { prefix: CategoryCode.replace("'", "''") };
                $.ajax({
                    type: "POST",
                    url: "frmInventoryMaster.aspx/GetDeptCode",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        $('#<%=txtDepartment.ClientID %>').val(msg.d);
                        //console.log($('#<%=txtDepartment.ClientID %>').val());
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message)
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
</script>
<%--<div class="container-left-price" id="pnlFilter" runat="server">--%>
<div id="accordion">
    <h1>Filtration</h1>
    <div>
    <%--<div class="control-group-price-header">
        <%--//<asp:Label ID="lblSectionHeading" runat="server"></asp:Label>--%>
    <%--</div>--%>
    <div class="control-group-single-res" runat="server" id="divLocation">
        <div class="label-left">
            <asp:Label ID="Label25" runat="server" Text='<%$ Resources:LabelCaption,lbl_location %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'searchFilterUserControl_txtLocation', 'searchFilterUserControl_txtVendor');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divVendorDropDown">
        <div class="label-left">
            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'searchFilterUserControl_txtVendor', 'searchFilterUserControl_txtDepartment');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divDepartmentDropDown">
        <div class="label-left">
            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'searchFilterUserControl_txtDepartment', 'searchFilterUserControl_txtCategory');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divCategoryDropDown">
        <div class="label-left">
            <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'searchFilterUserControl_txtCategory', 'searchFilterUserControl_txtBrand');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divBrandDropDown">
        <div class="label-left">
            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'searchFilterUserControl_txtBrand', 'searchFilterUserControl_txtSubCategory');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divSubCategoryDropDown">
        <div class="label-left">
            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblSubCategory %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'searchFilterUserControl_txtSubCategory', 'searchFilterUserControl_txtClass');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divClassDropDown">
        <div class="label-left">
            <asp:Label ID="Label21" runat="server" Text='<%$ Resources:LabelCaption,lbl_Class %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlClass" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'searchFilterUserControl_txtClass', 'searchFilterUserControl_txtSubClass');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divSubClassDropDown">
        <div class="label-left">
            <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lblSubClass %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlSubClass" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtSubClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubClass', 'searchFilterUserControl_txtSubClass', 'searchFilterUserControl_txtMerchandise');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divMerchandiseDropDown">
        <div class="label-left">
            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_Merchandise %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlMerchandise" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtMerchandise" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'searchFilterUserControl_txtMerchandise', 'searchFilterUserControl_txtManufacture');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divManufactureDropDown">
        <div class="label-left">
            <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Manufacture %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlManufacture" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'searchFilterUserControl_txtManufacture', 'searchFilterUserControl_txtFloor');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divFloorDropDown">
        <div class="label-left">
            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblFloor %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'searchFilterUserControl_txtFloor', 'searchFilterUserControl_txtSection');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divSectionDropDown">
        <div class="label-left">
            <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Section %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'searchFilterUserControl_txtSection', 'searchFilterUserControl_txtBin');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divBinDropDown">
        <div class="label-left">
            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bin %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlBin" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'searchFilterUserControl_txtBin', 'searchFilterUserControl_txtShelf');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divShelfDropDown">
        <div class="label-left">
            <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Shelf %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'searchFilterUserControl_txtShelf', 'searchFilterUserControl_txtWarehouse');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divWarehouseDropDown">
        <div class="label-left">
            <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lbl_WareHouse %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtWarehouse" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse', 'searchFilterUserControl_txtWarehouse', 'searchFilterUserControl_txtItemCode');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divBarcodeTextBox">
        <div class="label-left">
            <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Barcode %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtBarcode" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divBatchNoTextBox">
        <div class="label-left">
            <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lblBatchNo %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divItemCodeTextBox">
        <div class="label-left">
            <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtItemCode" onkeydown="return fncShowSearchDialogInventory(event, 'Inventory',  'searchFilterUserControl_txtItemCode', 'searchFilterUserControl_txtItemName');" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divItemNameTextBox" >
        <div class="label-left">
            <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemName %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res" ></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divItemTypeDropDown">
        <div class="label-left">
            <asp:Label ID="Label24" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemType %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlItemType" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtItemType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'ItemType', 'searchFilterUserControl_txtItemType', '');"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divGRNNoTextBox">
        <div class="label-left">
            <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNNo %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtGRNNo" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divDisplayCodeDropDown">
        <div class="label-left">
            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lbl_DisplayCode %>'></asp:Label>
        </div>
        <div class="label-right">
            <%--<asp:DropDownList ID="ddlDisplayCode" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
            <asp:TextBox ID="txtDisplayCode" runat="server" CssClass="form-control-res" 
                onkeydown="return fncShowSearchDialogCommon(event, 'DisplayCode', 'txtDisplayCode', '');"></asp:TextBox>
        </div>
    </div>
    <%-- <div class="control-group-single-res" runat="server" id="divDescriptionTextBox">
        <div class="label-left">
            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>--%>
    <div class="control-group-single-res" runat="server" id="divPriceTextBox">
        <div class="label-left">
            <asp:Label ID="Label18" runat="server" Text='<%$ Resources:LabelCaption,lbl_price %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divFromDateTextBox">
        <div class="label-left">
            <asp:Label ID="Label19" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divToDateTextBox">
        <div class="label-left">
            <asp:Label ID="Label20" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>
    <div class="control-group-single-res" runat="server" id="divGst">
        <div class="label-left">
            <asp:Label ID="lblGst" runat="server" Text="GST"></asp:Label>
        </div>
        <div class="label-right">
           <%-- <asp:DropDownList ID="ddGst" runat="server" CssClass="form-control-res">
            </asp:DropDownList>--%>
            <asp:TextBox ID="txtGst" runat="server" CssClass="form-control-res"
                 onkeydown="return fncShowSearchDialogCommon(event, 'TaxMasterQuck', 'searchFilterUserControl_txtGst', '');"></asp:TextBox>
        </div>
    </div>
            <div class="control-group-single-res" runat="server" id="divModelNoTextBox">
        <div class="label-left">
            <asp:Label ID="lblModel" runat="server" Text='Model No'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtModelNo" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>
</div>
    </div>
<div class="control-container" runat="server" id="divControlContainer">
    <div class="control-button" runat="server" id="divFilterButton">
        <asp:LinkButton ID="lnkFilter" runat="server" class="button-red" Text="Fetch"
            OnClick="lnkFilter_Click"></asp:LinkButton>
        <%--'<%$ Resources:LabelCaption,btn_Filter %>'--%>
    </div>
    <div class="control-button" runat="server" id="divClearButton">
        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btnClear %>'
            OnClick="lnkClear_Click"></asp:LinkButton>
    </div>

</div>
<%--</div>--%>
