﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportFilterUserControl.ascx.cs"
    Inherits="EnterpriserWebFinal.UserControls.ReportFilterUserControl" %>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('keydown', disableFunctionKeys);
    });
    function disableFunctionKeys(e) {
        var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
        if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

            if (e.keyCode == 112) {
                e.preventDefault();
            }
        }
    };
    function fncSetValue() {
        try {
            if (SearchTableName == "Inventory") {
            $('#<%=txtInventory.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Vendor") {
            $('#<%=txtVendor.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Department") {
            $('#<%=txtDepartment.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "category") {
            $('#<%=txtCategory.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Brand") {
            $('#<%=txtBrand.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "SubCategory") {
            $('#<%=txtSubCategory.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Class") {
            $('#<%=txtClass.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "SubClass") {
            $('#<%=txtSubClass.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Manafacture") {
            $('#<%=txtManufacture.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Merchandise") {
            $('#<%=txtMerchandise.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Floor") {
            $('#<%=txtFloor.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Section") {
            $('#<%=txtSection.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "MemberType") {
            $('#<%=txtMemberType.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Cashier") {
            $('#<%=txtCashier.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "WareHouse") {
           $('#<%=txtWarehouse.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Voucher") {
                $('#<%=txtVoucher.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Terminal") {
           $('#<%=txtTerminal.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Location") {
                $('#ContentPlaceHolder1_GSTReportFilterUserControl_txtLocation').val($.trim(Code));
           $('#<%=txtLocation.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "CompanyName") {
           $('#<%=txtCompany.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "CUSTOMER") {
                $('#ContentPlaceHolder1_txtCustomer').val($.trim(Description));
                $('#ContentPlaceHolder1_txtCustomerMore').val($.trim(Description));
                $('#<%=txtCustomer.ClientID %>').val($.trim(Code)); 
                $('#ContentPlaceHolder1_hidCustCode').val($.trim(Code));
            }
            if (SearchTableName == "Origin") {
                $('#<%=txtOrgin.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Agent") {
                $('#<%=txtAgent.ClientID %>').val($.trim(Code));
            }
            if (SearchTableName == "Companyid") {
                $('#<%=txtCompId.ClientID %>').val($.trim(Code));
            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
</script>


<div class="col-md-12 form_bootstyle"  runat="server" id="divInventory">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label8" runat="server" Text="Inventory"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtInventory" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'ReportFilterUserControl_txtInventory', 'ReportFilterUserControl_txtLocation');" CssClass="form-control"  style="width:100%"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle"  runat="server" id="divLocation">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label5" runat="server" Text="Location"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'ReportFilterUserControl_txtLocation', 'ReportFilterUserControl_txtDepartment');"></asp:TextBox>
    </div>
</div>
<div class="col-md-12 form_bootstyle" runat="server" id="divMemberType">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label3" runat="server" Text="Member Type"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <%--<asp:DropDownList ID="ddlMemberType" runat="server" style="width:100%">
            <asp:ListItem>CC</asp:ListItem>
            <asp:ListItem>CLS</asp:ListItem>
            <asp:ListItem>IBC</asp:ListItem>
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtMemberType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'MemberType', 'txtMemberType', '');"></asp:TextBox>
    </div>
</div>
<div class="col-md-12 form_bootstyle" runat="server" id="divCustomer">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label4" runat="server" Text="Customer"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
       <asp:TextBox ID="txtCustomer" runat="server" CssClass="form-control" onkeydown="return fncShowSearchDialogCommon(event, 'CUSTOMER',  'txtCustomer', 'txtBrand');"  style="width:100%"></asp:TextBox>
    </div>
</div>
<div class="col-md-12 form_bootstyle" runat="server" id="divDepartmentDropDown">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label6" runat="server" Text="Department"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'ReportFilterUserControl_txtDepartment', 'ReportFilterUserControl_txtCategory');"></asp:TextBox>
    </div>
</div>
<div class="col-md-12 form_bootstyle" runat="server" id="divCategoryDropDown">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label1" runat="server" Text="Category"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'ReportFilterUserControl_txtCategory', 'ReportFilterUserControl_txtSubCategory');"></asp:TextBox>
    </div>
</div>
<div class="col-md-12 form_bootstyle" runat="server" id="divSubCategoryDropDown">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label7" runat="server" Text="SubCategory"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'ReportFilterUserControl_txtSubCategory', 'ReportFilterUserControl_txtBrand');"></asp:TextBox>
    </div>
</div>
<div class="col-md-12 form_bootstyle" runat="server" id="divBrandDropDown">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label2" runat="server" Text="Brand"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'ReportFilterUserControl_txtBrand', 'ReportFilterUserControl_txtVendor');"></asp:TextBox>
    </div>
</div>
<div class="col-md-12 form_bootstyle"  runat="server" id="divVendorDropDown">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label9" runat="server" Text="Vendor"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
           <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'ReportFilterUserControl_txtVendor', 'ReportFilterUserControl_txtClass');"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle"  runat="server" id="divCashier">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label10" runat="server" Text="Cashier"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <%--<asp:DropDownList ID="ddlCashier" runat="server" style="width:100%">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtCashier" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Cashier', 'ReportFilterUserControl_txtCashier', '');"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle"  runat="server" id="divTerminal">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label11" runat="server" Text="Terminal"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <%--<asp:DropDownList ID="ddlTerminal" runat="server" style="width:100%">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtTerminal" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Terminal', 'ReportFilterUserControl_txtTerminal', '');"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle"  runat="server" id="divVoucher">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label12" runat="server" Text="Voucher Type"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <%--<asp:DropDownList ID="ddlvoucher" runat="server" style="width:100%">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtVoucher" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Voucher', 'ReportFilterUserControl_txtVoucher', '');"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle"  runat="server" id="divClass">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label13" runat="server" Text="Class"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'ReportFilterUserControl_txtClass', 'ReportFilterUserControl_txtSubClass');"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle" runat="server" id="divSubClass">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label14" runat="server" Text="SubClass"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtSubClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubClass', 'ReportFilterUserControl_txtSubClass', 'ReportFilterUserControl_txtWarehouse');"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle" runat="server" id="divWarehouse">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label15" runat="server" Text="Warehouse"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtWarehouse" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse', 'ReportFilterUserControl_txtWarehouse', 'ReportFilterUserControl_txtFloor');"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle" runat="server" id="divFloor">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label16" runat="server" Text="Floor"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'ReportFilterUserControl_txtFloor', 'txtCompany');"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle" runat="server" id="divCompany">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label17" runat="server" Text="Company"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <%--<asp:DropDownList ID="ddlCompany" runat="server" style="width:100%">
        </asp:DropDownList>--%>
        <asp:TextBox ID="txtCompany" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'CompanyName', 'ReportFilterUserControl_txtCompany', '');"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle" runat="server" id="divManufacture">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label18" runat="server" Text="Manufacture"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'ReportFilterUserControl_txtManufacture', 'ReportFilterUserControl_txtFloor');"></asp:TextBox>
    </div>
</div>





<div class="col-md-12 form_bootstyle" runat="server" id="divSection">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label21" runat="server" Text="Section"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'ReportFilterUserControl_txtSection', 'ReportFilterUserControl_txtBin');"></asp:TextBox>
    </div>
</div>


<div class="col-md-12 form_bootstyle" runat="server" id="divOrgin">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label24" runat="server" Text="Orgin"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtOrgin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Origin', 'ReportFilterUserControl_txtOrgin', 'ReportFilterUserControl_');"></asp:TextBox>
    </div>
</div>

<div class="col-md-12 form_bootstyle" runat="server" id="divMerchandise">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label23" runat="server" Text="Merchandise"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtMerchandise" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'ReportFilterUserControl_txtMerchandise', 'ReportFilterUserControl_txtManufacture');"></asp:TextBox>
    </div>
</div>
<div class="col-md-12 form_bootstyle" runat="server" id="divAgent">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label19" runat="server" Text="Agent"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtAgent" runat="server" CssClass="form-control-res" 
            onkeydown="return fncShowSearchDialogCommon(event, 'Agent', 'ReportFilterUserControl_txtAgent', '');"></asp:TextBox>
    </div>
</div>
<div class="col-md-12 form_bootstyle" runat="server" id="divCompId">
    <div class="col-md-4 form_bootstyle">
        <asp:Label ID="Label20" runat="server" Text="Company Id"></asp:Label>
    </div>
    <div class="col-md-8 form_bootstyle">
        <asp:TextBox ID="txtCompId" runat="server" CssClass="form-control-res" 
            onkeydown="return fncShowSearchDialogCommon(event, 'Companyid', 'ReportFilterUserControl_txtCompId', '');"></asp:TextBox>
    </div>
</div>





