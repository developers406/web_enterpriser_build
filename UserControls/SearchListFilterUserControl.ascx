﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchListFilterUserControl.ascx.cs"
    Inherits="EnterpriserWebFinal.UserControls.SearchListFilterUserControl" %>
<div class="control-group-price-header">
    <asp:Label ID="lblSectionHeading" runat="server"></asp:Label>
</div>
<div class="control-group-single-res" runat="server" id="divLocation">
    <div class="label-left">
        <asp:Label ID="Label25" runat="server" Text='<%$ Resources:LabelCaption,lbl_location %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlLocation" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divFromDateTextBox">
    <div class="label-left">
        <asp:Label ID="Label19" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divToDateTextBox">
    <div class="label-left">
        <asp:Label ID="Label20" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divVendorDropDown">
    <div class="label-left">
        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlVendor" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divDepartmentDropDown">
    <div class="label-left">
        <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlDept" runat="server" SelectionMode="Multiple" CssClass="form-control-res" /><%--SelectionMode="Multiple"--%>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divAttributewiseDepartmentDropDown">
    <div class="label-left">
        <asp:Label ID="Label26" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlAttributewiseDepartmentDropDown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDeptSelectedIndexChanged"  CssClass="form-control-res" /><%--SelectionMode="Multiple"--%>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divAttributeCategoryDropDown">
    <div class="label-left">
        <asp:Label ID="Label27" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlAttributeCategoryDropDown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategorySelectedIndexChanged" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divCategoryDropDown">
    <div class="label-left">
        <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlCategory" runat="server" SelectionMode="Multiple" CssClass="form-control-res" /><%--SelectionMode="Multiple"--%>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divBrandDropDown">
    <div class="label-left">
        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlBrand" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divSubCategoryDropDown">
    <div class="label-left">
        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblSubCategory %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlSubCategory" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divClassDropDown">
    <div class="label-left">
        <asp:Label ID="Label21" runat="server" Text='<%$ Resources:LabelCaption,lbl_Class %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlClass" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divSubClassDropDown">
    <div class="label-left">
        <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lblSubClass %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlSubClass" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divMerchandiseDropDown">
    <div class="label-left">
        <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_Merchandise %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlMerchandise" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divManufactureDropDown">
    <div class="label-left">
        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Manufacture %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:ListBox ID="ddlManufacture" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divFloorDropDown">
    <div class="label-left">
        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblFloor %>'></asp:Label>
    </div>
    <div class="label-right"> 
        <asp:ListBox ID="ddlFloor" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divSectionDropDown">
    <div class="label-left">
        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Section %>'></asp:Label>
    </div>
    <div class="label-right"> 
        <asp:ListBox ID="ddlSection" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divBinDropDown">
    <div class="label-left">
        <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bin %>'></asp:Label>
    </div>
    <div class="label-right"> 
        <asp:ListBox ID="ddlBin" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divShelfDropDown">
    <div class="label-left">
        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Shelf %>'></asp:Label>
    </div>
    <div class="label-right"> 
        <asp:ListBox ID="ddlShelf" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divWarehouseDropDown">
    <div class="label-left">
        <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lbl_WareHouse %>'></asp:Label>
    </div>
    <div class="label-right"> 
        <asp:ListBox ID="ddlWarehouse" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divBarcodeTextBox">
    <div class="label-left">
        <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Barcode %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtBarcode" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divBatchNoTextBox">
    <div class="label-left">
        <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lblBatchNo %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divItemCodeTextBox">
    <div class="label-left">
        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divItemNameTextBox">
    <div class="label-left">
        <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemName %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divItemTypeDropDown">
    <div class="label-left">
        <asp:Label ID="Label24" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemType %>'></asp:Label>
    </div>
    <div class="label-right"> 
         <asp:ListBox ID="ddlItemType" runat="server" SelectionMode="Multiple" CssClass="form-control-res" />
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divGRNNoTextBox">
    <div class="label-left">
        <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNNo %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtGRNNo" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>
<div class="control-group-single-res" runat="server" id="divDisplayCodeDropDown">
    <div class="label-left">
        <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lbl_DisplayCode %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:DropDownList ID="ddlDisplayCode" runat="server" CssClass="form-control-res">
        </asp:DropDownList>
    </div>
</div>
<%-- <div class="control-group-single-res" runat="server" id="divDescriptionTextBox">
        <div class="label-left">
            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
        </div>
    </div>--%>
<div class="control-group-single-res" runat="server" id="divPriceTextBox">
    <div class="label-left">
        <asp:Label ID="Label18" runat="server" Text='<%$ Resources:LabelCaption,lbl_price %>'></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>


<div class="control-container" runat="server" id="divControlContainer">
    <div class="control-button" runat="server" id="divFilterButton">
        <asp:LinkButton ID="lnkFilter" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btn_Filter %>'
            OnClick="lnkFilter_Click"></asp:LinkButton>
    </div>
    <div class="control-button" runat="server" id="divClearButton">
        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btnClear %>'
            OnClick="lnkClear_Click"></asp:LinkButton>
    </div>
</div>
<asp:HiddenField ID="hidParamCode" runat="server" Value="" />
<%--</div>--%>
