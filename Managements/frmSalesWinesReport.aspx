﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmSalesWinesReport.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmSalesWinesReport" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .divwidth {
            width: 100%;
            margin-left: 0;
        }

        .divwidthShow {
            width: 74%;
            margin-left: 0;
        }

          /*.grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 135px;
            max-width: 120px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 100px;
            max-width: 120px;
          
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 100px;
            max-width: 120px;*/
         /* //  text-align: center;*/
        /*}

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width:102px;
            max-width:150px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width:125px;
            max-width:150px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
           min-width:100px;
            max-width:150px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
             min-width:100px;
            max-width:150px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 100px;
            max-width: 135px;*/
           /* text-align: right;*/
        /*}

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width:116px;
            max-width:150px;
        }*/

          .columnwidth
        {
            width: 200px;
        }
      
    </style>
      <script type="text/javascript">
          function pageLoad() {
              $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
              $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });

              if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                  $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
              }

              if ($("#<%= txtToDate.ClientID %>").val() === '') {
                  $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
              }
          }
          function fncExcelExport() {
              try {
                  $('#<%=btnExcelExport.ClientID %>').click();
              }
              catch (err) {
                  fncToastError(err.message)
              }
          }
          function fncSetValue() {
              try {
                  if (SearchTableName == "Vendor") {
                      $('#<%=txtVendor.ClientID %>').val(Description);
                   }
               }
               catch (err) {
                   ShowPopupMessageBox(err.message);
               }
           }
      </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <asp:UpdatePanel ID="updtPnlTop" UpdateMode="Always" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Inventory Movement </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="container-group-price">

                    <%--<div class="container-date-report" id="pnlFilter" runat="server">--%>
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <div class="barcode-header">
                            Search By
                        </div>
                        <div id="locfilter" runat="server" class="control-group-single-res" style="margin-top: 10px">
                            <div class="label-left">
                                <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res"
                                    onchange="fncLocationChange();">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Department"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Category"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Brand"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtSubCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Labelsc" runat="server" Text="SubCategory"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtFloor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Floor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                            </div>
                        </div>
                       <%-- <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Labelsec" runat="server" Text="Section"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtShelf');"></asp:TextBox>
                            </div>
                        </div>--%>
                       <%-- <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text="Shelf"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtItemtype');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="LabelIT" runat="server" Text="Item Type"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemtype" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'ItemType', 'txtItemtype', 'txtItemCode');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text="Item Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            <%-- onkeyup="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', 'txtItemName');" Vijay
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label16" runat="server" Text="Item Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>--%>
                        <div class="control-group-single-res">
                            <div class="barcode-header">
                                Filterations
                            </div>
                        </div>
                        <div class="control-group-single-res" style="margin-top: 10px">
                            <div class="label-left">
                                <asp:Label ID="Lblfrod" runat="server" Text="FromDate"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblto" runat="server" Text="ToDate"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                       <%-- <div class="control-group-single-res">
                            <div class="float_left">
                                <asp:CheckBox ID="chkBatchwise" runat="server" Checked="true" GroupName="priceMenu" />
                                <asp:Label ID="Label10" runat="server" Checked="true" Text="Show Batch Wise"></asp:Label>
                            </div>
                            <div class="float_left" style="padding-left: 10px;">
                                <asp:CheckBox ID="chkShowPackage" runat="server" GroupName="allMenu" />
                                <asp:Label ID="Label19" runat="server" Checked="true" Text="Show Qty in Package"></asp:Label>
                            </div>
                        </div>--%>
                     <%--   <div class="control-group-single-res">
                            <div style="width: 45%; float: left;">
                                <div class="float_left" style="width: 50%">
                                    <asp:Label ID="Label6" runat="server" Checked="true" Text="No of items"></asp:Label>
                                </div>
                                <div class="float_left" style="padding-left: 2px; width: 50%">
                                    <asp:TextBox ID="txtNoofItems" runat="server" Text="100" CssClass="form-control-res-right"></asp:TextBox>
                                </div>
                            </div>
                            <div style="width: 45%; float: left; padding-left: 4px;">
                                <div class="float_left" style="width: 50%">
                                    <asp:CheckBox ID="cbNewItem" runat="server" Text="New Item" />
                                </div>
                                <div class="float_left" style="padding-left: 2px; width: 50%">
                                    <asp:TextBox ID="txtNewItem" runat="server" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                                </div>
                            </div>
                        </div>--%>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilter" runat="server" onclick="lnkFilter_Click" class="button-red"><i class="icon-play" ></i>Fetch</asp:LinkButton>

                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm()"><i class="icon-play"></i>Clear</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                    <div class="col-md-12">
                        <div class="col-md-9">
                        </div>
                       <%-- <div class="col-md-3 sort_by">
                            <div class="col-md-3">
                                <label id="lblSortby" style="margin-top: 5px;">Sort by:</label>
                            </div>
                            <div class="col-md-7">
                                <asp:DropDownList runat="server" Width="100%" ID="ddlSort">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-1">
                                <label id="lblUp" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                            </div>
                            <div class="col-md-1">
                                <label id="lblDown" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                            </div>
                        </div>--%>
                    </div>
                    <div class="col-md-12">

                          <asp:UpdatePanel ID="updtPnltopgrdDepartmentgrd" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails"  >
                    <div class="row">
                        <%--<div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                        <div class="grdLoad" >
                            <%--<table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                <thead>
                                    <tr>
                                    
                                        <th scope="col">Details
                                        </th>
                                        <th scope="col">Brandy Ltr
                                        </th>
                                        <th scope="col">Whisky Ltr
                                        </th>
                                        <th scope="col">Rum Ltr
                                        </th>
                                        <th scope="col">Gin +Vodka +Liqr Ltr
                                        </th>
                                        <th scope="col">LAB Ltr
                                        </th>
                                        <th scope="col">Wine Ltr
                                        </th>
                                        <th scope="col">Total
                                        </th>
                                        <th scope="col">Beer ltr
                                        </th>
                                    </tr>
                                </thead>
                            </table>--%>
                            <div class="GridDetails" style="overflow-x:scroll; overflow-y: scroll; height: 340px; width: 995px; background-color: aliceblue;">
                                <asp:GridView ID="grdItemDetails" runat="server" AllowSorting="true"
                                    AutoGenerateColumns="True" ShowHeaderWhenEmpty="true" oncopy="return false"
                                    AllowPaging="False" PageSize="10" ShowHeader="True" CssClass="pshro_GridDgn grdLoad"
                                    DataKeyNames="Details"  OnRowDataBound="GridView1_RowDataBound">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                 
                                   <%-- <Columns>
                                     
                                         <asp:BoundField DataField="Details" HeaderText="Details"></asp:BoundField>
                                        <asp:BoundField DataField="Brandy Ltr" HeaderText="Brandy Ltr"></asp:BoundField>
                                        <asp:BoundField DataField="Whisky Ltr" HeaderText="Whisky Ltr"></asp:BoundField>
                                         <asp:BoundField DataField="Rum Ltr" HeaderText="Rum Ltr"></asp:BoundField>
                                        <asp:BoundField DataField="Gin Ltr+ Vodka Ltr+LiqueurLtr" HeaderText="Gin Ltr+ Vodka Ltr+LiqueurLtr"></asp:BoundField>
                                        <asp:BoundField DataField="LAB Ltr" HeaderText="LAB Ltr"></asp:BoundField>
                                         <asp:BoundField DataField="Wine Ltr" HeaderText="Wine Ltr"></asp:BoundField>
                                        <asp:BoundField DataField="Total" HeaderText="Total"></asp:BoundField>
                                        <asp:BoundField DataField="Beer ltr" HeaderText="Beer ltr"></asp:BoundField>

                                   </Columns>--%>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                  <asp:UpdateProgress ID="uprepeaterprogress" runat="server">
                                <ProgressTemplate>
                                    <div class="modal-loader">
                                        <div class="center-loader">
                                            <img alt="" src="../images/loading_spinner.gif" />
                                        </div>
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
            </ContentTemplate>
        </asp:UpdatePanel>

                      <%--  <div class="control-button">
                            <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="return fncHideFilter()">Hide Filter</asp:LinkButton>
                        </div>--%>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkExport" runat="server" class="button-blue" OnClientClick="fncExcelExport();return false;">Export</asp:LinkButton>
                        </div>
                       <%-- <div class="control-button">
                            <asp:LinkButton ID="lnkClearAll" Visible="false" runat="server" class="button-blue"
                                OnClientClick="return clearForm()">Clear</asp:LinkButton>
                        </div>--%>
                       <%-- <div class="control-button">
                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" >Print</asp:LinkButton>
                        </div>--%>
                     <%--   <div class="control-button">
                            <asp:LinkButton ID="lnkAttribute" runat="server" class="button-blue" OnClientClick="fncOpenAttributeDailog();return false;">Attributes</asp:LinkButton>
                        </div>--%>
                    </div>
                </div>
                <div id="divtran" class="container-group-full" style="display: none;">
                    <div class="top-purchase-container">
                        <div class="top-purchase-middle-container" style="padding: 15px 0 5px 5px;">
                            <asp:RadioButtonList ID="rdoStatus" runat="server" Class="radioboxlist" RepeatDirection="Horizontal">
                                <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Received" Value="Received"></asp:ListItem>
                                <asp:ListItem Text="Issued" Value="Issued"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                
                     
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
     
    <div class="display_none">

        <asp:Button ID="btnExcelExport" runat="server" OnClick="lnkExport_Click" />
        <asp:HiddenField ID="hidCurLocation" runat="server" />

        <div class="container welltextiles" id="divAttribute">
            <div class="barcode-header" style="background-color: #c6b700">
                Attributes
            </div>
            <div style="height: auto; border: solid; overflow-x: visible; overflow-y: hidden; height: 215px;">
                <asp:GridView ID="grdAttribute" runat="server" AutoGenerateColumns="False"
                    ShowHeaderWhenEmpty="true" CssClass="fixed_header_Empty_grid"
                    EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                    <EmptyDataTemplate>
                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                    <RowStyle CssClass="pshro_GridDgnStyle" />
                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                    <PagerStyle CssClass="pshro_text" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <Columns>
                    </Columns>
                </asp:GridView>
            </div>
            <br />
            <div class="col-md-2">
            </div>
            <div class="col-md-9">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
        
    </div> 
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" /> 
</asp:Content>
