﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmVendorAnalysis.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmVendorAnalysis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'VendorAnalysis');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "VendorAnalysis";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        $(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        });

        function ValidateForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }

                if ($('#<%= txtVendor.ClientID %>').val() == "" && $('#<%= txtVendor.ClientID %>').val() == "") {
                    Show = Show + '\n  Choose any Filtration..!';
                    $("#<%= txtVendor.ClientID %>").focus();
                }

                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }

                else {
                    return true;
                }
            }
            catch (err) {
                return false;
                ShowPopupMessageBox(err.Message);
            }
        }

        function clearForm() {
            try {

                $('#<%= DropDownLocation.ClientID %>').val('');
                $('#<%= txtVendor.ClientID %>').val('');
                $("[id*=lnkPrint]").hide();
                $("table[id$='grdVendorDetails']").html("");
                //$("select").val(0);
                $("select").trigger("liszt:updated");
            }
            catch (err) {

                ShowPopupMessageBox(err.Message);
                return false;
            }

            return false;
        }
        
        function fncSetValue() {
            try {
                $('#<%=hidVendorName.ClientID %>').val(Description);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Management</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Vendor-Analysis</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-date-report" id="divDateFilter" runat="server">
            <div class="barcode-header">
                Filtrations
            </div>
            <asp:Panel ID="Panel7" runat="server">
                <div class="control-group" style="margin-top: 10px">
                    <div style="float: left">
                        <asp:Label ID="Label51" runat="server" Text='<%$ Resources:LabelCaption,lbllocation %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:DropDownList ID="DropDownLocation" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_vendor %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                        <asp:HiddenField ID="hidVendorName" runat="server" />
                    </div>
                </div>
                <div class="control-group" style="margin-top: 5px;">
                    <div style="float: left">
                        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblFromdate %>'></asp:Label>
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                    </div>
                    <div style="float: left; margin-left: 10px">
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblTodate %>'></asp:Label>
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                    </div>
                    <div style="float: right; margin-top: 20px">
                        <asp:LinkButton ID="lnkPrint" runat="server" class="btn btn-danger" Width="100px" Text='<%$ Resources:LabelCaption,btn_print %>'
                            Font-Bold="true" Visible="False" OnClick="lnkPrint_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkFetch" runat="server" class="btn btn-danger" Width="121px" Text='<%$ Resources:LabelCaption,btn_fetch %>'
                            Font-Bold="true" OnClientClick="return ValidateForm()" OnClick="lnkFetch_Click"> </asp:LinkButton>
                        <asp:LinkButton ID="lnkClear" runat="server" class="btn btn-danger" Width="100px" Text='<%$ Resources:LabelCaption,btnclear %>'
                            Font-Bold="true" OnClientClick="return clearForm()"></asp:LinkButton>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="container-date-report-right" id="div1" runat="server">
            <div class="barcode-header" id="divVendorDtl" visible="false" runat="server">
            </div>
            <div class="row">
                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                    <div class="grid-overflow-default" id="HideFilter_GridOverFlow" runat="server">
                        <asp:GridView ID="grdVendorDetails" runat="server" AutoGenerateColumns="false" PageSize="14"
                             ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgn">
                            <PagerStyle CssClass="pshro_text" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                            <Columns>
                                <asp:BoundField DataField="Location" HeaderText="Location" ItemStyle-Width="50">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="VendorCode" HeaderText="Vendor Code" ItemStyle-Width="50">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="VendorName" HeaderText="Vendor Name" ItemStyle-Width="400">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PurchaseValue" HeaderText="Purchase Value" ItemStyle-Width="50">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SalesValue" HeaderText="Sales Value" ItemStyle-Width="50">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PQty" HeaderText="PQty" ItemStyle-Width="50">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SQty" HeaderText="SQty" ItemStyle-Width="50">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="QoH" HeaderText="QoH" ItemStyle-Width="50">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="StockValue" HeaderText="StockValue" ItemStyle-Width="50">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                PageButtonCount="5" Position="Bottom" />
                            <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        //$(function () {
        //    $('.search_textbox').each(function (i) {
        //        $(this).quicksearch("[id*=grdVendorDetails] tr:not(:has(th))", {
        //            'testQuery': function (query, txt, row) {
        //                return $(row).children(":eq(" + i + ")").text().toLowerCase().indexOf(query[0].toLowerCase()) != -1;
        //            }
        //        });
        //    });
        //});

    </script>
</asp:Content>
