﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPurchaseAnalysis.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmPurchaseAnalysis" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 10px;
        }

        .radioboxlist label {
            color: Black;
            background-color: Silver;
            padding-left: 6px;
            padding-right: 5px;
            padding-top: 2px;
            padding-bottom: 2px;
            border: 1px solid #AAAAAA;
            white-space: nowrap;
            clear: left;
            margin-right: 5px;
            margin-left: 5px;
        }

            .radioboxlist label:hover {
                color: #CC3300;
                background: #D1CFC2;
            }

        input:checked + label {
            color: white;
            background: red;
        }

       td, th {
    padding: 3px;
}
         
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PurchaseAnalysis');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PurchaseAnalysis";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>


    <script type="text/javascript">
        //        $(document).ready(function () {
        //            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" }).datepicker("setDate", "0");
        //            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" }).datepicker("setDate", "0");
        //        });
        //        $(function () {
        //            LoadTranDiv();
        //            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" });
        //            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" });

        //            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
        //                $("#<%= txtFromDate.ClientID %>").datepicker("setDate", "0");
        //            }

        //            if ($("#<%= txtToDate.ClientID %>").val() === '') {
        //                $("#<%= txtToDate.ClientID %>").datepicker("setDate", "0");
        //            }
        //        });

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }

        };

        function pageLoad() {

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }



            $("select").chosen({ width: '100%' }); // width in px, %, em, etc 

            if ($("[id*=idPurchaseMargin]").val() == 'Y') {
                $("[id*=divRange]").show();
                $("[id*=lnkClose]").show();
                $("[id*=lnkConsolidatedPrint]").hide();
                $("[id*=divChkbox]").hide();
                $("[id*=lnkPurchaseMargin]").hide();
                $("[id*=idPurchaseMargin]").val('Y');
                //$("[id*=chkPurchaseMarg]").prop('checked', true); 
                $("[id*=lblFormName]").html('Purchase Margin');
            }
            setAutoComplete();
        }

        $(function () {
            LoadTranDiv();
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        });
        function ShowPurchaseMargin() {
            try {

                if (!$('#divRange').is(':visible')) {
                    $("[id*=divRange]").show();
                    $("[id*=lnkClose]").show();
                    $("[id*=lnkConsolidatedPrint]").hide();
                    $("[id*=divChkbox]").hide();
                    $("[id*=lnkPurchaseMargin]").hide();
                    $("[id*=idPurchaseMargin]").val('Y');
                    //$("[id*=chkPurchaseMarg]").prop('checked', true); 
                    $("[id*=lblFormName]").html('Purchase Margin');

                }
                else {
                    $("[id*=divRange]").hide();
                    $("[id*=lnkClose]").hide();
                    $("[id*=lnkConsolidatedPrint]").show();
                    $("[id*=divChkbox]").show();
                    $("[id*=idPurchaseMargin]").val('N');
                    $("[id*=lnkPurchaseMargin]").show();
                    //$("[id*=chkPurchaseMarg]").prop('checked', false);
                    $("[id*=lblFormName]").html('Purchase Analysis');
                }
                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function LoadTranDiv() {
            try {

                if ($("[id*=idPurchaseMargin]").val() == 'Y') {
                    $("[id*=divRange]").show();
                    $("[id*=lnkClose]").show();
                    $("[id*=lnkConsolidatedPrint]").hide();
                    $("[id*=divChkbox]").hide();
                    $("[id*=idPurchaseMargin]").val('Y');
                    $("[id*=lnkPurchaseMargin]").hide();
                }
                else {
                    $("[id*=divRange]").hide();
                    $("[id*=lnkClose]").hide();
                    $("[id*=lnkConsolidatedPrint]").show();
                    $("[id*=divChkbox]").show();
                    $("[id*=idPurchaseMargin]").val('N');
                    $("[id*=lblFormName]").html('Purchase Analysis');
                }
                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        function Closebutton() {
            try {
                $("[id*=divRange]").hide();
                $("[id*=lnkClose]").hide();
                $("[id*=lnkConsolidatedPrint]").show();
                $("[id*=divChkbox]").show();
                $("table[id$='grdItemDetails']").html("");
                $("[id*=idPurchaseMargin]").val('N');
                LoadTranDiv();
                $("[id*=pnlFilter]").show();
                $("[id*=lnkPurchaseMargin]").show();
                $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:74%');
                $("[id*=lblFormName]").html('Purchase Analysis');
                clearForm();

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }

        }
        function fncHideFilter() {
            try {

                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {

                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidth');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:98%');
                    $("select").trigger("liszt:updated");
                }
                else {
                    //alert("check")
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidthShow');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:74%');
                    $("select").trigger("liszt:updated");
                }

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function clearForm() {

            try {

                $('#<%= txtItemCode.ClientID %>').val('');
                $('#<%= txtItemName.ClientID %>').val('');
                $('#<%= txtMarginRange.ClientID %>').val('');
                $('#<%= txtVendor.ClientID %>').val('');
                $('#<%= txtDept.ClientID %>').val('');
                $('#<%= txtCategory.ClientID %>').val('');
                $('#<%= txtSubCategory.ClientID %>').val('');
                $('#<%= txtBrand.ClientID %>').val('');
                $('#<%= txtFloor.ClientID %>').val('');
                $('#<%= txtShelf.ClientID %>').val('');
                $('#<%= txtSection.ClientID %>').val('');
                //$("select").val(0);
                $("table[id$='grdItemDetails']").html("");
                $("select").trigger("liszt:updated");
            }
            catch (err) {

                alert(err.Message);
                return false;
            }

            return false;
        }

        function ShowPopup(name) {
            $("#divItemHistory").dialog({
                title: name,
                width: 1250,
                height: 600,
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function ShowPopupTrans(name) {
            $("#divItemHistory").dialog({
                title: name,
                width: 700,
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function setAutoComplete() {
      
            $("[id$=txtItemCode]").autocomplete({
                 source: function (request, response) {
                    var obj = {};
                    obj.searchData = request.term.replace("'", "''");
                    //obj.mode = "Package";
                    $.ajax({
                        url: '<%=ResolveUrl("~/Managements/frmPurchaseAnalysis.aspx/Fnc_ItemSerach")%>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valConvQty: item.split('|')[2]

                                }
                            }))
                        },

                        error: function (response) {
                            alert(response.message);
                        },
                        failure: function (response) {
                            alert(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    var items = i.item.value.split("_");
                    $('#<%=txtItemCode.ClientID %>').val($.trim(items));
                    event.preventDefault();
                },
                select: function (e, i) {
                    var items = i.item.label.split("_");
                    $('#<%=txtItemCode.ClientID %>').val($.trim(items[0]));
                    $('#<%=txtItemName.ClientID %>').val($.trim(items[1]));
                <%--   // $('#<%=txtItemcode.ClientID%>').select();
                 //   $('#<%=txtqty.ClientID%>').focus();--%>
                    return false;
                },
                minLength: 1

            });
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updtPnlTop" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Management</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">
                            <asp:Label ID="lblFormName" runat="server" Text="Purchase Analysis-Bulk"></asp:Label></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>

                <div class="container-group-price">
                    <%--<div class="container-date-report" id="pnlFilter" runat="server">--%>
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <div class="barcode-header">
                            Search By
                        </div>
                        <div class="control-group-single-res" style="margin-top: 10px">
                            <div class="label-left">
                                <asp:Label ID="LabelIT" runat="server" Text="Location"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtVendor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Department"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDept" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDept', 'txtCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Category"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Brand"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtSubCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Labelsc" runat="server" Text="SubCategory"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtFloor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Floor"></asp:Label>
                            </div>
                            <div class="label-right">
                              <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Labelsec" runat="server" Text="Section"></asp:Label>
                            </div>
                            <div class="label-right">
                              <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtShelf');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text="Shelf"></asp:Label>
                            </div>
                            <div class="label-right">
                             <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtVendor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text="Item Code" ></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"
                                           ></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label16" runat="server" Text="Item Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"
                                     ></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="barcode-header">
                                Filterations
                            </div>
                        </div>
                        <div class="control-group-single-res" style="margin-top: 10px">
                            <div class="label-left">
                                <asp:Label ID="Lblfrod" runat="server" Text="FromDate"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblto" runat="server" Text="ToDate"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                            </div>
                        </div>
                        <div id="divChkbox" class="control-group-single-res">
                            <asp:RadioButton ID="chkHistory" runat="server" Class="radioboxlist" Checked="true" GroupName="priceMenu"
                                Font-Bold="True" Text="History" />
                            <asp:RadioButton ID="chkNewItem" runat="server" Class="radioboxlist" GroupName="priceMenu" Font-Bold="True"
                                Text="New Items" />
                        </div>
                        <div id="divRange" class="control-group-single-res" style="display: none">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Margin Range"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtMarginRange" runat="server" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                <asp:RadioButtonList ID="rdoListMargin" runat="server" Class="radioboxlist" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Lesser" Value="Lesser" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Equal" Value="Equal"></asp:ListItem>
                                    <asp:ListItem Text="Greater" Value="Greater"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                         
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkLoadData" runat="server" class="button-red" OnClick="lnkLoadData_Click"><i class="icon-play" ></i>Load Data</asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm()"><i class="icon-play"></i>Clear</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">

                        <div class="grid-overflow" id="HideFilter_GridOverFlow" runat="server">
                            <asp:GridView ID="grdItemDetails" runat="server" Width="100%" AutoGenerateColumns="true" ShowFooter ="true"
                              CssClass="SalesPerformance"  AllowSorting="true" ShowHeaderWhenEmpty="True"  OnRowCommand="grdItemDetails_RowCommand">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                  <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings FirstPageText="First" LastPageText="Last" PageButtonCount="5" Position="Bottom" />
                                <PagerStyle Height="30px" VerticalAlign="Bottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Button" HeaderText="" Text="Invoice" CommandName="Invoice" />
                                    <%-- <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:Label ID="lbldet" runat="server" Text="Detail"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Button ID="btnTrans" BackColor="#FF3300" Font-Bold="True" ForeColor="White" CommandName="Invoicewise"
                                                    Text="Invoice" runat="server" AutoPostBack="true"   Height="100%"
                                                    Width="100%" />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                    <%--<asp:BoundField DataField="ItemCode" HeaderText="Item Code"></asp:BoundField>
                                        <asp:BoundField DataField="ShortDescription" HeaderText="Description">
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                        <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>
                                        <asp:BoundField DataField="OpeningStock" HeaderText="Opening Stock"></asp:BoundField>
                                        <asp:BoundField DataField="ReceivedQty" HeaderText="Received Stock"></asp:BoundField>
                                        <asp:BoundField DataField="IssuedQty" HeaderText="Issued Stock"></asp:BoundField>
                                        <asp:BoundField DataField="ClosingStock" HeaderText="Derived Stock"></asp:BoundField>
                                        <asp:BoundField DataField="QtyonHand" HeaderText="System Stock"></asp:BoundField>
                                        <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                        <asp:BoundField DataField="OpeningStockValue" HeaderText="Opening Value"></asp:BoundField>
                                        <asp:BoundField DataField="ReceivedQtyValue" HeaderText="Received Value"></asp:BoundField>
                                        <asp:BoundField DataField="IssuedQtyValue" HeaderText="Issued Value"></asp:BoundField>
                                        <asp:BoundField DataField="ClosingStockValue" HeaderText="Balance Value"></asp:BoundField>
                                        <asp:BoundField DataField="ResetOn" HeaderText="Last Stock Reset On"></asp:BoundField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="control-button">
                            <asp:LinkButton ID="lnkPurchaseMargin" runat="server" class="button-blue" OnClientClick="return ShowPurchaseMargin()">Purchase Margin</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="return fncHideFilter()">Hide Filter</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkConsolidatedPrint" runat="server" class="button-blue" OnClick="lnkConsolidatedPrint_Click">Consolidated Print</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" OnClick="lnkPrint_Click">Print</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-blue" OnClientClick="return Closebutton()"
                                Style="display: none"> Close</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div id="divtran" class="container-group-full">
                    <%--<div class="top-purchase-container">
                <div class="top-purchase-middle-container">
                    <div class="control-group" id="divDateFilter" style="float: left; padding: 5px 5px 0 5px;
                        width: 100%" runat="server">
                        <div style="float: right; margin-left: 25px">
                            <asp:Label ID="textName" runat="server" GroupName="tran" Text="" AutoPostBack="true"
                                Font-Bold="True" Font-Italic="False" Font-Names="Arial Black" ForeColor="Green" />
                        </div>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClose" runat="server" class="button-red"><i class="icon-play" ></i>Close</asp:LinkButton>
                    </div>
                </div>
                <div class="top-purchase-middle-container">
                </div>
            </div>--%>
                    <div id="divItemHistory" style="display: none;">
                        <asp:GridView ID="grdItemHistory" runat="server" Width="100%" AutoGenerateColumns="true"
                            AllowSorting="true" ShowHeaderWhenEmpty="True" ForeColor="#333333">
                            <Columns>
                            </Columns>
                            <AlternatingRowStyle BackColor="White" />
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <RowStyle BackColor="#FFFBD6" BorderColor="Black" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <SortedAscendingCellStyle BackColor="#FDF5AC" />
                            <SortedAscendingHeaderStyle BackColor="#4D0000" />
                            <SortedDescendingCellStyle BackColor="#FCF6C0" />
                            <SortedDescendingHeaderStyle BackColor="#820000" />
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="idPurchaseMargin" Value="N" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>