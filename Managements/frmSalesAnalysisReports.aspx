﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmSalesAnalysisReports.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmSalesAnalysisReports" %>


<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    <script type="text/javascript">
     
        function pageLoad() {
            //$('.ziehharmonika').ziehharmonika({
            //    highlander: true,
            //    collapsible: false,
            //    prefix: '★'//'♫'//,
            //    //collapseIcons: {
            //    //    opened: '☺',
            //    //    closed: '○'
            //    //}
            //});
            var ctrltxtCustomer;
            $("select").chosen({ width: '100%' });
            SetAutoComplete();

            //alert($("[id*=HidenReportType]").val());

            //if ($("[id*=HidenReportType]").val() == "1") {
            //    $('.ziehharmonika h3:eq(1)').ziehharmonika('open', true);
            //}

            //else if ($("[id*=HidenReportType]").val() == "2") {
            //    $('.ziehharmonika h3:eq(2)').ziehharmonika('open', true);
            //}

            //else if ($("[id*=HidenReportType]").val() == "0") {
            //    $('.ziehharmonika h3:eq(0)').ziehharmonika('open',true );
            //}


            //$(function () {
            //    var paneName = $("[id*=HidenReportType]").val() != "" ? $("[id*=HidenReportType]").val() : "collapseOne";
            //    //Remove the previous selected Pane.
            //    $("#accordion .in").removeClass("in");

            //    //Set the selected Pane.
            //    $("#" + paneName).collapse("show");

            //    //When Pane is clicked, save the ID to the Hidden Field.
            //    $(".panel-heading a").click(function () {
            //        $("[id*=PaneName]").val($(this).attr("href").replace("#", ""));
            //    });
            //});

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtFromDateMember.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDateMember.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtFromDatePromotion.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDatePromotion.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

           

            var checkedvalue = $("#<%=rdoSalesAnalysis.ClientID%>").find(":checked").val();

            if (checkedvalue === 'Billcount') {

                $("[id*=divSummDetail]").show();
            }
            else {

                $("[id*=divSummDetail]").hide();
            }

           

            $('#<%=chkBydate.ClientID%>').change(function () {
                if (this.checked) { 
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                     
                }
            });

            $('#<%=chkBydatePromotion.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtFromDatePromotion]").removeAttr("disabled");
                    $("[id*=txtToDatePromotion]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDatePromotion]").attr("disabled", "disabled");
                    $("[id*=txtToDatePromotion]").attr("disabled", "disabled");
                }
            });

            $('#<%=chkBydateMember.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtFromDateMember]").removeAttr("disabled");
                    $("[id*=txtToDateMember]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDateMember]").attr("disabled", "disabled");
                    $("[id*=txtToDateMember]").attr("disabled", "disabled");
                }
            });

            
            var Bydatevalue = $("#<%=chkBydate.ClientID%>").is(':checked'); 
            var BydatevaluePromotion = $("#<%=chkBydatePromotion.ClientID%>").is(':checked'); 
            var BydatevalueMember = $("#<%=chkBydateMember.ClientID%>").is(':checked');             

            SetDateDisable(Bydatevalue, $("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"))
            SetDateDisable(BydatevaluePromotion, $("#<%= txtFromDatePromotion.ClientID %>"), $("#<%= txtToDatePromotion.ClientID %>"))
            SetDateDisable(BydatevalueMember, $("#<%= txtFromDateMember.ClientID %>"), $("#<%= txtToDateMember.ClientID %>"))          
             

        }

        function SetAutoComplete() {

            ctrltxtCustomer = $("#<%= SalesReportFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.ReportFilterUserControl.FilterControls.CustomerTextBox).ClientID %>");
            ctrltxtCustomer.autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Managements/frmCreditInvoice.aspx/GetMemberDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valCode: item.split('|')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    ctrltxtCustomer.val(i.item.valCode);
                    //$("#HiddenMemberCode").val($.trim(i.item.valCode));
                    return false;
                },
                minLength: 2
            });
        }

        $(function () {
            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"))
            SetDefaultDate($("#<%= txtFromDatePromotion.ClientID %>"), $("#<%= txtToDatePromotion.ClientID %>"))
            SetDefaultDate($("#<%= txtFromDateMember.ClientID %>"), $("#<%= txtToDateMember.ClientID %>"))

            $('#<%= rdoListSalesAnalysMember.ClientID %> input').click(function () {
                var checkvalue = $("#<%= rdoListSalesAnalysMember.ClientID %> input:radio:checked").val();

                if (checkvalue == 'Credit') {
                    $("[id*=txtFromDateMember]").show();
                    $("[id*=txtToDateMember]").hide(); 
                }
                else if (checkvalue == 'Outstanding') {
                    $("[id*=GroupByCR]").show();
                    $("[id*=GroupByCRDue]").hide();
                }
                else if (checkvalue == 'Receipt') {
                    $("[id*=GroupByCR]").show();
                    $("[id*=GroupByCRDue]").hide();
                }
                else {
                    $("[id*=txtFromDateMember]").show();
                    $("[id*=txtToDateMember]").show();
                }
            });

           <%-- $('#<%=chkBydate.ClientID%>').change(function () { 
                if (this.checked) {
                    alert('chkBydate');
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                     
                }
            });

            $('#<%=chkBydatePromotion.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtFromDatePromotion]").removeAttr("disabled");
                    $("[id*=txtToDatePromotion]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDatePromotion]").attr("disabled", "disabled");
                    $("[id*=txtToDatePromotion]").attr("disabled", "disabled");
                }
            });

            $('#<%=chkBydateMember.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtFromDateMember]").removeAttr("disabled");
                    $("[id*=txtToDateMember]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDateMember]").attr("disabled", "disabled");
                    $("[id*=txtToDateMember]").attr("disabled", "disabled");
                }
            });--%>
        });

        function SetDateDisable(check, FromDate, Todate)
        { 
            if (!check)
            {
                FromDate.attr("disabled", "disabled");
                Todate.attr("disabled", "disabled");
            }
        }
        function SetDefaultDate(FromDate, Todate) {

            FromDate.datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            Todate.datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        }


        function ValidateForm() {
            var Show = '';
            if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose To date';
                $("#<%= txtFromDate.ClientID %>").focus();
            }

            if ($("#<%= txtToDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose from date';
                $("#<%= txtToDate.ClientID %>").focus();
            }

            if (Show != '') {
                alert(Show);
                //JSalerts(Show);
                return false;
            }

            else {
                var StartDate = $("#<%= txtFromDate.ClientID %>").val();
                var EndDate = $("#<%= txtToDate.ClientID %>").val();
                var eDate = new Date(EndDate);
                var sDate = new Date(StartDate);
                if (StartDate != '' && EndDate != '' && sDate > eDate) {
                    alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                    //JSalerts("Please ensure that the End Date is greater than or equal to the Start Date.");
                    return false;
                }
                return true;
            }
        }

        function Clearall() {

            $("select").val(0);
            $("select").trigger("liszt:updated");
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");

            return false;
        }



    </script>
    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updtPnlTop" runat="Server">
        <ContentTemplate>
            <div class="container">
                <h4></h4>
                <div class="ziehharmonika">
                    
                    <div style="overflow: visible; display: block;" runat="server" id="DivSalesAnalysis">
                        <h3>Sales Analysis</h3>
                        <div class="row">
                            <div class="col-md-3" style="margin-left: 20px">
                                <div class="small-box bg-aqua">
                                    <asp:RadioButtonList ID="rdoSalesAnalysis" runat="server" Class="radioboxlist">
                                        <asp:ListItem Selected="True" Value="Loc">Location wise Profit</asp:ListItem>
                                        <asp:ListItem Value="vendor">Vendor wise Profit</asp:ListItem>
                                        <asp:ListItem Value="dept">Department wise Profit</asp:ListItem>
                                        <asp:ListItem Value="category">Category wise Profit</asp:ListItem>
                                        <asp:ListItem Value="Brand">Brand wise Profit</asp:ListItem>
                                        <asp:ListItem Value="class">Class wise Profit</asp:ListItem>
                                        <asp:ListItem Value="Billcount">Location wise Bill Count Group</asp:ListItem>
                                        <asp:ListItem Value="soldQty">Location wise Sold Quantity</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>

                            <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                                <div class="barcode-header">
                                    Date Filter
                                </div>
                                <div class="small-box bg-aqua">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <div class="col-md-6 form_bootstyle">
                                                <asp:Label ID="Label7" runat="server" Text="From Date"></asp:Label>
                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                            </div>
                                            <div class="col-md-6 form_bootstyle">
                                                <asp:Label ID="Label8" runat="server" Text="To Date"></asp:Label>
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="margin-top: 15px">
                                            <div class="col-md-3 form_bootstyle">
                                                <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                            </div>
                                            <div class="col-md-4">
                                                <%--<asp:LinkButton ID="LinkButton2" runat="server" class="btn btn-primary"
                                                    Font-Bold="true" OnClientClick="return  Clearall()">Clear</asp:LinkButton>--%>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:LinkButton ID="lnkLoadSales" runat="server" class="btn btn-danger" Font-Bold="true" OnClientClick="return  ValidateForm()" OnClick="lnkLoadSales_Click">Load Report
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="divSummDetail" class="col-md-12 form_bootstyle_border" style="margin-top: 8px; width: 60%">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:RadioButton ID="rdoSummary" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="Summary"
                                            Checked="true" />
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:RadioButton ID="rdoDetail" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="Detail" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 form_bootstyle_border">
                                <div class="report-header-filter">
                                    Filtrations
                                </div>
                                <div class="small-box bg-aqua">
                                    <ups:ReportFilterUserControl runat="server" ID="SalesReportFilterUserControl"
                                        EnableVendorDropDown="true"
                                        EnablelocationDropDown="true"
                                        EnableDepartmentDropDown="true"
                                        EnableCategoryDropDown="true"
                                        EnableBrandDropDown="true"
                                        EnableClassDropDown="true" />
                                </div>
                            </div>

                        </div>
                    </div>


                    <div style="overflow: visible; display: block;" runat="server" id="DivPromotionEvents">
                        <h3>Sales Analysis Promotion Events</h3>
                        <div class="row">
                            <div class="col-md-3" style="margin-left: 20px">
                                <div class="small-box bg-aqua">
                                    <asp:RadioButtonList ID="rdoListPromotionEvents" runat="server" Class="radioboxlist">
                                        <asp:ListItem Selected="True">Inventory wise Sales</asp:ListItem>
                                        <asp:ListItem>Department wise Sales</asp:ListItem>
                                        <asp:ListItem>Category wise Sales</asp:ListItem>
                                        <asp:ListItem>Brand wise Sales</asp:ListItem>
                                        <asp:ListItem>Free Item Sales</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                                <div class="barcode-header">
                                    Date Filter
                                </div>
                                <div class="small-box bg-aqua">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <div class="col-md-6 form_bootstyle">
                                                <asp:Label ID="Label10" runat="server" Text="From Date"></asp:Label>
                                                <asp:TextBox ID="txtFromDatePromotion" runat="server" CssClass="form_textbox"></asp:TextBox>
                                            </div>
                                            <div class="col-md-6 form_bootstyle">
                                                <asp:Label ID="Label11" runat="server" Text="To Date"></asp:Label>
                                                <asp:TextBox ID="txtToDatePromotion" runat="server" CssClass="form_textbox"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="margin-top: 15px;">
                                            <div class="col-md-3 form_bootstyle">
                                                <asp:CheckBox ID="chkBydatePromotion" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                            </div>
                                            <div class="col-md-4">
                                              <%--  <asp:LinkButton ID="LinkButton1" runat="server" class="btn btn-primary"
                                                    Font-Bold="true" OnClientClick="return  Clearall()">Clear</asp:LinkButton>--%>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:LinkButton ID="lnkLoadPromotion" runat="server" class="btn btn-danger" Font-Bold="true" OnClientClick="return  ValidateForm()" OnClick="lnkLoadPromotion_Click">Load Report
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form_bootstyle_border" style="margin-top: 8px;">
                                    <div class="col-md-12 form_bootstyle">
                                        <asp:RadioButtonList ID="rdoListPromoType" runat="server" Class="radioboxlist">
                                            <asp:ListItem Selected="True">Promotions</asp:ListItem>
                                            <asp:ListItem>Sub Level</asp:ListItem>
                                            <asp:ListItem>Events</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 form_bootstyle_border">
                                <div class="report-header-filter">
                                    Filtrations
                                </div>
                                <div class="small-box bg-aqua">
                                    <ups:ReportFilterUserControl runat="server" ID="PromotionReportFilterUserControl"
                                        EnablelocationDropDown="true"
                                        EnableVendorDropDown="true"
                                        EnableDepartmentDropDown="true"
                                        EnableCategoryDropDown="true"
                                        EnableBrandDropDown="true" />
                                </div>
                            </div>
                        </div>
                    </div>


                    <div style="overflow: visible; display: block;" runat="server" id="DivMemberReport">
                        <h3>Sales Analysis Member Report</h3>
                        <div class="row">
                            <div class="col-md-3" style="margin-left: 20px">
                                <div class="small-box bg-aqua">
                                    <asp:RadioButtonList ID="rdoListSalesAnalysMember" runat="server" Class="radioboxlist">
                                        <asp:ListItem Selected="True" Value="Details">Member Details</asp:ListItem>
                                        <asp:ListItem Value="JoinDate">Member Details By Join Date</asp:ListItem>
                                        <asp:ListItem Value="Point">Member List with Point</asp:ListItem>
                                        <asp:ListItem Value="Transaction">Member Transaction</asp:ListItem>
                                        <asp:ListItem Value="Adjustment">Member Point Adjustment</asp:ListItem>
                                        <asp:ListItem Value="Age">Age Group</asp:ListItem>
                                        <asp:ListItem Value="Income">Income Group</asp:ListItem>
                                        <%--<asp:ListItem>Member List by Age Group</asp:ListItem>
                                        <asp:ListItem>Member List by Income</asp:ListItem>
                                        <asp:ListItem Value="IBC">IBC Member Trans</asp:ListItem> 
                                        <asp:ListItem>Member Age wise Sales</asp:ListItem>--%>
                                        <asp:ListItem Value="Outstanding">Member Outstanding</asp:ListItem>
                                        <asp:ListItem Value="Issued">Gift Issued</asp:ListItem>
                                        <asp:ListItem Value="Unclaimed">Unclaimed Gift </asp:ListItem>
                                        <asp:ListItem Value="Daywise">Daywise Sales</asp:ListItem>
                                        <asp:ListItem Value="Credit">Credit Sales</asp:ListItem>
                                        <%--<asp:ListItem Value="Hourly">Hourly Sales</asp:ListItem>--%>
                                        <asp:ListItem Value="Purchase">Avg.Purchase/Receipt</asp:ListItem>                                        
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                                <div class="barcode-header">
                                    Date Filter
                                </div>
                                <div class="small-box bg-aqua">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <div class="col-md-6 form_bootstyle">
                                                <asp:Label ID="Label19" runat="server" Text="From Date"></asp:Label>
                                                <asp:TextBox ID="txtFromDateMember" runat="server" CssClass="form_textbox"></asp:TextBox>
                                            </div>
                                            <div class="col-md-6 form_bootstyle">
                                                <asp:Label ID="Label20" runat="server" Text="To Date"></asp:Label>
                                                <asp:TextBox ID="txtToDateMember" runat="server" CssClass="form_textbox"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-12"  style="margin-top: 15px;">
                                            <div class="col-md-3 form_bootstyle">
                                                <asp:CheckBox ID="chkBydateMember" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                            </div>
                                            <div class="col-md-4">
                                               <%--<asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-primary"
                                                    Font-Bold="true" OnClientClick="return  Clearall()">Clear</asp:LinkButton>--%>
                                            </div>
                                            <div class="col-md-3">
                                                 <asp:LinkButton ID="lnkLoadMember" runat="server" class="btn btn-danger" Font-Bold="true" OnClientClick="return  ValidateForm()" OnClick="lnkLoadMember_Click">Load Report
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form_bootstyle_border" style="margin-top: 8px;">
                                    <div class="col-md-12 form_bootstyle">
                                        <asp:RadioButtonList ID="RadioButtonListActive" runat="server" Class="radioboxlist">
                                            <asp:ListItem Selected="True">Active</asp:ListItem>
                                            <asp:ListItem>InActive</asp:ListItem>
                                            <asp:ListItem>All</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 form_bootstyle_border">
                                <div class="report-header-filter">
                                    Filtrations
                                </div>
                                <div class="small-box bg-aqua">
                                    <ups:ReportFilterUserControl runat="server" ID="MemberReportFilterUserControl"
                                        EnableCustomerTextbox="true"
                                        EnableMemberTypeDropDown="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="HidenReportType" runat="server" Value="0" />
                <asp:HiddenField ID="HiddenMemberCode" runat="server" Value="0" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
