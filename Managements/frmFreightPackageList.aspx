﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmFreightPackageList.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmFreightPackageList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
          
    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
     </style>

    <script type="text/javascript">
        function frg_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Department");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hidDelValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));

                          $("#<%= btnDelete.ClientID %>").click();
                      },
                      "NO": function () {
                          $(this).dialog("close");
                          returnfalse();
                      }
                  }
              });
        }
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'FreightPackageList');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "FreightPackageList";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Freight Charges View</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <div class="gridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px"
                                OnClick="lnkSearchGrid_Click">Search</asp:LinkButton>
                            <asp:LinkButton ID="lnkBack" runat="server" class="button-blue" Width="100px"
                                PostBackUrl="~/Managements/frmFreightPackingCharges.aspx">New</asp:LinkButton>
                        </asp:Panel>
                    </div>

                    <asp:GridView ID="gvFrightList" runat="server" Width="100%" AutoGenerateColumns="False" style ="margin-top: 30px;"
                        ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" oncopy="return false" AllowPaging="True"
                        PageSize="18" OnPageIndexChanging="gvFrightList_PageIndexChanging" DataKeyNames="PackageCode">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                        <PagerStyle CssClass="pshro_text" />
                        <Columns>
                            <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                        ToolTip="Click here to edit" OnClick="imgbtnEdit_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                        ToolTip="Click here to Delete" OnClientClick="return frg_Delete(this);  return false;" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="PackageCode" HeaderText="Package Code"></asp:BoundField>
                            <asp:BoundField DataField="TransportCode" HeaderText="Transpor tCode"></asp:BoundField>
                            <asp:BoundField DataField="TransporName" HeaderText="Transport Name" />
                            <asp:BoundField DataField="CreateDate" HeaderText="Create Date"></asp:BoundField>
                            <asp:BoundField DataField="CreateUser" HeaderText="Create User"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <div class="hiddencol">
            <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
            <asp:HiddenField ID="hidDelValue" runat="server" />
        </div>
    </div>
</asp:Content>
