﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmCategoryAttributeNew.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmCategoryAttributeNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/CategoryAttribute.css" rel="stylesheet" />

    <script type="text/javascript"> 
        var ddlInputIGST, ddlOutputIGST, ddlInputSGST, ddlInputCGST, ddlOutputSGST, ddlOutputCGST, txtInputSGSTPrc, txtInputSGSTAmt, txtInputCGSTPrc,
            txtInputCGSTAmt, txtOutputSGSTPrc, txtOutputCGSTPrc, txtOutputIGSTPrc, txtInputIGSTPrc;
        function pageLoad() {
            $(".chzn-container").css('display', 'none');
            $(".form-control-res").css('height', '25px');
            $(".form-control-res").css('display', 'block');
            fncSetAttribute();
            ddlInputIGST = $('#<%=ddlPurchaseTax.ClientID%>');
            ddlOutputIGST = $('#<%=ddlSalesTax.ClientID%>');
            ddlInputSGST = $('#<%=ddlInputSGST.ClientID %>');
            ddlInputCGST = $('#<%=ddlInputCGST.ClientID %>');
            ddlOutputSGST = $('#<%=ddlOutputSGST.ClientID %>');
            ddlOutputCGST = $('#<%=ddlOutputCGST.ClientID %>');

            txtInputSGSTPrc = $('#<%=hidItaxPer1.ClientID %>');
            txtInputCGSTPrc = $('#<%=hidItaxPer2.ClientID %>');

            txtOutputSGSTPrc = $('#<%=hidOtaxPer1.ClientID %>');
            txtOutputCGSTPrc = $('#<%=hidOtaxPer2.ClientID %>');

            txtInputIGSTPrc = $('#<%=hidItaxPer3.ClientID %>');
            txtOutputIGSTPrc = $('#<%=hidOtaxPer3.ClientID %>');

            ddlInputIGST.change(function () {
                fncInputGSTValueChange($(this).val(), 'Pur');
            });

            ddlOutputIGST.change(function () {
                fncInputGSTValueChange($(this).val(), 'Sal');
            });
        }
        function fncSetAttribute() {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("frmCategoryAttributeNew.aspx/fncGetAttribute")%>',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    fncItemBindtoTable(msg);
                },
                error: function (data) {
                    ShowPopupMessageBox(data.message);
                }
            });
        }
        function fncItemBindtoTable(msg) {
            try {
                var objAttributeData = jQuery.parseJSON(msg.d);
                var Rowno = 0;
                var tblAttribute = $("#tblAttribute tbody");
                tblAttribute.children().remove();

                if (objAttributeData.length > 0) {

                    for (var i = 0; i < objAttributeData.length; i++) {
                        Rowno = parseFloat(i) + parseFloat(1);
                        var row = "<tr id='trAttribute_" + i + "' tabindex='" + i + "' >" +
                            "<td id='tdCode_" + i + "' >" + objAttributeData[i]["AttributeCode"] + "</td>" +
                            "<td id='tdItemName_" + i + "' > " + objAttributeData[i]["AttributeName"] + "</td>" +
                            "<td >  <input id ='tdMandatory_" + i + "'  type='checkbox'/></td>" +
                            "<td ><input id ='chkShow_" + i + "'   type='checkbox'/></td>"
                        "</tr>";
                        tblAttribute.append(row);
                    }

                    if ($('#<%=hidGetCategory.ClientID %>').val() != "")
                        fncLoadAttribute()
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncMandataryandShow(source, mode) {
            try {
                if (mode == "Man") {
                    if ($(source).is(":checked")) {
                        $("#tblAttribute tbody").children().each(function () {
                            $(this).find('td input[id*="tdMandatory"]').attr("checked", "checked");
                        });
                    }
                    else {
                        $("#tblAttribute tbody").children().each(function () {
                            $(this).find('td input[id*="tdMandatory"]').removeAttr("checked");
                        });
                    }
                }
                else {
                    if ($(source).is(":checked")) {
                        $("#tblAttribute tbody").children().each(function () {
                            $(this).find('td input[id*="chkShow"]').attr("checked", "checked");
                        });
                    }
                    else {
                        $("#tblAttribute tbody").children().each(function () {
                            $(this).find('td input[id*="chkShow"]').removeAttr("checked");
                        });
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function disableFunctionKeys(e) {
            try {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 115) {
                        if ($('#<%=lnkSave.ClientID %>').is(":visible")) {
                            if (fncSave() == true)
                                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                            else
                                return false;
                        }
                        e.preventDefault();
                    }
                    else if (e.keyCode == 117) {
                        fncClear();
                    }
                    else {
                        e.preventDefault();
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncClear() {
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        location.reload();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        return false();
                    }
                }
            });
        }

        function fncSave() {
            try {
                var Show = '';
                if ($('#<%=txtName.ClientID%>').val() == "") {
                    Show = Show + 'Please Enter Name.';
                }
                if ($('#<%=txtHsnCode.ClientID%>').val() == "") {
                    Show = Show + '<br />Please Enter HSN Code.';
                }
                if ($('#<%=ddlSalesTax.ClientID%>').val() == "") {
                    Show = Show + '<br />Please Enter Sales Tax.';
                }
                var HSNCode_len = $('#<%=txtHsnCode.ClientID%>').val().length;
                if (HSNCode_len != 0 && HSNCode_len != 2 && HSNCode_len != 4 && HSNCode_len != 8) {
                    Show = Show + '<br />Enter Valid HSN Code(2 or 4 or 8 Digits Length or Empty)';
                }
                var Count = 0
                $("#tblAttribute tbody").children().each(function () {
                    if ($(this).find('td input[id*="chkShow"]').is(":checked") || $(this).find('td input[id*="tdMandatory"]').is(":checked")) {
                        Count = parseFloat(Count) + 1;
                    }
                });
                if (parseFloat(Count) == 0) {
                    Show = Show + '<br />Please Choose Atleaset One Attribute.';
                }

                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }
                else {
                    if (fncSaveAttributeDetail() == false) {
                        ShowPopupMessageBox("Please Choose Attribute");
                        return false;
                    }
                    return true;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSaveAttributeDetail() {
            try {
                var xml = '<NewDataSet>';
                $("#tblAttribute tbody").children().each(function () {
                    if ($(this).find('td input[id*="chkShow"]').is(":checked") || $(this).find('td input[id*="tdMandatory"]').is(":checked")) {
                        var chkShow = '0', chkMandatory = '0';
                        if ($(this).find('td input[id*="chkShow"]').is(":checked")) {
                            chkShow = '1';
                        }
                        if ($(this).find('td input[id*="tdMandatory"]').is(":checked")) {
                            chkMandatory = '1';
                        }
                        xml += "<Table>";
                        xml += "<AttrCode>" + $(this).find('td').eq(0).html() + "</AttrCode>";
                        xml += "<Mandatory>" + chkMandatory + "</Mandatory>";
                        xml += "<Show>" + chkShow + "</Show>";
                        xml += "</Table>";
                    }
                });

                xml = xml + '</NewDataSet>'
                if (xml == '<NewDataSet></NewDataSet>') {
                    return false;
                }
                xml = escape(xml);
                $('#<%=hidAttributeDetail.ClientID %>').val(xml);
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        /// Input GST Value Changed
        function fncInputGSTValueChange(val, mode) {
            try {
                var InputIGSTCode = val;
                var GSTObj, GSTType;
                if (mode == "Pur") {
                    ddlInputIGST.val(val);
                    ddlInputIGST.trigger("liszt:updated");

                    var array = ddlInputIGST.find("option:selected").text().trim().split("-");
                    var InputIGSTPrc = array[1];
                    if (InputIGSTPrc != '' && InputIGSTPrc != '-- Select --') {
                        txtInputIGSTPrc.val(InputIGSTPrc);
                    }
                }

                if (mode == "Sal") {
                    ddlOutputIGST.val(val);
                    ddlOutputIGST.trigger("liszt:updated");

                    var array = ddlOutputIGST.find("option:selected").text().trim().split("-");
                    var OutputGSTPrc = array[1];
                    if (OutputGSTPrc != '' && OutputGSTPrc != '-- Select --') {
                        txtOutputIGSTPrc.val(OutputGSTPrc);
                    }
                }

                if (InputIGSTCode == "EIGST")
                    GSTType = "EIGST";
                else
                    GSTType = "IGST";

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/GetGstDetail") %>',
                    data: "{'GstCode':'" + InputIGSTCode + "', 'GstType':'" + GSTType + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        GSTObj = jQuery.parseJSON(msg.d);

                        if (GSTObj.length > 0) {
                            for (var i = 0; i < GSTObj.length; i++) {
                                if (GSTObj[i]["TaxCode"].indexOf("S") != -1 && mode == "Pur") {

                                    //=============================>  Input SGST Change
                                    ddlInputSGST.val(GSTObj[i]["TaxCode"])
                                    ddlInputSGST.trigger("liszt:updated");

                                    var array = ddlInputSGST.find("option:selected").text().trim().split("-");
                                    var InputSGSTPrc = array[1];
                                    if (InputSGSTPrc != '' && InputSGSTPrc != '-- Select --') {
                                        txtInputSGSTPrc.val(InputSGSTPrc);
                                    }
                                }
                                else if (GSTObj[i]["TaxCode"].indexOf("C") != -1 && mode == "Pur") {

                                    //=============================>  Input CGST Change
                                    ddlInputCGST.val(GSTObj[i]["TaxCode"])
                                    ddlInputCGST.trigger("liszt:updated");

                                    var array = ddlInputCGST.find("option:selected").text().trim().split("-");
                                    var InputCGSTPrc = array[1];
                                    if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                        txtInputCGSTPrc.val(InputCGSTPrc);
                                    }
                                }
                                else if (GSTObj[i]["TaxCode"].indexOf("S") != -1 && mode == "Sal") {

                                    //=============================>  Input CGST Change
                                    ddlOutputSGST.val(GSTObj[i]["TaxCode"])
                                    ddlOutputSGST.trigger("liszt:updated");

                                    var array = ddlOutputSGST.find("option:selected").text().trim().split("-");
                                    var InputCGSTPrc = array[1];
                                    if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                        txtOutputSGSTPrc.val(InputCGSTPrc);
                                    }
                                }
                                else if (GSTObj[i]["TaxCode"].indexOf("C") != -1 && mode == "Sal") {

                                    //=============================>  Input CGST Change
                                    ddlOutputCGST.val(GSTObj[i]["TaxCode"])
                                    ddlOutputCGST.trigger("liszt:updated");

                                    var array = ddlOutputCGST.find("option:selected").text().trim().split("-");
                                    var InputCGSTPrc = array[1];
                                    if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                        txtOutputCGSTPrc.val(InputCGSTPrc);
                                    }
                                }
                            }
                        }

                    },
                    error: function (data) {
                        ShowPopupMessageBox('Something Went Wrong')
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err);
            }
        }

        function fncShowSuccess() {
            $("#dialog-Save").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        window.location = window.location.pathname;
                        $(this).dialog("close");
                        
                        return false();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        return false();
                    }
                }
            });
        }

        function fncLoadAttribute() {
            var allObj, obj;
            allObj = jQuery.parseJSON($('#<%=hidGetCategory.ClientID %>').val());
            obj = allObj.Table1;
            for (var i = 0; i < obj.length; i++) {
                var CategoryGroupCode = obj[i]["AttrributeId"];
                var Mandatory = obj[i]["Mandatory"];
                var Show = obj[i]["Show"];
                $("#tblAttribute tbody").children().each(function () {
                    if ($(this).find('td').eq(0).html() == CategoryGroupCode) {
                        if (Show == "1")
                            $(this).find('td input[id*="chkShow"]').attr("checked", "checked");
                        if (Mandatory == "1")
                            $(this).find('td input[id*="tdMandatory"]').attr("checked", "checked");
                    }
                });
            } 
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a>Masters</a> <i class="fa fa-angle-right"></i></li>
                 <li><a href="../Managements/frmCategoryView.aspx">Category Attribute View</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Category Attribute</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>Do You Want Clear ?</p>
        </div>
        <div id="dialog-Save" style="display: none;" title="Enterpriser Web">
            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>Saved Successfully. Do You Want Clear ?</p>
        </div>
        <div class="EnableScroll">
            <div class="col-md-8">
                <div class="container-group-small" style="width: 100%;background-color: white;">
                    <div class="whole-price-header">
                        Product Details
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblProductGroup" runat="server" Text="Product Group"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlProductType" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                         <div class="col-md-3">
                            <asp:Label ID="Label2" runat="server" Text="Brand"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlBarand" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblType" runat="server" Text="Type"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblCode" runat="server" Text="Code"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="col-md-9">
                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblHSN" runat="server" Text="HSN Code"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="col-md-9">
                            <asp:TextBox ID="txtHsnCode" runat="server" CssClass="form-control-res" MaxLength="8"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblSalestax" runat="server" Text="Sales tax"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlSalesTax" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblPurchaseTax" runat="server" Text="Purchase Tax"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlPurchaseTax" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblBarcodeMode" runat="server" Text="BarCode Mode"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlBarCodeMode" runat="server" CssClass="form-control-res">
                                <asp:ListItem Value="--- Select ---" Text="--- Select ---"></asp:ListItem>
                                <asp:ListItem Value ="Running">Running Order</asp:ListItem>
                                <asp:ListItem Value ="Seperate">Seperate</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblSelMode" runat="server" Text="Selling Mode"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlSellingMode" runat="server" CssClass="form-control-res">
                                 <asp:ListItem Value="--- Select ---" Text="--- Select ---"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblMArgin" runat="server" Text="Margin"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblMin" runat="server" Text="Min"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:TextBox ID="txtMin" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblMax" runat="server" Text="Max"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtMax" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblDis" runat="server" Text="DisCount Mode"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlDisCount" runat="server" CssClass="form-control-res">
                                 <asp:ListItem Value="--- Select ---" Text="--- Select ---"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblValue" runat="server" Text="Value"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtValue" runat="server" CssClass="form-control-res text-right">0</asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblBarCodePrice" runat="server" Text="BarCode Source"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlbarCodeSource" runat="server" CssClass="form-control-res">
                                 <asp:ListItem Value="--- Select ---" Text="--- Select ---"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblAgeSet" runat="server" Text="Age Set"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlAgSet" runat="server" CssClass="form-control-res">
                                <asp:ListItem Value="--- Select ---" Text="--- Select ---"></asp:ListItem>
                                <asp:ListItem Value="0-5" Text="0-5"></asp:ListItem>
                                <asp:ListItem Value="5-10" Text="5-10"></asp:ListItem>
                                <asp:ListItem Value="10-15" Text="15-20"></asp:ListItem>
                                <asp:ListItem Value="20-25" Text="20-25"></asp:ListItem>
                                <asp:ListItem Value="25-30" Text="25-30"></asp:ListItem>
                                <asp:ListItem Value="30-35" Text="30-35"></asp:ListItem>
                                <asp:ListItem Value="35-45" Text="35-45"></asp:ListItem>
                                <asp:ListItem Value="45" Text="45+"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblStoCkHandling" runat="server" Text="Stok Holding Periods(days)"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtPeriodDays" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblIsCore" runat="server" Text="Is Core"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:CheckBox ID="chkIsCore" runat="server" Class="radioboxlist" Font-Bold="True" />
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblPrPlan" runat="server" Text="Purchase Plan Mode"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlPrPlanMode" runat="server" CssClass="form-control-res">
                                <asp:ListItem Value="--- Select ---" Text="--- Select ---"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblExclude" runat="server" Text="Exclude Reward"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:CheckBox ID="chkExclude" runat="server" Class="radioboxlist" Font-Bold="True" />
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblExpGen" runat="server" Text="Expected Gender"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control-res">
                                     <asp:ListItem Value="--- Select ---" Text="--- Select ---"></asp:ListItem>
                                <asp:ListItem Value="Male" Text="MALE"></asp:ListItem>
                                <asp:ListItem Value="Female" Text="FEMALE"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblDailyPrice" runat="server" Text="Daily Price"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:CheckBox ID="chkDailyPrice" runat="server" Class="radioboxlist" Font-Bold="True" />
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblFreightCharge" runat="server" Text="Total Freight Charge"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtFreightCharge" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblActive" runat="server" Text="Active"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:CheckBox ID="chkActive" runat="server" Checked="true" Class="radioboxlist" Font-Bold="True" />
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblSection" runat="server" Text="Section"></asp:Label>
                        </div>
                        <div class="col-md-9">
                            <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
                        </div>
                        <div class="col-md-9">
                            <asp:DropDownList ID="ddlComapany" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <div class="col-md-3">
                            <asp:Label ID="Label1" runat="server" Text="Floor"></asp:Label>
                        </div>
                        <div class="col-md-9">
                            <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="container-group-small" style="width: 100%; margin-top: 10px; background-color: white;">
                    <div class="col-md-12">
                        <asp:Label ID="lblPrAttribute" runat="server" Style="font-size: 13px; font-weight: 700;" Text="Product Entry Attributes(Configure)"></asp:Label>
                        <%--<div class="col-md-6 display_none"> 
                            <asp:CheckBox ID="chkPrAttribute" runat="server" Class="radioboxlist" Font-Bold="True" style="float: right"/>
                        </div>--%>
                    </div>
                    <div class="Payment_fixed_headers grdLoad_Details Cat_Margin">
                        <table id="tblAttribute" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">Attribute Code
                                    </th>
                                    <th scope="col">Name
                                    </th>
                                    <th scope="col">Mandatory 
                                    <asp:CheckBox ID="chkMandatory" onclick="fncMandataryandShow(this,'Man')" runat="server" />
                                    </th>
                                    <th scope="col">Show
                                    <asp:CheckBox ID="chkShow" onclick="fncMandataryandShow(this,'Show')" runat="server" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody style="height: 425px; overflow-y: scroll; background-color: white;">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="button-contol">
                        <div class="control-button display_none">
                            <asp:LinkButton ID="lnkSNew" runat="server" class="button-red"
                                OnClientClick="fncAdd();return false;"><i class="icon-play"></i>New(F7)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return fncSave();" OnClick="lnkSave_Click">
                            <i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="fncClear();return false;">
                            <i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                         <div class="control-button">
                            <asp:LinkButton ID="lnkView" runat="server" class="button-blue" PostBackUrl="~/Managements/frmCategoryView.aspx">
                            <i class="icon-play"></i>View</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hidAttributeDetail" runat="server" Value="" />
            <asp:HiddenField ID="hidItaxPer1" runat="server" Value="" />
            <asp:HiddenField ID="hidItaxPer2" runat="server" Value="" />
            <asp:HiddenField ID="hidItaxPer3" runat="server" Value="" />
            <asp:HiddenField ID="hidOtaxPer1" runat="server" Value="" />
            <asp:HiddenField ID="hidOtaxPer2" runat="server" Value="" />
            <asp:HiddenField ID="hidOtaxPer3" runat="server" Value="" />
            <asp:HiddenField ID="hidItaxAmt1" runat="server" Value="" />
            <asp:HiddenField ID="hidItaxAmt2" runat="server" Value="" />
            <asp:HiddenField ID="hidICode3" runat="server" Value="" />
            <asp:HiddenField ID="hidOCode1" runat="server" Value="" />
            <asp:HiddenField ID="hidOCode2" runat="server" Value="" />
            <asp:HiddenField ID="hidOCode3" runat="server" Value="" />
            <asp:HiddenField ID="hidGetCategory" runat="server" Value="" />
            <div class="display_none">
                <asp:DropDownList ID="ddlOutputSGST" runat="server" CssClass="form-control-res">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlOutputCGST" runat="server" CssClass="form-control-res">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlInputSGST" runat="server" CssClass="form-control-res">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlInputCGST" runat="server" CssClass="form-control-res">
                </asp:DropDownList>
            </div>
        </div>
    </div>
</asp:Content>
