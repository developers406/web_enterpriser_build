﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmFreightPackingCharges.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmFreightPackingCharges" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 140px;
            max-width: 140px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 140px;
            max-width: 140px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 140px;
            max-width: 140px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 110px;
            max-width: 110px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 140px;
            max-width: 140px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 110px;
            max-width: 110px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            display: none;
        }
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
          

    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        if (fncSave() == true)
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    fncClear();
                    e.preventDefault();
                }
            }
        };
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function pageLoad() {
            setAutocomplete();
            Decimal();
            $(<%=txtWagesPlace.ClientID%>).val('GF');
            $('#<%=txtFromStation.ClientID %>').attr('disabled', 'disabled');
            $('#<%=txtToStation.ClientID %>').attr('disabled', 'disabled');
            $('#<%=txtTransport.ClientID %>').focusout(function () {
                if ($('#<%=txtTransport.ClientID %>').val() != "") {
                    $('#<%=txtTransport.ClientID %>').attr('disabled', 'disabled');
                }
            });
        }


        function setAutocomplete() {
            try {
                $("[id$=txtTransport]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmFreightPackingCharges.aspx/GetTransportDetails",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[1],
                                        val: item.split('|')[1],
                                        val1: item.split('|')[2],
                                        val2: item.split('|')[3],
                                        val3: item.split('|')[4],
                                        val4: item.split('|')[5],
                                        val5: item.split('|')[6],
                                        val6: item.split('|')[7],
                                        val7: item.split('|')[8],
                                        val8: item.split('|')[0]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=hidTransfercode.ClientID %>').val($.trim(i.item.val8));
                        $('#<%=txtTransport.ClientID %>').val($.trim(i.item.val));
                        $('#<%=txtFromStation.ClientID %>').val($.trim(i.item.val5));
                        $('#<%=txtToStation.ClientID %>').val($.trim(i.item.val6));
                        $('#<%=hidTransportName.ClientID %>').val($.trim(i.item.val));

                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $('#<%=hidTransfercode.ClientID %>').val($.trim(i.item.val8));
                        $('#<%=txtTransport.ClientID %>').val($.trim(i.item.val));
                        $('#<%=txtFromStation.ClientID %>').val($.trim(i.item.val5));
                        $('#<%=txtToStation.ClientID %>').val($.trim(i.item.val6));
                        $('#<%=hidTransportName.ClientID %>').val($.trim(i.item.val));

                        return false;
                    },
                    minLength: 1
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncAdd() {
            try {
                var Show = '';

                if (document.getElementById("<%=txtTransport.ClientID%>").value == "") {
                    Show = Show + 'Please Enter Transport Name';
                }

                if (document.getElementById("<%=txtFromStation.ClientID%>").value == "") {
                    Show = Show + '<br />From Station is Invalid';
                }
                if (document.getElementById("<%=txtToStation.ClientID%>").value == "") {
                    Show = Show + '<br />To Station is Invalid';
                }

                if (document.getElementById("<%=txtPackages.ClientID%>").value == "") {
                    Show = Show + '<br />Please Enter Pakages';
                }
                if (document.getElementById("<%=txtFreightCharges.ClientID%>").value == "" ||
                    parseFloat(document.getElementById("<%=txtFreightCharges.ClientID%>").value) <= 0) {
                    Show = Show + '<br />Please Enter Valid Freight Charges';
                }
                if (document.getElementById("<%=txtWagesPlace.ClientID%>").value == "") {
                    Show = Show + '<br />Please Wages Place';
                }
                if (document.getElementById("<%=txtWagesAmt.ClientID%>").value == "" ||
                    parseFloat(document.getElementById("<%=txtWagesAmt.ClientID%>").value) <= 0) {
                    Show = Show + '<br />Please Enter Valid Wages Amount';
                }
                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }
                var Status = false;
                var pakage = '';
                $("#tblFreightDetails tbody").children().each(function () {
                    var cells = $("td", this);
                    if (cells.eq(2).text().trim() == $('#<%=txtFromStation.ClientID %>').val().trim() && cells.eq(3).text().trim() == $('#<%=txtToStation.ClientID %>').val().trim()
                        && cells.eq(4).text().trim() == $('#<%=txtPackages.ClientID %>').val().trim() && cells.eq(6).text().trim() == $('#<%=txtWagesPlace.ClientID %>').val().trim()
                        && cells.eq(8).text().trim() == $('#<%=hidTransfercode.ClientID %>').val().trim()) {
                        pakage = cells.eq(0).text().trim();
                        Status = true;
                    }
                });
                if (Status == true) {
                    ShowPopupMessageBox("Package Already Exists -S.NO = " + pakage);
                    return false;
                }
                var tblFreightDetails = $("#tblFreightDetails tbody");
                var sno = tblFreightDetails.children().length + 1;
                var row = "<tr><td class='text-center' id ='sno_'> " +
                    sno + "</td>" +
                    "<td class='tblbtn' title='Click here to Delete' style = 'cursor:pointer;'><img src='../images/delete.png' onclick ='fnctblClickDelete(this);return false;'/> " +
                    "</td><td>" +
                    $('#<%=txtFromStation.ClientID %>').val() + "</td><td>" +
                    $('#<%=txtToStation.ClientID %>').val() + "</td><td>" +
                    $('#<%=txtPackages.ClientID %>').val() + "</td><td>" +
                    $('#<%=txtFreightCharges.ClientID %>').val() + "</td><td>" +
                    $('#<%=txtWagesPlace.ClientID %>').val() + "</td><td>" +
                    $('#<%=txtWagesAmt.ClientID %>').val() + "</td></tr>";
                tblFreightDetails.append(row);
                $('#<%=txtPackages.ClientID %>').focus();
                fncClearDetails();
                Decimal();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function Decimal() {
            $('#<%=txtFreightCharges.ClientID %>').number(true, 2);
            $('#<%=txtWagesAmt.ClientID %>').number(true, 2);
        }
        function fncClearDetails() {
            $('#<%=txtPackages.ClientID %>').val('');
            $('#<%=txtFreightCharges.ClientID %>').val(0);
            $('#<%=txtWagesPlace.ClientID %>').val('GF');
            $('#<%=txtWagesAmt.ClientID %>').val(0);
        }
        function fnctblClickDelete(obj) {
            var row = $(obj).closest("tr");

            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        row.remove();
                        $(this).dialog("close");
                        return false;
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        return false;
                    }
                }
            });
        }
        function fncSetValue() {

        }

        function fncSave() {

            try {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
                if ($("#tblFreightDetails tbody").children().length <= 0) {
                    ShowPopupMessageBox("Freight Details Invalid");
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                    return false;
                }

                var rowFreight = "<table>";
                $("#tblFreightDetails tbody").children().each(function () {
                    var cells = $("td", this); 
                    rowFreight = rowFreight + "<fright  FromStation='" + cells.eq(2).text().trim() + "'";
                    rowFreight = rowFreight + " ToStation='" + cells.eq(3).text().trim() + "' ";
                    rowFreight = rowFreight + " Packages='" + cells.eq(4).text().trim() + "'";
                    rowFreight = rowFreight + " FreightAmt='" + cells.eq(5).text().trim() + "'";
                    rowFreight = rowFreight + " WagesPlace='" + cells.eq(6).text().trim() + "'";
                    rowFreight = rowFreight + " WagesAmt='" + cells.eq(7).text().trim() + "'";
                    rowFreight = rowFreight + "></fright>"; 
                });

                rowFreight = rowFreight + "</table>";

                $('#<%=hidXml.ClientID %>').val(escape(rowFreight));
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncClear() {
            $('#<%=txtTransport.ClientID %>').val('');
            $('#<%=txtTransport.ClientID %>').removeAttr('disabled');
            $("#tblFreightDetails tbody").children().remove();
            fncClearDetails();
            $('#<%=txtTransport.ClientID %>').focus();
        }

        function fncLoadFreightDetails() {

            var allObj = jQuery.parseJSON($('#<%=hidXml.ClientID %>').val());
            console.log(allObj);
            var tblFreightDetails = $("#tblFreightDetails tbody");
            tblFreightDetails.children().remove();
            for (var i = 0; i < allObj.length; i++) {
                var row = "<tr><td class='text-center'> " +
                    (parseFloat(i) + 1) + "</td>" +
                    "<td class='tblbtn' title='Click here to Delete' style = 'cursor:pointer;'><img src='../images/delete.png' onclick ='fnctblClickDelete(this);return false;'/> " +
                    "</td><td>" +
                    allObj[i]["FromStation"] + "</td><td>" +
                    allObj[i]["ToStation"]  + "</td><td>" +
                    allObj[i]["Packages"] + "</td><td>" +
                    allObj[i]["FreightAmt"]  + "</td><td>" +
                    allObj[i]["WagesPlace"] + "</td><td>" +
                    allObj[i]["WagesAmount"]  + "</td></tr>";
                tblFreightDetails.append(row);
            }
            return false;
        }

    </script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'FreightPackingCharges');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "FreightPackingCharges";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Masters</a> <i class="fa fa-angle-right"></i></li>
                <li><a href="../Managements/frmFreightPackageList.aspx">Freight Charges View</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Freight Packaging Charges</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
        </div>
        <asp:HiddenField ID="hdnValue" Value="" runat="server" />
        <div class="col-md-4">
            <div class="container-group-small">
                <div class="whole-price-header">
                    Transport Details
                </div>
                <div class="container-control">
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label1" runat="server" Text="Transport"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtTransport" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label9" runat="server" Text="From Station"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtFromStation" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label2" runat="server" Text="To Station"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtToStation" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label3" runat="server" Text="Packages"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtPackages" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label4" runat="server" Text="Freight Charges"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtFreightCharges" Text="0" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="container-group-small">
                    <div class="whole-price-header">
                        Wages
                    </div>
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Wages Place"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtWagesPlace" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtWagesPlace', 'txtWagesAmt');" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="Wages Amount"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtWagesAmt" Text="0" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button-contol">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue"
                            OnClientClick="fncAdd();return false;"><i class="icon-play"></i>Add</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClientClick="clearForm();return false;">
                            <i class="icon-play"></i>Clear</asp:LinkButton>
                    </div>
                </div>  
            </div>
        </div>
        <div class="col-md-8">
            <div class="Payment_fixed_headers grdLoad">
                <table id="tblFreightDetails" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">Delete
                            </th>
                            <th scope="col">From Station
                            </th>
                            <th scope="col">To Station
                            </th>
                            <th scope="col">Pakages
                            </th>
                            <th scope="col">Freight Amount
                            </th>
                            <th scope="col">Wages Place
                            </th>
                            <th scope="col">Wages Amount
                            </th>
                        </tr>
                    </thead>
                    <tbody style="height: 450px;"></tbody>
                </table>
            </div>
        </div>
        <div class="button-contol">
            <div class="control-button">
                <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                    OnClientClick="return fncSave();"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkAllClear" runat="server" class="button-red" OnClientClick="fncClear();return false;">
                            <i class="icon-play"></i>Clear(F6)</asp:LinkButton>
            </div>
             <div class="control-button">
                        <asp:LinkButton ID="lnkView" runat="server" class="button-red"
                            PostBackUrl="~/Managements/frmFreightPackageList.aspx">View</asp:LinkButton>
                    </div>
        </div>
        <div class="hiddencol">
            <asp:Button ID="btnDelete" runat="server" />
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
            <asp:HiddenField ID="hidTransfercode" Value="" runat="server" />
            <asp:HiddenField ID="hidXml" Value="" runat="server" />
            <asp:HiddenField ID="hidPackageCode" Value="" runat="server" />
            <asp:HiddenField ID="hidTransportName" Value="" runat="server" />

        </div>
    </div>
</asp:Content>
