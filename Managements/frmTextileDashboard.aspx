﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTextileDashboard.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmTextileDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/textilebarcode.css" rel="stylesheet" />
    <link href="../css/fonts/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet" />
    <link href="../css/fonts/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <script type="text/javascript">
        function pageLoad() {
            try {
                fncDashboard();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncDashboard() {
            try {
                  
                    $.ajax({
                        type: "POST",
                        url: "frmTextileDashboard.aspx/fncGetDashBoardDetails",
                        //data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            fncBindSalesDetail(msg);
                        },
                        error: function (data) {
                            ShowPopupMessageBoxPayment(data.message);
                            return false;
                        }
                    });
                 
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        function fncBindSalesDetail(source) {
            try { 
                var obj = jQuery.parseJSON(source.d);
                if (obj.Table.length > 0) {
                   
                    $("#divTodaySales").html(parseFloat($.trim(obj.Table[0]["TotalSales"])).toFixed(2));
                    $("#divCurrentMonthSales").html(parseFloat($.trim(obj.Table1[0]["TotalSales"])).toFixed(2));
                    $("#divPaymodeSales ").html(parseFloat($.trim(obj.Table2[0]["TotalSales"])).toFixed(2));
                    $("#divDueSales").html(parseFloat($.trim(obj.Table3[0]["TotalSales"])).toFixed(2));
                    $("#divTodayPurchase").html(parseFloat($.trim(obj.Table4[0]["TotalPurchase"])).toFixed(2));
                    $("#divMonthPurchase").html(parseFloat($.trim(obj.Table5[0]["TotalPurchase"])).toFixed(2));
                    $("#divTotalOutstanding").html(parseFloat($.trim(obj.Table6[0]["TotalOutstanding"])).toFixed(2));
                    $("#divProfitPer").html(parseFloat($.trim(obj.Table7[0]["TodayQty"])).toFixed(2));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-12">
        <div class="col-md-12" style="margin-top: 5px;">
        </div>
        <div class="panel-body">
            <div class="text-center border-line">
                <div class="col-md-12" style="margin-top: 5px;">
                    <div class="col-md-4"></div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        <span class="header">Sales Details</span>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4"></div>
                </div>
                <div class="col-md-12 border-double" style="border-right: none;">
                    <div class="col-md-3 col-xs-3 border-outset">
                        <div class="round round-lg green">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                        </div>
                        <div class="glyphicontextcolor">
                            <div class="col-md-12 breakeventext"><b id="divTodaySales">0.00</b></div>
                            <div class="col-md-12"><b>Today Sales</b></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3 border-outset">
                        <div class="round round-lg green">
                            <i class="fa fa-balance-scale"></i>
                        </div>
                        <div class="glyphicontextcolor">
                            <div class="col-md-12 breakeventext"><b id="divCurrentMonthSales">0.00</b></div>
                            <div class="col-md-12"><b>Current Month Sales</b></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3 border-outset left" style="left: 2px;">
                        <div class="round round-lg green">
                            <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                        </div>
                        <div class="glyphicontextcolor">
                            <div class="col-md-12 breakeventext"><b id="divPaymodeSales">0.00</b></div>
                            <div class="col-md-12"><b>Paymode Sales</b></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3 border-outset top">
                        <div class="round round-lg green">
                            <span class="glyphicon glyphicon-thumbs-up"></span>

                        </div>
                        <div class="glyphicontextcolor">
                            <div class="col-md-12 breakeventext"><b id="divDueSales">0.00</b></div>
                            <div class="col-md-12"><b>Due Sales</b></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center border-line">
                <div class="col-md-12" style="margin-top: 5px;">
                    <div class="col-md-4"></div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        <span class="header">Purchase Details</span>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4"></div>
                </div>
                <div class="col-md-12 border-double" style="border-right: none;">
                    <div class="col-md-3 col-xs-3 border-outset">
                        <div class="round round-lg green">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                        </div>
                        <div class="glyphicontextcolor">
                            <div class="col-md-12 breakeventext"><b id="divTodayPurchase">0.00</b></div>
                            <div class="col-md-12"><b>Today Purchase</b></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3 border-outset left">
                        <div class="round round-lg green">
                            <i class="fa fa-balance-scale"></i>
                        </div>
                        <div class="glyphicontextcolor">
                            <div class="col-md-12 breakeventext"><b id="divMonthPurchase">0.00</b></div>
                            <div class="col-md-12"><b>Current Month Purchase</b></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3 border-outset left" style="left: 2px;">
                        <div class="round round-lg green">
                            <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                        </div>
                        <div class="glyphicontextcolor">
                            <div class="col-md-12 breakeventext"><b id="divTotalOutstanding">0.00</b></div>
                            <div class="col-md-12"><b>Total Outstanding</b></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3 border-outset top">
                        <div class="round round-lg green">
                            <span class="glyphicon glyphicon-thumbs-up"></span>

                        </div>
                        <div class="glyphicontextcolor">
                            <div class="col-md-12 breakeventext"><b id="divProfitPer">0.00</b></div>
                            <div class="col-md-12"><b>Today Purchased Qty</b></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
