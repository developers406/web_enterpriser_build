﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmProductPlant.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmProductPlant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/CategoryAttribute.css" rel="stylesheet" />
    <style type="text/css">
        td, th {
            width: 191px !important;
        }

        #tblProductGroup td {
            text-align: center !important;
        }

        .Cat_Margin {
            margin-top: 4px;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            $(".chzn-container").css('display', 'none');
            $(".form-control-res").css('height', '25px');
            $(".form-control-res").css('display', 'block');
            fncDecimal();
            if ($.session.get('height') != null && $.session.get('height') != "") {
                var height = $.session.get('height');
                height = parseFloat(height) - 475;
                $("#tblProductGroup tbody").css('height', '' + height + "px" + '');
            }
            if ($('#<%=hidEditvalue.ClientID%>').val() != "") {
                $('#<%=txtQty.ClientID%>').attr('readonly', 'readonly');
                $('#<%=txtItmName.ClientID%>').attr('readonly', 'readonly');
                $('#<%=ddlProduct.ClientID%>').attr('readonly', 'readonly');
                $('#<%=ddlTax.ClientID%>').attr('readonly', 'readonly');
                $('#<%=lnkAClearAll.ClientID%>').css('display', 'none');
                $('#<%=lnkSave.ClientID%>').text('Update');
                $('#<%=txtCost.ClientID%>').select();
            }
        }

        function fncDecimal() {
            try {
                $('#<%=txtQty.ClientID%>').number(true, 2);
                $('#<%=txtCost.ClientID%>').number(true, 2);
                $('#<%=txtMRP.ClientID%>').number(true, 2);
                $('#<%=txtAssembleCost.ClientID%>').number(true, 2);
                $('#<%=txtLabourCharge.ClientID%>').number(true, 2);
                $('#<%=txtSPrice.ClientID%>').number(true, 2);
                $('#<%=txtMargin.ClientID%>').number(true, 2);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncClearAll() {
            $('#<%=txtQty.ClientID%>').val(0);
            $('#<%=txtCost.ClientID%>').val(0);
            $('#<%=txtMRP.ClientID%>').val(0);
            $('#<%=txtAssembleCost.ClientID%>').val(0);
            $('#<%=txtLabourCharge.ClientID%>').val(0);
            $('#<%=txtSPrice.ClientID%>').val(0);
            $('#<%=txtMargin.ClientID%>').val(0);
            $('#<%=ddlProduct.ClientID%>').val('');
            $('#<%=ddlTax.ClientID%>').val('');
            $('#<%=txtItmName.ClientID%>').val('');
            $('#<%=hidTaxAmt.ClientID%>').val(0);
            $('#<%=hidTaxCode.ClientID%>').val('');
            $("#tblProductGroup tbody").children().remove();
        }

        function fncGetGroupdetail() {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("frmProductPlant.aspx/fncGetProductData")%>',
                data: "{'GroupCode':'" + $('#<%=ddlProduct.ClientID %>').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    fncItemBindtoTable(msg); 
                    if ($('#<%=txtQty.ClientID%>').val() != '' && parseFloat($('#<%=txtQty.ClientID%>').val()) > 0)
                        fncQtyCalculation();
                    if ($('#<%=hidEditvalue.ClientID%>').val() == "")
                        $('#<%=txtItmName.ClientID%>').select();
                    if ($('#<%=hidEditvalue.ClientID%>').val() == "Update")
                         fncSaveAlert($('#<%=hidPlantNo.ClientID%>').val());
                },
                error: function (data) {
                    ShowPopupMessageBox(data.message);
                }
            });
        }

        function fncItemBindtoTable(msg) {
            try {
                var objAttributeData = jQuery.parseJSON(msg.d);
                var tblProductGroup = $("#tblProductGroup tbody");
                tblProductGroup.children().remove();

                if (objAttributeData.length > 0) {

                    for (var i = 0; i < objAttributeData.length; i++) {
                        var row = "<tr id='trAttribute_" + i + "' tabindex='" + i + "' >" +
                            "<td id='tdCode_" + i + "' >" + (parseFloat(i) + 1) + "</td>" +
                            "<td id='tdCode_" + i + "' >" + objAttributeData[i]["ItemCode"] + "</td>" +
                            "<td id='tdItemName_" + i + "' > " + objAttributeData[i]["Description"] + "</td>" +
                            "<td id='tdQty_" + i + "' > " + parseFloat(objAttributeData[i]["Qty"]).toFixed(2) + "</td>" +
                            "<td id='tdCost_" + i + "' > " + parseFloat(objAttributeData[i]["Cost"]).toFixed(2) + "</td>" +
                            "<td id='tdTotQty_" + i + "' > " + parseFloat(0).toFixed(2) + "</td>" +
                            "<td id='tdTotCost_" + i + "' > " + parseFloat(0).toFixed(2) + "</td>" +
                            "</tr>";
                        tblProductGroup.append(row);
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSave() {
            try {
                $('#<%=lnkSave.ClientID%>').css('disaplay', 'none');
                var Show = '';
                if ($('#<%=ddlProduct.ClientID%>').val() == "") {
                    Show = Show + 'Please Select Product.';
                }
                if ($('#<%=txtItmName.ClientID%>').val() == "") {
                    Show = Show + 'Please Enter Item Name.';
                }
                if ($('#<%=txtQty.ClientID%>').val() == "" || parseFloat($('#<%=txtQty.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter Valid Qty.';
                }
                if ($('#<%=txtCost.ClientID%>').val() == "" || parseFloat($('#<%=txtCost.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter Valid Cost.';
                }
                if ($('#<%=txtMRP.ClientID%>').val() == "" || parseFloat($('#<%=txtMRP.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter Valid MRP.';
                }
                if ($('#<%=ddlTax.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Valid GST.';
                }
                if ($('#<%=txtSPrice.ClientID%>').val() == "" || parseFloat($('#<%=txtSPrice.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter Valid Selling Price.';
                }
                if (parseFloat($('#<%=txtCost.ClientID%>').val()) > parseFloat($('#<%=txtMRP.ClientID%>').val())) {
                    Show = Show + '<br /> Cost Must be Less than or Equal to MRP.';
                }
                if (parseFloat($('#<%=txtSPrice.ClientID%>').val()) > parseFloat($('#<%=txtMRP.ClientID%>').val())) {
                    Show = Show + '<br /> Selling Price Must be Less than or Equal to MRP.';
                }
                if ($("#tblProductGroup tbody").children().length <= 0) {
                    Show = Show + '<br /> Invalid RawMaterial Details.';
                }
                if (Show != '') {
                    $('#<%=lnkSave.ClientID%>').css('disaplay', 'block');
                    ShowPopupMessageBox(Show);
                    return false;
                }
                else {
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSaveAlert(GroupNo) {
            try {
                fncClearAll();
                ShowPopupMessageBox("Plant Production Saved Successfully -" + GroupNo);
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncCalculation() {
            try {
                var Cost = $('#<%=txtCost.ClientID%>').val();
                var TotalPer = 0;
                var Tax = $('#<%=ddlTax.ClientID%> option:selected').text();
                if (Tax != '')
                    TotalPer = Tax.split('-')[1].trim();
                if (TotalPer == '' || TotalPer == undefined)
                    TotalPer = 0;
                var Margin = $('#<%=txtMargin.ClientID%>').val();
                var SPrice = parseFloat(Cost) + parseFloat((Cost * Margin) / 100);
                var TotalTaxAmt = parseFloat(SPrice * TotalPer / 100).toFixed(2);
                SPrice = parseFloat(SPrice) + parseFloat(TotalTaxAmt);
                $('#<%=txtSPrice.ClientID%>').val(parseFloat(SPrice).toFixed(2));
                $('#<%=hidTaxAmt.ClientID%>').val(parseFloat(TotalTaxAmt).toFixed(2));
                $('#<%=hidTaxCode.ClientID%>').val($('#<%=ddlTax.ClientID%> option:selected').val());
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncQtyCalculation() {
            try {
                var TotQty = $('#<%=txtQty.ClientID%>').val();
                var SumCost = 0, SumQty = 0;
                $("#tblProductGroup tbody").children().each(function () {
                    var obj = $(this);
                    var Qty = obj.find('td[id*=tdQty]').text();
                    var Cost = obj.find('td[id*=tdCost]').text();
                    var TQty = TotQty * Qty;
                    var TCost = TotQty * Cost;
                    obj.find('td[id*=tdTotQty]').text(parseFloat(TQty).toFixed(2));
                    obj.find('td[id*=tdTotCost]').text(parseFloat(TCost).toFixed(2));
                    SumCost = parseFloat(SumCost) + parseFloat(TCost);
                    SumQty = parseFloat(SumQty) + parseFloat(TQty);
                });
                $("#tblProductGroup tfoot").find('th[id*=tfTotalCost]').text(parseFloat(SumCost).toFixed(2));
                $("#tblProductGroup tfoot").find('th[id*=tfTotalQty]').text(parseFloat(SumQty).toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncValidateItemName() {
            try {
                var obj = {}, objdata;
                obj.ItemName = $('#<%=txtItmName.ClientID %>').val();

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncGetInventoryDetailForValidation")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var Itemname1 = $('#<%=txtItmName.ClientID %>').val().trim();
                        objdata = jQuery.parseJSON(msg.d);
                        if (objdata.Table.length > 0) {
                            var Itemname2;
                            Itemname2 = objdata.Table[0]["Description"];
                            if (Itemname1 == Itemname2) {
                                $('#<%=txtItmName.ClientID %>').select().focus();
                                fncToastError('This Inventory Name Already Exists !!');
                                $('#<%=txtItmName.ClientID %>').css("border", "1px solid red");
                                $('#<%=txtItmName.ClientID %>').select().focus();
                                return false;
                            }
                            else
                                return true;
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Plant Production View</li>
                <li class="active-page">Plant Production</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div id="dialog-confirm" style="display: none;"></div>
        <div class="EnableScroll">
            <div class="col-md-12" style="background-color: aliceblue">
                <div class="col-md-12">
                    <span class="glyphicon glyphicon-arrow-left hedertext"></span><span class="whole-price-header">Assemble Entry</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-2 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Product Group</span>
                    </div>
                    <div class="col-md-10 Cat_Margin">
                        <asp:DropDownList ID="ddlProduct" runat="server" CssClass="form-control-res" onchange="fncGetGroupdetail();return false;">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-2 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Item Name</span>
                    </div>
                    <div class="col-md-10 Cat_Margin">
                        <asp:TextBox ID="txtItmName" runat="server" CssClass="form-control-res"  onfocusout="return fncValidateItemName();"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-2 Cat_Margin">
                        <span class="lblBlack">Total Qty</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res" onchange="fncQtyCalculation();return false;"></asp:TextBox>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <span class="lblBlack">Cost</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtCost" runat="server" CssClass="form-control-res" onchange="fncCalculation();return false;"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-2 Cat_Margin">
                        <span class="lblBlack">MRP</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtMRP" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <span class="lblBlack">GST</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:DropDownList ID="ddlTax" runat="server" CssClass="form-control-res" onchange="fncCalculation();return false;">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-2 Cat_Margin">
                        <span class="lblBlack">LabourCharge</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtLabourCharge" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <span class="lblBlack">Assemble Cost</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtAssembleCost" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-2 Cat_Margin">
                        <span class="lblBlack">Margin %</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtMargin" runat="server" CssClass="form-control-res" onchange="fncCalculation();return false;"></asp:TextBox>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <span class="lblBlack">S.Price</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtSPrice" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-6"></div>
            <div class="col-md-12" style="background-color: aliceblue">
                <div class="col-md-12">
                    <span class="whole-price-header">Raw Material Consume</span>
                </div>
            </div>
            <div class="Payment_fixed_headers Cat_Margin">
                <table id="tblProductGroup" cellspacing="0" rules="all" border="1" style="width: 100%;" class="Invoice">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">ItemCode
                            </th>
                            <th scope="col">Description  
                            </th>
                            <th scope="col">Qty  
                            </th>
                            <th scope="col">Cost
                            </th>
                            <th scope="col">Total Consumed Qty  
                            </th>
                            <th scope="col">Total Consumed Cost
                            </th>
                        </tr>
                    </thead>
                    <tbody style="overflow-y: scroll; height: 200px;">
                    </tbody>
                    <tfoot style="background-color: yellow; font-size: 15px; font-weight: 900;">
                        <tr>
                            <th>Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th id="tfTotalQty"></th>
                            <th id="tfTotalCost"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-md-12 Cat_Margin">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" Style="float: right; margin-left: 10px;" OnClick="lnkSave_Click"
                        OnClientClick="return fncSave();"><i class="icon-play"></i>Save</asp:LinkButton>

                    <asp:LinkButton ID="lnkAClearAll" runat="server" class="button-blue" Style="float: right; margin-left: 10px;"
                        OnClientClick="fncClearAll();return false;"><i class="icon-play"></i>Clear</asp:LinkButton>

                    <asp:LinkButton ID="lnkClose" runat="server" class="button-green" Style="float: right; margin-left: 10px;" PostBackUrl="~/Managements/frmPlantView.aspx"><i class="icon-play"></i>Close</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidTaxAmt" runat="server" />
    <asp:HiddenField ID="hidPlantNo" runat="server" Value="" />
    <asp:HiddenField ID="hidEditvalue" runat="server" Value="" />
    <asp:HiddenField ID="hidTaxCode" runat="server" Value="" />
</asp:Content>
