﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmGstAnalysis_MC.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmGstAnalysis_MC" %>

<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    <style type="text/css">
        .chzn-drop {
            width: 100% !important;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'GSTAnalysis_MC');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "GSTAnalysis_MC";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
    <script type="text/javascript">
        $(document).ready(function () { 
            $('#<%= rdoGstr.ClientID %> input').click(function () { 
                var checkvalue = $("#<%= rdoGstr.ClientID %> input:radio:checked").val();
                if (checkvalue != "") {
                    $('#ContentPlaceHolder1_GSTReportFilterUserControl_txtLocation').val('');
                    $('#ContentPlaceHolder1_CompanyIdFilter_txtCompId').val('');
                }
            });
        });
        function pageLoad() {

            $("[id*=divLocation]").show();
            $("[id*=divComapnyId]").show();
            $("#ContentPlaceHolder1_GSTReportFilterUserControl_ddlLocation_chzn").css("width", "100%");

            $("select").chosen({ width: '100%' });   
             var checkedvalue = $("#<%=rdoGstr.ClientID%>").find(":checked").val();
             if (checkvalue != "") {
                    $('#ContentPlaceHolder1_GSTReportFilterUserControl_txtLocation').val('');
                    $('#ContentPlaceHolder1_CompanyIdFilter_txtCompId').val('');
                }
            $('#<%=chkByGstrDate.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtFromDateGstr]").removeAttr("disabled");
                    $("[id*=txtToDateGstr]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDateGstr]").attr("disabled", "disabled");
                    $("[id*=txtToDateGstr]").attr("disabled", "disabled");
                }
            });

            var ByGstrdatevalue = $("#<%=chkByGstrDate.ClientID%>").is(':checked');
            if (ByGstrdatevalue) {
                $("[id*=txtFromDateGstr]").removeAttr("disabled");
                $("[id*=txtToDateGstr]").removeAttr("disabled");
            }
            else {
                $("[id*=txtFromDateGstr]").attr("disabled", "disabled");
                $("[id*=txtToDateGstr]").attr("disabled", "disabled");
            }
        }

        function clearForm() {

            try {

                $("select").trigger("liszt:updated");
            }
            catch (err) {

                alert(err.Message);
                return false;
            }

            return false;
        }  
        $(function () { 
            try { 
                if ($("#<%= txtFromDateGstr.ClientID %>").val() === '') {
                    $("#<%= txtFromDateGstr.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
                if ($("#<%= txtToDateGstr.ClientID %>").val() === '') {
                    $("#<%= txtToDateGstr.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                alert(err.Message);
            }
        });


        function JSalerts(Show) {
            swal({
                title: "Please fill the details!",
                text: Show,
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                cancelButtonText: "I am not sure!",
                closeOnConfirm: true,
                closeOnCancel: true
            });
        }

        function DateCheck() {
            var StartDate = $("#<%= txtFromDateGstr.ClientID %>").val();
            var EndDate = $("#<%= txtToDateGstr.ClientID %>").val();
            var eDate = new Date(EndDate);
            var sDate = new Date(StartDate);
            if (StartDate != '' && EndDate != '' && sDate > eDate) {
                ShowPopupMessageBox("Please ensure that the End Date is greater than or equal to the Start Date.");
                return false;
            }
        }

         

        function ValidateGstrForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDateGstr.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose From date';
                    $("#<%= txtFromDateGstr.ClientID %>").focus();
                }

                if ($("#<%= txtToDateGstr.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtToDateGstr.ClientID %>").focus();
                }

                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }

                else {
                    DateCheck();
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        } 

    </script> 
    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 150px;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Management</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">GST-R Multi Company</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>  
                <div class="row">
                    <div class="col-md-4" style="margin-left: 20px">
                        <div>
                            <asp:RadioButtonList ID="rdoGstr" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="B2B"> B2B_Sale</asp:ListItem>
                                <asp:ListItem Value="B2CSmall"> B2CSmall_Sales</asp:ListItem>
                                <asp:ListItem Value="B2CLarge"> B2CLarge_Sales</asp:ListItem>
                                <asp:ListItem Value="SalesReg"> Sales Return (CR Note) to Reg.Customer</asp:ListItem>
                                <asp:ListItem Value="SalesUnReg"> Sales Return (CR Note) to Un Reg.Customer</asp:ListItem>
                                <asp:ListItem Value="HsnSummary">HSN_Summary</asp:ListItem>
                                <asp:ListItem Value="GstExcelExport" style ="display:none;">ANNUAL RETURN GST EXCEL EXPORT</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="lblFromDateGstr" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDateGstr" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="lblToDateGstr" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDateGstr" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px">
                            <div id="DivByGstrdate" class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkByGstrDate" runat="server" Text="By Date" Class="radioboxlistgreen" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-3">
                                <asp:LinkButton ID="lnkLoadGstr" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadGstr_Click">Load Report </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 form_bootstyle_border" id="divLocation" style="display: none;">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="GSTReportFilterUserControl"
                                EnablelocationDropDown="true" />
                        </div>
                        <div class="small-box bg-aqua" id="divComapnyId" style="display: none;">
                            <ups:ReportFilterUserControl runat="server" ID="CompanyIdFilter"
                                EnableCompanyIdTextBox="true" />
                        </div>
                    </div>
                </div>  
    </div>
</asp:Content>
