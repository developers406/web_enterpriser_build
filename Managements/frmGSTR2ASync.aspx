﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmGSTR2ASync.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmGSTR2ASync" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a>Management</a> <i class="fa fa-angle-right"></i></li> 
                <li class="active-page">GSTR2A Sync</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
         <div class="col-md-6">
                    <div class="button-contol">
                         <div class="control-button">
                                <asp:LinkButton ID="lnkFetch" runat="server" class="button-blue" 
                                    OnClick="lnkSync_Click" Text="GSTR2A Sync"></asp:LinkButton>
                            </div>
                    </div> 
                </div>
        </div>
</asp:Content>
