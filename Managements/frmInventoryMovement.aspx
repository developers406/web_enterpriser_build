﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmInventoryMovement.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmInventoryMovement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .divwidth {
            width: 100%;
            margin-left: 0;
        }

        .divwidthShow {
            width: 74%;
            margin-left: 0;
        }

        .invMovement td:nth-child(1), .invMovement th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }
         .invMovement td:nth-child(2), .invMovement th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align:center;
        }

        .invMovement td:nth-child(3), .invMovement th:nth-child(3) {
            min-width: 70px;
            max-width: 70px;
        }

        .invMovement td:nth-child(4), .invMovement th:nth-child(4) {
            min-width: 325px;
            max-width: 325px;
        }

        .invMovement td:nth-child(5), .invMovement th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(5) {
            text-align: right !important;
        }

        .invMovement td:nth-child(6), .invMovement th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(6) {
            text-align: right !important;
        }

        .invMovement td:nth-child(7), .invMovement th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(7) {
            text-align: right !important;
        }

        .invMovement td:nth-child(8), .invMovement th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(8) {
            text-align: right !important;
        }

        .invMovement td:nth-child(9), .invMovement th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(9) {
            text-align: right !important;
        }

        .invMovement td:nth-child(10), .invMovement th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(10) {
            text-align: right !important;
        }

        .invMovement td:nth-child(11), .invMovement th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(11) {
            text-align: right !important;
        }

        .invMovement td:nth-child(12), .invMovement th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(13), .invMovement th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(13) {
            text-align: right !important;
        }

        .invMovement td:nth-child(14), .invMovement th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(14) {
            text-align: right !important;
        }

        .invMovement td:nth-child(15), .invMovement th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(15) {
            text-align: right !important;
        }

        .invMovement td:nth-child(16), .invMovement th:nth-child(16) {
            min-width: 100px;
            max-width: 100px;
        }

        .invMovement td:nth-child(16) {
            text-align: right !important;
        }

        .invMovement td:nth-child(17), .invMovement th:nth-child(17) {
            min-width: 100px;
            max-width: 100px;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'InventoryMovement');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "InventoryMovement";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("[id*=grdItemDetails] tr").live('click', function (event) {
                $("[id*=grdItemDetails]").closest('tr').css("background-color", "");
                $(this).closest('tr').css("background-color", "#acb3a4");
            });
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" ) {
                   $('#<%=lnkExport.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkExport.ClientID %>').css("display", "none");
             }
            var newOption = {};
            var option = '';
            //alert($("#ContentPlaceHolder1_thBatchNo").text());
            $("#<%=ddlSort.ClientID %>").empty();
            $("[id*=tblInvMovement] th").each(function (e) {
                var index = $(this).index();
                var table = $(this).closest('table');
                var val = table.find('.click th').eq(index).text();

                if ($(this).is(":visible")) {
                    if (val != "Detail") {
                        if (val != "Last Stock Reset On")
                        option += '<option value="' + val + '">' + val + '</option>';

                    }
                }
            });

            $("#<%=ddlSort.ClientID %>").append(option);
            $("#<%=ddlSort.ClientID %>").prop('selectedIndex', 2);
            $("#<%=ddlSort.ClientID %>").trigger("liszt:updated");
            $("select").chosen({ width: '100%' });

            fncDecimal();
            SetQualifyingItemAutoComplete();
            $("[id*=divIssued]").hide();
            $("[id*=divReceived]").hide();
            $("[id*=divAllTrans]").show();

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

            $('#<%= rdoStatus.ClientID %> input').click(function () {
                var checkvalue = $(this).val();
                if (checkvalue != null) {
                    $("[id*=divIssued]").hide();
                    $("[id*=divReceived]").hide();
                    $("[id*=divAllTrans]").hide();
                    if (checkvalue == 'All') {
                        $("[id*=ContentPlaceHolder1_rdoStatus_0]").attr('checked', true);
                        $("[id*=divAllTrans]").show();
                    }
                    else if (checkvalue == 'Issued') {
                        $("[id*=ContentPlaceHolder1_rdoStatus_2]").attr('checked', true);
                        $("[id*=divIssued]").show();

                    }
                    else if (checkvalue == 'Received') {
                        $("[id*=ContentPlaceHolder1_rdoStatus_1]").attr('checked', true);
                        $("[id*=divReceived]").show();
                    }
                }
            });

            $('#lblUp').live('click', function (event) {
                $('#lblDown').css("color", "");
                $('#lblUp').css("color", "green");
                var columnIndex = $('#<%=grdItemDetails.ClientID %>').length;
                var sort = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex; 
                if (columnIndex > 0) { 
                    var currentRow = $("[id*=grdItemDetails]  tr:last-child()");
                    var col1 = currentRow.find("td:eq(0)").text();
                    var col2 = currentRow.find("td:eq(1)").text();
                    var col3 = currentRow.find("td:eq(2)").text();
                    var col4 = currentRow.find("td:eq(3)").text();
                    var col5 = currentRow.find("td:eq(4)").text();
                    var col6 = currentRow.find("td:eq(5)").text();
                    var col7 = currentRow.find("td:eq(6)").text();
                    var col8 = currentRow.find("td:eq(7)").text();
                    var col9 = currentRow.find("td:eq(8)").text();
                    var col10 = currentRow.find("td:eq(9)").text();
                    var col11 = currentRow.find("td:eq(10)").text();
                    var col12 = currentRow.find("td:eq(11)").text();
                    var col13 = currentRow.find("td:eq(12)").text();
                    var col14 = currentRow.find("td:eq(13)").text();
                    var col15 = currentRow.find("td:eq(14)").text();
                    var col16 = currentRow.find("td:eq(15)").text();

                    var tr = '<tr class ="GVFixedFooter">';
                    tr += '<td>' + col1 + '</td>';
                    tr += '<td>' + col2 + '</td>';
                    tr += '<td>' + col3 + '</td>';
                    tr += '<td>' + col4 + '</td>';
                    tr += '<td>' + col5 + '</td>';
                    tr += '<td>' + col6 + '</td>';
                    tr += '<td>' + col7 + '</td>';
                    tr += '<td>' + col8 + '</td>';
                    tr += '<td>' + col9 + '</td>';
                    tr += '<td>' + col10 + '</td>';
                    tr += '<td>' + col11 + '</td>';
                    tr += '<td>' + col12 + '</td>';
                    tr += '<td>' + col13 + '</td>';
                    tr += '<td>' + col14 + '</td>';
                    tr += '<td>' + col15 + '</td>';
                    tr += '<td>' + col16 + '</td>';
                    tr += '</tr>';

                    $("[id*=grdItemDetails] tr:last").remove();
                    var tdArray = $('#<%=grdItemDetails.ClientID%>').closest("table").find("tr  td:not(:last-child):nth-child(" + (sort + 2) + ")");
                    //tdArray = tdArray.slice(0, -1);
                    if (sort == 2 || sort == 10) {
                        tdArray.sort(function (p, n) {
                            var pData = $.trim($(p).text().toUpperCase());
                            var nData = $.trim($(n).text().toUpperCase());
                            return pData < nData ? -1 : 1;
                        });
                    }
                    //else if (sort == 15) {
                    //    $.each(tdArray, function (index, value) {
                    //        var t = value.cells[1].textContent.split('-');
                    //        value.data('_ts', new Date(t[2], t[1] - 1, t[0]).getTime());
                    //    }).sort(function (a, b) {
                    //        return $(a).data('_ts') < $(b).data('_ts');
                    //    });

                    //}
                    else {
                        tdArray.sort(function (p, n) {
                            var pData = $.trim($(p).text());
                            var nData = $.trim($(n).text());
                            return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                        });
                    }
                    tdArray.each(function () {
                        var row = $(this).parent();
                        $('#<%=grdItemDetails.ClientID%>').append(row);
                    });
                    $("[id*=grdItemDetails] tr:last").after(tr);
                }
                else {
                    $('#lblDown').css("color", "");
                    $('#lblUp').css("color", "");
                    ShowPopupMessageBox("Please Fetch Details");
                    return false;
                }  
            });
            $('#lblDown').live('click', function (event) {
                $('#lblDown').css("color", "green");
                $('#lblUp').css("color", "");
                var columnIndex = $('#<%=grdItemDetails.ClientID %>').length;
         var sort = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex;

         // tdArray = tdArray.slice(0, -1);
         if (columnIndex > 0) {
             var currentRow = $("[id*=grdItemDetails]  tr:last-child()");
             var col1 = currentRow.find("td:eq(0)").text();
             var col2 = currentRow.find("td:eq(1)").text();
             var col3 = currentRow.find("td:eq(2)").text();
             var col4 = currentRow.find("td:eq(3)").text();
             var col5 = currentRow.find("td:eq(4)").text();
             var col6 = currentRow.find("td:eq(5)").text();
             var col7 = currentRow.find("td:eq(6)").text();
             var col8 = currentRow.find("td:eq(7)").text();
             var col9 = currentRow.find("td:eq(8)").text();
             var col10 = currentRow.find("td:eq(9)").text();
             var col11 = currentRow.find("td:eq(10)").text();
             var col12 = currentRow.find("td:eq(11)").text();
             var col13 = currentRow.find("td:eq(12)").text();
             var col14 = currentRow.find("td:eq(13)").text();
             var col15 = currentRow.find("td:eq(14)").text();
             var col16 = currentRow.find("td:eq(15)").text();

             var tr = '<tr class ="GVFixedFooter">';
             tr += '<td>' + col1 + '</td>';
             tr += '<td>' + col2 + '</td>';
             tr += '<td>' + col3 + '</td>';
             tr += '<td>' + col4 + '</td>';
             tr += '<td>' + col5 + '</td>';
             tr += '<td>' + col6 + '</td>';
             tr += '<td>' + col7 + '</td>';
             tr += '<td>' + col8 + '</td>';
             tr += '<td>' + col9 + '</td>';
             tr += '<td>' + col10 + '</td>';
             tr += '<td>' + col11 + '</td>';
             tr += '<td>' + col12 + '</td>';
             tr += '<td>' + col13 + '</td>';
             tr += '<td>' + col14 + '</td>';
             tr += '<td>' + col15 + '</td>';
             tr += '<td>' + col16 + '</td>';
             tr += '</tr>';
             $("[id*=grdItemDetails] tr:last").remove();
             var tdArray = $('#<%=grdItemDetails.ClientID%>').closest("table").find("tr td:nth-child(" + (sort + 2) + ")");

             if (sort == 2 || sort == 10) {
                 tdArray.sort(function (p, n) {
                     var pData = $.trim($(p).text().toUpperCase());
                     var nData = $.trim($(n).text().toUpperCase());
                     return pData > nData ? -1 : 1;
                 });
             }
             else {
                 tdArray.sort(function (p, n) {
                     var pData = $.trim($(p).text());
                     var nData = $.trim($(n).text());
                     return parseFloat(pData) > parseFloat(nData) ? -1 : 1;
                 });
             }
             tdArray.each(function () {
                 var row = $(this).parent();
                 $('#<%=grdItemDetails.ClientID%>').append(row);
                });
                $("[id*=grdItemDetails] tr:last").after(tr);
            }
            else {
                $('#lblDown').css("color", "");
                $('#lblUp').css("color", "");
                ShowPopupMessageBox("Please Fetch Details");
                return false;
            }
     });
    }



    function CloseTranDiv() {
        try {
            // $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'display:block');  
            $("[id*=HideFilter_ContainerRight]").show();

            return false;
        }
        catch (err) {
            return false;
            alert(err.Message);
        }
    }

    function fncHideFilter() {
        try {

            if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {

            $("[id*=pnlFilter]").hide();
            $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");

                $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                'width': '97%'
            });
            $("select").trigger("liszt:updated");
        }
        else {

            $("[id*=pnlFilter]").show();
            $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                'width': '74%'
            });
            $("select").chosen({ width: '100%' });
            $("select").trigger("liszt:updated");
        }

        return false;
    }
    catch (err) {
        return false;
        alert(err.Message);
    }
}

function clearForm() {

    try {
        $('#<%= txtItemCode.ClientID %>').val('');
            $('#<%= txtItemName.ClientID %>').val('');
            $('#<%= txtVendor.ClientID %>').val('');
            $('#<%= txtDepartment.ClientID %>').val('');
            $('#<%= txtCategory.ClientID %>').val('');
            $('#<%= txtSubCategory.ClientID %>').val('');
            $('#<%= txtBrand.ClientID %>').val('');
            $('#<%= txtFloor.ClientID %>').val('');
            $('#<%= txtShelf.ClientID %>').val('');
            $('#<%= txtSection.ClientID %>').val('');
            $('#<%= txtItemtype.ClientID %>').val('');
            //$("select").val(0);
            $("select").trigger("liszt:updated");
        }
        catch (err) {

            alert(err.Message);
            return false;
        }

        return false;
    }

    function ShowPopup(name) {
        $("#dialog").dialog({
            title: name,
            width: 700,
            buttons: {
                OK: function () {
                    $(this).dialog('close');
                }
            },
            modal: true
        });
    }
    function SetQualifyingItemAutoComplete() {
        $('#<%=txtItemCode.ClientID %>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtItemName.ClientID %>').val($.trim(i.item.valName));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtItemName.ClientID %>').val($.trim(i.item.valName));
                    $('#<%=txtItemCode.ClientID %>').focus();
                    return false;
                },
                minLength: 1
            });
        }


        function ShowPopupTrans(name) {
            $("#divtran").dialog({
                title: name,
                width: 700,
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }

        function fncExcelExport() {
            try {
                $('#<%=btnExcelExport.ClientID %>').click();
            }
            catch (err) {
                fncToastError(err.message)
            }
        }

        function fncDecimal() {
            try {
                $('#<%=txtNoofItems.ClientID %>').number(true, 0);
                $('#<%=txtNewItem.ClientID %>').number(true, 0);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncLocationChange() {
            try {
                if ($('#<%=hidCurLocation.ClientID %>').val() == $('#<%=ddlLocation.ClientID %>').val()) {
                    $('#<%=chkBatchwise.ClientID %>').removeAttr("disabled", "disabled");
                }
                else {
                    $('#<%=chkBatchwise.ClientID %>').prop("checked", false);
                    $('#<%=chkBatchwise.ClientID %>').attr("disabled", "disabled");
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncOpenAttributeDailog() {
            try {
                $("#divAttribute").dialog({
                    resizable: false,
                    height: 320,
                    width: 1200,
                    modal: true,
                    title: "Attribute Filter",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemName.ClientID %>').val(Description);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncClick() {
            document.getElementById("<%=btnSubmit.ClientID %>").click();
        }
        function fncVenItemRowdblClk(rowObj) {
    try {
        rowObj = $(rowObj);
        fncOpenItemhistory($.trim($("td", rowObj).eq(2).text()));
    }
    catch (err) {
        fncToastError(err.message);
    }
}
/// Open Item History
function fncOpenItemhistory(itemcode) {
    var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
    page = page + "?InvCode=" + itemcode + "&Status=dailog";
    var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
        autoOpen: false,
        modal: true,
        height: 700,
        width: 1250,
        title: "Inventory History"
    });
    $dialog.dialog('open');
}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updtPnlTop" UpdateMode="Always" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Inventory Movement </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="container-group-price">

                    <%--<div class="container-date-report" id="pnlFilter" runat="server">--%>
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <div class="barcode-header">
                            Search By
                        </div>
                        <div id="locfilter" runat="server" class="control-group-single-res" style="margin-top: 10px">
                            <div class="label-left">
                                <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res"
                                    onchange="fncLocationChange();">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Department"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Category"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Brand"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtSubCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Labelsc" runat="server" Text="SubCategory"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtFloor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Floor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Labelsec" runat="server" Text="Section"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtShelf');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text="Shelf"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtItemtype');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="LabelIT" runat="server" Text="Item Type"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemtype" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'ItemType', 'txtItemtype', 'txtItemCode');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text="Item Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            <%-- onkeyup="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', 'txtItemName');" Vijay--%>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label16" runat="server" Text="Item Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="barcode-header">
                                Filterations
                            </div>
                        </div>
                        <div class="control-group-single-res" style="margin-top: 10px">
                            <div class="label-left">
                                <asp:Label ID="Lblfrod" runat="server" Text="FromDate"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblto" runat="server" Text="ToDate"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="float_left">
                                <asp:CheckBox ID="chkBatchwise" runat="server" Checked="true" GroupName="priceMenu" />
                                <asp:Label ID="Label10" runat="server" Checked="true" Text="Show Batch Wise"></asp:Label>
                            </div>
                            <div class="float_left" style="padding-left: 10px;">
                                <asp:CheckBox ID="chkShowPackage" runat="server" GroupName="allMenu" />
                                <asp:Label ID="Label19" runat="server" Checked="true" Text="Show Qty in Package"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div style="width: 45%; float: left;">
                                <div class="float_left" style="width: 50%">
                                    <asp:Label ID="Label6" runat="server" Checked="true" Text="No of items"></asp:Label>
                                </div>
                                <div class="float_left" style="padding-left: 2px; width: 50%">
                                    <asp:TextBox ID="txtNoofItems" runat="server" Text="100" CssClass="form-control-res-right"></asp:TextBox>
                                </div>
                            </div>
                            <div style="width: 45%; float: left; padding-left: 4px;">
                                <div class="float_left" style="width: 50%">
                                    <asp:CheckBox ID="cbNewItem" runat="server" Text="New Item" />
                                </div>
                                <div class="float_left" style="padding-left: 2px; width: 50%">
                                    <asp:TextBox ID="txtNewItem" runat="server" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilter" runat="server" OnClientClick="fncClick();return false;" class="button-red"><i class="icon-play" ></i>Fetch</asp:LinkButton>

                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm()"><i class="icon-play"></i>Clear</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                    <div class="col-md-12">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3 sort_by">
                            <div class="col-md-3">
                                <label id="lblSortby" style="margin-top: 5px;">Sort by:</label>
                            </div>
                            <div class="col-md-7">
                                <asp:DropDownList runat="server" Width="100%" ID="ddlSort">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-1">
                                <label id="lblUp" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                            </div>
                            <div class="col-md-1">
                                <label id="lblDown" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="Div1" class="GridDetails" style="width: 100%" runat="server">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30" style="width: 100%">
                                    <div id="HideFilter_GridOverFlow" runat="server" style="width: 100%">
                                        <div class="over_flowhorizontal">
                                            <table rules="all" border="1" id="tblInvMovement" runat="server" class="fixed_header invMovement">
                                                <tr class="click">
                                                    <th>Detail</th>
                                                    <th>S.No</th>
                                                    <th>Item Code</th>
                                                    <th>Description</th>
                                                    <th>MRP</th>
                                                    <th>Selling Price</th>
                                                    <th>Opening Stock</th>
                                                    <th>Received Stock</th>
                                                    <th>Issued Stock</th>
                                                    <th>Derived Stock</th>
                                                    <th>System Stock</th>
                                                    <th id="thBatchNo">Batch No</th>
                                                    <th>Opening Value</th>
                                                    <th>Received Value</th>
                                                    <th>Issued Value</th>
                                                    <th>Balance Value</th>
                                                    <th>Last Stock Reset On</th>
                                                </tr>
                                            </table>
                                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 390px; width: 1800px; background-color: aliceblue;">
                                                <asp:GridView ID="grdItemDetails" runat="server" Width="100%" AutoGenerateColumns="False" ShowHeader="false" ShowFooter="true"  AllowSorting="true" ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgn invMovement" OnRowDataBound="grdItemDetails_RowDataBound">
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                    <PagerSettings FirstPageText="First" LastPageText="Last" PageButtonCount="5" Position="Bottom" />
                                                    <PagerStyle Height="30px" VerticalAlign="Bottom" />
                                                    <PagerStyle CssClass="pshro_text" />
                                                    <FooterStyle CssClass="GVFixedFooter" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lbldet" runat="server" Text="Detail"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnTrans" BackColor="#FF3300" Font-Bold="True" ForeColor="White"
                                                                    Text="Tran" runat="server" AutoPostBack="true" OnClick="OnBtnClick" Height="100%"
                                                                    Width="100%" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                         <asp:BoundField DataField="RowNo" HeaderText="RowNo"></asp:BoundField>
                                                        <asp:BoundField DataField="Item Code" HeaderText="Item Code"></asp:BoundField>
                                                        <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                        <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                        <asp:BoundField DataField="Selling Price" HeaderText="Selling Price"></asp:BoundField>
                                                        <asp:BoundField DataField="Opening Stock" HeaderText="Opening Stock"></asp:BoundField>
                                                        <asp:BoundField DataField="Received Stock" HeaderText="Received Stock"></asp:BoundField>
                                                        <asp:BoundField DataField="Issued Stock" HeaderText="Issued Stock"></asp:BoundField>
                                                        <asp:BoundField DataField="Derived Stock" HeaderText="Derived Stock"></asp:BoundField>
                                                        <asp:BoundField DataField="System Stock" HeaderText="System Stock"></asp:BoundField>
                                                        <asp:BoundField DataField="Batch No" HeaderText="Batch No"></asp:BoundField>
                                                        <asp:BoundField DataField="Opening Value" HeaderText="Opening Value"></asp:BoundField>
                                                        <asp:BoundField DataField="Received Value" HeaderText="Received Value"></asp:BoundField>
                                                        <asp:BoundField DataField="Issued Value" HeaderText="Issued Value"></asp:BoundField>
                                                        <asp:BoundField DataField="Balance Value" HeaderText="Balance Value"></asp:BoundField>
                                                        <asp:BoundField DataField="Last Stock Reset On" HeaderText="Last Stock Reset On" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="false"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="return fncHideFilter()">Hide Filter</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkExport" runat="server" class="button-blue" OnClientClick="fncExcelExport();return false;">Export</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClearAll" Visible="false" runat="server" class="button-blue"
                                OnClientClick="return clearForm()">Clear</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" OnClick="lnkPrint_Click">Print</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkAttribute" runat="server" class="button-blue" OnClientClick="fncOpenAttributeDailog();return false;">Attributes</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div id="divtran" class="container-group-full" style="display: none;">
                    <div class="top-purchase-container">
                        <div class="top-purchase-middle-container" style="padding: 15px 0 5px 5px;">
                            <asp:RadioButtonList ID="rdoStatus" runat="server" Class="radioboxlist" RepeatDirection="Horizontal">
                                <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Received" Value="Received"></asp:ListItem>
                                <asp:ListItem Text="Issued" Value="Issued"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div id="divAllTrans" class="bottom-purchase-container-total" runat="server">
                            <asp:GridView ID="grdAllDetail" runat="server" CellPadding="4" AutoGenerateColumns="False"
                                ShowFooter="True" ForeColor="#333333" Font-Bold="False" GridLines="None" OnDataBound="grdAllDetail_DataBound"
                                OnRowDataBound="grdAllDetail_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="TransactionNo" HeaderText="TransactionNo" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TransactionDate" HeaderText="TransactionDate" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MovementType" HeaderText="TranType" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Party" HeaderText="Party / Customer" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Qty" HeaderText="Quantity" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                </Columns>
                                <AlternatingRowStyle BackColor="White" />
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" BorderColor="Black" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </div>
                        <div id="divReceived" class="bottom-purchase-container-total" runat="server">
                            <asp:GridView ID="grdReceived" runat="server" CellPadding="4" AutoGenerateColumns="False"
                                ShowFooter="True" ForeColor="#333333" Font-Bold="False" GridLines="None" OnDataBound="grdReceived_DataBound">
                                <Columns>
                                    <asp:BoundField DataField="TransactionNo" HeaderText="TransactionNo" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TransactionDate" HeaderText="TransactionDate" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MovementType" HeaderText="TranType" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Party" HeaderText="Party / Customer" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Qty" HeaderText="Quantity" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                </Columns>
                                <AlternatingRowStyle BackColor="White" />
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <RowStyle BackColor="#FFFBD6" BorderColor="Black" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                                <SortedDescendingHeaderStyle BackColor="#820000" />
                            </asp:GridView>
                        </div>
                        <div id="divIssued" class="bottom-purchase-container-total" runat="server">
                            <asp:GridView ID="grdIssued" runat="server" CellPadding="4" AutoGenerateColumns="false"
                                ShowFooter="True" ForeColor="#333333" Font-Bold="False" GridLines="None" OnDataBound="grdIssued_DataBound">
                                <Columns>
                                    <asp:BoundField DataField="TransactionNo" HeaderText="TransactionNo" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TransactionDate" HeaderText="TransactionDate" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MovementType" HeaderText="TranType" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Party" HeaderText="Party / Customer" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Qty" HeaderText="Quantity" ItemStyle-Width="150">
                                        <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                </Columns>
                                <AlternatingRowStyle BackColor="White" />
                                <EditRowStyle BackColor="#7C6F57" />
                                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#E3EAEB" BorderColor="Black" />
                                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                <SortedAscendingHeaderStyle BackColor="#246B61" />
                                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                <SortedDescendingHeaderStyle BackColor="#15524A" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
     
    <div class="display_none">

        <asp:Button ID="btnExcelExport" runat="server" OnClick="lnkExport_Click" />
        <asp:HiddenField ID="hidCurLocation" runat="server" />

        <div class="container welltextiles" id="divAttribute">
            <div class="barcode-header" style="background-color: #c6b700">
                Attributes
            </div>
            <div style="height: auto; border: solid; overflow-x: visible; overflow-y: hidden; height: 215px;">
                <asp:GridView ID="grdAttribute" runat="server" AutoGenerateColumns="False"
                    ShowHeaderWhenEmpty="true" CssClass="fixed_header_Empty_grid"
                    EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                    <EmptyDataTemplate>
                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                    <RowStyle CssClass="pshro_GridDgnStyle" />
                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                    <PagerStyle CssClass="pshro_text" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <Columns>
                    </Columns>
                </asp:GridView>
            </div>
            <br />
            <div class="col-md-2">
            </div>
            <div class="col-md-9">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
        <asp:Button ID="btnSubmit" OnClick="lnkFilter_Click" runat="server" Style="display: none" />
    </div> 
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" /> 
</asp:Content>
