﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmSalesAnalysis.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmSalesAnalysis" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <%------------------------- Alert Style --------------------------%>
    <script src="../js/Validation/sweetalert-dev.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../js/Validation/sweetalert.css" />
    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }

        };

        function pageLoad() {

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

            var checkedvalue = $("#<%=rdoSalesAnalysis.ClientID%>").find(":checked").val();
            var checkedvalueMember = $("#<%=rdoListSalesAnalysMember.ClientID%>").find(":checked").val();

            if (checkedvalue === 'Tran') {
                $("#<%=chkBydate.ClientID%>").prop('checked', 'checked');
                $("#<%=chkBydate.ClientID%>").attr('disabled', 'disabled');
                $('#divTransaction').show();
            }
            else {
                $("#<%=chkBydate.ClientID%>").removeAttr('disabled');
            }

            if (checkedvalue === 'Billcount') {
                $("[id*=divSummDetail]").show();
                $("[id*=divCatDrop]").hide();
                $("[id*=divDeptDrop]").hide();
                $("[id*=divBrandDrop]").hide();
                $("[id*=divVendorDrop]").hide();
                $("[id*=divPinCode]").hide();
            }
            else {
                if (checkedvalue == "PinCodeWise") {
                    $("[id*=divPinCode]").show();
                }
                else {
                    $("[id*=divPinCode]").hide();
                }
                $("[id*=divSummDetail]").hide();
                $("[id*=divCatDrop]").show();
                $("[id*=divDeptDrop]").show();
                $("[id*=divBrandDrop]").show();
                $("[id*=divVendorDrop]").show();
            }

            if ($("[id*=HidenReportType]").val() == "SalesAnalysisMember") {
                if (checkedvalueMember == "Daywise" || checkedvalueMember == "Credit" || checkedvalueMember == "Hourly") {
                    $("[id*=divCatDrop]").show();
                    $("[id*=divDeptDrop]").show();
                    $("[id*=divBrandDrop]").show();
                    $("[id*=divVendorDrop]").show();
                    $("[id*=divPinCode]").hide();
                }
                else {
                    $("[id*=divPinCode]").hide();
                    $("[id*=divCatDrop]").hide();
                    $("[id*=divDeptDrop]").hide();
                    $("[id*=divBrandDrop]").hide();
                    $("[id*=divVendorDrop]").hide();
                }
            }

            $('#<%=chkBydate.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                }
            });
            var Bydatevalue = $("#<%=chkBydate.ClientID%>").is(':checked');
            if (!Bydatevalue) {
                $("[id*=txtFromDate]").attr("disabled", "disabled");
                $("[id*=txtToDate]").attr("disabled", "disabled");
            }
            else {
                $("[id*=txtFromDate]").removeAttr("disabled");
                $("[id*=txtToDate]").removeAttr("disabled");
            }
            //$("#divLocation").hide();
        }

        $(document).ready(function () {

            $('#<%= rdoSalesAnalysis.ClientID %> input').click(function () {
                var checkvalue = $("#<%= rdoSalesAnalysis.ClientID %> input:radio:checked").val();
                if (checkvalue == 'Tran') {
                    $("#<%=chkBydate.ClientID%>").prop('checked', 'checked');
                    $("#<%=chkBydate.ClientID%>").attr('disabled', 'disabled');
                    $('#divTransaction').show();
                    $("#divLocation").hide();
                }
                else {
                    $("#<%=chkBydate.ClientID%>").removeAttr('disabled');
                    $("#divLocation").hide();
                }
                if (checkvalue == 'Billcount') {
                    $("[id*=divSummDetail]").show();
                    $("[id*=divCatDrop]").hide();
                    $("[id*=divDeptDrop]").hide();
                    $("[id*=divBrandDrop]").hide();
                    $("[id*=divVendorDrop]").hide();
                    $("[id*=divPinCode]").hide();
                    $("#divLocation").hide();
                }
                else {
                    if (checkvalue == "PinCodeWise") {
                        $("[id*=divPinCode]").show();
                    }
                    else {
                        $("[id*=divPinCode]").hide();
                    }
                    $("[id*=divSummDetail]").hide();
                    $("[id*=divCatDrop]").show();
                    $("[id*=divDeptDrop]").show();
                    $("[id*=divBrandDrop]").show();
                    $("[id*=divVendorDrop]").show();
                    $("#divLocation").hide();
                    if (checkvalue == "DailySales") {     //Dinesh 24032022
                        $("[id*=divPinCode]").show();
                        $("[id*=divCatDrop]").hide();
                        $("[id*=divDeptDrop]").hide();
                        $("[id*=divBrandDrop]").hide();
                        $("[id*=divVendorDrop]").hide();
                        $("[id*=divPinCode]").hide();
                        $("#divLocation").hide();
                    }
                }
                if (checkvalue == 'Loc') {
                    $("#divLocation").show();
                }
                // dinesh
                $('#ContentPlaceHolder1_rptVendorDrop').val('');
                $('#ContentPlaceHolder1_rptVendorDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_rptBrandDrop').val('');
                $('#ContentPlaceHolder1_rptBrandDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_rptCatDrop').val('');
                $('#ContentPlaceHolder1_rptCatDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_rptDeptDrop').val('');
                $('#ContentPlaceHolder1_rptDeptDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_DropDownLocation').val('');
                $('#ContentPlaceHolder1_DropDownLocation').trigger("liszt:updated");
                $('#ContentPlaceHolder1_DropMemberType').val('');
                $('#ContentPlaceHolder1_DropMemberType').trigger("liszt:updated");
                $('#ContentPlaceHolder1_DropMemberCode').val('');
                $('#ContentPlaceHolder1_DropMemberCode').trigger("liszt:updated");
                $('#ContentPlaceHolder1_ddPinCode').val('');
                $('#ContentPlaceHolder1_ddPinCode').trigger("liszt:updated");//
            });

            $('#<%= rdoListPromotionEvents.ClientID %> input').click(function () {
                // dinesh
                $('#ContentPlaceHolder1_rptVendorDrop').val('');
                $('#ContentPlaceHolder1_rptVendorDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_rptBrandDrop').val('');
                $('#ContentPlaceHolder1_rptBrandDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_rptCatDrop').val('');
                $('#ContentPlaceHolder1_rptCatDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_rptDeptDrop').val('');
                $('#ContentPlaceHolder1_rptDeptDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_DropDownLocation').val('');
                $('#ContentPlaceHolder1_DropDownLocation').trigger("liszt:updated");
                $('#ContentPlaceHolder1_DropMemberType').val('');
                $('#ContentPlaceHolder1_DropMemberType').trigger("liszt:updated");
                $('#ContentPlaceHolder1_DropMemberCode').val('');
                $('#ContentPlaceHolder1_DropMemberCode').trigger("liszt:updated");
                $('#ContentPlaceHolder1_ddPinCode').val('');
                $('#ContentPlaceHolder1_ddPinCode').trigger("liszt:updated");//
                $("#divLocation").hide();
            });

            $('#<%= rdoListSalesAnalysMember.ClientID %> input').click(function () {
                // dinesh
                var checkedvalueMember = $("#<%=rdoListSalesAnalysMember.ClientID%>").find(":checked").val();
                if (checkedvalueMember == "Daywise" || checkedvalueMember == "Credit" || checkedvalueMember == "Hourly") {
                    $("[id*=divCatDrop]").show();
                    $("[id*=divDeptDrop]").show();
                    $("[id*=divBrandDrop]").show();
                    $("[id*=divVendorDrop]").show();
                    $("[id*=divPinCode]").hide();
                }
                else {
                    $("[id*=divPinCode]").hide();
                    $("[id*=divCatDrop]").hide();
                    $("[id*=divDeptDrop]").hide();
                    $("[id*=divBrandDrop]").hide();
                    $("[id*=divVendorDrop]").hide();
                }
                $('#ContentPlaceHolder1_rptVendorDrop').val('');
                $('#ContentPlaceHolder1_rptVendorDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_rptBrandDrop').val('');
                $('#ContentPlaceHolder1_rptBrandDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_rptCatDrop').val('');
                $('#ContentPlaceHolder1_rptCatDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_rptDeptDrop').val('');
                $('#ContentPlaceHolder1_rptDeptDrop').trigger("liszt:updated");
                $('#ContentPlaceHolder1_DropDownLocation').val('');
                $('#ContentPlaceHolder1_DropDownLocation').trigger("liszt:updated");
                $('#ContentPlaceHolder1_DropMemberType').val('');
                $('#ContentPlaceHolder1_DropMemberType').trigger("liszt:updated");
                $('#ContentPlaceHolder1_DropMemberCode').val('');
                $('#ContentPlaceHolder1_DropMemberCode').trigger("liszt:updated");
                $('#ContentPlaceHolder1_ddPinCode').val('');
                $('#ContentPlaceHolder1_ddPinCode').trigger("liszt:updated");//
                $("#divLocation").hide();
            });

        });

        //        $(function () {

        //            if (!$("[id*=chkBydate]").is(":checked")) {
        //                alert('cjecl');
        //                $("[id*=divfilter]").attr("disabled", "disabled");
        //                $("[id*=txtFromDate]").attr("disabled", "disabled");
        //                $("[id*=txtToDate]").attr("disabled", "disabled");
        //            } else {
        //                $("[id*=divfilter]").removeAttr("disabled");
        //                $("[id*=txtFromDate]").removeAttr("disabled");
        //                $("[id*=txtToDate]").removeAttr("disabled");
        //            }
        //        });



        $(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        });

        function DateCheck() {
            var StartDate = $("#<%= txtFromDate.ClientID %>").val();
            var EndDate = $("#<%= txtToDate.ClientID %>").val();
            var eDate = new Date(EndDate);
            var sDate = new Date(StartDate);
            if (StartDate != '' && EndDate != '' && sDate > eDate) {
                alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                return false;
            }
        }

        function JSalerts(Show) {

            //            swal({
            //                title: 'Ajax request example',
            //                text: 'Submit to run ajax request',
            //                type: 'info',
            //                showCancelButton: true,
            //                closeOnConfirm: false,
            //                disableButtonsOnConfirm: true,
            //                confirmLoadingButtonColor: '#DD6B55'
            //            }, function (inputValue) {
            //                setTimeout(function () {
            //                    swal('Ajax request finished!');
            //                }, 2000);
            //            });

            //            swal({
            //                title: "An input!",
            //                text: 'Write something interesting:',
            //                type: 'input',
            //                showCancelButton: true,
            //                closeOnConfirm: false,
            //                animation: "slide-from-top"
            //            }, function (inputValue) {
            //                console.log("You wrote", inputValue);
            //                $("#<%= txtFromDate.ClientID %>").val(inputValue);
            //            });

            //            swal({
            //                title: "Are you sure?",
            //                text: "You will not be able to recover this imaginary file!",
            //                type: "warning",
            //                showCancelButton: true,
            //                confirmButtonColor: "#DD6B55",
            //                confirmButtonText: "Yes, delete it!",
            //                closeOnConfirm: false,
            //                html: false
            //            }, function () {
            //                swal("Deleted!",
            //  "Your imaginary file has been deleted.",
            //  "success");
            //            });


            //            swal("Oops...", "Something went wrong!", "error");
            swal({
                title: "Please fill the details!",
                text: Show,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: "OK",
                cancelButtonText: "I am not sure!",
                closeOnConfirm: false,
                closeOnCancel: false
            });

        }

        function ValidateForm() {
            var Show = '';
            if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose To date';
                $("#<%= txtFromDate.ClientID %>").focus();
            }

            if ($("#<%= txtToDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose from date';
                $("#<%= txtToDate.ClientID %>").focus();
            }

            if (Show != '') {
                alert(Show);
                //JSalerts(Show);
                return false;
            }

            else {
                var StartDate = $("#<%= txtFromDate.ClientID %>").val();
                var EndDate = $("#<%= txtToDate.ClientID %>").val();
                var eDate = new Date(EndDate);
                var sDate = new Date(StartDate);
                if (StartDate != '' && EndDate != '' && sDate > eDate) {
                    alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                    //JSalerts("Please ensure that the End Date is greater than or equal to the Start Date.");
                    return false;
                }
                return true;
            }
        }

        function Clearall() {

            $("select").val(0);
            $("select").trigger("liszt:updated");
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");

            return false;
        }

        function ToogleTextBoxes(s, e) {

            var ToDate = document.getElementById("<%=txtToDate.ClientID%>");
            var FromDate = document.getElementByID("<%=txtFromDate.ClientID%>");
            var disabledState = s.GetChecked() ? '' : 'disabled';

            ToDate.disabled = disabledState;
            FromDate.disabled = disabledState;
        }

        function getCheckedRadio() {
            //var radioButtons = document.getElementsByName("rdoListSalesAnalysMember");
            var selectedVal = $("[id$='rdoListSalesAnalysMember']").find(":checked").val();
            if (selectedVal = 'Member Outstanding') {
                alert(selectedVal)
                $("#divMOUT").show();
            }

        }

        function RadioClicked() {
            var radioButtons = document.getElementById('<%= rdoListSalesAnalysMember.ClientID %>').getElementsByTagName('select');
            //alert(radioButtons + " Clicked");
            for (i = 0; i < radioButtons.length; i++) {
                if (radioButtons[i].checked) {
                    alert(radioButtons[i].value + " Clicked");
                }
            }
        }

        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'SalesAnalysis');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "SalesAnalysis";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Management</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Sales-Analysis Reports </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <div class="container-date-report" id="divSalesAnalysis" runat="server" visible="false">
                <div class="barcode-header">
                    Sales-Analysis Reports
                </div>
                <div>
                    <div class="control-group" style="margin-top: 10px">
                        <asp:RadioButtonList ID="rdoSalesAnalysis" runat="server">
                            <asp:ListItem Selected="True" Value="Loc">Location wise Profit</asp:ListItem>
                            <asp:ListItem Value="vendor">Vendor wise Profit</asp:ListItem>
                            <asp:ListItem Value="dept">Department wise Profit</asp:ListItem>
                            <asp:ListItem Value="category">Category wise Profit</asp:ListItem>
                            <asp:ListItem Value="categoryMargin">Category wise Profit Margin(Excluding GST)</asp:ListItem>
                            <asp:ListItem Value="Brand">Brand wise Profit</asp:ListItem>
                            <asp:ListItem Value="class">Class wise Profit</asp:ListItem>
                            <asp:ListItem Value="Billcount">Location wise Bill Count Group</asp:ListItem>
                            <asp:ListItem Value="soldQty">Location wise Sold Quantity</asp:ListItem>
                            <asp:ListItem Value="Terminalwiseprofit">Terminal Wise Profit</asp:ListItem>
                            <asp:ListItem Value="PinCodeWise">PinCode Wise Profit</asp:ListItem>
                           <asp:ListItem Value="Tran">Transaction Comparision</asp:ListItem>
                              <asp:ListItem Value="DailySales">Daily Summary SalesReport</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="container-date-report" id="divSummDetail" style="float: left; border: 1px solid; padding: 5px 5px 0 5px; width: 70%"
                        runat="server">
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="rdoSummary" runat="server" GroupName="Groupby" Text="SUMMARY"
                                Checked="true" Font-Bold="True" />
                            &nbsp;&nbsp;
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="rdoDetail" runat="server" GroupName="Groupby" Text="DETAIL"
                                Font-Bold="True" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-date-report" id="divSalesAnalysisPromo" runat="server" visible="false">
                <div class="barcode-header">
                    Analysis
                </div>
                <div>
                    <div class="control-group" style="margin-top: 10px">
                        <asp:RadioButtonList ID="rdoListPromotionEvents" runat="server">
                            <asp:ListItem Selected="True">Inventory wise Sales</asp:ListItem>
                            <asp:ListItem>Department wise Sales</asp:ListItem>
                            <asp:ListItem>Category wise Sales</asp:ListItem>
                            <asp:ListItem>Brand wise Sales</asp:ListItem>
                            <asp:ListItem>Free Item Sales</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="container-date-report" >
                        <div class="barcode-header">
                            Promotion Types
                        </div>
                        <asp:RadioButtonList ID="rdoListPromoType" runat="server" Style="margin-top: 10px">
                            <asp:ListItem Selected="True">Promotions</asp:ListItem>
                            <%--<asp:ListItem>Sub Level</asp:ListItem>
                            <asp:ListItem>Events</asp:ListItem>--%>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="container-date-report" id="divSalesAnalysisMember" runat="server" visible="false">
                <div class="barcode-header">
                    Sales Analysis Member
                </div>
                <div style="display: table">
                    <div class="control-group" style="margin-top: 10px">
                        <asp:RadioButtonList ID="rdoListSalesAnalysMember" runat="server" onclick="RadioClicked()">
                            <asp:ListItem Selected="True">Member Details</asp:ListItem>
                            <asp:ListItem>Member Details By Join Date</asp:ListItem>
                            <asp:ListItem>Member List with Point</asp:ListItem>
                            <asp:ListItem Value="MemberTransaction">Member Transaction</asp:ListItem>
                            <asp:ListItem>Member Point Adjustment</asp:ListItem>
                            <asp:ListItem>Age Group</asp:ListItem>
                            <asp:ListItem>Income Group</asp:ListItem>
                            <asp:ListItem>Member List by Age Group</asp:ListItem>
                            <asp:ListItem>Member List by Income</asp:ListItem>
                            <%-- <asp:ListItem>Member Age wise Sales</asp:ListItem>  Dinesh 27102021 Not in --%>
                            <asp:ListItem>Member Outstanding</asp:ListItem>
                            <asp:ListItem>Gift Issued</asp:ListItem>
                           <%-- <asp:ListItem>Unclaimed Gift </asp:ListItem>   Dinesh 27102021 Not in --%>
                            <asp:ListItem Value="Daywise">Daywise Sales</asp:ListItem>
                            <asp:ListItem Value="Credit">Credit Sales</asp:ListItem>
                            <asp:ListItem Value="Hourly">Hourly Sales</asp:ListItem>
                            <asp:ListItem Value="Purchase">Avg.Purchase/Receipt</asp:ListItem>
                            <%--<asp:ListItem Value="IBC">IBC Member Trans</asp:ListItem>--%>
                        </asp:RadioButtonList>
                    </div>
                    <div class="control-group" id="divMOUT" visible="false" style="float: left; border: 1px solid; padding: 5px 5px 0 5px; width: 100%"
                        runat="server">
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="rdoDays" runat="server" Text="Days" Checked="true" GroupName="SRGroup"
                                Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            &nbsp;&nbsp;
                        </div>

                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="rdoLimit" runat="server" Text="Limits" Font-Bold="True" GroupName="SRGroup"
                                Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                        </div>
                    </div>
                    <div class="control-group" id="div5" style="float: left; border: 1px solid; padding: 5px 5px 0 5px; width: 100%"
                        runat="server">
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="rdoActive" runat="server" Text="ACTIVE" Checked="true" GroupName="SRGroup"
                                Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            &nbsp;&nbsp;
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="rdoInactive" runat="server" Text="IN ACTIVE" Font-Bold="True"
                                GroupName="SRGroup" Font-Overline="False" Font-Size="X-Small" ForeColor="Green" />
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="rdoAllMB" runat="server" Text="ALL" Font-Bold="True" GroupName="SRGroup"
                                Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-date-report-right40" id="divfilter" runat="server">
                <div class="barcode-header">
                    Filteration
                </div>
                <asp:Panel ID="PanelFilter" runat="server">
                    <div class="control-group" style="margin-top: 10px;" id="divLocation" >
                        <div style="float: left">
                            <asp:Label ID="Label51" runat="server" Text="Location"></asp:Label>
                        </div>
                        <div style="float: right">
                            <asp:DropDownList ID="DropDownLocation" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <%-- <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="Label17" runat="server" Text="Inventory"></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:DropDownList ID="rptInvDrop" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>--%>
                    <div class="control-group">
                        <div class="control-group" id="mType" visible="false" runat="server" style="display:none">
                            <div style="float: left">
                                <asp:Label ID="Label4" runat="server" Text="Member Type"></asp:Label>
                            </div>
                            <div style="float: right">
                                <asp:DropDownList ID="DropMemberType" runat="server" CssClass="form-control">
                                    <asp:ListItem></asp:ListItem> 
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group" id="mCode" visible="false" runat="server">
                            <div style="float: left">
                                <asp:Label ID="Label5" runat="server" Text="Member Code"></asp:Label>
                            </div>
                            <div style="float: right">
                                <asp:DropDownList ID="DropMemberCode" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="control-group" id="divDeptDrop">
                        <div style="float: left">
                            <asp:Label ID="Label2" runat="server" Text="Department"></asp:Label>
                        </div>
                        <div style="float: right">
                            <asp:DropDownList ID="rptDeptDrop" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group" id="divCatDrop">
                        <div style="float: left">
                            <asp:Label ID="Label1" runat="server" Text="Category"></asp:Label>
                        </div>
                        <div style="float: right">
                            <asp:DropDownList ID="rptCatDrop" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group" id="divBrandDrop">
                        <div style="float: left">
                            <asp:Label ID="Label3" runat="server" Text="Brand"></asp:Label>
                        </div>
                        <div style="float: right">
                            <asp:DropDownList ID="rptBrandDrop" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group" id="divVendorDrop">
                        <div style="float: left">
                            <asp:Label ID="Label6" runat="server" Text="Vendor"></asp:Label>
                        </div>
                        <div style="float: right">
                            <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group" id="divPinCode">
                        <div style="float: left">
                            <asp:Label ID="lblPinCode" runat="server" Text="PinCode"></asp:Label>
                        </div>
                        <div style="float: right">
                            <asp:DropDownList ID="ddPinCode" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="container-date-report-filter" id="divDateFilter" runat="server">
            <div class="barcode-header">
                Date filter
            </div>
            <asp:Panel ID="Panel7" runat="server">
                <div class="control-group" style="margin-top: 5px;">
                    <div style="float: left">
                        <asp:Label ID="Label7" runat="server" Text="From Date"></asp:Label>
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div style="float: left; margin-left: 5px">
                        <asp:Label ID="Label8" runat="server" Text="To Date"></asp:Label>
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div style="float: left; margin-top: 10px">
                        <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Style="margin-top: 20px"
                            Checked="true" Font-Bold="True" />
                    </div>
                    <div style="float: right; margin-top: 10px">
                        <asp:CheckBox ID="chkChart" runat="server" Text="With Chart" Font-Bold="True" Visible="false" />
                    </div>
                    <div class="col-md-12" style="display: none;" id="divTransaction">
                        <div class="barcode-header" style="margin-bottom: 10px;">
                            Group By
                        </div>
                        <div class="col-md-3">
                            <asp:RadioButton ID="rdoVendor" Checked ="true" runat="server" Class="radioboxlist" GroupName="Transaction" Text="Vendor" />
                        </div>
                        <div class="col-md-5">
                            <asp:RadioButton ID="rdoDept" runat="server" Class="radioboxlist" GroupName="Transaction" Text="Department" />
                        </div>
                        <div class="col-md-4">
                            <asp:RadioButton ID="rdoCat" runat="server" Class="radioboxlist" GroupName="Transaction" Text="Category" />
                        </div>
                        <div class="col-md-3">
                            <asp:RadioButton ID="rdoBrand" runat="server" Class="radioboxlist" GroupName="Transaction" Text="Brand" />
                        </div>
                        <div class="col-md-5">
                            <asp:RadioButton ID="rdoSub" runat="server" Class="radioboxlist" GroupName="Transaction" Text="SubCategory" />
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <div style="float: right; margin-top: 20px">
                        <asp:LinkButton ID="lnkFilter" runat="server" class="btn btn-danger" Width="100px"
                            Visible="false" Font-Bold="true" OnClick="lnkfilter_Click">Filter</asp:LinkButton>&nbsp;&nbsp;
                        <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Width="121px"
                            Font-Bold="true" OnClick="lnkLoadReport_Click" OnClientClick="return  ValidateForm()">Load Report</asp:LinkButton>&nbsp;&nbsp;
                        <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-danger" Width="100px"
                            Font-Bold="true" OnClientClick="return  Clearall()">Clear</asp:LinkButton>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <asp:HiddenField ID="HidenReportType" runat="server" Value="" />
        <%--<div class="panel panel-primary">
            <div class="panel-heading">
                Panel with panel-primary class</div>
            <div class="panel-body">
                Panel Content</div>
        </div>--%>
    </div>
</asp:Content>
