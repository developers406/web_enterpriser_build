﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmSalesPerformance.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmSalesPerformance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .process td:nth-child(1), .process th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .process td:nth-child(2), .process th:nth-child(2) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
        }

        .process td:nth-child(3), .process th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
            text-align: center;
        }

        .process td:nth-child(4), .process th:nth-child(4) {
            min-width: 77px;
            max-width: 77px;
            text-align: center;
        }

        .process td:nth-child(5), .process th:nth-child(5) {
            min-width: 60px;
            max-width: 60px;
            text-align: right;
        }

        .process td:nth-child(6), .process th:nth-child(6) {
            min-width: 60px;
            max-width: 60px;
            text-align: right;
        }

        .process td:nth-child(7), .process th:nth-child(7) {
            min-width: 70px;
            max-width: 70px;
            text-align: center;
        }
        .process td:nth-child(8), .process th:nth-child(8) {
            min-width: 70px;
            max-width: 70px;
            text-align: center;
        }
        .process td:nth-child(9), .process th:nth-child(9) {
            min-width: 70px;
            max-width: 70px;
            text-align: center;
        }

        .process td:nth-child(10), .process th:nth-child(10) {
            min-width: 70px;
            max-width: 70px;
            text-align: center;
        }
        .process td:nth-child(11), .process th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
            text-align: center;
        }
        .process td:nth-child(12), .process th:nth-child(12) {
            min-width: 110px;
            max-width: 110px;
            text-align: center;
        }

        process {
            padding: 2px;
        }

        .GVFixedFooter {
            background-color: #507CD1;
            color: White;
        }

        .SalesPerformance td:nth-child(1), .SalesPerformance th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }


        .SalesPerformance td:nth-child(2), .SalesPerformance th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .SalesPerformance td:nth-child(3), .SalesPerformance th:nth-child(3) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .SalesPerformance td:nth-child(4), .SalesPerformance th:nth-child(4) {
            min-width: 60px;
            max-width: 60px;
        }

        .SalesPerformance td:nth-child(5), .SalesPerformance th:nth-child(5) {
            min-width: 310px;
            max-width: 310px;
        }

        .SalesPerformance td:nth-child(6), .SalesPerformance th:nth-child(6) {
            min-width: 60px;
            max-width: 60px;
        }

        .SalesPerformance td:nth-child(6) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(7), .SalesPerformance th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .SalesPerformance td:nth-child(7) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(8), .SalesPerformance th:nth-child(8) {
            min-width: 90px;
            max-width: 90px;
        }

        .SalesPerformance td:nth-child(8) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(9), .SalesPerformance th:nth-child(9) {
            min-width: 90px;
            max-width: 90px;
        }

        .SalesPerformance td:nth-child(9) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(10), .SalesPerformance th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .SalesPerformance td:nth-child(10) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(11), .SalesPerformance th:nth-child(11) {
            min-width: 90px;
            max-width: 90px;
        }

        .SalesPerformance td:nth-child(11) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(12), .SalesPerformance th:nth-child(12) {
            min-width: 80px;
            max-width: 80px;
        }

        .SalesPerformance td:nth-child(12) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(13), .SalesPerformance th:nth-child(13) {
            min-width: 60px;
            max-width: 60px;
        }

        .SalesPerformance td:nth-child(13) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(14), .SalesPerformance th:nth-child(14) {
            min-width: 60px;
            max-width: 60px;
        }

        .SalesPerformance td:nth-child(14) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(15), .SalesPerformance th:nth-child(15) {
            min-width: 80px;
            max-width: 80px;
        }

        .SalesPerformance td:nth-child(15) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(16), .SalesPerformance th:nth-child(16) {
            min-width: 79px;
            max-width: 150px;
        }

        .SalesPerformance td:nth-child(17), .SalesPerformance th:nth-child(17) {
            min-width: 150px;
            max-width: 150px;
        }

        .SalesPerformance td:nth-child(18), .SalesPerformance th:nth-child(18) {
            min-width: 150px;
            max-width: 150px;
        }

        .SalesPerformance td:nth-child(19), .SalesPerformance th:nth-child(19) {
            min-width: 150px;
            max-width: 150px;
        }

        .SalesPerformance td:nth-child(20), .SalesPerformance th:nth-child(20) {
            min-width: 150px;
            max-width: 150px;
        }

        .SalesPerformance td:nth-child(21), .SalesPerformance th:nth-child(21) {
            min-width: 150px;
            max-width: 150px;
        }
        .SalesPerformance td:nth-child(22), .SalesPerformance th:nth-child(22) {
            min-width: 150px;
            max-width: 150px;
        }
        .SalesPerformance td:nth-child(23), .SalesPerformance th:nth-child(23) {
            min-width: 70px;
            max-width: 70px;
        }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'SalesPerformance');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "SalesPerformance";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
    <script type="text/javascript">
        $(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            //New Checkbox Date Picker
            $("#<%= txtFromDateNew.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDateNew.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDateNew.ClientID %>").val() === '') {
                $("#<%= txtFromDateNew.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDateNew.ClientID %>").val() === '') {
                $("#<%= txtToDateNew.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        });

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }

        };

        function ValidateForm() {
            var Show = '';


            if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose From date';
                $("#<%= txtFromDate.ClientID %>").focus();
            }

            if ($("#<%= txtToDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose To date';
                $("#<%= txtToDate.ClientID %>").focus();
            }

            if ($("#<%= cbNewItem.ClientID %>").is(':checked') == true) {

                if ($("#<%= txtFromDateNew.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose New From date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDateNew.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose New To date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {

                var StartDate = $("#<%= txtFromDate.ClientID %>").val();
                var EndDate = $("#<%= txtToDate.ClientID %>").val();

                // alert(StartDate);
                var eDate = new Date(EndDate);
                var sDate = new Date(StartDate);
                
                //  alert(sDate + '-'+ EndDate);
                if (StartDate == '' && EndDate == '') {
                    ShowPopupMessageBox("Please Ensure that the End Date is greater than or equal to the Start Date.");
                    //JSalerts("Please ensure that the End Date is greater than or equal to the Start Date.");
                    return false;
                }
                return true;
            }
        }

        function pageLoad() {
            var newOption = {};
            var option = '';
            $("#<%=ddlSort.ClientID %>").empty();
            $("[id*=tblTransfer] th").each(function (e) {
                var index = $(this).index();
                var table = $(this).closest('table');
                var val = table.find('.click th').eq(index).text();

                if ($(this).is(":visible")) {
                    option += '<option value="' + val + '">' + val + '</option>';
                }
            });
            $("#<%=ddlSort.ClientID %>").append(option);
            $("#<%=ddlSort.ClientID %>").prop('selectedIndex', 2);
            $("#<%=ddlSort.ClientID %>").trigger("liszt:updated");

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            $('#lblUp').on('click', function (event) {
                $('#lblDown').attr("disabled", false);
                $('#lblUp').attr("disabled", true);
                $('#lblDown').css("color", "");
                $('#lblUp').css("color", "green");
                var length = $('#<%=grdItemDetails.ClientID%>').length;
                var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex; //
                if (length > 0) {
                    var currentRow = $("[id*=grdItemDetails]  tr:last-child()");
                    var col1 = currentRow.find("td:eq(0)").text();
                    var col2 = currentRow.find("td:eq(1)").text();
                    var col3 = currentRow.find("td:eq(2)").text();
                    var col4 = currentRow.find("td:eq(3)").text();
                    var col5 = currentRow.find("td:eq(4)").text();
                    var col6 = currentRow.find("td:eq(5)").text();
                    var col7 = currentRow.find("td:eq(6)").text();
                    var col8 = currentRow.find("td:eq(7)").text();
                    var col9 = currentRow.find("td:eq(8)").text();
                    var col10 = currentRow.find("td:eq(9)").text();
                    var col11 = currentRow.find("td:eq(10)").text();
                    var col12 = currentRow.find("td:eq(11)").text();
                    var col13 = currentRow.find("td:eq(12)").text();
                    var col14 = currentRow.find("td:eq(13)").text();
                    var col15 = currentRow.find("td:eq(14)").text();
                    var col16 = currentRow.find("td:eq(15)").text();
                    var col17 = currentRow.find("td:eq(16)").text();
                    var col18 = currentRow.find("td:eq(17)").text();
                    var col19 = currentRow.find("td:eq(18)").text();
                    var col20 = currentRow.find("td:eq(19)").text();


                    var tr = '<tr class ="GVFixedFooter">';
                    tr += '<td>' + col1 + '</td>';
                    tr += '<td>' + col2 + '</td>';
                    tr += '<td>' + col3 + '</td>';
                    tr += '<td>' + col4 + '</td>';
                    tr += '<td>' + col5 + '</td>';
                    tr += '<td>' + col6 + '</td>';
                    tr += '<td>' + col7 + '</td>';
                    tr += '<td>' + col8 + '</td>';
                    tr += '<td>' + col9 + '</td>';
                    tr += '<td>' + col10 + '</td>';
                    tr += '<td>' + col11 + '</td>';
                    tr += '<td>' + col12 + '</td>';
                    tr += '<td>' + col13 + '</td>';
                    tr += '<td>' + col14 + '</td>';
                    tr += '<td>' + col15 + '</td>';
                    tr += '<td>' + col16 + '</td>';
                    tr += '<td>' + col17 + '</td>';
                    tr += '<td>' + col18 + '</td>';
                    tr += '<td>' + col19 + '</td>';
                    tr += '<td>' + col20 + '</td>';
                    tr += '</tr>';

                    $("[id*=grdItemDetails] tr:last").remove();
                    var tdArray = $('#<%=grdItemDetails.ClientID%>').closest("table").find("tr td:nth-child(" + (columnIndex + 1) + ")");

                    if (columnIndex == 2 || columnIndex == 13 || columnIndex == 14 || columnIndex == 15 || columnIndex == 16 || columnIndex == 17
                        || columnIndex == 18) {
                        tdArray.sort(function (p, n) {
                            var pData = $.trim($(p).text().toUpperCase());
                            var nData = $.trim($(n).text().toUpperCase());
                            return pData < nData ? -1 : 1;
                        });
                    }
                    else {
                        tdArray.sort(function (p, n) {
                            var pData = parseFloat($.trim($(p).text()));
                            var nData = parseFloat($.trim($(n).text()));
                            return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                        });
                    }
                    tdArray.each(function () {
                        var row = $(this).parent();
                        $('#<%=grdItemDetails.ClientID%>').append(row);
                    });
                    $("[id*=grdItemDetails] tr:last").after(tr);
                }
                else {
                    $('#lblDown').css("color", "");
                    $('#lblUp').css("color", "");
                    ShowPopupMessageBox("Invalid Records");
                    return false;
                }

            });
            $('#lblDown').on('click', function (event) {

                $('#lblDown').attr("disabled", true);
                $('#lblUp').attr("disabled", false);
                $('#lblDown').css("color", "green");
                $('#lblUp').css("color", "");

                var length = $('#<%=grdItemDetails.ClientID%>').length;
                var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex;

                if (length > 0) {
                    var currentRow = $("[id*=grdItemDetails]  tr:last-child()");
                    var col1 = currentRow.find("td:eq(0)").text();
                    var col2 = currentRow.find("td:eq(1)").text();
                    var col3 = currentRow.find("td:eq(2)").text();
                    var col4 = currentRow.find("td:eq(3)").text();
                    var col5 = currentRow.find("td:eq(4)").text();
                    var col6 = currentRow.find("td:eq(5)").text();
                    var col7 = currentRow.find("td:eq(6)").text();
                    var col8 = currentRow.find("td:eq(7)").text();
                    var col9 = currentRow.find("td:eq(8)").text();
                    var col10 = currentRow.find("td:eq(9)").text();
                    var col11 = currentRow.find("td:eq(10)").text();
                    var col12 = currentRow.find("td:eq(11)").text();
                    var col13 = currentRow.find("td:eq(12)").text();
                    var col14 = currentRow.find("td:eq(13)").text();
                    var col15 = currentRow.find("td:eq(14)").text();
                    var col16 = currentRow.find("td:eq(15)").text();
                    var col17 = currentRow.find("td:eq(16)").text();
                    var col18 = currentRow.find("td:eq(17)").text();
                    var col19 = currentRow.find("td:eq(18)").text();
                    var col20 = currentRow.find("td:eq(19)").text();


                    var tr = '<tr class ="GVFixedFooter">';
                    tr += '<td>' + col1 + '</td>';
                    tr += '<td>' + col2 + '</td>';
                    tr += '<td>' + col3 + '</td>';
                    tr += '<td>' + col4 + '</td>';
                    tr += '<td>' + col5 + '</td>';
                    tr += '<td>' + col6 + '</td>';
                    tr += '<td>' + col7 + '</td>';
                    tr += '<td>' + col8 + '</td>';
                    tr += '<td>' + col9 + '</td>';
                    tr += '<td>' + col10 + '</td>';
                    tr += '<td>' + col11 + '</td>';
                    tr += '<td>' + col12 + '</td>';
                    tr += '<td>' + col13 + '</td>';
                    tr += '<td>' + col14 + '</td>';
                    tr += '<td>' + col15 + '</td>';
                    tr += '<td>' + col16 + '</td>';
                    tr += '<td>' + col17 + '</td>';
                    tr += '<td>' + col18 + '</td>';
                    tr += '<td>' + col19 + '</td>';
                    tr += '<td>' + col20 + '</td>';
                    tr += '</tr>';
                    $("[id*=grdItemDetails] tr:last").remove();

                    var tdArray = $('#<%=grdItemDetails.ClientID%>').closest("table").find("tr td:nth-child(" + (columnIndex + 1) + ")");
                    if (columnIndex == 2 || columnIndex == 13 || columnIndex == 14 || columnIndex == 15 || columnIndex == 16 || columnIndex == 17
                        || columnIndex == 18) {
                        tdArray.sort(function (p, n) {
                            var pData = $.trim($(p).text().toUpperCase());
                            var nData = $.trim($(n).text().toUpperCase());
                            return pData > nData ? -1 : 1;
                        });
                    }
                    else {
                        tdArray.sort(function (p, n) {
                            var pData = parseFloat($.trim($(p).text()));
                            var nData = parseFloat($.trim($(n).text()));
                            return parseFloat(pData) > parseFloat(nData) ? -1 : 1;
                        });
                    }
                    tdArray.each(function () {
                        var row = $(this).parent();
                        $('#<%=grdItemDetails.ClientID%>').append(row);
                    });

                    $("[id*=grdItemDetails] tr:last").after(tr);

                }
                else {
                    $('#lblDown').css("color", "");
                    $('#lblUp').css("color", "");
                    ShowPopupMessageBox("Invalid Records");
                    return false;
                }
            });

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            fncDecimal();

            $('#divNewDate').hide();
            $("#<%= cbNewItem.ClientID %>").change(function () {

                if ($(this).is(':checked') == true) {
                    $('#divNewDate').show();
                }
                else {
                    $('#divNewDate').hide();
                }

            });

            $("[id*=grdItemDetails] tr").live('click', function (event) {
                $("[id*=grdItemDetails] tbody tr").closest('tr').css("background-color", "aliceblue");
                $(this).closest('tr').css("background-color", "#999999");
                $(this).closest('tr').focus(); 
                $('.GVFixedFooter').css("background-color", "#507CD1");
                //$(this).closest('tr').on("keydown", function (e) {
                //    var keyCode = (e.keyCode ? e.keyCode : e.which);
                //    alert(keyCode);
                //});

            });

        }



        function ShowPopup(name, h, w) {
            $("#divItemHistory").dialog({
                title: name,
                width: w,
                height: h,
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }

        function ShowPopupTrans(name) {
            $("#divtran").dialog({
                title: name,
                width: 700,
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }

        function fncHideFilter() {
            try {

                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                    $('#divBtn').attr('style', 'margin-top:0px !important;width: 284px;');
                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidth');
                    // $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:100%');
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'height:40%; width:100%');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").width("100%").height(200);
                    $("select").trigger("liszt:updated");
                }
                else {
                    //alert("check")
                    $('#divBtn').attr('style', 'margin-top:-50px !important;width: 284px;');
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidthShow');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:74%');
                    $("select").trigger("liszt:updated");
                }

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }
        function fncOpenAttributeDailog() {
            try {
                $("#divAttribute").dialog({
                    resizable: false,
                    height: 320,
                    width: 1200,
                    modal: true,
                    title: "Attribute Filter",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncDecimal() {
            try {
                $('#<%=txtNewItem.ClientID%>').number(true, 0);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncValidateExportNoItems() {
            try {
                var gridtr = $("#<%= grdItemDetails.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems

                    fncShowAlertNoItemsMessage();
                    return false;

                }
                else {
                    return true;

                }


            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncShowAlertNoItemsMessage() {
            try {
                InitializeDialogAlertNoItems();
                $("#AlertNoItems").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function InitializeDialogAlertNoItems() {
            try {
                $("#AlertNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        //Close Save Dialog
        function fncCloseAlertNoItemsDialog() {
            try {
                $("#AlertNoItems").dialog('close');
                return false;
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtItemName.ClientID %>').val($.trim(Description));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncVenItemRowdblClk(rowObj) {
            try {
                rowObj = $(rowObj);
                fncOpenItemhistory($.trim($("td", rowObj).eq(3).text()));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncOpenItemhistory(itemcode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            page = page + "?InvCode=" + itemcode + "&Status=dailog";
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 700,
                width: 1250,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }

        function fncShowSearchDialogInventory(event, Inventory, txtItemCode, txtItemName) {

            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 112) {
                    fncShowSearchDialogCommon(event, Inventory, 'txtItemCode', 'txtItemName');
                    event.preventDefault();
                }
                else if (keyCode == 13) {
                    event.preventDefault();
                    fncGetInventoryCode_Master($("#<%=txtItemCode.ClientID %>"));
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }

                else {
                    $("[id$=txtItemCode]").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                                data: "{ 'prefix': '" + request.term + "'}",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('|')[0],
                                            valitemcode: item.split('|')[1],
                                            valName: item.split('|')[2]
                                        }
                                    }))
                                },
                                error: function (response) {
                                    ShowPopupMessageBox(response.message);
                                },
                                failure: function (response) {
                                    ShowPopupMessageBox(response.message);
                                }
                            });
                        },
                        select: function (e, i) {
                            //alert(i.item.val);
                            $('#<%=txtItemCode.ClientID %>').val($.trim(i.item.valitemcode));
                            $('#<%=txtItemName.ClientID %>').val($.trim(i.item.valName));

                            fncGetInventoryCode_Master($("#<%=txtItemCode.ClientID %>"));

                            return false;
                        },
                        minLength: 1
                    });
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $("input").removeAttr('disabled');
            $("#<%= txtLocation.ClientID %>").val('HQ');
            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        }
       
        function fncCLickEvent(event, source,mode) {
            var keyCode = (event.keyCode ? event.keyCode : event.which);
            var rowObj = $(source).parent().parent();  
            
            var inv=$.trim($("td", rowObj).eq(3).text());
            try {
                var obj = {};
                
                if (mode == "1") {
                    $('#divIvoiceDetails').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 400,
                        width: 1030,
                        title: "Invoice Wise Details"
                    });
                }
                else {
                    $('#divDateDetails').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 400,
                        width: 500,
                        left:390,
                        title: "Date Wise Details"
                    });
                }
                
                
                obj.FromDate = $("#<%= txtFromDate.ClientID %>").val();
                obj.ToDate = $("#<%= txtToDate.ClientID %>").val();
                obj.LocationCode = $("#<%= txtLocation.ClientID %>").val();
                obj.InventoryCode = inv;
                obj.mode = mode;
                    $.ajax({
                        type: "POST",
                        url: "frmSalesPerformance.aspx/fncGetPendigDays",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (mode == "1") {
                                AssignTable(msg.d);
                            }
                            else {
                                AssignTableDate(msg.d);
                            }
                        },
                        error: function (data) {
                            ShowPopupMessageBox(data.message);
                        }

                    });
                   
                
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function AssignTableDate(jsonstring) {
            try {
                var rows;
                var r = 0;
                var tblProcessValue = $("#tblProcessValue1 tbody");
                var row = jQuery.parseJSON(jsonstring);
                if (jsonstring == "") {
                    ShowPopupMessageBox("No Record Found");
                    return;
                }
                tblProcessValue.children().remove();
                if (row.length > 0) {
                    for (var i = 0; i < row.length; i++) {
                        r++;
                        if ((row.length - 1) != i) {
                            rows = "<tr>"
                                + "<td id='tdrowno' >" + r + "</td>"
                               
                                + "<td id='tdinvoicedate' >" + row[i]["invoicedate"] + "</td>"
                               
                                + "<td id='tdqtysold' >" + row[i]["qtysold"] + "</td>"
                                + "<td id='tdsalesvalue' >" + row[i]["salesvalue"] + "</td>"
                                + "<td id='tddiscount' >" + row[i]["discount"] + "</td>"
                               

                                + "</tr>";
                            tblProcessValue.append(rows);
                        }
                        else {
                            rows = "<tr style='background:#507CD1;color:white'>"
                                + "<td id='tdrowno' >" + "" + "</td>"

                                + "<td id='tdinvoicedate' >" + row[i]["invoicedate"] + "</td>"

                                + "<td id='tdqtysold' >" + row[i]["qtysold"] + "</td>"
                                + "<td id='tdsalesvalue' >" + row[i]["salesvalue"] + "</td>"
                                + "<td id='tddiscount' >" + row[i]["discount"] + "</td>"


                                + "</tr>";
                            tblProcessValue.append(rows);
                        }

                    }
                    $('#divDateDetails').dialog('open');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function AssignTable(jsonstring) {
            try {
                var rows;
                var r = 0;
                var tblProcessValue = $("#tblProcessValue tbody");
                var row = jQuery.parseJSON(jsonstring);
                if (jsonstring == "") {
                    ShowPopupMessageBox("No Record Found");
                    return;
                }
                tblProcessValue.children().remove();
                if (row.length > 0) {
                    for (var i = 0; i <row.length; i++) {
                        r++;
                        if ((row.length - 1) != i) {
                            rows = "<tr>"
                                + "<td id='tdrowno' >" + r + "</td>"
                                + "<td id='tdinvoiceno' >" + row[i]["invoiceno"] + "</td>"
                                + "<td id='tdinvoicedate' >" + row[i]["invoicedate"] + "</td>"
                                + "<td id='tdcreatedate' >" + row[i]["createdate"] + "</td>"
                                + "<td id='tdqtysold' >" + row[i]["qtysold"] + "</td>"
                                + "<td id='tdsalesvalue' >" + row[i]["salesvalue"] + "</td>"
                                + "<td id='tddiscount' >" + row[i]["discount"] + "</td>"
                                + "<td id='tdMRP' >" + row[i]["MRP"] + "</td>"
                                + "<td id='tdSellingPrice' >" + row[i]["SellingPrice"] + "</td>"
                                + "<td id='tdNetCost' >" + row[i]["NetCost"] + "</td>"
                                + "<td id='tdcustomercode' >" + row[i]["customercode"] + "</td>"
                                + "<td id='tdcustomername' >" + row[i]["customername"] + "</td>"

                                + "</tr>";
                            tblProcessValue.append(rows);
                        }
                        else {
                            rows = "<tr style='background:#507CD1;color:white'>"
                                + "<td id='tdrowno' >" + "" + "</td>"
                                + "<td id='tdinvoiceno' >" + row[i]["invoiceno"] + "</td>"
                                + "<td id='tdinvoicedate' >" + row[i]["invoicedate"] + "</td>"
                                + "<td id='tdcreatedate' >" + row[i]["createdate"] + "</td>"
                                + "<td id='tdqtysold' >" + row[i]["qtysold"] + "</td>"
                                + "<td id='tdsalesvalue' >" + row[i]["salesvalue"] + "</td>"
                                + "<td id='tddiscount' >" + row[i]["discount"] + "</td>"
                                + "<td id='tdMRP' >" + "" + "</td>"
                                + "<td id='tdSellingPrice' >" + "" + "</td>"
                                + "<td id='tdNetCost' >" + "" + "</td>"
                                + "<td id='tdcustomercode' >" + row[i]["customercode"] + "</td>"
                                + "<td id='tdcustomername' >" + row[i]["customername"] + "</td>"

                                + "</tr>";
                            tblProcessValue.append(rows);
                        }
                       
                    }
                    $('#divIvoiceDetails').dialog('open');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>

    <style type="text/css">
        input:checked + label {
            color: white;
            background: red;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updtPnlTop" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li class="active-page">Sales Performance</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class ="EnableScroll">
                <div class="container-group-price">
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <div class="barcode-header">
                            Search By
                        </div>
                        <div class="control-group-single-res" style="margin-top: 10px">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtLocation');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Location"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtDepartment');"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Department"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Category"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                                <%-- <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Brand"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtSubCategory');"></asp:TextBox>
                                <%-- <asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Labelsc" runat="server" Text="SubCategory"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtMerchendise');"></asp:TextBox>
                                <%-- <asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Labelmer" runat="server" Text="Merchandise"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtMerchendise" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'txtMerchendise', 'txtManufacture');"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddlMerchandise" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Labelman" runat="server" Text="Manufacture"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'txtManufacture', 'txtFloor');"></asp:TextBox>
                                <%-- <asp:DropDownList ID="ddlManufacture" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Floor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Labelsec" runat="server" Text="Section"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtBin');"></asp:TextBox>
                                <%-- <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblBin" runat="server" Text="Bin"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', 'txtShelf');"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddlBin" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text="Shelf"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtWarwhouse');"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="LabelIT" runat="server" Text="WareHouse"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtWarwhouse" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse', 'txtWarwhouse', 'txtCustomer');"></asp:TextBox>
                                <%-- <asp:DropDownList ID="ddlwarehouse" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblCustomer" runat="server" Text="Customer"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCustomer" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'CUSTOMER', 'txtCustomer', 'txtSalesMan');"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddlCustomer" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblSalesman" runat="server" Text="Sales Man"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSalesMan" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Salesman', 'txtSalesMan', 'txtItemCode');"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddlSalesman" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text="Item Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogInventory(event, 'Inventory',  'txtItemCode', 'txtItemName');">
                                </asp:TextBox>
                                <%--<asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"></asp:TextBox>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label16" runat="server" Text="Item Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res">
                                </asp:TextBox>
                                <%--<asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"></asp:TextBox>--%>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblBatchNo" runat="server" Text="Batch No"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBatchNo" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'BatchNo',  'txtBatchNo', '');" MaxLength="15" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblclass" runat="server" Text="Class"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtclass" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Class',  'txtclass', '');" MaxLength="15" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                          <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblsubclass" runat="server" Text="SubClass"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubclass" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'SubClass',  'txtSubclass', '');" MaxLength="15" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res display_none">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="New Item"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:CheckBox ID="cbNewItem" runat="server" Class="radioboxlist" />
                            </div>
                        </div>
                        <div id="divNewDate">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label9" runat="server" Text="From Date"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtFromDateNew" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label10" runat="server" Text="To Date"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtToDateNew" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkLoadData" runat="server" class="button-red" OnClick="lnkLoadData_Click"
                                    OnClientClick="return ValidateForm()" Text="Fetch"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="clearForm();return false;" class="button-red" Text="Clear"></asp:LinkButton>
                            </div>
                        </div>

                    </div>
                    <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">

                        <div id="Div1" class="GridDetails" runat="server">
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <div class="col-md-12">
                                       <%-- <label id="lblF1" style="margin-top: 5px;">F1 -> To View/Close Invoice Wise Details</label>--%>
                                    </div>
                                    <div class="col-md-12">
                                        <%--<label id="lblF2" style="margin-top: 5px;">F2 -> To View/Close Date Wise Details</label>--%>
                                    </div>

                                </div>

                                <div class="col-md-3 sort_by">
                                    <div class="col-md-3">
                                        <label id="lblSortby" style="margin-top: 5px;">Sort by:</label>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:DropDownList runat="server" Width="100%" ID="ddlSort">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-1">
                                        <label id="lblUp" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label id="lblDown" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 sort_by">
                                <div class="row">
                                    <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                        <div class="grid-overflow" id="HideFilter_GridOverFlow" runat="server">
                                            <div class="over_flowhorizontal">
                                                <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header SalesPerformance">
                                                    <tr class="click">
                                                        <th>Inv.Det</th>
                                                        <th>D.Det</th>
                                                        <th>S.No</th>
                                                        <th>Itemcode</th>
                                                        <th>Description</th>
                                                        <th>MRP</th>
                                                        <th>Cost</th>
                                                        <th>Sellingprice</th>
                                                        <th>Stock</th>
                                                        <th>Margin Fixed</th>
                                                        <th>Sold Qty</th>
                                                        <th>Sold Value</th>
                                                        <th>TotalCost</th>
                                                        <th>ProfitAmt</th>
                                                        <th>Profit%</th>
                                                        <th>Avg.Qty</th>
                                                        <th>Department</th>
                                                        <th>Category</th>
                                                        <th>Brand</th>
                                                        <th>Vendor</th>
                                                        <th>Class</th>
                                                        <th>SubClass</th>
                                                        <th id="Batch" runat="server">BatcNho
                                                        </th>
                                                    </tr>
                                                </table>
                                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 390px; width: 2357px; background-color: aliceblue;">
                                                    <asp:GridView ID="grdItemDetails" EmptyDataRowStyle-CssClass="Emptyidclassforselector" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                                                        OnSorting="grdItemSales_Sorting" AllowSorting="true" OnRowDataBound="grdItemDetails_RowDataBound"
                                                        CssClass="SalesPerformance" OnRowCommand="grdItemDetails_RowCommand" ShowFooter="true">
                                                        <FooterStyle CssClass="GVFixedFooter" />
                                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                        <PagerSettings FirstPageText="First" LastPageText="Last" PageButtonCount="5" Position="Bottom" />
                                                        <PagerStyle Height="30px" VerticalAlign="Bottom" />
                                                        <PagerStyle CssClass="pshro_text" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Inv.Detail">
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btnInv" runat="server" OnClientClick="fncCLickEvent(event,this,'1');return false;" Text="Inv"/>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Inv.Detail">
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btndet" runat="server" OnClientClick="fncCLickEvent(event,this,'2');return false;" Text="Date"/>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                                            <asp:BoundField DataField="InventoryCode" HeaderText="Itemcode"></asp:BoundField>
                                                            <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                            <asp:BoundField DataField="MRP" HeaderText="MRP" />
                                                            <asp:BoundField DataField="NetCost" HeaderText="Cost"></asp:BoundField>
                                                            <asp:BoundField DataField="NetSellingPrice" HeaderText="Sellingprice"></asp:BoundField>
                                                            <asp:BoundField DataField="QtyOnHand" HeaderText="Stock"></asp:BoundField>
                                                            <asp:BoundField DataField="EarnedMargin" HeaderText="Margin Fixed"></asp:BoundField>
                                                            <asp:BoundField DataField="QtySold" HeaderText="Sold Qty"></asp:BoundField>
                                                            <asp:BoundField DataField="SalesValue" HeaderText="Sold Value"></asp:BoundField>
                                                            <asp:BoundField DataField="TotalCost" HeaderText="TotalCost"></asp:BoundField>
                                                            <asp:BoundField DataField="profitAmt" HeaderText="ProfitAmt"></asp:BoundField>
                                                            <asp:BoundField DataField="profitPer" HeaderText="Profit"></asp:BoundField>
                                                            <asp:BoundField DataField="AvgQty" HeaderText="Avg.Qty"></asp:BoundField>
                                                            <asp:BoundField DataField="DepartmentCode" HeaderText="Department"></asp:BoundField>
                                                            <asp:BoundField DataField="CategoryCode" HeaderText="Category"></asp:BoundField>
                                                            <asp:BoundField DataField="BrandCode" HeaderText="Brand"></asp:BoundField>
                                                            <asp:BoundField DataField="VendorCode" HeaderText="Vendor"></asp:BoundField>
                                                            <asp:BoundField DataField="Class" HeaderText="Class"></asp:BoundField>
                                                            <asp:BoundField DataField="SubClass" HeaderText="SubClass"></asp:BoundField>
                                                            <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="float_left" style="width: 100%">

                            <div class="cb_SalesPerformance">
                                <div class="Date_SalesPerformance">
                                    <asp:Label ID="Lblfrod" runat="server" Text="FromDate"></asp:Label>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>

                                </div>
                                <div class="Date_SalesPerformance">
                                    <asp:Label ID="lblto" runat="server" Text="ToDate"></asp:Label>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                                </div>
                                <div class="Date_SalesPerformance" style="width: 135px;">
                                    <div>
                                        <asp:RadioButton ID="rdoBestSeller" ForeColor="Green" Text="BEST SELL" runat="server" Class="radioboxlist"
                                            GroupName="RegularMenu" Checked="True" />
                                    </div>
                                    <div>
                                        <asp:RadioButton ID="rdoSlowMoving" ForeColor="red" Text="SLOW MOVING" runat="server" Class="radioboxlist"
                                            GroupName="RegularMenu" />
                                    </div>
                                </div>
                                <div class="Date_SalesPerformance" style="width: 142px;">
                                    <div>
                                        <asp:CheckBox ID="chkHistory" ForeColor="Green" Text="HISTORY" runat="server" Class="radioboxlist" />
                                    </div>
                                    <div>
                                        <asp:CheckBox ID="chkConsolidated" ForeColor="Red" Text="CONSOLIDATED" runat="server" Class="radioboxlist" />
                                    </div>
                                    <div>
                                        <asp:CheckBox ID="chkPurchase" ForeColor="Green" Text="PURCHASE" Visible="false" runat="server" Class="radioboxlist" />
                                    </div>
                                </div>
                                <div class="Date_SalesPerformance" style="width: 142px;">
                                    <div>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtNewItem" Text="0" runat="server" MaxLength="3" CssClass="form-control-res-right" Style="width: 55%"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <div id="divItemHistory" style="display: none;">
                    <asp:GridView ID="grdItemSales" runat="server" CellPadding="10" AutoGenerateColumns="true"
                        AllowSorting="true" ForeColor="#333333">
                        <Columns>
                        </Columns>
                        <AlternatingRowStyle BackColor="White" />
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <RowStyle BackColor="#FFFBD6" BorderColor="Black" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <SortedAscendingCellStyle BackColor="#FDF5AC" />
                        <SortedAscendingHeaderStyle BackColor="#4D0000" />
                        <SortedDescendingCellStyle BackColor="#FCF6C0" />
                        <SortedDescendingHeaderStyle BackColor="#820000" />
                    </asp:GridView>
                </div>
                    </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="InkExport" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="col-md-12">
        <div class="float_right" id="divBtn">
            <div class="control-button">
                <asp:LinkButton ID="InkExport" runat="server" class="button-blue" OnClick="btnExportToExcel_Click" OnClientClick="return fncValidateExportNoItems();"
                    Text='Export'></asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkFilterOption" runat="server" OnClientClick="return fncHideFilter()" Text='<%$ Resources:LabelCaption,btn_hide_filter %>'
                    class="button-blue"></asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkAttribute" runat="server" class="button-blue" OnClientClick="fncOpenAttributeDailog();return false;">Attributes</asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_export %>'
                    Visible="False"></asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue"
                    Text='Clear' OnClick="lnkClearAll_Click"></asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_print %>'></asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="display_none">
        <div class="container welltextiles" id="divAttribute">
            <div class="barcode-header" style="background-color: #c6b700">
                Attributes
            </div>
            <div style="height: auto; border: solid; overflow-x: visible; overflow-y: hidden; height: 215px;">
                <asp:GridView ID="grdAttribute" runat="server" AutoGenerateColumns="False"
                    ShowHeaderWhenEmpty="true" CssClass="fixed_header_Empty_grid"
                    EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                    <EmptyDataTemplate>
                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                    <RowStyle CssClass="pshro_GridDgnStyle" />
                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                    <PagerStyle CssClass="pshro_text" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <Columns>
                    </Columns>
                </asp:GridView>
            </div>
            <br />
            <div class="col-md-2">
            </div>
            <div class="col-md-9">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="AlertNoItems">
            <p>
                <%=Resources.LabelCaption.Alert_No_Items%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="hidden">
        <div class="col-md-12" id ="divIvoiceDetails">
            <div class="dialog-inv-container-set-price-header">
                Invoice Wise Particulars
            </div><br />
            <div class="process Vendor_margin_border">
                <div class="col-md-12" >
                    <table id="tblProcessValuehead" cellspacing="0" rules="all" border="1" class="fixed_header">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                <th scope="col">Invoice No
                                </th>
                                <th scope="col">InvoiceDate
                                </th>
                                <th scope="col">InvoiceTime
                                </th>
                                <th scope="col">Sold Qty
                                </th>
                                <th scope="col">SoldValue
                                </th>
                                <th scope="col">Discount
                                </th>
                                <th scope="col">MRP
                                </th>
                                <th scope="col">S.Price
                                </th>
                                <th scope="col">NetCost
                                </th>
                                <th scope="col">CustomerCode
                                </th>
                                <th scope="col">CustomerName
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-md-12" style="overflow-y: scroll; overflow-x: hidden; height: 236px; width: 100% !important">
                    <table id="tblProcessValue" cellspacing="0" rules="all" border="1">
                        <tbody></tbody>
                    </table>

                </div>
            </div>
        </div>
    </div> 
    <div class="hidden">
        <div class="col-md-12" id ="divDateDetails">
            <div class="dialog-inv-container-set-price-header">
                Date Wise Particulars
            </div><br />
            <div class="process Vendor_margin_border">
                <div class="col-md-12" >
                    <table id="tblProcessValuehead1" cellspacing="0" rules="all" border="1" class="fixed_header">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                
                                <th scope="col">InvoiceDate
                                </th>
                                
                                <th scope="col">Sold Qty
                                </th>
                                <th scope="col">SoldValue
                                </th>
                                <th scope="col">Discount
                                </th>
                              
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-md-12" style="overflow-y: scroll; overflow-x: hidden; height: 236px; width: 100% !important">
                    <table id="tblProcessValue1" cellspacing="0" rules="all" border="1">
                        <tbody></tbody>
                    </table>

                </div>
            </div>
        </div>
    </div> 
</asp:Content>
