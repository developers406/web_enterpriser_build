﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="frmVoucherCreation.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmVoucherCreation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
  </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'VoucherCreation');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "VoucherCreation";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
                else if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    e.preventDefault();
                }
        }
    };

    $(function () {
        SetAutoComplete();
    });

    function Padder(len, pad) {
        if (len === undefined) {
            len = 1;
        } else if (pad === undefined) {
            pad = '0';
        }

        var pads = '';
        while (pads.length < len) {
            pads += pad;
        }

        this.pad = function (what) {
            var s = what.toString();
            return pads.substring(0, pads.length - s.length) + s;
        };
    }

    function fncClose() {

        $('#<%=divVouchertransfer.ClientID %>').hide();
        $('#<%=divVoucherCreation.ClientID %>').show();
        return false;
    }
    function fncTransferBook() {

        $('#<%=divVouchertransfer.ClientID %>').show();
            $('#<%=divVoucherCreation.ClientID %>').hide();
            return false;
        }

        function DeleteConfirmbox() {

            if ($('#<%=txtBookNo.ClientID %>').val() == '') {

                alert('Book No should not be Empty !');
                $('#<%=txtBookNo.ClientID %>').focus().select();
                return false;
            }

            var sBookno = $('#<%=txtBookNo.ClientID %>').val();
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(sBookno + ' - ' + 'Are you sure delete Gift Voucher Book? ')) {
                confirm_value.value = "Yes";

            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);

            // confirm meg check for delete
            //                var sBookno = $('#<%=txtBookNo.ClientID %>').val();
            //                var result = confirm(sBookno + ' - ' + 'Are you sure delete Gift Voucher Book? ');
            //                if (result) {
            //                    try {
            //                        debugger;
            //                        $.ajax({
            //                            type: "POST",
            //                            url: '<%=ResolveUrl("~/Managements/frmVoucherCreation.aspx/DeleteGiftVoucher")%>',
            //                            data: "{'bookno':'" + sBookno + "'}",
            //                            contentType: "application/json; charset=utf-8",
            //                            dataType: "json",
            //                            success: OnSuccess,
            //                            error: function (data) {
            //                            }
            //                        });
            //                    }
            //                    catch (err) {
            //                        alert(err.Message);
            //                    }

            //                    return false;
            //                }
        }

        // function OnSuccess  
        function OnSuccess(response) {

            return false;
            alert(response.d);
            if (response.d == 'true') {
                alert("Deleted Successfully!");
            }
        }

        function SetAutoComplete() {

            $("[id$=txtBookNo]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Managements/frmVoucherCreation.aspx/GetGiftVoucherDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    //alert(i.item.val);  
                    $('#<%=txtBookNo.ClientID %>').val(i.item.val);
                    $('#<%=lnkSave.ClientID %>').hide();
                    $('#<%=txtValue.ClientID %>').focus();
                    //$('#<%=txtValue.ClientID %>').focus();
                    //__doPostBack('ctl00$ContentPlaceHolder1$lnkLoadBookList', ''); 

                    return false;

                },
                minLength: 0
            });
        }

        function ValidateForm() {
            try {

                if ($('#<%=txtBookNo.ClientID %>').val() == '') {

                    alert("Sorry BookNo Can't be Empty");
                    $('#<%=txtBookNo.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtDenomination.ClientID %>').val() == '') {

                    alert("Sorry Denomination Can't be Empty");
                    $('#<%=txtDenomination.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtValue.ClientID %>').val() == '' || $('#<%=txtValue.ClientID %>').val() == 0) {

                    alert("Sorry Value Can't be Empty");
                    $('#<%=txtValue.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtPrefix.ClientID %>').val() == '') {

                    alert("Sorry Prefix Can't be Empty");
                    $('#<%=txtPrefix.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtStartNo.ClientID %>').val() == '') {

                    alert("Sorry StartNo Can't be Empty");
                    $('#<%=txtStartNo.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtQty.ClientID %>').val() == '') {

                    alert("Sorry Quantity Can't be Empty");
                    $('#<%=txtQty.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtValidity.ClientID %>').val() == '') {

                    alert("Sorry Validity Can't be Empty");
                    $('#<%=txtValidity.ClientID %>').focus().select();
                    return false;
                }

                return true;
            }
            catch (err) {
                alert(err.message);
                return false;
            }

        }

        function ValidateTransfer() {
            try {

                if ($('#<%=txtLocation.ClientID %>').val() == '') {
                    alert("Sorry Location Can't be Empty");
                    $('#<%=txtLocation.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=BookNoDropDown.ClientID %>').val() == '') {
                    alert("Sorry BookNo Can't be Empty");
                    $('#<%=BookNoDropDown.ClientID %>').trigger("liszt:updated");
                    $('#<%=BookNoDropDown.ClientID %>').trigger("liszt:open");
                    return false;
                }
                if ($('#<%=txtTransDenomination.ClientID %>').val() == '') {
                    alert("Sorry Denomination Can't be Empty");
                    $('#<%=txtTransDenomination.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtTransStartNo.ClientID %>').val() == '') {
                    alert("Sorry Start No Can't be Empty");
                    $('#<%=txtTransStartNo.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtTransEndNo.ClientID %>').val() == '') {
                    alert("Sorry End No Can't be Empty");
                    $('#<%=txtTransEndNo.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtTransQty.ClientID %>').val() == '') {
                    alert("Sorry Quantity Can't be Empty");
                    $('#<%=txtQty.ClientID %>').focus().select();
                    return false;
                }

                return true;
            }
            catch (err) {
                alert(err.message);
                return false;
            }

        }

        function fncGenVoucherNo(Qty) {
            var Qty = $('#<%=txtQty.ClientID%>').val()
            if (Qty == 0) {
                alert('Invalid Quantity!')
                $('#<%=txtQty.ClientID %>').focus();
                return false;
            }
            var StartNo = $('#<%=txtStartNo.ClientID%>').val();
            var Prefix = $('#<%=txtPrefix.ClientID%>').val().toUpperCase();
            if (!StartNo.search('GV')) {
                StartNo = StartNo.substr(StartNo.length - 8)
                //alert(StartNo);
            }
            var EndNo = parseInt(StartNo) + parseInt(Qty);
            //alert(EndNo);
            var zero4 = new Padder(8);
            var Stcode = zero4.pad(StartNo);
            var Endcode = zero4.pad(EndNo - 1);
            //alert(Endcode);
            //alert(code);
            StartNo = "GV" + Prefix + Stcode;
            Endcode = "GV" + Prefix + Endcode;
            $('#<%=txtStartNo.ClientID%>').val(StartNo);
            $('#<%=txtEndNo.ClientID%>').val(Endcode);

            fncGenVoucherNoList();
        }

        function fncGenVoucherNoList() {

            var Qty = $('#<%=txtQty.ClientID%>').val()
            var StartNo = $('#<%=txtStartNo.ClientID%>').val();
            var Prefix = $('#<%=txtPrefix.ClientID%>').val().toUpperCase();
            if (!StartNo.search('GV')) {
                StartNo = StartNo.substr(StartNo.length - 8)
            }

            var arr = [];
            var sGVNo;
            for (var i = 1; i <= Qty; i++) {
                //alert(i);
                var zero4 = new Padder(8);
                var Stcode = zero4.pad(StartNo);
                sGVNo = "GV" + Prefix + Stcode;
                arr.push(sGVNo);
                StartNo = parseInt(StartNo) + 1;
            }
            alert(arr);
            $("#HidenGVNumbers").val(arr);

        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function isAlfa(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
                return false;
            }
            $('#<%=txtStartNo.ClientID%>').val('');
            $('#<%=txtEndNo.ClientID%>').val('');
            $('#<%=txtQty.ClientID%>').val('');
            return true;
        }

        $(function () {

            $("#<%= BookNoDropDown.ClientID %>").change(function () {
                var status = this.value;
                var sDvalue = $('#<%=BookNoDropDown.ClientID%>').val();
                $('#<%=txtTransDenomination.ClientID%>').val(sDvalue);

                return false;
            });


        });

        $(function () {

            $('#<%=txtBookNo.ClientID %>').focusout(function () {
                if ($('#<%=txtBookNo.ClientID%>').val() != '') {
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkLoadBookList', this);
                }

                return false;
            });

        });


        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
                $('#<%=lnkBookTransfer.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
                $('#<%=lnkBookTransfer.ClientID %>').css("display", "none");
            }

            if ($('#<%=hidDeletebtn.ClientID%>').val() == "D1") {
                $('#<%=lnkDelete.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkDelete.ClientID %>').css("display", "none");
             }

            //            if ($('#<%=divVouchertransfer.ClientID %>').show())
            //                $('#<%=divVouchertransfer.ClientID %>').show();

            $('#<%=txtStartNo.ClientID %>').focusout(function () {
                if (StartNo == '') {
                    alert('Please Enter StartNo');
                    $('#<%=txtStartNo.ClientID %>').focus();
                    return false;
                }

                // var zero4 = new Padder(4);
                // zero4.pad(12); // "0012"
                // zero4.pad(12345); // "12345"
                // zero4.pad("xx"); // "00xx"
                // var x3 = new Padder(3, "x");
                // x3.pad(12); // "x12" 

            });


            $('#<%=txtQty.ClientID %>').focusout(function () {

                if ($('#<%=txtStartNo.ClientID%>').val() == '') {
                    alert('Please Enter StartNo');
                    return false;
                }
                else if ($('#<%=txtQty.ClientID%>').val() == '') {
                    alert('Please Enter Qunatity');
                    return false;
                }

                if ($('#<%=txtQty.ClientID%>').val() != '') {
                    fncGenVoucherNo();
                }
            });
        }


        function Clearall() {

            $("select").val(0);
            $("select").trigger("liszt:updated");
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $("table[id$='grdGiftVoucher']").html("");
            $('#<%=lnkSave.ClientID %>').show();

            return false;
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Denamination") {
                    $('#<%=txtDenomination.ClientID %>').val($.trim(Description));
                    $('#<%=txtValue.ClientID %>').val($.trim(Code));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Gift Voucher Book Creation </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-pc">
            <div class="right-container-top-distribution">
                <div class="container-left-price" id="divVoucherCreation" runat="server">
                    <div class="right-container-top-header">
                        Gift Voucher Book Creation
                    </div>
                    <div class="control-group-single-res" style="margin-top: 15px">
                        <div class="label-left">
                            <asp:Label ID="Label1" runat="server" Text="BookNo"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtBookNo" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'BOOKNO',  'txtBookNo', 'txtDenomination');" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label4" runat="server" Text="Denomination"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="DenominationDropDown" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtDenomination" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Denamination', 'txtDenomination', '');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label10" runat="server" Text="Value"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtValue" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label12" runat="server" Text="Prefix"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtPrefix" runat="server" CssClass="form-control-res" Style="text-transform: uppercase"
                                onkeypress="return isAlfa(event)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label11" runat="server" Text="Start No"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtStartNo" runat="server" CssClass="form-control-res" onkeypress="return isNumber(event)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label14" runat="server" Text="Quantity"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res" onkeypress="return isNumber(event)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label13" runat="server" Text="End No"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtEndNo" runat="server" CssClass="form-control-res" onkeypress="return isNumber(event)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label15" runat="server" Text="Validity (Days)"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtValidity" runat="server" CssClass="form-control-res" onkeypress="return isNumber(event)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="button-contol" style="margin-top: 20px">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkDelete" runat="server" class="button-red" OnClientClick="return DeleteConfirmbox()"
                                Text='<%$ Resources:LabelCaption,btn_delete %>' OnClick="lnkDelete_Click"><i class="icon-play"></i></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkBookTransfer" runat="server" class="button-red" OnClientClick="return fncTransferBook()"><i class="icon-play"></i>Book Transfer</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return ValidateForm()"
                                Text='<%$ Resources:LabelCaption,btnsave %>' OnClick="lnkSave_Click"><i class="icon-play"></i></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="container-left-price" runat="server" id="divVouchertransfer" style="display: none">
                    <div class="right-container-top-header">
                        Gift Voucher Transfer
                    </div>
                    <div class="control-group-single-res" style="margin-top: 15px">
                        <div class="label-left">
                            <asp:Label ID="Label3" runat="server" Text="Location"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="LocationDropDown" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label5" runat="server" Text="Book No."></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:DropDownList ID="BookNoDropDown" runat="server" CssClass="form-control-res"
                                AutoPostBack="true" OnSelectedIndexChanged="BookNoDropDown_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label6" runat="server" Text="Denomination"></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--  <asp:DropDownList ID="TranDenomDropDown" runat="server" AutoPostBack="true" CssClass="form-control-res"
                                OnSelectedIndexChanged="TranDenomDropDown_SelectedIndexChanged">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtTransDenomination" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label9" runat="server" Text="Start No"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtTransStartNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label17" runat="server" Text="End No"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtTransEndNo" runat="server" CssClass="form-control-res" onkeypress="return isNumber(event)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label16" runat="server" Text="Quantity"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtTransQty" runat="server" CssClass="form-control-res" onkeypress="return isNumber(event)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="lblFromNo" Visible="false" runat="server" Text="From No"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:Label ID="lblToNo" Visible="false" runat="server" Text="To No"></asp:Label>
                        </div>
                    </div>
                    <div class="button-contol" style="margin-top: 20px">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkTransfer" runat="server" class="button-red"
                                OnClientClick="return ValidateTransfer()" OnClick="lnkTransfer_Click"><i class="icon-play"></i>Transfer</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-red" OnClientClick="return fncClose()"><i class="icon-play"></i>Close</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="container-right-price">
                    <%--<div class="right-container-top-header">
                        Gift Voucher Book Details
                    </div>--%>
                    <div class="grid-overflow-Invchange" id="HideFilter_GridOverFlow" runat="server">
                        <asp:GridView ID="grdGiftVoucher" runat="server" AutoGenerateColumns="true" PageSize="14"
                            ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgn">
                            <PagerStyle CssClass="pshro_text" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                            <Columns>
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                PageButtonCount="5" Position="Bottom" />
                            <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                        </asp:GridView>
                    </div>
                </div>
                <div class="button-contol">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return Clearall()"
                            Text='<%$ Resources:LabelCaption,btnclear %>'><i class="icon-play"></i></asp:LinkButton>
                    </div>
                    <%--               <div class="control-button">
                    <asp:LinkButton ID="lnkDiscoupon" runat="server" class="button-red"><i class="icon-play"></i>Discount Coupon</asp:LinkButton>
                </div>--%>
                    <%--               <div class="control-button">
                    <asp:LinkButton ID="lnkBarcode" runat="server" class="button-red"><i class="icon-play" ></i>Barcode</asp:LinkButton>
                </div>--%>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkLoadBookList" Visible="false" runat="server" class="button-red"
                            OnClick="lnkLoadBookList_Click"><i class="icon-play"></i>Book List</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="HidenGVNumbers" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>
