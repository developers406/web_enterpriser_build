﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPurchaseSalesStock.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmPurchaseSalesStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .SalesPerformance td:nth-child(1), .SalesPerformance th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .SalesPerformance td:nth-child(2), .SalesPerformance th:nth-child(2) {
            min-width: 200px;
            max-width: 200px;
        }

        .SalesPerformance td:nth-child(3), .SalesPerformance th:nth-child(3) {
            min-width: 310px;
            max-width: 310px;
        }

        .SalesPerformance td:nth-child(4), .SalesPerformance th:nth-child(4) {
            min-width: 110px;
            max-width: 110px;
        }

        .SalesPerformance td:nth-child(4) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(5), .SalesPerformance th:nth-child(5) {
            min-width: 110px;
            max-width: 110px;
        }

        .SalesPerformance td:nth-child(5) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(6), .SalesPerformance th:nth-child(6) {
            min-width: 110px;
            max-width: 110px;
        }

        .SalesPerformance td:nth-child(6) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(7), .SalesPerformance th:nth-child(7) {
            min-width: 90px;
            max-width: 90px;
        }

        .SalesPerformance td:nth-child(7) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(8), .SalesPerformance th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .SalesPerformance td:nth-child(8) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(9), .SalesPerformance th:nth-child(9) {
            min-width: 90px;
            max-width: 90px;
        }

        .SalesPerformance td:nth-child(9) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(10), .SalesPerformance th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .SalesPerformance td:nth-child(10) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(11), .SalesPerformance th:nth-child(11) {
            min-width: 110px;
            max-width: 110px;
        }

        .SalesPerformance td:nth-child(11) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(12), .SalesPerformance th:nth-child(12) {
            min-width: 110px;
            max-width: 110px;
        }

        .SalesPerformance td:nth-child(12) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(13), .SalesPerformance th:nth-child(13) {
            min-width: 80px;
            max-width: 80px;
        }

        .SalesPerformance td:nth-child(13) {
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(14), .SalesPerformance th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(15), .SalesPerformance th:nth-child(15) {
             min-width: 110px;
            max-width: 110px;
            text-align: right !important;
        }

        .SalesPerformance td:nth-child(16), .SalesPerformance th:nth-child(16) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;   
        }

        .SalesPerformance td:nth-child(17), .SalesPerformance th:nth-child(17) {
             min-width: 110px;
            max-width: 110px;
            text-align: right !important;
        }
         
    </style>
    <script type="text/javascript">
        function pageLoad() {
            try {
                fncDate();
                if ($("#<%= hidUserCode.ClientID %>").val() !='ADMIN') {
                    $('.SalesPerformance td:nth-child(13)').css('display','none');
                    $('.SalesPerformance th:nth-child(13)').css('display', 'none');
                    $('.SalesPerformance td:nth-child(14)').css('display', 'none');
                    $('.SalesPerformance th:nth-child(14)').css('display', 'none');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncDate() {
            try {
                if ($("#<%= txtSaleFromDate.ClientID %>").val() != '') {
                    $("#<%= txtSaleFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                    $("#<%= txtSalesToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                    $("#<%= txtPurchaseFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                    $("#<%= txtPurchaseToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                }
                else {
                    $("#<%= txtSaleFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                    $("#<%= txtSalesToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                    $("#<%= txtPurchaseFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                    $("#<%= txtPurchaseToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtItemName.ClientID %>').val($.trim(Description));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function ValidateForm() {
            if ($('#<%=txtPurchaseFromDate.ClientID %>').val() == '' || $('#<%=txtPurchaseToDate.ClientID %>').val() == '' ||
                $('#<%=txtSaleFromDate.ClientID %>').val() == '' || $('#<%=txtSalesToDate.ClientID %>').val() == '') {
                ShowPopupMessageBox('Please Enter Valid Date');
                return false;
            }
            return true;
        }

        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        }

        function fncChange(value) { 
            if (value == 'PFrom') {
                setTimeout(function () {
                    $('#<%=txtPurchaseToDate.ClientID %>').focus();
                }, 50);
            }
            else if (value == 'PTo') {
                setTimeout(function () {
                    $('#<%=txtSaleFromDate.ClientID %>').focus();
                }, 50);
            }
            else if (value == 'SFrom') {
                setTimeout(function () {
                    $('#<%=txtSalesToDate.ClientID %>').focus();
                }, 50);
            }
            else if (value == 'STo') {
                setTimeout(function () {
                    $('#<%=ddlGroupBy.ClientID %>').focus();
                }, 50);
            }

        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updtPnlTop" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Purchase Sales Stock</li>
                        <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="EnableScroll">
                    <div class="container-group-price">
                        <div class="container-left-price" id="pnlFilter" runat="server">
                            <div class="barcode-header">
                                Search By
                            </div>
                            <div class="control-group-single-res" style="margin-top: 10px">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtLocation');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label6" runat="server" Text="Location"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtDepartment');"></asp:TextBox>
                                    <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label3" runat="server" Text="Department"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                                    <%--<asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text="Category"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                                    <%-- <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label5" runat="server" Text="Brand"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtSubCategory');"></asp:TextBox>
                                    <%-- <asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Labelsc" runat="server" Text="SubCategory"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtMerchendise');"></asp:TextBox>
                                    <%-- <asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Labelmer" runat="server" Text="Merchandise"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtMerchendise" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'txtMerchendise', 'txtManufacture');"></asp:TextBox>
                                    <%--<asp:DropDownList ID="ddlMerchandise" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Labelman" runat="server" Text="Manufacture"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'txtManufacture', 'txtFloor');"></asp:TextBox>
                                    <%-- <asp:DropDownList ID="ddlManufacture" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label8" runat="server" Text="Floor"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                                    <%--<asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Labelsec" runat="server" Text="Section"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtBin');"></asp:TextBox>
                                    <%-- <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="lblBin" runat="server" Text="Bin"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', 'txtShelf');"></asp:TextBox>
                                    <%--<asp:DropDownList ID="ddlBin" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label12" runat="server" Text="Shelf"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtWarwhouse');"></asp:TextBox>
                                    <%--<asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="LabelIT" runat="server" Text="WareHouse"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtWarwhouse" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse', 'txtWarwhouse', 'txtType');"></asp:TextBox>
                                    <%-- <asp:DropDownList ID="ddlwarehouse" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="lblCustomer" runat="server" Text="Item Type"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtType" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'ItemType', 'txtType', 'txtItemCode');"></asp:TextBox> 
                                </div>
                            </div> 
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label15" runat="server" Text="Item Code"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', 'txtItemName');">
                                    </asp:TextBox>
                                    <%--<asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"></asp:TextBox>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label16" runat="server" Text="Item Name"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtItemName" runat="server"    CssClass="form-control-res">
                                    </asp:TextBox>
                                    <%--<asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"></asp:TextBox>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="lblBatchNo" runat="server" Text="Batch No"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtBatchNo" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'BatchNo',  'txtBatchNo', '');" MaxLength="15" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res display_none">
                                <div class="label-left">
                                    <asp:Label ID="Label7" runat="server" Text="New Item"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:CheckBox ID="cbNewItem" runat="server" Class="radioboxlist" />
                                </div>
                            </div>
                            <div id="divNewDate">
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label9" runat="server" Text="PurchaseDate"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtPurchaseFromDate" runat="server" CssClass="form-control-res" onchange ="fncChange('PFrom');return false;"></asp:TextBox>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtPurchaseToDate" runat="server" CssClass="form-control-res" onchange ="fncChange('PTo');return false;"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label10" runat="server" Text="SalesDate"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtSaleFromDate" runat="server" CssClass="form-control-res" onchange ="fncChange('SFrom');return false;"></asp:TextBox>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtSalesToDate" runat="server" CssClass="form-control-res" onchange ="fncChange('STo');return false;"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label2" runat="server" Text="Group by"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlGroupBy" runat="server" CssClass="form-control-res">
                                            <asp:ListItem Value="">--SELECT--</asp:ListItem>
                                             <asp:ListItem Value="Location">Location</asp:ListItem>
                                             <asp:ListItem Value="Vendor">Vendor</asp:ListItem>
                                             <asp:ListItem Value="Department">Department</asp:ListItem>
                                             <asp:ListItem Value="Category">Category</asp:ListItem>
                                             <asp:ListItem Value="SubCategory">SubCategory</asp:ListItem>
                                             <asp:ListItem Value="Brand">Brand</asp:ListItem>
                                             <asp:ListItem Value="Merchendise">Merchendise</asp:ListItem>
                                             <asp:ListItem Value="Manufacture">Manufacture</asp:ListItem>
                                             <asp:ListItem Value="ItemCode">ItemCode</asp:ListItem>
                                             <asp:ListItem Value="Bulk">Bulk</asp:ListItem>
                                             <asp:ListItem Value="Regular">Regular</asp:ListItem>
                                             <asp:ListItem Value="Repack">Repack</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="control-container">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkLoadData" runat="server" class="button-red" OnClick="lnkFilter_Click"
                                        OnClientClick="return ValidateForm()" Text="Fetch"></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="clearForm();return false;" class="button-red" Text="Clear"></asp:LinkButton>
                                </div>
                            </div>

                        </div>
                        <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">

                            <div id="Div1" class="GridDetails" runat="server">
                                <div class="col-md-12" style="display:none;">
                                    <div class="col-md-9">
                                    </div>
                                    <div class="col-md-3 sort_by">
                                        <div class="col-md-3">
                                            <label id="lblSortby" style="margin-top: 5px;">Sort by:</label>
                                        </div>
                                        <div class="col-md-7">
                                            <asp:DropDownList runat="server" Width="100%" ID="ddlSort">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-1">
                                            <label id="lblUp" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                                        </div>
                                        <div class="col-md-1">
                                            <label id="lblDown" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 sort_by">
                                    <div class="row">
                                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                            <div class="grid-overflow" id="HideFilter_GridOverFlow" runat="server">
                                                <div class="over_flowhorizontal">
                                                    <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header SalesPerformance">
                                                        <tr class="click">
                                                            <th>S.No</th>
                                                            <th>Code</th>
                                                            <th>Description</th>
                                                            <th>MRP</th>
                                                            <th>Purchased Qty</th>
                                                            <th>Purchase Value</th>
                                                            <th>Sold Qty</th>
                                                            <th>Sales Value</th>
                                                            <th>NetCost</th>
                                                            <th>Stock Qty</th>
                                                            <th>Stock Value</th>
                                                            <th>Total Cost</th>
                                                            <th>Profit</th>
                                                            <th>Profit %</th>
                                                            <th>Purchase Rate</th>
                                                            <th>Sales Rate</th>
                                                            <th>Stock Rate</th>
                                                        </tr>
                                                    </table>
                                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 390px; width: 1950px; background-color: aliceblue;">
                                                        <asp:GridView ID="grdItemDetails" EmptyDataRowStyle-CssClass="Emptyidclassforselector" runat="server" AutoGenerateColumns="false" ShowHeader="false" AllowSorting="true" CssClass="SalesPerformance"    ShowFooter="true"> 
                                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                            <PagerSettings FirstPageText="First" LastPageText="Last" PageButtonCount="5" Position="Bottom" />
                                                             <FooterStyle CssClass="GVFixedFooter" />
                                                            <PagerStyle Height="30px" VerticalAlign="Bottom" />
                                                            <PagerStyle CssClass="pshro_text" />
                                                            <Columns>
                                                                <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                                                <asp:BoundField DataField="InventoryCode" HeaderText="Itemcode"></asp:BoundField>
                                                                <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                                <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                                <asp:BoundField DataField="PurQty" HeaderText="PurQty"></asp:BoundField>
                                                                <asp:BoundField DataField="PurValue" HeaderText="PurValue"></asp:BoundField>
                                                                <asp:BoundField DataField="SoldQty" HeaderText="SoldQty"></asp:BoundField>
                                                                <asp:BoundField DataField="SalesValue" HeaderText="SalesValue"></asp:BoundField>
                                                                <asp:BoundField DataField="NetCost" HeaderText="NetCost"></asp:BoundField>
                                                                <asp:BoundField DataField="StockQty" HeaderText="StockQty"></asp:BoundField>
                                                                <asp:BoundField DataField="StockValue" HeaderText="StockValue"></asp:BoundField>
                                                                <asp:BoundField DataField="TotalCost" HeaderText="TotalCost"></asp:BoundField>
                                                                <asp:BoundField DataField="Profit" HeaderText="Profit"></asp:BoundField>
                                                                <asp:BoundField DataField="ProfitPer" HeaderText="ProfitPer"></asp:BoundField>
                                                                <asp:BoundField DataField="PurchaseRate" HeaderText="PurchaseRate"></asp:BoundField>
                                                                <asp:BoundField DataField="SalesRate" HeaderText="SalesRate"></asp:BoundField>
                                                                <asp:BoundField DataField="StockRate" HeaderText="StockRate"></asp:BoundField>
                                                               <%-- <asp:BoundField DataField="CategoryCode" HeaderText="Category" ItemStyle-CssClass="hidden"></asp:BoundField>
                                                                <asp:BoundField DataField="BrandCode" HeaderText="Brand" ItemStyle-CssClass="hidden"></asp:BoundField>
                                                                <asp:BoundField DataField="VendorCode" HeaderText="Vendor" ItemStyle-CssClass="hidden"></asp:BoundField>
                                                                <asp:BoundField DataField="Class" HeaderText="Class" ItemStyle-CssClass="hidden"></asp:BoundField>
                                                                <asp:BoundField DataField="SubClass" HeaderText="SubClass" ItemStyle-CssClass="hidden"></asp:BoundField>
                                                                <asp:BoundField DataField="DepartMentCode" HeaderText="DepartMentCode" ItemStyle-CssClass="hidden"></asp:BoundField>--%>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="control-container">
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkExport" runat="server" class="button-blue" OnClick ="btnExportToExcel_Click"
                                                  Text="Export"></asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClearAll" runat="server" OnClick="lnkClear_Click" class="button-blue" Text="Clear"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
             <asp:PostBackTrigger ControlID="lnkExport" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hidUserCode" runat="server" />
</asp:Content>
