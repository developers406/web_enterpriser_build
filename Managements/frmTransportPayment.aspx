﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTransportPayment.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmTransportPayment" EnableEventValidation="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 45px;
            max-width: 45px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 85px;
            max-width: 85px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 90px;
            max-width: 90px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 90px;
            max-width: 90px;
            text-align: left !important;
        }

        /*Details Table Style*/
        .grdLoad_Details td:nth-child(1), .grdLoad_Details th:nth-child(1) {
            min-width: 45px;
            max-width: 45px;
            text-align: center !important;
        }

        .grdLoad_Details td:nth-child(2), .grdLoad_Details th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center !important;
        }

        .grdLoad_Details td:nth-child(3), .grdLoad_Details th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
            text-align: left !important;
        }

        .grdLoad_Details td:nth-child(4), .grdLoad_Details th:nth-child(4) {
            min-width: 200px;
            max-width: 200px;
            text-align: left !important;
        }

        .grdLoad_Details td:nth-child(5), .grdLoad_Details th:nth-child(5) {
            min-width: 65px;
            max-width: 65px;
            text-align: left !important;
        }

        .grdLoad_Details td:nth-child(6), .grdLoad_Details th:nth-child(6) {
            min-width: 90px;
            max-width: 90px;
            text-align: left !important;
        }

        .grdLoad_Details td:nth-child(7), .grdLoad_Details th:nth-child(7) {
            min-width: 50px;
            max-width: 50px;
            text-align: left !important;
        }

        .grdLoad_Details td:nth-child(8), .grdLoad_Details th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;
        }

        .grdLoad_Details td:nth-child(9), .grdLoad_Details th:nth-child(9) {
            min-width: 75px;
            max-width: 75px;
            text-align: left !important;
        }

        .grdLoad_Details td:nth-child(10), .grdLoad_Details th:nth-child(10) {
            min-width: 200px;
            max-width: 200px;
            text-align: left !important;
        }

        .grdLoad_Details td:nth-child(11), .grdLoad_Details th:nth-child(11) {
            min-width: 65px;
            max-width: 65px;
            text-align: left !important;
        }

        .grdLoad_Details td:nth-child(12), .grdLoad_Details th:nth-child(12) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .grdLoad_Details td:nth-child(13), .grdLoad_Details th:nth-child(13) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .grdLoad_Details td:nth-child(14), .grdLoad_Details th:nth-child(14) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;
        }

        .grdLoad_Details td:nth-child(15), .grdLoad_Details th:nth-child(15) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;
        }

        .grdLoad_Details td:nth-child(16), .grdLoad_Details th:nth-child(16) {
            display: none;
        }

        .grdLoad_Details td:nth-child(17), .grdLoad_Details th:nth-child(17) {
            display: none;
        }
    </style>
    
   <script type="text/javascript">
       var d1;
       var d2;
       var Opendate;
       var dur;
       function fncGetUrl() {
           fncSaveHelpVideoDetail('', '', 'TransportPayment');
       }
       function fncOpenvideo() {

           document.getElementById("ifHelpVideo").src = HelpVideoUrl;

           var Mode = "TransportPayment";
           var d = new Date($.now());
           Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
           d1 = new Date($.now()).getTime();



           $("#dialog-Open").dialog({
               autoOpen: true,
               resizable: false,
               height: "auto",
               width: 1093,
               modal: true,
               dialogClass: "no-close",
               buttons: {
                   Close: function () {
                       $(this).dialog("destroy");
                       var d = new Date($.now());
                       var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                       d2 = new Date($.now()).getTime();
                       var Diff = Math.floor((d2 - d1) / 1000);
                       //alert(Diff);
                       if (Diff >= 60) {
                           fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                       }

                   }
               }
           });
       }


   </script>

    <script type="text/javascript">
        function pageLoad() {
            try {
                fncDisableTextBox();
                fncDecimal();
                fncDatepicker();
                $('#<%=txtOtherCharges.ClientID %>').focusout(function () {
                    if (parseFloat($('#<%=txtOtherCharges.ClientID %>').val()) > 0) {
                        var TotalNetAmount = parseFloat($('#<%=txtGrossAmt.ClientID %>').val()) + parseFloat($('#<%=txtTotLRCharges.ClientID %>').val()) +
                            parseFloat($('#<%=txtOtherCharges.ClientID %>').val());
                        $('#<%=txtNetAmt.ClientID %>').val(parseFloat(TotalNetAmount).toFixed(2));
                    }
                });
                if ($('#<%=hidPayMode.ClientID %>').val() != "") {
                    $('.grdLoad_Details td:nth-child(2)').css("display", "none");
                    $('.grdLoad_Details th:nth-child(2)').css("display", "none");
                    $('#<%=txtAmount.ClientID %>').val($('#<%=txtNetAmt.ClientID %>').val());
                    $("#<%= txtChequeDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "TransportPay") {
                    var splitDescription = Description.split('-');
                    $('#<%=hidTransfercode.ClientID %>').val($.trim(splitDescription[2]));
                    $('#<%=txtFromStation.ClientID %>').val(splitDescription[0]);
                    $('#<%=txtToStation.ClientID %>').val($.trim(splitDescription[1]));
                    $('#<%=txtTransport.ClientID %>').attr('disabled', 'disabled');
                }
                else if (SearchTableName == "Vendor") {
                    $('#<%=txtSupplier.ClientID %>').val(Description);
                    $('#<%=hidSupplierCode.ClientID %>').val(Code);
                }
                if (SearchTableName == "Freight Package") {
                    var splitDescription = Description.split('-');
                    $('#<%=txtFreightcharges.ClientID %>').val($.trim(splitDescription[0]));
                    $('#<%=txtWagesPalce.ClientID %>').val(splitDescription[1]);
                    $('#<%=txtWages.ClientID %>').val($.trim(splitDescription[2]));
                    $('#<%=txtPackage.ClientID %>').attr('disabled', 'disabled');
                    var paytype = $('#<%= ddlPaytype.ClientID %>').val();
                    if (paytype == "Paid") {
                        $('#<%=txtFreightcharges.ClientID %>').val(0);
                        $('#<%=txtFreightcharges.ClientID %>').removeAttr('disabled');
                    }
                    else {
                        $('#<%=txtFreightcharges.ClientID %>').attr('disabled', 'disabled');
                    } 
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncDisableTextBox() {
            $('#<%=txtFromStation.ClientID %>').attr('disabled', 'disabled');
            $('#<%=txtToStation.ClientID %>').attr('disabled', 'disabled');
            $('#<%=txtFreightcharges.ClientID %>').attr('disabled', 'disabled');
            $('#<%=txtWagesPalce.ClientID %>').attr('disabled', 'disabled');
            $('#<%=txtWages.ClientID %>').attr('disabled', 'disabled');
            $('#<%=txtGrossAmt.ClientID %>').attr('disabled', 'disabled');
            $('#<%=txtNetAmt.ClientID %>').attr('disabled', 'disabled');
            $('#<%=txtTotLRCharges.ClientID %>').attr('disabled', 'disabled');
        }
        function fncClear() {
            $('#<%=divinvoiceDetails.ClientID %>').find("input[type=text]").val('');
            $('#<%=txtInvoiceNo.ClientID %>').focus();
            $('#<%=txtFreightcharges.ClientID %>').val(0);
            $('#<%=txtLRCharges.ClientID %>').val(0);
            $('#<%=txtWages.ClientID %>').val(0);
            $('#<%=txtArticle.ClientID %>').val(0);
            fncDecimal();
            fncDatepicker();
            fncClearPackage();
            $('#<%=txtLRNo.ClientID %>').removeAttr('disabled');
            $('#<%=txtLRDate.ClientID %>').removeAttr('disabled');
            $('#<%=txtSupplier.ClientID %>').removeAttr('disabled');
            $('#<%=txtInvoiceNo.ClientID %>').removeAttr('disabled');
            $('#<%=txtDate.ClientID %>').removeAttr('disabled');
            $('#<%=ddlLrType.ClientID %>').removeAttr('disabled');
            $('#<%=ddlPaytype.ClientID %>').removeAttr('disabled');
            $('#<%=txtLRCharges.ClientID %>').removeAttr('disabled');
            $('#<%=txtLRNo.ClientID %>').focus();

        }

        function fncDecimal() {
            $('#<%=txtFreightcharges.ClientID %>').number(true, 2);
            $('#<%=txtLRCharges.ClientID %>').number(true, 2);
            $('#<%=txtWages.ClientID %>').number(true, 2);
            $('#<%=txtArticle.ClientID %>').number(true, 2);
            $('#<%=txtTotLRCharges.ClientID %>').number(true, 2);
            $('#<%=txtGrossAmt.ClientID %>').number(true, 2);
            $('#<%=txtOtherCharges.ClientID %>').number(true, 2);
            $('#<%=txtNetAmt.ClientID %>').number(true, 2);
        }
        function fncDatepicker() {

            $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtLRDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
        }
        function fncAdd() {
            var sno = 0, Qty = 0, FreightCharges = 0, Wages = 0, sumAmount = 0, LRCharges = 0;
            try {

                var Show = '';

                if (document.getElementById("<%=txtTransport.ClientID%>").value == "") {
                    Show = Show + 'Please Enter Transport Name';
                }

                if (document.getElementById("<%=txtFromStation.ClientID%>").value == "") {
                    Show = Show + '<br />From Station is Invalid';
                }
                if (document.getElementById("<%=txtToStation.ClientID%>").value == "") {
                    Show = Show + '<br />To Station is Invalid';
                }
                if (document.getElementById("<%=txtLRNo.ClientID%>").value == "") {
                    Show = Show + '<br />Please Enter LR No';
                }
                if (document.getElementById("<%=txtLRDate.ClientID%>").value == "") {
                    Show = Show + '<br />Please Enter LR Date';
                }
                if (document.getElementById("<%=txtSupplier.ClientID%>").value == "") {
                    Show = Show + '<br />Please Enter Supplier Name';
                }
               
                if (document.getElementById("<%=txtPackage.ClientID%>").value == "") {
                    Show = Show + '<br />Please Enter Package Name';
                }
                if (parseFloat(document.getElementById("<%=txtArticle.ClientID%>").value) <= 0 || document.getElementById("<%=txtArticle.ClientID%>").value == "") {
                    Show = Show + '<br />Number of Article is Must be Greater than Zero';
                }
                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }
                var pakage = '', Status = false;
                Qty = $('#<%=txtArticle.ClientID %>').val();
                FreightCharges = $('#<%=txtFreightcharges.ClientID %>').val();
                Wages = $('#<%=txtWages.ClientID %>').val();
                LRCharges = $('#<%=txtLRCharges.ClientID %>').val();

                sumAmount = parseFloat(0) + parseFloat(Qty * FreightCharges) + parseFloat(Qty * Wages);

                $("#tblDetails tbody").children().each(function () {
                    var cells = $("td", this);
                    if (cells.eq(5).text().trim() == $('#<%=txtLRNo.ClientID %>').val().trim() && cells.eq(15).text().trim() == $('#<%=hidSupplierCode.ClientID %>').val().trim()
                        && cells.eq(8).text().trim() == $('#<%=txtLRDate.ClientID %>').val().trim() && cells.eq(9).text().trim() == $('#<%=txtPackage.ClientID %>').val().trim() && $('#<%=txtWagesPalce.ClientID %>').val().trim() == cells.eq(10).text().trim()) {
                        //pakage = cells.eq(0).text().trim();
                        //Status = true;
                        cells.eq(11).text(parseFloat(cells.eq(11).text()) + parseFloat(Qty));
                        cells.eq(9).text($('#<%=txtPackage.ClientID %>').val());
                        cells.eq(12).text(parseFloat(parseFloat(cells.eq(12).text()) + parseFloat(FreightCharges)).toFixed(2));
                        cells.eq(13).text(parseFloat(parseFloat(cells.eq(13).text()) + parseFloat(Wages)).toFixed(2));
                        cells.eq(10).text($('#<%=txtWagesPalce.ClientID %>').val());
                        cells.eq(14).text(parseFloat(parseFloat(cells.eq(14).text()) + parseFloat(sumAmount)).toFixed(2));
                        Status = true;
                    }

                });

                if (Status == true) {
                    fncClearPackage();
                    fncDecimal();
                    fncTotalCalculation();
                    return false;
                }
                var tblDetails = $("#tblDetails tbody");
                sno = tblDetails.children().length + 1;


                var row = "<tr onclick='fncClick(this);return false;'><td class='text-center' id ='sno_'> " +
                    sno + "</td>" +
                    "<td class='tblbtn' title='Click here to Delete' style = 'cursor:pointer;'><img src='../images/delete.png' onclick ='fnctblClickDelete(this);return false;'/> " +
                    "</td><td>" +
                    $('#<%=txtInvoiceNo.ClientID %>').val() + "</td><td>" +
                    $('#<%=txtSupplier.ClientID %>').val() + "</td><td>" +
                    $('#<%=ddlPaytype.ClientID %>').val() + "</td><td>" +
                    $('#<%=txtLRNo.ClientID %>').val() + "</td><td>" +
                    $('#<%=ddlLrType.ClientID %>').val() + "</td><td>" +
                    parseFloat(LRCharges).toFixed(2) + "</td><td>" +
                    $('#<%=txtLRDate.ClientID %>').val() + "</td><td>" +
                    $('#<%=txtPackage.ClientID %>').val() + "</td><td>" +
                    $('#<%=txtWagesPalce.ClientID %>').val() + "</td><td>" +
                    Qty + "</td><td>" +
                    parseFloat(FreightCharges).toFixed(2) + "</td><td>" +
                    parseFloat(Wages).toFixed(2) + "</td><td>" +
                    parseFloat(sumAmount).toFixed(2) + "</td><td>" +
                    $('#<%=hidSupplierCode.ClientID %>').val() + "</td><td>" +
                    $('#<%=txtDate.ClientID %>').val() + "</td></tr>";
                tblDetails.append(row);
                fncClearPackage();
                fncDecimal();
                //fncDatepicker();
                fncTotalCalculation();
                //$('#<%=txtInvoiceNo.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncTotalCalculation() {
            try {
                var LRCharges = 0, row = 0, FreightCharges = 0, Wages = 0, TotalAmount = 0, LRNo = '';
                $("#tblDetails tbody").children().each(function () {
                    var cells = $("td", this);
                    row = parseFloat(row) + 1;
                    $(this).find('td').eq(0).text(row);
                    if ($(this).find('td').eq(5).text().trim() != LRNo) {
                        LRNo = $(this).find('td').eq(5).text().trim();
                        LRCharges = parseFloat(LRCharges) + parseFloat(cells.eq(7).text().trim());
                    }

                    TotalAmount = parseFloat(TotalAmount) + parseFloat(cells.eq(14).text().trim());
                });
                $('#<%=txtGrossAmt.ClientID %>').val(parseFloat(TotalAmount).toFixed(2));
                $('#<%=txtTotLRCharges.ClientID %>').val(parseFloat(LRCharges).toFixed(2));
                var TotalNetAmount = parseFloat($('#<%=txtGrossAmt.ClientID %>').val()) + parseFloat($('#<%=txtTotLRCharges.ClientID %>').val()) +
                    parseFloat($('#<%=txtOtherCharges.ClientID %>').val());
                $('#<%=txtNetAmt.ClientID %>').val(parseFloat(TotalNetAmount).toFixed(2));;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function clearForm() {
            $(':input').val('');
            $(':hidden').val('');
            fncClear();
            fncDecimal();
            fncDatepicker();
            $('#<%=txtTotLRCharges.ClientID %>').val(0);
            $('#<%=txtGrossAmt.ClientID %>').val(0);
            $('#<%=txtOtherCharges.ClientID %>').val(0);
            $('#<%=txtNetAmt.ClientID %>').val(0);
            $("#tblDetails tbody").children().remove();
            $('#<%=txtTransport.ClientID %>').removeAttr('disabled');
            $('#<%=txtLorryNo.ClientID %>').focus();
        }
        function fncSave() {

            try {
                $('#<%=divinvoiceDetails.ClientID %>').find("input[type=text]").val('');

                $('#<%=lnkSave.ClientID %>').css("display", "none");
                if ($("#tblDetails tbody").children().length <= 0) {
                    ShowPopupMessageBox("Payment Details Invalid");
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                    return false;
                }

                var rowFreight = "<table>";
                $('#<%=hidXml.ClientID %>').val('');
                $("#tblDetails tbody").children().each(function () {
                    var cells = $("td", this);
                    var invoiceDateSplit = cells.eq(16).text().trim().split('-');
                    var LRDateSplit = cells.eq(8).text().trim().split('-');

                    rowFreight = rowFreight + "<transportPayment  InvoiceNO='" + cells.eq(2).text().trim() + "'";
                    rowFreight = rowFreight + " InvovoiceDate='" + invoiceDateSplit[2] + '-' + invoiceDateSplit[1] + '-' + invoiceDateSplit[0] + "' ";
                    rowFreight = rowFreight + " SupplieCode='" + cells.eq(15).text().trim() + "'";
                    rowFreight = rowFreight + " LRNO='" + cells.eq(5).text().trim() + "'";
                    rowFreight = rowFreight + " LRDate='" + LRDateSplit[2] + '-' + LRDateSplit[1] + '-' + LRDateSplit[0] + "'";
                    rowFreight = rowFreight + " PayType='" + cells.eq(4).text().trim() + "'";
                    rowFreight = rowFreight + " Type='" + cells.eq(6).text().trim() + "'";
                    rowFreight = rowFreight + " LRCharge='" + cells.eq(7).text().trim() + "'";
                    rowFreight = rowFreight + " Package='" + cells.eq(9).text().trim() + "'";
                    rowFreight = rowFreight + " Qty='" + cells.eq(11).text().trim() + "'";
                    rowFreight = rowFreight + " FreightCharge='" + cells.eq(12).text().trim() + "'";
                    rowFreight = rowFreight + " WagesPlace='" + cells.eq(10).text().trim() + "'";
                    rowFreight = rowFreight + " WagesAmount='" + cells.eq(13).text().trim() + "'";
                    rowFreight = rowFreight + " TotalAmount='" + cells.eq(14).text().trim() + "'";
                    rowFreight = rowFreight + "></transportPayment>";
                });

                rowFreight = rowFreight + "</table>";

                console.log(escape(rowFreight));

                $('#<%=hidXml.ClientID %>').val(escape(rowFreight));
                $('#<%=hidFrStation.ClientID %>').val($('#<%=txtFromStation.ClientID %>').val());
                $('#<%=hidToStation.ClientID %>').val($('#<%=txtToStation.ClientID %>').val());
                $('#<%=hidGross.ClientID %>').val($('#<%=txtGrossAmt.ClientID %>').val());
                $('#<%=hidNetAmount.ClientID %>').val($('#<%=txtNetAmt.ClientID %>').val());
                $('#<%=hidTotalLRCharges.ClientID %>').val($('#<%=txtTotLRCharges.ClientID %>').val());

                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncLoadTranPayDetails() {
            var allObj = jQuery.parseJSON($('#<%=hidGetXml.ClientID %>').val());
            var tblDetails = $("#tblDetails tbody");
            tblDetails.children().remove();

            for (var i = 0; i < allObj.length; i++) {
                var row = "<tr onclick='fncClick(this);return false;'><td class='text-center' id ='sno_'> " +
                    (parseFloat(i) + 1) + "</td>" +
                    "<td class='tblbtn' title='Click here to Delete' style = 'cursor:pointer;'><img src='../images/delete.png' onclick ='fnctblClickDelete(this);return false;'/> " +
                    "</td><td>" +
                    allObj[i]["InvoiceNo"] + "</td><td>" +
                    allObj[i]["VendorName"] + "</td><td>" +
                    allObj[i]["PayType"] + "</td><td>" +
                    allObj[i]["LRNo"] + "</td><td>" +
                    allObj[i]["Type"] + "</td><td>" +
                    parseFloat(allObj[i]["LRCharges"]).toFixed(2) + "</td><td>" +
                    allObj[i]["LRDate"] + "</td><td>" +
                    allObj[i]["Package"] + "</td><td>" +
                    allObj[i]["WagesPlace"] + "</td><td>" +
                    allObj[i]["Qty"] + "</td><td>" +
                    parseFloat(allObj[i]["FreightCharges"]).toFixed(2) + "</td><td>" +
                    parseFloat(allObj[i]["WagesAmount"]).toFixed(2) + "</td><td>" +
                    parseFloat(allObj[i]["TotalAmt"]).toFixed(2) + "</td><td>" +
                    allObj[i]["SuppilierCode"] + "</td><td>" +
                    allObj[i]["InvoiceDate"] + "</td></tr>";
                tblDetails.append(row);
            }
            var paymodetype = $('#<%= ddlPaymode.ClientID %>').val();
            if (paymodetype == "CASH" || paymodetype == "CD/DN.Adj") {
                fncShowHideBankcode();
            }
            return false;
        }

        function fnctblClickDelete(source) {
            try {
                $("#dialog-confirm").dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "YES": function () {
                            $(this).dialog("close");
                            $(source).closest("tr").remove();
                            fncTotalCalculation();
                        },
                        "NO": function () {
                            $(this).dialog("close");
                            return false();
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncClearPackage() {
            $('#<%=txtPackage.ClientID %>').val('');
            $('#<%=txtArticle.ClientID %>').val(0);
            $('#<%=txtFreightcharges.ClientID %>').val(0);
            $('#<%=txtWagesPalce.ClientID %>').val('');
            $('#<%=txtWages.ClientID %>').val(0);
            $('#<%=txtPackage.ClientID %>').removeAttr('disabled');
            $('#<%=txtPackage.ClientID %>').focus();
        }

        function fncClick(source) {
            try {
                $('#<%=txtLRNo.ClientID %>').val($(source).closest("tr").find('td').eq(5).html());
                $('#<%=txtLRDate.ClientID %>').val($(source).closest("tr").find('td').eq(8).html());
                $('#<%=txtSupplier.ClientID %>').val($(source).closest("tr").find('td').eq(3).html());
                $('#<%=txtInvoiceNo.ClientID %>').val($(source).closest("tr").find('td').eq(2).html());
                $('#<%=txtDate.ClientID %>').val($(source).closest("tr").find('td').eq(16).html());
                $('#<%=ddlPaytype.ClientID %>').val($(source).closest("tr").find('td').eq(4).html());
                $('#<%=ddlLrType.ClientID %>').val($(source).closest("tr").find('td').eq(6).html());
                $('#<%=txtLRCharges.ClientID %>').val($(source).closest("tr").find('td').eq(7).html());
                $('#<%=txtPackage.ClientID %>').val($(source).closest("tr").find('td').eq(9).html());
                $('#<%=txtArticle.ClientID %>').val($(source).closest("tr").find('td').eq(11).html());
                $('#<%=txtFreightcharges.ClientID %>').val($(source).closest("tr").find('td').eq(12).html());
                $('#<%=txtWagesPalce.ClientID %>').val($(source).closest("tr").find('td').eq(10).html());
                $('#<%=txtWages.ClientID %>').val($(source).closest("tr").find('td').eq(13).html());
                $('#<%=hidSupplierCode.ClientID %>').val($(source).closest("tr").find('td').eq(15).html());


                $('#<%=txtLRNo.ClientID %>').attr('disabled', 'disabled');
                $('#<%=txtLRDate.ClientID %>').attr('disabled', 'disabled');
                $('#<%=txtSupplier.ClientID %>').attr('disabled', 'disabled');
                $('#<%=txtInvoiceNo.ClientID %>').attr('disabled', 'disabled');
                $('#<%=txtDate.ClientID %>').attr('disabled', 'disabled');
                $('#<%=ddlLrType.ClientID %>').attr('disabled', 'disabled');
                $('#<%=ddlPaytype.ClientID %>').attr('disabled', 'disabled');
                $('#<%=txtLRCharges.ClientID %>').attr('disabled', 'disabled');

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncAssignPaymodeDetail() {
            try {
                var paymodetype = $('#<%= ddlPaymode.ClientID %>').val();
                if (paymodetype == "CASH" || paymodetype == "CD/DN.Adj") {
                    fncShowHideBankcode();

                    if (paymodetype == "CASH") {
                        $('#<%=txtBankcode.ClientID %>').val('CASH');
                        $('#<%=txtBankName.ClientID %>').val('CASH ACCOUNT');
                        $('#<%=txtChequeNo.ClientID %>').val('CASH');
                        $('#<%=txtChequeNo.ClientID %>').on('mousedown', function () {
                            return false;
                        });
                    }
                    else {
                        $('#<%=txtBankcode.ClientID %>').val('CD/DN.Adj');
                        $('#<%=txtBankName.ClientID %>').val('CD/DN.Adj');
                        $('#<%=txtChequeNo.ClientID %>').val('CD/DN.Adj');
                        $('#<%=txtChequeNo.ClientID %>').off('mousedown');
                    }
                }
                else if (paymodetype == "RTGS") {
                    $('#<%=txtChequeNo.ClientID %>').val('RTGS');
                    $('#<%=txtChequeNo.ClientID %>').on('mousedown', function () {
                        return false;
                    });
                }
                else {
                    $('#divtxtBankcode').hide();
                    $('#divddlbankcode').show();
                    $('#<%=txtChequeNo.ClientID %>').off('mousedown');
                    $('#<%=txtChequeNo.ClientID %>').val('');
                    $('#<%=txtBankName.ClientID %>').val('');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            return false;
        }

        function fncGetBankName() {
            try {
                var bankname = $('#<%= ddlBankCode.ClientID %>').val();
                if (bankname != '<%=Resources.LabelCaption.ddl_Empty%>') {
                    $('#<%=txtBankName.ClientID %>').val(bankname);

                    if ($('#<%=ddlPaymode.ClientID %>').val() == "RTGS") {
                        setTimeout(function () {
                            $('#<%=txtChequeDate.ClientID %>').focus();
                        }, 10);
                    }
                    else {
                        setTimeout(function () {
                            $('#<%=txtChequeNo.ClientID %>').focus();
                        }, 10);
                    }
                }
                else
                    $('#<%=txtBankName.ClientID %>').val('');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowHideBankcode() {
            try {
                $('#divtxtBankcode').show();
                $('#divddlbankcode').hide();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncPayValidation() {
            try {
                $('#<%= lnkSave.ClientID %>').css("display", "none");
                if ($('#<%= ddlPaymode.ClientID %>').val() == '<%=Resources.LabelCaption.ddl_Empty%>') {
                    popUpObjectForSetFocusandOpen = $('#<%= ddlPaymode.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.Alert_PayType%>');
                    $('#<%= lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
                else if ($('#<%= ddlBankCode.ClientID %>').val() == '<%=Resources.LabelCaption.ddl_Empty%>' && $('#<%= ddlPaymode.ClientID %>').val() != "CASH"
                    && $('#<%= ddlPaymode.ClientID %>').val() != "CD/DN.Adj") {
                    popUpObjectForSetFocusandOpen = $('#<%= ddlBankCode.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.Alert_BankCode%>');
                    $('#<%= lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
                else if ($('#<%= ddlPaymode.ClientID %>').val() == "CD/DN.Adj") {
                    if (parseFloat($('#<%= txtAmount.ClientID %>').val()) != 0) {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_ZeroPayment%>');
                        $('#<%= lnkSave.ClientID %>').css("display", "block");
                        return false;
                    }
                }
                else if ($('#<%= txtChequeNo.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%= txtChequeNo.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterCheque%>');
                    $('#<%= lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
                else if (parseInt($('#<%= txtAmount.ClientID %>').val()) <= 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_InvalidAmt%>');
                    $('#<%= lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
                else {
                    return true;
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
         

        function fncOpenDialog() {
            $("#dialog-confirm-Print").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=btnPrint.ClientID %>').click();
                        // __doPostBack('ctl00$ContentPlaceHolder1$lnkPrint', '');
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        window.location.href = "frmTransportPaymentView.aspx";
                        return false();
                    }
                }
            });
        }

        function fncDuplicateChequeNumberCheck() {
            try {
                if ($('#<%= ddlPaymode.ClientID %>').val() != "CASH" || $('#<%= ddlPaymode.ClientID %>').val() != "CD/DN.Adj" ||
                    $('#<%= ddlPaymode.ClientID %>').val() != "RTGS") {
                    var obj = {};
                    obj.chequeNo = $.trim($('#<%= txtChequeNo.ClientID %>').val());
                    obj.BankCode = $('#<%=ddlBankCode.ClientID %>').find("option:selected").text();
                    obj.Paymode = $('#<%=ddlPaymode.ClientID %>').val();

                    if (obj.chequeNo == "")
                        return;

                    $.ajax({
                        type: "POST",
                        url: "frmTransportPayment.aspx/fncChequeChequeDuplication",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d > 0) {
                                popupStatus = "refVoucherNo";
                                ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_Cheque%>');
                        return false;
                    }
                },
                error: function (data) {
                    ShowPopupMessageBox(data.message);
                    return false;
                }
            });
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncChangePaymode() {
            var paytype = $('#<%= ddlPaytype.ClientID %>').val();
            if (paytype == "Paid") {
                $('#<%=txtFreightcharges.ClientID %>').val(0);
                $('#<%=txtFreightcharges.ClientID %>').removeAttr('disabled');
            }
            else {
                $('#<%=txtFreightcharges.ClientID %>').attr('disabled', 'disabled'); 
            } 
        }

        function fncTypeChange() { 
            var Type = $('#<%= ddlType.ClientID %>').val();
            if (Type == "Manual") {
                $('#<%=txtFreightcharges.ClientID %>').removeAttr('disabled');
                $('#<%=txtWagesPalce.ClientID %>').removeAttr('disabled');
                $('#<%=txtWages.ClientID %>').removeAttr('disabled');
            }
            else {
                $('#<%=txtFreightcharges.ClientID %>').attr('disabled', 'disabled');
                $('#<%=txtWagesPalce.ClientID %>').attr('disabled', 'disabled');
                $('#<%=txtWages.ClientID %>').attr('disabled', 'disabled');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a href="../Managements/frmTransportPaymentView.aspx">Transport Payment View</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Transport Payment</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </div>
        <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
        </div>
        <div class="col-md-8">
            <div class="container-group-small" style="width: 100%; margin-top: 15px;">
                <div class="whole-price-header">
                    Transport Details
                </div>
                <div class="col-md-12" style="margin-top: 15px;">
                    <div class="col-md-6">
                        <div class="col-md-2">
                            <asp:Label ID="Label1" runat="server" Text="Type"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control-res" onchange="fncTypeChange();return false;">
                                <asp:ListItem Value="Auto">Auto</asp:ListItem>
                                <asp:ListItem Value="Manual">Manual</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:Label ID="Label2" runat="server" Text="Lorry No"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtLorryNo" runat="server" CssClass="form-control-res"> </asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <asp:Label ID="Label3" runat="server" Text="Fr.Station"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="col-md-10">
                                <asp:TextBox ID="txtFromStation" runat="server" CssClass="form-control-res"> </asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 10px;">
                    <div class="col-md-6">
                        <div class="col-md-2">
                            <asp:Label ID="Label4" runat="server" Text="Transport"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="col-md-10">
                            <asp:TextBox ID="txtTransport" runat="server" CssClass="form-control-res"
                                onkeydown="return fncShowSearchDialogCommon(event, 'TransportPay',  'txtTransport', 'txtLRNo');"> </asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <asp:Label ID="Label5" runat="server" Text="ToStation"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="col-md-10">
                                <asp:TextBox ID="txtToStation" runat="server" CssClass="form-control-res"> </asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-group-small" style="width: 100%; margin-top: 10px;" id="divinvoiceDetails" runat="server">
                <div class="col-md-6">
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label6" runat="server" Text="LR.No."></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtLRNo" runat="server" CssClass="form-control-res"> </asp:TextBox>
                        </div>
                        <div class="col-md-6"><i style="font-size: 20px; cursor: pointer;" class="fa fa-plus-circle" onclick="fncClear();return false;"></i></div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label7" runat="server" Text="LR.Date"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtLRDate" runat="server" CssClass="form-control-res"> </asp:TextBox>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label8" runat="server" Text="Supplier"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="col-md-10">
                            <asp:TextBox ID="txtSupplier" runat="server" CssClass="form-control-res"
                                onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtSupplier', 'txtInvoiceNo');"> </asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label9" runat="server" Text="InvoiceNo"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control-res"> </asp:TextBox>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label10" runat="server" Text="Date"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtDate" runat="server" CssClass="form-control-res"> </asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:Label ID="Label11" runat="server" Text="Type"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlLrType" runat="server" CssClass="form-control-res">
                                <asp:ListItem Value="Add">Add</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label12" runat="server" Text="Pay Type"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlPaytype" runat="server" CssClass="form-control-res"
                                onchange="fncChangePaymode();return false;">
                                <asp:ListItem Value="ToPay">To Pay</asp:ListItem>
                                <asp:ListItem Value="Paid">Paid</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:Label ID="Label13" runat="server" Text="LRCharges"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtLRCharges" runat="server" CssClass="form-control-res text-right" Text="0"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label14" runat="server" Text="Package"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="col-md-10">
                            <asp:TextBox ID="txtPackage" runat="server" CssClass="form-control-res"
                                onkeydown="return fncShowSearchDialogCommon(event, 'Freight Package',  'txtPackage', 'txtArticle');"> </asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label15" runat="server" Text="No.Article"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtArticle" runat="server" onfocus="this.select()" CssClass="form-control-res text-right" Text="0"> </asp:TextBox>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label16" runat="server" Text="Fr.Charges"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtFreightcharges" runat="server" CssClass="form-control-res text-right" Text="0"> </asp:TextBox>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label17" runat="server" Text="Wg.Place"></asp:Label>
                        </div>
                        <div class="col-md-10">
                            <asp:TextBox ID="txtWagesPalce" runat="server" CssClass="form-control-res"> </asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-2">
                            <asp:Label ID="Label18" runat="server" Text="Wages"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtWages" runat="server" CssClass="form-control-res text-right" Text="0"> </asp:TextBox>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="button-contol">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue"
                                        OnClientClick="fncAdd();return false;"><i class="icon-play"></i>Add</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClientClick="fncClearPackage();return false;">
                            <i class="icon-play"></i>Clear</asp:LinkButton>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-group-small" style="width: 100%; margin-top: 130px;" id="divPayment" runat="server">
                <div class="whole-price-header">
                    Payment Detail
                </div>
                <div class="col-md-12" style="margin-top: 10px;">
                    <div class="col-md-1">
                        <asp:Label ID="Label19" runat="server" Text="Paymode"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="ddlPaymode" runat="server" CssClass="form-control-res" onchange="fncAssignPaymodeDetail()">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1">
                        <asp:Label ID="Label20" runat="server" Text="BankCode"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <div id="divddlbankcode">
                            <asp:DropDownList ID="ddlBankCode" runat="server" CssClass="form-control-res" onchange="fncGetBankName()">
                            </asp:DropDownList>
                        </div>
                        <div id="divtxtBankcode" style="display: none">
                            <asp:TextBox ID="txtBankcode" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <asp:Label ID="Label26" runat="server" Text="BankName"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 10px;">
                    <div class="col-md-1">
                        <asp:Label ID="Label27" runat="server" Text="ChequeNo"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtChequeNo" runat="server" CssClass="form-control-res" onblur="fncDuplicateChequeNumberCheck();"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:Label ID="Label28" runat="server" Text="ChequeDate"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtChequeDate" runat="server" CssClass="form-control-res"> </asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:Label ID="Label30" runat="server" Text="TranAmount"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:UpdatePanel ID="upAmt" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="txtAmount" runat="server" onkeypress="return numericOnly(event);" onkeydown="return fncAmtFocusOut(event,this);" CssClass="form-control-res"></asp:TextBox>
                                <%--onMouseDown="return false"--%>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="container-group-small" style="width: 100%; margin-top: 120px;">
                <div class="col-md-12" style="margin-top: 10px;">
                    <div class="col-md-5">
                        <asp:Label ID="Label24" runat="server" Text="TotalLRCharges"></asp:Label>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtTotLRCharges" runat="server" CssClass="form-control-res text-right">0</asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 5px;">
                    <div class="col-md-5">
                        <asp:Label ID="Label21" runat="server" Text="GrossAmt"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtGrossAmt" runat="server" CssClass="form-control-res text-right">0</asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 5px;">
                    <div class="col-md-5">
                        <asp:Label ID="Label22" runat="server" Text="OtherCharges"></asp:Label>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtOtherCharges" runat="server" CssClass="form-control-res text-right">0</asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 5px;">
                    <div class="col-md-5">
                        <asp:Label ID="Label23" runat="server" Text="Net Amount"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtNetAmt" runat="server" CssClass="form-control-res text-right">0</asp:TextBox>
                    </div>
                </div>

                <div class="col-md-12" style="margin-top: 5px;">
                    <div class="col-md-5">
                        <asp:Label ID="Label25" runat="server" Text="Remarks"></asp:Label>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" CssClass="form-control-res"> </asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 5px;">
                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-green" OnClick="lnkPrint_Click"><i class="icon-play"></i>Print</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkPay" runat="server" class="button-red" OnClick="lnkPay_Click"
                                OnClientClick="return fncPayValidation();"><i class="icon-play"></i>Pay</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return fncSave();"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkCleearAll" runat="server" class="button-red" OnClientClick="clearForm();return false;">
                            <i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkView" runat="server" class="button-blue" PostBackUrl="~/Managements/frmTransportPaymentView.aspx">
                            <i class="icon-play"></i>View</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="Payment_fixed_headers grdLoad_Details">
                <table id="tblDetails" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">Delete
                            </th>
                            <th scope="col">InvoiceNo
                            </th>
                            <th scope="col">Supplier
                            </th>
                            <th scope="col">PayType
                            </th>
                            <th scope="col">LRNo
                            </th>
                            <th scope="col">Type
                            </th>
                            <th scope="col">LR Charges
                            </th>
                            <th scope="col">LR Date
                            </th>
                            <th scope="col">Package
                            </th>
                            <th scope="col">Place
                            </th>
                            <th scope="col">Qty
                            </th>
                            <th scope="col">Charges
                            </th>
                            <th scope="col">Wages
                            </th>
                            <th scope="col">Amount
                            </th>
                            <th scope="col">VendorCode
                            </th>
                            <th scope="col">Invoice Date
                            </th>
                        </tr>
                    </thead>
                    <tbody style="height: 205px;"></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="display_none">
        <asp:Button ID="btnPrint" runat="server" OnClick="lnkPrint_Click" />
        <div id="dialog-confirm-Print" style="display: none;" title="Enterpriser Web">
            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>Paymet Saved Successfully.Do You Want Print ? </p>
        </div>
        <div id="PaymentSave">
            <div>
                <asp:Label ID="lblSave" runat="server" Text="" />
            </div>
            <div class="dialog_center">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                    OnClientClick="return fncSaveDialogClose()"> </asp:LinkButton>
            </div>
        </div> 
    </div>
    <asp:HiddenField ID="hidTransfercode" runat="server" Value="" />
    <asp:HiddenField ID="hidSupplierCode" runat="server" Value="" />
    <asp:HiddenField ID="hidXml" runat="server" Value="" />
    <asp:HiddenField ID="hidPackageCode" runat="server" Value="" />

    <asp:HiddenField ID="hidFrStation" runat="server" Value="" />
    <asp:HiddenField ID="hidToStation" runat="server" Value="" />
    <asp:HiddenField ID="hidGross" runat="server" Value="" />
    <asp:HiddenField ID="hidTotalLRCharges" runat="server" Value="" />
    <asp:HiddenField ID="hidNetAmount" runat="server" Value="" />
    <asp:HiddenField ID="hidGetXml" runat="server" Value="" />
    <asp:HiddenField ID="hidPayMode" runat="server" Value="" />
</asp:Content>
