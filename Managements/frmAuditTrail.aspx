﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmAuditTrail.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmAuditTrail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'AuditTrail');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "AuditTrail";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
    <script type="text/javascript">

        $(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        });

        function ValidateForm() {
            var Show = '';
            if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose To date';
                $("#<%= txtFromDate.ClientID %>").focus();
            }

            if ($("#<%= txtToDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose from date';
                $("#<%= txtToDate.ClientID %>").focus();
            }

            if (Show != '') {
                alert(Show);
                return false;
            }

            else {
                return true;
            }
        }

        function Clearall() {

            $("select").val(0);
            $("select").trigger("liszt:updated");
            $("table[id$='grdAuditTrails']").html("");
                       
            return false;
        }

        function Search_Gridview(strKey) {
            try {

                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=grdAuditTrails.ClientID %>");
                var rowData;

                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;

                }
            }
            catch (err) {
                alert(err.Message);
                console.log(err);
            }
        }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration:none;">Management</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Audit Trail</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full-gid">
            <div class="top-purchase-container">
                <div class="top-purchase-left-container" style="width: 50%">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblfromdate %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server"  Text='<%$ Resources:LabelCaption,lbl_Auditcode %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="DropDownAutitCode" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server"  Text='<%$ Resources:LabelCaption,lbltodate %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server"  Text='<%$ Resources:LabelCaption,lbl_formname %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="DropDownFormname" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-purchase-left-container" style="width: 50%">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server"  Text='<%$ Resources:LabelCaption,lblLocation %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="DropDownLocation" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server"  Text='<%$ Resources:LabelCaption,lbl_Searchname %>'></asp:Label>
                            </div>
                            <div class="label-right">
                              <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control-res" onkeyup="Search_Gridview(this)"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-container">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkFetch" runat="server" class="button-red" OnClick="lnkFetch_Click" Text='<%$ Resources:LabelCaption,btn_Fetch %>'></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-red"  Text='<%$ Resources:LabelCaption,btn_Export %>'
                                onclick="lnkPrint_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return Clearall()" Text='<%$ Resources:LabelCaption,btnclear %>'></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-audit-container">
                <asp:UpdatePanel runat="server" ID="upgrid" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="grdAuditTrails" runat="server" AllowSorting="true" Width="100%"
                            AutoGenerateColumns="true" ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" 
                            AlternatingRowStyle-BackColor="#c4ddff" CssClass="pshro_GridDgn" oncopy="return false">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                            <PagerStyle CssClass="pshro_text" />
                            <Columns>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="container-group-full">
            <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
            <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
            <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
        </div>
    </div>
</asp:Content>
