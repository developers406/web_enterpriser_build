﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmVATAnalysis.aspx.cs" Inherits="EnterpriserWebFinal.Managements.frmVATAnalysis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'VATAnalysis');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "VATAnalysis";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

    <script type="text/javascript">

        function pageLoad() {

            $("[id*=GroupBy]").hide();
            $("[id*=ViewBy]").hide();

            $('#<%=chkBydate.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=divfilter]").attr("disabled", "disabled");
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=divfilter]").removeAttr("disabled");
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                }
            });


            $('#<%=lnkAnnexure.ClientID%>').click(function () {

                if ($('#divVatAnalysisAnnexure').is(':visible')) {
                    $("[id*=divVatAnalysisAnnexure]").hide();
                    $("[id*=divSalesAnalysis]").show();
                }
                else {
                    $("[id*=divVatAnalysisAnnexure]").show();
                    $("[id*=divSalesAnalysis]").hide();
                }
                return false;
            });
        }

        $(document).ready(function () {

            $('#<%= rdoVatAnalysis.ClientID %> input').click(function () {
                //alert('Selected Value: ' + $("#<%= rdoVatAnalysis.ClientID %> input:radio:checked").val());
                var checkvalue = $("#<%= rdoVatAnalysis.ClientID %> input:radio:checked").val();
                if (checkvalue == 'Pur1') {
                    $("[id*=GroupBy]").show();
                    $("[id*=ViewBy]").hide();
                }
                else if (checkvalue == 'PurInv') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").show();
                }
                else if (checkvalue == 'PurItem') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").hide();
                }
                else if (checkvalue == 'Sales1') {
                    $("[id*=GroupBy]").show();
                    $("[id*=ViewBy]").show();
                }
                else if (checkvalue == 'SalesInv') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").show();
                }
                else if (checkvalue == 'SalesItem') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").hide();
                }
                else if (checkvalue == 'PurRet1') {
                    $("[id*=GroupBy]").show();
                    $("[id*=ViewBy]").hide();
                }
                else if (checkvalue == 'PurRetDet') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").show();
                }
                else if (checkvalue == 'SalesRet1') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").hide();
                }
                else if (checkvalue == 'SalesRetDet') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").show();
                }
                else if (checkvalue == 'SalesRetItem') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").hide();
                }
                else if (checkvalue == 'SalesRetInv') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").hide();
                }
                else if (checkvalue == 'CrSales') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").hide();
                }
                else if (checkvalue == 'SalesLoc') {
                    $("[id*=GroupBy]").hide();
                    $("[id*=ViewBy]").hide();
                }

            });
        });

        $(function () {

            try {

                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

                if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                }

                if ($("#<%= txtToDate.ClientID %>").val() === '') {
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                alert(err.Message);
            }
        });


        function JSalerts(Show) {
            swal({ title: "Please fill the details!",
                text: Show,
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                cancelButtonText: "I am not sure!",
                closeOnConfirm: true,
                closeOnCancel: true
            });
        }

        function DateCheck() {
            var StartDate = $("#<%= txtFromDate.ClientID %>").val();
            var EndDate = $("#<%= txtToDate.ClientID %>").val();
            var eDate = new Date(EndDate);
            var sDate = new Date(StartDate);
            if (StartDate != '' && EndDate != '' && sDate > eDate) {
                alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                return false;
            }
        }

        function ValidateForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }

                if (Show != '') {
                    alert(Show);
                    return false;
                }

                else {
                    DateCheck();
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        //        function ToogleTextBoxes(s, e) {
        //            var ToDate = document.getElementById("<%=txtToDate.ClientID%>");
        //            var FromDate = document.getElementByID("<%=txtFromDate.ClientID%>");
        //            var disabledState = s.GetChecked() ? '' : 'disabled';

        //            ToDate.disabled = disabledState;
        //            FromDate.disabled = disabledState;
        //        }
  
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration:none;">Management</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">VAT-Analysis Reports </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <div class="container-date-report" id="divSalesAnalysis" runat="server">
                <div class="barcode-header">
                    VAT-Analysis Reports
                </div>
                <div>
                    <div class="control-group" style="margin-top: 10px">
                        <asp:RadioButtonList ID="rdoVatAnalysis" runat="server">
                            <asp:ListItem Selected="True" Value="Pur1">Purchase VAT</asp:ListItem>
                            <asp:ListItem Value="PurInv">Purchase VAT Invoice Wise</asp:ListItem>
                            <asp:ListItem Value="PurItem">Purchase VAT Item Wise</asp:ListItem>
                            <asp:ListItem Value="Sales1"> Sales VAT</asp:ListItem>
                            <asp:ListItem Value="SalesInv">Sales VAT Invoice Wise</asp:ListItem>
                            <asp:ListItem Value="SalesItem">Sales VAT Item Wise</asp:ListItem>
                            <asp:ListItem Value="PurRet1">Purchase Return VAT</asp:ListItem>
                            <asp:ListItem Value="PurRetDet">Purchase Return VAT Detail</asp:ListItem>
                            <asp:ListItem Value="SalesRet1">Sales Return VAT</asp:ListItem>
                            <asp:ListItem Value="SalesRetDet">Sales Return VAT Detail</asp:ListItem>
                            <asp:ListItem Value="SalesRetItem">Sales Return VAT Item Wise</asp:ListItem>
                            <asp:ListItem Value="SalesRetInv">Sales Return VAT Invoice Wise</asp:ListItem>
                            <asp:ListItem Value="CrSales">Credit Sales</asp:ListItem>
                            <asp:ListItem Value="SalesLoc">Sales VAT Location Wise</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left-vat" id="GroupBy">
                            <div>
                                <asp:RadioButton ID="rdoDate" runat="server" GroupName="Groupby" Text="Date" Checked="true"
                                    Font-Bold="True" />
                            </div>
                            <div>
                                <asp:RadioButton ID="rdoMonth" runat="server" GroupName="Groupby" Text="Month" Font-Bold="True" />
                            </div>
                            <div>
                                <asp:RadioButton ID="rdoNone" runat="server" GroupName="Groupby" Text="None" Font-Bold="True" />
                            </div>
                        </div>
                        <div class="control-group-right-vat" id="ViewBy">
                            <div>
                                <asp:RadioButton ID="rdoView1" runat="server" GroupName="GroupView" Text="View1"
                                    Checked="true" Font-Bold="True" />
                            </div>
                            <div>
                                <asp:RadioButton ID="rdoView2" runat="server" GroupName="GroupView" Text="View2"
                                    Font-Bold="True" />
                            </div>
                            <div>
                                <asp:RadioButton ID="rdoView3" runat="server" GroupName="GroupView" Text="View3"
                                    Font-Bold="True" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-date-report" id="divVatAnalysisAnnexure" style="display: none">
                <div class="barcode-header">
                    VAT Report Annexure
                </div>
                <div>
                    <div class="control-group" style="margin-top: 10px">
                        <asp:RadioButtonList ID="rdoListAnnexure" runat="server">
                            <asp:ListItem Selected="True">Purchase Vat</asp:ListItem>
                            <asp:ListItem>Sales Vat</asp:ListItem>
                            <asp:ListItem>Purchace CST</asp:ListItem>
                            <asp:ListItem>Purchace Return VAT</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="container-date-report-filter" id="divDateFilter" runat="server">
                <div class="barcode-header">
                    Date filter
                </div>
                <div class="control-group-split">
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label7" runat="server" Text="From Date"></asp:Label>
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div style="float: left;">
                            <asp:Label ID="Label8" runat="server" Text="To Date"></asp:Label>
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div style="float: left;">
                            <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Style="margin-top: 20px"
                                Checked="true" Font-Bold="True" />
                        </div>
                        <div style="float: right; margin-top: 20px">
                            <asp:LinkButton ID="lnkAnnexure" runat="server" class="btn btn-danger" Width="100px"
                                Font-Bold="true">Annexure</asp:LinkButton>
                            <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Width="121px"
                                Font-Bold="true" OnClick="lnkLoadReport_Click">Load Report</asp:LinkButton>
                            <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-danger" Width="100px"
                                Font-Bold="true">Clear</asp:LinkButton>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
</asp:Content>
