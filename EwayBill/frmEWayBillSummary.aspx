﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MainMaster.master"   CodeBehind="frmEWayBillSummary.aspx.cs" Inherits="EnterpriserWebFinal.EWayBill.frmEWayBillSummary" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server"> 
    <link type="text/css" href="../css/ewaybill.css" rel="stylesheet" /> 
    <style type="text/css">
        .eWaysum td:nth-child(1), .eWaysum th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .eWaysum td:nth-child(2), .eWaysum th:nth-child(2) {
            min-width: 60px;
            max-width: 60px;
        }

        .eWaysum td:nth-child(3), .eWaysum th:nth-child(3) {
            min-width: 150px;
            max-width: 150px;
        }

        .eWaysum td:nth-child(4), .eWaysum th:nth-child(4) {
            min-width: 150px;
            max-width: 150px;
        }

        .eWaysum td:nth-child(5), .eWaysum th:nth-child(5) {
            min-width: 450px;
            max-width: 450px;
        }

        .eWaysum td:nth-child(6), .eWaysum th:nth-child(6) {
            min-width: 150px;
            max-width: 150px;
        }

        .eWaysum td:nth-child(6), .eWaysum th:nth-child(6) {
            min-width: 150px;
            max-width: 150px;
        }

        .eWaysum td:nth-child(7), .eWaysum th:nth-child(7) {
            min-width: 150px;
            max-width: 150px;
        }

        .eWaysum td:nth-child(8), .eWaysum th:nth-child(8) {
            display: none;
        }
    </style>

    <script type="text/javascript">

        function pageLoad() {
            try {
                $("select").chosen({ width: '100%' }); // width in px, %, em, etc
                $("#<%= txtFrmDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy" });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy" })
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSetDate() {
            try {

                $("#<%= txtFrmDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy" }).datepicker("setDate", "0");
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy" }).datepicker("setDate", "0");


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncRowClick(source) {
            var obj;
            try {

                obj = $(source);
                $("#<%=hidInvNo.ClientID%>").val($.trim($(obj).find('td span[id*="lblInvNo"]').text()));
                $("#<%=hidCustCode.ClientID%>").val($.trim($(obj).find('td span[id*="lblCustCode"]').text()));
                $("#<%=hidInvDate.ClientID%>").val($.trim($(obj).find('td span[id*="lblInvDate"]').text()));


                if ($("#<%=rbnTaxInv.ClientID%>").prop("checked")) {
                    $("#<%=hidType.ClientID%>").val("Tax");
                }
                else {
                    $("#<%=hidType.ClientID%>").val("Delcln");
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function showData() {
            $(function () {
                $("#Date-Dialog").dialog({
                    title: "Enterpriser Web",
                    width: "auto",
                    buttons: {
                        Ok: function () {
                            $(this).dialog("destroy");
                            $("#<%=btnDate.ClientID%>").click();
                        }
                    },
                    modal: true
                });
            });
        };

        function fncShowDataValidation() {
            try {

                var frmDate, toDate, date = {};

                frmDate = $('#<%=txtFrmDate.ClientID %>').val().split("-").reverse().join("-");;
                toDate = $('#<%=txtToDate.ClientID %>').val().split("-").reverse().join("-");
                

               <%-- date = $('#<%=txtFrmDate.ClientID %>').val().split("-");
                frmDate = date[2] + "-" + date[1] + "-" + date[0];
                date = $('#<%=txtToDate.ClientID %>').val().split("-");
                toDate = date[2] + "-" + date[1] + "-" + date[0];--%>


                if ($("#<%=txtMinBillvalue.ClientID%>").val() == "") {
                    popUpObjectForSetFocusandOpen = $("#<%=txtMinBillvalue.ClientID%>");
                    ShowPopupMessageBoxandFocustoObject("Please Enter Minimun Bill value");
                    return false;
                }
                else if ($("#<%=txtDis.ClientID%>").val() == "") {
                    popUpObjectForSetFocusandOpen = $("#<%=txtDis.ClientID%>");
                    ShowPopupMessageBoxandFocustoObject("Please Enter Minimun Distance");
                    return false;
                }
                else if (new Date(frmDate) > new Date(toDate)) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtFrmDate.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject("From date must less then or equal to To data");
                    return false;
                }
                else if (parseFloat($("#<%=txtMinBillvalue.ClientID %>").val()) > 50000) {
                    popUpObjectForSetFocusandOpen = $("#<%=txtMinBillvalue.ClientID %>");
                    ShowPopupMessageBoxandFocustoObject(" Please enter 50000 or less then 50000 values ");
                    status = false;
                }
                else if (parseFloat($("#<%=txtDis.ClientID %>").val()) > 10) {
                    popUpObjectForSetFocusandOpen = $("#<%=txtDis.ClientID %>");
                    ShowPopupMessageBoxandFocustoObject(" Please enter 10 or less then 10 values ");
                    status = false;
                }


}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="eWay_Summry">
        <div class="display_table">

            <div class="float_left ewmaster_height border_ewmaster">
                <div class="prepare">
                    <asp:Label ID="lblMinBillValue" runat="server" Text="Minimun Bill Value"></asp:Label>
                    <asp:TextBox ID="txtMinBillvalue" runat="server" onkeydown="return isNumberKeyWithDecimalNew(event);" Text="50000" CssClass="eway_minValue"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:Label ID="lblMinDis" runat="server" Text="Minimun Distance"></asp:Label>
                    <asp:TextBox ID="txtDis" runat="server" onkeydown="return isNumberKeyWithDecimalNew(event);" Text="10" CssClass="eway_minValue"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:Label ID="lblFrmDate" Text="From Date" runat="server" />
                    <asp:TextBox ID="txtFrmDate" runat="server" CssClass="eway_minValue"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:Label ID="lblToDate" Text="To Date" runat="server" />
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="eway_minValue"></asp:TextBox>
                </div>
            </div>

        </div>
        <div class="ewaybill_header2">
            <div class="float_left border_delType border">
                <div class="prepare">
                    <asp:Label ID="lblLoc" runat="server" Text="Location"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="ddl_width"></asp:DropDownList>
                </div>
            </div>


            <div class="float_left border_delType border">
                <div class="prepare">
                    <asp:RadioButton ID="rbnTaxInv" runat="server" Text="Tax Invoice" GroupName="Type" Checked="true" />
                </div>
                <div class="prepare">
                    <asp:RadioButton ID="rbnDeliveryChn" runat="server" GroupName="Type" Text="Delivery Challan" />
                </div>
            </div>

            <div class="float_left border_delType border">
                <div class="prepare">
                    <asp:RadioButton ID="rbnYettoPrepare" CssClass="prepare" Checked="true" OnCheckedChanged="rbnYettoPrepare_changed" AutoPostBack="true" runat="server" GroupName="prepare" />
                    <asp:Label ID="lblYettoPrepare" runat="server" CssClass="bold" Text="Yet to Prepare"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:RadioButton ID="rbnAlreadyPrepare" runat="server" OnCheckedChanged="rbnAlreadyPrepare_changed" AutoPostBack="true" GroupName="prepare" />
                    <asp:Label ID="lblAlreadyPrepare" runat="server" CssClass="bold" Text="Already Prepared"></asp:Label>
                </div>
            </div>


            <div class="prepare">
                <asp:UpdatePanel ID="upRefresh" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="lnkRefresh" CssClass="btn btn-primary" runat="server" OnClientClick=" return fncShowDataValidation();"
                            OnClick="lnkRefresh_click">Show Data</asp:LinkButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="display_table">
            <asp:UpdatePanel ID="uprptr" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divrepeater" class="table_headers  eWaysum eWaysum_new">
                        <table id="tblEWaySummary" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">View
                                    </th>
                                    <th scope="col">Doc.Date
                                    </th>
                                    <th scope="col" runat="server" id="thEdit">Doc.No
                                    </th>
                                    <th scope="col">Customer Name
                                    </th>
                                    <th scope="col">Value
                                    </th>
                                    <th scope="col">Distance
                                    </th>
                                    <th scope="col">CustCode
                                    </th>

                                </tr>
                            </thead>
                            <asp:Repeater ID="rptrewaySum" runat="server">
                                <HeaderTemplate>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr runat="server" onclick="fncRowClick(this);">
                                        <td>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("SNO") %>' />
                                        </td>
                                        <td runat="server">
                                            <asp:Button runat="server" Text="view" OnClick="view_click" />
                                        </td>
                                        <td runat="server">
                                            <asp:Label ID="lblInvDate" runat="server" Text='<%# Eval("InvoiceDate") %>' />
                                        </td>
                                        <td runat="server">
                                            <asp:Label ID="lblInvNo" runat="server" Text='<%# Eval("InvoiceNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCustName" runat="server" Text='<%# Eval("CustomerName") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblValue" runat="server" Text='<%# Eval("nettotal") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDistance" runat="server" Text='<%# Eval("distance") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCustCode" runat="server" Text='<%# Eval("MemberCode") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                            </tfoot>
                        </table>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="hidcol">

        <asp:HiddenField ID="hidInvNo" runat="server" />
        <asp:HiddenField ID="hidCustCode" runat="server" />
        <asp:HiddenField ID="hidInvDate" runat="server" />
        <asp:HiddenField ID="hidType" runat="server" />



        <div id="Date-Dialog" style="display: none">
            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
        </div>
        <asp:Button ID="btnDate" runat="server" />
    </div>
</asp:Content>

