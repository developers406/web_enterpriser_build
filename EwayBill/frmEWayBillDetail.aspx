﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MainMaster.master"
    CodeBehind="frmEWayBillDetail.aspx.cs" Inherits="EnterpriserWebFinal.EWayBill.frmEWayBillDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link type="text/css" href="../css/ewaybill.css" rel="stylesheet" />
    <style type="text/css">
        .eWayDet td:nth-child(1), .eWayDet th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .eWayDet td:nth-child(2), .eWayDet th:nth-child(2) {
            min-width: 300px;
            max-width: 300px;
        }

        .eWayDet td:nth-child(3), .eWayDet th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .eWayDet td:nth-child(4), .eWayDet th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .eWayDet td:nth-child(5), .eWayDet th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .eWayDet td:nth-child(6), .eWayDet th:nth-child(6) {
            min-width: 50px;
            max-width: 50px;
        }

        .eWayDet td:nth-child(7), .eWayDet th:nth-child(7) {
            min-width: 105px;
            max-width: 105px;
        }

        .eWayDet td:nth-child(8), .eWayDet th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .eWayDet td:nth-child(9), .eWayDet th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .eWayDet td:nth-child(10), .eWayDet th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .eWayDet td:nth-child(11), .eWayDet th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }




        .lblcolor {
            color: #125f81;
        }

        .lblvalue {
            color: black;
        }

        .lblheading {
            color: white;
        }

        .mandatory {
            color: red;
        }

        .mandatoryGSTR {
            color: green;
        }
    </style>

    <script type="text/javascript">
        function pageLoad() {
            try {
                $("#<%= txtTPDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function ValidateForm() {
            var ToPincode = $("<%=txtToPincode.ClientID%>").val();
            var ToGST = $("<%=txtToGSTN.ClientID%>").val();
            if (parseFloat(ToPincode) == NaN && ToPincode.lenght != 6) {
                fncToastError("Please Enter Only Number and in Correct PinCode Format");
                ToPincode.select();
                return false;
            } else if (parseFloat(ToPincode) == NaN) {
                fncToastError("Please Enter Only Number and in Correct State Code Format");
                ToPincode.select();
                return false;
            }

            else {
                $("input").removeAttr('disabled');
                __doPostBack('ctl00$ContentPlaceHolder1$lnkbtnSubmit', '')
                return true;
            }
        }
        function fncShowSaveDialog(message) {
            $(function () {
                $("#dialog-Save").html(message);
                $("#dialog-Save").dialog({
                    title: "Enterpriser Web",
                    width: "auto",
                    buttons: {
                        Ok: function () {
                            $(this).dialog("destroy");
                            fncGoEwaySummary();
                        }
                    },
                    modal: true
                });
            });
        };

        function fncGoEwaySummary() {
            try {

               //var file = "../JSONFile/" + $.trim($("#<%=txtdocNo.ClientID%>").val()) + ".json";
               var basePath = '<%= Page.ResolveUrl("~/JSONFile/")%>';
               var uri = basePath + $.trim($("#<%=txtdocNo.ClientID%>").val()) + ".json";
               var link = document.createElement("a");
               link.download = $.trim($("#<%=txtdocNo.ClientID%>").val()) + ".json";
               link.href = uri;
               link.click();
               $("#<%=btnURL.ClientID%>").click();


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSubmitValidation() {
            debugger;
            try {
                var ToPincode = $("<%=txtToPincode.ClientID%>").val();
                var ToGST = $("<%=txtToGSTN.ClientID%>").val();
                if ($("#<%= txtTranId.ClientID %>").val() == "" && $("#<%= txtDistance.ClientID %>").val() == "" &&
                    $("#<%= txtVehileNo.ClientID %>").val() == "") {
                    popUpObjectForSetFocusandOpen = $("#<%=txtDistance.ClientID%>");
                    ShowPopupMessageBoxandFocustoObject("Please Enter Transporter details Or Part-Detail");
                    return false;
                }
                else if ($("#<%= txtTranId.ClientID %>").val() == "" && $("#<%= txtDistance.ClientID %>").val() == "" &&
                    $("#<%= txtVehileNo.ClientID %>").val() != "") {
                    popUpObjectForSetFocusandOpen = $("#<%=txtDistance.ClientID%>");
                    ShowPopupMessageBoxandFocustoObject("Please Enter Approximate Distance (in KM)");
                    return false;
                }
                else if ($("#<%= txtTranId.ClientID %>").val() == "" && $("#<%= txtDistance.ClientID %>").val() != "" &&
                    $("#<%= txtVehileNo.ClientID %>").val() == "") {
                    popUpObjectForSetFocusandOpen = $("#<%=txtVehileNo.ClientID%>");
                    ShowPopupMessageBoxandFocustoObject("Please Enter Vehicle No");
                    return false;
                }

                else if (parseInt($("#<%=txtToPincode.ClientID%>").val().length) < parseInt(6)) {

                    $("#<%=txtToPincode.ClientID%>").focus().select();

                    fncToastError("Please Enter Only Number and in Correct PinCode Format");

                    return false;
                } else if ($("<%=txtToGSTN.ClientID%>").val() == "") {

                    $("#<%=txtToGSTN.ClientID%>").focus().select();

                    fncToastError("Please Enter GST Number");
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <div>
    <iframe id="my_iframe" style="display:none;"></iframe>
    </div>--%>
    <div class ="EnableScroll_EInvoice" style="overflow-y:auto;height:536px">
        <div class="col-md-12 bg-primary" style="margin-top: 10px;">
            <center>
                <h3>
                    <label class="lblheading">e- WayBill JSON Generation </label>
                </h3>
            </center>
        </div>
        <div class="form-inline text-right ">
            [ <span class="mandatory">*</span>indicates mandatory fields for E-Way Bill <span style="width: 10px"></span>and
      <span class="mandatoryGSTR">*</span>indicates mandatory fields for GSTR-1]
        </div>
        <div class="para col-md-12">
            <div class="col-md-12 bg-primary">
                <label class="lblheading">Transaction Details</label>
            </div>
            <br />
            <div class="col-md-3">
                <table>
                    <tr>
                        <td>
                            <label style="padding-right: 10px;" class="lblcolor">Transaction Type<span class="mandatory">*</span></label></td>
                        <td>

                            <%--<input type="radio" /><label class="lblvalue">Outward</label></td>--%>
                            <asp:RadioButton ID="rbnOutward" runat="server" Text="Outward" Checked="true" />
                    </tr>
                </table>



            </div>
            <div class="col-md-3">


                <table style="">
                    <tr>
                        <td style="">
                            <label style="padding-right: 10px;" class="lblcolor">Sub Type <span class="mandatory">*</span></label></td>
                        <td style="">
                            <%--<input type="radio" /><label class="lblvalue">Supply</label></td>--%>
                            <asp:RadioButton ID="rbnSupply" runat="server" Checked="true" />
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3">
                <label class="lblcolor">Document Type<span class="mandatory">*</span></label>
                <asp:TextBox ID="txtdocType" runat="server" ReadOnly="true" Text="Tax Invoice" CssClass="form-control1"></asp:TextBox>
            </div>
            <div class="col-md-3">
                <label class="lblcolor">Document No<span class="mandatory">*</span></label>
                <asp:TextBox ID="txtdocNo" runat="server" ReadOnly="true" CssClass="form-control1"></asp:TextBox>
            </div>
            <div class="col-md-3">
                <label class="lblcolor">Document Date<span class="mandatory">*</span></label>
                <asp:TextBox ID="txtdocDate" runat="server" ReadOnly="true" CssClass="form-control1"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12">
            <div class="bg-primary col-md-6 " style="margin-bottom: 10px;">
                <label class="lblheading">Bill From</label>
            </div>

            <div class="bg-primary col-md-6 " style="margin-bottom: 10px;">
                <label class="lblheading">Despatch From</label>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    <label class="lblcolor">Name</label>
                    <asp:TextBox ID="txtfrmName" runat="server" CssClass="form-control1"></asp:TextBox>
                </div>
                <div class="col-md-12">
                    <label class="lblcolor">GSTIN<span class="mandatory">*</span></label>
                    <asp:TextBox ID="txtfrmGSTN" runat="server" CssClass="form-control1"></asp:TextBox>

                </div>
                <div class="col-md-12">
                    <label class="lblcolor">State<span class="mandatory">*</span></label>
                    <br />
                    <%--<asp:TextBox ID="txtfrmState" runat="server" onkeydown="return isNumberKey(event);" CssClass="form-control1"></asp:TextBox>--%>
                    <asp:DropDownList ID="ddlfrmState" CssClass="form-control1" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    <label class="lblcolor">Address</label>
                    <asp:TextBox ID="txtfrmAdd1" runat="server" Style="margin-bottom: 10px" CssClass="form-control1"></asp:TextBox>
                    <asp:TextBox ID="txtfrmAdd2" runat="server" CssClass="form-control1"></asp:TextBox>
                </div>
                <div class="col-md-12">
                    <label class="lblcolor">Place</label>
                    <asp:TextBox ID="txtfrmPlace" runat="server" CssClass="form-control1"></asp:TextBox>

                </div>

                <div class="col-md-6">
                    <label class="lblcolor">PinCode<span class="mandatory">*</span></label>
                    <asp:TextBox ID="txtfrmPincode" MaxLength="6" runat="server" onkeydown="return isNumberKey(event);" CssClass="form-control1"></asp:TextBox>

                </div>
                <div class="col-md-6">
                    <label class="lblcolor">State</label>
                    <asp:DropDownList ID="ddlactualFromStateCode" CssClass="form-control1" runat="server">
                    </asp:DropDownList>

                </div>
            </div>
        </div>
        <div class="col-md-12">

            <div class="bg-primary col-md-6" style="margin-bottom: 10px;">
                <label class="lblheading">Bill TO</label>
            </div>
            <div class="bg-primary col-md-6 " style="margin-bottom: 10px;">
                <label class="lblheading">Ship To</label>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    <label class="lblcolor">Name</label>
                    <asp:TextBox ID="txtToName" runat="server" CssClass="form-control1"></asp:TextBox>
                </div>
                <div class="col-md-12">
                    <label class="lblcolor">GSTIN<span class="mandatory">*</span></label>
                    <asp:TextBox ID="txtToGSTN" runat="server" CssClass="form-control1"></asp:TextBox>

                </div>
                <div class="col-md-12">
                    <label class="lblcolor">State</label>
                    <br />
                    <%--<asp:TextBox ID="txtToState" runat="server" onkeydown="return isNumberKey(event);" CssClass="form-control1"></asp:TextBox>--%>
                    <asp:DropDownList ID="ddlToState" CssClass="form-control1" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    <label class="lblcolor">Address</label>
                    <asp:TextBox ID="txtToAdd1" runat="server" Style="margin-bottom: 10px" CssClass="form-control1"></asp:TextBox>
                    <asp:TextBox ID="txtToAdd2" runat="server" CssClass="form-control1"></asp:TextBox>

                </div>
                <div class="col-md-12">
                    <label class="lblcolor">Place</label>
                    <asp:TextBox ID="txtToPlace" runat="server" CssClass="form-control1"></asp:TextBox>

                </div>

                <div class="col-md-6">
                    <label class="lblcolor">PinCode<span class="mandatory">*</span></label>
                    <asp:TextBox ID="txtToPincode" MaxLength="6" runat="server" onkeydown="return isNumberKey(event);" CssClass="form-control1"></asp:TextBox>

                </div>
                <div class="col-md-6">
                    <label class="lblcolor">State</label>
                    <br />
                    <%--<asp:TextBox ID="txtToState" runat="server" onkeydown="return isNumberKey(event);" CssClass="form-control1"></asp:TextBox>--%>
                    <asp:DropDownList ID="ddlactualToStateCode" CssClass="form-control1" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class=" col-md-12">
            <div class="bg-primary col-md-12" style="margin-bottom: 10px;">
                <label class="lblheading">Item Details</label>
            </div>

            <div class="col-md-12" style="margin-bottom: 10px;">
                <div id="divrepeater" class="table_headers  eWayDet">
                    <table id="tblEWayDet" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                <th scope="col">Product Name
                                </th>
                                <th scope="col">Description
                                </th>
                                <th scope="col">HSN
                                </th>
                                <th scope="col">Quantity
                                </th>
                                <th scope="col">Unit
                                </th>
                                <th scope="col">Taxable Value
                                </th>
                                <th scope="col">CGST
                                </th>
                                <th scope="col">SGST
                                </th>
                                <th scope="col">IGST
                                </th>
                                <th scope="col">Cess
                                </th>

                            </tr>
                        </thead>
                        <asp:Repeater ID="rptreWayDet" runat="server">
                            <HeaderTemplate>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("SNO") %>' />
                                    </td>
                                    <td runat="server">
                                        <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("Name") %>' />
                                    </td>
                                    <td runat="server">
                                        <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("Desc") %>' />
                                    </td>
                                    <td runat="server">
                                        <asp:Label ID="lblHSN" runat="server" Text='<%# Eval("vatgroupcode") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblQty" runat="server" Text='<%# Eval("Qty") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOM") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTaxablevalue" runat="server" Text='<%# Eval("TaxableValue") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCGST" runat="server" Text='<%# Eval("CGST") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSGST" runat="server" Text='<%# Eval("SGST") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblIGST" runat="server" Text='<%# Eval("IGST") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCess" runat="server" Text='<%# Eval("cess") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </FooterTemplate>
                        </asp:Repeater>
                        <tfoot>
                        </tfoot>
                    </table>

                </div>
            </div>

            <div class="col-md-2">
                <label class="lblcolor">Total Amt/Tax'ble Amt</label>
                <asp:TextBox ID="txttableAmt" ReadOnly="true" runat="server" CssClass="form-control1"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <label class="lblcolor">CGST Amount<span class="mandatoryGSTR">*</span></label>
                <asp:TextBox ID="txtCGSTAmt" ReadOnly="true" runat="server" CssClass="form-control1"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <label class="lblcolor">SGST Amount<span class="mandatoryGSTR">*</span></label>
                <asp:TextBox ID="txtSGSTAmt" ReadOnly="true" runat="server" CssClass="form-control1"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <label class="lblcolor">IGST Amount<span class="mandatoryGSTR">*</span></label>
                <asp:TextBox ID="txtIGSTAmt" ReadOnly="true" runat="server" CssClass="form-control1"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <label class="lblcolor">CESS Amount<span class="mandatoryGSTR">*</span></label>
                <asp:TextBox ID="txtCessAmt" ReadOnly="true" runat="server" CssClass="form-control1"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <label class="lblcolor">Total Inv. Value<span class="mandatoryGSTR">*</span></label>
                <asp:TextBox ID="txtTotalInvoiceValue" ReadOnly="true" runat="server" CssClass="form-control1"></asp:TextBox>
            </div>

        </div>
        <div class=" col-md-12">
            <div class="bg-primary col-md-12 " style="margin-bottom: 10px;">
                <label class="lblheading">Transporter Details</label>
            </div>

            <div class="col-md-12">
                <table>
                    <tr>
                        <td>
                            <label style="padding-right: 10px;" class="lblcolor">Mode<span class="mandatory">*</span> </label>
                        </td>
                        <td>
                            <asp:RadioButton runat="server" GroupName="Transport" Checked="true" />
                            <label style="padding-right: 10px;" class="lblvalue">Road</label></td>
                        <td>

                            <asp:RadioButton runat="server" GroupName="Transport" /><label style="padding-right: 10px;" class="lblvalue">Rail</label></td>
                        <td>

                            <asp:RadioButton runat="server" GroupName="Transport" /><label style="padding-right: 10px;" class="lblvalue">Air</label></td>
                        <td>

                            <asp:RadioButton runat="server" GroupName="Transport" /><label style="padding-right: 10px;" class="lblvalue">Ship</label></td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <td>
                            <label style="padding-right: 10px;" class="lblcolor">Vehicle Type<span class="mandatory">*</span> </label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rbtnVehicleRegular" runat="server" GroupName="Vehicle" Checked="true" />
                            <label style="padding-right: 10px;" class="lblvalue">Regular</label></td>
                        <td>

                            <asp:RadioButton ID="rbtnVehicleODG" runat="server" GroupName="Vehicle" />
                            <label style="padding-right: 10px;" class="lblvalue">Over Dimensional Cargo</label></td>

                    </tr>
                </table>

            </div>
            <div class="col-md-12" style="background-color: burlywood">
                <div class="col-md-5">
                    <div class="col-md-12">
                        <label class="lblcolor">Transporter Name </label>
                        <asp:TextBox ID="txtTranName" runat="server" CssClass="form-control1"></asp:TextBox>
                    </div>
                    <div class="col-md-12">
                        <label class="lblcolor">Transporter ID<span class="mandatory">*</span></label>
                        <asp:TextBox ID="txtTranId" runat="server" CssClass="form-control1"></asp:TextBox>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <label class="lblcolor">TP No & Date</label>
                        </div>

                        <div class="col-md-5">
                            <div class="col-md-9">
                                <asp:TextBox ID="txtTPNo" runat="server" CssClass="form-control1"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <a style="display: none" class="agile-icon" href=""><i class="fa fa-plus-square" aria-hidden="true"></i></a>
                            </div>

                        </div>
                        <div class="col-md-5">
                            <div class="col-md-9">
                                <asp:TextBox ID="txtTPDate" runat="server" CssClass="form-control1"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <a style="display: none" class="agile-icon" href=""><i class="fa fa-minus-square" aria-hidden="true"></i></a>

                            </div>

                        </div>



                    </div>

                </div>
                <div class="col-md-2">
                    <div>
                        <center>
                            <i class="fa fa-arrow-left" aria-hidden="true"></i>OR <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </center>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="col-md-12">
                        <label class="lblcolor">Approximate Distance (in KM)<span class="mandatory">*</span></label>
                        <asp:TextBox ID="txtDistance" runat="server" onkeydown="return isNumberKey(event);" CssClass="form-control1"></asp:TextBox>
                    </div>
                    <div class="col-md-12" style="background-color: burlywood">
                        <div class="col-md-12 bg-primary">
                            <center>
                                <label class="lblheading">Part - B</label>
                            </center>
                        </div>
                        <label class="lblcolor">Vehicle No.<span class="mandatory">*</span></label>
                        <asp:TextBox ID="txtVehileNo" runat="server" CssClass="form-control1"></asp:TextBox>
                    </div>

                </div>

            </div>
            <div class="col-md-12" style="margin-top: 10px; margin-bottom: 10px;">
                <center>
                    <asp:LinkButton ID="lnkbtnSubmit" CssClass="btn btn-primary" runat="server" OnClientClick=" return fncSubmitValidation();" OnClick="lnkbtnSubmit_click">Generate Json</asp:LinkButton>
                    <asp:LinkButton ID="lnkbtnExit" CssClass="btn btn-primary" runat="server" OnClick="lnkbtnExit_click">Back</asp:LinkButton>
                </center>
            </div>
            <div class="hidcol">
                <asp:HiddenField ID="hidfrmStatecode" runat="server" />
                <asp:HiddenField ID="hidtoStatecode" runat="server" />
                <asp:HiddenField ID="hidMainhsncode" runat="server" />
                <div id="dialog-Save" style="display: none">
                </div>
                <asp:Button ID="btnURL" runat="server" OnClick="lnkbtnExit_click" />
            </div>

        </div>
    </div>
</asp:Content>
