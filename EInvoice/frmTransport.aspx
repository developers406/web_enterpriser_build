﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" CodeBehind="frmTransport.aspx.cs" Inherits="EnterpriserWebFinal.EInvoice.frmTransport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../css/einvoice.css" rel="stylesheet" />
    <script type="text/javascript" src="../js/jquery.number.js"></script>
    <script type="text/javascript">
        function AlerSave(msg) {
            fncClear();
            ShowPopupMessageBoxSave("Saved Successfully - " + msg);
            return false;
        } 

        $(document).ready(function () {
            $('#<%=txtDistance.ClientID%>').number(true, 0);
        });

        function fncClear() {
            $('#<%=txtTransportId.ClientID%>').val('');
            $('#<%=txtTransportName.ClientID%>').val('');
            $('#<%=txtDistance.ClientID%>').val('0');
            $('#<%=txtVehicleNumber.ClientID%>').val('');
            $('#<%=hidTransportId.ClientID %>').val('');
            $('#<%=txtTransportId.ClientID %>').prop("disabled", false);
            $('#<%=chkActive.ClientID %>').prop("checked", false);
            $('#<%=txtTransportId.ClientID%>').focus();
        }

        function fncSaveValidation() {
            try {
                var show = '';
                var TransportName = $.trim($('#<%=txtTransportName.ClientID %>').val());   //22112022 musaraf

                if ($('#<%=txtTransportId.ClientID%>').val() == "" || $('#<%=txtTransportId.ClientID%>').val().length != 15) {
                    show = "Please Enter Valid Transport GSTNo and Length Must be 15."
                }
                if (TransportName == "") {
                        Show = Show + '<br />Please Enter Transport Name';
                        document.getElementById("<%=txtTransportName.ClientID %>").focus();
                }
                if ($('#<%=txtDistance.ClientID%>').val() == "" || parseFloat($('#<%=txtDistance.ClientID%>').val()) <= 0) {
                    show = show + "<br />Please Enter Transport Distance."
                }
                if ($('#<%=txtVehicleNumber.ClientID%>').val() == "") {
                    show = show + "<br />Please Enter Transport Vechicle NUmber."
                }
                if (show != "") {
                    ShowPopupMessageBox(show);
                    return false;
                }
                else
                    return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function imgbtnEdit_ClientClick(source) {
            fncClear();
            DisplayDetails($(source).closest("tr"));
        }

        function DisplayDetails(row) {
            $('#<%=txtTransportId.ClientID %>').prop("disabled", true);
            $('#<%=hidTransportId.ClientID %>').val(row.find("td span[id*=lblTransporterId]").text().replace(/&amp;/g, '&'));
            $('#<%=txtTransportId.ClientID %>').val(row.find("td span[id*=lblTransporterId]").text().replace(/&amp;/g, '&'));
            $('#<%=txtTransportName.ClientID %>').val(row.find("td span[id*=lblTransportName]").text().replace(/&amp;/g, '&'));
            $('#<%=txtDistance.ClientID %>').val(row.find("td span[id*=lblDistance]").text().replace(/&amp;/g, '&'));
            $('#<%=txtVehicleNumber.ClientID %>').val(row.find("td span[id*=lblVehicle]").text().replace(/&amp;/g, '&'));
            var Active = (row.find("td span[id*=lblActive]").text().replace(/&amp;/g, '&'));
            if (Active == "1") {
                $('#<%=chkActive.ClientID %>').prop("checked", true);
            }
            else {
                $('#<%=chkActive.ClientID %>').prop("checked", false);
            }
        }
        function ShowPopupMessageBoxSave(message) {
            $(function () {
                $("#dialog-MessageBox").html(message);
                $("#dialog-MessageBox").dialog({
                    title: "E-Invoice",
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("destroy");
                            if (i == 0)
                                window.location.href = "../Forms/frmInvoiceSummary.aspx";
                        }
                    },
                    modal: true
                });
            });
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="eWay_Summry">
        <div class="breadcrumbs col-md-12">
            <ul>
                <li><a href="../Forms/frmDashboard.aspx">Home</a><span class="glyphicon glyphicon-chevron-right"></span>
                </li>
                <li><a>Forms</a><span class="glyphicon glyphicon-chevron-right"></span>
                <li id="invhdr" class="header_cls">Transport Creation</li>
            </ul>
        </div>
        <div class="ewaybill_header2">
            <div class="col-md-6 border" style="margin-top: 20px">
                <div class="col-md-12">
                    <div class="prepare col-md-5">
                        <asp:Label ID="lblTransportId" runat="server" Text="TransportID(GSTNo)"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="prepare col-md-7">
                        <asp:TextBox ID="txtTransportId" MaxLength="15" runat="server" CssClass="full_width"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 margin_10">
                    <div class="prepare col-md-5">
                        <asp:Label ID="lbl" runat="server" Text="Transport Name"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="prepare col-md-7">
                        <asp:TextBox ID="txtTransportName" runat="server" CssClass="full_width"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 margin_10">
                    <div class="prepare col-md-5">
                        <asp:Label ID="lblDistance" runat="server" Text="Distance"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="prepare col-md-7">
                        <asp:TextBox ID="txtDistance" runat="server" CssClass="full_width" Text="0" onfocus="this.select();"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 margin_10">
                    <div class="prepare col-md-5">
                        <asp:Label ID="lblVehicleNo" runat="server" Text="Vehicle No"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="prepare col-md-7">
                        <asp:TextBox ID="txtVehicleNumber" runat="server" CssClass="full_width"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 margin_10">
                    <div class="prepare col-md-5">
                        <asp:Label ID="lblActive" runat="server" Text="Active"></asp:Label>
                    </div>
                    <div class="prepare col-md-7">
                        <asp:CheckBox ID="chkActive" runat="server" CssClass="full_width"></asp:CheckBox>
                    </div>
                </div>
                <div class="col-md-12 margin_10">
                    <div class="prepare col-md-5"></div>
                    <div class="prepare col-md-7">
                        <div class="button-contol" style="float: right;">
                            <asp:LinkButton ID="lnkSave" CssClass="btn btn-primary" runat="server" OnClientClick="return fncSaveValidation();" OnClick="lnkSave_click">Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBack" CssClass="btn btn-primary" runat="server" OnClientClick="fncClear();return false;">Clear</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 margin_10">
                <div id="divrepeater" class="table_headers  eWayDetail eWaysum_new">
                    <table id="tblEWayDetail" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                <th scope="col">Edit
                                </th>
                                <th scope="col">Transport ID
                                </th>
                                <th scope="col">Transport Name
                                </th>
                                <th scope="col">Distance
                                </th>
                                <th scope="col">Vehicle Number 
                                </th>
                                <th scope="col">Active 
                                </th>
                            </tr>
                        </thead>
                        <asp:Repeater ID="rptrEInvoiceDetail" runat="server">
                            <HeaderTemplate>
                                <tbody style ="height:215px;">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr runat="server" onclick="fncRowClick(this);">
                                    <td>
                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                    </td>
                                    <td runat="server">
                                        <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                            ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />
                                    </td>
                                    <td runat="server">
                                        <asp:Label ID="lblTransporterId" runat="server" Text='<%# Eval("TransporterId") %>' />
                                    </td>
                                    <td runat="server">
                                        <asp:Label ID="lblTransportName" runat="server" Text='<%# Eval("TransporteName") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDistance" runat="server" Text='<%# Eval("Distance") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblVehicle" runat="server" Text='<%# Eval("VehicleNumber") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblActive" runat="server" Text='<%# Eval("Active") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </FooterTemplate>
                        </asp:Repeater>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
            <asp:HiddenField ID ="hidTransportId" runat ="server" />
        </div>
    </div>
    <div id="dialog-MessageBox" style="display: none">
    </div>
</asp:Content>
