﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmIrnDetails.aspx.cs"  Async="true" Inherits="EnterpriserWebFinal.EInvoice.frmIrnDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/einvoice.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-2");
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
            else {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            }
        });

        function fncGetIrnNo(source) {
            try {
                var rowobj = $(source).parent().parent();  
                if ($(rowobj).find('td span[id*="lblInvNo"]').text().trim() != '') {
                    $("#<%= hidIrnNo.ClientID %>").val($(rowobj).find('td span[id*="lblInvNo"]').text().trim());
                    return true;
                }
                else
                    return false;
            } 
            catch (err) {
                alert(err.message);
            }
        }

        function Showpopup(msg) {
            i = 1;
            ShowPopupMessageBoxSave(msg);

        }
        function ShowPopupMessageBoxSave(message) {
            $(function () {
                $("#dialog-MessageBox").html(message);
                $("#dialog-MessageBox").dialog({
                    title: "E-Invoice",
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("destroy"); 
                        }
                    },
                    modal: true
                });
            });
            return false;
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="eWay_Summry">
        <div class="breadcrumbs col-md-12">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a><span class="glyphicon glyphicon-chevron-right"></span></li>
                <li><a>Add On</a><span class="glyphicon glyphicon-chevron-right"></span></li>
                <li id="invhdr" class="header_cls">IRN Detail</li>
            </ul>
        </div>
        <div class="display_table">
        </div>
        <div class="ewaybill_header2">
            <div class="float_left border_delType border">
                <div class="prepare">
                    <asp:Label ID="Label1" runat="server" Text="From Date"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="eway_minValue"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:Label ID="Label2" runat="server" Text="To Date"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="eway_minValue"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:Label ID="Label3" runat="server" Text="IRN No"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:TextBox ID="txtIrnNo" style ="width: 200px;" runat="server" CssClass="eway_minValue"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:LinkButton ID="lnkSearch" CssClass="btn btn-primary" runat="server" OnClick="lnkSearch_Click">Search</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="display_table">
            <div id="divrepeater" class="table_headers  eWaysumIrn eWaysum_new">
                <table id="tblEWayDetail" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">IRN No
                            </th>
                            <th scope="col">Invoice No
                            </th>
                            <th scope="col">IRN Date
                            </th> 
                            <th scope="col" >Print
                            </th>
                        </tr>
                    </thead>
                    <asp:Repeater ID="rptrEInvoiceDetail" runat="server">
                        <HeaderTemplate>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr runat="server">
                                <td>
                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                </td>
                                <td runat="server">
                                    <asp:Label ID="lblInvNo" runat="server" Text='<%# Eval("IrnNo") %>' />
                                </td>
                                <td runat="server">
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("InvoiceNo") %>' />
                                </td>
                                <td runat="server">
                                    <asp:Label ID="lblInvDate" runat="server" Text='<%# Eval("Createdate") %>' />
                                </td>
                                <td runat="server" >
                                    <asp:Button runat="server" OnClientClick="return fncGetIrnNo(this);" Text="Print" OnClick="btnPrint_Click" /> 
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                        </FooterTemplate>
                    </asp:Repeater>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidIrnNo" runat ="server" />
    <asp:Image ID="imgpreview" runat="server" style ="display:none;" />   
    <div id="dialog-MessageBox" style="display: none">
        </div>
</asp:Content>
