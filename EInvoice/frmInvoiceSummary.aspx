﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmInvoiceSummary.aspx.cs" Inherits="EnterpriserWebFinal.EInvoice.frmInvoiceSummery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">  
    <link href="../css/einvoice.css" rel="stylesheet" />
     
    <script type="text/javascript">
        $(document).ready(function () {
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-2");
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
            else {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            }
        });  
    </script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="eWay_Summry">
        <div class="breadcrumbs col-md-12">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a><span class="glyphicon glyphicon-chevron-right"></span></li>
                <li><a>Add On</a><span class="glyphicon glyphicon-chevron-right"></span></li>
                <li id="invhdr" class="header_cls">Invoice Summary</li>
            </ul>
        </div>
        <div class="display_table">
        </div>
        <div class="ewaybill_header2">
            <div class="float_left border_delType border">
                <div class="prepare">
                    <asp:Label ID="lblLoc" runat="server" Text="Location"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="ddl_width"></asp:DropDownList>
                </div>
            </div>


            <div class="float_left border_delType border">
                <div class="prepare">
                    <asp:Label ID="Label1" runat="server" Text="From Date"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="eway_minValue"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:Label ID="Label2" runat="server" Text="To Date"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="eway_minValue"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:Label ID="Label3" runat="server" Text="Invoice No"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="eway_minValue"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:LinkButton ID="lnkSearch" CssClass="btn btn-primary" runat="server" OnClick="lnkSearch_Click">Search</asp:LinkButton>
                </div>
            </div>

        </div>
        <div class="display_table">
            <div id="divrepeater" class="table_headers  eWaysum eWaysum_new">
                <table id="tblEWaySummary" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">InvoiceNo
                            </th>
                            <th scope="col">Inv.Date
                            </th>
                            <th scope="col">CustomerName
                            </th>
                            <th scope="col">Value
                            </th>
                            <th scope="col">Location
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <asp:Repeater ID="rptrEInvoiceSummary" runat="server">
                        <HeaderTemplate>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr runat="server" onclick="fncRowClick(this);">
                                <td>
                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                </td>
                                <td runat="server">
                                    <asp:Label ID="lblInvNo" runat="server" Text='<%# Eval("InvoiceNo") %>' />
                                </td>
                                <td runat="server">
                                    <asp:Label ID="lblInvDate" runat="server" Text='<%# Eval("InvoiceDate") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblCustName" runat="server" Text='<%# Eval("CustomerName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblValue" runat="server" Text='<%# Eval("NetTotal") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("LocationCode") %>' />
                                </td> 
                                <td runat="server">
                                    <asp:Button runat="server" Text="Go" OnClick ="lnkGo_Click"/>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                        </FooterTemplate>
                    </asp:Repeater>
                    <tfoot>
                    </tfoot>
                </table>

            </div>
        </div>
    </div> 
</asp:Content>
