﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmEInvoiceDetail.aspx.cs" Inherits="EnterpriserWebFinal.EInvoice.frmEInvoiceDetail" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/einvoice.css" rel="stylesheet" />
    <script type ="text/javascript">
        var i = 0;
        function AlerSave(msg) { 
            i = 0;
            ShowPopupMessageBoxSave("Saved Successfully. Your Irn No is - " + msg); 
        }
        function Exists(msg) { 
            i = 0;
            ShowPopupMessageBoxSave("Already Exists. Your Irn No is - " + msg); 

        }
        function Error() {
            i = 1;
            ShowPopupMessageBoxSave("Invalid Details Please Check Log NotePad File.");  
        }
        function PinError(msg) {
            i = 1;
            ShowPopupMessageBoxSave("Invalid " + msg +". Please Enter Valid Pin.");
        }
        function Showpopup(msg) {
            i = 1;
            ShowPopupMessageBoxSave(msg);

        }
        function ShowPopupMessageBoxSave(message) {
            $(function () {
                $("#dialog-MessageBox").html(message);
                $("#dialog-MessageBox").dialog({
                    title: "E-Invoice",
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("destroy");
                            if(i==0)
                            window.location.href = "../EInvoice/frmInvoiceSummary.aspx";
                        }
                    },
                    modal: true
                });
            });
            return false;
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="eWay_Summry">
        <div class="breadcrumbs col-md-12">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a><span class="glyphicon glyphicon-chevron-right"></span></li>
                <li><a>Add On</a><span class="glyphicon glyphicon-chevron-right"></span></li>
                <li><a href="../EInvoice/frmInvoiceSummary.aspx">Invoice Summary</a><span class="glyphicon glyphicon-chevron-right"></span></li>
                <li id="invhdr" class="header_cls">Invoice Detail</li>
            </ul>
        </div>
        <div class="display_table"> 
            <asp:Label ID="lblCOmapanyName" style ="font-size: 15px;font-weight: bold;color: red;" runat="server"></asp:Label>
        </div>
        <div class="ewaybill_header2">


            <div class="float_left border_delType border">
                <div class="prepare">
                    <asp:Label ID="Label1" runat="server" Text="Location"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:TextBox ID="txtLocation" runat="server" CssClass="eway_minValue" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:Label ID="Label3" runat="server" Text="Invoice No"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:TextBox ID="txtInvoiceNo" Style="width: 150px;" runat="server" CssClass="eway_minValue" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:Label ID="Label5" runat="server" Text="Transport"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:DropDownList ID="ddlTransport" runat="server" CssClass="ddl_width"></asp:DropDownList>
                     <asp:TextBox ID="txtInvDate" runat="server" style ="display:none;" CssClass="eway_minValue" ReadOnly="true"></asp:TextBox> 
                </div>
            </div>
            <div class="float_left border_delType border">
                <div class="prepare">
                    <asp:Label ID="Label2" runat="server" Text="CustName"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:TextBox ID="txtCustName" runat="server" CssClass="eway_minValue" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="prepare">
                    <asp:Label ID="Label4" runat="server" Text="Total"></asp:Label>
                </div>
                <div class="prepare">
                    <asp:TextBox ID="txtNetTotal" runat="server" CssClass="eway_minValue" ReadOnly="true"></asp:TextBox>
                </div>

            </div>
        </div>
        <div class="display_table">
            <div id="divrepeater" class="table_headers  eWayDetail eWaysum_new">
                <table id="tblEWayDetail" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">ItemCode
                            </th>
                            <th scope="col">BatchNo
                            </th>
                            <th scope="col">Description
                            </th>
                            <th scope="col">Qty
                            </th>
                            <th scope="col">Price
                            </th>
                            <th scope="col">TotalValue
                            </th>
                        </tr>
                    </thead>
                    <asp:Repeater ID="rptrEInvoiceDetail" runat="server">
                        <HeaderTemplate>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr runat="server" onclick="fncRowClick(this);">
                                <td>
                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                </td>
                                <td runat="server">
                                    <asp:Label ID="lblInvNo" runat="server" Text='<%# Eval("InventoryCode") %>' />
                                </td>
                                <td runat="server">
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("BatchNo") %>' />
                                </td>
                                <td runat="server">
                                    <asp:Label ID="lblInvDate" runat="server" Text='<%# Eval("Description") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblCustName" runat="server" Text='<%# Eval("Qty") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblValue" runat="server" Text='<%# Eval("Price") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("TotalValue") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                        </FooterTemplate>
                    </asp:Repeater>
                    <tfoot>
                    </tfoot>
                </table>

            </div>
        </div>
        <div class="col-md-12"">
            <div class="col-md-10">
                <asp:LinkButton ID="lnkBack" CssClass="btn btn-primary" style ="float:right;" runat="server" PostBackUrl="../EInvoice/frmInvoiceSummary.aspx">Back</asp:LinkButton>
            </div>
            <div class="col-md-2">
                <asp:LinkButton ID="lnkSave" CssClass="btn btn-primary" runat="server" OnClick="lnkSave_Click">Update</asp:LinkButton>
            </div>
        </div>
    </div>
     <div id="dialog-MessageBox" style="display: none">
        </div>
</asp:Content>
