﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="frmMonthWisePurchase.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmMonthWisePurchase" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript"> 
        
    </script>
</asp:Content>
<%--EnableFromDateTextBox="true" EnableToDateTextBox="true"  OnFilterButtonClick="lnkLoadFilter_Click"  EnableFilterButton="true" --%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-group-price">
        <%--<div class="container-left-price" id="FilterDialog" runat="server">
            <ups:SearchListFilterUserControl runat="server" ID="searchListFilterUserControl"
                EnablelocationDropDown="true" EnableVendorDropDown="true" EnableDepartmentDropDown="true"
                EnableCategoryDropDown="true" EnableSubCategoryDropDown="true" EnableBrandDropDown="true"
                EnableClassDropDown="true" EnableSubClassDropDown="true" EnableFloorDropDown="true"
                EnableSectionDropDown="true" EnableBinDropDown="true" EnableShelfDropDown="true"
                EnableWarehouseDropDown="true" EnableItemTypeDropDown="true" EnableClearButton="true"
                OnClearButtonClientClick="clearForm(); return false;" />
        </div>--%>
        <div class="container-pers-top-header-invoice">
            <div class="container-pers-header-left" style="width: 60%;">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-left">
                            <asp:Label ID="Label29" runat="server" Text="Location" CssClass="label-invoice"></asp:Label><span
                                class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                           <dx:ASPxComboBox ID="ddlLocation" CssClass="form-control" runat="server" Theme="SoftOrange"
                                IncrementalFilteringMode="StartsWith">
                            </dx:ASPxComboBox>
                        </div>
                    </div>
                    <div class="control-group-right" style="width: 38%">
                        <div class="label-left">
                            <asp:Label ID="Label31" runat="server" CssClass="label-invoice" Text="No of Months"></asp:Label>
                        </div>
                        <div class="label-right" style="color: Black">
                           <dx:ASPxComboBox ID="ddlMonth" CssClass="form-control" runat="server" Theme="SoftOrange"
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True">
                               <Items>
                                   <dx:ListEditItem Text="3" Value="3" />
                                   <dx:ListEditItem Text="6" Value="6" />
                                   <dx:ListEditItem Text="9" Value="9" />
                                   <dx:ListEditItem Text="12" Value="12" />
                               </Items>
                            </dx:ASPxComboBox>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-pers-header-right"  style="width: 40%;">
                <div class="control-group-split">
                    <div class="control-group-right">
                        <div class="label-right" style="width: 50%;">
                            <asp:LinkButton ID="lnkLoadReport" runat="server" class="button-blue" Text="Load Report" 
                                OnClick="lnkLoadReport_Click"> </asp:LinkButton>
                        </div>
                        <div class="label-left" style="width: 50%; display: none">
                            <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-red" Text="Hide Filter"> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-overflow-Invchange" id="HideFilter_GridOverFlow" runat="server">
            <dx:ASPxPivotGrid ID="pivotGridControlOS" runat="server" ClientIDMode="AutoID" EnableTheming="True"
                Theme="Office2003Blue">
            </dx:ASPxPivotGrid>
        </div>
    </div>
</asp:Content>
