﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true" CodeBehind="frmMasterProducts.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmMasterProducts" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register TagPrefix="ups" TagName="SearchListFilterUserControl" Src="~/UserControls/SearchListFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <script type="text/javascript">
        var fromdatectrl;
        var todatectrl;
        function pageLoad() {
            $("select").SumoSelect({ selectAll: false, search: true, searchText: 'Search here.' });

            $(function () {

                <%--//                fromdatectrl = $("#<%= searchListFilterUserControl.GetFilterControl<TextBox>(RetailIntelligenceWEB.UserControls.SearchListFilterUserControl.FilterControls.FromDateTextBox).ClientID %>");
                //                todatectrl = $("#<%= searchListFilterUserControl.GetFilterControl<TextBox>(RetailIntelligenceWEB.UserControls.SearchListFilterUserControl.FilterControls.ToDateTextBox).ClientID %>");--%>

                fromdatectrl = $('#<%=txtFromDate.ClientID%>');
                todatectrl = $('#<%=txtToDate.ClientID%>');

                fromdatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'light'
                });

                todatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'light'
                });


                if (fromdatectrl.val() === '') {
                    fromdatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'light'
                    });
                }

                if (todatectrl.val() === '') {
                    todatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'light'
                    });
                }
            });
        }
        function clearForm() {

            $("select").val(0);
            $("select").sumo.reload();
        }
        function ShowFilterPopup() {
            try {

                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                    $("[id*=FilterDialog]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").width("100%").height("100%");
                    $("select").trigger("liszt:updated");
                }
                else {
                    $("[id*=FilterDialog]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:74%');
                    $("select").trigger("liszt:updated");
                }

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }

            return false;
        }
        function ShowPopup() {
            $("#divExport").dialog({
                title: 'Export Option',
                width: 340,
                height: 200,
                //buttons: {
                //    OK: function () {
                //        $(this).dialog('close');
                //    }
                //},
                modal: true
            });

            return false;
        }
        function excelexport(name) {
            //alert(name  );
            if (name === "Excel")
                __doPostBack('ctl00$ContentPlaceHolder1$btnExcel', '');
            if (name === "Word")
                __doPostBack('ctl00$ContentPlaceHolder1$btnWord', '');
            if (name === "Pdf")
                __doPostBack('ctl00$ContentPlaceHolder1$btnPdf', '');

            return false;
        }
         function ValidateForm() {

            fromdatectrl = $('#<%=txtFromDate.ClientID%>');
            todatectrl = $('#<%=txtToDate.ClientID%>');

            var Show = '';
            if (fromdatectrl.val() == "") {
                Show = Show + '\n  Choose From date';
                fromdatectrl.focus();
            }

            if (todatectrl.val() == "") {
                Show = Show + '\n  Choose To date';
                todatectrl.focus();
            }

            if (Show != '') {
                alert(Show); 
                return false;
            }
             
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="container-group-price">
    <div class="container-left-price" id="FilterDialog" runat="server">
            <ups:SearchListFilterUserControl runat="server" ID="searchListFilterUserControl"
                 EnableVendorDropDown="true" EnableAttributewiseDepartmentDropDown="true"
                EnableAttributeCategoryDropDown="true" EnableSubCategoryDropDown="true" EnableBrandDropDown="true"
                EnableClassDropDown="true" EnableSubClassDropDown="true" EnableFloorDropDown="true"
                EnableSectionDropDown="true" EnableBinDropDown="true" EnableShelfDropDown="true"
                 EnableItemTypeDropDown="true" EnableClearButton="true" 
                ParamCode="AttributewiseSales"   
                OnClearButtonClientClick="clearForm(); return false;" />

       

        </div>
         <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
            <div class="container-pers-top-header">
                <div class="container-pers-header-left">
                    <div class="control-group-split">
                        <div class="control-group-left" style="width: 50%">
                            <div class="label-left">
                                <asp:Label ID="Label29" runat="server" Text="From Date" CssClass="label-invoice"></asp:Label><span
                                    class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right" style="width: 40%">
                            <div class="label-left">
                                <asp:Label ID="Label31" runat="server" CssClass="label-invoice" Text="To Date"></asp:Label>
                            </div>
                            <div class="label-right" style="color: Black">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                                                    
                            
                        </div>
                            
<div class="control-group-single-res" runat="server" id="DivStyleCode">
    <div class="label-left">
        <asp:Label ID="Label26" runat="server"  CssClass="label-invoice" Text="Style Code"></asp:Label>
    </div>
    <div class="label-right">
        <asp:TextBox ID="txtStyleCode" runat="server" CssClass="form-control-res"></asp:TextBox>
    </div>
</div>
                    </div>
                </div>
                <div class="container-pers-header-right">
                    <div class="control-group-split">
                        <div class="control-group-right">
                            <div class="label-right" style="width: 40%;">
                                <asp:LinkButton ID="lnkLoadReport" runat="server" class="button-red" Text="Load" OnClientClick = "return ValidateForm()"
                                    OnClick="lnkLoadReport_Click"> </asp:LinkButton><%--OnClick="lnkLoadReport_Click"--%>
                            </div>
                            <div class="label-left" style="width: 40%">
                                <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue"
                                    Text="Hide Filter" OnClientClick="return ShowFilterPopup()"> </asp:LinkButton>
                            </div>
                            <div class="label-right" style="width: 20%;">
                                <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" Text="Export"
                                    OnClientClick="return ShowPopup()"> </asp:LinkButton>
                            </div>
                             <div class="label-right" style="width: 60%;">
                                <asp:LinkButton ID="lnkFormat" runat="server" class="button-red" Text="Save Format "
                                    OnClick="lnkFormat_Click"> </asp:LinkButton><%--OnClick="lnkFormat_Click"--%>
                            </div>
                        </div>
                    </div>
                </div>



                </div>
             <div class="grid-overflow-Invchange" id="HideFilter_GridOverFlow" runat="server">
                <dx:ASPxPivotGrid ID="pivotGridControlOS" runat="server" ClientIDMode="AutoID" EnableTheming="True"  
                    Theme="PlasticBlue" Height="100%">
                    <OptionsPager NumericButtonCount="200" RowsPerPage="300">
                    </OptionsPager>
<StylesPrint Cell-BackColor2="" Cell-GradientMode="Horizontal" FieldHeader-BackColor2="" FieldHeader-GradientMode="Horizontal" TotalCell-BackColor2="" TotalCell-GradientMode="Horizontal" GrandTotalCell-BackColor2="" GrandTotalCell-GradientMode="Horizontal" CustomTotalCell-BackColor2="" CustomTotalCell-GradientMode="Horizontal" FieldValue-BackColor2="" FieldValue-GradientMode="Horizontal" FieldValueTotal-BackColor2="" FieldValueTotal-GradientMode="Horizontal" FieldValueGrandTotal-BackColor2="" FieldValueGrandTotal-GradientMode="Horizontal" Lines-BackColor2="" Lines-GradientMode="Horizontal"></StylesPrint>
                </dx:ASPxPivotGrid>
                <dx:ASPxPivotGridExporter ID="ASPxPivotGridExporter1" runat="server" ASPxPivotGridID="pivotGridControlOS"
                    Visible="False">
                </dx:ASPxPivotGridExporter>
            </div>
            <div class="container-bottom-invchange">
                <div class="control-button">
                </div>
            </div>
             </div>
         <div class="whole-price" id="divExport">
            <div class="control-group-full">
                <div class="label-left">
                    <asp:ImageButton ID="btnExcel" runat="server" class="button-export"
                        ImageUrl="~/Images/excel1.PNG" OnClientClick="return excelexport('Excel')"  OnClick="btnExcel_Click"/><%--OnClick="btnExcel_Click"--%>
                </div>
                <div class="label-left">
                    <asp:ImageButton ID="btnWord" runat="server" class="button-export"
                        ImageUrl="~/Images/word1.PNG" OnClientClick="return excelexport('Word')"  OnClick="btnWord_Click"/><%--OnClick="btnWord_Click"--%>
                </div>
                <div class="label-right">
                    <asp:ImageButton ID="btnPdf" runat="server" class="button-export"
                        ImageUrl="~/Images/pdf1.PNG" OnClientClick="return excelexport('Pdf')"  OnClick="btnPdf_Click"/><%--OnClick="btnPdf_Click"--%>
                </div>
            </div>
        </div>
         </div>
</asp:Content>
