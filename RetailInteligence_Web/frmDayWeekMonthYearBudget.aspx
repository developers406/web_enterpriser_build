﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="frmDayWeekMonthYearBudget.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmDayWeekMonthYearBudget" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .radioboxlist radioboxlistStyle
        {
            font-size: x-large;
            padding-right: 10px;
        }
        .radioboxlist label
        {
            color: Black;
            background-color: Silver;
            padding-left: 6px;
            padding-right: 5px;
            padding-top: 2px;
            padding-bottom: 2px;
            border: 1px solid #AAAAAA;
            white-space: nowrap;
            clear: left;
            margin-right: 5px;
            margin-left: 5px;
        }
        
        input:checked + label
        {
            color: white;
            background: red;
        }
    </style>
    <script type="text/javascript">
        var fromdatectrl;
        var todatectrl;
        function pageLoad() {
            $("select").SumoSelect({ selectAll: false, search: true, searchText: 'Search here.' });

            $(function () {


                fromdatectrl = $('#<%=txtFromDate.ClientID%>');
                todatectrl = $('#<%=txtToDate.ClientID%>');

                fromdatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'dark'
                    //                yearOffset: 222,
                    //                format: 'd/m/Y',
                    //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                    //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                });

                todatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'dark'
                    //                yearOffset: 222,
                    //                format: 'd/m/Y',
                    //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                    //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                });


                if (fromdatectrl.val() === '') {
                    fromdatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'light'
                        //                yearOffset: 222,
                        //                format: 'd/m/Y',
                        //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                        //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                    });
                }

                if (todatectrl.val() === '') {
                    todatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'light'
                        //                yearOffset: 222,
                        //                format: 'd/m/Y',
                        //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                        //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                    });
                }
            });
        } 

             
        function ValidateForm() {

            fromdatectrl = $('#<%=txtFromDate.ClientID%>');
            todatectrl = $('#<%=txtToDate.ClientID%>');

            var Show = '';
            if (fromdatectrl.val() == "") {
                Show = Show + '\n  Choose From date';
                fromdatectrl.focus();
            }

            if (todatectrl.val() == "") {
                Show = Show + '\n  Choose To date';
                todatectrl.focus();
            }

            if (Show != '') {
                alert(Show); 
                return false;
            }
             
          }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-group-price">
        <div class="container-pers-top-header-reports">
            <div class="container-pers-header-left">
                <div class="control-group-split">
                    <div class="control-group-left" style="width: 50%">
                        <div class="label-left">
                            <asp:Label ID="Label29" runat="server" Text="From Date" CssClass="label-invoice"></asp:Label><span
                                class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-right" style="width: 48%">
                        <div class="label-left">
                            <asp:Label ID="Label31" runat="server" CssClass="label-invoice" Text="To Date"></asp:Label>
                        </div>
                        <div class="label-right" style="color: Black">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-pers-header-right">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-right" style="width: 20%; margin-left: 10px;">
                            <asp:Label ID="lblFiltername" runat="server" CssClass="label-invoice" Text="Profitability Through"></asp:Label>
                        </div>
                        <div class="label-left" style="width: 70%">
                            <dx:ASPxComboBox ID="ddlFilter" CssClass="form-control" runat="server" Theme="SoftOrange"
                                IncrementalFilteringMode="StartsWith">
                            </dx:ASPxComboBox>
                        </div>
                    </div>
                    <div class="control-group-right">
                        <%-- <div class="label-right" style="width: 50%;">
                            <asp:LinkButton ID="lnkLoadReport" runat="server" class="button-blue" Text="Load Report"> </asp:LinkButton>
                        </div>--%>
                        <div class="label-left">
                            <asp:LinkButton ID="lnkLoadReport" runat="server" class="button-blue" Text="Load Report"  OnClientClick = "return ValidateForm()"
                                OnClick="lnkLoadReport_Click"> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <%--   <div class="container-pers-top-header-blue">
            <div class="container-pers-header-left" style="width: 60%; margin-left: 10px">
                <asp:RadioButtonList ID="rdoGroup" runat="server" RepeatDirection="Horizontal" Class="radioboxlist">
                    <asp:ListItem Selected= "True" Value="Year">Year</asp:ListItem>
                    <asp:ListItem Value="Month">Month</asp:ListItem>
                    <asp:ListItem Value="Week">Week</asp:ListItem>
                    <asp:ListItem Value="Day">Day</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>--%>
        <div class="grid-overflow-Invchange" id="HideFilter_GridOverFlow" runat="server">
            <dx:ASPxPivotGrid ID="pivotGridControlGroup" runat="server" ClientIDMode="AutoID"
                EnableTheming="True" Theme="PlasticBlue">
            </dx:ASPxPivotGrid>
        </div>
    </div>
</asp:Content>
