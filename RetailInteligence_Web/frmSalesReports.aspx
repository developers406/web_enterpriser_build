﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="frmSalesReports.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmSalesReports" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var fromdatectrl;
        var todatectrl;
        function pageLoad() {
            $("select").SumoSelect({ selectAll: false, search: true, searchText: 'Search here.' });


            $("[id$=txtItemsearch]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/RetailInteligence_Web/frmSalesReports.aspx/GetInventoryDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valCode: item.split('|')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#HiddenItemCode").val($.trim(i.item.valCode));
                    alert($("#HiddenItemCode").val());
                    return false;
                },
                minLength: 3
            });


            $(function () {


                fromdatectrl = $('#<%=txtFromDate.ClientID%>');
                todatectrl = $('#<%=txtToDate.ClientID%>');

                fromdatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'dark'
                    //                yearOffset: 222,
                    //                format: 'd/m/Y',
                    //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                    //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                });

                todatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'dark'
                    //                yearOffset: 222,
                    //                format: 'd/m/Y',
                    //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                    //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                });


                if (fromdatectrl.val() === '') {
                    fromdatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'light'
                        //                yearOffset: 222,
                        //                format: 'd/m/Y',
                        //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                        //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                    });
                }

                if (todatectrl.val() === '') {
                    todatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'light'
                        //                yearOffset: 222,
                        //                format: 'd/m/Y',
                        //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                        //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                    });
                }
            });
        }

        function ShowPopup() {
            $("#divExport").dialog({
                title: 'Export Option',
                width: 340,
                height: 200,
                //buttons: {
                //    OK: function () {
                //        $(this).dialog('close');
                //    }
                //},
                modal: true
            });

            return false;
        }

        function excelexport(name) {
            //alert(name  );
            if (name === "Excel")
                __doPostBack('ctl00$ContentPlaceHolder1$btnExcel', '');
            if (name === "Word")
                __doPostBack('ctl00$ContentPlaceHolder1$btnWord', '');
            if (name === "Pdf")
                __doPostBack('ctl00$ContentPlaceHolder1$btnPdf', '');

            return false;
        }

        function ValidateForm() {

            fromdatectrl = $('#<%=txtFromDate.ClientID%>');
            todatectrl = $('#<%=txtToDate.ClientID%>');

            //alert($("[id*=txtItemsearch]").val());
            //alert($("#HiddenItemCode").val());

            var Show = '';
            if (fromdatectrl.val() == "") {
                Show = Show + '\n  Choose From date';
                fromdatectrl.focus();
            }

            if (todatectrl.val() == "") {
                Show = Show + '\n  Choose To date';
                todatectrl.focus();
            }

            if ($("#HiddenItemCode").val() == "0") {
                if ($('#ContentPlaceHolder1_ddlBrand_I').val() == "") {
                    Show = Show + '\n  Choose Any Filtration';
                    $('#ContentPlaceHolder1_ddlBrand_I').focus();
                }
            }
            else {
                if ($("[id*=txtItemsearch]").val() == "") {
                    Show = Show + '\n  Select any Inventory..!';
                    $("[id*=txtItemsearch]").focus();
                }
            }

            if (Show != '') {
                alert(Show);
                return false;
            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-group-price">
        <div class="container-pers-top-header-reports" style="margin-top: 60px;">
            <div class="container-pers-header-left">
                <div class="control-group-split">
                    <div class="control-group-left" style="width: 50%">
                        <div class="label-left">
                            <asp:Label ID="Label29" runat="server" Text="From Date" CssClass="label-invoice"></asp:Label><span
                                class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-right" style="width: 48%">
                        <div class="label-left">
                            <asp:Label ID="Label31" runat="server" CssClass="label-invoice" Text="To Date"></asp:Label>
                        </div>
                        <div class="label-right" style="color: Black">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-pers-header-right">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-right" style="width: 20%; margin-left: 10px;">
                            <asp:Label ID="lblFiltername" runat="server" CssClass="label-invoice" Text="Brand Name"></asp:Label>
                        </div>
                        <div class="label-left" style="width: 70%">
                            <dx:ASPxComboBox ID="ddlBrand" CssClass="form-control" runat="server"
                                Theme="SoftOrange" IncrementalFilteringMode="StartsWith">
                            </dx:ASPxComboBox>
                            <asp:TextBox ID="txtItemsearch" runat="server" Visible="false" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-right">
                        <div class="label-right" style="width: 50%;">
                            <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-primary btn-sm btn-rounded waves-effect" Text="Load Report"
                                OnClick="lnkLoadReport_Click" OnClientClick="return ValidateForm()" Style="background-color: #f5081e; border-radius: 35px; border: 2px solid rgba(140, 5, 5, 0.75); height: 30px; width: 90px;"> </asp:LinkButton>

                        </div>
                        <div class="label-left" style="width: 50%">
                            <asp:LinkButton ID="lnkExport" runat="server" class="btn btn-primary btn-sm btn-rounded waves-effect" Text="Export Report" OnClientClick="return ShowPopup()" Style="background-color: #007bff; border-radius: 35px; border: 2px solid rgba(0, 70, 147, 0.75);"> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-overflow-Invchange_RI" id="HideFilter_GridOverFlow" runat="server">
            <dx:ASPxPivotGrid ID="pivotGridControlGroup" runat="server" ClientIDMode="AutoID" EnableTheming="True"
                Theme="PlasticBlue" CssClass="RI-Report-Grid-Height" OnCustomCellValue="pivotGridControlGroup_CustomCellValue"  >
                <Styles>
                    <DataAreaStyle BackColor="Yellow">
                    </DataAreaStyle>
                    <ColumnAreaStyle BackColor="Black">
                    </ColumnAreaStyle>
                    <RowAreaStyle BackColor="#10EF80">
                    </RowAreaStyle>
                </Styles>
                <StylesPrint Cell-BackColor2="" Cell-GradientMode="Horizontal" FieldHeader-BackColor2="" FieldHeader-GradientMode="Horizontal" TotalCell-BackColor2="" TotalCell-GradientMode="Horizontal" GrandTotalCell-BackColor2="" GrandTotalCell-GradientMode="Horizontal" CustomTotalCell-BackColor2="" CustomTotalCell-GradientMode="Horizontal" FieldValue-BackColor2="" FieldValue-GradientMode="Horizontal" FieldValueTotal-BackColor2="" FieldValueTotal-GradientMode="Horizontal" FieldValueGrandTotal-BackColor2="" FieldValueGrandTotal-GradientMode="Horizontal" Lines-BackColor2="" Lines-GradientMode="Horizontal"></StylesPrint>
                <OptionsPager NumericButtonCount="200" RowsPerPage="300">
                </OptionsPager>
            </dx:ASPxPivotGrid>
            <dx:ASPxPivotGridExporter ID="ASPxPivotGridExporter1" runat="server" ASPxPivotGridID="pivotGridControlGroup"
                Visible="False">
            </dx:ASPxPivotGridExporter>
        </div>
        <div class="whole-price" id="divExport" style="display: none">
            <div class="control-group-full">
                <div class="label-left">
                    <asp:ImageButton ID="btnExcel" runat="server" class="button-export"
                        ImageUrl="~/Images/excel1.PNG" OnClientClick="return excelexport('Excel')" OnClick="btnExcel_Click" />
                </div>
                <div class="label-left">
                    <asp:ImageButton ID="btnWord" runat="server" class="button-export"
                        ImageUrl="~/Images/word1.PNG" OnClientClick="return excelexport('Word')" OnClick="btnWord_Click" />
                </div>
                <div class="label-right">
                    <asp:ImageButton ID="btnPdf" runat="server" class="button-export"
                        ImageUrl="~/Images/pdf1.PNG" OnClientClick="return excelexport('Pdf')" OnClick="btnPdf_Click" />
                </div>
            </div>
            <asp:HiddenField ID="HiddenItemCode" runat="server" ClientIDMode="Static" Value="0" />
        </div>
    </div>
</asp:Content>
