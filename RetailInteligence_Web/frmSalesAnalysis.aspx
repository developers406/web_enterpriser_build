﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="frmSalesAnalysis.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmSalesAnalysis" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var fromdatectrl;
        var todatectrl;
        function pageLoad() {
            $("select").SumoSelect({ selectAll: false, search: true, searchText: 'Search here.' });

            $(function () {


                fromdatectrl = $('#<%=txtFromDate.ClientID%>');
                todatectrl = $('#<%=txtToDate.ClientID%>');

                fromdatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'dark'
                    //                yearOffset: 222,
                    //                format: 'd/m/Y',
                    //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                    //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                });

                todatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'dark'
                    //                yearOffset: 222,
                    //                format: 'd/m/Y',
                    //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                    //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                });


                if (fromdatectrl.val() === '') {
                    fromdatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'light'
                        //                yearOffset: 222,
                        //                format: 'd/m/Y',
                        //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                        //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                    });
                }

                if (todatectrl.val() === '') {
                    todatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'light'
                        //                yearOffset: 222,
                        //                format: 'd/m/Y',
                        //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                        //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                    });
                }
            });
        }

        function ShowPopup() {
            $("#divExport").dialog({
                title: 'Export Option',
                width: 340,
                height: 200,
                //buttons: {
                //    OK: function () {
                //        $(this).dialog('close');
                //    }
                //},
                modal: true
            });

            return false;
        }

        function excelexport(name) {
            //alert(name  );
            if (name === "Excel")
                __doPostBack('ctl00$ContentPlaceHolder1$btnExcel', '');
            if (name === "Word")
                __doPostBack('ctl00$ContentPlaceHolder1$btnWord', '');
            if (name === "Pdf")
                __doPostBack('ctl00$ContentPlaceHolder1$btnPdf', '');

            return false;
        }

        function ValidateForm() {

            fromdatectrl = $('#<%=txtFromDate.ClientID%>');
              todatectrl = $('#<%=txtToDate.ClientID%>');
            //alert($('#ContentPlaceHolder1_ddlBrand_I').val());
              var Show = '';
              if (fromdatectrl.val() == "") {
                  Show = Show + '\n  Choose From date';
                  fromdatectrl.focus();
              }

              if (todatectrl.val() == "") {
                  Show = Show + '\n  Choose To date';
                  todatectrl.focus();
              }

              if ($('#ContentPlaceHolder1_ddlBrand_I').val() == "") {
                  Show = Show + '\n  Choose Any Vendor';
                  $('#ContentPlaceHolder1_ddlBrand_I').focus();
              }

              if ($('#ContentPlaceHolder1_ddlLocation_I').val() == "") {
                  Show = Show + '\n  Choose Any Location';
                  $('#ContentPlaceHolder1_ddlBrand_I').focus();
              }

              if (Show != '') {
                  alert(Show);
                  return false;
              }

          }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-group-price">
        <div class="container-pers-top-header-reports" style="margin-top: 60px;">
            <div class="container-pers-header-left">
                <div class="control-group-split">
                    <div class="control-group-left" style="width: 50%">
                        <div class="label-left">
                            <asp:Label ID="Label29" runat="server" Text="From Date" CssClass="label-invoice"></asp:Label><span
                                class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-right" style="width: 48%">
                        <div class="label-left">
                            <asp:Label ID="Label31" runat="server" CssClass="label-invoice" Text="To Date"></asp:Label>
                        </div>
                        <div class="label-right" style="color: Black">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-pers-header-right">
                <div class="control-group-split">
                    <div class="control-group-right">
                        <div class="label-left">
                            <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-primary btn-sm btn-rounded waves-effect" Text="Load Report" 
                                OnClick="lnkLoadReport_Click" OnClientClick="return ValidateForm()" style="background-color: #f5081e;border-radius: 35px;border: 2px solid rgba(140, 5, 5, 0.75);height: 30px;width: 90px;"> </asp:LinkButton> <%--class="button-red" --%>
                        </div>
                        <div class="label-right">
                            <asp:LinkButton ID="lnkExport" runat="server" class="btn btn-primary btn-sm btn-rounded waves-effect"
                                 Text="Export Report" OnClientClick="return ShowPopup()" style="background-color: #007bff;border-radius: 35px;border: 2px solid rgba(0, 70, 147, 0.75);width: 95px;"> </asp:LinkButton><%--class="button-blue" --%>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-pers-top-header-blue">
            <div class="container-pers-header-left" style="width: 60%">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-left">
                            <asp:Label ID="lblFiltername" runat="server" CssClass="label-invoice" Text="Brand Name"></asp:Label><span
                                class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <dx:ASPxComboBox ID="ddlBrand" CssClass="form-control" runat="server" Theme="SoftOrange"
                                IncrementalFilteringMode="StartsWith">
                            </dx:ASPxComboBox>
                        </div>
                    </div>
                    <div class="control-group-right">
                        <div class="label-left">
                            <asp:Label ID="Label1" runat="server" Text="Location" CssClass="label-invoice"></asp:Label><span
                                class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <dx:ASPxComboBox ID="ddlLocation" CssClass="form-control" runat="server" Theme="SoftOrange"
                                IncrementalFilteringMode="StartsWith">
                            </dx:ASPxComboBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-overflow-Invchange" id="HideFilter_GridOverFlow" runat="server">
            <dx:ASPxPivotGrid ID="pivotGridControlGroup" runat="server" ClientIDMode="AutoID"
                EnableTheming="True" Theme="PlasticBlue">
            </dx:ASPxPivotGrid>
            <dx:ASPxPivotGridExporter ID="ASPxPivotGridExporter1" runat="server" ASPxPivotGridID="pivotGridControlGroup"
                Visible="False">
            </dx:ASPxPivotGridExporter>
        </div>
        <div class="whole-price" id="divExport" style="display: none">
            <div class="control-group-full">
                <div class="label-left">
                    <asp:ImageButton ID="btnExcel" runat="server" class="button-export"
                        ImageUrl="~/Images/excel1.PNG" OnClientClick="return excelexport('Excel')" OnClick="btnExcel_Click" />
                </div>
                <div class="label-left">
                    <asp:ImageButton ID="btnWord" runat="server" class="button-export"
                        ImageUrl="~/Images/word1.PNG" OnClientClick="return excelexport('Word')" OnClick="btnWord_Click" />
                </div>
                <div class="label-right">
                    <asp:ImageButton ID="btnPdf" runat="server" class="button-export"
                        ImageUrl="~/Images/pdf1.PNG" OnClientClick="return excelexport('Pdf')" OnClick="btnPdf_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
