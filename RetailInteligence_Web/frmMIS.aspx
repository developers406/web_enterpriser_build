﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="frmMIS.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmMIS" %>


<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register TagPrefix="ups" TagName="SearchListFilterUserControl" Src="~/UserControls/SearchListFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var fromdatectrl;
        var todatectrl;
        function pageLoad() {
            $("select").SumoSelect({ selectAll: false, search: true, searchText: 'Search here.' });

            $(".whole-price-header").click(function () {
                $header = $(this);
                //getting the next element
                $content = $header.next();
                //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                $content.fadeToggle(400, function () {
                    //execute this after slideToggle is done
                    //change text of header based on visibility of content div
                    $header.text(function () {
                        //change text based on condition .slideDown();
                        return $content.is(":visible") ? "Filtration Hide" : "Filtration Show";
                    });
                });

            });


            $(".barcode-header-filter").click(function () {
                $header = $(this);
                //getting the next element
                $content = $header.next();
                //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                $content.slideToggle(100, function () {
                    //execute this after slideToggle is done
                    //change text of header based on visibility of content div
                    $header.text(function () {
                        //change text based on condition .slideDown();
                        return $content.is(":visible") ? "Filtration Hide" : "Filtration Show";
                    });
                    if ($content.is(":visible")) {
                        $("#<%=HideFilter_ContainerRight.ClientID%>").width("74%").height("100%");
                    }
                    else {
                        $("#<%=HideFilter_ContainerRight.ClientID%>").width("100%").height("100%");
                    }
                });

            });

            $(function () {


                fromdatectrl = $('#<%=txtFromDate.ClientID%>');
                todatectrl = $('#<%=txtToDate.ClientID%>');

                fromdatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'dark'
                    //                yearOffset: 222,
                    //                format: 'd/m/Y',
                    //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                    //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                });

                todatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'dark'
                    //                yearOffset: 222,
                    //                format: 'd/m/Y',
                    //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                    //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                });


                if (fromdatectrl.val() === '') {
                    fromdatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'dark'
                        //                yearOffset: 222,
                        //                format: 'd/m/Y',
                        //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                        //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                    });
                }

                if (todatectrl.val() === '') {
                    todatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'dark'
                        //                yearOffset: 222,
                        //                format: 'd/m/Y',
                        //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                        //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                    });
                }
            });
        }
        function clearForm() {


            //return;
            //            $("select").each(function (indexed, elemt) {
            //                $(elemt).val(0);
            //                alert($(elemt));
            //                alert($(elemt).sumo);
            //                if ($(elemt).sumo)
            //                    $(elemt).sumo.reload();
            //                //                $(elemt).find('option').each(function (index, optelemt) {
            //                //                    console.log($(optelemt));
            //                //                    //$(optelemt).parent().sumo.unSelectItem(index);
            //                //                });
            //                //console.log(index + ": " + $(this).text());
            //            }); 

            //console.log(err);
            //            $(':checkbox, :radio').prop('checked', false);
        }


        function ValidateForm() {

            fromdatectrl = $('#<%=txtFromDate.ClientID%>');
            todatectrl = $('#<%=txtToDate.ClientID%>');

            var Show = '';
            if (fromdatectrl.val() == "") {
                Show = Show + '\n  Choose From date';
                fromdatectrl.focus();
            }

            if (todatectrl.val() == "") {
                Show = Show + '\n  Choose To date';
                todatectrl.focus();
            }

            if (Show != '') {
                alert(Show);
                return false;
            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-pers-top-header-mis">
        <%--<div class="whole-price-header" style="margin-top: 55px">
            <%--style="margin-top: 55px"
            <span>Filtration Show</span>
        </div>--%>
        <div class="selling" style="padding-top: 15px">

            <div class="report-left" style="margin-top: 35px;">
                <div class="whole-price">
                    <div class="whole-price-header">
                        Period
                    </div>
                    <div class="whole-price-detail">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="From Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="To Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="By Period"></asp:Label>
                            </div>
                            <div class="label-right">
                                <dx:ASPxComboBox ID="ddlMonth" CssClass="form-control" runat="server" Theme="SoftOrange"
                                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True">
                                    <Items>
                                        <dx:ListEditItem Text="Yearly" Value="Yearly" />
                                        <dx:ListEditItem Text="Monthly" Value="Monthly" />
                                        <dx:ListEditItem Text="Weekly" Value="Weekly" />
                                        <%--<dx:ListEditItem Text="Daily" Value="Daily" />
                                   <dx:ListEditItem Text="Last" Value="Last" />--%>
                                    </Items>
                                </dx:ASPxComboBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="report-right" style="margin-top: 35px;">
                <div class="barcode">
                    <div class="barcode-header">
                        Column Name
                    </div>
                    <div class="barcode-detail">
                        <div class="whole-price-detail">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label37" runat="server" Text="Sales"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lvSales" runat="server" SelectionMode="Multiple" CssClass="form-control-res">
                                        <asp:ListItem>SalesValue</asp:ListItem>
                                        <asp:ListItem>SoldQty</asp:ListItem>
                                        <asp:ListItem>NetCost</asp:ListItem>
                                        <asp:ListItem>Discount Amount</asp:ListItem>
                                        <asp:ListItem>Discount Percent</asp:ListItem>
                                        <asp:ListItem>Profit Amount</asp:ListItem>
                                        <asp:ListItem>Profit Percent</asp:ListItem>
                                        <asp:ListItem>Tax Amount</asp:ListItem>
                                        <asp:ListItem>SalesReturnValue</asp:ListItem>
                                        <asp:ListItem>SalesReturnQty </asp:ListItem>
                                    </asp:ListBox>
                                </div>
                            </div>

                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label38" runat="server" Text="Stock & Purchase"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lvPurchaseStock" runat="server" SelectionMode="Multiple" CssClass="form-control-res">
                                        <asp:ListItem>PurchaseValue</asp:ListItem>
                                        <asp:ListItem>PurchaseQty</asp:ListItem>
                                        <asp:ListItem>GidNo</asp:ListItem>
                                        <asp:ListItem>DONo</asp:ListItem>
                                        <asp:ListItem>DODate</asp:ListItem>
                                        <asp:ListItem>FocQty</asp:ListItem>
                                        <asp:ListItem>TotalQty</asp:ListItem>
                                        <asp:ListItem>UnitCost</asp:ListItem>
                                        <asp:ListItem>GrossCost</asp:ListItem>
                                        <asp:ListItem>NetCost</asp:ListItem>
                                        <asp:ListItem>Gst</asp:ListItem>
                                        <asp:ListItem>Freight</asp:ListItem>
                                        <asp:ListItem>MRP </asp:ListItem>
                                        <asp:ListItem>SellingPrice</asp:ListItem>
                                        <asp:ListItem>POValue</asp:ListItem>
                                        <asp:ListItem>POQty</asp:ListItem>
                                        <asp:ListItem>PRValue</asp:ListItem>
                                        <asp:ListItem>PRQty</asp:ListItem>
                                        <asp:ListItem>PRRValue</asp:ListItem>
                                        <asp:ListItem>PRRQty</asp:ListItem>
                                        <asp:ListItem>CurrentStock</asp:ListItem>
                                        <asp:ListItem>StockValue</asp:ListItem>
                                    </asp:ListBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="label-left">
                                    <asp:LinkButton ID="lnkLoad" runat="server" class="btn btn-primary btn-sm btn-rounded waves-effect" Text="Load Report" OnClientClick="return ValidateForm()"
                                        OnClick="lnkLoad_Click" Style="background-color: #f5081e; border-radius: 35px; border: 2px solid rgba(140, 5, 5, 0.75); height: 30px; width: 90px;"> </asp:LinkButton>
                                </div>
                                <div class="label-left">
                                    <asp:Label ID="lblReportName" runat="server" Text="Report Name"> </asp:Label>
                                    <asp:TextBox ID="txtReportName" runat="server" Text=""> </asp:TextBox>
                                    <div class="label-right" style="width: 60%;">
                                        <asp:LinkButton ID="lnkFormat" runat="server" class="btn btn-primary btn-sm btn-rounded waves-effect" Text="Save Format "
                                            OnClick="lnkFormat_Click" Style="background-color: #f5081e; border-radius: 35px; border: 2px solid rgba(140, 5, 5, 0.75); height: 30px; width: 90px;"> </asp:LinkButton>
                                    </div>
                                </div>
                                <%-- <div class="label-right">
                                    <asp:LinkButton ID="lnkDeleteReport" runat="server" class="button-red" Text="Delete"
                                        Style="margin-left: 5px"> </asp:LinkButton>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="report-middle" style="margin-top: 35px;">
                <div class="whole-price">
                    <div class="whole-price-header">
                        Export
                    </div>
                    <div class="whole-price-detail">
                        <div class="control-group">
                            <div class="label-left">
                                <asp:ImageButton ID="btnExcel" runat="server" class="button-export"
                                    ImageUrl="~/Images/excel1.PNG" OnClick="btnExcel_Click" />
                            </div>
                            <div class="label-right">
                                <asp:ImageButton ID="btnWord" runat="server" class="button-export"
                                    ImageUrl="~/Images/word1.PNG" OnClick="btnWord_Click" />
                            </div>
                            <div class="label-right">
                                <asp:ImageButton ID="btnPdf" runat="server" class="button-export"
                                    ImageUrl="~/Images/pdf1.PNG" OnClick="btnPdf_Click" />
                            </div>
                        </div>
                        <%-- <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label39" runat="server" Text="Save As"></asp:Label>
                            </div>
                            <div class="label-right">
                                 <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>--%>
                    </div>
                </div>
            </div>
            <div class="report-middle" style ="display:none;">
                <div class="whole-price">
                    <div class="whole-price-header">
                        Operations
                    </div>
                    <div class="whole-price-detail" id="divSales" style ="height:50px;">
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlOpereation" runat="server" SelectionMode="Multiple" CssClass="form-control-res">
                                <asp:ListItem>SalesValue</asp:ListItem>
                                        <asp:ListItem>SoldQty</asp:ListItem>
                                        <asp:ListItem>NetCost</asp:ListItem>
                                        <asp:ListItem>Discount Amount</asp:ListItem>
                                        <asp:ListItem>Discount Percent</asp:ListItem>
                                        <asp:ListItem>Profit Amount</asp:ListItem>
                                        <asp:ListItem>Profit Percent</asp:ListItem>
                                        <asp:ListItem>Tax Amount</asp:ListItem>
                                        <asp:ListItem>SalesReturnValue</asp:ListItem>
                                        <asp:ListItem>SalesReturnQty </asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlCalculation" runat="server" SelectionMode="Multiple" CssClass="form-control-res">
                                <asp:ListItem>+</asp:ListItem>
                                <asp:ListItem>-</asp:ListItem>
                                <asp:ListItem>*</asp:ListItem>
                                <asp:ListItem>/</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlOperationPurchaseList" runat="server" SelectionMode="Multiple" CssClass="form-control-res">

                                <asp:ListItem>PurchaseValue</asp:ListItem>
                                <asp:ListItem>PurchaseQty</asp:ListItem>
                                <asp:ListItem>GidNo</asp:ListItem>
                                <asp:ListItem>DONo</asp:ListItem>
                                <asp:ListItem>DODate</asp:ListItem>
                                <asp:ListItem>FocQty</asp:ListItem>
                                <asp:ListItem>TotalQty</asp:ListItem>
                                <asp:ListItem>UnitCost</asp:ListItem>
                                <asp:ListItem>GrossCost</asp:ListItem>
                                <asp:ListItem>NetCost</asp:ListItem>
                                <asp:ListItem>Gst</asp:ListItem>
                                <asp:ListItem>Freight</asp:ListItem>
                                <asp:ListItem>MRP</asp:ListItem>
                                <asp:ListItem>SellingPrice</asp:ListItem> 
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="container-group-price">
        <div class="barcode-header-filter">
            <span>Filtration Show</span>
        </div>
        <div class="container-left-price" id="FilterDialog" runat="server">
            <ups:SearchListFilterUserControl runat="server" ID="searchListFilterUserControl"
                EnablelocationDropDown="true" EnableVendorDropDown="true" EnableDepartmentDropDown="true"
                EnableCategoryDropDown="true" EnableSubCategoryDropDown="true" EnableBrandDropDown="true"
                EnableClassDropDown="true" EnableSubClassDropDown="true" EnableFloorDropDown="true"
                EnableSectionDropDown="true" EnableBinDropDown="true" EnableShelfDropDown="true"
                EnableWarehouseDropDown="true" EnableItemTypeDropDown="true" EnableClearButton="true"
                OnClearButtonClientClick="clearForm(); return false;" />
        </div>
        <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
            <div class="grid-overflow-Invchange" id="HideFilter_GridOverFlow" runat="server">
                <dx:ASPxPivotGrid ID="pivotGridControlOS" runat="server" ClientIDMode="AutoID" EnableTheming="True"
                    Theme="PlasticBlue">
                    <OptionsPager NumericButtonCount="200" RowsPerPage="300">
                    </OptionsPager>
                </dx:ASPxPivotGrid>
                <dx:ASPxPivotGridExporter ID="ASPxPivotGridExporter1" runat="server" ASPxPivotGridID="pivotGridControlOS"
                    Visible="False">
                </dx:ASPxPivotGridExporter>
            </div>
            <div class="container-bottom-invchange">
                <div class="control-button">
                </div>
            </div>
            <div id="FilterDialog1" style="display: none">
                <div>
                    <%-- <ups:SearchListFilterUserControl runat="server" ID="searchListFilterUserControl1"
                        EnableVendorDropDown="true" EnableDepartmentDropDown="true" EnableCategoryDropDown="true"
                        EnableSubCategoryDropDown="true" EnableBrandDropDown="true" EnableClassDropDown="true"
                        EnableSubClassDropDown="true" EnableFloorDropDown="true" EnableSectionDropDown="true"
                        EnableBinDropDown="true" EnableShelfDropDown="true" EnableWarehouseDropDown="true"
                        EnableItemTypeDropDown="true" EnableFilterButton="true" EnableClearButton="true"
                        OnClearButtonClientClick="clearForm(); return false;" OnFilterButtonClick="lnkLoadFilter_Click" />--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
