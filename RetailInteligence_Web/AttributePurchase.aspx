﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="AttributePurchase.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.AttributePurchase" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register TagPrefix="ups" TagName="SearchListFilterUserControl" Src="~/UserControls/SearchListFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <script type="text/javascript">
        var fromdatectrl;  
        var todatectrl;
        function pageLoad() {
            //Jegan 08062019
            $('#ImageVisibility').hide();

            $("select").SumoSelect({ selectAll: false, search: true, searchText: 'Search here.' });

            $(function () {

                <%--//                fromdatectrl = $("#<%= searchListFilterUserControl.GetFilterControl<TextBox>(RetailIntelligenceWEB.UserControls.SearchListFilterUserControl.FilterControls.FromDateTextBox).ClientID %>");
                //                todatectrl = $("#<%= searchListFilterUserControl.GetFilterControl<TextBox>(RetailIntelligenceWEB.UserControls.SearchListFilterUserControl.FilterControls.ToDateTextBox).ClientID %>");--%>

                fromdatectrl = $('#<%=txtFromDate.ClientID%>');
                todatectrl = $('#<%=txtToDate.ClientID%>');

                fromdatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'light'
                    //                yearOffset: 222,
                    //                format: 'd/m/Y',
                    //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                    //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                });

                todatectrl.datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'Y-m-d',
                    formatDate: 'Y-m-d',
                    //value: new Date().toDateString(),
                    maxDate: '0',
                    theme: 'light'
                    //                yearOffset: 222,
                    //                format: 'd/m/Y',
                    //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                    //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                });


                if (fromdatectrl.val() === '') {
                    fromdatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'light'
                        //                yearOffset: 222,
                        //                format: 'd/m/Y',
                        //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                        //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                    });
                }

                if (todatectrl.val() === '') {
                    todatectrl.datetimepicker({
                        lang: 'ch',
                        timepicker: false,
                        format: 'Y-m-d',
                        formatDate: 'Y-m-d',
                        value: new Date().toDateString(),
                        maxDate: '0',
                        theme: 'light'
                        //                yearOffset: 222,
                        //                format: 'd/m/Y',
                        //                minDate: '-1970/01/02', // yesterday is minimum date // {minDate:0} // today
                        //                maxDate: '+1970/01/02' // and tommorow is maximum date calendar
                    });
                }
            });
        }
        function clearForm() {

            $("select").val(0);
            $("select").sumo.reload();

            //return;
            //            $("select").each(function (indexed, elemt) {
            //                $(elemt).val(0);
            //                alert($(elemt));
            //                alert($(elemt).sumo);
            //                if ($(elemt).sumo)
            //                    $(elemt).sumo.reload();
            //                //                $(elemt).find('option').each(function (index, optelemt) {
            //                //                    console.log($(optelemt));
            //                //                    //$(optelemt).parent().sumo.unSelectItem(index);
            //                //                });
            //                //console.log(index + ": " + $(this).text());
            //            });

            //$("select")[0].sumo.reload();
            //            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //            $("select").val(0);
            //            $("select").trigger("liszt:updated");
            //console.log(err);
            //            $(':checkbox, :radio').prop('checked', false);
        }

        function ShowFilterPopup() {
            //            $("#FilterDialog").dialog({
            //                title: 'Filteration',
            //                width: 500,
            //                //                buttons: {
            //                //                    OK: function () {
            //                //                        $(this).dialog('close');
            //                //                    }
            //                //                },
            //                modal: true
            //            });
            try {

                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                    $("[id*=FilterDialog]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").width("100%").height("100%");
                    $("select").trigger("liszt:updated");
                }
                else {
                    $("[id*=FilterDialog]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'padding-top:55px');
                    $("select").trigger("liszt:updated");
                }

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }

            return false;
        }
        function ShowPopup() {
            $("#divExport").dialog({
                title: 'Export Option',
                width: 340,
                height: 200,
                //buttons: {
                //    OK: function () {
                //        $(this).dialog('close');
                //    }
                //},
                modal: true
            });

            return false;
        }

        function excelexport(name) {
            //alert(name  );
            if (name === "Excel")
                __doPostBack('ctl00$ContentPlaceHolder1$btnExcel', '');
            if (name === "Word")
                __doPostBack('ctl00$ContentPlaceHolder1$btnWord', '');
            if (name === "Pdf")
                __doPostBack('ctl00$ContentPlaceHolder1$btnPdf', '');

            return false;
        }

        function ValidateForm() {

            fromdatectrl = $('#<%=txtFromDate.ClientID%>');
            todatectrl = $('#<%=txtToDate.ClientID%>');

            var Show = '';
            if (fromdatectrl.val() == "") {
                Show = Show + '\n  Choose From date';
                fromdatectrl.focus();
            }

            if (todatectrl.val() == "") {
                Show = Show + '\n  Choose To date';
                todatectrl.focus();
            }

            if (Show != '') {
                alert(Show);
                return false;
            }

        }


    </script>
</asp:Content>
<%--EnableFromDateTextBox="true" EnableToDateTextBox="true"  OnFilterButtonClick="lnkLoadFilter_Click"  EnableFilterButton="true" --%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-group-price">
        <div class="container-left-price" id="FilterDialog" runat="server" style="padding-top:60px" >
            <ups:SearchListFilterUserControl runat="server" ID="searchListFilterUserControl"
                EnablelocationDropDown="true" EnableVendorDropDown="true" EnableDepartmentDropDown="true"
                EnableCategoryDropDown="true" EnableSubCategoryDropDown="true" EnableBrandDropDown="true"
                EnableClassDropDown="true" EnableSubClassDropDown="true" EnableFloorDropDown="true"
                EnableSectionDropDown="true" EnableBinDropDown="true" EnableShelfDropDown="true"
                EnableWarehouseDropDown="true" EnableItemTypeDropDown="true" EnableClearButton="true"
                OnClearButtonClientClick="clearForm(); return false;" />



        </div>
        <div class="container-right-price" id="HideFilter_ContainerRight" runat="server" style="padding-top:55px">
            <div class="container-pers-top-header">
                <div class="container-pers-header-left">
                    <div class="control-group-split">
                        <div class="control-group-left" style="width: 55%">
                            <div class="label-left">
                                <asp:Label ID="Label29" runat="server" Text="From Date" CssClass="label-invoice"></asp:Label><span
                                    class="mandatory">*</span>
                            </div>
                            <div class="label-right" style="padding-left: 27px;">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right" style="width: 40%">
                            <div class="label-left">
                                <asp:Label ID="Label31" runat="server" CssClass="label-invoice" Text="To Date"></asp:Label>
                            </div>
                            <div class="label-right" style="color: Black">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>


                        </div>

                        <div class="control-group-single-res" runat="server" id="DivStyleCode">
                            <div class="label-left">
                                <asp:Label ID="Label26" runat="server" CssClass="label-invoice" Text="Style Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtStyleCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-pers-header-right">
                    <div class="control-group-split">
                        <div class="control-group-right" style="padding-left: 46px; height: 50px; width: 547px;">
                            <div class="label-right" style="width: 25%;">
                                <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-primary btn-sm btn-rounded waves-effect" Text="Load" OnClientClick="return ValidateForm()"
                                    OnClick="lnkLoadReport_Click" style="background-color: #f5081e;border-radius: 35px;border: 2px solid rgba(140, 5, 5, 0.75);height: 30px;width: 80px;"> </asp:LinkButton> <%--class="button-red" --%>
                            </div>
                            <div class="label-left" style="width: 25%">
                                <asp:LinkButton ID="lnkFilterOption" runat="server" class="btn btn-primary btn-sm btn-rounded waves-effect"
                                    Text="Hide Filter" OnClientClick="return ShowFilterPopup()" style="background-color: #007bff;border-radius: 35px;border: 2px solid rgba(0, 70, 147, 0.75);width: 85px;"> </asp:LinkButton>
                            </div>
                            <div class="label-right" style="width: 25%;">
                                <asp:LinkButton ID="LinkButton1" runat="server" class="btn btn-primary btn-sm btn-rounded waves-effect" Text="Export"
                                    OnClientClick="return ShowPopup()" style="background-color: #007bff;border-radius: 35px;border: 2px solid rgba(0, 70, 147, 0.75);width: 85px;"> </asp:LinkButton>
                            </div>
                            <div class="label-right" style="width: 25%;">
                                <asp:LinkButton ID="lnkFormat" runat="server" class="btn btn-primary btn-sm btn-rounded waves-effect" Text="Save Format "
                                    OnClick="lnkFormat_Click" style="background-color: #f5081e;border-radius: 35px;border: 2px solid rgba(140, 5, 5, 0.75);height: 30px;width: 105px;"> </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-overflow-Invchange" id="HideFilter_GridOverFlow" runat="server" style="height:545px">
                <dx:ASPxPivotGrid ID="pivotGridControlOS" runat="server" ClientIDMode="AutoID" EnableTheming="True"
                    Theme="PlasticBlue" Height="0%" >
                    <OptionsPager NumericButtonCount="200" RowsPerPage="300">
                    </OptionsPager>
                    <StylesPrint Cell-BackColor2="" Cell-GradientMode="Horizontal" FieldHeader-BackColor2=""
                        FieldHeader-GradientMode="Horizontal" TotalCell-BackColor2="" TotalCell-GradientMode="Horizontal"
                        GrandTotalCell-BackColor2="" GrandTotalCell-GradientMode="Horizontal" CustomTotalCell-BackColor2=""
                        CustomTotalCell-GradientMode="Horizontal" FieldValue-BackColor2="" FieldValue-GradientMode="Horizontal"
                        FieldValueTotal-BackColor2="" FieldValueTotal-GradientMode="Horizontal" FieldValueGrandTotal-BackColor2=""
                        FieldValueGrandTotal-GradientMode="Horizontal" Lines-BackColor2="" Lines-GradientMode="Horizontal"></StylesPrint>
                </dx:ASPxPivotGrid>
                <dx:ASPxPivotGridExporter ID="ASPxPivotGridExporter1" runat="server" ASPxPivotGridID="pivotGridControlOS"
                    Visible="False">
                </dx:ASPxPivotGridExporter>
            </div>
            <div class="container-bottom-invchange">
                <div class="control-button">
                </div>
            </div>
            <div id="FilterDialog1" style="display: none">
                <div>
                    <%-- <ups:SearchListFilterUserControl runat="server" ID="searchListFilterUserControl1"
                        EnableVendorDropDown="true" EnableDepartmentDropDown="true" EnableCategoryDropDown="true"
                        EnableSubCategoryDropDown="true" EnableBrandDropDown="true" EnableClassDropDown="true"
                        EnableSubClassDropDown="true" EnableFloorDropDown="true" EnableSectionDropDown="true"
                        EnableBinDropDown="true" EnableShelfDropDown="true" EnableWarehouseDropDown="true"
                        EnableItemTypeDropDown="true" EnableFilterButton="true" EnableClearButton="true"
                        OnClearButtonClientClick="clearForm(); return false;" OnFilterButtonClick="lnkLoadFilter_Click" />--%>
                </div>
            </div>
        </div>

        <%--Changes by JEGAN - 08062019--%>
        <div id="ImageVisibility">
            <div class="whole-price" id="divExport">
                <div class="control-group-full">
                    <div class="label-left">
                        <asp:ImageButton ID="btnExcel" runat="server" class="button-export"
                            ImageUrl="~/Images/excel1.PNG" OnClientClick="return excelexport('Excel')" OnClick="btnExcel_Click" />
                    </div>
                    <div class="label-left">
                        <asp:ImageButton ID="btnWord" runat="server" class="button-export"
                            ImageUrl="~/Images/word1.PNG" OnClientClick="return excelexport('Word')" OnClick="btnWord_Click" />
                    </div>
                    <div class="label-right">
                        <asp:ImageButton ID="btnPdf" runat="server" class="button-export"
                            ImageUrl="~/Images/pdf1.PNG" OnClientClick="return excelexport('Pdf')" OnClick="btnPdf_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
