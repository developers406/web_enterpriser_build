﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="frmReportDesign.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmReportDesign" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-group-price">
        <div class="container-pers-top-header">
            <div class="container-pers-header-left-design">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-left">
                            <asp:Label ID="Label29" runat="server" Text="Group Name" CssClass="label-invoice"></asp:Label><span
                                class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <dx:ASPxComboBox ID="ddlGroupName" CssClass="form-control" runat="server" Theme="Aqua"
                                ValueType="System.String">
                            </dx:ASPxComboBox>
                        </div>
                    </div>
                    <div class="control-group-right">
                        <div class="label-left">
                            <asp:Label ID="Label31" runat="server" CssClass="label-invoice" Text="Report Name"></asp:Label>
                        </div>
                        <div class="label-right" style="color: Black">
                            <dx:ASPxComboBox ID="ddlReportName" CssClass="form-control" runat="server" Theme="Aqua"
                                ValueType="System.String">
                            </dx:ASPxComboBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-pers-header-right-design">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-right">
                            <asp:Label ID="Label1" runat="server" CssClass="label-invoice" Text="New Report"></asp:Label>
                        </div>
                        <div class="label-left">
                            <asp:TextBox ID="txtNewReportname" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-right">
                        <div class="label-left">
                            <asp:LinkButton ID="lnkTemplateSave" runat="server" class="button-red" Text="Save"
                                OnClick="lnkTemplateSave_Click"> </asp:LinkButton>
                        </div>
                        <div class="label-right">
                            <asp:LinkButton ID="lnkLoadReport" runat="server" class="button-blue" Text="Load Report"
                                OnClick="lnkLoadReport_Click"> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-overflow-Invchange" id="HideFilter_GridOverFlow" runat="server">
            <dx:ASPxPivotGrid ID="pivotGridControlDesign" runat="server" ClientIDMode="AutoID"
                EnableTheming="True" Theme="PlasticBlue" OnCustomCellStyle="pivotGridControlDesign_CustomCellStyle">
                <Styles>
                    <FilterAreaStyle >
                    </FilterAreaStyle>
                    <DataAreaStyle  >
                    </DataAreaStyle>
                    <ColumnAreaStyle  >
                    </ColumnAreaStyle>
                </Styles>
<StylesPrint Cell-BackColor2="" Cell-GradientMode="Horizontal" FieldHeader-BackColor2="" FieldHeader-GradientMode="Horizontal" TotalCell-BackColor2="" TotalCell-GradientMode="Horizontal" GrandTotalCell-BackColor2="" GrandTotalCell-GradientMode="Horizontal" CustomTotalCell-BackColor2="" CustomTotalCell-GradientMode="Horizontal" FieldValue-BackColor2="" FieldValue-GradientMode="Horizontal" FieldValueTotal-BackColor2="" FieldValueTotal-GradientMode="Horizontal" FieldValueGrandTotal-BackColor2="" FieldValueGrandTotal-GradientMode="Horizontal" Lines-BackColor2="" Lines-GradientMode="Horizontal"></StylesPrint>
            </dx:ASPxPivotGrid>
        </div>
    </div>
</asp:Content>
