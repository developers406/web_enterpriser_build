﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="frmDashboard.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <style>
        canvas {
            width: 100% !important;
            height: 100% !important;
        }

        .loading-image {
            position: center;  
            margin-top:50px;
        }

        .loader {
            display: none;
            width: 100%;
            height: 100%;   
            position: center; 
        }
    </style>

    <script type="text/javascript">
        function pageLoad() {

            $.ajax({
                type: "POST",
                //url: "/WebService.asmx/fncGetInventoryDetail",
                url: '<%=ResolveUrl("~/RetailInteligence_Web/frmDashboard.aspx/GetChartDatas")%>',
                //data: "{'inventorycode':'" + inventory + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    //var s = msg.d; //" [{'id':1,'name':'Test1'},{'id':2,'name':'Test2'}]";
                    var aLocation = new Array();
                    var aDailySales = new Array();
                    var aMonthlySales = new Array();
                    var aPurchase = new Array();
                    var aStock = new Array();

                    var myObject = eval('(' + msg.d + ')');
                    for (i in myObject) {
                        aLocation[i] = myObject[i]["locationName"];
                        aDailySales[i] = myObject[i]["DailySales"];
                        aMonthlySales[i] = myObject[i]["MonthlySales"];
                        aPurchase[i] = myObject[i]["Purchase"];
                        aStock[i] = myObject[i]["Stock"];
                        //alert(myObject[i]["locationName"]);
                    }

                    //alert(Location);
                    fncLoadChart(aLocation, aDailySales);
                    fncLoadChart1(aLocation, aMonthlySales);
                    fncLoadChart2(aLocation, aPurchase);
                    fncLoadChart3(aLocation, aStock);
                },
                error: function (data) {
                    alert(data.message);
                },
                beforeSend: function () {
                    // Code to display spinner
                    $('.loader').show()
                },
                complete: function () {
                    // Code to hide spinner.
                    $('.loader').hide()
                }
            });
        }


        function fncLoadChart(aLocation, aSales) {
            var ctx = document.getElementById("DailyChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: aLocation, //["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                    datasets: [{
                        label: '# Sales',
                        data: aSales, //[12, 19, 3, 5, 2, 3],
                        backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
                        ],

                        borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    title:
                               {
                                   display: true,
                                   text: "Daily Sales"
                               },
                }
            });
        }

        function fncLoadChart1(aLocation, aSales) {

            // And for a doughnut chart 
            var ctx = document.getElementById("MonthlyChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: aLocation, //["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                    datasets: [{
                        label: '# Sales',
                        data: aSales, //[12, 19, 3, 5, 2, 3],
                        backgroundColor: [
                         'rgba(255, 99, 132, 0.2)',
                         'rgba(54, 162, 235, 0.2)',
                         'rgba(255, 206, 86, 0.2)',
                         'rgba(75, 192, 192, 0.2)',
                         'rgba(153, 102, 255, 0.2)',
                         'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 2
                    }]
                },
                options:
                            {
                                title:
                                {
                                    display: true,
                                    text: "Monthly Sales"
                                },
                                responsive: true,
                                maintainAspectRatio: true
                            }
            });
        }

        function fncLoadChart2(aLocation, aSales) {
            // And for a doughnut chart 
            var ctx = document.getElementById("PurchaseChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: aLocation, //["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                    datasets: [{
                        label: '# Sales',
                        data: aSales, //[12, 19, 3, 5, 2, 3],
                        backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 159, 64, 0.2)'
                        ],
                        //                        borderColor: [
                        //                'rgba(255,99,132,1)',
                        //                'rgba(54, 162, 235, 1)',
                        //                'rgba(255, 206, 86, 1)',
                        //                'rgba(75, 192, 192, 1)',
                        //                'rgba(153, 102, 255, 1)',
                        //                'rgba(255, 159, 64, 1)'
                        //            ],
                        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3cba9f", "#e8c3b9", "#c45850", "#3cba9f", "#e8c3b9", "#c45850"],
                        borderWidth: 2
                    }]
                },
                options:
                            {
                                title:
                                {
                                    display: true,
                                    text: "Purchase"
                                },
                                responsive: true,
                                maintainAspectRatio: true
                            }
            });
        }

        function fncLoadChart3(aLocation, aSales) {
            var ctx = document.getElementById("StockChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: aLocation, //["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                    datasets: [{
                        label: '# Stock',
                        data: aSales, //[12, 19, 3, 5, 2, 3],
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderWidth: 2
                    }]
                },
                options: {
                    title:
                                  {
                                      display: true,
                                      text: "Stock"
                                  },
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="loader">
            <center>
             <img class="loading-image" src="../images/loading_spinner.gif" alt="loading..">
            </center>
        </div>
        <div class="main-image">
            <!-- /.row -->
            <div class="row">
                <div class="col-md-6" style="margin-top: 65px;">
                    <div class="box box-solid bg-blue">
                        <div class="box-body">
                            <div class="row">
                                <canvas id="DailyChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-6" style="margin-top: 65px;">
                    <div class="box box-solid bg-purple">
                        <div class="box-body">
                            <div class="row">
                                <canvas id="MonthlyChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-solid bg-blue">
                        <div class="box-body">
                            <div class="row">
                                <canvas id="PurchaseChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="box box-solid bg-purple">
                        <div class="box-body">
                            <div class="row">
                                <canvas id="StockChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
        </div>
    </div>
</asp:Content>
