﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="frmMenuList.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmMenuList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        /* Picture Thumbnails */
        .thumbnails ul
        {
            width: 800px;
            list-style: none;
        }
        .thumbnails li
        {
            text-align: center;
            display: inline;
            width: 20%;
            height: 95px;
            float: left;
            color: Yellow;
            margin-bottom: 2px;
            padding: 5px 5px 5px 5px;
        }
        .button_free
        {
        	text-align:justify;
            border: 0px solid #15aeec;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            font-family: arial, helvetica;
            padding: 7px 14px 7px 14px;
            text-decoration: none;
            display: inline-block;
            font-weight: bold;
            color: #FCFCF7;
            background-color: #49C0F0;
            background-image: linear-gradient(to bottom, #49C0F0, #2CAFE3);
        }
        
        .button_free:hover
        {
            border: 0px solid #FAF7F2;
            background-color: #1ab0ec;
            background-image: linear-gradient(to bottom, #1ab0ec, #1a92c2);
        }
    </style>
    <script type="text/javascript">
        function TableSelection(editButton) {

            //            row = $(editButton).parent().parent();
            //            //alert(row.find('itemtemplate input[id*="btnUpdate"]').val());
            //            $("#hidenTableno").val(row.find('itemtemplate input[id*="btnUpdate"]').val());
            //            $("[id*=cartdiv]").show();
            //            $("[id*=divtables]").hide();
            //            $("[id*=divWaiter]").hide();

            //          
            //            //document.location.href = "online_order_DB.aspx?";
            //            //alert($(editButton).attr('class'));
            //            var status = $(editButton).attr('class');
            //            if (status == 'button_occupied' || status == 'button_purple') {
            //                $("[id*=cartdiv]").hide();
            //                $("[id*=divtables]").hide();
            //                $("[id*=divWaiter]").show();
            //                $("[id*=btnBack]").show();
            //                $("[id*=divCheckOut]").show();
            //                $("[id*=btnCheckoutdetails]").click();
            //            }

            window.location.href = 'frmDashBoard.aspx';
            return;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divtables" runat="server" style="padding-top: 55px;">
        <ul class="thumbnails ul" id="Ul1" style="padding-top: 10px; padding-left: 10px;">
            <asp:ListView runat="server" ID="PLListView" ItemPlaceholderID="PLPlaceholder">
                <%--OnItemCommand="EmployeesListView_OnItemCommand" OnItemDataBound="PLListView_ItemDataBound">--%>
                <ItemTemplate>
                    <li class="thumbnails li">
                        <itemtemplate runat="server">                                   
                                
                                <asp:LinkButton ID="btnButton" Width="100%"  Height="100%" style="padding: 30px 15px;font-weight:bold" class="btn  btn-primary"  runat="server" Text='<%# Eval("ReportName") %>' href ='<%# Eval("URL") %>' ></asp:LinkButton>                                    
                                      <%--<input runat = "server" type="submit" style="width:100%; height:100%; font-weight:bolder;" id="btnUpdate" class= '<%# changestyle(Eval("Color").ToString()) %>' value='<%# Eval("TableNumber") %>'  onclick="TableSelection(this); return false;" />--%>
                                      <%--<input runat = "server" type="submit" style="width:100%; height:100%; font-weight:bolder;" id="Submit1" class= "button_free" value='<%# Eval("ReportName") %>'  onclick="TableSelection(this); return false;" />--%>
                                       
                         </itemtemplate>
                    </li>
                </ItemTemplate>
            </asp:ListView>
        </ul>
    </div>
</asp:Content>
