﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="frmReportCreation.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmReportCreation" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function checkAll(objRef) {
            try {
                var GridView = objRef.parentNode.parentNode.parentNode;
                var inputList = GridView.getElementsByTagName("input");
                for (var i = 0; i < inputList.length; i++) {
                    //Get the Cell To find out ColumnIndex
                    var row = inputList[i].parentNode.parentNode;
                    if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                        if (objRef.checked) {
                            //If the header checkbox is checked
                            //check all checkboxes
                            //and highlight all rows
                            row.style.backgroundColor = "aqua";
                            inputList[i].checked = true;
                        }
                        else {
                            //If the header checkbox is checked
                            //uncheck all checkboxes
                            //and change rowcolor back to original 
                            if (row.rowIndex % 2 == 0) {
                                //Alternating Row Color
                                row.style.backgroundColor = "#c4ddff";
                            }
                            else {
                                row.style.backgroundColor = "white";
                            }
                            inputList[i].checked = false;
                        }
                    }
                }

            }
            catch (err) {
                alert(err.Message);
            }
        }

        function Check_Click(objRef) {

            try {
                //Get the Row based on checkbox
                var row = objRef.parentNode.parentNode;
                if (objRef.checked) {
                    //If checked change color to Aqua
                    row.style.backgroundColor = "aqua";
                }
                else {
                    //If not checked change back to original color
                    if (row.rowIndex % 2 == 0) {
                        //Alternating Row Color
                        row.style.backgroundColor = "#c4ddff";
                    }
                    else {
                        row.style.backgroundColor = "white";
                    }
                }
                //Get the reference of GridView
                var GridView = row.parentNode;

                //Get all input elements in Gridview
                var inputList = GridView.getElementsByTagName("input");

                for (var i = 0; i < inputList.length; i++) {
                    //The First element is the Header Checkbox
                    var headerCheckBox = inputList[0];
                    //Based on all or none checkboxes
                    //are checked check/uncheck Header Checkbox
                    var checked = true;
                    if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                        if (!inputList[i].checked) {
                            checked = false;
                            break;
                        }
                    }
                }
                headerCheckBox.checked = checked;
            }
            catch (err) {
                alert(err.Message);
            }
        }

         function ValidateForm() {             

             var Show = '';

             if ($('#ContentPlaceHolder1_ddlGroupName_I').val() == "") {
                Show = Show + '\n  Choose Any GroupName';
                $('#ContentPlaceHolder1_ddlGroupName_I').focus();
            }

             if ($('#ContentPlaceHolder1_ddlGroupwith_I').val() == "") {
                Show = Show + '\n  Choose Any GroupValue';
                $('#ContentPlaceHolder1_ddlGroupwith_I').focus();
            }

             if ($('#ContentPlaceHolder1_ddlTransaction_I').val() == "") {
                Show = Show + '\n  Choose Any Transaction';
                $('#ContentPlaceHolder1_ddlTransaction_I').focus();
            }
                        
            if (Show != '') {
                alert(Show);
                return false;
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-group-price">
        <%--  <div class="container-pers-top-header">
            <div class="container-pers-header-left-design">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-left">
                            <asp:Label ID="Label29" runat="server" Text="Group Name" CssClass="label-invoice"></asp:Label><span
                                class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <dx:ASPxComboBox ID="ddlGroupName" CssClass="form-control" runat="server" Theme="Aqua"
                                ValueType="System.String">
                            </dx:ASPxComboBox>
                        </div>
                    </div>
                    <div class="control-group-right">
                        <div class="label-left">
                            <asp:Label ID="Label31" runat="server" CssClass="label-invoice" Text="Report Name"></asp:Label>
                        </div>
                        <div class="label-right" style="color: Black">
                            <dx:ASPxComboBox ID="ddlReportName" CssClass="form-control" runat="server" Theme="Aqua"
                                ValueType="System.String">
                            </dx:ASPxComboBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-pers-header-right-design">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-right">
                            <asp:Label ID="Label1" runat="server" CssClass="label-invoice" Text="New Report"></asp:Label>
                        </div>
                        <div class="label-left">
                            <asp:TextBox ID="txtNewReportname" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-right">
                        <div class="label-left">
                            <asp:LinkButton ID="lnkTemplateSave" runat="server" class="button-red" Text="Save"
                                OnClick="lnkTemplateSave_Click"> </asp:LinkButton>
                        </div>
                        <div class="label-right">
                            <asp:LinkButton ID="lnkLoadReport" runat="server" class="button-blue" Text="Load Report"
                                OnClick="lnkLoadReport_Click"> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
        <div class="container-pers-top-header-mis">
            <div class="selling">
                <div class="report-middle">
                    <div class="whole-price">
                        <div class="whole-price-header">
                            Group Creation
                        </div>
                        <div class="whole-price-detail">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label37" runat="server" Text="Group Name"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <dx:ASPxComboBox ID="ddlGroupName" CssClass="form-control-res" runat="server" Theme="DevEx"
                                        ValueType="System.String" IncrementalFilteringMode="Contains" DropDownStyle="DropDown">
                                    </dx:ASPxComboBox>
                                    
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label38" runat="server" Text="Group With"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <dx:ASPxComboBox ID="ddlGroupwith" CssClass="form-control-res" runat="server" Theme="DevEx"
                                        AutoPostBack="true" ValueType="System.String" OnSelectedIndexChanged="ddlGroupwith_SelectedIndexChanged"
                                        IncrementalFilteringMode="Contains">
                                    </dx:ASPxComboBox>
                                    <%--<asp:ListBox ID="ddlGroupwith1" runat="server" CssClass="form-control-res" />--%>
                                    <%--<asp:DropDownList ID="ddlGroupwith2" runat="server" CssClass="form-control-res">
        </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label39" runat="server" Text="Transaction"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <dx:ASPxComboBox ID="ddlTransaction" CssClass="form-control-res" runat="server" Theme="DevEx">
                                        <Items>
                                            <dx:ListEditItem Text="Sales" Value="Sales" />
                                            <dx:ListEditItem Text="Purchase" Value="Purchase" />
                                        </Items>
                                    </dx:ASPxComboBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="label-left">
                                    <asp:LinkButton ID="lnkTemplateSave" runat="server" class="button-blue" Text="Save" OnClientClick="return ValidateForm()"
                                        OnClick="lnkTemplateSave_Click"> </asp:LinkButton>
                                </div>
                                <div class="label-right">
                                    <asp:LinkButton ID="lnkDeleteReport" runat="server" class="button-red" Text="Delete"
                                        Style="margin-left: 5px" onclick="lnkDeleteReport_Click"> </asp:LinkButton>
                                </div>
                                <div class="label-right">
                                    <asp:LinkButton ID="lnkLoadReport" runat="server" class="button-blue" Text="Load" OnClientClick="return ValidateForm()"
                                        Style="margin-left: 5px" OnClick="lnkLoadReport_Click"> </asp:LinkButton>
                                </div>
                                <div class="label-right">
                                    <asp:LinkButton ID="LinkClear" runat="server" class="button-red" Text="Clear"
                                        Style="margin-left: 5px" onclick="LinkClear_Click"> </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="report-right-full">
                    <div class="barcode">
                        <div class="barcode-detail">
                            <div class="gridDetails grid-overflow-default" id="Div1" runat="server">
                                <asp:GridView ID="grdGroupCreation" runat="server" Width="100%" AutoGenerateColumns="false"
                                    ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                    CssClass="pshro_GridDgn" OnRowDataBound="grdGroupCreation_RowDataBound">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <Columns>
                                        <asp:BoundField DataField="Code" HeaderText="Code" />
                                        <asp:BoundField DataField="Description" HeaderText="Description" />
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="checkAll" runat="server"   Text="Select All" onclick="checkAll(this);" />
                                               <%-- <asp:Label ID="Label39" runat="server" Text="Select All"></asp:Label>--%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" Checked='<%# Eval("Select") %>' runat="server" onclick="Check_Click(this)" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gridDetails grid-overflow-default" id="HideFilter_GridOverFlow" runat="server">
        </div>
    </div>
</asp:Content>
