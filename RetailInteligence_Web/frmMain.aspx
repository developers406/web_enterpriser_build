﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RIMaster.master" AutoEventWireup="true"
    CodeBehind="frmMain.aspx.cs" Inherits="EnterpriserWebFinal.RetailInteligence_Web.frmMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    </style>
    <link href="../css/dashboard/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/jsCommon.js" type="text/javascript"></script>
    <link href="../css/dashboard/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/dashboard/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/dashboard/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/dashboard/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        *
        {
            margin: 0;
            padding: 0;
        }
        #display
        {
            background: transparent;
            height: 50px;
            width: 500px;
            overflow: hidden;
            position: relative;
        }
        #text
        {
            background: transparent;
            cursor: pointer;
            overflow: hidden;
            position: absolute;
            width: auto;
            left: 10px;
            margin-right: 10px;
        }
        .dropdown-menu > li > a
        {
            color: black;
            font-family: Roboto,sans-serif;
        }
        .main-nav > li > a
        {
            font-family: Roboto,sans-serif;
        }
        .container-fluid
        {
            padding-right: 5px;
            padding-left: 5px;
        }
    </style>
    <script type="text/javascript">

        function pageLoad() {


            try {

                var TrnsOutListtbody;
                fncGetSummaryRecord();
            }
            catch (err) {
                err.Message;
            }
        }

        //Date Initialize
        $(document).ready(function () {
            // marquee($('#display'), $('#text'));
            TrnsOutListtbody = $("#tblTrnsOutList tbody");
            TrnsInListtbody = $("#tblTrnsInList tbody");
        });
        function marquee(a, b) {
            var width = b.width();
            var start_pos = a.width();
            var end_pos = -width;

            function scroll() {
                if (b.position().left <= -width) {
                    b.css('left', start_pos);
                    scroll();
                }
                else {
                    time = (parseInt(b.position().left, 10) - end_pos) *
                (10000 / (start_pos - end_pos)); // Increase or decrease speed by changing value 10000
                    b.animate({
                        'left': -width
                    }, time, 'linear', function () {
                        scroll();
                    });
                }
            }

            b.css({
                'width': width,
                'left': start_pos
            });
            scroll(a, b);

            b.mouseenter(function () {     // Remove these lines
                b.stop();                 //
                b.clearQueue();           // if you don't want
            });                           //
            b.mouseleave(function () {     // marquee to pause
                scroll(a, b);             //
            });                           // on mouse over

        }


        function fncGetSummaryRecord() {
            try {
                $('[id*=Sales_Refresh]').show();
                $('[id*=Purchase_Refresh]').show();
                $('[id*=Stock_Refresh]').show();
                $('[id*=SalesRet_Refresh]').show();
                $('[id*=TransferIn_Refresh]').show();
                $('[id*=TransferOut_Refresh]').show();
                $.ajax({
                    type: "POST",
                    url: "frmMain.aspx/GetMainRecord",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {



                        var objvendor = jQuery.parseJSON(msg.d);
                        if (objvendor.Table.length > 0) {


                            $('#<%=lbl_SalesAmt.ClientID %>').text(Number(objvendor.Table[0]["TotalSales"]).toFixed(2));
                            if ($('#<%=lbl_SalesAmt.ClientID %>').text().length > 15) {
                                $('#<%=lbl_SalesAmt.ClientID %>').html("<marquee onmouseover='this.stop();' onmouseout='this.start();'><span>" + Number(objvendor.Table[0]["TotalSales"]).toFixed(2) + "</span></marquee>");
                            } else {
                                $('#<%=lbl_SalesAmt.ClientID %>').text(Number(objvendor.Table[0]["TotalSales"]).toFixed(2));
                            }

                        }
                        if (objvendor.Table1.length > 0) {

                            $('#<%=lbl_PurchaseAmt.ClientID %>').text(Number(objvendor.Table1[0]["PurTotal"]).toFixed(2));

                            if ($('#<%=lbl_PurchaseAmt.ClientID %>').text().length > 15) {
                                $('#<%=lbl_PurchaseAmt.ClientID %>').html("<marquee onmouseover='this.stop();' onmouseout='this.start();'><span>" + Number(objvendor.Table1[0]["PurTotal"]).toFixed(2) + "</span></marquee>");
                            } else {
                                $('#<%=lbl_PurchaseAmt.ClientID %>').text(Number(objvendor.Table1[0]["PurTotal"]).toFixed(2));
                            }

                        }
                        if (objvendor.Table2.length > 0) {

                            $('#<%=lbl_StockAmt.ClientID %>').text(Number(objvendor.Table2[0]["totalinvamt"]).toFixed(2));

                            //console.log($('#<%=lbl_StockAmt.ClientID %>').text().length);
                            if ($('#<%=lbl_StockAmt.ClientID %>').text().length > 15) {
                                $('#<%=lbl_StockAmt.ClientID %>').html("<marquee onmouseover='this.stop();' onmouseout='this.start();'><span>" + Number(objvendor.Table2[0]["totalinvamt"]).toFixed(2) + "</span></marquee>");
                            } else {

                                $('#<%=lbl_StockAmt.ClientID %>').text(Number(objvendor.Table2[0]["totalinvamt"]).toFixed(2));
                            }


                        }
                        if (objvendor.Table3.length > 0) {


                            $('#<%=lbl_SalesRetAmt.ClientID %>').text(Number(objvendor.Table3[0]["TotalRetSales"]).toFixed(2));
                            if ($('#<%=lbl_SalesRetAmt.ClientID %>').text().length > 15) {
                                $('#<%=lbl_SalesRetAmt.ClientID %>').html("<marquee onmouseover='this.stop();' onmouseout='this.start();'><span>" + Number(objvendor.Table3[0]["TotalRetSales"]).toFixed(2) + "</span></marquee>");
                            } else {
                                $('#<%=lbl_SalesRetAmt.ClientID %>').text(Number(objvendor.Table3[0]["TotalRetSales"]).toFixed(2));
                            }

                        }

                        $('[id*=Sales_Refresh]').hide();
                        $('[id*=Purchase_Refresh]').hide();
                        $('[id*=Stock_Refresh]').hide();
                        $('[id*=SalesRet_Refresh]').hide();



                        if (objvendor.Table4.length > 0) {
                            TrnsOutListtbody.children().remove();

                            for (var i = 0; i < objvendor.Table4.length; i++) {
                                TrnsOutListtbody.append(

                                "<tr></td><td id='tdTransferNo_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table4[i]["TransferNo"]
                                + "</td><td style=padding:5px;border-color:initial;>" + objvendor.Table4[i]["Date"]
                                + "</td><td style=padding:5px;border-color:initial;>" + objvendor.Table4[i]["ToLocation"]
                                + "</td><td  id='tdToLocationStatus_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table4[i]["FromLocationStatus"]
                                + "</td></tr>");
                            }

                        }
                        if (objvendor.Table5.length > 0) {

                            for (var i = 0; i < objvendor.Table5.length; i++) {
                                TrnsInListtbody.append(

                                "<tr></td><td id='tdTransferNo_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table5[i]["TransferNo"]
                                + "</td><td style=padding:5px;border-color:initial;>" + objvendor.Table5[i]["Date"]
                                + "</td><td style=padding:5px;border-color:initial;>" + objvendor.Table5[i]["FromLocation"]
                                + "</td><td  id='tdToLocationStatus_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table5[i]["ToLocationStatus"]
                                + "</td></tr>");
                            }

                        }

                        $('[id*=TransferIn_Refresh]').hide();
                        $('[id*=TransferOut_Refresh]').hide();
                        setTimeout('fncGetSummaryRecord()', 60000);
                    },
                    error: function (data) {
                        // alert('Something Went Wrong')
                        setTimeout('fncGetSummaryRecord()', 60000);
                    }
                });

            }
            catch (err) {
                alert(err.Message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="main-image">
            <h4 class="page-header">
                Today's Status
            </h4>
            <%--<br />
            <div>
                <asp:Label ID="lbl_Today" runat="server" Font-Bold="true">Today's Status</asp:Label>
            </div>
            <br />--%>
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><asp:Label ID="lbl_SalesAmt" runat="server"><span id="bSales" style="display:block;">0.00</span></asp:Label></h3>
                            <p>Sales</p>
                        </div>
                        <div class="icon" style="padding-top: 10px;">
                            <i class="ion ion-bag"></i>
                        </div>
                        <div id="Sales_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lbl_PurchaseAmt" runat="server"><span id="bPur" style="display:block;">0.00</span></asp:Label></h3>
                            <p>
                                Purchase</p>
                        </div>
                        <div class="icon" style="padding-top: 10px;">
                            <i class="ion ion-ios-cart-outline"></i>
                        </div>
                        <div id="Purchase_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <div id="display">
                                <h3 id="text">
                                    <asp:Label ID="lbl_StockAmt" runat="server"><span id="bStock" style="display:block">0.00</span></asp:Label></h3>
                            </div>
                            <p>
                                Stock</p>
                        </div>
                        <div class="icon" style="padding-top: 10px;">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <div id="Stock_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lbl_SalesRetAmt" runat="server"><span id="bSalRet" style="display:block;">0.00</span></asp:Label></h3>
                            <p>
                                Sales Return</p>
                        </div>
                        <div class="icon" style="padding-top: 10px;">
                            <i class="ion-arrow-return-left"></i>
                        </div>
                        <div id="SalesRet_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-4">
                    <!-- Success box -->
                    <div class="box box-solid bg-blue">
                        <div id="TransferOut_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px;
                            z-index: 0;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <div class="box-header">
                            <h3 class="box-title">
                                Transfer Out</h3>
                        </div>
                        <div class="box-body">
                            <div class="row" style="overflow: auto; height: 300px; width: auto">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <table id="tblTrnsOutList" cellspacing="0" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 34%; border-color: initial; border-width: 2px; text-align: center;
                                                    font-weight: bold;">
                                                    Transfer No
                                                </th>
                                                <th scope="col" style="width: 30%; border-color: initial; border-width: 2px; text-align: center;
                                                    font-weight: bold;">
                                                    Transfer Date
                                                </th>
                                                <th scope="col" style="border-color: initial; border-width: 2px; text-align: center;
                                                    font-weight: bold;">
                                                    To
                                                </th>
                                                <th scope="col" style="width: 35%; border-color: initial; border-width: 2px; text-align: center;
                                                    font-weight: bold;">
                                                    Status
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                    <!-- Success box -->
                    <div class="box box-solid bg-purple">
                        <div id="TransferIn_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px;
                            z-index: 0;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <div class="box-header">
                            <h3 class="box-title">
                                Transfer In</h3>
                        </div>
                        <div class="box-body">
                            <div class="row" style="overflow: auto; height: 300px; width: auto">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <table id="tblTrnsInList" cellspacing="0" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 34%; border-color: initial; border-width: 2px; text-align: center;
                                                    font-weight: bold;">
                                                    Transfer No
                                                </th>
                                                <th scope="col" style="width: 30%; border-color: initial; border-width: 2px; text-align: center;
                                                    font-weight: bold;">
                                                    Transfer Date
                                                </th>
                                                <th scope="col" style="border-color: initial; border-width: 2px; text-align: center;
                                                    font-weight: bold;">
                                                    From
                                                </th>
                                                <th scope="col" style="width: 35%; border-color: initial; border-width: 2px; text-align: center;
                                                    font-weight: bold;">
                                                    Status
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </div>
</asp:Content>
