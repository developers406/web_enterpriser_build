﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPickerSummary.aspx.cs" Inherits="EnterpriserWebFinal.Picker.frmPickerSummary" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        body {
            font-family: Arial, Helvetica, sans-serif !important;
            font-size: 15px !important;
        }

        .select {
            cursor: pointer;
        }

        .my_content_container {
            border-bottom: 1px solid #777777;
            border-left: 1px solid #000000;
            border-right: 1px solid #333333;
            border-top: 1px solid #000000;
            color: #000000;
            display: block;
            height: 2.5em;
            padding: 0 1em;
            width: 5em;
            text-decoration: none;
        }

        .btn2 {
            display: inline-block;
            cursor: pointer;
            text-align: center;
            outline: 1px;
            color: #fff;
            background-color: #5b923e;
            border: none;
            /* border-radius: 10px;
            box-shadow: 0 7px #CCCCCC;*/
            height: 30px;
            width: 103px;
        }

            .btn2:hover {
                background-color: #9999FF;
            }

            .btn2:active {
                background-color: #9999FF;
                box-shadow: 0 4px #666;
                transform: translateY(5px);
            }

        .btn3 {
            display: inline-block;
            cursor: pointer;
            text-align: center;
            outline: 1px;
            color: #fff;
            background-color: #3e4095;
            border: none;
            /*border-radius: 10px;
            box-shadow: 0 7px #CCCCCC;*/
            height: 30px;
            width: 103px;
        }

            .btn3:hover {
                background-color: #9999FF;
            }

            .btn3:active {
                background-color: #9999FF;
                box-shadow: 0 4px #666;
                transform: translateY(5px);
            }

        .GridStyle {
            border: #3e4095;
            background-color: White;
            font-family: arial;
            font-size: 12px;
            border-collapse: collapse;
            margin-bottom: 0px;
        }

            .GridStyle tr {
                border: 1px solid rgb(217, 231, 255);
                color: Black;
                height: 25px;
            }
            /* Your grid header column style */
            .GridStyle th {
                background-color: #3e4095;
                border: none;
                text-align: center;
                font-weight: bold;
                font-size: 15px;
                padding: 4px;
                color: #ffffff;
            }
            /* Your grid header link style */
            .GridStyle tr th a, .GridStyle tr th a:visited {
                color: Black;
            }

            .GridStyle tr th, .GridStyle tr td table tr td {
                border: none;
            }

            .GridStyle td {
                border-bottom: 1px solid rgb(217, 231, 255);
                padding: 2px;
            }

        .headers {
            font-size: 30px;
            font-family: sans-serif;
            color: #f1350a;
            font-weight: bold;
        }

        .ui-datepicker-trigger {
            margin-left: 305px;
            margin-top: -18px;
        }

        .GridPager a,
        .GridPager span {
            display: inline-block;
            padding: 5px 13px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background: #616161;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
            border: 1px solid #3AC0F2;
        }

        a:hover, a:focus {
            color: white !important;
            text-decoration: none !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({
                dateFormat: "yy-mm-dd",
                showOn: "button",
                buttonImage: "../images/calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true

            }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({
                dateFormat: "yy-mm-dd",
                showOn: "button",
                buttonImage: "../images/calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true

            }).datepicker("setDate", "0");
           // fncPickerSearch();

        });
     <%--   function fncPickerSearch() {
            try {
                $("#<%= txtPickerName.ClientID %>").autocomplete({
                    source: function (request, response) {
                        var obj = {};
                        obj.prefix = escape(request.term);
                        obj.location = $('#<%=txtPickerName.ClientID%>').val();

                        $.ajax({
                            url: "PickerSummaryDetails.aspx/GetSearchPickerName",
                            data: "{ 'Code': '" + $("#<%=ddlPickerName.ClientID%>").val() + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                debugger
                                response($.map(data.d, function (item) {
                                    debugger
                                    if (data.d[0] != null) {
                                        return {
                                            label: item.split('|')[0],
                                            valitemcode: item.split('|')[1]
                                        }
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    focus: function (event, i) {
                        debugger
                        $('#<%=txtPickerName.ClientID %>').val($.trim(i.item.label));
                        event.preventDefault();
                    },
                    select: function (e, i) {
                        debugger
                        $('#<%=txtPickerName.ClientID %>').val($.trim(i.item.valitemcode));
                    },
                    minLength: 0
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }--%>
        function datevalidation() {

            var from = $("#<%= txtFromDate.ClientID %>").val();
            var to = $("#<%= txtToDate.ClientID %>").val();

            if (Date.parse(from) > Date.parse(to)) {
                ShowPopupMessageBox("Invalid Date Range");
            }
        }
        function datassigned() {
            $('#<%= ddlPickerName.ClientID %>').prop('disabled', false);
        };
        function datassigneds() {
            $('#<%= ddlPickerName.ClientID %>').prop('disabled', true);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <article class="wrapper">
	    <section class="main">
	        <section class="tab-content">
	           <section class="tab-pane active fade in content" id="dashboard">
	                <div class="row" style="margin-top: 6px;">
                        <div class="headers">Bucket - 1</div>
                        <div class="col-xs-6 col-sm-5">
	                        <div class="panel panel-primary">
	                            <div class="panel-body" style="padding: 15px 5px 1px 5px !important;">
	                                     <div class="form-group row">
                                <label for="lblFromDate" style="font-weight:bold" class="col-md-4 col-form-label text-md-right">From Date</label>
                                <div class="col-md-6">
                                     <asp:TextBox ID="txtFromDate"  onchange="return datevalidation();" runat="server" CssClass="form-control-res select"></asp:TextBox>                                                                 
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lblToDate" style="font-weight:bold" class="col-md-4 col-form-label text-md-right">To Date</label>
                                <div class="col-md-6">                                 
                                   <asp:TextBox ID="txtToDate" runat="server"  onchange="return datevalidation();"  CssClass="form-control-res select"></asp:TextBox>

                                </div>
                            </div>
	                            </div>
	                        </div>
	                    </div>
	                    
	                    <div class="col-xs-6 col-sm-2">
	                        <div class="panel panel-primary">
	                            <div class="panel-body"  style="padding: 12px 5px 1px 5px !important;">
	                                   <div class="form-group row" style="margin-left: 42px;">
                                           <div class="col-md-12">
                                              <asp:RadioButton ID="rbAssigned" onchange="return datassigneds();"   Checked="true" Text="  Assigned" GroupName="gp_rbnunass" runat="server"></asp:RadioButton>                                                                                                       
                                </div>
                            </div>
                            <div class="form-group row" style="margin-left: 42px;">                              
                                <div class="col-md-12">   
                                     <asp:RadioButton ID="rbUnassigned" onchange="return datassigned();" Text="  Unassigned" GroupName="gp_rbnunass" runat="server"></asp:RadioButton>   
                                  
                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    </div>
	                    <div class="col-xs-6 col-sm-5">
	                        <div class="panel panel-primary">
	                            <div class="panel-body" style="padding: 15px 5px 1px 5px !important;">
	                                <div class="form-group row">
                               <label for="lblToDate" style="font-weight:bold" class="col-md-4 col-form-label text-md-right">Picker Name</label>
                                <div class="col-md-6">     
                                     <asp:DropDownList  placeholder="Search for Picker Name..." ID="ddlPickerName" AutoPostBack="true" runat="server" CssClass="form-control-res"></asp:DropDownList>   
                                   <%--<asp:TextBox ID="txtPickerName" placeholder="Search for Picker Name..." runat="server" CssClass="form-control-res"></asp:TextBox>   --%>                               
                                </div>
                            </div>

                            <div class="form-group row">
                                                             <div class="col-md-6"> 
                                      <div class="control-button">
                                             <asp:Button ID="btnsearch" runat="server" class="button-blue" OnClick="btnsearch_Click" style="margin-top: -17px; margin-left: 397px;" CssClass="form-control btn3" Text="Search"  />      
                                          </div>
                                </div>
                            </div>
	                            </div>
	                        </div>
	                    </div>
	               </div>
                   <div class="row" style="margin-top: 6px;">
                       <div class="col-xs-6 col-sm-12">
	                        <div class="panel panel-primary">
	                            <div class="panel-body" style="padding: 15px 5px 0px 5px !important;">
                                    <div class="col-md-12">
	                          <div class="col-md-11">
                                  <label for="lblsalesorderlist"  style="font-weight:bold;" class="col-md-4 col-form-label text-md-right">Sales Order List</label>
                                  </div>
                                    <div class="col-md-1">
                                         <div class="control-button">
                            <asp:LinkButton ID="lnkSave" OnClientClick="OnClieckEvent()" OnClick="lnkSave_Click" runat="server" style="margin-top: -10px;background-color: #c94e41;" CssClass="form-control btn2"><i class="icon-play"></i>Pick List</asp:LinkButton>
                        </div>                                                                      
                                    </div>
                                        </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
                   <div class="row" style="margin-top: 6px;">
                       <div class="col-xs-6 col-sm-12">
	                        <div class="panel panel-primary">
	                            <div class="panel-body">
                                  <asp:GridView ID="gv_pickerSummarydetails" CssClass="pshro_GridDgn grdLoad GridStyle GridPager" runat="server"  PageSize="10" OnPageIndexChanging="gv_pickerSummarydetails_PageIndexChanging" AllowPaging="true"  AutoGenerateColumns="False"  EmptyDataText="There are no data records to display." BorderStyle="Solid" Style="max-width: 100%">
    <Columns>        
        <asp:BoundField DataField="PNo" HeaderText="S.No" />
        <asp:BoundField DataField="SONo" HeaderText="SO No" />
        <asp:BoundField DataField="SODate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="So. Date" />
         <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" />     
           <asp:BoundField DataField="Phone" HeaderText="Cust Mobile No" /> 
         <asp:BoundField DataField="Address" HeaderText="Address" />        
        <asp:BoundField DataField="TotalItem" HeaderText="Total Order Items" /> 
         <asp:BoundField DataField="TotalValue" HeaderText="Net Order Value" />
        <asp:BoundField DataField="PickerName" HeaderText="Picker Name" />        
        <asp:BoundField DataField="PickerDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Assigned Date" />
        <asp:BoundField DataField="PickTime"  HeaderText="Picking Total Time" />
        <asp:BoundField DataField="Status"  HeaderText="Order Status" />
                                       
    </Columns>
</asp:GridView>

                                     <asp:GridView ID="gv_UnpickerSummarydetails" CssClass="pshro_GridDgn grdLoad GridStyle GridPager" runat="server" PageSize="10" OnPageIndexChanging="gv_UnpickerSummarydetails_PageIndexChanging"  AutoGenerateColumns="False" AllowPaging="true" EmptyDataText="There are no data records to display." BorderStyle="Solid" Style="max-width: 100%">
    <Columns>        
        <asp:BoundField DataField="PNo" HeaderText="S.No" />
        <asp:BoundField DataField="SONo" HeaderText="SO No" />
        <asp:BoundField DataField="SODate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="So. Date" />
         <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" />   
          <asp:BoundField DataField="Phone" HeaderText="Cust Mobile No" />   
         <asp:BoundField DataField="Address" HeaderText="Address" />        
        <asp:BoundField DataField="TotalItem" HeaderText="Total Order Items" /> 
         <asp:BoundField DataField="TotalValue" HeaderText="Net Order Value" />   
                        
    </Columns>
</asp:GridView>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	           </section>
	        </section>
	    </section>
	</article>
</asp:Content>
