﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="PickerSummaryDetails.aspx.cs" Inherits="EnterpriserWebFinal.Picker.PickerSummaryDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        body {
            font-family: Arial, Helvetica, sans-serif !important;
            font-size: 15px !important;
        }

        .select {
            cursor: pointer;
        }

        .my_content_container {
            border-bottom: 1px solid #777777;
            border-left: 1px solid #000000;
            border-right: 1px solid #333333;
            border-top: 1px solid #000000;
            color: #000000;
            display: block;
            height: 2.5em;
            padding: 0 1em;
            width: 5em;
            text-decoration: none;
        }
          .btn2 {
            display: inline-block;
            cursor: pointer;
            text-align: center;
            outline: 1px;
            color: #fff;
            background-color: #5b923e;
            border: none;
           /* border-radius: 10px;
            box-shadow: 0 7px #CCCCCC;*/
            height: 30px;
            width: 103px;
        }

            .btn2:hover {
                background-color: #9999FF;
            }

            .btn2:active {
                background-color: #9999FF;
                box-shadow: 0 4px #666;
                transform: translateY(5px);
            }
        .btn3 {
            display: inline-block;
            cursor: pointer;
            text-align: center;
            outline: 1px;
            color: #fff;
            background-color: #3e4095;
            border: none;
            /* border-radius: 10px;
            box-shadow: 0 7px #CCCCCC;*/
            height: 30px;
            width: 103px;
        }

            .btn3:hover {
                background-color: #9999FF;
            }

            .btn3:active {
                background-color: #9999FF;
                box-shadow: 0 4px #666;
                transform: translateY(5px);
            }

        .GridStyle {
            border: #3e4095;
            background-color: White;
            font-family: arial;
            font-size: 12px;
            border-collapse: collapse;
            margin-bottom: 0px;
        }

            .GridStyle tr {
                border: 1px solid rgb(217, 231, 255);
                color: Black;
                height: 25px;
            }
            /* Your grid header column style */
            .GridStyle th {
                background-color: #3e4095;
                border: none;
                text-align: center;
                font-weight: bold;
                font-size: 15px;
                padding: 4px;
                color: #ffffff;
            }
            /* Your grid header link style */
            .GridStyle tr th a, .GridStyle tr th a:visited {
                color: Black;
            }

            .GridStyle tr th, .GridStyle tr td table tr td {
                border: none;
            }

            .GridStyle td {
                border-bottom: 1px solid rgb(217, 231, 255);
                padding: 2px;
            }

        .headers {
            font-size: 30px;
            font-family: sans-serif;
            color: #f1350a;
            font-weight: bold;
        }

        .ui-datepicker-trigger {
            margin-left: 305px;
            margin-top: -18px;
        }


        .GridPager a,
        .GridPager span {
            display: inline-block;
            padding: 0px 9px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background: #616161;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
            border: 1px solid #3AC0F2;
        }
         a:hover, a:focus {
            color: white !important;
            text-decoration: none !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({
                dateFormat: "yy-mm-dd",
                showOn: "button",
                buttonImage: "../images/calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true
            }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({
                dateFormat: "yy-mm-dd",
                showOn: "button",
                buttonImage: "../images/calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true
            }).datepicker("setDate", "0");
           
        });
    
        function datevalidation() {
            var from = $("#<%= txtFromDate.ClientID %>").val();
                var to = $("#<%= txtToDate.ClientID %>").val();
            if (Date.parse(from) > Date.parse(to)) {
                ShowPopupMessageBox("Invalid Date Range");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <article class="wrapper">
	    <section class="main">
	        <section class="tab-content">
	           <section class="tab-pane active fade in content" id="dashboard">
	                <div class="row" style="margin-top: 6px;">
                        <div class="headers">Bucket - 2</div>
                        <div class="col-xs-6 col-sm-5">
	                        <div class="panel panel-primary">
	                            <div class="panel-body" style="padding: 8px 5px 1px 5px !important;">
	                                     <div class="form-group row">
                                <label for="lblFromDate" style="font-weight:bold" class="col-md-4 col-form-label text-md-right">From Date</label>
                                <div class="col-md-6">
                                     <asp:TextBox ID="txtFromDate" onchange="return datevalidation();" runat="server" CssClass="form-control-res select"></asp:TextBox>                                   
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lblToDate" style="font-weight:bold" class="col-md-4 col-form-label text-md-right">To Date</label>
                                <div class="col-md-6">                                 
                                   <asp:TextBox ID="txtToDate" runat="server"  onchange="return datevalidation();"  CssClass="form-control-res select"></asp:TextBox>

                                </div>
                            </div>
	                            </div>
	                        </div>
	                    </div>
	                    
	                    <div class="col-xs-6 col-sm-2">
	                        <div class="panel panel-primary">
	                            <div class="panel-body" style="padding: 8px 5px 1px 5px !important;">
	                                   <div class="form-group row" style="margin-left: 42px;">
                                           <div class="col-md-3">
                                               </div>    
                                <div class="col-md-9" style=" margin-left: 10px; font-weight: bold;">
                                      <asp:RadioButton ID="rbPending" Checked="true" Font-Bold="true"   Text="Pending" GroupName="gp_rbnunass" runat="server"></asp:RadioButton> 
                                </div>
                            </div>
                            <div class="form-group row" style="margin-left: 42px; margin-bottom: 10px;">                              
                                <div class="col-md-3">   
                                     </div>
                                    <div class="col-md-9" style=" margin-left: 10px; font-weight: bold;"> 
                                        <asp:RadioButton ID="rbCompleted" GroupName="gp_rbnunass" Font-Bold="true"  Text="Completed" runat="server"></asp:RadioButton>                                
                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    </div>
	                    <div class="col-xs-6 col-sm-5">
	                        <div class="panel panel-primary">
                                <div class="panel-body" style="padding: 7px 5px 1px 5px !important;">
	                                <div class="form-group row">
                               <label for="lblToDate" style="font-weight:bold" class="col-md-4 col-form-label text-md-right">Picker Name</label>
                                <div class="col-md-6">     
                                     <asp:DropDownList  placeholder="Search for Picker Name..." ID="ddlPickerName" AutoPostBack="true" runat="server" CssClass="form-control-res"></asp:DropDownList>   
                                </div>
                            </div>

                            <div class="form-group row">
                                                             <div class="col-md-6"> 
                                      <div class="control-button">
                                             <asp:Button ID="btnsearch" runat="server" class="button-blue" OnClick="btnsearch_Click" style="margin-top: -17px; margin-left: 397px;" CssClass="form-control btn3" Text="Search"  />      
                                          </div>
                                </div>
                            </div>
	                            </div>                 
	                        </div>
	                    </div>
	               </div>
                   <div class="row" style="margin-top: 6px;">
                       <div class="col-xs-6 col-sm-12">
	                        <div class="panel panel-primary">
	                            <div class="panel-body" style="padding: 8px 5px 1px 5px !important;">
                                    <div class="col-md-12">
	                          <div class="col-md-10">
                                  <label for="lblsalesorderlist"  style="font-weight:bold;margin-top:8px;" class="col-md-4 col-form-label text-md-right">Pick List</label>
                                  </div>
                                    <div class="col-md-2" style="top: -6px;">
                                          <div class="control-button" >

                                                    <asp:LinkButton ID="btnback" OnClick="btnback_Click" runat="server"  style="background-color: #2ecc71;" CssClass="form-control btn2"><i class="icon-play"></i> Back</asp:LinkButton>

                            <asp:LinkButton ID="lnkSave" style="background-color: #c94e41;" CssClass="form-control btn2" OnClientClick="OnCreateEvent()" OnClick="lnkSave_Click" runat="server" ><i class="icon-play"></i> Create</asp:LinkButton>
                        </div>                                                                      
                                    </div>
                                        </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
                   <div class="row" style="margin-top: 6px;">
                       <div class="col-xs-6 col-sm-12">
	                        <div class="panel panel-primary">
	                            <div class="panel-body">
                                   <asp:GridView ID="gv_pickerSummarydetails" CssClass="pshro_GridDgn grdLoad GridStyle GridPager" runat="server" OnRowDataBound="gv_pickerSummarydetails_RowDataBound" AllowPaging="true" AutoGenerateColumns="False" PageSize = "10" OnPageIndexChanging="gv_pickerSummarydetails_PageIndexChanging" OnSelectedIndexChanged="gv_pickerSummarydetails_SelectedIndexChanged" EmptyDataText="There are no data records to display." BorderStyle="Solid" Style="max-width: 100%">
    <Columns>        
        <asp:BoundField DataField="PNo" HeaderText="S.No" />
        <asp:BoundField DataField="PCID" HeaderText="Picker No" />
         <asp:BoundField DataField="PickerDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Picker Date" />       
        <asp:BoundField DataField="OrderCount" HeaderText="Total order" />         
        <asp:BoundField DataField="PickerName" HeaderText="Picker Name" />
        <asp:BoundField DataField="Status" HeaderText="Status" />
         <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" OnClick="imgbtnEdit_Click" ImageUrl="~/images/edit-icon.png" ToolTip="Click here to delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
    </Columns>
</asp:GridView>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	           </section>
	        </section>
	    </section>
	</article>
</asp:Content>
