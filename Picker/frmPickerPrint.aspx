﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmPickerPrint.aspx.cs" Inherits="EnterpriserWebFinal.Picker.frmPickerPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style media="screen">
        .noPrint {
            display: block;
        }

        .yesPrint {
            display: block !important;
        }
    </style>
    <style media="print">
        .noPrint {
            display: none;
        }

        .yesPrint {
            display: block !important;
        }
    </style>
    <title></title>
</head>

<body>

   <table>
  <tr>
    <td class="noPrint">Navigate</td>
    <td class="yesPrint">Content</td>
    <td>My button is hidden <input type="button" 
          value="Print" class="noPrint" onclick="window.print();"></td>
  </tr>
</table>

    <br />
    <br />
    <link href="style.css" rel="stylesheet" type="text/css" />
    <div id="dvContents">
        <div class="left">
            <img alt="" src="images/ASPSnippets.png" />
        </div>
        <div class="right">
            <span class="label">ASPSnippets.com Sample page</span>
        </div>
        <div class="clear">
        </div>
        <hr />
        <div class="contents">
            <span class="label">Hello,
                <br />
                This is <span class="name">Mudassar Khan</span>.<br />
                Hoping that you are enjoying my articles!</span>
        </div>
    </div>
    <br />
    <input type="button" id="btnPrint" value="Print" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnPrint").click(function () {
                var contents = $("#dvContents").html();
                var frame1 = $('<iframe />');
                frame1[0].name = "frame1";
                frame1.css({ "position": "absolute", "top": "-1000000px" });
                $("body").append(frame1);
                var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
                frameDoc.document.open();
                //Create a new HTML document.
                frameDoc.document.write('<html><head><title>DIV Contents</title>');
                frameDoc.document.write('</head><body>');
                //Append the external CSS file.
                frameDoc.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
                //Append the DIV contents.
                frameDoc.document.write(contents);
                frameDoc.document.write('</body></html>');
                frameDoc.document.close();
                setTimeout(function () {
                    window.frames["frame1"].focus();
                    window.frames["frame1"].print();
                    frame1.remove();
                }, 500);
            });
        });
    </script>
</body>
</html>
