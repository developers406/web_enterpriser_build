﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="PickerCreating.aspx.cs" EnableEventValidation="false" Inherits="EnterpriserWebFinal.Picker.PickerCreating" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css" media="print">
        @page {
            size: auto; /* auto is the initial value */
            margin: 4.5mm; /* this affects the margin in the printer settings */
        }
    </style>
    <style type="text/css">
        .headertag {
            width: 100%;
            height: 100%;
            font-Size: 10px;
            font-Weight: 800;
            font-Family: system-ui;
            text-Align: Center;
            color: Black;
        }

        .headertagss {
            width: 100%;
            height: 100%;
            top: 10px;
            font-Size: 10px;
            font-Weight: 800;
            font-Family: system-ui;
            text-Align: Center;
            color: Black;
        }

        .headertagleft {
            width: 100%;
            height: 100%;
            font-Size: 10px;
            font-Weight: 900;
            font-Family: system-ui;
            text-Align: left;
            color: Black;
        }

        .headertagright {
            width: 100%;
            height: 100%;
            font-Size: 10px;
            font-Weight: bold;
            font-Family: system-ui;
            text-Align: right;
            color: Black;
        }


        .tagsleft {
            font-Size: 10px;
            font-Weight: bold;
            font-Family: system-ui;
            text-Align: left;
            color: Black;
        }

        .tagsright {
            font-Size: 10px;
            font-Weight: bold;
            font-Family: system-ui;
            text-Align: right;
            color: Black;
        }

        .alignleft {
            float: left;
        }

        .alignright {
            float: right;
        }

        .simpleshape1 {
            color: #fff;
            background-color: #2ecc71;
            height: 38px;
            width: 175px;
            padding: 0px;
            border: none 0px transparent;
            font-size: 25px;
            font-weight: lighter;
            webkit-border-radius: 2px 16px 16px 16px;
            -moz-border-radius: 2px 16px 16px 16px;
            border-radius: 16px 16px 16px 16px;
        }

            .simpleshape1:hover {
                background-color: #e74c3c;
                border: solid 1px #fff;
            }

            .simpleshape1:focus {
                color: #383838;
                background-color: #fff;
                border: solid 3px rgba(98,176,255,0.3);
            }

        .btn.btn-success {
            background: #ffffff !important;
            text-shadow: none;
            color: #da1313 !important;
            filter: none;
            font-size: 22px;
        }

        .btn2 {
            display: inline-block;
            cursor: pointer;
            text-align: center;
            outline: 1px;
            color: #fff;
            background-color: #5b923e;
            border: none;
            /* border-radius: 10px;
            box-shadow: 0 7px #CCCCCC;*/
            height: 30px;
            width: 103px;
        }

        .chzn-container-single .chzn-single {
            color: black;
        }

        .btn2:hover {
            background-color: #9999FF;
        }

        .btn2:active {
            background-color: #9999FF;
            box-shadow: 0 4px #666;
            transform: translateY(5px);
        }

        .btn3 {
            display: inline-block;
            cursor: pointer;
            text-align: center;
            outline: 1px;
            color: #fff;
            background-color: #3e4095;
            border: none;
            /* border-radius: 10px;
            box-shadow: 0 7px #CCCCCC;*/
            height: 30px;
            width: 103px;
        }

            .btn3:hover {
                background-color: #9999FF;
            }

            .btn3:active {
                background-color: #9999FF;
                box-shadow: 0 4px #666;
                transform: translateY(5px);
            }

        .GridStyle {
            border: #3e4095;
            background-color: White;
            font-family: arial;
            font-size: 12px;
            border-collapse: collapse;
            margin-bottom: 0px;
        }

            .GridStyle tr {
                border: 1px solid rgb(217, 231, 255);
                color: Black;
                height: 25px;
            }
            /* Your grid header column style */
            .GridStyle th {
                background-color: #3e4095;
                border: none;
                text-align: center;
                font-weight: bold;
                font-size: 15px;
                padding: 4px;
                color: #ffffff;
            }
            /* Your grid header link style */
            .GridStyle tr th a, .GridStyle tr th a:visited {
                color: Black;
            }

            .GridStyle tr th, .GridStyle tr td table tr td {
                border: none;
            }

            .GridStyle td {
                border-bottom: 1px solid rgb(217, 231, 255);
                padding: 2px;
            }


        .table th {
            text-align: center !important;
            border-color: #ffffff !important;
            text-align: left !important;
        }

        .td, th {
            padding: 0px !important;
            border-color: #ffffff !important;
        }

        th {
            text-align: center !important;
            font-Weight: bold;
        }

        td {
            text-align: center !important;
            font-Weight: bold;
        }

        .headers {
            font-size: 30px;
            font-family: sans-serif;
            color: #f1350a;
            font-weight: bold;
        }

        .imglist {
            width: 30%;
        }

        .imglists {
            width: 7%;
        }

        .ui-datepicker-trigger {
            margin-left: 240px;
            margin-top: -18px;
        }

        .popupstyle {
            position: absolute;
            height: auto;
            width: 700px;
            top: 75px;
            left: 400px;
            /* display: block; */
            z-index: 101;
        }

        .GridPager a,
        .GridPager span {
            display: inline-block;
            padding: 0px 9px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background: #616161;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
            border: 1px solid #3AC0F2;
        }

        a:hover, a:focus {
            color: white !important;
            text-decoration: none !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({
                dateFormat: "yy-mm-dd",
                showOn: "button",
                buttonImage: "../images/calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true
            }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({
                dateFormat: "yy-mm-dd",
                showOn: "button",
                buttonImage: "../images/calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true
            }).datepicker("setDate", "0");
        });

        function fncShowBarcodeFailureMessage() {
            try {
                ShowPopupMessageBox("Please check at least one checkbox...");
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function PopupMessage(data) {
            console.log(data);
        }
        function datevalidation() {
            var from = $("#<%= txtFromDate.ClientID %>").val();
            var to = $("#<%= txtToDate.ClientID %>").val();
            if (Date.parse(from) > Date.parse(to)) {
                ShowPopupMessageBox("Invalid Date Range");
                $("#<%= txtFromDate.ClientID %>").datepicker({
                    dateFormat: "yy-mm-dd",
                    showOn: "button",
                    buttonImage: "~/images/calendar.gif",
                    buttonImageOnly: true,
                    changeMonth: true,
                    changeYear: true
                }).datepicker("setDate", "0");
                $("#<%= txtToDate.ClientID %>").datepicker({
                    dateFormat: "yy-mm-dd",
                    showOn: "button",
                    buttonImage: "~/images/calendar.gif",
                    buttonImageOnly: true,
                    changeMonth: true,
                    changeYear: true
                }).datepicker("setDate", "0");
            }
            else {
                var from = $("#<%= txtFromDate.ClientID %>").val();
                var to = $("#<%= txtToDate.ClientID %>").val();
                var sono = $('#<%=ddlpickername.ClientID %> option:selected').text()
                filterdatedetails(from, to, sono);
            }
        }
        function filterdatedetails(from, to, sono) {
            $.ajax({
                url: "PickerCreating.aspx/GtSearchList",
                data: "{ 'FromDate': '" + from + "','ToDate': '" + to + "','SONO': '" + sono + "'}",
                dataType: "json",
                type: "Post",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                }
            });
        }
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
        function fncValidation() {
            debugger
            var pname = $('#<%=ddlpickername.ClientID %> option:selected').text();
            if (pname == "---Select---") {
                ShowPopupMessageBox("Select Picker Name...");
                return false;
            }
        }
        function fncShowBarcodeSuccessMessage() {

            ShowPopupMessageBox("Data saved successfully...");

            return false;
        }
        function fncShowPrinter() {
            $("#btnPrint").show();
            ShowPopupMessageBox("Successfully assigned picker...");
        }
        function fncShowPrinters() {
            $("#btnPrint").show();
        }
        function fncShowDelete() {
            ShowPopupMessageBox("Data Deleted successfully...");
            window.location.href = "../Picker/PickerSummaryDetails.aspx";
        }
        function ShowPopup() {
            $("#dialog").dialog({
                title: "Item List",
                width: 750,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }
        function OpenthePOPup() {
            ShowPopup();
            return false;
        }
        function Cols() {
            ShowPopup();
        }


        function txtCQty_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent(); //GET CURRENT ROW FOR UP AND DOWN KEY PRESS
            var row = lnk.parentNode.parentNode; //GET CURRENT ROW FOR LEFT AND RIGHT KEY PRESS
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtCQty"]').select();
                        return;
                    }
                }
                else {
                    validation = false;
                }
            }
            if (charCode == 39) {
                row.cells[6].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtCQty"]').select();
                    return;
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtCQty"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtCQty"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtCQty"]').select();
                }
            }
            return true;


        }
        function txtCQty_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;// Row Index
                var sSellingPrice = row.cells[6].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].getElementsByTagName("input")[0].value;
                var sNetCost = row.cells[11].innerHTML;
                if (sMRP != "") {
                    if (parseFloat(sSellingPrice) > parseFloat(sMRP)) // SellingPrice > MRP
                    {
                        row.cells[5].getElementsByTagName("input")[0].value = row.cells[5].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False
                        validation = true;
                        ShowPopupMessageBox("MRP Should be Greater than or Equal to Sellinprice", "#ContentPlaceHolder1_grdNonBatchPriceChange_txtMRP_" + rowIndex);

                        return;
                    }
                    else if (parseFloat(sNetCost) > parseFloat(sMRP)) {
                        row.cells[5].getElementsByTagName("input")[0].value = row.cells[5].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    
                        validation = true;
                        ShowPopupMessageBox("MRP must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_grdNonBatchPriceChange_txtMRP_" + rowIndex);

                        return;
                    }
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true; //Check Box False
                //row.cells[8].getElementsByTagName("input")[0].select(); // Focus to Next Row  
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function setCustID(SONo, InventoryCode) {
            $.ajax({
                url: "PickerCreating.aspx/DeleteSelectItem",
                data: "{ 'SONo': '" + SONo + "','InventoryCode': '" + InventoryCode + "'}",
                dataType: "json",
                type: "Post",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d = "Success") {
                        location.reload();
                    }
                }
            });


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <article class="wrapper">
	    <section class="main">
	        <section class="tab-content">
	           <section class="tab-pane active fade in content" id="dashboard">	              
                   <div class="row" style="margin-top: 6px;">
                       <div class="headers">Bucket - 3</div>
                       <div class="col-xs-6 col-sm-12">
	                        <div class="panel panel-primary">
	                            <div class="panel-body" style="padding: 5px 0px 0px 0px !important;">
                                   
                                    <div class="col-md-12">
                                         
	                          <div class="col-md-9">
                                   <div class="col-md-5">
                                  <div class="panel-body">
                                      
	                                     <div class="form-group row" >
                                             <div class="col-md-12">
                                <label for="lblPickerID" style="font-weight:bold" class="col-md-2 col-form-label text-md-right">Pick No</label>
                                <div class="col-md-4">
                                     <asp:TextBox ID="txtPicker_ID"  runat="server" CssClass="form-control-res select"></asp:TextBox>                                   
                                </div>                         
                                <label for="lblToDate" style="font-weight:bold" class="col-md-3 col-form-label text-md-right">Picker Name</label>
                                <div class="col-md-3">                                 
                                  <asp:DropDownList style="width:202px;" ID="ddlpickername" AutoPostBack="false" runat="server"></asp:DropDownList>
                                </div>
                                                 </div>
                            </div>
                                       </div>
	                            </div>

                                  </div>
                                    <div class="col-md-3">
                                        <div class="panel-body" style="padding: 5px 0px 0px 0px !important;">
	                                     <div class="form-group row">
                                             <div class="col-md-12">
                                                   <div class="col-md-9">                                 
                                  <asp:Button  Text="Completed" OnClick="btncompleted_Click"  ID="btncompleted" CssClass="simpleshape1" runat="server" />
                                </div>
                                                 <div class="col-md-3">
                                 <asp:LinkButton runat="server" ID="btndelete"  OnClick="btndelete_Click" CssClass="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></asp:LinkButton>                    
                                </div>
                                               
                                                 </div>
                                
                            </div>
                         
	                            </div>                                                                  
                                      </div>
                                   </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
                   <div class="row" style="margin-top: 6px;">
                      <div class="col-xs-6 col-sm-12">
	                        <div class="panel panel-primary">
	                            <div class="panel-body" style="padding: 10px 7px 0px 7px!important;">
                              <div class="col-md-6"> 
                                  <div class="col-md-12" >
	                        <div class="form-group row">
                                 <div class="col-md-12">
                                <label for="lblFromDate" style="font-weight:bold" class="col-md-2 col-form-label text-md-right">From Date</label>
                                <div class="col-md-4">
                                     <asp:TextBox ID="txtFromDate" onchange="return datevalidation();" OnTextChanged="txtFromDate_TextChanged"  runat="server" CssClass="form-control-res select"></asp:TextBox>                                   
                                </div>                          
                                <label for="lblToDate" style="font-weight:bold;left: 35px;" class="col-md-2 col-form-label text-md-right">To Date</label>
                                <div class="col-md-4">                                 
                                   <asp:TextBox ID="txtToDate" runat="server"  onchange="return datevalidation();" CssClass="form-control-res select"></asp:TextBox>

                                </div>
                                     </div>
                            </div>
	                           </div>
                                   </div>
                                    <div class="col-md-6"> 
                                           <div class="col-md-12" >
	                        <div class="form-group row">
                                <label for="lblSONo" style="font-weight:bold;text-align: center;" class="col-md-1 col-form-label text-md-right">SO No</label>
                                <div class="col-md-4">                                  
                                    <asp:DropDownList style="width:202px;" ID="ddlsonodetails" AutoPostBack="false" runat="server"></asp:DropDownList>                                   
                                </div>                        
                                <label for="lblToDate" style="font-weight:bold;" class="col-md-2 col-form-label text-md-right">Delivery Time</label>
                                <div class="col-md-3" >                                 
                                  <asp:DropDownList style="width:202px;" ID="ddlDeliveryTime" AutoPostBack="false" runat="server"></asp:DropDownList>  
                                    
                                </div>
                                 <div class="col-md-2" >                                 
                                   <asp:Button ID="btnsearch" style="margin-left: 27px;margin-top: -5px;" runat="server" class="button-red" CssClass="form-control btn3" Text="Search" OnClick="btnsearch_Click" />
                                </div>
                                              </div>
                                        </div>
	                            </div>
                                   
	                        </div>
	                    </div>
	                </div>
                    <div class="row" style="margin-top: 6px;">
                       <div class="col-xs-6 col-sm-12">
	                        <div class="panel panel-primary">
	                            <div class="panel-body">
                                    <asp:GridView ID="gv_pickerdetails" CssClass="pshro_GridDgn grdLoad GridStyle GridPager" runat="server" PageSize = "10" OnPageIndexChanging="gv_pickerdetails_PageIndexChanging" AllowPaging="true"   AutoGenerateColumns="False" EmptyDataText="There are no data records to display." BorderStyle="Solid" Style="max-width: 100%">
    <Columns>
         <asp:TemplateField>
                <HeaderTemplate>
                    <asp:CheckBox ID="chkb1"  runat="server"  AutoPostBack="true" OnCheckedChanged="sellectAll"/>    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkb2" runat="server" AutoPostBack="true" OnCheckedChanged="Singlesellect" />
                </ItemTemplate>
            </asp:TemplateField>        
        <asp:BoundField DataField="SONo" HeaderText="SO No" />
        <asp:BoundField DataField="SODate" HeaderText="SO Date" DataFormatString="{0:dd/MM/yyyy}" />
        <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" />
        <asp:BoundField DataField="Phone" HeaderText="Cust Mobile No" />
        <asp:BoundField DataField="Itemcount" HeaderText="Total Item's" />
        <asp:BoundField DataField="NetTotal" HeaderText="Net Total" />
        <asp:TemplateField HeaderText="Distribution" ItemStyle-CssClass="imglists" >
        <ItemTemplate >
        <asp:ImageButton ID="Add_Button" runat="server" CssClass="imglist" OnClick="Add_Button_Click"    ImageUrl="../images/distribute_icon.png" ToolTip="Click here to Distribution" />
        </ItemTemplate>
        </asp:TemplateField>           
    </Columns>
</asp:GridView>
	                            </div>
	                        </div>
	                    </div>
	                </div>
                    <div class="row" style="margin-top: 6px;">
                       <div class="col-xs-6 col-sm-12">
	                        <div class="panel panel-primary">
	                            <div class="panel-body">
                              <div class="col-md-9"> 
                                <body >
       <link type="text/css" href="../css/style_printer.css" rel="stylesheet" />
         <a href="javascript:void(0)" id="btnPrint" onclick="fncprinterdetails()" style="display:none;" class="btn btn-primary hidden-print"><span class="glyphicon glyphicon-print"></span> Print</a>
        <%--<input type="button"   value="Print" />       --%>                           
        <div id="divmaster"></div>

                                     <div id="divheader"></div> 
       
        <script type="text/javascript">

            function fncprinterdetails() {
                var arrylist = [];
                $.ajax({
                    type: "POST",
                    url: "PickerCreating.aspx/GetPrinterHeaderDetails",
                    data: "{ 'Print_PCID': '" + $("#<%= hidPrint_PCID.ClientID %>").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $.each(data.d, function (key, val) {
                            if (val.SONO != "OVER") {
                                arrylist.push(val.SONO);
                            }
                        });
                        var SONOLIST = arrylist.join(",");
                        console.log(SONOLIST);
                        $.each(data.d, function (key, valus) {
                            var dvContent = document.createElement("div");
                            dvContent.id = "dvContents";
                            $.ajax({
                                type: "POST",
                                url: "PickerCreating.aspx/GetPrinterDetails",
                                data: "{ 'PCID': '" + valus.PCID + "','SONO': '" + valus.SONO + "'}",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data) {
                                    console.log(valus.SONO);
                                    if (data != null) {
                                        var date = new Date();
                                        var CurrentDate = formatDate(date);
                                        var CompanyNmae = document.createElement("div");
                                        CompanyNmae.className = "headertag";
                                        CompanyNmae.innerHTML = data.d[1][0].CompanyNmae;
                                        var Details = document.createElement("div");
                                        Details.className = "headertag";
                                        if (valus.SONO == "OVER") {
                                            Details.innerHTML = "Pick List";
                                        }
                                        else {
                                            Details.innerHTML = "Sales Order";
                                        }
                                        var line0 = document.createElement("div");
                                        line0.className = "headertag";
                                        line0.innerHTML = "**************************************************************************************************************************************************************************";
                                        var PNo = document.createElement("div");
                                        PNo.className = "headertagleft";
                                        PNo.innerHTML = "Picker No : " + valus.PCID;
                                        var PName = document.createElement("div");
                                        PName.className = "headertagleft";
                                        PName.innerHTML = "Picker Name : " + valus.PickerName;
                                        var PDate = document.createElement("div");
                                        PDate.className = "headertagleft";
                                        PDate.innerHTML = "Picker Date : " + valus.PickerDate;
                                        var line1 = document.createElement("div");
                                        var lineOver = document.createElement("div");
                                        line1.className = "headertag";
                                        lineOver.className = "headertagss";
                                        if (valus.SONO == "OVER") {
                                            line1.innerHTML = "**************************************************************************************************************************************************************************";
                                            lineOver.innerHTML = "**************************************************************************************************************************************************************************";
                                        }
                                        else {
                                            line1.innerHTML = "**************************************************************************************************************************************************************************";
                                        }
                                        var BillNo = document.createElement("div");
                                        BillNo.className = "headertagleft";
                                        if (valus.SONO == "OVER") {
                                            BillNo.innerHTML = "S No : " + "Consolidate List";
                                        }
                                        else {
                                            BillNo.innerHTML = "S No : " + valus.SONO;
                                        }
                                        var BillDate = document.createElement("div");
                                        BillDate.className = "headertagleft";
                                        BillDate.innerHTML = "S Date : " + CurrentDate;
                                        var line2 = document.createElement("div");
                                        var linetwo = document.createElement("div");
                                        line2.className = "headertag";
                                        linetwo.className = "headertagss";
                                        if (valus.SONO == "OVER") {
                                            line2.innerHTML = "**************************************************************************************************************************************************************************";
                                            linetwo.innerHTML = "**************************************************************************************************************************************************************************";
                                        }
                                        else {
                                            line2.innerHTML = "**************************************************************************************************************************************************************************";
                                        }
                                        document.getElementById("dvContents").appendChild(CompanyNmae);
                                        document.getElementById("dvContents").appendChild(Details);
                                        document.getElementById("dvContents").appendChild(line0);
                                        document.getElementById("dvContents").appendChild(PNo);
                                        document.getElementById("dvContents").appendChild(PName);
                                        document.getElementById("dvContents").appendChild(PDate);
                                        if (valus.SONO == "OVER") {
                                            document.getElementById("dvContents").appendChild(line1);
                                            document.getElementById("dvContents").appendChild(lineOver);
                                        }
                                        else {
                                            document.getElementById("dvContents").appendChild(line1);
                                        }
                                        document.getElementById("dvContents").appendChild(BillNo);
                                        document.getElementById("dvContents").appendChild(BillDate);
                                        document.getElementById("dvContents").appendChild(line2);
                                        document.getElementById("dvContents").appendChild(linetwo);
                                        var table = document.createElement("table");
                                        table.id = "usersTable";
                                        table.style.width = "100%"
                                        table.style.height = "100%"
                                        table.style.fontSize = "10px";
                                        table.style.fontWeight = "bold";
                                        document.getElementById("dvContents").appendChild(table);
                                        var tableBody = "";
                                        var columns = [];
                                        if (valus.SONO == "OVER") {
                                            debugger
                                            var parsJson = JSON.parse(data.d[0][0].p1);
                                            console.log(parsJson);
                                            tableBody = tableBody + "<tr>";
                                            for (var prop in parsJson[0]) {
                                                if (parsJson[0].hasOwnProperty(prop)) {
                                                    if (prop == "No") {
                                                        tableBody = tableBody + ("<th>" + prop + "</th>");
                                                        columns.push(prop);
                                                    }
                                                    else if (prop == "Description") {
                                                        tableBody = tableBody + ("<th style='text-align:center !important;'>" + "Product" + "</th>");
                                                        columns.push(prop);
                                                    }
                                                    else if (prop == "Price") {
                                                        tableBody = tableBody + ("<th style='text-align:right !important;'>" + "MRP" + "</th>");
                                                        columns.push(prop);
                                                    }
                                                    else {
                                                        tableBody = tableBody + ("<th style='text-align:right !important;'>" + " " + prop + "</th>");
                                                        columns.push(prop);
                                                    }
                                                }
                                            }
                                            tableBody = tableBody + "</tr>";
                                            parsJson.forEach(function (row) {
                                                tableBody = tableBody + "<tr>";
                                                columns.forEach(function (cell) {
                                                    if (cell == "No") {
                                                        tableBody = tableBody + "<td style='text-align:left !important;'>" + row[cell] + "</td>";
                                                    }
                                                    else if (cell == "Description") {
                                                        tableBody = tableBody + "<td style='text-align:left !important;'>" + row[cell] + "</td>";
                                                    }
                                                    else if (cell == "Price") {
                                                        tableBody = tableBody + "<td style='text-align:center !important;'>" + row[cell] + "</td>";
                                                    }
                                                    else {
                                                        tableBody = tableBody + "<td style='text-align:center !important;'>" + row[cell] + "</td>";
                                                    }
                                                });
                                                tableBody = tableBody + "</tr>";
                                            });
                                        }
                                        else {
                                            tableBody = tableBody + "<tr>";
                                            for (var prop in data.d[0][0]) {
                                                if (data.d[0][0].hasOwnProperty(prop)) {

                                                    if (prop == "Product") {
                                                        tableBody = tableBody + ("<th style='text-align:center !important;'>" + prop + "</th>");
                                                        columns.push(prop);
                                                    }
                                                    else if (prop == "MRP") {
                                                        tableBody = tableBody + ("<th style='text-align:right !important;'>" + prop + "</th>");
                                                        columns.push(prop);
                                                    }
                                                    else if (prop != "p1" && prop != "p2" && prop != "p3") {
                                                        tableBody = tableBody + ("<th>" + prop + "</th>");
                                                        columns.push(prop);
                                                    }
                                                }
                                            }
                                            tableBody = tableBody + "</tr>";
                                            data.d[0].forEach(function (row) {
                                                tableBody = tableBody + "<tr>";
                                                columns.forEach(function (cell) {
                                                    if (cell == "No") {
                                                        tableBody = tableBody + "<td style='text-align:left !important;'>" + row[cell] + "</td>";
                                                    }
                                                    else if (cell == "Product") {
                                                        tableBody = tableBody + "<td style='text-align:left !important;'>" + row[cell] + "</td>";
                                                    }
                                                    else if (cell == "Qty") {
                                                        tableBody = tableBody + "<td style='text-align:center !important;'>" + row[cell] + "</td>";
                                                    }
                                                    else if (cell == "MRP") {
                                                        tableBody = tableBody + "<td style='text-align:right !important;'>" + row[cell] + "</td>";
                                                    }
                                                });
                                                tableBody = tableBody + "</tr>";
                                            });
                                        }
                                        $("#usersTable").append(tableBody);
                                        var line3 = document.createElement("div");
                                        line3.className = "headertag";
                                        line3.innerHTML = "**************************************************************************************************************************************************************************";
                                        var TotalTag = document.createElement("div");
                                        TotalTag.id = "dvTotalTag";
                                        var TotalLable = document.createElement("P");
                                        TotalLable.className = "alignleft tagsleft";
                                        TotalLable.innerHTML = "Total Amount";
                                        var TotalAmount = document.createElement("P");
                                        TotalAmount.className = "alignright tagsright";
                                        TotalAmount.innerHTML = data.d[2][0].TotalMRP;
                                        var line5 = document.createElement("div");
                                        line5.className = "headertag";
                                        line5.innerHTML = ".";
                                        var TotalItemTag = document.createElement("div");
                                        TotalItemTag.id = "dvTotalItemTag";
                                        var TotalItem = document.createElement("P");
                                        TotalItem.className = "alignleft tagsleft";
                                        TotalItem.style = "margin-left: -65px;";
                                        TotalItem.innerHTML = "Total Item";
                                        var TotalCount = document.createElement("P");
                                        TotalCount.className = "alignright tagsright";
                                        TotalCount.style = "margin-right: -30px;";
                                        TotalCount.innerHTML = data.d[2][0].TotalQty;
                                        var line4 = document.createElement("div");
                                        line4.className = "headertag";
                                        line4.innerHTML = "**************************************************************************************************************************************************************************";
                                        document.getElementById("dvContents").appendChild(line3);
                                        document.getElementById("dvContents").appendChild(TotalTag);
                                        document.getElementById("dvTotalTag").appendChild(TotalLable);
                                        document.getElementById("dvTotalTag").appendChild(TotalAmount);
                                        document.getElementById("dvContents").appendChild(line5);
                                        document.getElementById("dvContents").appendChild(TotalItemTag);
                                        document.getElementById("dvTotalItemTag").appendChild(TotalItem);
                                        document.getElementById("dvTotalItemTag").appendChild(TotalCount);
                                        document.getElementById("dvContents").appendChild(line4);

                                        //printDiv('divmaster');
                                        //jQuery('#divmaster div').html('');
                                    }
                                    printDiv('divmaster');
                                    jQuery('#divmaster div').html('');
                                },

                                failure: function (response) {
                                    alert("Boobalan");
                                },
                                error: function (response) {
                                    alert("Boobalan1");
                                }
                            });
                            document.getElementById("divmaster").appendChild(dvContent);
                            //document.getElementById("divheader").appendChild(dvContent);
                            //printDiv('divheader');
                        });
                    },
                    failure: function (response) {
                        alert(response.d);
                    },
                    error: function (response) {
                        alert(response.d);
                    }
                });
            }

            function printDiv(divName) {

                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
            function formatDate(date) {
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0' + minutes : minutes;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                return date.getDate() + 1 + "-" + date.getMonth() + "-" + date.getFullYear() + " " + strTime;
            }


            function itemdelete() {

            }


        </script>
    </body>

                                  </div>
                                     <div class="col-md-3"> 
                                        
                                              <asp:Button  Text="Assigned" ID="btnassigned" OnClientClick="return fncValidation();" OnClick="btnassigned_Click" CssClass="form-control btn2" runat="server" />
                                              <asp:Button Text="Cancel" ID="btncancel" style="background-color: #c94e41;" CssClass="form-control btn2" runat="server" />
                                         <asp:Button Text="Back" ID="btnback" OnClick="btnback_Click" style="background-color: #2ecc71;" CssClass="form-control btn2" runat="server" />
                                        
                                  </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
                       </div>
                   
	           </section>
	        </section>
	    </section>
	</article>

    <asp:HiddenField ID="hidPrint_PCID" runat="server" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">

        <ContentTemplate>

            <div id="dialog" style="display: none">
                <asp:GridView ID="gv_outstack" runat="server" CssClass="pshro_GridDgn grdLoad GridStyle GridPager" AutoGenerateColumns="false" OnPageIndexChanging="gv_outstack_PageIndexChanging" OnRowEditing="gv_outstack_RowEditing" OnDataBound="gv_outstack_DataBound" OnRowDataBound="gv_outstack_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="SNo" HeaderText="S.No" />
                        <asp:BoundField DataField="Description" HeaderText="Description" />
                        <asp:BoundField DataField="Stockcount" HeaderText="Stock Details" />
                        <asp:BoundField DataField="CQty" Visible="false" HeaderText="Customer Request" />
                        <asp:TemplateField HeaderText="Customer Request">
                            <ItemTemplate>
                                <%--Visible='<%# Eval("StatusQty") == "failure" ? true:false %>'--%>
                                <asp:Label ID="lblCQty" runat="server" Text='<%# Eval("CQty") %>' Style="display: none;"></asp:Label>
                                <asp:TextBox ID="txtCQty" runat="server" Text='<%# Eval("CQty") %>' onkeyup="return txtCQty_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)"
                                    onchange="return txtCQty_TextChanged(this);" CssClass="grid-textbox" Width="50%" Style="" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete" ItemStyle-CssClass="imglists">
                            <ItemTemplate>
                                <%--Visible='<%# Eval("StatuStock").ToString() == "0.00" ? true : false %>'  --%>
                                <asp:Label ID="lblSONo" runat="server" Text='<%# Eval("SONo") %>' Style="display: none;"></asp:Label>
                                <asp:ImageButton ID="Edit_btn" runat="server" OnClientClick='<%# String.Format("javascript:return setCustID(\"{0},{1}\")", Eval("SONo").ToString(),Eval("InventoryCode").ToString()) %>' ImageUrl="../images/DeleteRed.png" ToolTip="Click here to Edit" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

        </ContentTemplate>

    </asp:UpdatePanel>
</asp:Content>

<%-- --%>