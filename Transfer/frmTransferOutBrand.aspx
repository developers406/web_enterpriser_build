﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTransferOutBrand.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmTransferOutBrand" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

       
        /*.form-control-res[readonly] {
                background-color: lightpink;
            }*/

        #divAddButtons1 {
            position: fixed;
            left: 0;
            width: 100%;
            bottom: 0;
        }

        /*Attribute Count Table */
        #grdAttributeSize {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdAttributeSize td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdAttributeSize tr:nth-child(even) {
                background-color: white;
            }

            #grdAttributeSize tr:hover {
                background-color: white;
            }

            #grdAttributeSize th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: indigo;
                font-weight: bolder;
                color: white;
            }

        /*Item Detials*/
        #grdItemDetails {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
        }

            #grdItemDetails td, #grdItemDetails th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdItemDetails tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #grdItemDetails tr:hover {
                background-color: lavender;
            }

            #grdItemDetails th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: slateblue;
                font-weight: bolder;
                color: white;
                width: 100%;
            }

        #tblStyleCode {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            /*width: 100%;*/
            height: auto;
        }

            #tblStyleCode td, #tblStyleCode th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #tblStyleCode tr:nth-child(even) {
                background-color: #f2f2f2;
                min-width: 20px;
            }

            #tblStyleCode tr:hover {
                background-color: pink;
            }

            #tblStyleCode th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: red;
                font-weight: bolder;
                color: white;
            }

        .divtblStyleCode th {
            padding-top: 4px;
            padding-bottom: 4px;
            text-align: left !important;
            background-color: red;
            font-weight: bolder;
            color: white;
        }

            .divtblStyleCode td:nth-child(2), .divtblStyleCode th:nth-child(2) {
                min-width: 110px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblStyleCode td:nth-child(3), .divtblStyleCode th:nth-child(3) {
                min-width: 110px;
                max-width: 65px;
                text-align: right !important;
            }

            .divtblStyleCode td:nth-child(4), .divtblStyleCode th:nth-child(4) {
                min-width: 110px;
                max-width: 65px;
                text-align: right !important;
            }


        .divtblItemdetail th {
            padding-top: 4px;
            padding-bottom: 4px;
            text-align: left !important;
            background-color: darkslateblue;
            font-weight: bolder;
            color: white;
        }

            .divtblItemdetail td:nth-child(1), .divtblItemdetail th:nth-child(1) {
                min-width: 40px;
                max-width: 65px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(2), .divtblItemdetail th:nth-child(2) {
                min-width: 75px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblItemdetail td:nth-child(3), .divtblItemdetail th:nth-child(3) {
                min-width: 75px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblItemdetail td:nth-child(4), .divtblItemdetail th:nth-child(4) {
                min-width: 340px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblItemdetail td:nth-child(5), .divtblItemdetail th:nth-child(5) {
                min-width: 70px;
                max-width: 65px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(6), .divtblItemdetail th:nth-child(6) {
                min-width: 90px;
                max-width: 65px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(7), .divtblItemdetail th:nth-child(7) {
                min-width: 60px;
                max-width: 65px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(8), .divtblItemdetail th:nth-child(8) {
                min-width: 75px;
                max-width: 65px;
                text-align: right !important;
            }

            .divtblItemdetail td:nth-child(9), .divtblItemdetail th:nth-child(9) {
                min-width: 90px;
                max-width: 65px;
                text-align: center !important;
            }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'TransferOutBrand');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "TransferOutBrand";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">

        var gRowno = 0;

        function pageLoad() {


        }

        $(document).ready(function () {

            fncGetAttributes('StyleCode', 'TEST');
            $('#<%=txtSearch.ClientID%>').focus().select();
        });


        /// delete  Added Row
        function fncGRNRowKeydown(evt, source) {
            var charCode, rowobj, scrollheight;
            try {
                rowobj = $(source);
                charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 46) {
                    $(source).remove();
                    fncUpdateRowNow();

                }
                //else if (charCode == 113) {
                //    fncOpenItemhistory(rowobj.find('td input[id*="txtItemcode"]').val());
                //}
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncUpdateRowNow() {
            var rowNo = 0;
            try {

                $("#grdItemDetails tbody").children().each(function () {
                    rowNo = parseInt(rowNo) + 1;
                    debugger;
                    //$(this).find('td input[id*="txtSNo"]').val(rowNo);
                    $(this).find("td").eq(0).html(rowNo);
                });

                fncShowTotalValues();
                //fncNetValueCalculation();
                //fncSaveGRNEntryTemporarly();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        function fncPriceCalc(evt) {
            try {

                var amountTD = null;
                var Qtytxt = null;
                var Stocktxt = null;
                var Costtxt = null;
                amountTD = $(evt.target).closest("td").next().next().next();
                Costtxt = $(evt.target).closest("td").next().next();
                Stocktxt = $(evt.target).closest("td").prev();
                Qtytxt = $(evt.target);

                if (Qtytxt.val() == '') {
                    Qtytxt.val('0');
                }

                console.log(Qtytxt.val())
                console.log(Stocktxt.text())
                console.log(Costtxt.text())
                console.log(amountTD.text())

                debugger;
                var Qty = parseFloat(Qtytxt.val());
                var Stock = parseFloat(Stocktxt.text());
                var Cost = parseFloat(Costtxt.text());
                var OldQty = parseFloat(amountTD.text()) / parseFloat(Costtxt.text());


                if (Qty == OldQty) {
                    Qtytxt.css('border', '1px solid #ddd');
                    Qtytxt.css('border-width', '1px');
                    Qtytxt.css('border-style', 'inset');
                }

                if (Qty == 0) {
                    //ShowPopupMessageBox('Invalid Transfer Quantity !');
                    fncToastInformation('Invalid Transfer Quantity !');
                    Qtytxt.val(OldQty);
                    return;
                }

                if (Qty > Stock) {
                    //ShowPopupMessageBox('Please Check Stock !');
                    fncToastInformation('Please Check Stock !');
                    Qtytxt.val(OldQty);
                    Qtytxt.focus().select();
                    Qty = OldQty;
                    Qtytxt.css('border-color', 'red');
                }

                var dTotalamount = parseFloat(Costtxt.text()) * Qty;
                amountTD.text(dTotalamount.toFixed(2));
                console.log(amountTD.text());
                fncShowTotalValues();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSetFocustoNextRow(evt, source) {
            try {
                var rowobj = $(source).parent().parent();
                var amountTD = null;
                var Qtytxt = null;
                var Costtxt = null;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                //alert(charCode);
                //GSStock = rowobj.find('td input[id*="txtQty"]').val();

                if (charCode == 13 || charCode == 40) {


                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {

                        NextRowobj.find('td input[id*="txtQty"]').focus();
                        NextRowobj.find('td input[id*="txtQty"]').select();
                    }
                    else {

                        rowobj.siblings().find('td input[id*="txtQty"]').focus();
                        rowobj.siblings().find('td input[id*="txtQty"]').select();
                    }

                }
                else if (charCode == 38) {

                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="txtQty"]').focus();
                        prevrowobj.find('td input[id*="txtQty"]').select();
                    }
                    else {
                        rowobj.siblings().last().find('td input[id*="txtQty"]').focus();
                        rowobj.siblings().last().find('td input[id*="txtQty"]').select();
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        // Get Attributes
        function fncGetAttributes(mode, attributecode) {
            var obj = {};
            try {
                obj.mode = mode;
                obj.attributecode = attributecode;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmCategoryAttributeMapping.aspx/fncGetAttributes") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncBindStyleCode(jQuery.parseJSON(msg.d));
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBindStyleCode(jsonObj) {
            var StyleObj, row, rowNo = 0;
            var StyleBody;
            try {

                StyleObj = jsonObj;
                StyleBody = $("#tblStyleCode tbody");
                StyleBody.children().remove();

                for (var i = 0; i < StyleObj.length; i++) {
                    rowNo = parseInt(rowNo) + 1;
                    row = "<tr onclick='javascript:showRow(this);' id='trStyleCode_" + i + "' tabindex='" + i + "' >"
                                            + "<td id='tdRowNo'  class='hiddencol' >" + rowNo + "</td>"
                                           + "<td id='tdStyleCode' >" + StyleObj[i]["StyleCode"] + "</td>"
                                           + "<td id='tdBalanceqty' align='right' >" + parseFloat(StyleObj[i]["Stock"]).toFixed(2) + "</td>"
                                           + "<td id='tdSellingPrice' align='right'>" + parseFloat(StyleObj[i]["SellingPrice"]).toFixed(2) + "</td>"
                                           + "</tr>";
                    StyleBody.append(row);
                    //StyleBody.children().dblclick(fncStyleRowDoubleClick);
                    StyleBody.children().click(fncSearchRowClick);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncBindInventory(jsonObj) {
            var StyleObj, row, rowNo = 0;
            var StyleBody;
            try {

                StyleObj = jsonObj;
                StyleBody = $("#tblInventory tbody");
                StyleBody.children().remove();
                console.log(jsonObj);
                for (var i = 0; i < StyleObj.length; i++) {
                    rowNo = parseInt(rowNo) + 1;
                    //row = "<tr onclick='javascript:showRow(this);' id='trInventory_" + i + "' tabindex='" + i + "' >"
                    row = "<tr id='trInventory_" + i + "' tabindex='" + i + "' >"
                                            + "<td id='tdSize' >" + StyleObj[i]["ValueName"] + "</td>"
                                           + "<td id='tdCode' >" + StyleObj[i]["InventoryCode"] + "</td>"
                                           + "<td id='tdName' align='right' >" + StyleObj[i]["Description"] + "</td>"
                                           + "<td id='tdStock' align='right'>" + parseFloat(StyleObj[i]["Stock"]).toFixed(2) + "</td>"
                                           + "<td id='tdCost' align='right'> " + parseFloat(StyleObj[i]["BasicCost"]).toFixed(2) + "</td>"
                                           + "</tr>";
                    StyleBody.append(row);
                    //StyleBody.children().dblclick(fncStyleRowDoubleClick);
                    //StyleBody.children().click(fncSearchRowClick);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function showRow(row) {
            // if ($("#grdItemDetails tbody").children().length == 0) {
            var x = row.cells;
            //alert(x[1].innerHTML);
            batchNo = $.trim(x[1].innerHTML);
            // console.log(batchNo);
            $('#<%=hiddenStylecode.ClientID%>').val('');
                LoadDynamicAttribute(batchNo);
                $('#<%=hiddenStylecode.ClientID%>').val(batchNo);
            //  }
            //  else {
            //  ShowPopupMessageBox("Already One Style Code is Added");
            //  }
            }

            function fncStyleRowDoubleClick() {
                try {

                    var obj, batchNo;
                    batchNo = $.trim($(this).find('td[id*=tdStyleCode]').text().trim());
                    //console.log(batchNo); 
                    LoadDynamicAttribute("TEST");
                    //fncGetInventoryDetailForTransferOnEnter(batchNo);
                    event.preventDefault();
                    event.stopPropagation();

                    return false;
                }
                catch (err) {
                    fncToastError(err.message);
                }
            }

            /// Show Save Dailog 
            function fncShowSaveDailog(message) {
                $(function () {
                    $("#Save-dialog").html(message);
                    $("#Save-dialog").dialog({
                        title: "Enterpriser Web",
                        closeOnEscape: false,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("destroy");
                                window.location = "frmTransferOut.aspx";
                            }
                        },
                        modal: true
                    });
                });
            };
            /// to show back ground color on click
            function fncSearchRowClick() {
                try {

                    $(this).css("background-color", "pink");
                    $(this).siblings().css("background-color", "transparent");
                    $("tr:even").css("background-color", "#f2f2f2");

                    $(this).css("background-color", "#f2f2f2").filter(function (index) {
                        return $(this).css("background-color") === "rgb(242, 242, 242)";
                    }).css("background-color", "pink");

                    //console.log($(this).css("background-color"));               
                }
                catch (err) {
                    fncToastError(err.message);
                }
            }

            function fncShowTotalValues() {
                try {
                    var totalQuantity = 0;
                    var totalAmount = 0;
                    var objAmount = null;
                    var objQuantity = null;

                    $('#<%=txtTotalItems.ClientID%>').val($("#grdItemDetails tbody").children().length);

                $("#grdItemDetails tr").each(function () {
                    if (!this.rowIndex) return;
                    objQuantity = $(this).find('td input[id*="txtQty"]');
                    objAmount = $(this).find('.Total');

                    if (objQuantity.val() != '') {
                        totalQuantity = totalQuantity + parseFloat(objQuantity.val());
                    }

                    if (objAmount.text() != '') {
                        totalAmount = totalAmount + parseFloat(objAmount.text());
                    }

                });
                //alert(totalAmount);
                $("[id*=txtTotalQty]").val(totalQuantity);
                $("[id*=txtTotalCost]").val(totalAmount.toFixed(2));

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncDisableStockRows() {
            try {
                // Disable Odd Rows in Table
                $("#grdAttributeSize tr:odd").each(function () {
                    var cells = $("td", this);
                    var Sleeve = "";
                    if (cells.length > 0) {
                        for (var j = 0; j < cells.length; ++j) {

                            if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {

                                $("input:text", cells.eq(j)).attr('readonly', 'true');
                                $("input:text", cells.eq(j)).css('background-color', 'lavender');
                                $("input:text", cells.eq(j)).css('font-weight', 'bold');

                            }
                        }
                    }
                });

                return false;

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncClear() {

            $("#grdAttributeSize tr:even").each(function () {
                var cells = $("td", this);
                var Sleeve = "";
                if (cells.length > 0) {
                    for (var j = 0; j < cells.length; ++j) {

                        if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {

                            if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {
                                $("input:text", cells.eq(j)).val('0');
                            }
                        }
                    }
                }
            });

            return false;
        }



        function LoadDynamicAttribute(ProductCode) {
            var obj = {};
            obj.Code = ProductCode;

            $.ajax({
                type: "POST",
                url: "frmTransferOutBrand.aspx/GetAttributes",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var objvendor = jQuery.parseJSON(msg.d);
                    console.log(objvendor);
                    //console.log(Object.keys(objvendor).length);
                    numofTable = Object.keys(objvendor).length;
                    //debugger;

                    $('#grdAttributeSize').find("tr").remove();
                    if (numofTable > 0) {

                        if (objvendor.Table.length > 0) {

                            funBindSizeGrid(objvendor.Table);
                            fncBindInventory(objvendor.Table1);
                            $("[id*=divAddButtons]").show();
                            $("[id*=divScanBarcode]").show();

                            fncDisableStockRows();

                            return false;
                        }
                        else {

                            console.log('No mapping');
                            $("[id*=divAddButtons]").show();
                            $("[id*=divScanBarcode]").show();
                            return false;
                        }
                    }

                    return false;

                },
                error: function (data) {
                    ShowPopupMessageBox(data.message);
                    return false;
                }
            });
        }


        function fncAddTransDetail() {
            try {

                var objSearchData, tblSearchData, tblSearchDataTemp, Rowno = 0, batchNo = "";

                //tblSearchData = $("#grdItemDetails");
                tblSearchData = $("#TempgrdItemDetails tbody");

                var itemName = "";
                var CheckSize = true;
                var iTranQty = 0;
                var sSize = "";
                var sItemCode = "";
                var sDescription = "";
                var sCost = "";
                var sStock = "";
                var iTotalCost = 0;
                var StyleCodeCheck = false;
                var QuantityCheck = false;
                var iTxtboxQty = 0;

          <%--      $("#grdItemDetails tr").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        for (var j = 0; j < cells.length; ++j) {
                            if ($(this).parents('table:first').find('th').eq(j).text().trim() == 'StyleCode') {
                                debugger;
                                if (cells.eq(1).text().trim() == $('#<%=hiddenStylecode.ClientID %>').val()) {                                    
                                    StyleCodeCheck = true;
                                    return;
                                }
                            }
                        }
                    }
                });--%>


                $("#grdAttributeSize tr:even").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        for (var j = 0; j < cells.length; ++j) {

                            if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
                                if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {
                                    QuantityCheck = true;
                                    return;
                                }
                                else {
                                    var isnum = /^\d+$/.test($("input:text", cells.eq(j)).val());
                                    if (isnum) {
                                        iTxtboxQty = iTxtboxQty + 1;
                                    }
                                }
                            }
                        }
                    }

                    iTxtboxQty = cells.length - iTxtboxQty;
                    $("input:text", cells.eq(iTxtboxQty)).select().focus();
                });

                console.log(iTranQty);

                if (QuantityCheck == false) {
                    fncToastInformation('Please Enter Transfer-Out Quantity !');
                    return;
                }

                if (StyleCodeCheck == true) {
                    fncToastInformation('StyleCode Already Added !');
                    fncClear();
                }

                if (StyleCodeCheck == false && QuantityCheck == true) {

                    var tablerow = $("#grdAttributeSize tr").length - 1;
                    tablerow = tablerow / 2;

                    //for (var p = 1; p <= tablerow; ++p) {
                    // $("#grdAttributeSize tr:nth-last-child("+ p +")").each(function () {
                    $("#grdAttributeSize tr:even").each(function () {

                        var cells = $("td", this);
                        var Sleeve = "";
                        if (cells.length > 0) {
                            for (var j = 0; j < cells.length; ++j) {
                                if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() == 'SLEEVE') {
                                    Sleeve = cells.eq(j).text();
                                }
                                if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
                                    if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {
                                        var Header = $(this).parents('table:first').find('th').eq(j).text().toUpperCase();
                                        Header = Header.replace('(', '').replace(')', '');
                                        console.log(Header + '--' + $("input:text", cells.eq(j)).val());
                                        iTranQty = $("input:text", cells.eq(j)).val();

                                        $("#tblInventory tr").each(function () {
                                            if (!this.rowIndex) return;
                                            debugger;
                                            sSize = $(this).find("td").eq(0).html();
                                            sItemCode = $(this).find("td").eq(1).html();
                                            sDescription = $(this).find("td").eq(2).html();
                                            sStock = $(this).find("td").eq(3).html();
                                            sCost = $(this).find("td").eq(4).html();

                                            isSleeve = sDescription.includes(Sleeve);
                                            gRowno = $("#grdItemDetails tbody").children().length + 1;

                                            if (isSleeve) {
                                                if (Header.toUpperCase() == sSize.toUpperCase()) {
                                                    console.log(sDescription);
                                                    //gRowno = parseFloat(gRowno) + parseFloat(1);                                                  
                                                    iTotalCost = iTranQty * parseFloat(sCost);
                                                    row = "<tr id='trTOItem_" + gRowno + "' onkeydown='return fncGRNRowKeydown(event,this);' tabindex='" + gRowno + "' >" +
                                                            '<td align="center">' + gRowno + '</td>' +
                                                            '<td>' + $('#<%=hiddenStylecode.ClientID %>').val() + '</td>' +
                                                            '<td>' + sItemCode + '</td>' +
                                                            '<td align="Left">' + sDescription + '</td>' +
                                                            '<td>' + sStock + '</td>' +
                                                            '<td align="center">' + iTranQty + '</td>' +
                                                            '<td align="right">' + sCost + '</td>' +
                                                            '<td align="right" class="Total">' + iTotalCost.toFixed(2) + '</td>' +
                                                            '</tr>';
                                                    tblSearchData.append(row);


                                                    console.log('Row' + gRowno);
                                                }
                                                else if (Header.toUpperCase() == 'Quantity') {
                                                    console.log(sDescription);
                                                    //gRowno = parseFloat(gRowno) + parseFloat(1);
                                                    row = "<tr id='trTOItem_" + gRowno + "' onkeydown='return fncGRNRowKeydown(event,this);' tabindex='" + gRowno + "' >" +
                                                            '<td align="center">' + gRowno + '</td>' +
                                                            '<td>' + $('#<%=hiddenStylecode.ClientID %>').val() + '</td>' +
                                                            '<td>' + sItemCode + '</td>' +
                                                            '<td align="Left">' + sDescription + '</td>' +
                                                            '<td>' + sStock + '</td>' +
                                                            '<td align="center">' + iTranQty + '</td>' +
                                                            '<td align="right">' + sCost + '</td>' +
                                                            '<td align="right" class="Total">' + iTotalCost.toFixed(2) + '</td>' +
                                                            '</tr>';
                                                    tblSearchData.append(row);
                                                    // gRowno = $("#grdItemDetails tbody").children().length;
                                                }

                                            $("[id*=divTransferBtn]").show();
                                        }

                                            //break;
                                        });
                                }
                            }
                        }
                    }
                    });
                    //} 
                    //debugger;
                fncUpdateTransDetailTable();
                fncClear();
                fncShowTotalValues();
                $('#<%=txtBarcodeScan.ClientID%>').focus().select();

            }

        }
        catch (err) {
            fncToastError(err.message);
        }
    }


    function fncUpdateTransDetailTable() {
        try {

            debugger;
            var tblSearchData = $("#grdItemDetails");
            var sItemCode = '';
            var bCheckItemCode = false;

            $("#TempgrdItemDetails tr").each(function () {
                if (!this.rowIndex) return;
                debugger;

                sTempStyleCode = $(this).find("td").eq(1).html().trim();
                sTempItemCode = $(this).find("td").eq(2).html();
                sTempDescription = $(this).find("td").eq(3).html();
                sTempStock = $(this).find("td").eq(4).html();
                iTranQty = $(this).find("td").eq(5).html();
                sTempCost = $(this).find("td").eq(6).html();

                gRowno = $("#grdItemDetails tbody").children().length + 1;

                $("#grdItemDetails tr").each(function () {
                    if (!this.rowIndex) return;
                    debugger;
                    sItemCode = $(this).find("td").eq(2).html();
                    sStyleCode = $(this).find("td").eq(1).html().trim();
                    if (sItemCode == sTempItemCode && sTempStyleCode == sStyleCode) {
                        $(this).find('td').eq(5).find('input').val(iTranQty);
                        $(this).find('td').eq(5).find('input').focus().select();
                        $(this).remove();
                        fncUpdateRowNow();
                        bCheckItemCode = true;
                    }
                });

                gRowno = $("#grdItemDetails tbody").children().length + 1;
                //if (bCheckItemCode == false) {
                iTotalCost = iTranQty * parseFloat(sTempCost);
                row = "<tr id='trTOOItem_" + gRowno + "' onkeydown='return fncGRNRowKeydown(event,this);' tabindex='" + gRowno + "' >" +
                            '<td align="center">' + gRowno + '</td>' +
                            '<td>' + $('#<%=hiddenStylecode.ClientID %>').val() + '</td>' +
                                    '<td>' + sTempItemCode + '</td>' +
                                    '<td align="Left">' + sTempDescription + '</td>' +
                                    '<td>' + sTempStock + '</td>' +
                                    '<td align="center"> <input type="text" id="txtQty"  class="inputs" onfocus="this.select();"  onkeydown="return fncSetFocustoNextRow(event,this);" oninput="return fncPriceCalc(event)"  onkeypress= "return isNumberKeyGA(event);" style="width:100%; text-align:center;" value= "0" > </td>' +
                                    '<td>' + iTranQty + '</td>' +
                                    '<td align="right">' + sTempCost + '</td>' +
                                    '<td align="right" class="Total">' + iTotalCost.toFixed(2) + '</td>' +
                                    '</tr>';

                tblSearchData.append(row);
                console.log('Row' + gRowno);
                // }

            });

            $('#TempgrdItemDetails').find("tr:gt(0)").remove();
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncScanBarcode(evt) {
        try {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            InventoryCode = $('#<%=txtBarcodeScan.ClientID %>').val();
            var bCheckItemCode = false;
            if (charCode == 13) {
                var iTranQty = 0;
                var iManualQty = 0;
                var iCost = 0;
                var iTotalCost = 0;
                var sItemCode = "";
                var sBatchNo = "";

                $("#grdItemDetails tr").each(function () {
                    if (!this.rowIndex) return;
                    debugger;
                    sItemCode = $(this).find("td").eq(2).html();
                    sBatchNo = $(this).find("td").eq(1).html();
                    sBatchNo = sItemCode.trim() + sBatchNo.trim();

                    iManualQty = parseInt($(this).find("td").eq(6).html());
                    iCost = parseFloat($(this).find("td").eq(7).html());

                    if (sBatchNo == InventoryCode.trim()) {
                        iTranQty = parseInt($(this).find('td').eq(5).find('input').val()) + 1;
                        $(this).find('td').eq(5).find('input').val(iTranQty);
                        iTotalCost = iTranQty * iCost;
                        $(this).find("td").eq(8).html(iTotalCost.toFixed(2));
                        //$(this).find('td').eq(5).find('input').focus().select(); 
                        //$(this).find('td').eq(5).find('input').css('border', '2px solid red');
                        debugger;
                        if (iTranQty > iManualQty)
                            $(this).find('td').eq(5).find('input').css('border', '2px solid red');
                        else if (iManualQty == iTranQty)
                            $(this).find('td').eq(5).find('input').css('border', '2px solid green');

                        fncUpdateTransDetailTable();
                        fncClear();
                        fncShowTotalValues();
                        bCheckItemCode = true;
                        $('#<%=txtBarcodeScan.ClientID %>').val('');
                        $('#<%=txtBarcodeScan.ClientID %>').focus().select();

                      }
                    if (parseFloat($.trim($(this).find('td').eq(5).find('input').val())) > parseFloat($.trim($(this).find("td").eq(4).html()))) {
                        fncToastInformation('Please Check Stock !');
                        $(this).find('td').eq(5).find('input').val(iManualQty);
                        $(this).find('td').eq(5).find('input').css('border-color', 'red');
                        return false;
                    }
                });

                  if (bCheckItemCode == false) {
                      fncToastInformation('Invalid Barcode');
                      $('#<%=txtBarcodeScan.ClientID %>').focus().select();
                }
            }

            return true;
        }
        catch (err) {
            fncToastError(err.message);
        }
    }



    function ValidateForm() {

        var rows = 0;
        var iTranQty = 0;
        rows = $('#grdItemDetails tbody tr').length;
        var bQuantitycheck = false;
        $('#<%=lnkSave.ClientID %>').css("display", "none");
        $("#grdItemDetails tr").each(function () {
            if (!this.rowIndex) return;
            debugger;
            iTranQty = parseInt($(this).find('td').eq(5).find('input').val());

            if (iTranQty > 0)
                bQuantitycheck = true;

            $(this).find('td').eq(5).find('input').focus().select();
        });

        if (bQuantitycheck == false) {
            //popUpObjectForSetFocusandOpen = $('#<%=txtSearch.ClientID %>');
            //ShowPopupMessageBox('Enter Transfer Quantity to Proceed Transfer!');
            fncToastInformation('Enter Transfer Quantity to Proceed Transfer!');
            $('#<%=lnkSave.ClientID %>').css("display", "block");
            return false;
        }
        else if (rows <= 0) {
            popUpObjectForSetFocusandOpen = $('#<%=txtSearch.ClientID %>');
            ShowPopupMessageBoxandOpentoObject('No items to Proceed Transfer!');
            $('#<%=lnkSave.ClientID %>').css("display", "block");
            return false;
        }
        else if ($('#<%=DropDownToLocation.ClientID %>').find('option:selected').text() == "-- Select --") {
            fncToastInformation('<%=Resources.LabelCaption.Alert_To_Location%>');
            fncOpenDropdownlist($('#<%=DropDownToLocation.ClientID %>'));
            $('#<%=lnkSave.ClientID %>').css("display", "block");
            return false;
        }
        else {

            XmlGridValue($('#grdItemDetails tr'), $('#<%=hiddenXml.ClientID %>'));
            return true;
        }
}


function XmlGridValue(tableid, XmlID) {
    try {

        var xml = '<NewDataSet>';

        // $('#grdAttribute tr').each(function () {
        tableid.each(function () {
            var cells = $("td", this);
            if (cells.length > 0) {

                xml += "<Table>";
                for (var j = 0; j < cells.length; ++j) {

                    if ($(this).parents('table:first').find('th').eq(j).text().trim() == 'ScanQty')
                        xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + $("input:text", cells.eq(j)).val() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';
                    else
                        xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';

                }
                xml += "</Table>";
            }
        });

        xml = xml + '</NewDataSet>'
        console.log(xml);
        //alert(xml);

        XmlID.val(escape(xml));
        //alert(XmlID.val());
        //$("#hiddenXml").val($("[id*=txtPoNumber]").val());
    }
    catch (err) {
        return false;
        fncToastError(err.message);
    }
}

function fncCheckStock(evt) {
    //alert($(evt.target).val());
    try {
        //debugger;
        var Qtytxt = null;
        var iStock = 0;

        Qtytxt = $(evt.target);
        var Header = Qtytxt.attr("id");
        Header = Header.replace('(', '').replace(')', '');
        console.log(Header);
        var iTranqty = parseInt(Qtytxt.val());

        $("#tblInventory tr").each(function () {
            var cells = $("td", this);
            if (cells.length > 0) {
                for (var j = 0; j < cells.length; ++j) {
                    if ($(this).parents('table:first').find('th').eq(j).text().trim() == 'Size') {
                        if (cells.eq(j).text().trim() == Header) {
                            iStock = parseInt(cells.eq(3).text());
                            if (iTranqty > iStock) {
                                fncToastInformation('Please Check Stock !');
                                Qtytxt.val(0);
                                Qtytxt.css('border', '1px solid red');
                                Qtytxt.focus().select();
                            }
                            else {
                                Qtytxt.css('border', '1px solid #ddd');
                                Qtytxt.css('border-width', '1px');
                                Qtytxt.css('border-style', 'inset');
                                //Qtytxt.focus().select();
                            }
                        }
                    }
                }
            }
        });


        //var checkcode = false;
        //$("#grdItemDetails tr").each(function () {
        //    var cells = $("td", this);
        //    if (cells.length > 0) {
        //        for (var j = 0; j < cells.length; ++j) {
        //            debugger;
        //            if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() == 'DESCRIPTION') {
        //                if (cells.eq(j).text().trim().includes('-' + Header.trim())) {
        //                    //$("input:text", cells.eq(4)).select().focus();       
        //                    checkcode = true;
        //                }
        //            }
        //            if (checkcode) {
        //                if ($(this).parents('table:first').find('th').eq(j).text().trim() == 'Qty') {
        //                    $("input:text", cells.eq(j)).val(Qtytxt.val());
        //                    $("input:text", cells.eq(j)).select().focus();
        //                }
        //            }
        //        }
        //    }
        //});

    }
    catch (err) {
        fncToastError(err.message);
    }
}


function isNumberKeyGA(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
        return false;

    if (charCode == 46 && $(evt.target).val().indexOf('.') != -1) {
        evt.preventDefault();
    } // prevent if 

    if (charCode == 13) {
        if ($(evt.target).closest("td").next().length == 0) {
            // is last  
            Qtytxt = $(evt.target).closest("tr").children('td:first-child').find("input");
            Qtytxt.focus().select();
        }

        Qtytxt = $(evt.target).closest("td").next().find("input");
        Qtytxt.focus().select();
    }


    return true;
}



function getSortedKeys(obj) {
    var keys = []; for (var key in obj) keys.push(key);
    return keys.sort(function (a, b) { return obj[b] - obj[a] });
}

var sortByProperty = function (property) {

    return function (x, y) {

        return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));

    };

};

/// Bind Size Grid
function funBindSizeGrid(jsonObj) {

    try {

        if (jsonObj == null)
            return;

        debugger;
        // Call Sort By Name
        //jsonObj = jsonObj.sort();
        jsonObj = jsonObj.sort(sortByProperty('SLEEVE'));
        ////Creating table rows 
        var table1 = document.getElementById("grdAttributeSize");
        //$('#grdAttributeSize').find("tr").remove();
        var BodyElement = document.createElement("tbody");

        if (jsonObj.length > 0) {
            var keys = Object.keys(jsonObj[0]);
            //keys = keys.filter(e => e !== 'SLEEVE');
            //keys.splice(0, 0, "SLEEVE");
            //console.log(arr.join());
            var rowth = document.createElement("thead");
            var rowHeader = document.createElement("tr");
            var cellHeader = null;
            for (var i = 0; i < keys.length; i++) {
                cellHeader = document.createElement("th");
                cellHeader.appendChild(document.createTextNode(keys[i]));
                rowHeader.appendChild(cellHeader);
            }
            rowth.appendChild(rowHeader);
            table1.appendChild(rowth);
        }


        //Creating table rows
        for (var j = 0; j < jsonObj.length; j++) {
            var tableRowElement = document.createElement("tr");

            var tableRowId = "grdAttributeSize_row" + j;
            tableRowElement.setAttribute("id", tableRowId);
            //tableElement.setAttribute("class", tableclass);

            console.log(jsonObj[j]);
            for (var i = 0; i < keys.length; i++) {
                var tableCellElement = document.createElement("td");
                var key = Object.keys(jsonObj[j])[i];
                var cellText;
                var value = jsonObj[j][key];
                console.log(key);
                console.log(value);

                //cellText = document.createTextNode(value);
                //tableCellElement.contentEditable = "true";
                //cellText.innerHTML = "<input type='text' class='Margin'onkeypress='return isNumberKeyGA(event);' style='width:100%; text-align:center;'/>";
                if (key.toUpperCase() != "SLEEVE") {
                    var el = document.createElement('input');
                    el.type = 'text';
                    el.name = key;
                    el.id = key;
                    el.setAttribute("style", "width:100%; text-align:center;");
                    el.setAttribute("MaxLength", "3");
                    el.value = value;
                    //el.setAttribute("onkeypress", "return isNumberKeyGA(this);");
                    el.onkeypress = function (event) { return isNumberKeyGA(event) }
                    el.oninput = function (event) { return fncCheckStock(event) }
                    el.onfocus = function (event) { event.target.select() }

                    tableCellElement.appendChild(el);
                }
                else {
                    cellText = document.createTextNode(value);
                    tableCellElement.appendChild(cellText);
                }

                tableRowElement.appendChild(tableCellElement);
            }
            BodyElement.appendChild(tableRowElement);
        }

        table1.appendChild(BodyElement);

    }
    catch (err) {
        fncToastError(err.message);
    }
}



function showpreview(input) {
          <%--  if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgpreview.ClientID %>').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                }--%>
}

        function fncShowItemEditDailog(Gidno) {
            $(function () {

                $("#ItemEdit").html("GRN Created Successfully, " + "<H1><B>Style Code:" + Gidno + "</B></H1>");
                $("#ItemEdit").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        "Ok": function () {
                            $(this).dialog("destroy");
                            window.location.href = "frmGoodsAcknowledgement.aspx";
                        }
                    },
                    modal: true
                });
            });
        };

        function Search_Gridview(strKey) {

            // Declare variables 
            var table, tr, td, i;
            table = document.getElementById("tblStyleCode");
            tr = table.getElementsByTagName("tr");
            var strData = strKey.value.toLowerCase().split(" ");
            var rowData;
            for (i = 0; i < tr.length; i++) {
                rowData = tr[i].innerHTML;
                var styleDisplay = 'none';
                for (var j = 0; j < strData.length; j++) {
                    if (rowData.toLowerCase().indexOf(strData[j]) >= 0) {
                        styleDisplay = 'table-row';
                    }
                    else {
                        styleDisplay = 'none';
                        break;
                    }
                }

                tr[i].style.display = styleDisplay;
                table.style.height = 'auto';
            }

        }


        function fncClearStyleCode() {
            if ($("#grdItemDetails tbody").children().length > 0)
                $("#grdItemDetails tbody").empty();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="breadcrumbs" class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a style="text-decoration: none;">Transfer</a><i class="fa fa-angle-right"></i></li>
            <li class="active-page">Transfer Out Entry</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
        </ul>
    </div>

    <div class="container-group-price">

        <div class="container-left-grn" id="pnlFilter" runat="server" style="width: auto;">
            <div class="container-group-small" style="margin-top: 5px; width: 350px;">
                <div class="container-control" style="background: white;">

                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label16" runat="server" Text="Style Search"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtSearch" runat="server" onkeyup="Search_Gridview(this)" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>

                    <div class="control-group-single-res" style="background: white;">
                        <table class="divtblStyleCode" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col" class="hiddencol">S.No
                                    </th>
                                    <th scope="col">StyleCode
                                    </th>
                                    <th scope="col">Stock
                                    </th>
                                    <th scope="col">SellingPrice
                                    </th>
                                </tr>
                            </thead>

                        </table>

                        <div style="overflow: auto; height: 280px;">
                            <table id="tblStyleCode" class="divtblStyleCode" cellspacing="0" rules="all" border="1">
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                        <table id="tblInventory" cellspacing="0" rules="all" border="1" style="display: none;">
                            <thead>
                                <tr>
                                    <th scope="col">Size
                                    </th>
                                    <th scope="col">InventoryCode
                                    </th>
                                    <th scope="col">Description
                                    </th>
                                    <th scope="col">Stock
                                    </th>
                                    <th scope="col">Cost
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label17" Font-Bold="true" runat="server" Text="From Location"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:DropDownList ID="DropDownFromLocation" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label18" Font-Bold="true" runat="server" Text="To Location"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:DropDownList ID="DropDownToLocation" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="control-group-single-res">
                        <%--<asp:Image ID="imgpreview" runat="server" ImageAlign="AbsMiddle" Height="50%" Width="100%" ImageUrl="~/images/No-image-available.jpg" />--%>
                        <%--<asp:FileUpload ID="fuimage" runat="server" onchange="showpreview(this);" />--%>
                        <%-- <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                            <Triggers>
                                <asp:PostBackTrigger ControlID="lnkAdd" />
                            </Triggers>
                            <ContentTemplate>
                                <asp:FileUpload ID="fuimage" runat="server" onchange="showpreview(this);" />
                            </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>

                    <div class="button-contol" id="divTransferBtn" style="display: none;">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <%-- <div class="control-button">
                                    <asp:LinkButton ID="lnkRefresh" runat="server" class="button-blue" OnClick="lnkRefresh_Click"><i class="icon-play"></i>Refresh</asp:LinkButton>
                                </div>--%>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return ValidateForm();" OnClick="lnkSave_Click"><i class="icon-play"></i>Transfer Out</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                </div>
            </div>
        </div>

        <div class="container-right-grn" style="width: 70%; margin-left: 0.5%; height: 225px; padding: 5px;" id="HideFilter_ContainerRight" runat="server">

            <div class="control-group-single-res" id="dynamicInput1">
                <table id="grdAttributeSize" cellspacing="0" rules="all" border="1">
                </table>
            </div>

            <div class="bottom-purchase-container-add" id="divAddButtons" style="padding: 2px 10px; display: none;">
                <div style="margin-top: 10px; font-weight: bold; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif">
                    <div class="col-md-2">
                        <asp:Label ID="Label1" runat="server" Text="Total Items :" Style="float: right;"></asp:Label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtTotalItems" Text="0" runat="server" Style="text-align: center;" BackColor="Snow" ReadOnly="true" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-2" style="margin-left: 10px;">
                        <asp:Label ID="Label3" runat="server" Text="Total Qty :" Style="float: right;"></asp:Label>
                    </div>
                    <div class="col-md-1" style="margin-left: 10px;">
                        <asp:TextBox ID="txtTotalQty" Text="0" runat="server" Style="text-align: center;" BackColor="Snow" ReadOnly="true" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-2" style="margin-left: 10px;">
                        <asp:Label ID="Label5" runat="server" Text="Total Cost :" Style="float: right;"></asp:Label>
                    </div>
                    <div class="col-md-1" style="margin-left: 6px;">
                        <asp:TextBox ID="txtTotalCost" Text="0.00" runat="server" Style="text-align: center;" BackColor="Snow" ReadOnly="true" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>

                <div class="button-contol" style="margin-top: -30px; margin-bottom: -2px; display: block;">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-red" OnClientClick="fncAddTransDetail(); return false;"><i class="icon-play"></i>Add</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="return fncClear();" class="button-red"><i class="icon-play"></i>Clear</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="bottom-purchase-container-add" id="divScanBarcode" style="display: none; width: auto; background-color: darkseagreen; margin-top: 10px;">
                <div style="font-weight: bold; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif">
                    <div style="width: auto;">
                        <asp:Label ID="Label2" runat="server" Text="Scan Barcode :" Style="float: left;"></asp:Label>
                    </div>
                    <div style="width: auto;">
                        <asp:TextBox ID="txtBarcodeScan" onkeydown="return fncScanBarcode(event);" runat="server" Style="float: left;" BackColor="Snow" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-right-grn" style="width: 70%; margin-left: 0.5%; height: auto; padding: 5px;" id="divTransDetail" runat="server">

            <table class="divtblItemdetail" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th scope="col">S.No</th>
                        <th scope="col">StyleCode</th>
                        <th scope="col">ItemCode</th>
                        <th scope="col">Description</th>
                        <th scope="col">Stock</th>
                        <th scope="col">ScanQty</th>
                        <th scope="col">Qty</th>
                        <th scope="col">Cost</th>
                        <th scope="col">Total</th>
                    </tr>
                </thead>
            </table>

            <div style="overflow: auto; height: 150px;">
                <table id="grdItemDetails" class="divtblItemdetail" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr class="hiddencol">
                            <th scope="col">S.No</th>
                            <th scope="col">StyleCode</th>
                            <th scope="col">ItemCode</th>
                            <th scope="col">Description</th>
                            <th scope="col">Stock</th>
                            <th scope="col">ScanQty</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Cost</th>
                            <th scope="col">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <div class="col-md-11">
                </div>
                <div class="col-md-1">
                    <div class="control-button">
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="fncClearStyleCode();return false;"
                            class="button-red"><i class="icon-play"></i>Clear</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="button-contol" style="display: none; margin-top: 10px;">
                <div class="control-button">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-red" OnClientClick="ValidateForm();" OnClick="lnkSave_Click"><i class="icon-play"></i>Save</asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="LinkButton4" runat="server" OnClientClick="return fncClear(); return false;" class="button-red"><i class="icon-play"></i>Clear</asp:LinkButton>
                </div>
                <table id="TempgrdItemDetails" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">StyleCode</th>
                            <th scope="col">ItemCode</th>
                            <th scope="col">Description</th>
                            <th scope="col">Stock</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Cost</th>
                            <th scope="col">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>

        <div id="Save-dialog" style="display: none">
        </div>

        <asp:HiddenField ID="hiddenXml" runat="server" Value="" />
        <asp:HiddenField ID="hiddenAttributes" runat="server" Value="" />
        <asp:HiddenField ID="hiddenStylecode" runat="server" Value="" />

    </div>

</asp:Content>
