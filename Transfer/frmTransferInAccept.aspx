﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTransferInAccept.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmTransferInAccept" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold;
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .transfer td:nth-child(1), .transfer th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(2), .transfer th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(3), .transfer th:nth-child(3) {
            min-width: 587px;
            max-width: 587px;
        }

        .transfer td:nth-child(4), .transfer th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(4) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(5), .transfer th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(5) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(6), .transfer th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(7), .transfer th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(7) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(8), .transfer th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(8) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(9), .transfer th:nth-child(9) {
            display: none;
        }

        .transfer td:nth-child(10), .transfer th:nth-child(10) {
            display: none;
        }

        .transfer td:nth-child(11), .transfer th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(11) {
            text-align: right !important;
            padding-right: 9px;
        }
    </style>
    <script type="text/javascript">

        var glAcceptQty = 0, glTotalQty = 0;
        function pageLoad() {
            try {
                if ($('#<%=hidReason.ClientID %>').val() == "Y") {
                    $('#divStockRequest').css('display', 'block');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetValue() {
            if (SearchTableName == "StockRequet") {
                $('#<%=txtStockRequest.ClientID %>').val(Code);
                  $('#<%=lnkAccept.ClientID %>').focus();
            }
        }

        function fncAddvalidation() {
            if ($("#<%=hidReason.ClientID %>").val() == "Y" && $("#<%=txtStockRequest.ClientID %>").val() == "") {
                  fncToastInformation("Please Enter Stock Request No.");
                  $("#<%=txtStockRequest.ClientID %>").focus();
                return false;
            }
            else {
                return true;
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        //Show Popup After Save
        function fncShowSuccessMessage() {
            try {
                InitializeDialog();
                $("#TransferInAccept").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#TransferInAccept").dialog('close');


            }
            catch (err) {
                alert(err.message);
            }
        }

        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#TransferInAccept").dialog({
                    title: "EnterpriserWeb",
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        //Focus Set to Next Row
        function fncSetFocustoNextRow(evt, source) {
            try {
                var rowobj = $(source).parent().parent();
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 40) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        setTimeout(function () {
                            NextRowobj.find('td input[id*="txtAcceptQty"]').focus();
                        }, 50);
                    }
                    else {
                        setTimeout(function () {
                            rowobj.siblings().find('td input[id*="txtAcceptQty_0"]').focus();
                        }, 50);
                    }
                    return false;
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        setTimeout(function () {
                            prevrowobj.find('td input[id*="txtAcceptQty"]').focus();
                        }, 50);
                    }
                    else {
                        setTimeout(function () {
                            rowobj.siblings().last().find('td input[id*="txtAcceptQty"]').focus();
                        }, 50);
                    }
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Add Accept Qty
        function fncAddAcceptQty(source) {

            var rowObj, totalAcceptQty = 0, acceptQty = 0, transferQty = 0;
            try {
                rowObj = $(source).closest("tr");
                transferQty = $("td", rowObj).eq(9).html().replace(/&nbsp;/g, '');

                rowObj = $(source).parent().parent();

                acceptQty = rowObj.find('td input[id*="txtAcceptQty"]').val();

                if (parseFloat(acceptQty) > parseFloat(transferQty)) {
                    rowObj.find('td input[id*="txtAcceptQty"]').val(transferQty);
                    acceptQty = rowObj.find('td input[id*="txtAcceptQty"]').val();
                    fncToastInformation("Accept Qty Must <= Transfer Qty. Now Altered to be equal");
                }

                fncCalAcceptQty();

              <%--  glTotalQty = parseFloat(glTotalQty) - parseFloat(glAcceptQty);
                $('#<%=txtAcceptQty.ClientID %>').val((parseFloat(glTotalQty) + parseFloat(acceptQty)).toFixed(2));--%>
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncCalAcceptQty() {
            var acceptQty = 0;
            try {
                $("#ContentPlaceHolder1_TrasnferAcceptGrid tbody").children().each(function () {
                    acceptQty = parseFloat(acceptQty) + parseFloat($(this).find('td input[id*="txtAcceptQty"]').val());
                });
                $('#<%=txtAcceptQty.ClientID %>').val(acceptQty.toFixed(2));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //// Get Transfer Qty
        function fncGetTransferQty(source) {
            var rowObj;
            try {
                setTimeout(function () {
                    rowObj = $(source).parent().parent();
                    glAcceptQty = rowObj.find('td input[id*="txtAcceptQty"]').val();
                    glTotalQty = $('#<%=txtAcceptQty.ClientID %>').val();
                    rowObj.find('td input[id*="txtAcceptQty"]').select();
                }, 50);



            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function disableFunctionKeys(e) {
            try {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 115) {
                        if (saveStatus == false) {
                            saveStatus = true;
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkAccept', '')
                            e.preventDefault();
                            return false;
                        }
                    }
                    else if (e.keyCode == 119) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkCancel', '');
                        e.preventDefault();
                        return false;
                    }
                    e.preventDefault();
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        var saveStatus = false;
        function fncAccept() {
            try {
                if (saveStatus == false) {
                    saveStatus = true;
                    $('#<%=btnSave.ClientID%>').click();
        }
    }
    catch (ex) {
        fncToastError(ex.message);
    }
}


        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }


        function InitializeDialogConfirmUpdate() {
            try {
                $("#ConfirmSaveDialogupdate").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

/// Show Save Dailog 
function fncShowSaveDailog(message) {
    $(function () {
        $("#Save-dialog").html(message);
        $("#Save-dialog").dialog({
            title: "Enterpriser Web",
            closeOnEscape: false,
            buttons: {
                Ok: function () {
                    $(this).dialog("destroy");
                    window.location = "frmTransferIn.aspx";
                }
            },
            modal: true
        });
    });
};

function fncShowConfirmSaveMessage(val) {
    try {
        if (val == 0) {
            InitializeDialogConfirmSave();
            $("#ConfirmSaveDialog").dialog('open');
        }
        else {
            InitializeDialogConfirmUpdate();
            $("#ConfirmSaveDialogupdate").dialog('open');
        }
    }
    catch (err) {
        alert(err.message);
        console.log(err);
    }
}

        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
                return false;
            }
            catch (err) {
                alert(err.message);
            }
        }

        function CloseConfirmSave() {
            try {
                $("#CloseConfirmupdate").dialog('close');
                return false;
            }
            catch (err) {
                alert(err.message);
            }
        }

function fncVenItemRowdblClk(rowObj) {
    try {
        rowObj = $(rowObj);
        fncOpenItemhistory($.trim($("td", rowObj).eq(1).text()));
    }
    catch (err) {
        fncToastError(err.message);
    }
}
/// Open Item History
function fncOpenItemhistory(itemcode) {
    var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
        page = page + "?InvCode=" + itemcode + "&Status=dailog";
        var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
            autoOpen: false,
            modal: true,
            height: 700,
            width: 1250,
            title: "Inventory History"
        });
        $dialog.dialog('open');
    }
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'TransferInAccept');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "TransferInAccept";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a href="../Transfer/frmTransferIn.aspx">Transfer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page"><%=Resources.LabelCaption.lbl_Transfer_Accept%> </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="top-purchase-container" style="padding-left: 5px;">
            <div class="top-purchase-left-container" style="width: 50%">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-left">
                            <asp:Label ID="Label4" runat="server" Text="Transfer NO"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtTRNO" runat="server" CssClass="form-control-res"
                                ReadOnly="True"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-right">
                        <div class="label-left">
                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lbl_FromLocation %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:DropDownList ID="sFromLocation" runat="server" CssClass="form-control-res"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>


            </div>
            <div class="top-purchase-left-container" style="width: 50%">
                <div class="control-group-split">

                    <div class="control-group-left">
                        <div class="label-left">
                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lbl_Data %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtDate" runat="server" CssClass="form-control-res"
                                ReadOnly="True"></asp:TextBox>
                        </div>
                    </div>
                      <div id="divStockRequest" class="control-group-right display_none">
                        <div class="label-left">
                            <asp:Label ID="Label7" runat="server" Text="StockRequestCode"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                               <asp:TextBox ID="txtStockRequest" runat="server" CssClass="form-control-res"
                                onkeydown="return fncShowSearchDialogCommon(event, 'StockRequet', 'txtStockRequest', 'lnkAccept');"></asp:TextBox>
                        </div>
                    </div> 
                </div> 
            </div> 
        </div> 
        <div class="container-group-full" style="padding-left: 5px;">
            <asp:UpdatePanel ID="upTranAcc" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="over_flowhorizontal">
                        <table rules="all" border="1" id="tblPOSummary" runat="server" class="fixed_header transfer">
                            <tr>
                                <th>S.No</th>
                                <th>Item Code</th>
                                <th>Description</th>
                                <th>Transfer Qty</th>
                                <th>Accept Qty</th>
                                <th id="thbatchNo">Batch No</th>
                                <th>MRP</th>
                                <th>Tr.Sell Price</th>
                                <th>Seq.No</th>
                                <th>ReminQty</th>
                                <th>Cur.Sell Price</th>
                                
                            </tr>
                        </table>
                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 320px; width: 1356px; background-color: aliceblue;">
                            <asp:GridView ID="TrasnferAcceptGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                ShowHeaderWhenEmpty="True" ShowHeader="false" OnRowDataBound="grdItemDetails_RowDataBound"
                                CssClass="transfer">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="Records Not Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                    <asp:BoundField DataField="InventoryCode" HeaderText="Inventory Code"></asp:BoundField>
                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                    <asp:BoundField DataField="TransferQty" HeaderText="Transfer Qty"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Accept Qty">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAcceptQty" runat="server" Text='<%# Eval("AcceptQty") %>'
                                                onkeypress=" return isNumberKeyWithDecimalNew(event);" onkeydown=" return fncSetFocustoNextRow(event,this);"
                                                onchange="fncAddAcceptQty(this);"
                                                CssClass="gridTransfer-textbox"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="AcceptQty" HeaderText="Accept Qty"></asp:BoundField>--%>
                                    <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                    <asp:BoundField DataField="Mrp" HeaderText="Mrp" />
                                    <asp:BoundField DataField="sellingprice" HeaderText="S.Price" />
                                    <asp:BoundField DataField="SeqNo" HeaderText="SeqNo"></asp:BoundField>
                                    <asp:BoundField DataField="AcceptQty" HeaderText="ReminQty" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                    <asp:BoundField DataField="NetSellingPrice" HeaderText="NetSellingPrice"></asp:BoundField>
                                     
                                </Columns>
                            </asp:GridView>
                        </div> 
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="top-purchase-container">
            <%-- <asp:UpdatePanel ID="upTotal" runat="server" UpdateMode="Always" >
                <ContentTemplate>--%>
            <div class="top-purchase-left-container" style="width: 50%">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-left">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalItems %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtTotalItems" runat="server" CssClass="form-control-res"
                                ReadOnly="True"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-right">
                        <div class="label-left">
                            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalQty %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtTotalQty" runat="server" CssClass="form-control-res"
                                ReadOnly="True"></asp:TextBox>
                        </div>
                    </div>
                </div>


            </div>
            <div class="top-purchase-left-container" style="width: 50%">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-left">
                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_AcceptQty %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtAcceptQty"  runat="server" CssClass="form-control-res"
                                Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <%--   </ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <div class="control-container">
            <asp:UpdatePanel ID="upAccept" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkAccept" runat="server" class="button-red" OnClick="lnkSave_Click" Text='Accept(F4)'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkCancel" runat="server" class="button-red"
                            OnClick="lnkCancelAll_Click" Text='Cancel(F8)'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkCLose" runat="server" class="button-red" PostBackUrl="~/Transfer/frmTransferIn.aspx" Text='Back'></asp:LinkButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="container-group-full" style="display: none">

            <div id="TransferInAccept">
                <p>
                    <%=Resources.LabelCaption.Alert_Save%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div id="Save-dialog" style="display: none">
    </div>
    <div class="display_none">
        <asp:Button ID="btnSave" runat="server" OnClick="lnkAccept_Click" />

    </div>
    <div class="hiddencol">
        <div id="ConfirmSaveDialog">
            <p>
                Do you want to save with the Accepted Qty as you Entered ?
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick ="return fncAddvalidation();"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkAccept_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>    
        <div class="hiddencol">
        <div id="ConfirmSaveDialogupdate">
            <p>
                TransferIn Completed.click Yes to update the Status ?
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" 
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="LinkButton3_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return CloseConfirmupdate() "
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>    

    <asp:HiddenField ID="hidReason" runat="server" /> 

</asp:Content>
