﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmTransferOut.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmTransferOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .transfer td:nth-child(1), .transfer th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(2), .transfer th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(3), .transfer th:nth-child(3) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(4), .transfer th:nth-child(4) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(5), .transfer th:nth-child(5) {
            min-width: 150px;
            max-width: 150px;
        }

        .transfer td:nth-child(6), .transfer th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(7), .transfer th:nth-child(7) {
            min-width: 200px;
            max-width: 200px;
        }

        .transfer td:nth-child(8), .transfer th:nth-child(8) {
            min-width: 200px;
            max-width: 200px;
        }

        .transfer td:nth-child(9), .transfer th:nth-child(9) {
            min-width: 150px;
            max-width: 150px;
        }

        .transfer td:nth-child(8), .transfer th:nth-child(8) {
            min-width: 150px;
            max-width: 150px;
        }

        .transfer td:nth-child(10), .transfer th:nth-child(10) {
            min-width: 187px;
            max-width: 187px;
        }

        .transfer td:nth-child(11), .transfer th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(12), .transfer th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'TransferOut');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "TransferOut";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtFromDate.ClientID %>, #<%= txtToDate.ClientID %>").on('keydown', function () {
                return false;

            });
        });
    </script>
    <script type="text/javascript">
        function pageLoad() {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });

            function confirmation() {
                if (confirm('Do u want Approve ?')) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    </script>
    <script type="text/javascript">
        function confirmation() {
            if (confirm('Do u want Approve ?')) {
                return true;
            } else {
                return false;
            }
        }

        /// work start by saravanan 
        function fncDeleteConfirm(source) {
            try {
                var rowobj;
                if ($("#<%=ddlToLocStatus.ClientID %>").val() == "Partially Received") {
                    fncToastError('<%=Resources.LabelCaption.msg_CannotDelParTran%>');
                    return false;
                }
                else if ($("#<%=ddlToLocStatus.ClientID %>").val() == "Accepted") {
                    fncToastError('<%=Resources.LabelCaption.msg_CannotDelAccTran%>');
                    return false;
                }
                else if ($("#<%=ddlToLocStatus.ClientID %>").val() == "Checking") {
                    fncToastError("Already In Progress.You Cannot be Delete.");
                    return false;
                }
                  else if ($("#<%=hidDeletebtn.ClientID %>").val() == "D0") {
                      fncToastError("You have no permission to Delete this TransferOut.");
                      return false;
                }
                else {
                    rowobj = $(source).parent().parent();
                    $("#<%=hidTranNo.ClientID %>").val($.trim($("td", rowobj).eq(4).text()));
                    fncShowDeleteDailog();
                }

        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
        return false;
    }

    //Show Delete Dailog
    function fncShowDeleteDailog() {
        $(function () {
            setTimeout(function () {
                $("#dialog-Delete").html('<%=Resources.LabelCaption.alert_TranConfirm%>');
            }, 50);
            $("#dialog-Delete").dialog({
                title: "Enterpriser Web",
                buttons: {
                    Yes: function () {
                        $(this).dialog("destroy");
                        $("#<%=btnDelete.ClientID %>").click();
                    },
                    No: function () {
                        $(this).dialog("destroy");
                    }
                },
                modal: true
            });
        });
    };

    /// Check Row Selected or Not
    function fncPrintValidation() {
        try {
            if ($("#<%=hidTranNo.ClientID %>").val() == "") {
                ShowPopupMessageBox('<%=Resources.LabelCaption.msg_select%>');
                return false;
            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    $(document).ready(function () {
        $(document).on('keydown', disableFunctionKeys);
    });

    function disableFunctionKeys(e) {
        try {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 116) {
                     if ($('#<%=lnkAdd.ClientID %>').is(":visible"))
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkAdd', '');
                    e.preventDefault();
                    return false;
                }
                else if (e.keyCode == 118) {
                    __doPostBack('ctl00$ContentPlaceHolder1$LinkFetch', '');
                    e.preventDefault();
                    return false;
                }
                e.preventDefault();
                return false;
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }
    function fncSetValue() {
        try {

        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
        function fncDiscontinue() {
            $(function () {
                $("#dialog-DiscontinueEntry").html('<%=Resources.LabelCaption.lbl_loadDiscontinueEntry%>');
                $("#dialog-DiscontinueEntry").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("destroy");
                            $("#<%=btnDis.ClientID%>").click();
                        },
                        "No": function () {
                            $(this).dialog("destroy");
                            $("#<%=hidNew.ClientID%>").val('New'); 
                              $("#<%=btnDis.ClientID%>").click();
                        }
                    },
                    modal: true
                });
            });
        }; 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Transfer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Transfer Out </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full">
            <div class="transfer-order-header" style="background-color: #e6e6e6">
                <div class="control-container" style="float: left; clear: both; display: table; width: 30%">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" OnClick="lnkAdd_Click" Text="Add (F5)"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:UpdatePanel ID="upfetch" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:LinkButton ID="LinkFetch" runat="server" class="button-blue" OnClick="lnkFetch_Click" Text="Refresh (F7)"></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div>
                        <div style="margin-right: 15%; float: right; margin-top: 10px;">
                            <asp:CheckBox ID="chSmallPrint" runat="server" Text="Small Print"></asp:CheckBox>
                        </div>
                    </div>
                    </div>
                    <div class="transfeOut">
                        <div class="transferIn_btn">
                            <div>
                                <div class="tran_lbl">
                                    <asp:Label ID="Label5" runat="server" Text="Warhouse"></asp:Label>
                                </div>
                                <div class="tran_txt">
                                    <asp:TextBox ID="txtWarehouse" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse', 'txtWarehouse', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div>
                                <div class="tran_lbl">
                                    <asp:Label ID="Label4" runat="server" Text="OutletName"></asp:Label>
                                </div>
                                <div class="tran_txt">
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="transferIn_btn">
                            <div>
                                <div class="tran_lbl">
                                    <asp:Label ID="Label1" runat="server" Text="From Loc.Status"></asp:Label>
                                </div>
                                <div class="tran_txt">
                                    <asp:DropDownList ID="ddlFromLocStatus" runat="server" CssClass="form-control-res">
                                        <asp:ListItem Value="Transfered"></asp:ListItem>
                                        <asp:ListItem Value="Revoked"></asp:ListItem>
                                        <asp:ListItem Value="Partially Revoked"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div>
                                <div class="tran_lbl">
                                    <asp:Label ID="Label26" runat="server" Text="From Date"></asp:Label>
                                </div>
                                <div class="tran_txt">
                                    <asp:TextBox ID="txtFromDate" runat="server" Style="padding-left: 8px;" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="transferIn_btn">
                            <div>
                                <div class="tran_lbl">
                                    <asp:Label ID="Label3" runat="server" Text="To Loc.Status"></asp:Label>
                                </div>
                                <div class="tran_txt">
                                    <asp:DropDownList ID="ddlToLocStatus" runat="server" CssClass="form-control-res"
                                        OnSelectedIndexChanged="ddlToLocationStatus_SelectedIndexChanged"
                                        AutoPostBack="True">
                                        <asp:ListItem Value="Not Received"></asp:ListItem>
                                        <asp:ListItem Value="Checking"></asp:ListItem>
                                        <asp:ListItem Value="Accepted"></asp:ListItem>
                                        <asp:ListItem Value="Partially Received"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div>
                                <div class="tran_lbl">
                                    <asp:Label ID="Label6" runat="server" Text="To Date"></asp:Label>
                                </div>
                                <div class="tran_txt">
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="transfer-order-detail">
                <div class="over_flowhorizontal">
                    <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header transfer">
                        <tr>
                            <th>S.No</th>
                            <th>Delete</th>
                            <th>View</th>
                            <th>Print</th>
                            <th>Transfer No</th>
                            <th>Transfer Data</th>
                            <th>From Location</th>
                            <th>To Location</th>
                            <th>Frm.Loc.Status</th>
                            <th>To.Loc.Status</th>
                            <th>Created User</th>
                            <th>Create Date</th>
                        </tr>
                    </table>
                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 400px; width: 1355px; background-color: aliceblue;">
                        <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="gvPO" runat="server" AllowSorting="True" Width="100%" AutoGenerateColumns="False"
                                    ShowHeaderWhenEmpty="True" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                    ShowHeader="false"
                                    CssClass="pshro_GridDgn transfer">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <%--<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />--%>
                                    <Columns>
                                        <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                    ToolTip="Click here to Delete" OnClientClick="fncDeleteConfirm(this);return false;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Print">
                                            <ItemTemplate>
                                                <asp:Button ID="btnView" runat="server" Text='View'
                                                    OnClick="lnkView_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Print">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnPrint" runat="server" ImageUrl="~/images/print1.png"
                                                    ToolTip="Click here to Print" OnClick="lnkPrint_click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TransferNo" HeaderText="Transfer No"></asp:BoundField>
                                        <asp:BoundField DataField="CurTransferDate" HeaderText="Transfer Date"></asp:BoundField>
                                        <asp:BoundField DataField="FromLocation" HeaderText="From Location"></asp:BoundField>
                                        <asp:BoundField DataField="ToLocation" HeaderText="To Location"></asp:BoundField>
                                        <asp:BoundField DataField="FromLocationStatus" HeaderText="From Loc.Status"></asp:BoundField>
                                        <asp:BoundField DataField="ToLocationStatus" HeaderText="To Loc.Status" />
                                        <asp:BoundField DataField="CreateUser" HeaderText="Created By"></asp:BoundField>
                                        <asp:BoundField DataField="CreateDate" HeaderText="Created On"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                <asp:HiddenField ID="hidTranNo" runat="server" Value=""></asp:HiddenField>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

            </div>

        </div>
    </div>
    
    <div class="hiddencol">
         <div id="dialog-DiscontinueEntry">
            </div>
        <div id="dialog-Delete">
        </div>
        <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" />
        <asp:Button ID="btnDis" runat="server" OnClick="btnDis_Click" />
        <asp:HiddenField ID ="hidNew" runat="server" />
          <asp:HiddenField ID ="hidEditbtn" runat="server" />
          <asp:HiddenField ID ="hidDeletebtn" runat="server" />
          <asp:HiddenField ID ="hidViewbtn" runat="server" />  
         <asp:HiddenField ID ="hidSavebtn" runat="server" />


    </div>

</asp:Content>

