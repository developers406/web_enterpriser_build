﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmRequestedStockView.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmRequestedStockView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .hiddencol {
            display: none;
        }

        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }

        .StkReqView td:nth-child(1), .StkReqView th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .StkReqView td:nth-child(2), .StkReqView th:nth-child(2) {
            min-width: 60px;
            max-width: 60px;
            text-align: center;
        }

        .StkReqView td:nth-child(3), .StkReqView th:nth-child(3) {
            min-width: 60px;
            max-width: 60px;
            text-align: center;
        }

        .StkReqView td:nth-child(4), .StkReqView th:nth-child(4) {
            min-width: 150px;
            max-width: 150px;
        }

        .StkReqView td:nth-child(5), .StkReqView th:nth-child(5) {
            min-width: 300px;
            max-width: 300px;
        }

        .StkReqView td:nth-child(6), .StkReqView th:nth-child(6) {
            min-width: 167px;
            max-width: 167px;
        }

        .StkReqView td:nth-child(7) {
            text-align: right !important;
            padding-right: 9px;
        }

        .StkReqView td:nth-child(7), .StkReqView th:nth-child(7) {
            min-width: 150px;
            max-width: 150px;
        }

        .StkReqView td:nth-child(8), .StkReqView th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .StkReqView td:nth-child(9), .StkReqView th:nth-child(9) {
            min-width: 150px;
            max-width: 150px;
        }

        .StkReqView td:nth-child(10), .StkReqView th:nth-child(10) {
            min-width: 150px;
            max-width: 150px;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'RequestedStockView');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "RequestedStockView";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript" language="Javascript">

        function pageLoad() {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $(document).on('keydown', disableFunctionKeys);
        }

        function fncValidateDelete() {
            try {
                var gridtr = $("#<%= gvRequestedStock.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    //alert($("#<%=gvRequestedStock.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvRequestedStock.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmDeleteMessage1();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        $(document).ready(function () {
            $('#<%=rdReportClick.ClientID %> input').change(function () {
                 if ((this).value == "PickList") {
                     $("#divSmall").hide();
                 }
                 else {
                     $("#divSmall").show();
                 }
             });
         });
         function InitializeDialogDelete1() {
             try {
                 // alert("sdfg");
                 $("#DisplayDelete").dialog({
                     autoOpen: false,
                     resizable: false,
                     height: 150,
                     width: 370,
                     modal: true
                 });
             }
             catch (err) {
                 alert(err.message);
             }
         }
         function fncShowSuccessDeleteMessage() {
             try {
                 //alert("message");
                 InitializeDialogDelete1();
                 $("#DisplayDelete").dialog('open');
             }
             catch (err) {
                 alert(err.message);
                 console.log(err);
             }
         }

         function fncCloseDeleteDialog() {
             try {
                 $("#DisplayDelete").dialog('close');


             }
             catch (err) {
                 alert(err.message);
             }
         }
         function fncShowConfirmDeleteMessage1() {
             try {
                 InitializeDialogDeletes();
                 $("#DeleteConfirm").dialog('open');
             }
             catch (err) {
                 alert(err.message);
                 console.log(err);
             }
         }
         function InitializeDialogDeletes() {
             try {
                 $("#DeleteConfirm").dialog({
                     autoOpen: false,
                     resizable: false,
                     height: 150,
                     width: 370,
                     modal: true
                 });
             }
             catch (err) {
                 alert(err.message);
             }
         }
         function CloseConfirmDelete() {
             try {
                 $("#DeleteConfirm").dialog('close');

             }
             catch (err) {
                 alert(err.message);
             }
         }
         function Clear() {
             try {
                 $("#DeleteConfirm").dialog('close'); return false;
             }
             catch (err) {
                 alert(err.message);
             }
         }


         function EnableOrDisableDropdown(element, isEnable) {
             //alert(element[0]);
             //console.log(element);
             element[0].selectedIndex = 0;
             element.attr("disabled", isEnable);
             element.trigger("liszt:updated");

         }
         function isNumberKey(evt) {
             var charCode = (evt.which) ? evt.which : evt.keyCode;
             if (charCode != 46 && charCode > 31
             && (charCode < 48 || charCode > 57))
                 return false;

             return true;
         }

         function fncShowAlertNoItemsMessage() {
             try {
                 InitializeDialogAlertNoItems();
                 $("#AlertNoItems").dialog('open');
             }
             catch (err) {
                 alert(err.message);
                 console.log(err);
             }
         }
         //Close Save Dialog
         function fncCloseAlertNoItemsDialog() {
             try {
                 $("#AlertNoItems").dialog('close');
                 return false;

             }
             catch (err) {
                 alert(err.message);
             }
         }
         //Save Dialog Initialation
         function InitializeDialogAlertNoItems() {
             try {
                 $("#AlertNoItems").dialog({
                     autoOpen: false,
                     resizable: false,
                     height: 150,
                     width: 370,
                     modal: true
                 });
             }
             catch (err) {
                 alert(err.message);
             }
         }
         function fncValidateSave() {
             try {
                 var gridtr = $("#<%= gvRequestedStock.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    //alert($("#<%=gvRequestedStock.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvRequestedStock.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmSaveMessage();
                    } else {
                        fncShowMessage();
                    }
                }
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }


        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#SelectAny").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#SelectAny").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#SelectAny").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmDeleteMessage() {
            try {
                InitializeDialogDelete();
                $("#DeleteStockUpdatePosting").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close'); return false;
            }
            catch (err) {
                alert(err.message);
            }
        }
        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');
                return false;
            }
            catch (err) {
                alert(err.message);
            }
        }
        function disableFunctionKeys(e) {
            try {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 123) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkRefresh', '');
                        e.preventdefault();
                        return false;
                    }
                    else if (e.keyCode == 112) {
                        e.preventDefault();
                    }
                    else {
                        e.preventdefault();
                        return false;
                    }

                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncAcceptValidation(source) {
            var rowobj;
            try {
                if ($("#<%=hidSavebtn.ClientID %>").val() == "N0") {
                    fncToastError("You have no permission to Accept this Transferin.");
                    return false;
                }
                rowobj = $(source).parent().parent();
                if ($("#<%=chkProcessPO.ClientID %>").is(':checked') || $("#<%=chkProcessed.ClientID %>").is(':checked')) {  
                    ShowPopupMessageBox("Already this Request was Processed");
                    return false;
                }

                else {
                    return true;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Transfer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_StockReqFrmOutlet%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <div class="top-container-im">
                <div class="StkSumView">
                    <div>
                        <div class="tran_lbl">
                            <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lbl_location %>'></asp:Label>
                        </div>
                        <div class="tran_txt">
                            <%--<asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" CssClass="form-control-res">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                        </div>
                    </div>
                    <div>
                        <div class="tran_lbl">
                            <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                        </div>
                        <div class="tran_txt">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="StkSumView">
                    <div>
                        <div class="tran_lbl">
                            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lbl_WarehouseName %>'></asp:Label>
                        </div>
                        <div class="tran_txt">
                            <%--<asp:DropDownList ID="ddlwarehouse" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlwarehouse_SelectedIndexChanged" CssClass="form-control-res">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtWarehouse" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse', 'txtWarehouse', '');"></asp:TextBox>
                        </div>
                    </div>
                    <div>
                        <div class="tran_lbl">
                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                        </div>
                        <div class="tran_txt">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="StkSumView">
                    <div>
                        <div class="width_hunper" style="width: 51% !important">
                            <asp:CheckBox ID="chkProcessed" runat="server" AutoPostBack="True" OnCheckedChanged="chkProcessed_CheckedChanged" Text='Processed' />
                        </div>
                    </div>
                    <div>
                        <div class="width_hunper" style="width: 51% !important">
                            <asp:CheckBox ID="chkProcessPO" runat="server" AutoPostBack="True" OnCheckedChanged="chkProcessPO_CheckedChanged" Text='<%$ Resources:LabelCaption,lbl_ProcessedPO %>' />
                        </div>
                    </div>

                    <asp:RadioButtonList ID="rdReportClick" runat="server" AutoPostBack="false">
                        <asp:ListItem Value="Document" Selected="True">Print Document</asp:ListItem>
                        <asp:ListItem Value="PickList">Print Pick List</asp:ListItem>
                    </asp:RadioButtonList>
                    <div id="divSmall">
                        <asp:CheckBox ID="chSmallPrint" Style="margin-left: 170px;" runat="server" Text="Small Print"></asp:CheckBox>
                    </div>
                    <%--forPrint--%>
                </div>
                <div class="StkSumView">
                    <div class="control-button float_right">
                        <asp:LinkButton ID="lnkRefresh" runat="server" class="button-blue" OnClick="lnkRefresh_Click"
                            Text='ReFresh (F12)'></asp:LinkButton>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                <ContentTemplate>
                    <div class="over_flowhorizontal" style="margin-top: 5px;">
                        <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header StkReqView">
                            <tr>
                                <th>S.No</th>
                                <th>Proceed</th>
                                <th>Print</th>
                                <th>Request No</th>
                                <th>Request Date</th>
                                <th>From Location</th>
                                <th>Total Qty</th>
                                <th>WareHouse</th>
                                <th>Created User</th>
                                <th>Created Date</th>
                            </tr>
                        </table>
                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 430px; width: 1355px; background-color: aliceblue;">
                            <asp:GridView ID="gvRequestedStock" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                CssClass="pshro_GridDgn StkReqView" EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="Records Not Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter " />
                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Proceed">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnProceed" runat="server" ImageUrl="~/images/accept.png"
                                                ToolTip="Click here to Proceed" OnClick="imgbtnProceed_click" OnClientClick=" return fncAcceptValidation(this);" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Print">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnPrint" runat="server" ImageUrl="~/images/print1.png"
                                                ToolTip="Click here to Print" OnClick="lnkPrint_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="StockReqNo" HeaderText="Request No"></asp:BoundField>
                                    <asp:BoundField DataField="ReqDate" HeaderText="Request Date"></asp:BoundField>
                                    <asp:BoundField DataField="LocationName" HeaderText="From Location"></asp:BoundField>
                                    <asp:BoundField DataField="ReqQty" HeaderText="Total Qty"></asp:BoundField>
                                    <asp:BoundField DataField="WareHouse" HeaderText="WareHouse"></asp:BoundField>
                                    <asp:BoundField DataField="createUser" HeaderText="Created User"></asp:BoundField>
                                    <asp:BoundField DataField="createDate" HeaderText="Created Date"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div class="modal-loader">
                        <div class="center-loader">
                            <img alt="" src="../images/loading_spinner.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="container-group-full" style="display: none">
                <div id="SelectAny">
                    <p>
                        <%=Resources.LabelCaption.Alert_Select_Any%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="DisplayDelete">
                    <p>
                        <%=Resources.LabelCaption.Alert_Delete%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton7" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="DeleteConfirm">
                    <p>
                        <%=Resources.LabelCaption.Alert_DeleteSure%>
                    </p>
                    <div style="width: 150px; margin: auto">
                        <div style="float: left">
                            <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" OnClientClick="return CloseConfirmDelete()"
                                Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                        </div>
                        <div style="float: right">
                            <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                                Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="ConfirmSaveDialog">
                    <p>
                        <%=Resources.LabelCaption.Alert_Confirm_Save%>
                    </p>
                    <div style="width: 150px; margin: auto">
                        <div style="float: left">
                            <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                                Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                        </div>
                        <div style="float: right">
                            <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                                Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="StockUpdatePosting">
                    <p>
                        <%=Resources.LabelCaption.Alert_Delete%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="StockUpdateSave">
                    <p>
                        <%=Resources.LabelCaption.Alert_Save%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="AlertNoItems">
                    <p>
                        <%=Resources.LabelCaption.Alert_No_Items%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk%>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full">
                <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
                <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
                <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />

                <asp:HiddenField ID="hidEditbtn" runat="server" />
                <asp:HiddenField ID="hidDeletebtn" runat="server" />
                <asp:HiddenField ID="hidViewbtn" runat="server" />
                <asp:HiddenField ID="hidSavebtn" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
