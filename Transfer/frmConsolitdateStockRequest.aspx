﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmConsolitdateStockRequest.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmConsolitdateStockRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        

    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

       
        .stkAllocation td:nth-child(1) {
            min-width: 40px;
            max-width: 40px;
        }

        .stkAllocation td:nth-child(2) {
            min-width: 90px;
            max-width: 90px;
        }

        .stkAllocation td:nth-child(3) {
            min-width: 350px;
            max-width: 350px;
        }

        .stkAllocation td:nth-child(4) {
            min-width: 60px;
            max-width: 60px;
            text-align: right !important;
            padding-right: 9px;
        }

        .stkAllocation td:nth-child(5) {
            min-width: 60px;
            max-width: 60px;
            text-align: right !important;
            padding-right: 9px;
        }

        .stkAllocation td:nth-child(6) {
            min-width: 60px;
            max-width: 60px;
            text-align: right !important;
            padding-right: 9px;
        }

        .stkAllocation td:nth-child(7) {
            min-width: 60px;
            max-width: 60px;
            text-align: right !important;
            padding-right: 9px;
        }

        .stkAllocation th:nth-child(8), .stkAllocation td:nth-child(8) {
            display: none;
        }

        .stkAllocation td:nth-child(9) {
            min-width: 300px;
            max-width: 300px;
        }

        .stkAllocation th:nth-child(10), .stkAllocation td:nth-child(10) {
            display: none;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'ConsolidateStockRequest');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "ConsolidateStockRequest";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript" language="Javascript">
        function pageLoad() {

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            fncCreateCellWidth();

            $('#<%=ddlStkAllMethod.ClientID %>').show().removeClass('chzn-done');
            $('#<%=ddlStkAllMethod.ClientID %>').next().remove();
        }

        $(document).ready(function () {
            $('.pshro_GridDgn input[type=text]').keydown(function (e) {
                if (e.keyCode == 40) { // down
                    $(':focus').nextAll('input:not([tabIndex=-1]):first').focus()
                } else if (e.keyCode == 39) { //right
                    $(':focus').nextAll('input:not([tabIndex=-1]):first').focus()
                } else if (e.keyCode == 38) { //up
                    $(':focus').prevAll('input:not([tabIndex=-1]):first').focus()
                } else if (e.keyCode == 37) { //left
                    $(':focus').prevAll('input:not([tabIndex=-1]):first').focus()
                }
            });
        });

        function fncCreateCellWidth() {
            var colcnt = 0;
            try {
                colcnt = parseInt(10) + parseInt($("#<%=hidLocCont.ClientID %>").val());

                    for (var i = 11; i <= colcnt; i++) {
                        $(".stkAllocation td:nth-child(" + i + ")").css("min-width", "100px");
                        $(".stkAllocation td:nth-child(" + i + ")").css("max-width", "100px");

                    }


                }
                catch (err) {
                    fncToastError(err.message);
                }
            }

            function ShowPopupPO(name) {
                $("#dvPo").dialog({
                    title: name,
                    width: 500,
                    buttons: {
                        OK: function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            }


            function XmlGridValue() {
                try {

                    var xml = '<NewDataSet>';
                    $("#<%=gvRequestedStock.ClientID %> tr").each(function () {

                        var cells = $("td", this);
                        if (cells.length > 0) {

                            xml += "<Table>";
                            for (var j = 0; j < cells.length; ++j) {
                                if ($(this).parents('table:first').find('th').eq(j).text().indexOf('_') != -1) {
                                    xml += '<' + $(this).parents('table:first').find('th').eq(j).text() + '>' + $("input:text", cells.eq(j)).val() + '</' + $(this).parents('table:first').find('th').eq(j).text() + '>';
                                }
                                else {
                                    xml += '<' + $(this).parents('table:first').find('th').eq(j).text() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text() + '>';
                                }
                            }
                            xml += "</Table>";
                        }
                    });

                    xml = xml + '</NewDataSet>'

                    // alert(xml);


                    $("#<%=HiddenXmldata.ClientID %>").val(escape(xml));
                    //alert($("#<%=HiddenXmldata.ClientID %>").val()); 


                }
                catch (err) {
                    return false;
                    alert(err.Message);
                }
            }


            //$("#<%=lstRequestNo.ClientID %>,#<%=lstRequestNo.ClientID %>_chzn,#<%=lstRequestNo.ClientID %>_chzn > div,#<%=lstRequestNo.ClientID %>_chzn > div > div > input").css("width", '100%');

        $(function () {

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        });

        function Savevalue(evt) {
            //console.log("Saving value " + $(evt.target).val());
            //$(evt.target).focus().select();
            $(evt.target).data('val', $(evt.target).val());

        }

        function onFocusOutQty(evt) {

            var currentRow = $(evt.target).closest("tr");
            var Stock = parseFloat(currentRow.find("td:eq(4)").html());
            //var col1 = currentRow.find("td:eq(0)").html(); // get current row 1st table cell TD value 
            //var col3 = currentRow.find(".CRP").val(); // get current row 3rd table cell  TD value
            //var data = col1 + "\n" + col2 + "\n" + col3;
            var dReqQty = 0;
            $(evt.target).closest("tr").each(function () {
                var cells = $("td", this);
                if (cells.length > 0) {
                    for (var j = 0; j < cells.length; ++j) {
                        if ($(this).parents('table:first').find('th').eq(j).text().indexOf('_') != -1) {
                            dReqQty += parseFloat($("input:text", cells.eq(j)).val());
                        }
                    }
                }
            });

            if (dReqQty > Stock) {
                var prev = $(evt.target).data('val');
                var current = $(evt.target).val();
                $(evt.target).val(prev);
                //console.log("Prev value " + prev);
                //console.log("New value " + current);
                $(evt.target).focus().select();
                //ShowPopupMessageBox('Invalid Quantity..!');
                alert('Invalid Quantity..!');
            }

            XmlGridValue();

            <%--    $('#<%=gvRequestedStock.ClientID%>').find('tr').each(function(row) {
                    $(this).find('input,select,textarea').each(function (Pricetxt) {
                        $ctl = $(this);
                        if ($ctl.is('select')) {
                            //dropdown code
                        } else if  (ctl.is('input:checkbox')) {
                            //checkbox code 
                        }  else {
                            // textbox/textarea code
                        } 
                    });
                });--%>

            //$(evt.target).each(function () {
            //    //if ($(this).find('input:checkbox').attr("checked"))
            //    sum += parseInt($(this).find('input:text').attr("value"));
            //});
        }


        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                evt.preventDefault();

            if (charCode == 46 && $(evt.target).val().indexOf('.') != -1) {
                evt.preventDefault();
            }
            if (charCode == 13) {
                XmlGridValue();
                Pricetxt = $(evt.target).closest("td").next().find("input");
                Pricetxt.focus().select();
            }
        }
        function fncRequestqtyChange(evt) {

            var POtd = null;
            var TOtd = null;
            var Qtytxt = null;
            var Stocktd = null;
            var ActualQtytd = null;

            Qtytxt = $(evt.target);
            Stocktd = $(evt.target).closest("td").next();
            POtd = $(evt.target).closest("td").next().next().next();
            TOtd = $(evt.target).closest("td").next().next().next();
            ActualQtytd = $(evt.target).closest("tr").children('td:last');

            var ReqQty = parseFloat(Qtytxt.val());
            var Stock = parseFloat(Stocktd.text());
            var ActualQty = parseFloat(ActualQtytd.text());

            if (ReqQty > Stock) {
                ShowPopupMessageBox('Req Qty is Greater than Stock..!');
                Qtytxt.val(ActualQty.toFixed(3));
                Qtytxt.focus();
                return;
            }

            if (ReqQty <= Stock) {
                TOtd.text(ReqQty.toFixed(3));
                Qtytxt.val(ReqQty.toFixed(3));
            }

            return true;
        }

        //function isReqQtyText(evt) {

        //    var charCode = (evt.which) ? evt.which : evt.keyCode;
        //    if (charCode != 46 && charCode > 31
        //    && (charCode < 48 || charCode > 57))
        //        return false;

        //    if (charCode == 13) {
        //        //alert('enter');
        //        //                var Pricetxt = $(evt.target).closest("td").next().find("input");
        //        //                Pricetxt.focus().select();
        //        fncRequestqtyChange(evt);

        //    }
        //    return true;
        //}

        function isReqQtyText(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57) && !(charCode > 95 && charCode < 106) && charCode != 110) {
                    return false;
                    evt.preventDefault();
                }

                if (charCode == 13) {

                    fncRequestqtyChange(evt);
                }
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function GetValues() {
            var values = "";
            var listBox = document.getElementById("<%= lstRequestNo.ClientID%>");
            for (var i = 0; i < listBox.options.length; i++) {
                if (listBox.options[i].selected) {
                    values += listBox.options[i].innerHTML + " " + listBox.options[i].value + "\n";
                }
            }
            $("#hidenReqNo").val(values);
            //alert(values);
            return false;
        }

        $(document).ready(function () {
            $("#ContentPlaceHolder1_lstRequestNo_chzn").css("height", "30px;");
        });

    </script>

    <style type="text/css">
        .HeaderFreez {
            position: relative;
            top: expression(this.offsetParent.scrollTop);
            z-index: 10;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="maincontainer" style="padding: 10px 3px 3px;">
         <div class="breadcrumbs">
            <ul> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
                </ul></div>
        <div class="stk_Allocation" id="divDateFilter">
            <div class="container-invoice-header">
                DATE FILTER
            </div>
            <div class="small-box bg-aqua" style="margin-top: 5px;">
                <div class="col-md-12 ">
                    <div class="col-md-5">
                        <asp:Label ID="Label4" Font-Bold="true" runat="server" Text="From Date"></asp:Label>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                    </div>
                    <div class="col-md-5" style="margin-top: 5px;">
                        <asp:Label ID="Label5" Font-Bold="true" runat="server" Text="To Date"></asp:Label>
                    </div>
                    <div class="col-md-7" style="margin-top: 5px;">
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>

        <div class="stk_Allocation display_none" id="divLocationFilter"  >

            <div class="container-invoice-header">
                LOCATION FILTER
            </div>
            <div class="small-box bg-aqua" style="margin-top: 5px;">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <asp:Label ID="Label3" Font-Bold="true" runat="server" Text="Location"></asp:Label>
                    </div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="DropDownLocation" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4" style="margin-top: 5px;">
                        <asp:Label ID="Label18" Font-Bold="true" runat="server" Text="Vendor"></asp:Label>
                    </div>
                    <div class="col-md-8" style="margin-top: 5px;">
                        <asp:DropDownList ID="DropdownVendor" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>

                </div>
            </div>

        </div>

        <div class="stk_Allocation display_none" id="divRequestFilter">
            <div class="container-invoice-header">
                STOCK REQUEST FILTER
            </div>
            <div class="small-box bg-aqua" style="margin-top: 5px;">
                <div class="col-md-12">
                    <asp:ListBox ID="lstRequestNo" runat="server" Rows="3" CssClass="form-control-res" SelectionMode="Multiple"></asp:ListBox>
                </div>
            </div>
        </div>
        <div class="stk_Allocation">
            <div class="container-invoice-header">
                GRN FILTER
            </div>
            <div class="small-box bg-aqua" style="margin-top: 5px;">
                <div class="col-md-12">
                    <asp:ListBox ID="lstGRN" runat="server" Rows="3" CssClass="form-control-res" SelectionMode="Multiple"></asp:ListBox>
                </div>
            </div>
        </div>
        <div class="stk_fetch">
            <div>
                <div class="stkalloc_radiobtn">
                    <asp:LinkButton ID="lnkCreatePo" runat="server" class="button-blue"
                        Text="Proceed" OnClientClick="XmlGridValue()" OnClick="lnkCreatePo_Click"></asp:LinkButton>
                </div>
                <div class="stkalloc_radiobtn">
                    <asp:LinkButton ID="lnkLoadReq" runat="server" class="button-blue"
                        Text="Fetch" OnClientClick="GetValues()" OnClick="lnkLoadReq_Click"></asp:LinkButton>
                </div>
            </div>
            <div>
                <div class="stkalloc_radiobtn">
                    <div class="float_left">
                        <asp:Label ID="lblStkAMethid" runat="server" Text="Stk.AM"></asp:Label>
                    </div>
                    <div class="stkAlloc_ratio">
                        <asp:DropDownList ID="ddlStkAllMethod" runat="server" CssClass="form-control-res">
                            <asp:ListItem Value="Equal" Text="Equal"> </asp:ListItem>
                            <asp:ListItem Value="SalesBase" Text="SalesBase"> </asp:ListItem>
                            <asp:ListItem Value="PenStkReq" Text="PenStkReq"> </asp:ListItem>
                            <asp:ListItem Value="GRN-Equal" Text="GRN-Equal"> </asp:ListItem>
                            <asp:ListItem Value="GRN-SalesBase" Text="GRN-SalesBase"> </asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="col-md-12" style="margin-top: 3px; height: 100%;" id="divIndentRequest">
                    <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                        style="height: 400px">
                        <asp:GridView ID="gvRequestedStock" runat="server" Width="100%" AutoGenerateColumns="true"
                            ShowHeaderWhenEmpty="true"
                            CssClass="stkAllocation" EmptyDataRowStyle-CssClass="Emptyidclassforselector" OnRowDataBound="gvRequestedStock_DataBound">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                            <PagerStyle CssClass="pshro_text" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />                           
                        </asp:GridView>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>



    </div>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div id="dvPo">
        <asp:GridView ID="grdPOCreated" runat="server" CellPadding="10" AutoGenerateColumns="false"
            ForeColor="#333333">
            <Columns>
                <asp:BoundField DataField="PONO" HeaderText="PO-No" ItemStyle-Width="150">
                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="VendorCode" HeaderText="Vendor Code" ItemStyle-Width="100">
                    <ItemStyle Width="100" HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="VendorName" HeaderText="Vendor Name" ItemStyle-Width="400">
                    <ItemStyle Width="400px" HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Width="150">
                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
            </Columns>
            <AlternatingRowStyle BackColor="White" />
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" BorderColor="Black" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
    </div>
    <div class="display_none">
        <asp:HiddenField ID="hidenReqNo" runat="server" Value="" />
        <input type="hidden" id="HiddenXmldata" runat="server" />
        <asp:HiddenField ID="hidLocCont" runat="server" Value="0" />
    </div>

</asp:Content>
