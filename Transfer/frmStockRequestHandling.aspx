﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmStockRequestHandling.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmStockRequestHandling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .hiddencol {
            display: none;
        }

        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }

        .stk_handling td:nth-child(1), .stk_handling th:nth-child(1) {
            min-width: 40px;
            max-width: 40px;
        }

        .stk_handling td:nth-child(2), .stk_handling th:nth-child(2) {
            min-width: 65px;
            max-width: 65px;
        }

        .stk_handling td:nth-child(3), .stk_handling th:nth-child(3) {
            min-width: 325px;
            max-width: 325px;
            text-align: left !important;
        }

        .stk_handling td:nth-child(4), .stk_handling th:nth-child(4) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .stk_handling td:nth-child(5), .stk_handling th:nth-child(5) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .stk_handling td:nth-child(6), .stk_handling th:nth-child(6) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .stk_handling td:nth-child(7), .stk_handling th:nth-child(7) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .stk_handling td:nth-child(8), .stk_handling th:nth-child(8) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .stk_handling td:nth-child(9), .stk_handling th:nth-child(9) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .stk_handling td:nth-child(10), .stk_handling th:nth-child(10) {
            display: none;
        }

        .stk_handling td:nth-child(11), .stk_handling th:nth-child(11) {
            min-width: 190px;
            max-width: 190px;
        }

        .stk_handling td:nth-child(12), .stk_handling th:nth-child(12) {
            min-width: 110px;
            max-width: 110px;
        }

        .stk_handling td:nth-child(13), .stk_handling th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }


        .stk_handling td:nth-child(14), .stk_handling th:nth-child(14) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'StockRequestHandling');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "StockRequestHandling";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
    <script type="text/javascript" language="Javascript">
        var rowObjCom;
        function pageLoad() {
            $("#<%= txtRequestedDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("select").chosen({ width: '100%' });
            fncVendorSearch();
            $(document).on('keydown', disableFunctionKeys);
            $('#<%=lnkSave.ClientID %>').css("display", "block");
        }

        function isReqQtyText(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            if (charCode == 13) {
                //alert('enter');
                //                var Pricetxt = $(evt.target).closest("td").next().find("input");
                //                Pricetxt.focus().select();
                fncRequestqtyChange(evt);

            }
            return true;
        }

        function ShowPopupPO(name) {
            $("#dvPo").dialog({
                title: name,
                width: 350,
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }
        //Purchase Order saved successfully
        /// Show Save Dailog
        function fncShowPOSaveDailog() {
            $('#<%=lblTran.ClientID %>').text('<%=Resources.LabelCaption.msg_ConvertStkRegToPO%>');
            fncOpenSaveDailog();
        }

        function fncShowTransferSaveDailog(TranNo) {
            $('#<%=lblTran.ClientID %>').text('<%=Resources.LabelCaption.msg_ConvertStkToTran%>' + TranNo);
            fncOpenSaveDailog();
        }

        function fncOpenSaveDailog() {
            $("#StockRegSave").dialog({
                title: name,
                width: 350,
                modal: true
            });
        }


        function fncRequestqtyChange(evt) {

            var POtd = null;
            var TOtd = null;
            var Qtytxt = null;
            var Stocktd = null;
            var ActualQtytd = null;

            Qtytxt = $(evt.target);
            Stocktd = $(evt.target).closest("td").next();
            POtd = $(evt.target).closest("td").next().next().next();
            TOtd = $(evt.target).closest("td").next().next().next().next();
            ActualQtytd = $(evt.target).closest("tr").children('td:last');

            var ReqQty = parseFloat(Qtytxt.val());
            var Stock = parseFloat(Stocktd.text());
            var ActualQty = parseFloat(ActualQtytd.text());

            if (ReqQty > Stock) {
                alert('Req Qty is Greater than Stock..!');
                Qtytxt.val(ActualQty.toFixed(3));
                Qtytxt.focus();
                return;
            }

            if (ReqQty <= Stock) {
                TOtd.text(ReqQty.toFixed(3));
                Qtytxt.val(ReqQty.toFixed(3));
            }

            return true;
        }


        function fncValidateCheckAll() {

            try {
                var gridtr = $("#<%= gvRequestedStock.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    if ($("#<%=Check.ClientID %>").text() == "Select All") {

                        $("#<%=Check.ClientID %>").text("De-Select All");
                        $("#<%=gvRequestedStock.ClientID %> [id*=chkSingle]").prop('checked', true);


                    }
                    else {

                        $("#<%=Check.ClientID %>").text("Select All");
                        $("#<%=gvRequestedStock.ClientID %> [id*=chkSingle]").prop('checked', false);


                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncValidateDelete() {
            try {
                var gridtr = $("#<%= gvRequestedStock.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    //alert($("#<%=gvRequestedStock.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvRequestedStock.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmDeleteMessage1();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete1() {
            try {
                // alert("sdfg");
                $("#DisplayDelete").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessDeleteMessage() {
            try {
                //alert("message");
                InitializeDialogDelete1();
                $("#DisplayDelete").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncCloseDeleteDialog() {
            try {
                $("#DisplayDelete").dialog('close');


            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowConfirmDeleteMessage1() {
            try {
                InitializeDialogDeletes();
                $("#DeleteConfirm").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDeletes() {
            try {
                $("#DeleteConfirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteConfirm").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteConfirm").dialog('close'); return false;
            }
            catch (err) {
                alert(err.message);
            }
        }


        function EnableOrDisableDropdown(element, isEnable) {
            //alert(element[0]);
            //console.log(element);
            element[0].selectedIndex = 0;
            element.attr("disabled", isEnable);
            element.trigger("liszt:updated");

        }
        //Get Inventory Code DropDownList
        function setItemcode(itemcode) {
            try {

            }
            catch (err) {
                alert(err.Message);
            }
        }
        //Get Inventory code
        function fncGetInventoryCode(event, invcode) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    event.preventDefault();
                    fncGetBatchDetail_Master(invcode);
                    $('#<%=txthiddeninvcode.ClientID%>').val(invcode);
                    event.preventDefault();
                    event.stopPropagation();

                }
                return false;
            }
            catch (err) {
                alert(err.Message);
            }
        }


        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost, stock) {
            try {
                batchno = batchno.trim();
                $("#<%=gvRequestedStock.ClientID%> tr").each(function () {

                    var invcodeHidden = $('#<%=txthiddeninvcode.ClientID%>').val();
                    var gitemcode = $(this).find('td:eq(1)').text().trim();
                    if (gitemcode == invcodeHidden) {
                        $(this).find('input[id*=txtBatchno]').val(batchno);
                        $(this).find('input[id*=txtMrp]').val(mrp);
                        $(this).find('td:eq(6)').text(batchno);

                    }
                    console.log(gitemcode);

                });
                $('#<%=txthiddeninvcode.ClientID%>').val('');


            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function fncShowAlertNoItemsMessage() {
            try {
                InitializeDialogAlertNoItems();
                $("#AlertNoItems").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseAlertNoItemsDialog() {
            try {
                $("#AlertNoItems").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogAlertNoItems() {
            try {
                $("#AlertNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncValidateMakeTransferandPO() {
            try {
                fncShowConfirmSaveMessageTransferandPO();
            }
            catch (err) {
                fncToastError(err.message);

            }
        }
        function fncValidateSave() {
            try {
                fncShowConfirmSaveMessage();
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }


        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#SelectAny").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#SelectAny").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#SelectAny").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }


        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowConfirmSaveMessageTransferandPO() {
            try {
                InitializeDialogConfirmSaveTransferandPO();
                $("#ConfirmSaveDialogTransferandPO").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSaveTransferandPO() {
            try {
                $("#ConfirmSaveDialogTransferandPO").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSaveTransferandPO() {
            try {
                $("#ConfirmSaveDialogTransferandPO").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowConfirmDeleteMessage() {
            try {
                InitializeDialogDelete();
                $("#DeleteStockUpdatePosting").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close'); return false;
            }
            catch (err) {
                alert(err.message);
            }
        }
        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');
                return true;
            }
            catch (err) {
                alert(err.message);
            }
        }

        function InitializeDialogUpdate() {
            try {
                $("#UpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowUpdateMessage() {
            try {
                InitializeDialogUpdate();
                $("#UpdateSave").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseUpdateDialog() {
            try {
                $("#UpdateSave").dialog('close');
                return true;
            }
            catch (err) {
                alert(err.message);
            }
        }

    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //$(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");


        }


        function fncVendorChange(source) {
            try {
                rowObjCom = $(source).parent().parent();
                fncShowVendorSearch();
                $("#dialog-Vendor").dialog("destroy");
                //rowObj.find('td input[id*="hidvendorCode"]').val($(source).val());                
                //$("td", rowObj).eq(10).text($(source).val());
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        ///Show Item Search Table
        function fncShowVendorSearch() {
            try {
                $("#dialog-Vendor").dialog({
                    resizable: false,
                    height: "auto",
                    width: 500,
                    modal: true,
                    title: "Vendor Search",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //// AutoComplete Search for Vendor
        function fncVendorSearch() {
            try {
                $("[id$=txtVendorSearch]").autocomplete({
                    source: function (request, response) {
                        var obj = {};
                        obj.searchData = escape(request.term);
                        obj.mode = "Vendor";

                        $.ajax({
                            url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncCommonSearch")%>',
                            data: JSON.stringify(obj),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1],
                                        valAddress: item.split('|')[2]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=txtVendorSearch.ClientID %>').val($.trim(i.item.valitemcode));
                        $('#<%=txtVendorAddress.ClientID %>').val($.trim(i.item.valAddress));
                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $("#dialog-Vendor").dialog("destroy");
                        rowObjCom.find('td input[id*="hidvendorCode"]').val($.trim(i.item.valitemcode));
                        $("td", rowObjCom).eq(10).text($.trim(i.item.label.split('-')[1]));
                        return false;
                    },
                    appendTo: $("#dialog-Vendor"),
                    minLength: 2
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncPOCreateClick() {
            try {
                if ($('#<%=chkCreatePO.ClientID %>').is(":checked") || $('#<%=hidTranStatus.ClientID %>').val() == "1") {
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkSave.ClientID %>').css("display", "none");
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function disableFunctionKeys(e) {
            try {

                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 115) {
                        if ($('#<%=chkCreatePO.ClientID %>').is(":checked") || $('#<%=hidTranStatus.ClientID %>').val() == "1") {
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                            e.preventDefault();
                            return false;
                        }
                    }
                    else {
                        e.preventDefault();
                        return false;
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Vendor") {
                    $('#<%=txtVendorSearch.ClientID %>').val($.trim(Description));
                $('#<%=txtVendorAddress.ClientID %>').val($.trim(VendorAddress));
                $("#dialog-Vendor").dialog("destroy");
                rowObjCom.find('td input[id*="hidvendorCode"]').val($.trim(Code));
                $("td", rowObjCom).eq(10).text($.trim(Description));
                appendTo: $("#dialog-Vendor");
            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    var Inventtorycode = '';
    function fncMRPChange(source) {
        try {
            var row = source.parentNode.parentNode;
            var itemcode = row.cells[1].innerHTML;
            fncGetItemDetail(itemcode)
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    function fncGetItemDetail(itemcode) {
        var obj = {};
        try {
            obj.itemcode = itemcode;
            obj.vendorcode = "";
            $.ajax({
                url: '<%=ResolveUrl("~/WebService.asmx/fncGetItemdetailForPR")%>',
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    fncAssignValuesToControl($.parseJSON(data.d));
                },
                error: function (response) {
                    fncToastError(response.message);
                },
                failure: function (response) {
                    fncToastError(response.message);
                }
            });
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncAssignValuesToControl(obj) {
        try {

            if (obj[0]["Status"] == "InvalidItems") {
                fncToastInformation("Invalid Items");
                Inventtorycode = '';
                clearForm();
                return;
            }
            else if (obj[0]["Status"] == "Batch") {
                Inventtorycode = obj[0]["InventoryCode"];
                fncGetBatchDetail_Master(obj[0]["InventoryCode"]);

            }
            else {
                Inventtorycode = '';
                ShowPopupMessageBox("This is Non Batch Item");
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncAssignbatchvaluestotextbox(batchNo, mrp, sellingprice, expMonth, expYear, basiccost, stock, netCost, gross) {
        try {
            $("#<%= gvRequestedStock.ClientID %> tbody tr").each(function () {
                    rowObjCom = $(this).parent().parent();
                    var Itemcode = $(this).find("td").eq(1).text();
                    if ($.trim(Itemcode) == $.trim(Inventtorycode)) {
                        //$("td", rowObjCom).eq(4).text($.trim(stock));
                        $(this).find("td").eq(4).html(stock); 
                        $(this).find("td").eq(5).text(mrp);
                        if (parseFloat($(this).find("td").eq(3).text()) < parseFloat(stock)) {
                            $(this).find("td").eq(6).text('0.000');
                            $(this).find("td").eq(7).text($(this).find("td").eq(3).text());
                        }
                        else {
                            var PoQty = parseFloat($(this).find("td").eq(3).text()) - parseFloat($(this).find("td").eq(4).text());
                            if (parseFloat(PoQty) < 0)
                                PoQty = '0.000'; 
                            $(this).find("td").eq(6).text(parseFloat(PoQty).toFixed(3));
                            if (parseFloat($(this).find("td").eq(4).text()) < 0)
                                $(this).find("td").eq(4).text() = '0.000';
                            $(this).find("td").eq(7).text($(this).find("td").eq(4).text());
                        }

                        $(this).find("td").eq(8).text(sellingprice);
                        $(this).find("td").eq(13).text(stock);
                        $(this).find("td").eq(14).text(batchNo);

                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncGetGridValuesForSave() {
            var obj;
            try {
                var xml = '<NewDataSet>';
                $("#<%= gvRequestedStock.ClientID %> tbody tr").each(function () {
                    if ($(this).find('td input[id*=txtTransferQty]').val().trim() > 0) {
                        xml += "<Table>";
                        xml += "<RowNo>" + $(this).find("td").eq(0).text().trim() + "</RowNo>";
                        xml += "<StockReqNo>" + $('#<%=txtStockReqNo.ClientID %>').val().trim() + "</StockReqNo>";
                        xml += "<Inventorycode>" + $(this).find("td").eq(1).text().trim() + "</Inventorycode>";
                        xml += "<Description>" + $(this).find("td").eq(2).text().trim().replace('>', ')').replace('<', '(') + "</Description>";
                        xml += "<ReqQty>" + $(this).find("td").eq(3).text().trim() + "</ReqQty>";
                        xml += "<AvailableQty>" + $(this).find("td").eq(4).text().trim() + "</AvailableQty>";
                        xml += "<MRP>" + $(this).find("td").eq(5).text().trim() + "</MRP>";
                        xml += "<POQTY>" + $(this).find("td").eq(6).text().trim() + "</POQTY>";
                        xml += "<tranQty>" + $(this).find('td input[id*=txtTransferQty]').val().trim() + "</tranQty>";
                        xml += "<BatchNo>" + $(this).find("td").eq(14).text().trim() + "</BatchNo>";
                        xml += "<VendorCode>" + $(this).find('td input[id*=hidvendorCode]').val().trim() + "</VendorCode>";
                        xml += "<VendorName>" + $(this).find("td").eq(10).text().trim() + "</VendorName>";
                        xml += "</Table>";
                    }
            });
            xml = xml + '</NewDataSet>'
            xml = escape(xml);
            $('#<%=hidXml.ClientID %>').val(xml);
                return xml;

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function txtReqQty_change(source) {
            try {
                var rowObj = $(source).parent().parent();
                var val1 = rowObj.find('td input[id*="txtTransferQty"]').val();
                var val2 = rowObj.find("td").eq(4).text().trim();
                var diff = (parseFloat(val1) - parseFloat(val2)).toFixed(2);
                //rowObj.find("td").eq(13).html(val1);
                //if (parseFloat(val1) > parseFloat(val2)) {
                //    rowObj.find("td").eq(6).html(diff);
                //    rowObj.find("td").eq(7).html(val2);
                //    rowObj.find("td").eq(13).html(val2);
                //}
                //else {

                //    rowObj.find("td").eq(7).html(val1);
                //    rowObj.find("td").eq(13).html(val1);
                //}

                //if (parseInt(diff) < 0) {
                //    diff = parseFloat(diff * (-1));
                //}
                //rowObj.find("td").eq(6).val($.trim("0.00"));



                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">

                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Transfer</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Transfer/frmRequestedStockView.aspx">
                    <%=Resources.LabelCaption.lbl_StockReqFrmOutlet%></a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_StockReqHandle%></li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">

            <div class="top-container-im">
                <div class="transferIn_btn">
                    <div>
                        <div class="tran_lbl">
                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lbl_StockReqNo %>'></asp:Label>
                        </div>
                        <div class="tran_txt">
                            <asp:TextBox ID="txtStockReqNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div>
                        <div class="tran_lbl">
                            <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lblRequestdate %>'></asp:Label>
                        </div>
                        <div class="tran_txt">
                            <asp:TextBox ID="txtRequestedDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="transferIn_btn">
                    <div>
                        <div class="tran_lbl">
                            <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lbl_location %>'></asp:Label>
                        </div>
                        <div class="tran_txt">
                            <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div>
                        <div class="tran_lbl">
                            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lbl_WarehouseName %>'></asp:Label>
                        </div>
                        <div class="tran_txt">
                            <asp:TextBox ID="txtWarehouse" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="transferIn_btn">
                    <div>
                        <div class="tran_lbl">
                            <asp:CheckBox ID="chkCreatePO" runat="server" Text="Create PO" onchange="fncPOCreateClick();" />
                        </div>
                    </div>
                </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                <ContentTemplate>
                    <div class="over_flowhorizontal" style="margin-top: 5px;">
                        <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header stk_handling">
                            <tr>
                                <th>S.No</th>
                                <th>Item code</th>
                                <th>Description</th>
                                <th>ReqQty</th>
                                <th>Cur.Qty</th>
                                <th>MRP</th>
                                <th>POQty</th>
                                <th>TransferQty</th>
                                <th>Price</th>
                                <th>Vendor Code(Default)</th>
                                <th>Vendor Name(Default)</th>
                                <th>Vendor Change</th>
                                <th>MRP Change</th>
                                <th>Outlet Qty</th>
                            </tr>
                        </table>
                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 390px; width: 1355px; background-color: aliceblue;">
                            <asp:GridView ID="gvRequestedStock" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                CssClass="stk_handling" EmptyDataRowStyle-CssClass="Emptyidclassforselector"
                                DataKeyNames="StockReqNo" OnRowDataBound="gvRequestedStock_DataBound" OnRowEditing="gvRequestedStock_OnRowEditing">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="" />
                                <Columns>
                                    <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                    <asp:BoundField DataField="Inventorycode" HeaderText="Item code"></asp:BoundField>
                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                    <asp:BoundField DataField="ReqQty" HeaderText="ReqQty"></asp:BoundField>
                                     <%--<asp:TemplateField HeaderText="ReqQty">
                                                                <ItemTemplate>
                                                                    
                                      <asp:TextBox ID="txtReqQty" runat="server" Text='<%# Eval("ReqQty") %>' Width="67px" onfocus="this.select()" onchange="return txtReqQty_change(this)" ></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                      
                                    <asp:BoundField DataField="AvailableQty" HeaderText="Cur.Qty"></asp:BoundField>
                                    <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                    <asp:BoundField DataField="POQty" HeaderText="POQty"></asp:BoundField>
                                   <%-- <asp:BoundField DataField="tranQty" HeaderText="TransferQty"></asp:BoundField>--%>

                                     <asp:TemplateField HeaderText="TransferQty">
                                                                <ItemTemplate>
                                                                    
                                      <asp:TextBox ID="txtTransferQty" runat="server" Text='<%# Eval("tranQty") %>' Width="67px" onfocus="this.select()" onchange="return txtReqQty_change(this)"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                    <asp:BoundField DataField="Price" HeaderText="Price"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Vendor Code(Default)">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hidvendorCode" runat="server" Value='<%# Eval("VendorCode") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="VendorName" HeaderText="Vendor Name(Default)"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Supplier (Vendor) Name">
                                        <ItemTemplate>
                                            <asp:Button ID="btnVendorChange" runat="server" Text="Vendor Change" OnClientClick="fncVendorChange(this);return false;" />
                                            <%--<asp:DropDownList ID="ddlVendor" runat="server" Width="150px" CssClass="form-control-res"
                                                onchange="fncVendorChange(this);">
                                            </asp:DropDownList>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Supplier (Vendor) Name">
                                        <ItemTemplate>
                                            <asp:Button ID="btnMRPChange" runat="server" Text="MRP Change" OnClientClick="fncMRPChange(this);return false;" />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AvailableQty" HeaderText="Outlet Qty"></asp:BoundField>
                                    <asp:BoundField DataField="BatchNo" HeaderText="BatchNo" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div style="float: right; display: table">
                <div class="control-button">
                    <asp:LinkButton ID="lnkMakeSales" runat="server" class="button-blue" Visible="false"
                        Text="Convert to Bill" OnClick="lnkMakeSales_Click"></asp:LinkButton>
                </div>
                <div class="control-button hiddencol">
                    <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                        Text='<%$ Resources:LabelCaption,btn_SelectAll %>'></asp:LinkButton>
                </div>
                <div class="control-button hiddencol">
                    <asp:LinkButton ID="Delete" runat="server" class="button-blue" OnClientClick="fncValidateDelete();return false;"
                        Text='<%$ Resources:LabelCaption,btn_Delete %>'></asp:LinkButton>
                </div>
                <%--  <div class="control-button">
                    <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="fncValidateSave();return false;"
                        Text='<%$ Resources:LabelCaption,btn_Update %>' Width="100px"></asp:LinkButton>
                </div>--%>
                <div class="control-button">
                    <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClick="lnkSave_click"
                        OnClientClick="fncGetGridValuesForSave();" Text='Make Transfer and PO (F4)'></asp:LinkButton>
                </div>
            </div>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div class="modal-loader">
                        <div class="center-loader">
                            <img alt="" src="../images/loading_spinner.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="container-group-full" style="display: none">
                <div id="SelectAny">
                    <p>
                        <%=Resources.LabelCaption.Alert_Select_Any%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="DisplayDelete">
                    <p>
                        <%=Resources.LabelCaption.Alert_Delete%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton7" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="DeleteConfirm">
                    <p>
                        <%=Resources.LabelCaption.Alert_DeleteSure%>
                    </p>
                    <div style="width: 150px; margin: auto">
                        <div style="float: left">
                            <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" OnClientClick="return CloseConfirmDelete()"
                                Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkDelbtnOk_Click"> </asp:LinkButton>
                        </div>
                        <div style="float: right">
                            <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                                Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="ConfirmSaveDialog">
                    <p>
                        <%=Resources.LabelCaption.Alert_Confirm_Save%>
                    </p>
                    <div style="width: 150px; margin: auto">
                        <div style="float: left">
                            <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                                Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmUpdatebtnOk_Click"> </asp:LinkButton>
                        </div>
                        <div style="float: right">
                            <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                                Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="ConfirmSaveDialogTransferandPO">
                    <p>
                        <%=Resources.LabelCaption.Alert_Confirm_Save%>
                    </p>
                    <div style="width: 150px; margin: auto">
                        <div style="float: left">
                            <asp:LinkButton ID="LinkButton6" runat="server" class="button-blue" OnClientClick="return CloseConfirmSaveTransferandPO()"
                                Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                        </div>
                        <div style="float: right">
                            <asp:LinkButton ID="LinkButton8" runat="server" class="button-blue" OnClientClick="return CloseConfirmSaveTransferandPO() "
                                Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="StockUpdatePosting">
                    <p>
                        <%=Resources.LabelCaption.Alert_Delete%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="StockUpdateSave">
                    <p>
                        <%=Resources.LabelCaption.Alert_Save%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="UpdateSave">
                    <p>
                        <%=Resources.LabelCaption.Alert_Update%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton9" runat="server" class="button-blue" OnClientClick="return fncCloseUpdateDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnUpdate_Click"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="AlertNoItems">
                    <p>
                        <%=Resources.LabelCaption.Alert_No_Items%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk%>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full">
                <asp:TextBox ID="txthiddeninvcode" runat="server" Style="display: none" Font-Bold="True"></asp:TextBox>
                <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
                <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
                <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
                <asp:HiddenField ID="hidTranStatus" runat="server" Value="1" />
                <asp:HiddenField ID="hidXml" runat="server" Value="1" />
            </div>
        </div>
    </div>
    <div id="dvPo">
        <asp:GridView ID="grdPOCreated" runat="server" CellPadding="10" AutoGenerateColumns="False"
            ForeColor="#333333">
            <Columns>
                <asp:BoundField DataField="PONO" HeaderText="PO-No" ItemStyle-Width="150">
                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="VendorCode" HeaderText="Vendor" ItemStyle-Width="150">
                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Width="150">
                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
            </Columns>
            <AlternatingRowStyle BackColor="White" />
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" BorderColor="Black" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
    </div>
    <div class="hiddencol">
        <div id="dialog-Vendor">
            <div>
                <div class="float_left">
                    <asp:Label ID="lblItemSearch" runat="server" Text="Search"></asp:Label>
                </div>
                <div class="gidItem_Search">
                    <asp:TextBox ID="txtVendorSearch" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor',  'txtVendorSearch', '');" runat="server" class="form-control-res"></asp:TextBox>
                </div>
            </div>
            <div class="vendor_Searchaddress">
                <asp:TextBox ID="txtVendorAddress" runat="server" TextMode="MultiLine"
                    class="form-control-res vendor_Searchaddress_height"></asp:TextBox>
            </div>

        </div>
        <div id="StockRegSave">
            <div>
                <asp:Label ID="lblTran" runat="server"></asp:Label>
            </div>
            <div class="dialog_center">
                <asp:UpdatePanel ID="upsave" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="LinkButton10" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                            OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
