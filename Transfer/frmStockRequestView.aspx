﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmStockRequestView.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmStockRequestView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .selectedCell
        {
            background-color: Green;
        }
        .unselectedCell
        {
            background-color: white;
        }
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        td
        {
            cursor: grab;
        }
        .selected_row
        {
            background-color: Teal;
            color: White;
        }
        
        .editableTable
        {
            border: solid 1px;
            width: 100%;
        }
        
        .editableTable td
        {
            border: solid 1px;
        }
        
        .editableTable .cellEditing
        {
            padding: 0;
        }
        
        .editableTable .cellEditing input[type=text]
        {
            width: 100%;
            border: 0;
            background-color: rgb(255,253,210);
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'StockRequestView');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "StockRequestView";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


    <script type="text/javascript">

        $(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        });

        function ValidateForm() {
            var Show = '';
            if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose To date';
                $("#<%= txtFromDate.ClientID %>").focus();
            }

            if ($("#<%= txtToDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose from date';
                $("#<%= txtToDate.ClientID %>").focus();
            }

            if (Show != '') {
                alert(Show);
                return false;
            }

            else {

                var StartDate = $("#<%= txtFromDate.ClientID %>").val();
                var EndDate = $("#<%= txtToDate.ClientID %>").val();
                var eDate = new Date(EndDate);
                var sDate = new Date(StartDate);
                if (StartDate != '' && EndDate != '' && sDate > eDate) {
                    alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                    return false;
                }
                else {
                    return true;
                }
            }
        }

        //        $("#<%=grdStockReqList.ClientID%> tr:has(td)").click(function (e) {
        //            var selTD = $(e.target).closest("td");
        //            $("#<%=grdStockReqList.ClientID%> td").removeClass("selected");
        //            selTD.addClass("selected");
        //            alert('Selected Cell Value is: <b> ' + selTD.text() + '</b>');
        //        });

        function Clearall() {

            $("select").val(0);
            $("select").trigger("liszt:updated");
            $("table[id$='grdStockReqList']").html("");

            return false;
        }

        function checkAll(objRef) {
            try {
                var GridView = objRef.parentNode.parentNode.parentNode;
                var inputList = GridView.getElementsByTagName("input");
                for (var i = 0; i < inputList.length; i++) {
                    //Get the Cell To find out ColumnIndex
                    var row = inputList[i].parentNode.parentNode;
                    if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                        if (objRef.checked) {
                            //If the header checkbox is checked
                            //check all checkboxes
                            //and highlight all rows
                            row.style.backgroundColor = "aqua";
                            inputList[i].checked = true;
                        }
                        else {
                            //If the header checkbox is checked
                            //uncheck all checkboxes
                            //and change rowcolor back to original 
                            if (row.rowIndex % 2 == 0) {
                                //Alternating Row Color
                                row.style.backgroundColor = "#c4ddff";
                            }
                            else {
                                row.style.backgroundColor = "white";
                            }
                            inputList[i].checked = false;
                        }
                    }
                }

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function Check_Click(objRef) {

            try {
                //Get the Row based on checkbox
                var row = objRef.parentNode.parentNode;
                if (objRef.checked) {
                    //If checked change color to Aqua
                    row.style.backgroundColor = "aqua";
                }
                else {
                    //If not checked change back to original color
                    if (row.rowIndex % 2 == 0) {
                        //Alternating Row Color
                        row.style.backgroundColor = "#c4ddff";
                    }
                    else {
                        row.style.backgroundColor = "white";
                    }
                }
                //Get the reference of GridView
                var GridView = row.parentNode;

                //Get all input elements in Gridview
                var inputList = GridView.getElementsByTagName("input");

                for (var i = 0; i < inputList.length; i++) {
                    //The First element is the Header Checkbox
                    var headerCheckBox = inputList[0];
                    //Based on all or none checkboxes
                    //are checked check/uncheck Header Checkbox
                    var checked = true;
                    if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                        if (!inputList[i].checked) {
                            checked = false;
                            break;
                        }
                    }
                }
                headerCheckBox.checked = checked;

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function GetSelectedRequest() {
            var counter = 0;
            $("#<%=grdStockReqList.ClientID%> input[id*='CheckBox1']:checkbox").each(function (index) {
                if ($(this).is(':checked'))
                    counter++;

            });
            // alert(counter);
            if (counter > 1) {
                alert('Please Select Any One Request to Edit!');
                $("#<%=grdStockReqList.ClientID%> input[id*='CheckBox1']:checkbox").each(function (index) {
                    $("#<%=grdStockReqList.ClientID%> input[id*='CheckBox1']:checkbox").prop('checked', false);
                });
                return false;
            }
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration:none;">Transfer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Stock Requested List</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full-gid">
            <div class="top-purchase-container">
                <div class="top-purchase-left-container" style="width: 50%">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblfromdate %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Created By"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="DropDownUser" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbltodate %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lbl_warehouse %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="DropDownwarehouse" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="control-container-purch">
                        <div class="control-button" style="float: left;">
                            <asp:LinkButton ID="lnkAdd" runat="server" class="button-red" PostBackUrl="~/Transfer/frmStockRequest.aspx"
                                Text='<%$ Resources:LabelCaption,btnadd %>'></asp:LinkButton>
                        </div>
                        <div class="control-button" style="float: left;">
                            <asp:LinkButton ID="lnkEdit" runat="server" class="button-red" OnClientClick="return GetSelectedRequest()"
                                OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                        </div>
                        <div class="control-button hiddencol" style="float: left;">
                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btn_print %>'
                                OnClick="lnkPrint_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button" style="float: left;">
                            <asp:LinkButton ID="lnkSendtoHQ" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,lnkSend %>'
                                OnClick="lnkSend_Click"></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="top-purchase-left-container" style="width: 50%">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-right">
                                <asp:RadioButton ID="rdoAuto" runat="server" GroupName="RegularMenu" Text="Auto" />
                                <asp:RadioButton ID="rdoManual" runat="server" GroupName="RegularMenu" Text="Manual" />
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-button" style="float: left;">
                            <asp:LinkButton ID="lnkRefresh" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_refresh %>'
                                OnClick="lnkRefresh_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button" style="float: left;">
                            <asp:LinkButton ID="lnkShowHQ" runat="server" class="button-blue" Text="Show Request Send to HQ"
                                OnClick="lnkShowHQ_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button" style="float: right;">
                            <asp:LinkButton ID="lnkVeiwPending" runat="server" class="button-red" Text="View Pending List"
                                OnClick="lnkVeiwPending_Click"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-audit-container">
                <asp:GridView ID="grdStockReqList" runat="server" AutoGenerateColumns="true" PageSize="14"
                    ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgn" OnRowCommand="grdStockReqList_RowCommand"
                    OnRowDataBound="grdStockReqList_RowDataBound" OnRowDeleting="grdStockReqList_RowDeleting">
                    <PagerStyle CssClass="pshro_text" />
                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" onclick="Check_Click(this)" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField HeaderText="Print" ShowSelectButton="True" ButtonType="Button"
                            SelectText="Print" />
                        <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                        PageButtonCount="5" Position="Bottom" />
                    <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                    <RowStyle CssClass="pshro_GridDgnStyle" />
                </asp:GridView>
            </div>
        </div>
        <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
        <asp:HiddenField ID="hidenReqNo" runat="server" Value="" />
        <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
    </div>
</asp:Content>
