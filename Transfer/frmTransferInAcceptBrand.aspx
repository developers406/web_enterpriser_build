﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTransferInAcceptBrand.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmTransferInAcceptBrand" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        #grdAttributeSize {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdAttributeSize td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdAttributeSize tr:nth-child(even) {
                background-color: white;
            }

            #grdAttributeSize tr:hover {
                background-color: white;
            }

            #grdAttributeSize th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: indigo;
                font-weight: bolder;
                color: white;
            }

        /*Item Detials*/

        #grdItemDetails {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
        }

            #grdItemDetails td, #grdItemDetails th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdItemDetails tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #grdItemDetails tr:hover {
                background-color: lavender;
            }

            #grdItemDetails th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: green;
                font-weight: bolder;
                color: white;
                width: 100%;
            }

        .divtblItemdetail th {
            padding-top: 4px;
            padding-bottom: 4px;
            text-align: left !important;
            background-color: crimson;
            font-weight: bolder;
            color: white;
        }

            .divtblItemdetail td:nth-child(1), .divtblItemdetail th:nth-child(1) {
                min-width: 50px;
                max-width: 65px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(2), .divtblItemdetail th:nth-child(2) {
                min-width: 85px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblItemdetail td:nth-child(3), .divtblItemdetail th:nth-child(3) {
                min-width: 85px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblItemdetail td:nth-child(4), .divtblItemdetail th:nth-child(4) {
                min-width: 275px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblItemdetail td:nth-child(5), .divtblItemdetail th:nth-child(5) {
                min-width: 65px;
                max-width: 60px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(6), .divtblItemdetail th:nth-child(6) {
                min-width: 55px;
                max-width: 50px;
                text-align: right !important;
            }

            .divtblItemdetail td:nth-child(7), .divtblItemdetail th:nth-child(7) {
                min-width: 75px;
                max-width: 65px;
                text-align: right !important;
            }

            .divtblItemdetail td:nth-child(8), .divtblItemdetail th:nth-child(8) {
                min-width: 75px;
                max-width: 65px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(9), .divtblItemdetail th:nth-child(9) {
                min-width: 75px;
                max-width: 65px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(9), .divtblItemdetail th:nth-child(9) {
                min-width: 75px;
                max-width: 65px;
                text-align: center !important;
            }

        .transfer td:nth-child(1), .transfer th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(2), .transfer th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(3), .transfer th:nth-child(3) {
            min-width: 687px;
            max-width: 687px;
        }

        .transfer td:nth-child(4), .transfer th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(4) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(5), .transfer th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(5) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(6), .transfer th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(7), .transfer th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(7) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(8), .transfer th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(8) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(9), .transfer th:nth-child(9) {
            display: none;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'TransferInAcceptBrand');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "TransferInAcceptBrand";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">

        var glAcceptQty = 0, glTotalQty = 0;
        var GRNNo = "";
        var gRowno = 0;
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function pageLoad() {

            GRNNo = getParameterByName("GRNNo");
            //console.log(GRNNo);
            //LoadDynamicAttribute(GRNNo);
            //alert(GRNNo);
        }

        function fncScanBarcode(evt) {
            try {
                
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                InventoryCode = $('#<%=txtBarcodeScan.ClientID %>').val().toUpperCase();
                var bCheckItemCode = false;
                if (charCode == 13) {
                    var iTranQty = 0;
                    var iManualQty = 0;
                    var iCost = 0;
                    var iTotalCost = 0;
                    var sItemCode = "";
                    var sBatchNo = "";
                    var row = '';
                    $("#grdItemDetails tr").each(function () {
                        if (!this.rowIndex) return;                        
                        sItemCode = $(this).find("td").eq(2).html();
                        sBatchNo = $(this).find("td").eq(1).html();
                        sBatchNo = sItemCode.trim() + sBatchNo.trim(); 
                        iManualQty = parseInt($(this).find("td").eq(5).html());
                        iCost = parseFloat($(this).find("td").eq(6).html());
                        var AcceptQty = parseFloat($(this).find("td").eq(8).html());

                        $(this).closest('tr').children('td,th').css('background-color', '');
                        if (sBatchNo == InventoryCode.trim()) {
                           // row = $(this).parents('tr:first');

                            iTranQty = parseInt($(this).find('td').eq(4).find('input').val()) + 1;
                            var sum = parseFloat(iTranQty) + parseFloat(AcceptQty);
                            if (iManualQty < parseFloat(sum)) {
                                var Min = parseFloat(iManualQty) - parseFloat(AcceptQty);
                                $(this).find('td').eq(4).find('input').val(Min);
                                fncToastInformation('Already you accept ' + AcceptQty +' Qty');
                                $(this).find('td').eq(4).find('input').css('border', '2px solid red');
                            }
                           else if (iTranQty <= iManualQty)
                            {
                                $(this).find('td').eq(4).find('input').val(iTranQty);
                            }
                            
                            //iTotalCost = iTranQty * iCost;
                            //$(this).find("td").eq(8).html(iTotalCost.toFixed(2));
                            //$(this).find('td').eq(5).find('input').focus().select(); 
                            //$(this).find('td').eq(5).find('input').css('border', '2px solid red');
                            
                            if (iTranQty > iManualQty)
                            {
                                fncToastInformation('Receiving Quantity is Greater than Transfer Quantity !');
                                $(this).find('td').eq(4).find('input').css('border', '2px solid red');
                            }
                            else if (iManualQty == iTranQty)
                            {
                                $(this).find('td').eq(4).find('input').css('border', '2px solid green');
                            }
                            //if ($(this).find('td').eq(4).find('input').css('border') === '2px solid rgb(0, 128, 0)') {
                                
                            //    row.insertBefore(row.prev());
                            //}
                            //else {
                            //    row.insertAfter(row.next());
                            //}

                            $(this).closest('tr').children('td,th').css('background-color', 'rgb(117, 204, 200)');
                            $(this).closest('tr').children('td,th').focus();
                             
                          //  var row = $(this).closest('tr');
                            
                            //row.prev().insertAfter(row);
                            //$('table > tbody > tr:first').before(row);
                            // row.insertRow(0);
                            ///row.insertAfter(row.next());
                            //$(this).insertBefore('grdItemDetails > tbody > tr:first');

                            fncUpdateTransDetailTable(); 
                            fncShowTotalValues();
                            bCheckItemCode = true;
                            $('#<%=txtBarcodeScan.ClientID %>').val('');
                            $('#<%=txtBarcodeScan.ClientID %>').focus().select();

                        }
                    });

                    if (bCheckItemCode == false) {
                        fncToastInformation('Invalid Barcode');
                        $('#<%=txtBarcodeScan.ClientID %>').focus().select();
                         
                    } 
            }

            return true;
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    /// delete  Added Row
    function fncGRNRowKeydown(evt, source) {
        var charCode, rowobj, scrollheight;
        try {
            rowobj = $(source);
            charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 46) {
                $(source).remove();
                fncUpdateRowNow(); 
            }
            //else if (charCode == 113) {
            //    fncOpenItemhistory(rowobj.find('td input[id*="txtItemcode"]').val());
            //}
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncUpdateRowNow() {
        var rowNo = 0;
        try {

            $("#grdItemDetails tbody").children().each(function () {
                rowNo = parseInt(rowNo) + 1;
                
                //$(this).find('td input[id*="txtSNo"]').val(rowNo);
                $(this).find("td").eq(0).html(rowNo);
            });

            fncShowTotalValues();
            //fncNetValueCalculation();
            //fncSaveGRNEntryTemporarly();
        }
        catch (err) {
            fncToastError(err.message);
        }
    }


    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function ValidateForm() {

        var rows = 0;
        var iTranQty = 0;
        rows = $('#grdItemDetails tbody tr').length;
        $('#<%=lnkAccept.ClientID %>').css("display", "none");
        var bQuantitycheck = false;

        //$("#grdItemDetails tr").each(function () {
        //    if (!this.rowIndex) return;
        //    
        //    iTranQty = parseInt($(this).find('td').eq(5).find('input').val());

        //    if (iTranQty > 0)
        //        bQuantitycheck = true;

        //    $(this).find('td').eq(5).find('input').focus().select();
        //});

        //if (bQuantitycheck == false) { 
        //    //fncToastInformation('Enter Transfer Quantity to Proceed Transfer!');
        //    //return false;
        //}
        //else

        if (rows <= 0) {
            popUpObjectForSetFocusandOpen = $('#<%=txtBarcodeScan.ClientID %>');
            ShowPopupMessageBoxandOpentoObject('No items to Proceed Transfer!');
            $('#<%=lnkAccept.ClientID %>').css("display", "block");
            return false;
        } 
        else {

            XmlGridValue($('#grdItemDetails tr'), $('#<%=GRNXmldata.ClientID %>'));
            return true;
        }
}

function LoadDynamicAttribute(ProductCode) {
    var obj = {};
    obj.Code = ProductCode;

    $.ajax({
        type: "POST",
        url: "frmTransferInAcceptBrand.aspx/GetAttributes",
        data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            var objvendor = jQuery.parseJSON(msg.d);
            console.log(objvendor);
            //console.log(Object.keys(objvendor).length);
            numofTable = Object.keys(objvendor).length;
            //

            $('#grdAttributeSize').find("tr").remove();
            if (numofTable > 0) {

                if (objvendor.Table.length > 0) {
                    var bSizeCheck = false;
                    var bSleeveCheck = false;

                    funBindSizeGrid(objvendor.Table);
                    fncBindInventory(objvendor.Table1);
                   // $("[id*=divAddButtons]").show();
                    fncDisableStockRows();

                    return false;
                }
                else {

                    return false;
                }
            }

            return false;

        },
        error: function (data) {
            ShowPopupMessageBox(data.message);
            return false;
        }
    });
}

function fncDisableStockRows() {
    try {
        // Disable Odd Rows in Table  tr:odd
        $("#grdAttributeSize tr:odd").each(function () {
            var cells = $("td", this);
            var Sleeve = "";
            if (cells.length > 0) {
                for (var j = 0; j < cells.length; ++j) {

                    if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
                        //if ($("input:text", cells.eq(j)).val() != '0') {
                        $("input:text", cells.eq(j)).attr('readonly', 'true');
                        $("input:text", cells.eq(j)).css('background-color', 'lavender');
                        $("input:text", cells.eq(j)).css('font-weight', 'bold');
                        //}
                    }
                }
            }
        });

        //$("#grdAttributeSize tr:even").each(function () {
        //    var cells = $("td", this);
        //    var Sleeve = "";
        //    if (cells.length > 0) {
        //        for (var j = 0; j < cells.length; ++j) {

        //            if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
        //                if ($("input:text", cells.eq(j)).val() != '0') {
        //                    $("input:text", cells.eq(j)).attr('readonly', 'true');
        //                    $("input:text", cells.eq(j)).css('background-color', 'lavender');
        //                    $("input:text", cells.eq(j)).css('font-weight', 'bold');
        //                }
        //            }
        //        }
        //    }
        //});

        return false;

    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncBindInventory(jsonObj) {
    var StyleObj, row, rowNo = 0;
    var StyleBody;
    try {

        //StyleObj = jsonObj;
        //StyleBody = $("#tblInventory tbody");
        //StyleBody.children().remove();
        //console.log(jsonObj);
        //for (var i = 0; i < StyleObj.length; i++) {
        //    rowNo = parseInt(rowNo) + 1;
        //    //row = "<tr onclick='javascript:showRow(this);' id='trInventory_" + i + "' tabindex='" + i + "' >"
        //    row = "<tr id='trInventory_" + i + "' tabindex='" + i + "' >"
        //                           + "<td id='tdSize' >" + StyleObj[i]["ValueName"] + "</td>"
        //                           + "<td id='tdCode' >" + StyleObj[i]["InventoryCode"] + "</td>"
        //                           + "<td id='tdName' align='right' >" + StyleObj[i]["Description"] + "</td>"
        //                           + "<td id='tdStock' align='right'>" + parseFloat(StyleObj[i]["Stock"]).toFixed(2) + "</td>"
        //                           + "<td id='tdMRP' align='right'>" + parseFloat(StyleObj[i]["MRP"]).toFixed(2) + "</td>"
        //                           + "<td id='tdSellingPrice' align='right'> " + parseFloat(StyleObj[i]["SellingPrice"]).toFixed(2) + "</td>"
        //                           //+ "<td id='tdSDAmt' align='right'> " + parseFloat(StyleObj[i]["SDAmt"]).toFixed(2) + "</td>"
        //                           //+ "<td id='tdDiscAmt' align='right'> " + parseFloat(StyleObj[i]["DiscAmt"]).toFixed(2) + "</td>"
        //                           //+ "<td id='tdVendorCode' align='right'> " + StyleObj[i]["VendorCode"] + "</td>"
        //                           + "<td id='tdStyleCode' align='right'> " + StyleObj[i]["StyleCode"] + "</td>"
        //                           + "</tr>";
        //    StyleBody.append(row);
        StyleObj = jsonObj;
        StyleBody = $("#grdItemDetails tbody");
        StyleBody.children().remove();
        console.log(jsonObj);
        for (var i = 0; i < StyleObj.length; i++) {
            rowNo = parseInt(rowNo) + 1;
            //row = "<tr onclick='javascript:showRow(this);' id='trInventory_" + i + "' tabindex='" + i + "' >"
            row = "<tr id='trgrddetails_" + i + "' onkeydown='return fncGRNRowKeydown(event,this);' tabindex='" + i + "' >"  
                                   + "<td id='tdRow' >" + rowNo + "</td>"
                                   + "<td id='tdStyleCode' >" + StyleObj[i]["StyleCode"] + "</td>"
                                   + "<td id='tdCode' align='right' >" + StyleObj[i]["InventoryCode"] + "</td>"
                                   + "<td id='tdDesc' align='right'>" + $.trim(StyleObj[i]["Description"]) + "</td>"
                                   + "<td id='tdSQty' align='right'>" + "<input type='text' id='txtQty' class='inputs' onfocus='this.select();'" + "onkeydown='return fncSetFocustoNextRow(event,this);' oninput='return fncPriceCalc(event)'  onkeypress= 'return isNumberKeyGA(event);' style='width:100%; text-align:center;' value= '" + parseFloat(StyleObj[i]["AcceptQty"]).toFixed(2) + "' > </td>" + "</td>"
                                   + "<td id='tdStock' align='right'> " + parseFloat(StyleObj[i]["Stock"]).toFixed(2) + "</td>"
                                   + "<td id='tdMrp' align='right'> " + parseFloat(StyleObj[i]["MRP"]).toFixed(2) + "</td>"
                                   //+ "<td id='tdDiscAmt' align='right'> " + parseFloat(StyleObj[i]["DiscAmt"]).toFixed(2) + "</td>"
                                   //+ "<td id='tdVendorCode' align='right'> " + StyleObj[i]["VendorCode"] + "</td>"
                                   + "<td id='tdSPrice' align='right'> " + parseFloat(StyleObj[i]["SellingPrice"]).toFixed(2) + "</td>"
                                   + "<td id='tdAccept' style ='display:none' align='right'> " + parseFloat(StyleObj[i]["AcceptQty"]).toFixed(2) + "</td>"
                                   + "</tr>";
            StyleBody.append(row);
            //StyleBody.children().dblclick(fncStyleRowDoubleClick);
            //StyleBody.children().click(fncSearchRowClick);
            //grdItemDetails
        }
            fncUpdateTransDetailTable();
            fncShowTotalValues();
            fncQtyCheck();
            XmlGridValue($('#grdItemDetails tr'), $('#<%=GRNXmldata.ClientID %>'));
    }
    catch (err) {
        fncToastError(err.message);
    }
}

        function fncQtyCheck() {//Vijay Qty Check
            $("#grdItemDetails tr").each(function () {
                if (!this.rowIndex) return;  
                
                var ScanQty = $(this).find('td').eq(4).find('input').val();
                var Qty = $(this).find("td").eq(5).html();

                if (parseFloat(ScanQty) >= parseFloat(Qty)) {
                    $(this).find('td').eq(4).find('input').attr("disabled", "disabled");
                }
            });
        }

function fncAddTransDetail() {
    try {

        var objSearchData, tblSearchData, Rowno = 0, batchNo = "";
        //tblSearchData = $("#grdItemDetails");
        tblSearchData = $("#TempgrdItemDetails tbody");
        var itemName = "";
        var CheckSize = true;
        var iTranQty = 0;
        var sSize = "";
        var sItemCode = "";
        var sDescription = "";
        var sCost = "";
        var sStock = "";
        var sSP = "";
        var sMRP = "";
        var sSDAmt = "";
        var sDiscAmt = "";
        var iTotalCost = 0;
        var StyleCodeCheck = false;
        var iTxtboxQty = 0;
        var QuantityCheck = false;

        $("#grdAttributeSize tr:even").each(function () {
            var cells = $("td", this);
            if (cells.length > 0) {
                for (var j = 0; j < cells.length; ++j) {

                    if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
                        if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {
                            QuantityCheck = true;
                            return;
                        }
                        else {
                            var isnum = /^\d+$/.test($("input:text", cells.eq(j)).val());
                            if (isnum) {
                                iTxtboxQty = iTxtboxQty + 1;
                            }
                        }
                    }
                }
            }

            iTxtboxQty = cells.length - iTxtboxQty;
            $("input:text", cells.eq(iTxtboxQty)).select().focus();
        });

        if (QuantityCheck == false) {
            fncToastInformation('Please Enter Accept Quantity !');
        }

        if (StyleCodeCheck == false && QuantityCheck == true) {

            var tablerow = $("#grdAttributeSize tr").length - 1;
            tablerow = tablerow / 2;
            var bitemAdded = false;
            //for (var p = 1; p <= tablerow; ++p) {
            // $("#grdAttributeSize tr:nth-last-child("+ p +")").each(function () {
            $("#grdAttributeSize tr:even").each(function () {

                var cells = $("td", this);
                var Sleeve = "";
                if (cells.length > 0) {
                    for (var j = 0; j < cells.length; ++j) {
                        if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() == 'SLEEVE') {
                            Sleeve = cells.eq(j).text();
                        }
                        if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
                            if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {
                                var Header = $(this).parents('table:first').find('th').eq(j).text().toUpperCase();
                                Header = Header.replace('(', '').replace(')', '');
                                console.log(Header + '--' + $("input:text", cells.eq(j)).val());
                                iTranQty = $("input:text", cells.eq(j)).val();

                                $("#tblInventory tr").each(function () {
                                    if (!this.rowIndex) return;
                                    
                                    sSize = $(this).find("td").eq(0).html();
                                    sItemCode = $(this).find("td").eq(1).html();
                                    sDescription = $(this).find("td").eq(2).html();
                                    sMRP = $(this).find("td").eq(4).html();
                                    sSP = $(this).find("td").eq(5).html();
                                    sStyleCode = $(this).find("td").eq(6).html();
                                    //sDiscAmt = $(this).find("td").eq(6).html();
                                    //sVendorCode = $(this).find("td").eq(7).html();
                                    //sStyleCode = $(this).find("td").eq(8).html();

                                    isSleeve = sDescription.includes(Sleeve);
                                    if (isSleeve) {
                                        if (Header.toUpperCase() == sSize.toUpperCase()) {
                                            console.log(sDescription);
                                            gRowno = parseFloat(gRowno) + parseFloat(1);
                                            iTotalCost = iTranQty * parseFloat(sCost);
                                            row = "<tr id='trTOItem_" + gRowno + "' onkeydown='return fncGRNRowKeydown(event,this);' tabindex='" + gRowno + "' >" +
                                                        '<td align="center">' + gRowno + '</td>' +
                                                        '<td>' + sStyleCode + '</td>' +
                                                        '<td>' + sItemCode + '</td>' +
                                                        '<td align="Left">' + sDescription + '</td>' +
                                                        '<td align="right">' + iTranQty + '</td>' +
                                                        '<td align="right">' + sMRP + '</td>' +
                                                        '<td align="right">' + sSP + '</td>' +
                                                        //'<td align="right">' + sSDAmt + '</td>' +
                                                        //'<td align="right">' + sDiscAmt + '</td>' +
                                                        //'<td align="right">' + sVendorCode + '</td>' +
                                                        '</tr>';
                                            tblSearchData.append(row);
                                            bitemAdded = true;
                                        }
                                        else if (Header.toUpperCase() == 'Quantity') {
                                            console.log(sDescription);
                                            gRowno = parseFloat(gRowno) + parseFloat(1);
                                            row = "<tr id='trTOItem_" + gRowno + "' onkeydown='return fncGRNRowKeydown(event,this);' tabindex='" + gRowno + "' >" +
                                                        '<td align="center">' + gRowno + '</td>' +
                                                        '<td>' + sStyleCode + '</td>' +
                                                        '<td>' + sItemCode + '</td>' +
                                                        '<td align="Left">' + sDescription + '</td>' +
                                                        '<td align="right">' + iTranQty + '</td>' +
                                                        '<td align="right">' + sMRP + '</td>' +
                                                        '<td align="right">' + sSP + '</td>' +
                                                        //'<td align="right">' + sSDAmt + '</td>' +
                                                        //'<td align="right">' + sDiscAmt + '</td>' +
                                                        //'<td align="right">' + sVendorCode + '</td>' +
                                                        '</tr>';
                                            tblSearchData.append(row);
                                            bitemAdded = true;
                                        }

                                        //$("[id*=divTransferBtn]").show();
                                    }

                                    //break;
                                });
                            }
                        }
                    }
                }
            });
            //}

            fncUpdateTransDetailTable();
            fncShowTotalValues();
            XmlGridValue($('#grdItemDetails tr'), $('#<%=GRNXmldata.ClientID %>'));
                //fncClear();

                //if (bitemAdded == false) {
                //    fncToastInformation('Enter Print Quantity!');
                //}
                //else {

                //}
            }

        }
        catch (err) {
            fncToastError(err.message);
        }
    }


        function XmlGridValue(tableid, XmlID) {
            try {

                var xml = '<NewDataSet>';
                
                // $('#grdAttribute tr').each(function () {
                tableid.each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {

                        xml += "<Table>";
                        for (var j = 0; j < cells.length; ++j) {

                            if ($(this).parents('table:first').find('th').eq(j).text().trim() == 'ScanQty'){
                                var scanqty ='';
                                if($("input:text", cells.eq(j)).is(':disabled'))
                                    scanqty ='0'
                                else
                                    scanqty = $("input:text", cells.eq(j)).val()
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + scanqty + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';
                            }
                            else
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';

                        }
                        xml += "</Table>";
                    }
                });

                xml = xml + '</NewDataSet>'
                console.log(xml);
                //alert(xml);

                XmlID.val(escape(xml));
                //alert(XmlID.val());
                //$("#hiddenXml").val($("[id*=txtPoNumber]").val());
            }
            catch (err) {
                return false;
                fncToastError(err.message);
            }
        }

    //function XmlGridValue(tableid, XmlID) {
    //    try {

    //        var xml = '<NewDataSet>';

    //        // $('#grdAttribute tr').each(function () {
    //        tableid.each(function () {
    //            var cells = $("td", this);
    //            if (cells.length > 0) {

    //                xml += "<Table>";
    //                for (var j = 0; j < cells.length; ++j) {
    //                    if ($(this).parents('table:first').find('th').eq(j).text().trim() == 'ScanQty')
    //                        xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + $("input:text", cells.eq(j)).val() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';
    //                    else
    //                        xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';

    //                }
    //                xml += "</Table>";
    //            }
    //        });

    //        xml = xml + '</NewDataSet>'
    //        console.log(xml);
    //        //alert(xml);

    //        XmlID.val(escape(xml));
    //        //alert(XmlID.val());
    //        //$("#hiddenXml").val($("[id*=txtPoNumber]").val());
    //    }
    //    catch (err) {
    //        return false;
    //        alert(err.Message);
    //    }
    //}

    function fncUpdateTransDetailTable() {
        try {

            
            var tblSearchData = $("#grdItemDetails");
            var sItemCode = '';
            var bCheckItemCode = false;

            $("#TempgrdItemDetails tr").each(function () {
                if (!this.rowIndex) return;
                

                sTempStyleCode = $(this).find("td").eq(1).html();
                sTempItemCode = $(this).find("td").eq(2).html();
                sTempDescription = $(this).find("td").eq(3).html();
                iTranQty = $(this).find("td").eq(4).html();
                sTempMRP = $(this).find("td").eq(5).html();
                sTempSP = $(this).find("td").eq(6).html();
                sTempSD = $(this).find("td").eq(7).html();
                sTempDISC = $(this).find("td").eq(8).html();
                sTempVendor = $(this).find("td").eq(9).html();

                gRowno = $("#grdItemDetails tbody").children().length + 1;

                $("#grdItemDetails tr").each(function () {
                    if (!this.rowIndex) return;
                    
                    sItemCode = $(this).find("td").eq(2).html();
                    if (sItemCode == sTempItemCode) {
                        $(this).find('td').eq(4).find('input').val(iTranQty);
                        $(this).find('td').eq(4).find('input').focus().select();
                        $(this).remove();
                        fncUpdateRowNow();
                        bCheckItemCode = true;
                    }
                });

                gRowno = $("#grdItemDetails tbody").children().length + 1;
                row = "<tr id='trTOoItem_" + gRowno + "' onkeydown='return fncGRNRowKeydown(event,this);' tabindex='" + gRowno + "' >" +
                                                       '<td align="center">' + gRowno + '</td>' +
                                                       '<td>' + sTempStyleCode + '</td>' +
                                                           '<td>' + sTempItemCode + '</td>' +
                                                           '<td align="Left">' + sTempDescription + '</td>' +
                                                           //'<td align="center"> <input type="text" id="txtQty" MaxLength="3" onfocus="this.select();" class="inputs" onkeydown="return fncSetFocustoNextRow(event,this);" oninput="return fncPriceCalc1(event)"  onkeypress= "return isNumberKeyGA(event);" style="width:100%; text-align:center;" readonly value=' + iTranQty + '> </td>' +
                                                           '<td align="center"> <input type="text" id="txtQty"  class="inputs" onfocus="this.select();"  onkeydown="return fncSetFocustoNextRow(event,this);" oninput="return fncPriceCalc(event)"  onkeypress= "return isNumberKeyGA(event);" style="width:100%; text-align:center;" value= "0" > </td>' +
                                                           '<td align="center" class="TransQty">' + iTranQty + '</td>' +
                                                           '<td align="right">' + sTempMRP + '</td>' +
                                                           '<td align="right">' + sTempSP + '</td>' +
                                                           //'<td align="right"  class= "hiddencol">' + sTempSD + '</td>' +
                                                           //'<td align="right"  class= "hiddencol">' + sTempDISC + '</td>' +
                                                           //'<td align="right" class= "hiddencol">' + sTempVendor + '</td>' +
                                                           '</tr>';

                tblSearchData.append(row);
                console.log('Row' + gRowno);

            });

            $('#TempgrdItemDetails').find("tr:gt(0)").remove();
        }
        catch (err) {
            fncToastError(err.message);
        }
    }


    function fncPriceCalc(evt) {
        try {

            var amountTD = null;
            var Qtytxt = null;
            var Stocktxt = null;
            var Costtxt = null;
            var AcceptQty = null;
            amountTD = $(evt.target).closest("td").next().next().next();
            Costtxt = $(evt.target).closest("td").next().next();
            Stocktxt = $(evt.target).closest("td").next();
            Qtytxt = $(evt.target);
            AcceptQty = $(evt.target).closest("tr").find("td").eq(8).text();

            if (Qtytxt.val() == '') {
                Qtytxt.val('0');
            } 
            
            var Qty = parseFloat(Qtytxt.val());
            var Stock = parseFloat(Stocktxt.text());
            var Cost = parseFloat(Costtxt.text());
            var OldQty = parseFloat(amountTD.text()) / parseFloat(Costtxt.text());


            if (Qty == OldQty) {
                Qtytxt.css('border', '1px solid #ddd');
                Qtytxt.css('border-width', '1px');
                Qtytxt.css('border-style', 'inset');
            }

            if (Qty == 0) {
                //ShowPopupMessageBox('Invalid Transfer Quantity !');
                fncToastInformation('Invalid Transfer Quantity !');
                Qtytxt.val(0);
                return;
            }

            if (Qty > Stock) {
                //ShowPopupMessageBox('Please Check Stock !');
                fncToastInformation('Please Check Transfer Quantity !');
                Qtytxt.val(0);
                Qtytxt.focus().select();
                Qty = 0;
                Qtytxt.css('border-color', 'red');
            }
            var sum =parseFloat(Qtytxt.val()) + parseFloat(AcceptQty);
            if (parseFloat(sum) > parseFloat(Stock)) {
                fncToastInformation('Already you accept ' + AcceptQty +' Qty');
                Qtytxt.val(parseFloat(Stock) - parseFloat(AcceptQty));
                Qtytxt.focus().select();
                Qty = AcceptQty;
                Qtytxt.css('border-color', 'red');
            }
            // var dTotalamount = parseFloat(Costtxt.text()) * Qty;
            //amountTD.text(dTotalamount.toFixed(2));
            //console.log(amountTD.text());
            fncShowTotalValues();
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    var sortByProperty = function (property) {

        return function (x, y) {

            return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));

        };

    };

    /// Bind Size Grid
    function funBindSizeGrid(jsonObj) {

        try {

            if (jsonObj == null)
                return;

            
            // Call Sort By Name
            //jsonObj = jsonObj.sort();
            jsonObj = jsonObj.sort(sortByProperty('SLEEVE'));
            ////Creating table rows 
            var table1 = document.getElementById("grdAttributeSize");
            //$('#grdAttributeSize').find("tr").remove();
            var BodyElement = document.createElement("tbody");

            if (jsonObj.length > 0) {
                var keys = Object.keys(jsonObj[0]);
                //keys = keys.filter(e => e !== 'SLEEVE');
                //keys.splice(0, 0, "SLEEVE");
                //console.log(arr.join());
                var rowth = document.createElement("thead");
                var rowHeader = document.createElement("tr");
                var cellHeader = null;
                for (var i = 0; i < keys.length; i++) {
                    cellHeader = document.createElement("th");
                    cellHeader.appendChild(document.createTextNode(keys[i]));
                    rowHeader.appendChild(cellHeader);
                }
                rowth.appendChild(rowHeader);
                table1.appendChild(rowth);
            }


            //Creating table rows
            for (var j = 0; j < jsonObj.length; j++) {
                var tableRowElement = document.createElement("tr");

                var tableRowId = "grdAttributeSize_row" + j;
                tableRowElement.setAttribute("id", tableRowId);
                //tableElement.setAttribute("class", tableclass);

                console.log(jsonObj[j]);
                for (var i = 0; i < keys.length; i++) {
                    var tableCellElement = document.createElement("td");
                    var key = Object.keys(jsonObj[j])[i];
                    var cellText;
                    var value = jsonObj[j][key];
                    console.log(key);
                    console.log(value);

                    //cellText = document.createTextNode(value);
                    //tableCellElement.contentEditable = "true";
                    //cellText.innerHTML = "<input type='text' class='Margin'onkeypress='return isNumberKeyGA(event);' style='width:100%; text-align:center;'/>";
                    if (key.toUpperCase() != "SLEEVE") {
                        var el = document.createElement('input');
                        el.type = 'text';
                        el.name = key;
                        el.id = key;
                        el.setAttribute("style", "width:100%; text-align:center;");
                        el.setAttribute("MaxLength", "3");
                        el.value = value;
                        //el.setAttribute("onkeypress", "return isNumberKeyGA(this);");
                        el.onkeypress = function (event) { return isNumberKeyGA(event) }
                        el.oninput = function (event) { return fncCheckStock(event) }
                        el.onfocus = function (event) { event.target.select() }

                        tableCellElement.appendChild(el);
                    }
                    else {
                        cellText = document.createTextNode(value);
                        tableCellElement.appendChild(cellText);
                    }

                    tableRowElement.appendChild(tableCellElement);
                }
                BodyElement.appendChild(tableRowElement);
            }

            table1.appendChild(BodyElement);

        }
        catch (err) {
            ShowPopupMessageBox(err.message)
        }
    }

    function isNumberKeyGA(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 46 && charCode > 31
          && (charCode < 48 || charCode > 57))
            return false;

        if (charCode == 46 && $(evt.target).val().indexOf('.') != -1) {
            evt.preventDefault();
        } // prevent if 

        if (charCode == 13) {
            if ($(evt.target).closest("td").next().length == 0) {
                // is last  
                Qtytxt = $(evt.target).closest("tr").children('td:first-child').find("input");
                Qtytxt.focus().select();
            }

            Qtytxt = $(evt.target).closest("td").next().find("input");
            Qtytxt.focus().select();
        }


        return true;
    }


    function fncCheckStock(evt) {
        //alert($(evt.target).val());
        try {
            //
            var Qtytxt = null;
            var iStock = 0;

            Qtytxt = $(evt.target);
            var Header = Qtytxt.attr("id");
            Header = Header.replace('(', '').replace(')', '');
            console.log(Header);


            $("#tblInventory tr").each(function () {
                var cells = $("td", this);
                if (cells.length > 0) {
                    for (var j = 0; j < cells.length; ++j) {
                        if ($(this).parents('table:first').find('th').eq(j).text().trim() == 'Size') {
                            if (cells.eq(j).text().trim() == Header) {
                                iStock = cells.eq(3).text();
                                if (Qtytxt.val() > iStock) {
                                    fncToastInformation('Accept Quantity Must be Less than or Equal to Transfer Quantity !');
                                    Qtytxt.val(parseInt(iStock));
                                    Qtytxt.css('border', '1px solid red');
                                    Qtytxt.focus().select();
                                }
                                else {
                                    Qtytxt.css('border', '1px solid #ddd');
                                    Qtytxt.css('border-width', '1px');
                                    Qtytxt.css('border-style', 'inset');
                                    Qtytxt.focus().select();
                                }
                            }
                        }
                    }
                }
            });
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    //Show Popup After Save
    function fncShowSuccessMessage() {
        try {
            InitializeDialog();
            $("#TransferInAccept").dialog('open');
        }
        catch (err) {
            alert(err.message);
            console.log(err);
        }
    }
    //Close Save Dialog
    function fncCloseSaveDialog() {
        try {
            $("#TransferInAccept").dialog('close');


        }
        catch (err) {
            alert(err.message);
        }
    }

    //Save Dialog Initialation
    function InitializeDialog() {
        try {
            $("#TransferInAccept").dialog({
                title: "EnterpriserWeb",
                autoOpen: false,
                resizable: false,
                height: 150,
                width: 370,
                modal: true
            });
        }
        catch (err) {
            alert(err.message);
        }
    }

    //Focus Set to Next Row
    function fncSetFocustoNextRow(evt, source) {
        try {
            var rowobj = $(source).parent().parent();
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13 || charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    setTimeout(function () {
                        NextRowobj.find('td input[id*="txtQty"]').focus();
                    }, 50);
                }
                else {
                    setTimeout(function () {
                        rowobj.siblings().find('td input[id*="txtQty_0"]').focus();
                    }, 50);
                }
                return false;
            }
            else if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    setTimeout(function () {
                        prevrowobj.find('td input[id*="txtQty"]').focus();
                    }, 50);
                }
                else {
                    setTimeout(function () {
                        rowobj.siblings().last().find('td input[id*="txtQty"]').focus();
                    }, 50);
                }
                return false;
            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    ///Add Accept Qty
    function fncAddAcceptQty(source) {

        var rowObj, totalAcceptQty = 0, acceptQty = 0, transferQty = 0;
        try {
            rowObj = $(source).closest("tr");
            transferQty = $("td", rowObj).eq(3).html().replace(/&nbsp;/g, '');
            
            rowObj = $(source).parent().parent();

            acceptQty = rowObj.find('td input[id*="txtAcceptQty"]').val();

            if (parseFloat(acceptQty) > parseFloat(transferQty)) {
                rowObj.find('td input[id*="txtAcceptQty"]').val(transferQty);
                acceptQty = rowObj.find('td input[id*="txtAcceptQty"]').val();
                fncToastInformation('<%=Resources.LabelCaption.msg_accept%>');
            }

            fncCalAcceptQty();

              <%--  glTotalQty = parseFloat(glTotalQty) - parseFloat(glAcceptQty);
                $('#<%=txtAcceptQty.ClientID %>').val((parseFloat(glTotalQty) + parseFloat(acceptQty)).toFixed(2));--%>
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

        function fncClear()
        {
            $('#grdItemDetails').find("tr:gt(0)").remove();
            $('#TempgrdItemDetails').find("tr:gt(0)").remove();
            return false;
        }


         function fncShowTotalValues() {
            try {
                var totalQuantity = 0;
                var AcceptQty = 0;
                var objAcceptQty = null;
                var objQuantity = null;

                $('#<%=txtTotalItems.ClientID%>').val($("#grdItemDetails tbody").children().length);

                $("#grdItemDetails tr").each(function () {
                    if (!this.rowIndex) return;
                    objAcceptQty = $(this).find('td input[id*="txtQty"]');
                    objQuantity = $(this).find('.TransQty');

                    if (objAcceptQty.val() != '') {
                        
                        AcceptQty = AcceptQty + parseFloat(objAcceptQty.val());

                        if (objQuantity.text() != '') {
                            totalQuantity = totalQuantity + parseFloat(objQuantity.text());
                        }
                    } 

                });

                //alert(totalAmount);
                //$("[id*=txtTotalQty]").val(totalQuantity.toFixed(2));             
                $("[id*=txtAcceptQty]").val(AcceptQty.toFixed(2));

                //$("[id*=hidTotalQty]").val(totalQuantity.toFixed(2));
                $("[id*=hidAcceptQty]").val(AcceptQty.toFixed(2));
                
            }
            catch (err) {
                fncToastError(err.message);
            }
         }

    function fncCalAcceptQty() {
        var acceptQty = 0;
        try {
            $("#ContentPlaceHolder1_TrasnferAcceptGrid tbody").children().each(function () {
                acceptQty = parseFloat(acceptQty) + parseFloat($(this).find('td input[id*="txtAcceptQty"]').val());
            });
            $('#<%=txtAcceptQty.ClientID %>').val(acceptQty.toFixed(2));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //// Get Transfer Qty
        function fncGetTransferQty(source) {
            var rowObj;
            try {
                setTimeout(function () {
                    rowObj = $(source).parent().parent();
                    glAcceptQty = rowObj.find('td input[id*="txtAcceptQty"]').val();
                    glTotalQty = $('#<%=txtAcceptQty.ClientID %>').val();
                    rowObj.find('td input[id*="txtAcceptQty"]').select();
                }, 50);



            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function disableFunctionKeys(e) {
            try {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 115) {
                        if (saveStatus == false) {
                            saveStatus = true;
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkAccept', '')
                            e.preventDefault();
                            return false;
                        }
                    }
                    else if (e.keyCode == 119) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkCancel', '');
                        e.preventDefault();
                        return false;
                    }
                    e.preventDefault();
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        var saveStatus = false;
        function fncAccept() {
            try {
                if (saveStatus == false) {
                    saveStatus = true;
                    // btnSave.ClientID .click();
                }
            }
            catch (ex) {
                fncToastError(ex.message);
            }
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Transfer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page"><%=Resources.LabelCaption.lbl_Transfer_Accept%> </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="top-purchase-container" style="padding-left: 10px;">
            <div class="col-md-6">
                <div class="control-group-split">
                    <div class="control-group-left">
                        <div class="label-left">
                            <asp:Label ID="Label4" runat="server" Text="Transfer NO"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtTRNO" runat="server" CssClass="form-control-res"
                                ReadOnly="True"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-right">
                        <div class="label-left">
                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lbl_FromLocation %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:DropDownList ID="sFromLocation" runat="server" CssClass="form-control-res"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="control-group-split">
                    <div class="control-group-right">
                        <div class="label-left">
                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lbl_Data %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtDate" runat="server" CssClass="form-control-res"
                                ReadOnly="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="control-group-left" id="divScanBarcode" style="display: block; background-color: darkseagreen; margin-top: 0px;">
                        <div style="font-weight: bold; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif">
                            <div style="width: auto;">
                                <asp:Label ID="Label7" runat="server" Text="Scan Barcode :" Style="float: left;"></asp:Label>
                            </div>
                            <div style="width: auto;">
                                <asp:TextBox ID="txtBarcodeScan" onkeydown="return fncScanBarcode(event);" runat="server" Style="float: left;" BackColor="Snow" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-group-full">
            <asp:UpdatePanel ID="upTranAcc" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="over_flowhorizontal" style="display: none; height: 200px;">
                        <table rules="all" border="1" id="tblPOSummary" runat="server" class="fixed_header transfer">
                            <tr>
                                <th>S.No</th>
                                <th>Item Code</th>
                                <th>Description</th>
                                <th>Transfer Qty</th>
                                <th>Accept Qty</th>
                                <th id="thbatchNo">Batch No</th>
                                <th>MRP</th>
                                <th>Selling Price</th>
                                <th>Seq.No</th>
                            </tr>
                        </table>
                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 320px; width: 1356px; background-color: aliceblue;">
                            <asp:GridView ID="TrasnferAcceptGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                ShowHeaderWhenEmpty="True" ShowHeader="false"
                                CssClass="transfer">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="Records Not Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                    <asp:BoundField DataField="InventoryCode" HeaderText="Inventory Code"></asp:BoundField>
                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                    <asp:BoundField DataField="TransferQty" HeaderText="Transfer Qty"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Accept Qty">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAcceptQty" runat="server" Text='<%# Eval("AcceptQty") %>'
                                                onkeypress=" return isNumberKeyWithDecimalNew(event);" onkeydown=" return fncSetFocustoNextRow(event,this);"
                                                onchange="fncAddAcceptQty(this);"
                                                CssClass="gridTransfer-textbox"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="AcceptQty" HeaderText="Accept Qty"></asp:BoundField>--%>
                                    <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                    <asp:BoundField DataField="Mrp" HeaderText="Mrp" />
                                    <asp:BoundField DataField="sellingprice" HeaderText="S.Price" />
                                    <asp:BoundField DataField="SeqNo" HeaderText="SeqNo"></asp:BoundField>
                                    <asp:BoundField DataField="AcceptQty" HeaderText="ReminQty" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>

                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="container-right-grn" style="width: 100%; margin-left: 0%; height: auto; padding: 5px; margin-bottom: 5px;display:none;" id="HideFilter_ContainerRight" runat="server">
                <div class="control-group-single-res" id="dynamicInput1">
                    <table id="grdAttributeSize" cellspacing="0" rules="all" border="1">
                    </table>
                    <table id="tblInventory" cellspacing="0" rules="all" border="1" style="display: none;">
                        <thead>
                            <tr>
                                <th scope="col">Size</th>
                                <th scope="col">InventoryCode </th>
                                <th scope="col">Description</th>
                                <th scope="col">Stock</th>
                                <th scope="col">MRP</th>
                                <th scope="col">SellingPrice</th>
                                <th scope="col">StyleCode</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <table id="TempgrdItemDetails" cellspacing="0" rules="all" border="1" style="display: none;">
                        <thead>
                            <tr>
                                <th scope="col">S.No</th>
                                <th scope="col">StyleCode</th>
                                <th scope="col">ItemCode</th>
                                <th scope="col">Description</th>
                                <th scope="col">Qty</th>
                                <th scope="col">MRP</th>
                                <th scope="col">SellingPrice</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="button-contol" id="divAddButtons" style="display: none; margin-top: 10px;">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-red" OnClientClick="fncAddTransDetail(); return false;"><i class="icon-play"></i>Add Item</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="return fncClear();" class="button-red"><i class="icon-play"></i>Clear</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-right-grn" style="width: 100%; margin-left: 0%; height: 450px; padding: 5px;" id="div1" runat="server">
                <div class="container-right-grn" style="width: 58%; margin-left: 0%; margin-top: 0%; height: auto; padding: 5px;" id="divTransDetail" runat="server">
                    <table class="divtblItemdetail" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">S.No</th>
                                <th scope="col">StyleCode</th>
                                <th scope="col">ItemCode</th>
                                <th scope="col">Description</th>
                                <th scope="col">ScanQty</th>
                                <th scope="col">Qty</th>
                                <th scope="col">MRP</th>
                                <th scope="col">Price</th>
                                    <th style ="display:none" scope="col">ScanQtyBackup</th>
                                <%--<th scope="col" class="hiddencol">SDAmount</th>
                            <th scope="col" class="hiddencol">DiscAmt</th>
                            <th scope="col" class="hiddencol">VendorCode</th>--%>
                            </tr>
                        </thead>
                    </table>

                    <div style="overflow: auto; height: 410px;">
                        <table id="grdItemDetails" class="divtblItemdetail" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr class="hiddencol">
                                    <th scope="col">S.No</th>
                                    <th scope="col">StyleCode</th>
                                    <th scope="col">ItemCode</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">ScanQty</th>
                                    <th scope="col">Qty</th>
                                    <th scope="col">MRP</th>
                                    <th scope="col">SellingPrice</th>
                                    <th style ="display:none" scope="col">ScanQtyBackup</th>
                                    <%--  <th scope="col">SDAmt</th>
                                <th scope="col">DiscAmt</th>
                                <th scope="col">VendorCode</th>--%>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="container-right-grn" style="width: 40%; margin-left: 2%; margin-top: 0%; height: auto; padding: 20px;" id="div2" runat="server">
                    <div class="top-purchase-contadiner">
                        <div class="top-purchase-left-contaiasdner">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalItems %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtTotalItems" runat="server" CssClass="form-control-res"
                                            ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalQty %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtTotalQty" runat="server" CssClass="form-control-res"
                                            ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="top-purchase-left-containaser">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_AcceptQty %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAcceptQty" Text="0.00" runat="server" CssClass="form-control-res"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="control-container">
                        <asp:UpdatePanel ID="upAccept" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkAccept" runat="server" class="button-red" OnClientClick="return ValidateForm();" OnClick="lnkAccept_Click" Text='Accept(F4)'></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkCancel" runat="server" class="button-red"
                                        OnClick="lnkCancelAll_Click" Text='Cancel(F8)'></asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>



        <div class="container-group-full" style="display: none">

            <div id="TransferInAccept">
                <p>
                    <%=Resources.LabelCaption.Alert_Save%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="display_none">
        <asp:HiddenField ID="hiddenGRNNo" runat="server" Value="" />
        <asp:HiddenField ID="GRNXmldata" runat="server" Value="" />
        <asp:HiddenField ID="hidAcceptQty" runat="server" Value="0" />
        <asp:HiddenField ID="hidTotalQty" runat="server" Value="0" />
        <%-- <asp:Button ID="btnSave" runat="server" OnClick="lnkAccept_Click" />--%>
    </div>
</asp:Content>
