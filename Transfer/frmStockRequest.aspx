﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmStockRequest.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmStockRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        $(function () {
            SetAutoComplete();
        });

        $(function () {

            $("#<%= DropDownVendor.ClientID %>").change(function () {
                var status = this.value;
                setTimeout(function () {
                    $('#<%=txtReqQty.ClientID %>').focus().select();
                }, 100);
                // if (status == "1")
                //   $("#icon_class, #background_class").hide(); // hide multiple sections
            });

        });

        //                $(function () {
        //                    $('#<%=txtReqQty.ClientID %>').focusout(function () {
        //                        fncCalcTotalAmount();
        //                    });
        //                });

        //Get Calc Total amount
        function fnctxtRequestQty(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    if ($('#<%=txtReqQty.ClientID %>').val() <= 0) {

                        alert('Invalid Qty !');
                        $('#<%=txtReqQty.ClientID %>').focus().select();

                        return false;
                    }

                    $('#<%=lnkAdd.ClientID %>').focus().select();
                    return false;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        $(function () {
            $("#<%= txtRequestdate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

            if ($("#<%= txtRequestdate.ClientID %>").val() === '') {
                $("#<%= txtRequestdate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

        });

        //        function reloadPage() {
        //            window.location.reload();
        //            // window.location.href="~/Purchase/frmPurchaseReturnList.aspx" ;
        //        }

        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function SetAutoComplete() {

            $("[id$=txtInventoryCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Transfer/frmStockRequest.aspx/GetInventoryDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valCode: item.split('|')[1],
                                    valDescription: item.split('|')[2],
                                    valMrp: item.split('|')[3],
                                    valSellprice: item.split('|')[4],
                                    valCost: item.split('|')[5],
                                    valBatch: item.split('|')[6],
                                    valQOH: item.split('|')[7],
                                    valMinQty: item.split('|')[8],
                                    valReorder: item.split('|')[9]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {

                    //alert(i.item.valCost);
                    $('#<%=txtInventoryCode.ClientID %>').val($.trim(i.item.valCode));
                    $('#<%=txtdescription.ClientID %>').val($.trim(i.item.valDescription));
                    $('#<%=txtMrp.ClientID %>').val($.trim(i.item.valMrp));
                    $('#<%=txtCost.ClientID %>').val($.trim(i.item.valCost));
                    $('#<%=txtPrice.ClientID %>').val($.trim(i.item.valSellprice));
                    $('#<%=txtStock.ClientID %>').val($.trim(i.item.valQOH));
                    $('#<%=txtMinQty.ClientID %>').val($.trim(i.item.valMinQty));
                    $('#<%=txtReorderQty.ClientID %>').val($.trim(i.item.valReorder));

                    $("#HidenIsBatch").val($.trim(i.item.valBatch));
                    //alert($("#HidenIsBatch").val());
                    if ($("#HidenIsBatch").val() == 'True')
                        fncGetBatchDetail_Master(i.item.valCode);

                    $('#<%=txtReqQty.ClientID %>').focus().select();
                    return false;
                },
                minLength: 2
            });
        }

        function fncVendorDetails(itemCode) {
            try {
                //var sInvCode = $('#<%=txtInventoryCode.ClientID%>').val();
                $.ajax({
                    type: "POST",
                    url: "frmPurchaseReturnRequest.aspx/GetVendorDetails",
                    data: "{'sInvCode':'" + itemCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var ddlVendorsrc = $('#<%=DropDownVendor.ClientID%>');
                        //ddlBactno.empty().append('<option selected="selected" Value="0">Please select</option>');                        
                        $("select[id$=DropDownVendor] > option").remove();
                        //alert(msg.d.length);
                        if (msg.d.length > '1')
                            ddlVendorsrc.empty().append('<option selected="selected" Value="0">Please select</option>');
                        $.each(msg.d, function () {
                            ddlVendorsrc.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                        $('#<%=DropDownVendor.ClientID %>').trigger("liszt:updated");
                        if (msg.d.length > '1')
                            $('#<%=DropDownVendor.ClientID %>').trigger("liszt:open");

                    },
                    error: function (data) {
                        alert('Something Went Wrong')
                    }
                });
            }
            catch (err) {
                alert(err.Message);
            }
        }

        //        //Get Vendor Code from DropDownList
        //        function fncVendorSelect() {
        //            try { 
        //                var selectedText = $('#<%=DropDownVendor.ClientID%>').find("option:selected").text();
        //                var selectedValue = $('#<%=DropDownVendor.ClientID%>').val();
        //                alert("Selected Text: " + selectedText + " Value: " + selectedValue);
        //                $("select[id$=DropDownVendor] > option").remove();
        //                $('#<%=DropDownVendor.ClientID%>').append($("<option></option>").val(selectedValue).html(selectedText));

        //                $("#divVendor").dialog('close');
        //                return false;
        //            }
        //            catch (err) {
        //                alert(err.Message);
        //            }
        //        }

        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost, stock) {
            try {
                $('#<%=txtMrp.ClientID%>').val(mrp);
                $('#<%=txtCost.ClientID%>').val(Basiccost);
                $('#<%=txtBatchNo.ClientID%>').val(batchno);
                $('#<%=txtBatchStock.ClientID%>').val(stock);
                $('#<%=txtPrice.ClientID%>').val(sprice);

                //fncVendorDetails($('#<%=txtInventoryCode.ClientID%>').val());
                //ShowPopupBatch("Select Vendor");
            }
            catch (err) {
                alert(err.message);
            }
        }

        function ShowPopupBatch(name) {
            $("#divVendor").dialog({
                appendTo: 'form:first',
                closeOnEscape: false,
                title: name,
                width: 365,
                //                buttons: {
                //                    PRINT: function () {
                //                        $(this).dialog('close');
                //                    }
                //                },
                modal: true

            });
        }

        $('body').on('focus', '.chzn-container-single input', function () {
            if (!$(this).closest('.chzn-container').hasClass('chzn-container-active'))
                $(this).closest('.chzn-container').trigger('mousedown');
            //or use this instead
            //$('#select').trigger('liszt:open');  
        });

        //  var saveStatus = false;
        function fncValidateSave() {
            var Show = '';
            $('#<%=lnkSave.ClientID %>').css("display", "none");
            var totalRows = $("#<%=grdStockRequest.ClientID %> tr").length;
            //alert(totalRows);
            if (totalRows <= 1) {

                //alert('Please Add any Item to Proceed !');
                Show = 'Please Add any Item to Proceed !';
                $('#<%=txtInventoryCode.ClientID %>').focus().select();

            }
            if ($('#<%=txtTotalQty.ClientID %>').val() == 0) {
                Show = Show + '<br /> Total Qty is cannot be zero !';

                $('#<%=txtInventoryCode.ClientID %>').focus().select();
                // return false;
            }
            if ($('#<%=LocationDropdown.ClientID %>').val() == '') {

                $('#<%=LocationDropdown.ClientID %>').trigger("liszt:open");
                //$('#<%=LocationDropdown.ClientID %>').trigger('mousedown');
                Show = Show + '<br /> Please select Location to Proceed !';
                //return false;
            }
            if ($('#<%=WarehouseDropdown.ClientID %>').val() == '') {

                $('#<%=WarehouseDropdown.ClientID %>').trigger("liszt:open");
                Show = Show + '<br /> Please select Warehouse to Proceed !';
                //return false;
            }
            if (Show != '') {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
                ShowPopupMessageBox(Show);
                return false;
            }
            else
                <%--  $('#<%=btnSave.ClientID%>').click();--%>
                return true;              
        }

        function fncGetInventoryCode_onEnter(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                var Itemcode, vendorcode;
                if (keyCode == 13) {
                    return false;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        function AddbtnValidation() {

            try {

                if ($('#<%=txtInventoryCode.ClientID %>').val() == '') {

                    alert('Item Code should not be Empty !');
                    $('#<%=txtInventoryCode.ClientID %>').focus().select();
                    return false;

                }

                if ($('#<%=txtReqQty.ClientID %>').val() <= 0) {
                    alert('Invalid Request Qty !');
                    $('#<%=txtReqQty.ClientID %>').focus().select();

                    return false;
                }

                var totalRows = $("#<%=grdStockRequest.ClientID %> tr").length;

                $("#<%=grdStockRequest.ClientID%> tr").each(function () {
                    if (!this.rowIndex) return;
                    var gitemcode = $(this).find("td.Itemcode").html();
                    var gBatchNo = $(this).find("td.BatchNo").html();
                    //alert(gitemcode + '-' + gBatchNo);
                    if ($.trim(gitemcode) == $.trim($('#<%=txtInventoryCode.ClientID %>').val()) && $.trim(gBatchNo) == $.trim($('#<%=txtBatchNo.ClientID %>').val())) {
                        var confirm_value = document.createElement("INPUT");
                        confirm_value.type = "hidden";
                        confirm_value.name = "confirm_value";
                        if (confirm("Item Already exists!. Do you want to update Return Qty?")) {
                            confirm_value.value = "Yes";

                        } else {
                            confirm_value.value = "No";
                        }
                        document.forms[0].appendChild(confirm_value);
                    }
                    else if ($.trim(gitemcode) == $.trim($('#<%=txtInventoryCode.ClientID %>').val())) {
                        var confirm_value = document.createElement("INPUT");
                        confirm_value.type = "hidden";
                        confirm_value.name = "confirm_value";
                        if (confirm("Item Already exists!. Do you want to update Return Qty?")) {
                            confirm_value.value = "Yes";

                        } else {
                            confirm_value.value = "No";
                        }
                        document.forms[0].appendChild(confirm_value);
                    }

                });

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        //Get Calc Total amount
        function fnctxtReturnQty(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    if ($('#<%=txtReqQty.ClientID %>').val() <= 0) {

                        alert('Invalid Weight Qty !');
                        $('#<%=txtReqQty.ClientID %>').focus().select();

                        return false;
                    }

                    //fncCalcTotalAmount();

                    $('#<%=lnkAdd.ClientID %>').focus().select();
                    return false;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncCalcTotalAmount() {

            try {

                var ReturnQty = $('#<%=txtReqQty.ClientID%>').val();
                var Cost = $('#<%=txtCost.ClientID%>').val();
                var Amount;
                Amount = ReturnQty * Cost;
                $('#<%=txtReqQty.ClientID%>').val(Amount.toFixed(2));

            }
            catch (err) {
                alert(err.Message);
            }
        }

        function ClearTextBox() {
            //$('input[type="text"]').val('');
            $('#<%=txtInventoryCode.ClientID %>').val('');
            $('#<%=txtdescription.ClientID %>').val('');
            $('#<%=txtBatchNo.ClientID %>').val('');
            $('#<%=txtReqQty.ClientID %>').val('0');
            $('#<%=txtMrp.ClientID %>').val('0');
            $('#<%=txtCost.ClientID %>').val('0');
            $('#<%=txtPrice.ClientID %>').val('0');
            $('#<%=txtStock.ClientID %>').val('0');
            $('#<%=txtBatchStock.ClientID %>').val('0');
            $('#<%=txtMinQty.ClientID %>').val('0');
            $('#<%=txtReorderQty.ClientID %>').val('0');
            //$('#<%=txtTotalQty.ClientID %>').val('0');

            $("#HiddenVendorName").val('');
            $('#HiddenVendorCode').val('');

            $("#<%= txtRequestdate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");

            $('#<%=txtInventoryCode.ClientID %>').focus().select();
            return false;
        }

        function ClearForm() {
            //$('input[type="text"]').val('');
            $('#<%=txtInventoryCode.ClientID %>').val('');
            $('#<%=txtdescription.ClientID %>').val('');
            $('#<%=txtBatchNo.ClientID %>').val('');
            $('#<%=txtReqQty.ClientID %>').val('0');
            $('#<%=txtMrp.ClientID %>').val('0');
            $('#<%=txtCost.ClientID %>').val('0');
            $('#<%=txtStock.ClientID %>').val('0');

            $("table[id$='grdStockRequest']").html("");

            $("#HiddenVendorName").val('');
            $('#HiddenVendorCode').val('');

            $("#<%= txtRequestdate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");

            $('#<%=txtInventoryCode.ClientID %>').focus().select();
            return false;
        }

        $(function () {
            $("[id*=grdStockRequest]").on('click', 'td > img', function (e) {

                var r = confirm("Do you want to Delete?");
                if (r === false) {
                    return false;
                }
                $(this).closest("tr").remove();
                $('#<%=txtInventoryCode.ClientID %>').focus().select();
                return false;
            });

        });

        //Show Stock Request Save  Dailog
        function fncShowStockRequestDailog() {
            try {                
                $('#<%=lblStockRequest.ClientID %>').text('<%=Resources.LabelCaption.msg_StockReqSave%>');
                $("#saveStockRequest").dialog({
                    resizable: false,
                    height: 130,
                    width: 325,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" id="form1">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../Masters/frmMain.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Transfer</a><i class="fa fa-angle-right"></i></li>
                <li><a  href="../Transfer/frmStockRequestView.aspx">Stock Request</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Stock Request Creation</li>
            </ul>
        </div>
        <div class="container-group-pc">
            <div class="right-container-top-distribution">
                <div class="right-container-bottom-detail">
                    <div class="control-group-split">
                        <div class="control-group-left-Distb">
                            <div class="label-left-pc">
                                <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblRequestdate %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtRequestdate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="Label19" runat="server" Text='<%$ Resources:LabelCaption,lbl_RequestNo %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtRequestNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="Label18" runat="server" Text='<%$ Resources:LabelCaption,lbl_location %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:DropDownList ID="LocationDropdown" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Warehouse %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:DropDownList ID="WarehouseDropdown" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <%--   <div class="control-group-left-Distribut-txt">
                            <div class="label-left-pc">
                                <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>--%>
                        <div class="control-group-left-Distb">
                            <div class="label-left-pc">
                                <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_totalQty %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtTotalQty" runat="server" Text="0" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb" style="display: none">
                            <div class="label-left-pc">
                                <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_totalAmount %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtTotalAmt" runat="server" Text="0" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-button" style="margin-left: 20px">
                            <asp:LinkButton ID="lnkSave" runat="server" CssClass="button-red" OnClientClick="return fncValidateSave()"
                                Text="Create Stock Request" OnClick="lnkSave_Click">  </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="container-horiz-top-purreturn">
                    <div class="right-container-top-header">
                        Stock Request
                    </div>
                    <div class="right-container-top-detail">
                        <div class="GridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="grid-overflow-height" id="HideFilter_GridOverFlow" runat="server">
                                        <asp:GridView ID="grdStockRequest" runat="server" AutoGenerateColumns="false" PageSize="14"
                                            ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgn" OnRowDataBound="grdStockRequest_RowDataBound"
                                            OnRowDeleting="grdStockRequest_RowDeleting">
                                            <PagerStyle CssClass="pshro_text" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <Columns>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />
                                                <asp:BoundField ItemStyle-CssClass="Itemcode" HeaderText="Item Code" DataField="ItemCode"></asp:BoundField>
                                                <asp:BoundField HeaderText="Description" DataField="Description"></asp:BoundField>
                                                <asp:BoundField ItemStyle-CssClass="BatchNo" HeaderText="BatchNo" DataField="BatchNo"></asp:BoundField>
                                                <asp:BoundField HeaderText="Qty" DataField="ReqQty"></asp:BoundField>
                                                <asp:BoundField HeaderText="Price" DataField="Price"></asp:BoundField>
                                                <asp:BoundField HeaderText="Qty OnHand" DataField="QtyOnHand"></asp:BoundField>
                                                <asp:BoundField HeaderText="Mrp" DataField="Mrp"></asp:BoundField>
                                                <asp:BoundField HeaderText="Cost" DataField="Cost"></asp:BoundField>
                                                <asp:BoundField HeaderText="Min Qty" DataField="MinQty"></asp:BoundField>
                                                <asp:BoundField HeaderText="Reorder Qty" DataField="ReOrderLevel"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                PageButtonCount="5" Position="Bottom" />
                                            <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-horiz-top-purreturn">
                        <div class="right-container-bottom-header">
                        </div>
                        <div class="right-container-bottom-detail">
                            <div class="control-group-split">
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblitemcode %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtInventoryCode" runat="server" CssClass="form-control-res" onkeydown=" return fncGetInventoryCode_onEnter(event);"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="control-group-left-Description-sm">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtdescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label3" runat="server" Text="W Qty"></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtWqty" runat="server" onkeypress="return isNumberKey(event)" onkeydown="return fnctxtReturnQty(event)"
                                            CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label4" runat="server" Text="W Unit"></asp:Label>
                                    </div>
                                    <div class="label-right-small">
                                        <asp:TextBox ID="txtWUnit" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label5" runat="server" Text="Req Qty"></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtReqQty" runat="server" Text="0" onkeypress="return isNumberKey(event)"
                                            onkeydown="return fnctxtRequestQty(event)" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_mrp %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtMrp" runat="server" Text="0" onkeypress="return isNumberKey(event)"
                                            CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-Description-vendor" visible="false">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label10" Visible="false" runat="server" Text='<%$ Resources:LabelCaption,lbl_vendor %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc" visible="false">
                                        <asp:DropDownList ID="DropDownVendor" Visible="false" runat="server" CssClass="form-control-res">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Price %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtPrice" runat="server" Text="0" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label8" runat="server" Text="QtyOnHand"></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtStock" runat="server" Text="0" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Batchno %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtBatchNo" runat="server" Text="" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label20" runat="server" Text='<%$ Resources:LabelCaption,lbl_stock %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtBatchStock" runat="server" Text="" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lbl_cost %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtCost" runat="server" Text="" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label12" runat="server" Text="Min Qty"></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtMinQty" runat="server" Text="" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-small">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label14" runat="server" Text="ReOrder Qty"></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtReorderQty" runat="server" Text="" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkAdd" runat="server" CssClass="button-red" OnClientClick="return AddbtnValidation()"
                                    Text='<%$ Resources:LabelCaption,btnadd %>' OnClick="lnkAdd_Click"> </asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" CssClass="button-red" OnClientClick="return ClearTextBox()"
                                    Text='<%$ Resources:LabelCaption,btnclear %>'> Clear </asp:LinkButton>
                            </div>
                            <div class="control-button">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="HidenIsBatch" runat="server" ClientIDMode="Static" Value="" />
            <asp:HiddenField ID="hidenExists" runat="server" ClientIDMode="Static" Value="" />
            <asp:HiddenField ID="HiddenVendorCode" runat="server" ClientIDMode="Static" Value="" />
            <asp:HiddenField ID="HiddenVendorName" runat="server" ClientIDMode="Static" Value="" />

            <%--<asp:Button ID="btnSave" runat="server" OnClick="lnkSave_Click" />--%>
            <div class="container-group-pc" id="divVendor">
                <div class="label-right-pc">
                    <div class="label-left-pc">
                        <%--<asp:DropDownList ID="DropDownVendord" Style="width: 330px" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hiddencol">
        <div id="saveStockRequest">
            <div>
                <asp:Label ID="lblStockRequest" runat="server"></asp:Label>
            </div>
            <div class="dialog_center">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                    OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
