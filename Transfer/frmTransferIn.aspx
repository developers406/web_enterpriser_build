﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmTransferIn.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmTransferIn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .transfer td:nth-child(1), .transfer th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(2), .transfer th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(2) {
            text-align: center;
        }

        .transfer td:nth-child(3), .transfer th:nth-child(3) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(3) {
            text-align: center;
        }

        .transfer td:nth-child(4), .transfer th:nth-child(4) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(4) {
            text-align: center;
        }

        .transfer td:nth-child(5), .transfer th:nth-child(5) {
            min-width: 175px;
            max-width: 175px;
        }

        .transfer td:nth-child(6), .transfer th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
        }

        .transfer td:nth-child(7), .transfer th:nth-child(7) {
            min-width: 125px;
            max-width: 125px;
        }

        .transfer td:nth-child(8), .transfer th:nth-child(8) {
            min-width: 150px;
            max-width: 150px;
        }

        .transfer td:nth-child(9), .transfer th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(10), .transfer th:nth-child(10) {
            min-width: 107px;
            max-width: 107px;
        }

        .transfer td:nth-child(11), .transfer th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(12), .transfer th:nth-child(12) {
            min-width: 150px;
            max-width: 150px;
        }

        .transfer td:nth-child(13), .transfer th:nth-child(13) {
            min-width: 150px;
            max-width: 150px;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'TransferIn');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "TransferIn";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtFromDate.ClientID %>, #<%= txtToDate.ClientID %>").on('keydown', function () {
                return false;

            });
        });
    </script>
    <script type="text/javascript">
        function pageLoad() {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });

            //function confirmation() {
            //    if (confirm('Do u want Approve ?')) {
            //        return true;
            //    } else {
            //        return false;
            //    }
            //} 
        }
    </script>
    <script type="text/javascript">
        function fncPrintValidation() {
            try {
                if ($("#<%=hidTranNo.ClientID %>").val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.msg_select%>');
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncAcceptValidation(source) {
            var rowobj;
            try {
                  if ($("#<%=hidSavebtn.ClientID %>").val() == "N0") {
                      fncToastError("You have no permission to Accept this Transferin.");
                      return false;
                  }
                rowobj = $(source).parent().parent();
                if ($("#<%=hidTransferSeq.ClientID %>").val() == "Y") {
                    if ($("#<%=hidFirstTranNo.ClientID %>").val() != $.trim($("td", rowobj).eq(4).text())) {
                        fncToastInformation("Please Accept First Transfer No - " + $("#<%=hidFirstTranNo.ClientID %>").val());
                        return false;
                    }
                }
                if ($('#<%=ddlToLocStatus.ClientID %> option:selected').val() == "Accepted"
                  || $('#<%=ddlFromLocStatus.ClientID %> option:selected').val() == "Revoked"
                    || $.trim($("td", rowobj).eq(8).text()) == "Revoked") {
                    ShowPopupMessageBox("Please Click on View");

                    return false;
                }

                else {
                    return true;
                }


        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    /// Grid View Search
    function fncTransferNoSearch(itemObj) {
        var rowObj, SearchData;
        try {
            SearchData = $(itemObj).val().trim();
            SearchData = SearchData.toLowerCase();
            if (SearchData == "") {
                $("#ContentPlaceHolder1_gvPO tbody").children().removeClass("display_none");
            }
            else {
                $("#ContentPlaceHolder1_gvPO tbody").children().each(function () {
                    rowObj = $(this);
                    if ($.trim($("td", rowObj).eq(4).text()).toLowerCase().indexOf(SearchData) < 0) {
                        rowObj.addClass("display_none");
                    }
                    else {
                        rowObj.removeClass("display_none");
                    }

                });
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    $(document).ready(function () {
        $(document).on('keydown', disableFunctionKeys);
    });

    function disableFunctionKeys(e) {
        try {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 118) {
                    __doPostBack('ctl00$ContentPlaceHolder1$LinkFetch', '');
                    e.preventDefault();
                    return false;
                }
                e.preventDefault();
                return false;
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }


    function fncViewClick() {
        try {
            if ($('#<%=ddlToLocStatus.ClientID %> option:selected').val() == "Accepted") {
                    return true;
            }
             else if ($("#<%=hidViewbtn.ClientID %>").val() == "V0") {
                      fncToastError("You have no permission to View this Transferin.");
                      return false;
             }
             else {
                    ShowPopupMessageBox("Please Click on Accept");
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncToast(msg) {
            var obj = {};
            obj = msg.split('-');
            ShowPopupMessageBox("Please Activate this Item. ItemCode - " + obj[0] + ". BatchNo - " + obj[1] + ".");

            return false;
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Transfer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Transfer In </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
            <div class="transferIn_PendingStatus">
                <asp:Label ID="lblTotalPending" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalPendingTransfer %>'></asp:Label>
                <asp:Label ID="lblTotalPendingQty" runat="server" Text="0"></asp:Label>
            </div>
        </div>
        <div class="container-group-full">
            <div class="transfer-order-header" style="background-color: #e6e6e6">
                <div class="control-container" style="float: right; clear: both; display: table; width: 30%">
                    <%-- <div class="control-button">
                        <asp:LinkButton ID="lnkAccept" runat="server" class="button-blue" OnClick="lnkAccept_Click" Text="Accept(F5)" ></asp:LinkButton>
                    </div>--%>
                    <div class="control-button float_right">
                        <asp:UpdatePanel ID="upfetch" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:LinkButton ID="LinkFetch" runat="server" class="button-blue" OnClick="lnkFetch_Click" Text="Refresh(F7)"></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

                <div class="transferIn">
                    <div class="transferIn_btn">
                        <div>
                            <div class="tran_lbl">
                                <asp:Label ID="Label2" runat="server" Text="TransferNo"></asp:Label>
                            </div>
                            <div class="tran_txt">
                                <asp:TextBox ID="txtTranNo" runat="server" CssClass="form-control-res" oninput="fncTransferNoSearch(this);"></asp:TextBox>
                                <%-- <asp:DropDownList ID="ddlTransferNo" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            </div>
                        </div>
                        <div>
                            <div class="tran_lbl">
                                <asp:Label ID="Label6" runat="server" Text="From Location"></asp:Label>
                            </div>
                            <div class="tran_txt">
                                <asp:DropDownList ID="ddlOutLet" runat="server" CssClass="form-control-res">
                                    <asp:ListItem Value="ALL"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="transferIn_btn">
                        <div>
                            <div class="tran_lbl">
                                <asp:Label ID="Label1" runat="server" Text="From Loc.Status"></asp:Label>
                            </div>
                            <div class="tran_txt">
                                <asp:DropDownList ID="ddlFromLocStatus" runat="server" CssClass="form-control-res">
                                    <asp:ListItem Value="Transfered"></asp:ListItem>
                                    <asp:ListItem Value="Revoked"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div class="tran_lbl">
                                <asp:Label ID="Label26" runat="server" Text="From Date"></asp:Label>
                            </div>
                            <div class="tran_txt">
                                <asp:TextBox ID="txtFromDate" runat="server" Style="padding-left: 8px;" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="transferIn_btn">
                        <div>
                            <div class="tran_lbl">
                                <asp:Label ID="Label3" runat="server" Text="To Loc.Status"></asp:Label>
                            </div>
                            <div class="tran_txt">
                                <asp:DropDownList ID="ddlToLocStatus" runat="server" AutoPostBack="true" CssClass="form-control-res"
                                    OnSelectedIndexChanged="ddlToLocationStatus_SelectedIndexChanged">
                                    <asp:ListItem Value="Not Received"></asp:ListItem>
                                    <asp:ListItem Value="Checking"></asp:ListItem>
                                    <asp:ListItem Value="Accepted"></asp:ListItem>
                                    <asp:ListItem Value="Partially Received"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div class="tran_lbl">
                                <asp:Label ID="Label32" runat="server" Text="To Date"></asp:Label>
                            </div>
                            <div class="tran_txt">
                                <asp:TextBox ID="txtToDate" runat="server" Style="padding-left: 8px;" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--    <div class="control-container" style="float: left; display: table; width: 30%">
                        <div class="control-group-split">
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label2" runat="server" Text="TransferNo"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlTransferNo" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label6" runat="server" Text="OutletName"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlOutLet" runat="server" CssClass="form-control-res">
                                        <asp:ListItem Value="ALL"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="transfer-option-container" style="width: 40%;">
                        <div class="control-group-split">
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server" Text="From Loc.Status"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlFromLocStatus" runat="server" CssClass="form-control-res">
                                        <asp:ListItem Value="Transfered"></asp:ListItem>
                                        <asp:ListItem Value="Revoked"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-right">
                                <div class="label-left">
                                    <asp:Label ID="Label3" runat="server" Text="To Loc.Status"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlToLocStatus" runat="server" AutoPostBack="true" CssClass="form-control-res"
                                        OnSelectedIndexChanged="ddlToLocationStatus_SelectedIndexChanged">
                                        <asp:ListItem Value="Not Received"></asp:ListItem>
                                        <asp:ListItem Value="Checking"></asp:ListItem>
                                        <asp:ListItem Value="Accepted"></asp:ListItem>
                                        <asp:ListItem Value="Partially Received"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="transfer-option-container" style="width: 40%;">
                        <div class="control-group-split">
                             <asp:UpdatePanel ID="update" runat="server" UpdateMode="Always">
                            <ContentTemplate> 
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label26" runat="server" Text="From Date"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtFromDate" runat="server" Style="padding-left: 8px;" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-right">
                                <div class="label-left">
                                    <asp:Label ID="Label32" runat="server" Text="To Date"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtToDate" runat="server" Style="padding-left: 8px;" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                         </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                     <div class="transferbutton-option-container" style="width: 10%; float: right">
                        <div class="control-group-split">
                            <div class="control-group-middle">
                                <div class="control-button">
                                    <div class="label-right">
                                        <asp:UpdatePanel ID="upfetch" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                        <asp:LinkButton ID="LinkFetch" runat="server" class="button-blue" OnClick="lnkFetch_Click">Fetch</asp:LinkButton>
                                         </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                


                <div class="transferwarhouse-option-container" style="width: 50%; float: left">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Visible="false" Text="Warhouse"></asp:Label>
                            </div>
                            <div class="label-right" id="ddl">
                                <asp:DropDownList ID="ddlWarehouse" Visible="false" runat="server" CssClass="form-control-res">
                                    <asp:ListItem Value="ALL"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="transfer-option-container" style="width: 20%;">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="TransferNo"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlTransferNo" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="OutletName"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlOutLet" runat="server" CssClass="form-control-res">
                                    <asp:ListItem Value="ALL"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>--%>
        </div>
        <div class="over_flowhorizontal">
            <table rules="all" border="1" id="tblPOSummary" runat="server" class="fixed_header transfer">
                <tr>
                    <th>S.No</th>
                    <th>Accept</th>
                    <th>Print</th>
                    <th>View</th>
                    <th>Tran No</th>
                    <th>Tran Date</th>
                    <th>Frm Loc</th>
                    <th>To Loc</th>
                    <th>Frm.Loc.Status</th>
                    <th>To.Loc.Status</th>
                    <th>Transfer Type</th>
                    <th>Create User</th>
                    <th>Create Date</th>
                </tr>
            </table>
            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 400px; width: 1355px; background-color: aliceblue;">
                <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="gvPO" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            ShowHeaderWhenEmpty="True" ShowHeader="false"
                            CssClass="pshro_GridDgn transfer">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label3" runat="server" Text="Records Not Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                            <PagerStyle CssClass="pshro_text" />
                            <Columns>
                                <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                <asp:TemplateField HeaderText="Accept">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnAccept" runat="server" ImageUrl="~/images/accept.png"
                                            ToolTip="Click here to Accept" OnClick="lnkAccept_Click" OnClientClick=" return fncAcceptValidation(this);" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Print">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnPrint" runat="server" ImageUrl="~/images/print1.png"
                                            ToolTip="Click here to Print" OnClick="lnkPrint_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <asp:Button ID="btnTranView" runat="server" Text='<%$ Resources:LabelCaption,btn_View%>'
                                            OnClientClick="return fncViewClick();" OnClick="lnkView_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="TransferNo" HeaderText="Transfer No"></asp:BoundField>
                                <asp:BoundField DataField="CurTransferDate" HeaderText="Transfer Date"></asp:BoundField>
                                <asp:BoundField DataField="FromLocation" HeaderText="From Location"></asp:BoundField>
                                <asp:BoundField DataField="ToLocation" HeaderText="To Location"></asp:BoundField>
                                <asp:BoundField DataField="FromLocationStatus" HeaderText="From Loc.Status"></asp:BoundField>
                                <asp:BoundField DataField="ToLocationStatus" HeaderText="To Loc.Status" />
                                <asp:BoundField DataField="TransferType" HeaderText="Transfer Type" />
                                <asp:BoundField DataField="CreateUser" HeaderText="Created By"></asp:BoundField>
                                <asp:BoundField DataField="CreateDate" HeaderText="Created On"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="hidTranNo" runat="server" Value=""></asp:HiddenField>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="display_none">
        <asp:UpdatePanel ID="upTranNo" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hidFirstTranNo" runat="server" Value="" />
                <asp:HiddenField ID="hidTransferSeq" runat="server" Value="N" />
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
      <asp:HiddenField ID ="hidEditbtn" runat="server" />
          <asp:HiddenField ID ="hidDeletebtn" runat="server" />
          <asp:HiddenField ID ="hidViewbtn" runat="server" />  
         <asp:HiddenField ID ="hidSavebtn" runat="server" />
    </div>



</asp:Content>
