﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmTransferOutAdd.aspx.cs" Inherits="EnterpriserWebFinal.Transfer.frmTransferOutAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 10px;
        }

        .radioboxlist label {
            color: white;
            background-color: dodgerblue;
            padding-left: 6px;
            padding-right: 5px;
            padding-top: 2px;
            padding-bottom: 2px;
            border: 1px solid #AAAAAA;
            white-space: nowrap;
            clear: left;
            margin-right: 5px;
            margin-left: 5px;
        }

        input:checked + label {
            color: white;
            background: orangered;
        }

        .gid_Itemsearch td:nth-child(1), .gid_Itemsearch th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .gid_Itemsearch td:nth-child(2), .gid_Itemsearch th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(3), .gid_Itemsearch th:nth-child(3) {
            min-width: 300px;
            max-width: 300px;
        }

        .gid_Itemsearch td:nth-child(4), .gid_Itemsearch th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(5), .gid_Itemsearch th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(5) {
            text-align: right !important;
            padding-right: 9px;
        }

        .gid_Itemsearch td:nth-child(6), .gid_Itemsearch th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(6) {
            text-align: right !important;
        }

        .gid_Itemsearch td:nth-child(7), .gid_Itemsearch th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(7) {
            text-align: right !important;
        }

        .gid_Itemsearch td:nth-child(8), .gid_Itemsearch th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(8) {
            text-align: right !important;
        }

        .gid_Itemsearch td:nth-child(9), .gid_Itemsearch th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(9) {
            text-align: right !important;
        }

        .gid_Itemsearch td:nth-child(10), .gid_Itemsearch th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(10) {
            text-align: right !important;
        }

        .gid_Itemsearch td:nth-child(11), .gid_Itemsearch th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(11) {
            text-align: right !important;
        }

        .gid_Itemsearch td:nth-child(12), .gid_Itemsearch th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(12) {
            text-align: right !important;
        }

        .gid_Itemsearch td:nth-child(13), .gid_Itemsearch th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(13) {
            text-align: right !important;
        }

        .gid_Itemsearch td:nth-child(14), .gid_Itemsearch th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(14) {
            text-align: right !important;
        }

        .gid_Itemsearch td:nth-child(15), .gid_Itemsearch th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(15) {
            text-align: right !important;
        }

        .gid_Itemsearch td:nth-child(16), .gid_Itemsearch th:nth-child(16) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(16) {
            text-align: right !important;
            padding-right: 9px;
        }

        .gid_Itemsearch td:nth-child(17), .gid_Itemsearch th:nth-child(17) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(18), .gid_Itemsearch th:nth-child(18) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(19), .gid_Itemsearch th:nth-child(19) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(20), .gid_Itemsearch th:nth-child(20) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_Itemsearch td:nth-child(21), .gid_Itemsearch th:nth-child(21) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(1), .transfer th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(2), .transfer th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .transfer td:nth-child(3), .transfer th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(4), .transfer th:nth-child(4) {
            min-width: 340px;
            max-width: 340px;
        }

        .transfer td:nth-child(5), .transfer th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(6), .transfer th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(4) {
            text-align: left !important;
        }

        .transfer td:nth-child(7), .transfer th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(7) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(8), .transfer th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(8) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(9), .transfer th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(9) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(10), .transfer th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(10) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(11), .transfer th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(11) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(12), .transfer th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(12) {
            text-align: right;
        }

        .transfer td:nth-child(13), .transfer th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(14), .transfer th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(15), .transfer th:nth-child(15) {
            min-width: 200px;
            max-width: 200px;
        }

        .transfer td:nth-child(16), .transfer th:nth-child(16) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(16) {
            text-align: right;
        }

        .transfer td:nth-child(17), .transfer th:nth-child(17) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(18), .transfer th:nth-child(18) {
            min-width: 80px;
            max-width: 80px;
        }

        .transfer td:nth-child(19), .transfer th:nth-child(19) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(20), .transfer th:nth-child(20) {
            display: none;
        }

        .transfer td:nth-child(21), .transfer th:nth-child(21) {
            display: none;
        }

        .transfer td:nth-child(22), .transfer th:nth-child(22) {
            display: none;
        }


        .transfer td:nth-child(10), .transfer th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .multipleUOM td:nth-child(1), .multipleUOM th:nth-child(1) {
            min-width: 40px;
            max-width: 40px;
        }

        .multipleUOM td:nth-child(2), .multipleUOM th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .multipleUOM td:nth-child(3), .multipleUOM th:nth-child(3) {
            min-width: 60px;
            max-width: 60px;
        }

        .multipleUOM td:nth-child(3) {
            text-align: right !important;
        }

        .multipleUOM td:nth-child(4), .multipleUOM th:nth-child(4) {
            min-width: 110px;
            max-width: 110px;
            padding-right: 20px;
        }

        .multipleUOM td:nth-child(4) {
            text-align: right !important;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'TransferOutAdd');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "TransferOutAdd";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
    <script type="text/javascript">
        var itemcodecl = '';


        $(function () {
            $('#<%=txtInventoryCode.ClientID%>').focus();
            $('#<%=Label2.ClientID%>').text($('#<%=hidBatchNoText.ClientID%>').val());
            $('#ContentPlaceHolder1_batchNo').text($('#<%=hidBatchNoText.ClientID%>').val());
        });
        //Get Inventory Code DropDownList
        function setItemcode(itemcode) {
            try {
                $('#<%=txtInventoryCode.ClientID %>').val(itemcode);
                $('#<%=txtInventoryCode.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function sortTable(n) { //sankar

            var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
            table = document.getElementById("tblItemSearch");
            switching = true;
            dir = "asc";
            while (switching) {
                switching = false;
                rows = table.rows;
                for (i = 1; i < (rows.length - 1); i++) {
                    shouldSwitch = false;
                    x = rows[i].getElementsByTagName("TD")[n];
                    y = rows[i + 1].getElementsByTagName("TD")[n];
                    if (dir == "asc") {
                        if (n == "2") {
                            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {

                                shouldSwitch = true;
                                break;
                            }
                        }
                        else {
                            if (parseFloat(x.innerHTML) > parseFloat(y.innerHTML)) {

                                shouldSwitch = true;
                                break;
                            }
                        }
                    }
                    else if (dir == "desc") {
                        if (n == "2") {
                            if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {

                                shouldSwitch = true;
                                break;
                            }
                        }
                        else {
                            if (parseFloat(x.innerHTML) < parseFloat(y.innerHTML)) {

                                shouldSwitch = true;
                                break;
                            }

                        }
                    }
                }
                if (shouldSwitch) {
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;

                    switchcount++;
                } else {

                    if (switchcount == 0 && dir == "asc") {
                        dir = "desc";
                        switching = true;
                    }
                }

            }
        }

        //Get Inventory code
        function fncGetInventoryCode(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    event.preventDefault();
                    fncGetInventoryCode_Master($("#<%=txtInventoryCode.ClientID %>"));
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Get BatchStatus
        function fncInventoryCodeEnterkey_Master() {
            try {
                fncGetBatchStatus_Master($('#<%=txtInventoryCode.ClientID%>').val(), $('#<%=hidisbatch.ClientID%>'));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        //Get Inventory Detail
        function fncGetBatchStatus() {
            try {
                fncGetInventoryDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Assign values to text box
        function fncAssignInventoryDetailToControls_Master(inventorydata) {
            try {
                $('#<%=txtItemNameAdd.ClientID%>').val(inventorydata[0]);
                $('#<%=hidvendorcode.ClientID %>').val(inventorydata[9]);


                if ($('#<%=hidisbatch.ClientID%>').val() == "1") {
                    fncGetBatchDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());
                }
                else {
                    $('#<%=txtSellingPrice.ClientID%>').val(inventorydata[5]);
                    $('#<%=txtMrp.ClientID%>').val(inventorydata[6]);
                    $('#<%=txtCost.ClientID%>').val(inventorydata[4]);
                    netcost = inventorydata[4];
                    $('#<%=txtStock.ClientID%>').val(inventorydata[11]);
                    $('#<%=txtWQty.ClientID%>').focus();
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            return false;
        }

        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost, stock) {
            try {
                $('#<%=txtBatchNo.ClientID%>').val(batchno);
                $('#<%=txtMrp.ClientID%>').val(mrp);
                $('#<%=txtCost.ClientID%>').val(Basiccost);
                netcost = Basiccost;
                $('#<%=txtSellingPrice.ClientID%>').val(sprice);
                $('#<%=txtStock.ClientID%>').val(stock);
                $('#<%=txtWQty.ClientID%>').focus();

            }
            catch (err) {
                ShowPopupMessageBox(err.Messagek);
            }
            return false;
        }
    </script>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function fncHangdlingCharge() {
            try {
                if ($('#<%=gvTransferOutAdd.ClientID%> tr').length > 0) {
                    var Amt = $('#<%=txtHandlingCharge.ClientID%>').val();
                    var totalpayable = 0;
                    var GrnRowCount = $('#<%=gvTransferOutAdd.ClientID%> td').closest("tr").length;
                    var AddAmt = parseFloat(Amt / GrnRowCount).toFixed(2);
                    $('#<%=gvTransferOutAdd.ClientID%> tr').each(function () {
                        var rowObj = $(this);

                        var Netcost = parseFloat(rowObj.find('td').eq(20).text());
                        var Total = parseFloat(rowObj.find('td').eq(21).html());
                        var invQty = parseFloat(rowObj.find('td input[id*="txtLQtygrd"]').val());
                        var AffecNetcost = parseFloat(AddAmt / invQty);
                        Netcost = (parseFloat(Netcost) + parseFloat(AffecNetcost)).toFixed(2);
                        rowObj.find('td input[id*="txtCostgrd"]').val(Netcost);
                        rowObj.find('td').eq(11).html((parseFloat(Netcost) * parseFloat(invQty)).toFixed(2));
                        totalpayable = parseFloat(totalpayable) + parseFloat(rowObj.find('td').eq(11).html());
                        $('#<%=txtTotalValue.ClientID%>').val(parseFloat(totalpayable).toFixed(2));
                    });
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(document).ready(function () {


            $('#<%=txtInventoryCode.ClientID%>').focus();

            $("txtLQty:text").focus(function () {
                $(this).select();
            });
            $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0").attr('readonly', 'readonly');
            $("#<%= txtDate.ClientID %>").on('keydown', function () {
                return false;

            });
            fncGidSearch();
            $(document).on('keydown', disableFunctionKeys);

        });
    </script>
    <script type="text/javascript">
        function CloseWindow() {
            window.close();
        }
        function pageLoad() {
            $("#<%= txtType.ClientID %>").val('Transfer');
            $("#<%= txtFrmLocationCode.ClientID %>").val($("#<%= hidLocation.ClientID %>").val());
            $("#<%= txtFrmLocationCode.ClientID %>").text($("#<%= hidLocation.ClientID %>").val());
            $("#divStyleCode").css("display", "none");
            $("#<%=txtLQty.ClientID %>").val('1');
            $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0").attr('readonly', 'readonly');
            $('#<%=txtInventoryCode.ClientID%>').focus();
            $('#<%=txtMrp.ClientID %>').attr('readonly', 'readonly');
            $('#<%=txtSellingPrice.ClientID %>').attr('readonly', 'readonly');
            $('#<%=txtCost.ClientID %>').attr('readonly', 'readonly');
            $('#<%=txtTotalItems.ClientID %>').attr('readonly', 'readonly');
            $('#<%=txtTotalQty.ClientID %>').attr('readonly', 'readonly');
            $('#<%=txtTotalValue.ClientID %>').attr('readonly', 'readonly');
            $('#<%=txtTotCost.ClientID %>').attr('readonly', 'readonly');
            if ($('#<%=hidHandlingCharge.ClientID %>').val() == "Y") {
                $('#divHandlingCharge').show();

                $('#<%= txtHandlingCharge.ClientID%>').change(function () {
                    if ($("#ContentPlaceHolder1_gvTransferOutAdd tbody").children().length >= 1) {
                        if ($("#ContentPlaceHolder1_gvTransferOutAdd_Label3").text() == "Records Not Found") {
                            fncToastInformation('Records Not Found');
                            $('#<%= txtHandlingCharge.ClientID%>').val('0');
                            return false;
                        }
                        else {
                            fncHangdlingCharge();
                        }
                    }
                });
            }
            else {
                $('#divHandlingCharge').hide();
            }

            if ($('#<%=hidNetCostChange.ClientID %>').val() == "Y") {
                $('#<%=txtCost.ClientID %>').removeAttr('readonly');
                $('#<%= txtCost.ClientID%>').change(function () {
                    var cost = $("#<%=txtCost.ClientID %>").val();
                    var Mrp = $("#<%=txtMrp.ClientID %>").val();
                    var SPrice = $("#<%=txtSellingPrice.ClientID %>").val();
                    if (parseFloat(Mrp) < parseFloat(cost)) {
                        popUpObjectForSetFocusandOpen = $('#<%=txtCost.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject('Mrp must be Greater than NetCost');
                        $("#<%=txtCost.ClientID %>").val(netcost);
                        return false;
                    }
                    else if (parseFloat(SPrice) < parseFloat(cost)) {
                        popUpObjectForSetFocusandOpen = $('#<%=txtCost.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject('Selling Price must be Greater than NetCost');
                        $("#<%=txtCost.ClientID %>").val(netcost);
                        return false;
                    }
                    else {
                        var cost = $("#<%=txtCost.ClientID %>").val();
                        var Qty = $("#<%=txtLQty.ClientID %>").val();
                        $("#<%=txtTotCost.ClientID %>").val(parseFloat(parseFloat(cost) * parseFloat(Qty)).toFixed(2));
                    }
                });
            }
            fncDecimal();

        }
        //Get Total Cost
        function fncCost(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    var qty = $("#<%=txtLQty.ClientID %>").val();
                if (!qty == '') {
                    var cost = $("#<%=txtCost.ClientID %>").val();
                        var totalcost = parseFloat(qty) * parseFloat(cost);
                        $("#<%=txtTotCost.ClientID %>").val(totalcost.toFixed(2));
                        if (!$("#<%=txtCost.ClientID %>").is('[readonly]'))
                            $("#<%=txtCost.ClientID %>").select();
                        else
                            $("#<%=lnkAdd.ClientID %>").focus();
                    } else {
                        $("#<%=txtLQty.ClientID %>").focus();
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncFoucustoLQty(event) {    // musaraf 03122022
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    var uom = $("#<%=txtWQty.ClientID %>").val() * $("#<%=ddlMultipleUOM.ClientID %>").val();
                    $("#<%=txtLQty.ClientID %>").val(uom);
                    $("#<%=txtLQty.ClientID %>").focus().select();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// work start by saravanan 11-12-2017
        function fncSubClear() {
            try {
                $("#<%=txtInventoryCode.ClientID %>").val('');
                $("#<%=txtItemNameAdd.ClientID %>").val('');
                $("#<%=txtStock.ClientID %>").val('0');
                $("#<%=txtWQty.ClientID %>").val('0');
                $("#<%=txtLQty.ClientID %>").val('1');
                $("#<%=txtMrp.ClientID %>").val('0');
                $("#<%=txtSellingPrice.ClientID %>").val('0');
                $("#<%=txtCost.ClientID %>").val('0');
                $("#<%=txtTotCost.ClientID %>").val('0');
                $("#<%=txtBatchNo.ClientID %>").val('');
                $("select[id$=ddlMultipleUOM] > option").remove();
                fncItemcodeboxDisable(false);
                $('#<%=txtInventoryCode.ClientID%>').select();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        var saveStatus = false;
        /// Transfer Validation // work done by saravanan
        function fncTransferValidation() {
            var status = true;
            try {
                if ($("#<%=txtFrmLocationCode.ClientID %>").val() == $("#<%=txtToLocation.ClientID %>").val()) {
                    fncToastError("Please change Tolocation code ");
                    $('#ContentPlaceHolder1_txtToLocation').focus();
                    status = false;
                    return false;
                }
                $("#<%=lnkTransfer.ClientID %>").css("display", "none");
                if ($("#ContentPlaceHolder1_gvTransferOutAdd tbody").children().length == 1) {
                    if ($("#ContentPlaceHolder1_gvTransferOutAdd_Label3").text() == "Records Not Found") {
                        $("#<%=lnkTransfer.ClientID %>").css("display", "block");
                        fncToastInformation('Records Not Found');
                        return false;
                    }
                }

                if ($("#<%=txtToLocation.ClientID %>").val() == "") {
                    $("#<%=lnkTransfer.ClientID %>").css("display", "block");
                    fncToastInformation('<%=Resources.LabelCaption.Alert_To_Location%>');
                    $("#<%=txtToLocation.ClientID %>").focus();
                    status = false;
                }
                else if ($("#<%=txtVehicleNo.ClientID %>").val() == "") {
                    $("#<%=lnkTransfer.ClientID %>").css("display", "block");
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Vehicle%>');
                    $("#<%=txtVehicleNo.ClientID %>").focus();
                    status = false;
                }
                else if ($("#<%=txtType.ClientID %>").val() == "") {
                    $("#<%=lnkTransfer.ClientID %>").css("display", "block");
                    fncToastInformation('<%=Resources.LabelCaption.alert_type%>');
                    $("#<%=txtType.ClientID %>").focus();
                    status = false;
                }
                else if ($("#<%=hidReason.ClientID %>").val() == "Y" && $("#<%=ddlReasonCode.ClientID %>").val() == "") {
                    $("#<%=lnkTransfer.ClientID %>").css("display", "block");
                    fncToastInformation("Please Enter ReasonCode.");
                    $("#<%=ddlReasonCode.ClientID %>").focus();
                    return false;
                }

                $("#<%=gvTransferOutAdd.ClientID %> tbody tr").each(function () {
                    var rowObj = $(this);
                    var netCost = rowObj.find('td input[id*="txtCostgrd"]').val();
                    var mrp = rowObj.find('td').eq(8).html();
                    var sellingprice = rowObj.find('td').eq(9).html();

                    <%--if (parseFloat(mrp) < parseFloat(netCost)) {
                $("#<%=lnkTransfer.ClientID %>").css("display", "block");
                        fncToastError("Netcost is Not Greater than MRP");
                        status = false;
                    }
                    else if (parseFloat(sellingprice) < parseFloat(netCost)) {
                $("#<%=lnkTransfer.ClientID %>").css("display", "block");
                        fncToastError("Netcost is Not Greater than Selling Price");
                        status = false;
                    }--%>
        });
                if (status == true) {
                    saveStatus = true;
                    $('#<%=btnSave.ClientID%>').click();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            return status;
        }

        /// Show Save Dailog 
        function fncShowSaveDailog(message) {
            $(function () {
                $("#Save-dialog").html(message);
                $("#Save-dialog").dialog({
                    title: "Enterpriser Web",
                    closeOnEscape: false,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("destroy");
                            window.location = "frmTransferOut.aspx";
                        }
                    },
                    modal: true
                });
            });
        };
        ///Show Item Search Table
        function fncShowGidSearch() {
            try {
                $("#dialog-Gid").dialog({
                    resizable: false,
                    height: 300,
                    width: 500,
                    modal: true,
                    title: "Gid Search",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowVendorSearchDailog() {
            try {
                $('#<%=txtGidSearch.ClientID%>').val($('#<%=txtGidNo.ClientID%>').val());
                fncShowGidSearch();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }




        //// AutoComplete Search for Gid
        function fncGidSearch() {
            try {
                $("[id$=txtGidNo]").autocomplete({
                    source: function (request, response) {
                        var obj = {};
                        obj.prefix = escape(request.term);
                        obj.location = $('#<%=txtFrmLocationCode.ClientID%>').val();

                        $.ajax({
                            url: '<%=ResolveUrl("frmTransferOutAdd.aspx/fncGetGidNo")%>',
                            data: JSON.stringify(obj),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=txtGidNo.ClientID %>').val($.trim(i.item.valitemcode));

                        event.preventDefault();
                    },
                    select: function (e, i) {
                        //$("#dialog-Gid").dialog("destroy");
                        $('#<%=txtGidNo.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=btnGid.ClientID %>').click();
                return false;
            },
                    //appendTo: $("#dialog-Gid"),
                    minLength: 0
                });
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

        //Get Inventory code
       // var Weight = 0;
function fncGetInventoryCode_onEnter(event) {
    try {
        var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        var Itemcode;
        if (keyCode == 13) {
            $('#<%=hidActQty.ClientID%>').val('');
            Itemcode = $('#<%=txtInventoryCode.ClientID %>').val().trim();
            //vendorcode = "";
            itemcodecl = Itemcode;
            //if (Itemcode.length == 13 && Itemcode.charAt(0) == "W")
            //{
            //    var itemId = Itemcode.substring(1, Itemcode.length);
            //    var res = itemId.substring(0, 6);
            //    var wei= itemId.substring(6, 12);
            //    Itemcode = res;
            //    var position = 0;

            //    //for (var i = 1; i < Itemcode.length; i++) {
            //    //    if (parseFloat(position) == 0) {
            //    //        if (parseFloat(Itemcode.charAt(i)) > 0) {
            //    //            position = i;
            //    //        }
            //    //    }
            //    //}
            //    //alert(position);
            //    Weight =parseFloat(wei) /1000;
            //}

            fncGetInventoryDetailForTransferOnEnter(Itemcode);

            event.preventDefault();
            event.stopPropagation();
            return false;
        }
        else if (keyCode == 35) {
            fncItemSearch();
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

//// Item Search 
function fncItemSearch() {
    try {

        fncItemSearchForGrid($("#<%=txtInventoryCode.ClientID%>").val().trim());




    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Show Item Search Table
function fncShowItemSearchTable() {
    try {
        $("#divItemSearch").dialog({
            resizable: false,
            height: "auto",
            width: 1093,
            modal: true,
            title: "Enterpriser Web",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

/// Data Bind to Grid
function fncItemSearchForGrid(value) {
    try {
        var obj = {};


        obj.vendorcode = "";
        obj.searchData = value;
        obj.franchise = "N";
        obj.gidLocation = $("#<%=txtFrmLocationCode.ClientID%>").val();

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNote.aspx/fncItemSearchForGrid")%>',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                fncItemBindtoTable(jQuery.parseJSON(msg.d));
                $("#divItemSearch").css("display", "block");
                $("#divTranGrid").css("display", "none");


            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });
    }
    catch (err) {
        fncToastError(err.message);
    }

}

/// Item bind Search table
function fncItemBindtoTable(msg) {
    try {

        var objSearchData, tblSearchData, Rowno = 0, batchNo = "";
        //objSearchData = jQuery.parseJSON(msg);
        objSearchData = msg;

        tblSearchData = $("#tblItemSearch tbody");
        tblSearchData.children().remove();


        if (objSearchData.length > 0) {
            for (var i = 0; i < objSearchData.length; i++) {

                if (objSearchData[i]["BatchNo"] == null) {
                    batchNo = "";
                }
                else {
                    batchNo = objSearchData[i]["BatchNo"];
                }


                Rowno = parseFloat(i) + parseFloat(1);
                row = "<tr id='trSearchRow_" + i + "' tabindex='" + i + "' >" +
                    "<td id='tdRowNo_" + i + "' >" + Rowno + "</td>" +
                    "<td id='tdItemcode_" + i + "' > " + objSearchData[i]["Inventorycode"] + "</td>" +
                    "<td >" + objSearchData[i]["Description"] + "</td>" +
                "<td id='tdBatchNo_" + i + "' >" + batchNo + "</td>" +
                    "<td >" + objSearchData[i]["Compatible"] + "</td>" +
                     "<td >" + objSearchData[i]["ActQty"].toFixed(2) + "</td>" +
                    "<td >" + objSearchData[i]["PRReqQty"].toFixed(2) + "</td>" +
                "<td >" + objSearchData[i]["ActMRP"].toFixed(2) + "</td>" +
                "<td  >" + objSearchData[i]["ActSellingPrice"].toFixed(2) + "</td>" +
                "<td  >" + objSearchData[i]["Itaxper3"].toFixed(2) + "</td>" +
                "<td  >" + objSearchData[i]["ActNet"].toFixed(2) + "</td>" +
                "<td  >" + objSearchData[i]["CashDiscount"].toFixed(2) + "</td>" +
                "<td  >" + objSearchData[i]["SchemeDiscount"].toFixed(2) + "</td>" +
                "<td  >" + "" + "</td>" +
                "<td  >" + "" + "</td>" +
                "<td  >" + "" + "</td>" +
                "<td  >" + "" + "</td>" +
                "<td  >" + "" + "</td>" +
                "<td  >" + "" + "</td>" +
                "<td id='tdExpMonth_" + i + "' >" + objSearchData[i]["ExpMonth"] + "</td>" +
                "<td id='tdExpYear_" + i + "' >" + objSearchData[i]["ExpYear"] + "</td>" +
                "<td style ='display:none' id='tdDepartmentCode_" + i + "' >" + objSearchData[i]["DepartmentCode"] + "</td>" +
                "<td style ='display:none' id='tdQtyOnHand_" + i + "' >" + objSearchData[i]["QtyOnHand"] + "</td>" +
                "<td style ='display:none' id='tdQualifyItem_" + i + "'>" + objSearchData[i]["QualifyItem"] + "</td>" +
                "<td style ='display:none' id='tdFlag_" + i + "' >" + objSearchData[i]["Flag"] + "</td>" +
                "<td style ='display:none' id='tdQualifyingBatch_" + i + "' >" + objSearchData[i]["QualifyingBatch"] + "</td>" +
                "<td style ='display:none' id='tdAllowNegative_" + i + "' >" + objSearchData[i]["AllowNegative"] + "</td>" +
                "<td style ='display:none' id='tdAllowDC_" + i + "' >" + objSearchData[i]["AllowDC"] + "</td>" +
                "</tr>";
                tblSearchData.append(row);
            }

            //console.log(tblSearchData.children()); Vijay tbl

            tblSearchData.children().dblclick(fncSearchRowDoubleClick);
            tblSearchData.children().click(fncSearchRowClick);
            fncAssignBackgroundColor();
        }
    }
    catch (err) {
        fncToastError(err.message);
    }

}


/// Search Row Click
function fncSearchRowDoubleClick() {
    try {

        var obj, Itemcode, batchNo;
        //$("#divItemSearch").dialog("destroy");


        Itemcode = $.trim($(this).find('td[id*=tdItemcode]').text());
        batchNo = $.trim($(this).find('td[id*=tdBatchNo]').text());
        $('#<%=txtInventoryCode.ClientID%>').val(Itemcode);
        $('#<%=txtBatchNo.ClientID%>').val(batchNo);
        $('#<%=hidExpMonth.ClientID%>').val($.trim($(this).find('td[id*=tdExpMonth]').text()));
        $('#<%=hidExpYear.ClientID%>').val($.trim($(this).find('td[id*=tdExpYear]').text()));
        $('#<%=hidDepartmentCode.ClientID%>').val($.trim($(this).find('td[id*=tdDepartmentCode]').text()));
        $('#<%=hidQtyOnHand.ClientID%>').val($.trim($(this).find('td[id*=tdQtyOnHand]').text()));
        $('#<%=hidQualifyItem.ClientID%>').val($.trim($(this).find('td[id*=tdQualifyItem]').text()));
        $('#<%=hidFlag.ClientID%>').val($.trim($(this).find('td[id*=tdFlag]').text()));
        $('#<%=hidQualifyingBatch.ClientID%>').val($.trim($(this).find('td[id*=tdQualifyingBatch]').text()));
        $('#<%=hidItemAllowNegative.ClientID%>').val($.trim($(this).find('td[id*=tdAllowNegative]').text()));
        $('#<%=hidItemAllowDC.ClientID%>').val($.trim($(this).find('td[id*=tdAllowDC]').text()));

        fncGetInventoryDetailForGID(Itemcode, batchNo, "");


        $("#divItemSearch").css("display", "none");
        $("#divTranGrid").css("display", "block");
    }
    catch (err) {
        fncToastError(err.message);
    }
}
//

function fncStyleRowDoubleClick() {
    try {

        var obj, batchNo;
        //$("#divItemSearch").dialog("destroy");


        batchNo = $.trim($(this).find('td[id*=tdStyleCode]').text().trim());
        $('#<%=txtInventoryCode.ClientID%>').val(batchNo);


        //$("#divStyleCode").css("display", "none");
        $("#divStyleCode").dialog("close");
        $("#divStyleCode").css("display", "none");
        $("#divTranGrid").css("display", "block");
        fncGetInventoryDetailForTransferOnEnter(batchNo);
        event.preventDefault();
        event.stopPropagation();
        return false;

    }
    catch (err) {
        fncToastError(err.message);
    }
}

/// to show back ground color on click
function fncSearchRowClick() {
    try {
        $(this).css("background-color", "#80b3ff");
        $(this).siblings().css("background-color", "white");

    }
    catch (err) {
        fncToastError(err.message);
    }
}


///Assign Value to On Enter key Press
var netcost = 0;

function fncGetInventoryDetailForTransferOnEnter(Itemcode) {
    try {
        var obj = {}, objInventory;
        obj.Itemcode = Itemcode;
        obj.franchise = "N";
        obj.gidLocation = $('#<%=txtFrmLocationCode.ClientID%>').val();

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("frmTransferOutAdd.aspx/fncGetInventoryDetailForTransferOnEnter")%>',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                objInventory = jQuery.parseJSON(msg.d);
                console.log(objInventory);
                if (objInventory.Table.length > 0) {
                    if (objInventory.Table[0]["InventoryStatus"] == "0") {
                        fncSubClear();
                        $('#<%=txtInventoryCode.ClientID%>').select();
                        fncToastInformation('<%=Resources.LabelCaption.Alert_InvalidItemcode%>');
                        return;
                    }
                }
                if (objInventory.Table1.length > 0) {
                    if (objInventory.Table1[0]["AllowDC"] == "0") {
                        fncSubClear();
                        $('#<%=txtInventoryCode.ClientID%>').select();
                        fncToastInformation("Allow For This ItemCode");
                        return;
                    }
                }
                if (objInventory.Table2[0]["BatchStatus"].trim() == "1") {
                    fncItemBindtoTable(objInventory.Table1);
                    $("#divItemSearch").css("display", "block");
                    $("#divTranGrid").css("display", "none");
                }
                else {
                    if (objInventory.Table1.length > 0) {

                        $('#<%=txtInventoryCode.ClientID%>').val(objInventory.Table1[0]["Inventorycode"]);
                        $('#<%=txtItemNameAdd.ClientID%>').val(objInventory.Table1[0]["Description"]);
                        $('#<%=txtMrp.ClientID%>').val(objInventory.Table1[0]["MRP"]);
                        $('#<%=txtCost.ClientID%>').val(objInventory.Table1[0]["NetCost"]);
                        netcost = objInventory.Table1[0]["NetCost"];
                        $('#<%=txtSellingPrice.ClientID%>').val(objInventory.Table1[0]["SellingPrice"]);
                        $('#<%=hidUOMConv.ClientID%>').val(objInventory.Table1[0]["UomConv"]);
                        $('#<%=hidLUOM.ClientID%>').val(objInventory.Table1[0]["UOM"].trim());
                        $('#<%=hidWOM.ClientID%>').val(objInventory.Table1[0]["Compatible"].trim());
                        $('#<%=txtStock.ClientID%>').val(objInventory.Table1[0]["Stock"]);
                        $('#<%=txtBatchNo.ClientID%>').val(objInventory.Table1[0]["BatchNo"]);

                        $('#<%=hidExpMonth.ClientID%>').val(objInventory.Table1[0]["ExpMonth"]);
                        $('#<%=hidExpYear.ClientID%>').val(objInventory.Table1[0]["ExpYear"]);
                        $('#<%=hidDepartmentCode.ClientID%>').val(objInventory.Table1[0]["DepartmentCode"]);
                        $('#<%=hidQtyOnHand.ClientID%>').val(objInventory.Table1[0]["QtyOnHand"]);
                        $('#<%=hidQualifyItem.ClientID%>').val(objInventory.Table1[0]["QualifyItem"]);
                        $('#<%=hidFlag.ClientID%>').val($.trim(objInventory.Table1[0]["Flag"]));
                        $('#<%=hidQualifyingBatch.ClientID%>').val(objInventory.Table1[0]["QualifyingBatch"]);

                        $('#<%=hidItemAllowNegative.ClientID%>').val(objInventory.Table1[0]["AllowNegative"]);
                        $('#<%=hidItemAllowDC.ClientID%>').val(objInventory.Table1[0]["AllowDC"]);
                        fncDecimalChangeBasedOnUOM(objInventory.Table1[0]["UOM"].trim());
                    }

                    fncTextDecimalRestrictionwithUOM();
                    fncItemcodeboxDisable(true);

                    if (objInventory.Table1[0]["Lqty"]  != undefined)
                        $('#<%=txtLQty.ClientID%>').val(parseFloat(parseFloat(objInventory.Table1[0]["Lqty"].trim()) / 1000).toFixed(3));

                    if ($('#<%=hidWOM.ClientID%>').val() != "") {
                        $('#<%=txtWQty.ClientID%>').focus().select();
                    }
                    else {
                        $('#<%=txtLQty.ClientID%>').focus().select();
                    }



                }


            },
            error: function (data) {
                fncToastError(data.message);
            }
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}


        $(document).ready(function () {                  //03122022 musaraf
            $('#<%=ddlMultipleUOM.ClientID %>').change(function () {
                $('#<%=txtWQty.ClientID%>').focus().select();
            });
         });



///Assign Value to Text Box
function fncGetInventoryDetailForGID(Itemcode, batchNo, vendorcode) {
    try {
        var obj = {}, objInventory;
        obj.Itemcode = Itemcode;
        obj.batchNo = batchNo;
        obj.franchise = "N";
        obj.gidLocation = $('#<%=txtFrmLocationCode.ClientID%>').val();

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("frmTransferOutAdd.aspx/fncGetInventoryDetailForTransferSearch")%>',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                objInventory = jQuery.parseJSON(msg.d);

                if (objInventory.Table.length > 0) {
                    if (objInventory.Table[0]["InventoryStatus"] == "0") {
                        fncSubClear();
                        $('#<%=txtInventoryCode.ClientID%>').select();
                        fncToastInformation('<%=Resources.LabelCaption.Alert_InvalidItemcode%>');
                        return;
                    }
                }


                if (objInventory.Table1.length > 0) {

                    $('#<%=txtInventoryCode.ClientID%>').val(objInventory.Table1[0]["inventorycode"].trim());
                    $('#<%=txtItemNameAdd.ClientID%>').val(objInventory.Table1[0]["Description"]);
                    $('#<%=txtMrp.ClientID%>').val(objInventory.Table1[0]["MRP"]);
                    $('#<%=txtCost.ClientID%>').val(objInventory.Table1[0]["NetCost"]);
                    netcost = objInventory.Table1[0]["NetCost"];
                    $('#<%=txtSellingPrice.ClientID%>').val(objInventory.Table1[0]["SellingPrice"]);
                    $('#<%=hidUOMConv.ClientID%>').val(objInventory.Table1[0]["UomConv"]);
                    $('#<%=hidLUOM.ClientID%>').val(objInventory.Table1[0]["UOM"]);
                    $('#<%=hidWOM.ClientID%>').val(objInventory.Table1[0]["Compatible"].trim());
                    $('#<%=txtStock.ClientID%>').val(objInventory.Table1[0]["Stock"]);
                    $('#<%=txtBatchNo.ClientID%>').val(objInventory.Table1[0]["BatchNo"]);
                    fncDecimalChangeBasedOnUOM(objInventory.Table1[0]["UOM"].trim());
                    for (var i = 0; i < objInventory.Table2.length; i++) {      //03122022 musaraf
                        $('#<%=ddlMultipleUOM.ClientID%>').append($("<option></option>").val(objInventory.Table2[i]["ConvQty"]).html(objInventory.Table2[i]["UOM"]));
                    }
                    $('#<%=ddlMultipleUOM.ClientID %>').trigger("liszt:updated");

                    if ($('#<%=ddlMultipleUOM.ClientID %>').val() != null) {
                        $("#divUOM").css("display", "block");
                        fncOpenDropdownlist($('#<%=ddlMultipleUOM.ClientID %>'));
                    }
                    else {
                        $("#divUOM").css("display", "none");
                    }
                   
                }

                fncTextDecimalRestrictionwithUOM();
                fncItemcodeboxDisable(true);

                if ($('#<%=hidWOM.ClientID%>').val() != "") {

                    $('#<%=txtWQty.ClientID%>').focus().select();
                    
                }
                else {
                    $('#<%=txtLQty.ClientID%>').focus().select();
                
                }
            },
            error: function (data) {
                fncToastError(data.message);
            }
        });
    }

    catch (err) {
        fncToastError(err.message);
    }
      
}

    


       



function fncTextDecimalRestrictionwithUOM() {
    try {
        if ($('#<%=hidLUOM.ClientID%>').val().trim() == "PCS") {
            <%--$('#<%=txtWQty.ClientID%>').number(true, 0);
                $('#<%=txtLQty.ClientID%>').number(true, 0);--%>

        }
          <%--  else {
                $('#<%=txtWQty.ClientID%>').number(true, 3);
                $('#<%=txtLQty.ClientID%>').number(true, 3);
            }--%>

    }
    catch (err) {
        fncToastError(err.message);
    }

}
// Itemcode box disable and Enable
function fncItemcodeboxDisable(value) {
    try {
            <%--$('#<%=txtInventoryCode.ClientID%>').val(itemcodecl);--%>
        $('#<%=txtInventoryCode.ClientID%>').prop('disabled', value);
    }
    catch (err) {
        fncToastError(err.message);
    }
}
/// Assign Background color for Search grid
function fncAssignBackgroundColor() {

    try {
        $("#tblItemSearch tbody > tr").first().css("background-color", "#80b3ff");
        setTimeout(function () {
            $("#tblItemSearch tbody > tr").first().focus().select();
        }, 50);

        $("#tblItemSearch tbody > tr").on('keydown', function (evt) {
            var rowobj = $(this);
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            var scrollheight = 0;

            if (charCode == 13) {
                rowobj.dblclick();
                return false;
            }
            else if (charCode == 40) {

                var NextRowobj = rowobj.next(); //$(this).closest('tr').next('tr'); //rowobj.next();                        
                if (NextRowobj.length > 0) {
                    NextRowobj.css("background-color", "#80b3ff");
                    NextRowobj.siblings().css("background-color", "white");
                    NextRowobj.select().focus();

                    setTimeout(function () {
                        scrollheight = $("#tblItemSearch tbody").scrollTop();
                        if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                            $("#tblItemSearch tbody").scrollTop(0);
                        }
                        else if (parseFloat(scrollheight) > 10) {
                            scrollheight = parseFloat(scrollheight) - 10;
                            $("#tblItemSearch tbody").scrollTop(scrollheight);
                        }
                    }, 50);

                }

            }
            else if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.css("background-color", "#80b3ff");
                    prevrowobj.siblings().css("background-color", "white");
                    prevrowobj.select().focus();

                    setTimeout(function () {
                        scrollheight = $("#tblItemSearch tbody").scrollTop();
                        if (parseFloat(scrollheight) > 3) {
                            scrollheight = parseFloat(scrollheight) + 1;
                            $("#tblItemSearch tbody").scrollTop(scrollheight);
                        }
                    }, 50);

                }
            }

        });
    }
    catch (err) {
        fncToastError(err.message);
    }

}


function fncAssignBackgroundColorstylecode() {

    try {
        $("#tblStylecode tbody > tr").first().css("background-color", "#80b3ff");
        setTimeout(function () {
            $("#tblStylecode tbody > tr").first().focus().select();
        }, 50);

        $("#tblStylecode tbody > tr").on('keydown', function (evt) {
            var rowobj = $(this);
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            var scrollheight = 0;

            if (charCode == 13) {
                rowobj.dblclick();
                return false;
            }
            else if (charCode == 40) {

                var NextRowobj = rowobj.next(); //$(this).closest('tr').next('tr'); //rowobj.next();                        
                if (NextRowobj.length > 0) {
                    NextRowobj.css("background-color", "#80b3ff");
                    NextRowobj.siblings().css("background-color", "white");
                    NextRowobj.select().focus();

                    setTimeout(function () {
                        scrollheight = $("#tblStylecode tbody").scrollTop();
                        if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                            $("#tblStylecode tbody").scrollTop(0);
                        }
                        else if (parseFloat(scrollheight) > 10) {
                            scrollheight = parseFloat(scrollheight) - 10;
                            $("#tblStylecode tbody").scrollTop(scrollheight);
                        }
                    }, 50);

                }

            }
            else if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.css("background-color", "#80b3ff");
                    prevrowobj.siblings().css("background-color", "white");
                    prevrowobj.select().focus();

                    setTimeout(function () {
                        scrollheight = $("#tblStylecode tbody").scrollTop();
                        if (parseFloat(scrollheight) > 3) {
                            scrollheight = parseFloat(scrollheight) + 1;
                            $("#tblStylecode tbody").scrollTop(scrollheight);
                        }
                    }, 50);

                }
            }

        });
    }
    catch (err) {
        fncToastError(err.message);
    }

}



function fncAddValidation() {
    var prvQty = 0, actQty = 0;
    try {
   
        $("#ContentPlaceHolder1_gvTransferOutAdd tbody").children().each(function () {
            rowObj = $(this);
            if ($.trim($("td", rowObj).eq(2).text()) == $('#<%=txtInventoryCode.ClientID%>').val().trim() && $.trim($("td", rowObj).eq(6).text()) == $('#<%=txtBatchNo.ClientID%>').val().trim()) {
                $('#<%=hidActQty.ClientID%>').val(rowObj.find('td input[id*="txtLQtygrd"]').val());
            }
        });

        if ($('#<%=txtInventoryCode.ClientID%>').val() == "") {
            fncToastError("Please enter Itemcode ");
            fncSubClear();
            return false;
        }
        else if (parseFloat($('#<%=txtLQty.ClientID%>').val()) <= 0) {
            fncToastError("Please enter valid transfer Qty ");
            $('#<%=txtLQty.ClientID%>').focus();
                return false;
            //fncSubClear();
            }

        if (parseFloat($('#<%=hidActQty.ClientID%>').val()) > 0) {
            fncDuplicateEntryalert($('#<%=hidActQty.ClientID%>').val());
            return false;
        }
        else {
                <%-- if (($('#<%=hidAllownegtr.ClientID%>').val()) == "N") {
                if (parseFloat($('#<%=txtLQty.ClientID%>').val()) > parseFloat($('#<%=txtStock.ClientID%>').val())) {
                        popUpObjectForSetFocusandOpen = $('#<%=txtLQty.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject("Please check entered Qty.Transfer Qty should be less then or equal to Stock ");
                        return false;
                    }--%>
            if (($('#<%=hidAllowNegative.ClientID%>').val()) == "NO") {
                if (parseFloat($('#<%=txtLQty.ClientID%>').val()) > parseFloat($('#<%=txtStock.ClientID%>').val())) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtLQty.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject("Please check entered Qty.Transfer Qty should be less then or equal to Stock ");
                    return false;
                }
                else {
                    return true;
                }//Vijay AllowNegative
            }
            else if (($('#<%=hidAllowNegative.ClientID%>').val()) == "NA") {
                if (($('#<%=hidItemAllowNegative.ClientID%>').val()) != "true") {
                    if (parseFloat($('#<%=txtLQty.ClientID%>').val()) > parseFloat($('#<%=txtStock.ClientID%>').val())) {
                        popUpObjectForSetFocusandOpen = $('#<%=txtLQty.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject("Please check entered Qty.Transfer Qty should be less then or equal to Stock ");
                        return false;
                    }
                    else {
                        return true;
                    }
                }

            }
            else {
                return true;
            }

    }

}
    catch (err) {
        fncToastError(err.message);
    }
}

function disableFunctionKeys(e) {
    try {

        var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
        if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

            if (e.keyCode == 115) {

                if (fncTransferValidation() == true) {
                    if (saveStatus == false) {
                        saveStatus = true;
                        $('#<%=btnSave.ClientID%>').click();
                    }
                    //__doPostBack('ctl00$ContentPlaceHolder1$lnkTransfer', '');
                    return false;
                }
            }
            else if (e.keyCode == 117) {
                window.location = "frmTransferOutAdd.aspx";
                return false;
            }
            else if (e.keyCode == 119) {
                window.location = "frmTransferOut.aspx";
                return false;
            }

            else if (e.keyCode == 112) {
                return false;
            }

            e.preventDefault();
            return false;
        }

        if (e.keyCode == 27) {
            $("#divItemSearch").css("display", "none");
            $("#divTranGrid").css("display", "block");
            $('#<%=txtInventoryCode.ClientID%>').select();
        }


    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncDecimal() {
   <%-- $('#<%=txtMrp.ClientID%>').number(true, 2);
    $('#<%=txtSellingPrice.ClientID%>').number(true, 2);
    $('#<%=txtCost.ClientID%>').number(true, 2);
    $('#<%=txtTotCost.ClientID%>').number(true, 2);--%>
}

        function fncDecimalChangeBasedOnUOM(uom) {
            try {
                if (uom == "PCS") {
                    <%--$('#<%=txtWQty.ClientID%>').number(true, 0);
            $('#<%=txtLQty.ClientID%>').number(true, 0);--%>
                }
       <%-- else {
            $('#<%=txtWQty.ClientID%>').number(true, 3);
            $('#<%=txtLQty.ClientID%>').number(true, 3);
        }--%>
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncDuplicateEntryalert(msg) {
            $(function () {
                $('#<%=lblAddMsg.ClientID%>').text(msg + " Qty for this item found in tranfer list (Do you want to add  '" + $('#<%=txtLQty.ClientID%>').val() + "' qty )");
                $("#TransferAdd").dialog({
                    title: "Enterpriser Web",
                    modal: true,
                    width: "auto"
                });
            });
        };


        function fncNoClick() {
            try {
                $("#TransferAdd").dialog("destroy");
                fncSubClear();
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowMultipUOM() {
            try {

                $("#divStyleCode").dialog({
                    resizable: false,
                    height: "auto",
                    width: 360,
                    modal: true,
                });

                fncBindStyleCode()
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncBindStyleCode() {
            var uomObj, row, rowNo = 0;
            var mulUOMBody;
            try {
                uomObj = jQuery.parseJSON($('#<%=hidStyleCode.ClientID %>').val());
                mulUOMBody = $("#tblStylecode tbody");
                mulUOMBody.children().remove();

                for (var i = 0; i < uomObj.length; i++) {
                    rowNo = parseInt(rowNo) + 1;
                    row = "<tr id='trStyleCode_" + i + "' tabindex='" + i + "' >"
                                           + "<td id='tdRowNo' >" + rowNo + "</td>"
                                           + "<td id='tdStyleCode' >" + uomObj[i]["BatchNo"] + "</td>"
                                           + "<td id='tdBalanceqty' >" + parseFloat(uomObj[i]["BalanceQty"]).toFixed(2) + "</td>"
                                           + "<td id='tdSellingPrice' >" + parseFloat(uomObj[i]["SellingPrice"]).toFixed(2) + "</td>"
                                           + "</tr>";
                    mulUOMBody.append(row);
                    mulUOMBody.children().dblclick(fncStyleRowDoubleClick);
                    mulUOMBody.children().click(fncSearchRowClick);
                    fncAssignBackgroundColorstylecode();


                }



            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncSetValue() {
            try {
                if (SearchTableName == "WareHouse") {
                    if ($('#<%=hidHandlingCharge.ClientID %>').val() == "Y") {
                        document.getElementById("<%=txtHandlingCharge.ClientID %>").select();
                    }
                    else {
                        document.getElementById("<%=lnkTransfer.ClientID %>").focus();
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function NegativeStacks(source) {
            try {
                var Qty = {};
                Qty = source.split('-');
                var Lqty = Qty[0];
                var BQty = Qty[1];
                alert(message); ("Qty '" + Lqty + "is Greater than Balance Qty'" + BQty + '""');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fnc_ShowSearchDialogCommon(event) {
            // alert();
            if (event.keyCode == 13) {
                if ($('#<%=txtWarehouse.ClientID %>').val() == '') {
                    if ($('#<%=hidHandlingCharge.ClientID %>').val() == "Y") {
                        document.getElementById("<%=txtHandlingCharge.ClientID %>").select();
                    }
                    else {
                        document.getElementById("<%=lnkTransfer.ClientID %>").focus();
                    }
                }
                else {
                    fncShowSearchDialogCommon(event, 'WareHouse', 'txtWarehouse', '');
                }
            }
            else {
                fncShowSearchDialogCommon(event, 'WareHouse', 'txtWarehouse', '');
            }
            return false;
        }

        function fncVenItemRowdblClk(rowObj) {
            try {
                rowObj = $(rowObj);
                var item = $(rowObj).selector;
                fncOpenItemhistory(item);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        /// Open Item History
        function fncOpenItemhistory(itemcode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            page = page + "?InvCode=" + itemcode + "&Status=dailog";
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 700,
                width: 1250,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }

        function txtLQty_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sLqty = row.cells[7].getElementsByTagName("input")[0].value;
                var Qtyhand = row.cells[15].innerHTML;
                if (sLqty != "") {
                    if (parseFloat(sLqty) > parseFloat(Qtyhand)) {
                        row.cells[7].getElementsByTagName("input")[0].value = row.cells[7].getElementsByTagName("span")[0].innerHTML;
                        validation = true;
                        ShowPopupMessageBox("Enter Qty is Greater than Actual Qty", "#ContentPlaceHolder1_gvTransferOutAdd_txtLQty_" + rowIndex);
                        return;
                    }
                }
                //row.cells[11].innerHTML = parseFloat(sLqty) * parseFloat(row.cells[10].getElementsByTagName("input")[0].value);
                validation = true;
                fncTotal();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function txtCostgrd_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var NetCost = row.cells[10].getElementsByTagName("input")[0].value;
                var Mrp = row.cells[8].innerHTML;
                var SPrice = row.cells[9].innerHTML;
                if (NetCost != "") {
                    if (parseFloat(NetCost) > parseFloat(Mrp)) {
                        row.cells[10].getElementsByTagName("input")[0].value = row.cells[10].getElementsByTagName("span")[0].innerHTML;
                        validation = true;
                        ShowPopupMessageBox("NetCost is Must be Lessar than MRP", "#ContentPlaceHolder1_gvTransferOutAdd_txtCostgrd_" + rowIndex);
                        return;
                    }
                    else if (parseFloat(NetCost) > parseFloat(SPrice)) {
                        row.cells[10].getElementsByTagName("input")[0].value = row.cells[10].getElementsByTagName("span")[0].innerHTML;
                        validation = true;
                        ShowPopupMessageBox("NetCost is Must be Lessar than Selling Price", "#ContentPlaceHolder1_gvTransferOutAdd_txtCostgrd_" + rowIndex);
                        return;
                    }
                }
                validation = true;
                fncTotal();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        var validation = false;
        function txtLQty_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtLQtygrd"]').select();
                    event.preventDefault();
                    return;
                }
                else {
                    rowobj.find('td input[id*="txtLQtygrd"]').select();
                    event.preventDefault();
                    return;
                }
            }
            if (charCode == 39) {
                var NextRowobj = rowobj;
                NextRowobj.find('td input[id*="txtLQtygrd"]').select();
                event.preventDefault();
                return;
            }
            if (charCode == 37) {
                rowobj.find('td input[id*="txtLQtygrd"]').select();
                event.preventDefault();
                return;

            }
            if (charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtLQtygrd"]').select();
                    event.preventDefault();
                    return;
                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtLQtygrd"]').select();
                    event.preventDefault();
                    return;
                }
            }
            return true;
        }
        function txtCostgrd_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtCostgrd"]').select();
                    event.preventDefault();
                    return;
                }
                else {
                    rowobj.find('td input[id*="txtCostgrd"]').select();
                    event.preventDefault();
                    return;
                }
            }
            if (charCode == 39) {
                var NextRowobj = rowobj;
                NextRowobj.find('td input[id*="txtCostgrd"]').select();
                event.preventDefault();
                return;
            }
            if (charCode == 37) {
                rowobj.find('td input[id*="txtCostgrd"]').select();
                event.preventDefault();
                return;

            }
            if (charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtCostgrd"]').select();
                    event.preventDefault();
                    return;
                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtCostgrd"]').select();
                    event.preventDefault();
                    return;
                }
            }
            return true;
        }
        function fncTotal() {
            try {
                var qty = 0;
                var Total = 0;
                $("#<%=gvTransferOutAdd.ClientID %> tbody tr").each(function () {
                    var row = $(this).closest("tr");
                    var currQty = $(this).find('td input[id*="txtLQtygrd"]').val();
                    qty = parseFloat(qty) + parseFloat(currQty); 
                    var cost = parseFloat($(this).find('td input[id*="txtCostgrd"]').val()); 
                    $(this).find("td").eq(11).html(parseFloat(cost * currQty).toFixed(2));
                    Total = parseFloat(Total) + parseFloat($(this).find("td").eq(11).html());
                });
                $("#<%=txtTotalQty.ClientID %>").val(parseFloat(qty).toFixed(2));
                $("#<%=txtTotalValue.ClientID %>").val(parseFloat(Total).toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function ValidateText(val) {
            var intValue = parseInt(val.value, 10);
            var row = val.parentNode.parentNode;
            if (isNaN(intValue)) {
                row.cells[7].getElementsByTagName("input")[0].value = row.cells[7].getElementsByTagName("span")[0].innerHTML;
                alert("please enter only number");
            }
        }



        function numericOnly(elementRef) {

            var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

            if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57) || (keyCodeEntered == 189) || (keyCodeEntered == 109)) {

                return true;

            }

            else if (keyCodeEntered == 46) {

                // Allow only 1 decimal point ('.')...  

                if ((elementRef.target.value) && (elementRef.target.value.indexOf('.') >= 0))

                    return false;

                else

                    return true;

            }

            return false;

        }

         function fncYesClick_master() {
        try {
            $('#<%=hidActQty.ClientID%>').val(parseFloat($('#<%=hidActQty.ClientID%>').val()) + parseFloat($('#<%=txtLQty.ClientID%>').val()));
            if (($('#<%=hidAllownegtr.ClientID%>').val()) == "N") {
                if (parseFloat($('#<%=hidActQty.ClientID%>').val()) > parseFloat($('#<%=txtStock.ClientID%>').val())) {
                    fncToastError(" Please check entered Qty.Transfer Qty should be less then or equal to Stock ");
                    $('#<%=txtLQty.ClientID%>').focus();
                        $("#TransferAdd").dialog("destroy");
                        return false;
                    }
                    else {
                        $("#TransferAdd").dialog("destroy");
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkAdd', '');
                    }
                }
                else {
                    $("#TransferAdd").dialog("destroy");
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkAdd', '');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
             }

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Transfer</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Transfer/frmTransferOut.aspx?id=TransferOut">View Transfer Out</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Transfer Out Add </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div>
            <%-- <asp:UpdatePanel ID="upAdditem" runat="server" UpdateMode="Always">
                <ContentTemplate>--%>
            <div class="container-group-full-gid">
                <div class="top-container-im">
                    <div class="left-container-detail">
                        <div class="control-group-split">
                            <div class="control-group-split" style="width: 100%;">
                                <div class="control-group-left" style="width: 25%; padding-right: 10px;">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="Transfer NO"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtTRNO" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left" style="width: 25%; padding-right: 10px;">
                                    <div class="label-left">
                                        <asp:Label ID="Label6" runat="server" Text="Type"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">

                                        <asp:TextBox ID="txtType" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        <%--onkeydown="return fncShowSearchDialogCommon(event, 'TransferType', 'txtType', '');"--%>
                                    </div>
                                </div>
                                <div class="control-group-right" style="width: 25%;">
                                    <div class="label-left">
                                        <asp:Label ID="Label3" runat="server" Text="To Location"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlToLocation" runat="server" CssClass="form-control-res">
                                        </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtToLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'ToLocation', 'txtToLocation', 'txtVehicleNo');" Text="HQ"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right" style="width: 25%; padding-right: 10px;">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="From Location"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlFrmLocationCode" runat="server" CssClass="form-control-res"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlFrmLocationCode_SelectedIndexChanged">
                                        </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtFrmLocationCode" runat="server" CssClass="form-control-res"></asp:TextBox><%--onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtFrmLocationCode', '');"--%>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split" style="width: 100%;">
                                <div class="control-group-left" style="width: 25%; padding-right: 10px;">
                                    <div class="label-left">
                                        <asp:Label ID="Label7" runat="server" Text="Vehicle No"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlVehicleNo" runat="server" CssClass="form-control-res">
                                        </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtVehicleNo" runat="server" CssClass="form-control-res"
                                            onkeydown="return fncShowSearchDialogCommon(event, 'Vehicle', 'txtVehicleNo', 'txtWarehouse');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left" style="width: 25%; padding-right: 10px;">
                                    <div class="label-left">
                                        <asp:Label ID="Label8" runat="server" Text="Date"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtDate" runat="server" onmousedown="return false;" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left" style="width: 25%; padding-right: 10px;">
                                    <div class="label-left">
                                        <asp:Label ID="Label11" runat="server" Text=" GID/Distribution No"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:TextBox ID="txtGidNo" runat="server" CssClass="form-control-res" oninput="fncShowVendorSearchDailog();"></asp:TextBox>--%>
                                        <asp:TextBox ID="txtGidNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        <%--<asp:DropDownList ID="ddlGIDNo" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlGIDNo_Change" CssClass="form-control-res">
                                        </asp:DropDownList>--%>
                                    </div>
                                </div>
                                <div class="control-group-left" style="width: 25%;">
                                    <div class="label-left">
                                        <asp:Label ID="Label5" runat="server" Text="WareHouse"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlwarehouse" runat="server" CssClass="form-control-res">
                                        </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtWarehouse" runat="server" CssClass="form-control-res"
                                            onkeydown="return fnc_ShowSearchDialogCommon(event);"></asp:TextBox>
                                    </div>
                                </div>
                                <div  id="divReason"  class="control-group-right display_none" style="width: 25%; padding-right: 10px;">
                                    <div class="label-left">
                                        <asp:Label ID="Label14" runat="server" Text="Reason Code"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                     <asp:DropDownList ID="ddlReasonCode" runat="server" CssClass="form-control-res" >
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-purchase-container">
                    <div id="divItemSearch" class="display_none">
                        <%--   <div class="float_left">
                            <asp:Label ID="Label18" runat="server" Text="Search"></asp:Label>
                        </div>
                        <div class="gidItem_Search">
                            <asp:TextBox ID="txtItemSearch" onkeydown=" return fncDailogItemSearch(event);" runat="server"></asp:TextBox>
                        </div>--%>
                        <div class="Payment_fixed_headers gid_Itemsearch gidItem_Searchtbl">
                            <table id="tblItemSearch" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr id="itemsearch" runat="server">
                                        <th scope="col" onclick="sortTable(0)">S.No
                                        </th>
                                        <th scope="col" onclick="sortTable(1)">Code
                                        </th>
                                        <th scope="col" onclick="sortTable(2)">Item Description
                                        </th>
                                        <th onclick="sortTable(3)">Batch No
                                        </th>
                                        <th scope="col" onclick="sortTable(4)">Size
                                        </th>
                                        <th scope="col" onclick="sortTable(5)">Qty in Hand
                                        </th>
                                        <th scope="col" onclick="sortTable(6)">PRRQty
                                        </th>
                                        <th scope="col" onclick="sortTable(7)">MRP
                                        </th>
                                        <th scope="col" onclick="sortTable(8)">SellingPrice
                                        </th>
                                        <th scope="col">GST(%)
                                        </th>
                                        <th scope="col">Nett
                                        </th>
                                        <th scope="col">C.D
                                        </th>
                                        <th scope="col">S.D
                                        </th>
                                        <th scope="col">Margin
                                        </th>
                                        <th scope="col">Pur.Date
                                        </th>
                                        <th scope="col">PO Qty
                                        </th>
                                        <th scope="col">Pur.Qty
                                        </th>
                                        <th scope="col">Sold.Qty
                                        </th>
                                        <th scope="col">SINO
                                        </th>
                                        <th scope="col">Exp Date
                                        </th>
                                        <th scope="col">Exp Month
                                        </th>
                                        <th scope="col">Allow Negative
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div id="divTranGrid" class="GridDetails" style="padding-left: 5px;">
                        <div class="over_flowhorizontal">
                            <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header transfer">
                                <tr>
                                    <th>S.No</th>
                                    <th>Delete</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>From.Loc.Qty</th>
                                    <th>To.Loc.Qty</th>
                                    <th id="batchNo">Batch No</th>
                                    <th>L.Qty</th>
                                    <th>MRP</th>
                                    <th>Selling</th>
                                    <th>Cost</th>
                                    <th>Total</th>
                                    <th>ExpMonth</th>
                                    <th>ExpYear</th>
                                    <th>DepartmentCode</th>
                                    <th>QtyOnHand</th>
                                    <th>QualifyItem</th>
                                    <th>Flag</th>
                                    <th>QualifyingBatch</th>
                                    <th>AllowDc</th>
                                    <th>OldCost</th>
                                    <th>OldTotal</th>
                                </tr>
                            </table>

                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 300px; width: 2135px; background-color: aliceblue;">
                                <asp:UpdatePanel runat="server" ID="upgrid" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvTransferOutAdd" runat="server" Width="100%" ShowHeader="false"
                                            AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" OnRowDataBound="grdItemDetails_RowDataBound"
                                            CssClass="pshro_GridDgn transfer">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="Records Not Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                            ToolTip="Click here to Delete" OnClick="imgbtnDelete_click" />
                                                        <%--OnClientClick="fncDelete(event);return false;""--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Inventorycode" HeaderText="Code"></asp:BoundField>
                                                <asp:BoundField DataField="Description" HeaderText="Item Description"></asp:BoundField>
                                                <asp:BoundField DataField="Stock" HeaderText="From.Loc.Qty"></asp:BoundField>
                                                <asp:BoundField DataField="StockTo" HeaderText="To.Loc.Qty"></asp:BoundField>
                                                <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                                <asp:TemplateField HeaderText="LQty">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLQty" runat="server" Text='<%# Eval("LQty") %>'
                                                            CssClass="hiddencol"></asp:Label>
                                                        <asp:TextBox ID="txtLQtygrd" runat="server" Text='<%# Eval("LQty") %>' Style="text-align: right"
                                                            onkeydown="return txtLQty_Keystroke(event,this);" onkeypress="return isNumberKeyWithDecimalNew(event)" onkeyup="ValidateText(this);"
                                                            onchange="return txtLQty_TextChanged(this);" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"
                                                            CssClass="grid-textbox"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:BoundField DataField="LQty" HeaderText="L.Qty"></asp:BoundField>--%>
                                                <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                <asp:BoundField DataField="Selling" HeaderText="Selling"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Cost">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCosst" runat="server" Text='<%# Eval("Cost") %>'
                                                            CssClass="hiddencol"></asp:Label>
                                                        <asp:TextBox ID="txtCostgrd" runat="server" Text='<%# Eval("Cost") %>' Style="text-align: right"
                                                            onkeydown="return txtCostgrd_Keystroke(event,this);" onkeypress="return isNumberKeyWithDecimalNew(event)" onkeyup="ValidateText(this);"
                                                            onchange="return txtCostgrd_TextChanged(this);" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"
                                                            CssClass="grid-textbox"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:BoundField DataField="Cost" HeaderText="Cost"></asp:BoundField>--%>
                                                <asp:BoundField DataField="Total" HeaderText="Total"></asp:BoundField>
                                                <asp:BoundField DataField="ExpMonth" HeaderText="ExpMonth"></asp:BoundField>
                                                <asp:BoundField DataField="ExpYear" HeaderText="ExpYear"></asp:BoundField>
                                                <asp:BoundField DataField="DepartmentCode" HeaderText="DepartmentCode"></asp:BoundField>
                                                <asp:BoundField DataField="QtyOnHand" HeaderText="QtyOnHand"></asp:BoundField>
                                                <asp:BoundField DataField="QualifyItem" HeaderText="QualifyItem"></asp:BoundField>
                                                <asp:BoundField DataField="Flag" HeaderText="Flag"></asp:BoundField>
                                                <asp:BoundField DataField="QualifyingBatch" HeaderText="QualifyingBatch"></asp:BoundField>
                                                <asp:BoundField DataField="AllowDC" HeaderText="AllowDC" ItemStyle-CssClass="hidden"></asp:BoundField>
                                                <asp:BoundField DataField="Cost" HeaderText="Net Cost" ItemStyle-CssClass="hidden"></asp:BoundField>
                                                <asp:BoundField DataField="Total" HeaderText="Total" ItemStyle-CssClass="hidden"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-purchase-container-add">
                    <div class="bottom-purchase-container-add-Text">
                        <div class="control-group-split">
                            <div class="container7">
                                <div class="label-left">
                                    <asp:Label ID="Label61" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtInventoryCode" runat="server" onkeydown="return fncGetInventoryCode_onEnter(event);"
                                        CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="transfer_container1" style="width: 30%;">
                                <div class="label-left">
                                    <asp:Label ID="Label71" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtItemNameAdd" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                </div>
                                </div>
                                <%-- <div class="col-md-2">
                                      <div class="col-md-6">--%>
                                     <%--<div class="label-right" style="width: 100%">
                  <asp:Label ID="lblWUom" runat="server" Text='<%$ Resources:LabelCaption,lbl_WUOM %>'></asp:Label>
                                         </div>--%>
                                         <%-- <asp:Label ID="lblWUom" runat="server" Text='<%$ Resources:LabelCaption,lbl_WUOM %>'></asp:Label>
                                      </div>  
                                        <div class="col-md-6">
                                            <%--<asp:TextBox ID="txtwuom" runat="server" CssClass="form-control-res-right" Enabled="false"></asp:TextBox>--%>
                                          <%--  <div id="divMultiple" runat="server">
                                                <asp:DropDownList ID="ddlMultipleUOM" runat="server" Width="100%"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>--%>
                            <div class="container2">
                                <div class="label-left">
                                    <asp:Label ID="Label2" runat="server" Text="Batch No"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res"
                                        Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="container2">
                                <div class="label-left">
                                    <asp:Label ID="Label13" runat="server" Text="Stock"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtStock" runat="server" CssClass="form-control-res-right" Text="0"
                                        Font-Bold="true" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>


                             <div class="container2" id="divUOM" style="display:none" >
                                <div class="label-left">
                                    <asp:Label ID="lblWUom" runat="server" Text='<%$ Resources:LabelCaption,lbl_WUOM %>'></asp:Label>
                                </div>
                                <div class="label-right">
                                    <div id="divMultiple" runat="server" >
                                                <asp:DropDownList ID="ddlMultipleUOM" runat="server" Width="67%"></asp:DropDownList>
                                            </div>
                                </div>
                               </div>

                            <div class="container2">
                                <div class="label-left">
                                    <asp:Label ID="Label141" runat="server" Text="W.Qty"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtWQty" runat="server" onkeydown="fncFoucustoLQty(event);" onkeypress='return numericOnly(event);'
                                         onFocus="this.select()"
                                        CssClass="form-control-res-right" 
                                        Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="container2">
                                <div class="label-left">
                                    <asp:Label ID="Label151" runat="server" Text="L.Qty"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtLQty" runat="server" CssClass="form-control-res-right" onFocus="this.select()"
                                        onkeypress='return numericOnly(event);' Font-Bold="true" onkeydown="fncCost(event); "></asp:TextBox>
                                </div>
                            </div>
                            <div class="container2">
                                <div class="label-left">
                                    <asp:Label ID="Label161" runat="server" Text="MRP"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtMrp" runat="server" CssClass="form-control-res-right" onkeypress='return numericOnly(event);'
                                        Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="container10 float_left">
                                <div class="label-left">
                                    <asp:Label ID="Label10" runat="server" Text="S.Price"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtSellingPrice" onkeypress='return numericOnly(event);' runat="server"
                                        CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="container2">
                                <div class="label-left">
                                    <asp:Label ID="Label171" runat="server" Text="Cost"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtCost" runat="server" onkeypress='return numericOnly(event);' CssClass="form-control-res-right"
                                        Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="container2">
                                <div class="label-left">
                                    <asp:Label ID="Label12" runat="server" Text="Total Cost"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtTotCost" runat="server" CssClass="form-control-res-right"
                                        Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>

                            <div class="transfer_Add">
                                <asp:UpdatePanel ID="upAdditem" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <div class="container2">
                                            <div class="control-button">
                                                <div class="label-left" style="width: 100%">
                                                    <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue transfer_btn" OnClientClick=" return fncAddValidation();" OnClick="lnkAdd_Click"><i class="icon-play"></i>Add</asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="control-button">
                                                <div class="label-right" style="width: 100%">
                                                    <asp:LinkButton ID="lnkClear" runat="server" class="button-blue transfer_btn" OnClientClick="fncSubClear();return false;"><i class="icon-play"></i>Clear</asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                        </div>
                    </div>
                    <div style="width: 100%">
                        <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel runat="server" ID="upSave" UpdateMode="Always">
                    <ContentTemplate>
                        <div class="col-md-12" style="margin-top: 5px;">
                            <div class="col-md-8">
                                <div class="col-md-3">
                                    <div class="transferOut_Totalpadding transferOutlblLen">
                                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalItems %>'></asp:Label>
                                    </div>
                                    <div class="transferOut_Totalpadding transferOutlblLen">
                                        <asp:TextBox ID="txtTotalItems" runat="server" Text="0.00" CssClass="form-control-res-right"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="col-md-6">
                                        <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalQty %>'></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtTotalQty" runat="server" Text="0.00" CssClass="form-control-res-right"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="col-md-6">
                                        <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_Totalvalue %>'></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtTotalValue" runat="server" Text="0.00" CssClass="form-control-res-right"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="divHandlingCharge" class="col-md-4">
                                    <div class="col-md-6">
                                        <asp:Label ID="Label9" runat="server" Style="font-weight: 600;" Text="Handling Charge"></asp:Label>
                                    </div>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="txtHandlingCharge" runat="server" CssClass="form-control-res-right" Text="0"
                                            onkeypress='return numericOnly(event);' Font-Bold="true" Style="text-align: right"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkTransfer" runat="server" class="button-blue" OnClientClick="fncTransferValidation();return false;" OnClick="lnkSave_Click" Text="Transfer(F4)"></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkStylecode" runat="server" class="button-blue-withoutanim" OnClientClick="fncShowMultipUOM();return false;"
                                        Text='StyleCode'></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" OnClientClick="clearForm()"
                                        OnClick="lnkGridClear_Click" Text="Clear(F6)"></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkTransferList" runat="server" class="button-blue"
                                        OnClick="lnkTransferList_Click" Text="Transfer Out List(F8)"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">

                            <div class="col-md-6" id="divQty" runat="server" style="visibility: hidden">
                                <asp:RadioButtonList ID="rdoGstType" runat="server" Class="radioboxlist" onclick="return false;" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Qty on Hand is Less than or Equal to Zero" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Actual Qty is Applied" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="container-group-full">
                <asp:HiddenField ID="HidenIsBatch" runat="server" ClientIDMode="Static" Value="" />
                <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
                <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
                <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
                <asp:HiddenField ID="hidUOMConv" runat="server" Value="" />
                <asp:HiddenField ID="hidLUOM" runat="server" Value="" />
                <asp:HiddenField ID="hidWOM" runat="server" Value="" />
                <asp:HiddenField ID="hidActQty" runat="server" Value="" />
                <asp:HiddenField ID="hidBatchNoText" runat="server" Value="" />
                <asp:HiddenField ID="hidStyleCode" runat="server" Value="" />
                <asp:HiddenField ID="hidAllownegtr" runat="server" Value="" />
            </div>
            <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>

        <div id="Save-dialog" style="display: none">
        </div>
        <div class="display_none">
            <div id="TransferAdd">
                <div>
                    <asp:Label ID="lblAddMsg" runat="server"></asp:Label>
                </div>
                <div class="float_right">
                    <div class="inv_dailog_btn">
                        <asp:LinkButton ID="lnkNewItemYes" runat="server" Text="Yes" class="button-blue-withoutanim"
                            OnClientClick="fncYesClick_master();return false;"></asp:LinkButton>
                    </div>
                    <div class="inv_dailog_btn">
                        <button type="button" id="lnkNew" runat="server" class="button-blue-withoutanim"
                            onclick="return fncNoClick();">
                            No</button>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnGid" runat="server" OnClick="btnGid_click" />

            <div id="dialog-Gid">
                <div>
                    <div class="float_left">
                        <asp:Label ID="lblItemSearch" runat="server" Text="Search"></asp:Label>
                    </div>
                    <div class="gidItem_Search">
                        <asp:TextBox ID="txtGidSearch" runat="server" class="form-control-res"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>


        <div id="divStyleCode" title="Style Code Wise Stock">
            <div class="Payment_fixed_headers multipleUOM">
                <table id="tblStylecode" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">Style Code
                            </th>
                            <th scope="col">Qty
                            </th>
                            <th scope="col">Rate
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>

        </div>
        <div class="display_none">
            <asp:Button ID="btnSave" runat="server" OnClick="lnkSave_Click" />
            <asp:HiddenField ID="hidExpMonth" runat="server" Value="" />
            <asp:HiddenField ID="hidExpYear" runat="server" Value="" />
            <asp:HiddenField ID="hidQtyOnHand" runat="server" Value="" />
            <asp:HiddenField ID="hidDepartmentCode" runat="server" Value="" />
            <asp:HiddenField ID="hidQualifyItem" runat="server" Value="" />
            <asp:HiddenField ID="hidFlag" runat="server" Value="" />
            <asp:HiddenField ID="hidQualifyingBatch" runat="server" Value="" />
            <asp:HiddenField ID="hidTempSave" runat="server" Value="" />
            <asp:HiddenField ID="hidAllNeg" runat="server" Value="" />
            <asp:HiddenField ID="hidAllowNegative" runat="server" Value="" />
            <asp:HiddenField ID="hidItemAllowNegative" runat="server" Value="" />
            <asp:HiddenField ID="hidItemAllowDC" runat="server" Value="" />
        </div>
        <asp:HiddenField ID="comment_like" runat="server" Value='<%#Eval("comment_id") %>' />

        <div class="container-group-full" style="display: none">
            <div id="ConfirmSaveDialog">
                <p>
                    Qty is Greater than Balance Qty
                </p>
                <div style="width: 150px; margin: auto">
                    <div style="float: left">
                        <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                            Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                    </div>
                    <div style="float: right">
                        <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                            Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
         
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidHandlingCharge" runat="server" />
        <asp:HiddenField ID="hidNetCostChange" runat="server" />
        <asp:HiddenField ID="hidLocation" runat="server" />      
        <asp:HiddenField ID="hidReason" runat="server" />
  </div>
    </asp:Content>
