﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="ViewReport.aspx.cs" 
Inherits="EnterpriserWebFinal.Transfer.ViewReport" %>

<%--<%@ Page  Title="" Language="C#" AutoEventWireup="true"  EnableEventValidation="true"  CodeFile="View.aspx.cs" Inherits="View" %>--%>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="GridDetails" style="width: 100%">
        <br />
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" Width="100%">
            <ContentTemplate>
                <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" EnableDatabaseLogonPrompt="False"
                    DisplayGroupTree="False" Width="100%" BestFitPage="True" />
                <asp:Label ID="Label5" runat="server" Text="Label" Visible="False"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="CrystalReportViewer1" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <%--DisplayGroupTree="False" ToolbarStyle-BorderStyle="Solid" BestFitPage="True"
                ToolbarStyle-BackColor="#FFCC66" Width="100%" PrintMode="ActiveX"--%>
</asp:Content>
